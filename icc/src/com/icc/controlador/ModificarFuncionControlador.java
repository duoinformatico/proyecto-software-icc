package com.icc.controlador;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.icc.modelo.Funcion;
import com.icc.utilidades.Conexion;

/**
 * Servlet implementation class ModificarFuncionControlador
 */
@WebServlet("/ModificarFuncionControlador")
public class ModificarFuncionControlador extends HttpServlet {
	private static final long serialVersionUID = 1L;
    String mensaje="";
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ModificarFuncionControlador() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String funcion_id = request.getParameter("funcion_id");
		request.setAttribute("id", funcion_id);
		request.setAttribute("funcion", new Conexion().getFuncion(Integer.parseInt(funcion_id)));
		request.getRequestDispatcher("ajax/modificar_funcion.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("funcion_id_m");
		String nombre = request.getParameter("nombre_m");
		int exito = 0;
		try {	
			int i = new Conexion().ModificarDatosFuncion(new Funcion(Integer.parseInt(id),nombre));
			if(i == 1)
			{
				mensaje = "Los datos de la Función fueron modificados correctamente";
				request.setAttribute("mensaje", mensaje);
				exito = 1;
				request.setAttribute("exito", exito);
				request.setAttribute("funcionesList", new Conexion().getFunciones());
				request.getRequestDispatcher("vistas_admin/funciones.jsp").forward(request, response);
			}
			else{
				mensaje = "Hubo un error al modificar los datos de la Función";
				request.setAttribute("mensaje", mensaje);
				exito = 0;
				request.setAttribute("exito", exito);
				request.setAttribute("funcionesList", new Conexion().getFunciones());
				request.getRequestDispatcher("vistas_admin/funciones.jsp").forward(request, response);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
