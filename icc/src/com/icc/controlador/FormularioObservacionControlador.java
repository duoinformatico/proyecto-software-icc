package com.icc.controlador;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.icc.modelo.Observacion;
import com.icc.modelo.Practica;
import com.icc.utilidades.Conexion;

/**
 * Servlet implementation class FormularioObservacionControlador
 */
@WebServlet("/FormularioObservacion")
public class FormularioObservacionControlador extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FormularioObservacionControlador() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String bit_id =  request.getParameter("bit_id");
		String pra_id =  request.getParameter("pra_id");
		request.setCharacterEncoding("UTF-8");
		String observacion = request.getParameter("observacion");
		String tipo = (String) request.getSession().getAttribute("tipo");
		String usuario = (String) request.getSession().getAttribute("usuario");
		int exito =0;
		String mensaje="";
		try {
			new Conexion().agregarObservacion(observacion+" \n - Observación realizada por :"+usuario);
			Observacion obs = new Conexion().obtenerObservacion(observacion+" \n - Observación realizada por :"+usuario);
			new Conexion().agregarObservacionBitacora(Integer.parseInt(bit_id),obs.getObs_id());
			request.setAttribute("bitacoraList",new Conexion().getBitacorasAlumno(Integer.parseInt(pra_id)));
			Practica practica = new Conexion().getPractica(Integer.parseInt(pra_id));
			request.setAttribute("practica", practica);
			request.setAttribute("alumno", new Conexion().getDatosAlumno(practica.getAlu_rut()));
			mensaje = "Se ha registrado la Observación exitosamente";
			request.setAttribute("mensaje", mensaje);
			exito = 1;
			request.setAttribute("exito", exito);
			if(tipo == "supervisor")
			{
				request.getRequestDispatcher("vistas_supervisor/bitacoras_alumno.jsp").forward(request, response);
			}
			if(tipo == "academico" || tipo == "administrador-academico")
			{
				request.getRequestDispatcher("vistas_academico/bitacoras_alumno.jsp").forward(request, response);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			mensaje = "Error al registrar la Observación";
			request.setAttribute("mensaje", mensaje);
			exito = 0;
			request.setAttribute("exito", exito);
			if(tipo == "supervisor")
			{
				request.getRequestDispatcher("vistas_supervisor/bitacoras_alumno.jsp").forward(request, response);
			}
			if(tipo == "academico" || tipo == "administrador-academico")
			{
				request.getRequestDispatcher("vistas_academico/bitacoras_alumno.jsp").forward(request, response);
			}
		}
	}

}
