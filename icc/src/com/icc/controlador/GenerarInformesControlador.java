package com.icc.controlador;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;

import com.icc.modelo.Academico;
import com.icc.modelo.Alumno;
import com.icc.modelo.Area;
import com.icc.modelo.Asignatura;
import com.icc.modelo.Bitacora;
import com.icc.modelo.Comuna;
import com.icc.modelo.Empresa;
import com.icc.modelo.Practica;
import com.icc.modelo.Supervisor;
import com.icc.utilidades.Conexion;

/**
 * Servlet implementation class GenerarInformesControlador
 */
@WebServlet("/GenerarInformesControlador")
public class GenerarInformesControlador extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GenerarInformesControlador() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("/vistas_admin/informes.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String tipo = (String) request.getSession().getAttribute("tipo");
		String tipo_informe = request.getParameter("tipo_informe");
		
		//genera informe del resumen de bitacoras , recibe id == 1
		
		if(tipo == "academico" || tipo == "administrador-academico" || tipo == "alumno")
		{
			if(tipo_informe.equals("10"))
			{
					int pra_id =  Integer.parseInt(request.getParameter("pra_id"));
					try 
					{
						Practica practica = new Conexion().getPractica(pra_id);
						Supervisor supervisor = new Conexion().getSupervisor(practica.getSup_rut());
						Academico academico = new Conexion().getAcademico(practica.getAca_rut());
						Alumno alumno = new Conexion().getDatosAlumno(practica.getAlu_rut());
						Asignatura asignatura = new Conexion().getAsignatura(practica.getAsi_codigo(), practica.getPlan_id());
						response.setHeader("Content-Disposition", "attachment; filename=\"ResumenBitacoras"+alumno.getAlu_nombres()+"_"+alumno.getAlu_apellido_p()+"_"+alumno.getAlu_apellido_m()+".pdf\";");
					    response.setHeader("Cache-Control", "no-cache");
					    response.setHeader("Pragma", "no-cache");
					    response.setDateHeader("Expires", 0);
						response.setContentType("application/pdf");
						ServletOutputStream out = response.getOutputStream();
						List<Bitacora> bitacoras = new ArrayList<Bitacora>();
						
						bitacoras = new Conexion().getBitacorasDescargar(pra_id,practica.getPlan_id());
						@SuppressWarnings("deprecation")
						JasperReport reporte = (JasperReport) JRLoader.loadObject(getServletContext().getRealPath("WEB-INF/informes/bitacoras.jasper"));
						Map<String, Object> parametros = new HashMap<String, Object>();
					    parametros.put("obra", practica.getPra_obra_nombre());
					    parametros.put("gestion", asignatura.getAsi_codigo()+" "+asignatura.getAsi_nombre());
					    parametros.put("alumno", alumno.getAlu_nombres()+" "+alumno.getAlu_apellido_p()+" "+alumno.getAlu_apellido_m());
					    parametros.put("supervisor", supervisor.getSup_nombres()+" "+supervisor.getSup_apellido_p()+" "+supervisor.getSup_apellido_m());
					    parametros.put("profesor", academico.getAca_nombres()+" "+academico.getAca_apellido_p()+" "+academico.getAca_apellido_m());
					    JasperPrint jasperPrint = JasperFillManager.fillReport(reporte, parametros, new JRBeanCollectionDataSource(bitacoras));
					    JRExporter exporter = new JRPdfExporter();
					    exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
					    exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, out);
					    exporter.exportReport();
					} catch (Exception e) {
						e.printStackTrace();
					}
			 }
	   }
	   if( tipo == "administrador-academico" || tipo == "admin" )
	   {
		   if(tipo_informe.equals("1"))
		   {
				try 
				{
					String estado = request.getParameter("informe");
					int asi_codigo = Integer.parseInt(request.getParameter("practica"));
					response.setHeader("Content-Disposition", "attachment; filename=\"informeGestiones.pdf\";");
				    response.setHeader("Cache-Control", "no-cache");
				    response.setHeader("Pragma", "no-cache");
				    response.setDateHeader("Expires", 0);
					response.setContentType("application/pdf");
					ServletOutputStream out = response.getOutputStream();
					List<Practica> practicas = new ArrayList<Practica>();
					practicas = new Conexion().getAlumnosEnPractica(estado,asi_codigo); 
					@SuppressWarnings("deprecation")
					JasperReport reporte = (JasperReport) JRLoader.loadObject(getServletContext().getRealPath("WEB-INF/informes/informeGestiones.jasper"));
					Map<String, Object> parametros = new HashMap<String, Object>();
					parametros.put("gestion1", new Conexion().contarAlumnosEnGestiones(asi_codigo,estado));
					if(estado.equals("activa"))
					{
						parametros.put("estado", "2. Listado de Alumnos con Práctica Activa");
					}

					if(estado.equals("inactiva"))
					{
						parametros.put("estado", "2. Listado de Alumnos con Práctica Aprobada o Reprobada");
					}
					JasperPrint jasperPrint = JasperFillManager.fillReport(reporte, parametros, new JRBeanCollectionDataSource(practicas));
				    JRExporter exporter = new JRPdfExporter();
				    exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
				    exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, out);
				    exporter.exportReport();
				} catch (JRException e) {
					e.printStackTrace();
				}
		   }
		   if(tipo_informe.equals("2"))
		   {
				try 
				{
					String estado = request.getParameter("informe");
					response.setHeader("Content-Disposition", "attachment; filename=\"informeRegiones.pdf\";");
				    response.setHeader("Cache-Control", "no-cache");
				    response.setHeader("Pragma", "no-cache");
				    response.setDateHeader("Expires", 0);
					response.setContentType("application/pdf");
					ServletOutputStream out = response.getOutputStream();
					List<Practica> practicas = new ArrayList<Practica>();
					practicas = new Conexion().getRegionesPracticas(estado); 
					@SuppressWarnings("deprecation")
					JasperReport reporte = (JasperReport) JRLoader.loadObject(getServletContext().getRealPath("WEB-INF/informes/informeRegiones.jasper"));
					Map<String, Object> parametros = new HashMap<String, Object>();
					if(estado.equals("activa"))
					{
						parametros.put("estado", "2. Listado de Alumnos con Práctica Activa por Región");
						parametros.put("estado2", "1. Total de Alumnos con Práctica Activa por Región");
					}

					if(estado.equals("inactiva"))
					{
						parametros.put("estado2", "1. Histórico Total de Alumnos con Práctica Terminada por Región");
						parametros.put("estado", "2. Listado de Alumnos con Práctica Terminada por Región");
					}
					parametros.put("arica", new Conexion().contarRegion(estado,1));
					parametros.put("tarapaca", new Conexion().contarRegion(estado,2));
					parametros.put("antofagasta",new Conexion().contarRegion(estado,3) );
					parametros.put("atacama", new Conexion().contarRegion(estado,4));
					parametros.put("coquimbo", new Conexion().contarRegion(estado,5));
					parametros.put("valparaiso", new Conexion().contarRegion(estado,6));
					parametros.put("metropolitana", new Conexion().contarRegion(estado,7));
					parametros.put("ohiggins", new Conexion().contarRegion(estado,8));
					parametros.put("maule", new Conexion().contarRegion(estado,9));
					parametros.put("biobio", new Conexion().contarRegion(estado,10));
					parametros.put("araucania", new Conexion().contarRegion(estado,11));
					parametros.put("rios", new Conexion().contarRegion(estado,12));
					parametros.put("lagos", new Conexion().contarRegion(estado,13));
					parametros.put("aysen", new Conexion().contarRegion(estado,14));
					parametros.put("magallanes",new Conexion().contarRegion(estado,15) );
					JasperPrint jasperPrint = JasperFillManager.fillReport(reporte, parametros, new JRBeanCollectionDataSource(practicas));
				    JRExporter exporter = new JRPdfExporter();
				    exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
				    exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, out);
				    exporter.exportReport();
				} catch (JRException e) {
					e.printStackTrace();
				}
		   }
		   if(tipo_informe.equals("3"))
		   {
				try 
				{
					String accion = request.getParameter("accion");
					if (accion == null) {
					    System.out.println("No fue seleccionado nada");
					} else if (accion.equals("listado")) {
						String estado = request.getParameter("informe");
						response.setHeader("Content-Disposition", "attachment; filename=\"informeListadoPorComunas.pdf\";");
					    response.setHeader("Cache-Control", "no-cache");
					    response.setHeader("Pragma", "no-cache");
					    response.setDateHeader("Expires", 0);
						response.setContentType("application/pdf");
						ServletOutputStream out = response.getOutputStream();
						List<Practica> practicas = new ArrayList<Practica>();
						practicas = new Conexion().getAlumnoComunasPracticas(estado); 
						@SuppressWarnings("deprecation")
						JasperReport reporte = (JasperReport) JRLoader.loadObject(getServletContext().getRealPath("WEB-INF/informes/listadoComunas.jasper"));
						Map<String, Object> parametros = new HashMap<String, Object>();
						if(estado.equals("activa"))
						{
							parametros.put("estado", "1. Listado de Alumnos con Práctica Activa por Comuna (Solo se muestran las Comunas que tienen más de un alumno en práctica)");
						}

						if(estado.equals("inactiva"))
						{
							parametros.put("estado", "1. Listado de Alumnos con Práctica Terminada por Comuna (Solo se muestran las Comunas que tubieron más de un alumno en práctica)");
						}
						
						JasperPrint jasperPrint = JasperFillManager.fillReport(reporte, parametros, new JRBeanCollectionDataSource(practicas));
					    JRExporter exporter = new JRPdfExporter();
					    exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
					    exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, out);
					    exporter.exportReport();
					} else if (accion.equals("total")) {
						String estado = request.getParameter("informe");
						response.setHeader("Content-Disposition", "attachment; filename=\"informeComunas.pdf\";");
					    response.setHeader("Cache-Control", "no-cache");
					    response.setHeader("Pragma", "no-cache");
					    response.setDateHeader("Expires", 0);
						response.setContentType("application/pdf");
						ServletOutputStream out = response.getOutputStream();
						List<Comuna> comunas = new ArrayList<Comuna>();
						comunas = new Conexion().getComunasPracticas(estado); 
						@SuppressWarnings("deprecation")
						JasperReport reporte = (JasperReport) JRLoader.loadObject(getServletContext().getRealPath("WEB-INF/informes/informeComunas.jasper"));
						Map<String, Object> parametros = new HashMap<String, Object>();
						System.out.println(estado);
						if(estado.equals("activa"))
						{
							parametros.put("estado2", "1. Total de Alumnos con Práctica Activa por Comuna (Solo se muestran las Comunas que tienen uno o más alumnos en práctica)");
						}

						if(estado.equals("inactiva"))
						{
							parametros.put("estado2", "1. Histórico Total de Alumnos con Práctica Terminada por Comuna (Solo se muestran las Comunas que tubieron uno o más alumnos en práctica)");
						}
						
						JasperPrint jasperPrint = JasperFillManager.fillReport(reporte, parametros, new JRBeanCollectionDataSource(comunas));
					    JRExporter exporter = new JRPdfExporter();
					    exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
					    exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, out);
					    exporter.exportReport();
					}
					
				} catch (JRException e)
				{
					e.printStackTrace();
				}
		   }
		   if(tipo_informe.equals("4"))
		   {
				try 
				{
					String accion = request.getParameter("accion");
					if (accion == null) {
					    System.out.println("No fue seleccionado nada");
					} else if (accion.equals("listado")) {
						String estado = request.getParameter("informe");
						int asi_codigo = Integer.parseInt(request.getParameter("practica"));
						response.setHeader("Content-Disposition", "attachment; filename=\"informeListadoAlumnosEmpresas.pdf\";");
					    response.setHeader("Cache-Control", "no-cache");
					    response.setHeader("Pragma", "no-cache");
					    response.setDateHeader("Expires", 0);
						response.setContentType("application/pdf");
						ServletOutputStream out = response.getOutputStream();
						List<Practica> practicas = new ArrayList<Practica>();
						practicas = new Conexion().getListadoAlumnosEmpresasPracticas(estado,asi_codigo); 
						@SuppressWarnings("deprecation")
						JasperReport reporte = (JasperReport) JRLoader.loadObject(getServletContext().getRealPath("WEB-INF/informes/informeListadoEmpresas.jasper"));
						Map<String, Object> parametros = new HashMap<String, Object>();
						System.out.println(estado);
						if(estado.equals("activa"))
						{
							parametros.put("estado", "1. Listado de Alumnos por Empresa con Práctica Activa");
						}

						if(estado.equals("inactiva"))
						{
							parametros.put("estado", "1. Listado de Alumnos por Empresa con Práctica Terminada");
						}
						
						JasperPrint jasperPrint = JasperFillManager.fillReport(reporte, parametros, new JRBeanCollectionDataSource(practicas));
					    JRExporter exporter = new JRPdfExporter();
					    exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
					    exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, out);
					    exporter.exportReport();
					} else if (accion.equals("total")) {
						String estado = request.getParameter("informe");
						int asi_codigo = Integer.parseInt(request.getParameter("practica"));
						response.setHeader("Content-Disposition", "attachment; filename=\"informeTotalAlumnosEmpresas.pdf\";");
					    response.setHeader("Cache-Control", "no-cache");
					    response.setHeader("Pragma", "no-cache");
					    response.setDateHeader("Expires", 0);
						response.setContentType("application/pdf");
						ServletOutputStream out = response.getOutputStream();
						List<Empresa> empresas = new ArrayList<Empresa>();
						empresas = new Conexion().getTotalEmpresasPracticas(estado,asi_codigo); 
						@SuppressWarnings("deprecation")
						JasperReport reporte = (JasperReport) JRLoader.loadObject(getServletContext().getRealPath("WEB-INF/informes/informeTotalEmpresas.jasper"));
						Map<String, Object> parametros = new HashMap<String, Object>();
						System.out.println(estado);
						if(estado.equals("activa"))
						{
							parametros.put("estado", "1. Total de Alumnos por Empresa con Práctica Activa");
						}

						if(estado.equals("inactiva"))
						{
							parametros.put("estado", "1. Histórico Total de Alumnos por Empresa con Práctica Terminada");
						}
						
						JasperPrint jasperPrint = JasperFillManager.fillReport(reporte, parametros, new JRBeanCollectionDataSource(empresas));
					    JRExporter exporter = new JRPdfExporter();
					    exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
					    exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, out);
					    exporter.exportReport();
					}
					
				} catch (JRException e)
				{
					e.printStackTrace();
				}
		   }
		   if(tipo_informe.equals("5"))
		   {
				try 
				{
					String accion = request.getParameter("accion");
					if (accion == null) {
					    System.out.println("No fue seleccionado nada");
					} else if (accion.equals("listado")) {
						int asi_codigo = Integer.parseInt(request.getParameter("practica"));
						response.setHeader("Content-Disposition", "attachment; filename=\"informeListadoAlumnosTipoObra.pdf\";");
					    response.setHeader("Cache-Control", "no-cache");
					    response.setHeader("Pragma", "no-cache");
					    response.setDateHeader("Expires", 0);
						response.setContentType("application/pdf");
						ServletOutputStream out = response.getOutputStream();
						List<Practica> practicas = new ArrayList<Practica>();
						practicas = new Conexion().getListadoAlumnosTipoObraPracticas(asi_codigo); 
						@SuppressWarnings("deprecation")
						JasperReport reporte = (JasperReport) JRLoader.loadObject(getServletContext().getRealPath("WEB-INF/informes/informeListadoTipoObra.jasper"));
						Map<String, Object> parametros = new HashMap<String, Object>();
						parametros.put("estado", "1. Listado de Alumnos por Tipo de Obra Segun Gestión");
						
						JasperPrint jasperPrint = JasperFillManager.fillReport(reporte, parametros, new JRBeanCollectionDataSource(practicas));
					    JRExporter exporter = new JRPdfExporter();
					    exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
					    exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, out);
					    exporter.exportReport();
					} else if (accion.equals("total")) {
						int asi_codigo = Integer.parseInt(request.getParameter("practica"));
						response.setHeader("Content-Disposition", "attachment; filename=\"informeTotalAlumnosTipoObra.pdf\";");
					    response.setHeader("Cache-Control", "no-cache");
					    response.setHeader("Pragma", "no-cache");
					    response.setDateHeader("Expires", 0);
						response.setContentType("application/pdf");
						ServletOutputStream out = response.getOutputStream();
						List<Practica> practicas = new ArrayList<Practica>();
						practicas = new Conexion().getTotalTipoObraPracticas(asi_codigo); 
						@SuppressWarnings("deprecation")
						JasperReport reporte = (JasperReport) JRLoader.loadObject(getServletContext().getRealPath("WEB-INF/informes/informeTotalAlumnosTipoObra.jasper"));
						Map<String, Object> parametros = new HashMap<String, Object>();
						parametros.put("estado", "1. Total de Alumnos por Tipo de Obra Según Gestión");
						JasperPrint jasperPrint = JasperFillManager.fillReport(reporte, parametros, new JRBeanCollectionDataSource(practicas));
					    JRExporter exporter = new JRPdfExporter();
					    exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
					    exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, out);
					    exporter.exportReport();
					}
					
				} catch (JRException e)
				{
					e.printStackTrace();
				}
		   }
		   if(tipo_informe.equals("6"))
		   {
				try 
				{
					String accion = request.getParameter("accion");
					if (accion == null) {
					    System.out.println("No fue seleccionado nada");
					} else if (accion.equals("listado")) {
						int asi_codigo = Integer.parseInt(request.getParameter("practica"));
						String estado = request.getParameter("informe");
						response.setHeader("Content-Disposition", "attachment; filename=\"informeListadoAlumnosSupervisor.pdf\";");
					    response.setHeader("Cache-Control", "no-cache");
					    response.setHeader("Pragma", "no-cache");
					    response.setDateHeader("Expires", 0);
						response.setContentType("application/pdf");
						ServletOutputStream out = response.getOutputStream();
						List<Practica> practicas = new ArrayList<Practica>();
						practicas = new Conexion().getListadoAlumnosSupervisorPracticas(estado,asi_codigo); 
						@SuppressWarnings("deprecation")
						JasperReport reporte = (JasperReport) JRLoader.loadObject(getServletContext().getRealPath("WEB-INF/informes/informeListadoAlumnosSupervisor.jasper"));
						Map<String, Object> parametros = new HashMap<String, Object>();
						if(estado.equals("activa"))
						{
							parametros.put("estado", "1. Listado de Alumnos por Supervisor según Gestión Elegida con Práctica Vigente");
						}
						if(estado.equals("inactiva"))
						{
							parametros.put("estado", "1. Histórico de Alumnos por Supervisor según Gestión Elegida con Práctica Terminada");
						}
						JasperPrint jasperPrint = JasperFillManager.fillReport(reporte, parametros, new JRBeanCollectionDataSource(practicas));
					    JRExporter exporter = new JRPdfExporter();
					    exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
					    exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, out);
					    exporter.exportReport();
					} else if (accion.equals("total")) {
						String estado = request.getParameter("informe");
						int asi_codigo = Integer.parseInt(request.getParameter("practica"));
						response.setHeader("Content-Disposition", "attachment; filename=\"informeTotalAlumnosSupervisor.pdf\";");
					    response.setHeader("Cache-Control", "no-cache");
					    response.setHeader("Pragma", "no-cache");
					    response.setDateHeader("Expires", 0);
						response.setContentType("application/pdf");
						ServletOutputStream out = response.getOutputStream();
						List<Supervisor> supervisores = new ArrayList<Supervisor>();
						supervisores = new Conexion().getTotalSupervisorPracticas(estado,asi_codigo); 
						@SuppressWarnings("deprecation")
						JasperReport reporte = (JasperReport) JRLoader.loadObject(getServletContext().getRealPath("WEB-INF/informes/informeTotalAlumnosSupervisor.jasper"));
						Map<String, Object> parametros = new HashMap<String, Object>();
						if(estado.equals("activa"))
						{
							parametros.put("estado", "1. Total de Alumnos por Supervisor según Gestión Elegida con Práctica Vigente");
						}
						if(estado.equals("inactiva"))
						{
							parametros.put("estado", "1. Histórico Total de Alumnos por Supervisor según Gestión Elegida con Práctica Terminada");
						}
						
						JasperPrint jasperPrint = JasperFillManager.fillReport(reporte, parametros, new JRBeanCollectionDataSource(supervisores));
					    JRExporter exporter = new JRPdfExporter();
					    exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
					    exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, out);
					    exporter.exportReport();
					}
					
				} catch (JRException e)
				{
					e.printStackTrace();
				}
		   }
		   if(tipo_informe.equals("7"))
		   {
				try 
				{
					String accion = request.getParameter("accion");
					if (accion == null) {
					    System.out.println("No fue seleccionado nada");
					} else if (accion.equals("listado")) {
						int asi_codigo = Integer.parseInt(request.getParameter("practica"));
						String estado = request.getParameter("informe");
						response.setHeader("Content-Disposition", "attachment; filename=\"informeListadoAlumnosAreas.pdf\";");
					    response.setHeader("Cache-Control", "no-cache");
					    response.setHeader("Pragma", "no-cache");
					    response.setDateHeader("Expires", 0);
						response.setContentType("application/pdf");
						ServletOutputStream out = response.getOutputStream();
						List<Practica> practicas = new ArrayList<Practica>();
						practicas = new Conexion().getListadoAlumnosAreasPracticas(estado,asi_codigo); 
						@SuppressWarnings("deprecation")
						JasperReport reporte = (JasperReport) JRLoader.loadObject(getServletContext().getRealPath("WEB-INF/informes/informeListadoAlumnosAreas.jasper"));
						Map<String, Object> parametros = new HashMap<String, Object>();
						if(estado.equals("activa"))
						{
							parametros.put("estado", "1. Listado de Alumnos por Area según Gestión Elegida con Práctica Vigente");
						}
						if(estado.equals("inactiva"))
						{
							parametros.put("estado", "1. Histórico de Alumnos por Area según Gestión Elegida con Práctica Terminada");
						}
						JasperPrint jasperPrint = JasperFillManager.fillReport(reporte, parametros, new JRBeanCollectionDataSource(practicas));
					    JRExporter exporter = new JRPdfExporter();
					    exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
					    exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, out);
					    exporter.exportReport();
					} else if (accion.equals("total")) {
						String estado = request.getParameter("informe");
						int asi_codigo = Integer.parseInt(request.getParameter("practica"));
						response.setHeader("Content-Disposition", "attachment; filename=\"informeTotalAlumnosArea.pdf\";");
					    response.setHeader("Cache-Control", "no-cache");
					    response.setHeader("Pragma", "no-cache");
					    response.setDateHeader("Expires", 0);
						response.setContentType("application/pdf");
						ServletOutputStream out = response.getOutputStream();
						List<Area> areas = new ArrayList<Area>();
						areas = new Conexion().getTotalAreasPracticas(estado,asi_codigo); 
						@SuppressWarnings("deprecation")
						JasperReport reporte = (JasperReport) JRLoader.loadObject(getServletContext().getRealPath("WEB-INF/informes/informeTotalAlumnosArea.jasper"));
						Map<String, Object> parametros = new HashMap<String, Object>();
						if(estado.equals("activa"))
						{
							parametros.put("estado", "1. Total de Alumnos por Area según Gestión Elegida con Práctica Vigente");
						}
						if(estado.equals("inactiva"))
						{
							parametros.put("estado", "1. Histórico Total de Alumnos por Area según Gestión Elegida con Práctica Terminada");
						}
						
						JasperPrint jasperPrint = JasperFillManager.fillReport(reporte, parametros, new JRBeanCollectionDataSource(areas));
					    JRExporter exporter = new JRPdfExporter();
					    exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
					    exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, out);
					    exporter.exportReport();
					}
					
				} catch (JRException e)
				{
					e.printStackTrace();
				}
		   }
		   if(tipo_informe.equals("8"))
		   {
				try 
				{
					String accion = request.getParameter("accion");
					if (accion == null) {
					    System.out.println("No fue seleccionado nada");
					} else if (accion.equals("listado")) {
						int asi_codigo = Integer.parseInt(request.getParameter("practica"));
						response.setHeader("Content-Disposition", "attachment; filename=\"informeListadoAlumnosAcademico.pdf\";");
					    response.setHeader("Cache-Control", "no-cache");
					    response.setHeader("Pragma", "no-cache");
					    response.setDateHeader("Expires", 0);
						response.setContentType("application/pdf");
						ServletOutputStream out = response.getOutputStream();
						List<Practica> practicas = new ArrayList<Practica>();
						practicas = new Conexion().getListadoAlumnosAcademicoPracticas(asi_codigo); 
						@SuppressWarnings("deprecation")
						JasperReport reporte = (JasperReport) JRLoader.loadObject(getServletContext().getRealPath("WEB-INF/informes/informeListadoAlumnosAcademico.jasper"));
						Map<String, Object> parametros = new HashMap<String, Object>();
						parametros.put("estado", "1. Listado de Alumnos por Académico según Gestión Elegida con Práctica Vigente");
						JasperPrint jasperPrint = JasperFillManager.fillReport(reporte, parametros, new JRBeanCollectionDataSource(practicas));
					    JRExporter exporter = new JRPdfExporter();
					    exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
					    exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, out);
					    exporter.exportReport();
					} else if (accion.equals("total")) {
						int asi_codigo = Integer.parseInt(request.getParameter("practica"));
						response.setHeader("Content-Disposition", "attachment; filename=\"informeTotalAlumnosAcademico.pdf\";");
					    response.setHeader("Cache-Control", "no-cache");
					    response.setHeader("Pragma", "no-cache");
					    response.setDateHeader("Expires", 0);
						response.setContentType("application/pdf");
						ServletOutputStream out = response.getOutputStream();
						List<Academico> academicos = new ArrayList<Academico>();
						academicos = new Conexion().getTotalAcademicoPracticas(asi_codigo); 
						@SuppressWarnings("deprecation")
						JasperReport reporte = (JasperReport) JRLoader.loadObject(getServletContext().getRealPath("WEB-INF/informes/informeTotalAlumnosAcademico.jasper"));
						Map<String, Object> parametros = new HashMap<String, Object>();
						parametros.put("estado", "1. Total de Alumnos por Académico según Gestión Elegida con Práctica Vigente");
						JasperPrint jasperPrint = JasperFillManager.fillReport(reporte, parametros, new JRBeanCollectionDataSource(academicos));
					    JRExporter exporter = new JRPdfExporter();
					    exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
					    exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, out);
					    exporter.exportReport();
					}
					
				} catch (JRException e)
				{
					e.printStackTrace();
				}
		   }
		   if(tipo_informe.equals("9"))
		   {
				try 
				{
					String accion = request.getParameter("accion");
					if (accion == null) {
					    System.out.println("No fue seleccionado nada");
					} else if (accion.equals("listado")) {
						int asi_codigo = Integer.parseInt(request.getParameter("practica"));
						int agnio = Integer.parseInt(request.getParameter("agnio"));
						int semestre = Integer.parseInt(request.getParameter("semestre"));
						response.setHeader("Content-Disposition", "attachment; filename=\"informeListadoAlumnosNotas.pdf\";");
					    response.setHeader("Cache-Control", "no-cache");
					    response.setHeader("Pragma", "no-cache");
					    response.setDateHeader("Expires", 0);
						response.setContentType("application/pdf");
						ServletOutputStream out = response.getOutputStream();
						List<Practica> practicas = new ArrayList<Practica>();
						practicas = new Conexion().getListadoAlumnosNotasPracticas(agnio,semestre,asi_codigo); 
						@SuppressWarnings("deprecation")
						JasperReport reporte = (JasperReport) JRLoader.loadObject(getServletContext().getRealPath("WEB-INF/informes/informeListadoAlumnosNotas.jasper"));
						Map<String, Object> parametros = new HashMap<String, Object>();
						parametros.put("estado", "1. Información de Notas de Alumnos según Gestión Elegida ");
						parametros.put("estado2", "2. Listado de Alumnos según Gestión Elegida ");
						parametros.put("promedio", new Conexion().getInfoNotas(1,agnio,semestre,asi_codigo));
						parametros.put("maximo", new Conexion().getInfoNotas(2,agnio,semestre,asi_codigo));
						parametros.put("minimo", new Conexion().getInfoNotas(3,agnio,semestre,asi_codigo));
						parametros.put("agnio", String.valueOf(agnio));
						parametros.put("semestre", String.valueOf(semestre));
						JasperPrint jasperPrint = JasperFillManager.fillReport(reporte, parametros, new JRBeanCollectionDataSource(practicas));
					    JRExporter exporter = new JRPdfExporter();
					    exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
					    exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, out);
					    exporter.exportReport();
					}
				} catch (JRException e)
				{
					e.printStackTrace();
				}
		   }
		   
	   }
	}
	
}
