package com.icc.controlador;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.icc.modelo.Bitacora;
import com.icc.modelo.Practica;
import com.icc.utilidades.Conexion;

/**
 * Servlet implementation class BitacorasAlumnoControlador
 */
@WebServlet("/BitacorasAlumnoControlador")
public class BitacorasAlumnoControlador extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public BitacorasAlumnoControlador() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String pra_id =  request.getParameter("id");
		String tipo = (String) request.getSession().getAttribute("tipo");
		//se cargan todas las bitacoras del alumno correspondiente a la gestión realizada
		if(tipo=="alumno")
		{
			try {	
				response.setHeader("Cache-control", "no-cache, no-store, must-revalidate");
				ArrayList<Bitacora> bitacoras = new Conexion().getBitacorasAlumno(Integer.parseInt(pra_id));
				request.setAttribute("bitacoraList",bitacoras);
				request.setAttribute("practica", new Conexion().getPractica(Integer.parseInt(pra_id)));
				request.getRequestDispatcher("vistas_alumno/bitacoras_alumno.jsp").forward(request, response);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		//se cargan todas las bitacoras del alumno a cargo correspondiente a la gestión realizada 
		if(tipo=="supervisor")
		{
			try {	
				response.setHeader("Cache-control", "no-cache, no-store, must-revalidate");
				request.setAttribute("bitacoraList",new Conexion().getBitacorasAlumno(Integer.parseInt(pra_id)));
				Practica practica = new Conexion().getPractica(Integer.parseInt(pra_id));
				request.setAttribute("practica", practica);
				request.setAttribute("alumno", new Conexion().getDatosAlumno(practica.getAlu_rut()));
				request.getRequestDispatcher("vistas_supervisor/bitacoras_alumno.jsp").forward(request, response);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		//se cargan todas las bitacoras del alumno a cargo correspondiente a la gestión realizada
		if(tipo=="academico")
		{
			try {	
				response.setHeader("Cache-control", "no-cache, no-store, must-revalidate");
				request.setAttribute("bitacoraList",new Conexion().getBitacorasAlumno(Integer.parseInt(pra_id)));
				Practica practica = new Conexion().getPractica(Integer.parseInt(pra_id));
				request.setAttribute("practica", practica);
				request.setAttribute("alumno", new Conexion().getDatosAlumno(practica.getAlu_rut()));
				request.getRequestDispatcher("vistas_academico/bitacoras_alumno.jsp").forward(request, response);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String pra_id =  request.getParameter("id");
		String tipo = (String) request.getSession().getAttribute("tipo");
		if(tipo=="alumno")
		{
			try {	
				response.setHeader("Cache-control", "no-cache, no-store, must-revalidate");
				ArrayList<Bitacora> bitacoras = new Conexion().getBitacorasAlumno(Integer.parseInt(pra_id));
				request.setAttribute("bitacoraList",bitacoras);
				request.setAttribute("practica", new Conexion().getPractica(Integer.parseInt(pra_id)));
				request.getRequestDispatcher("vistas_alumno/bitacoras_alumno.jsp").forward(request, response);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if(tipo=="academico" || tipo=="administrador-academico")
		{
			try {	
				response.setHeader("Cache-control", "no-cache, no-store, must-revalidate");
				request.setAttribute("bitacoraList",new Conexion().getBitacorasAlumno(Integer.parseInt(pra_id)));
				Practica practica = new Conexion().getPractica(Integer.parseInt(pra_id));
				request.setAttribute("practica", practica);
				request.setAttribute("alumno", new Conexion().getDatosAlumno(practica.getAlu_rut()));
				request.getRequestDispatcher("vistas_academico/bitacoras_alumno.jsp").forward(request, response);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if(tipo=="supervisor")
		{
			try {	
				response.setHeader("Cache-control", "no-cache, no-store, must-revalidate");
				request.setAttribute("bitacoraList",new Conexion().getBitacorasAlumno(Integer.parseInt(pra_id)));
				Practica practica = new Conexion().getPractica(Integer.parseInt(pra_id));
				request.setAttribute("practica", practica);
				request.setAttribute("alumno", new Conexion().getDatosAlumno(practica.getAlu_rut()));
				request.getRequestDispatcher("vistas_supervisor/bitacoras_alumno.jsp").forward(request, response);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
