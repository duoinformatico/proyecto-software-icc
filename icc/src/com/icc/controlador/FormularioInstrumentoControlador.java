package com.icc.controlador;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.icc.modelo.Escala;
import com.icc.modelo.Instrumento;
import com.icc.modelo.Item;
import com.icc.modelo.Opcion;
import com.icc.utilidades.Conexion;

/**
 * Servlet implementation class FormularioInstrumentoControlador
 */
@WebServlet("/FormularioInstrumentoControlador")
public class FormularioInstrumentoControlador extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FormularioInstrumentoControlador() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		int etapa = Integer.parseInt(request.getParameter("etapa"));
		
		if(etapa == 1){//recolectar datos de configuracion de creacion de instrumento nuevo
			//escala, carrera, asignatura, rol_evaluador, nombre_instrumento, cantidad de items
			request.setAttribute("escala", Integer.parseInt(request.getParameter("escala")));
			request.setAttribute("carrera", Integer.parseInt(request.getParameter("carrera")));
			request.setAttribute("asignatura", request.getParameter("asignatura"));
			request.setAttribute("rol", Integer.parseInt(request.getParameter("rol")));
			request.setAttribute("n_items", Integer.parseInt(request.getParameter("items")));
			request.setAttribute("nombre_instrumento", request.getParameter("nombre_instrumento"));
			
			request.setAttribute("escala", new Conexion().getEscalaByID(Integer.parseInt(request.getParameter("escala"))));
			request.setAttribute("categoriasList", new Conexion().getCategorias());
			
			request.getRequestDispatcher("vistas_admin/formulario_rubricas.jsp").forward(request, response);

		}
		if(etapa == 2){//recolectar datos para registrar nuevo instrumento en la BD
			Escala escala = new Conexion().getEscalaByID(Integer.parseInt(request.getParameter("esc_id")));
			Instrumento instrumento = new Instrumento(Integer.parseInt(request.getParameter("esc_id")), Integer.parseInt(request.getParameter("rol_id")), request.getParameter("nombre_instrumento"), Integer.parseInt(request.getParameter("n_items")), Double.parseDouble(request.getParameter("max_puntaje")), request.getParameter("descripcion"));
			int respuesta = new Conexion().agregarInstrumento(instrumento);
			
			//obtener el id del nuevo instrumento
			int ins_id = new Conexion().getLastInstrumentoID();
			
			if(respuesta == 1 && ins_id != 0){//si se ha insertado el instrumento agregar los items con las opciones
				//recorrer y recolectar los items del formulario con sus categorias
				Item item;
				for (int i = 1; i <= Integer.parseInt(request.getParameter("n_items")); i++) {
					item = new Item(9999, ins_id, Integer.parseInt(request.getParameter("esc_id")), Integer.parseInt(request.getParameter("categoria_"+i)), request.getParameter("item_"+i));
					respuesta = new Conexion().agregarItem(item);
					//obtener el id del ultimo item insertado
					int item_id = new Conexion().getLastItemID();
					if(respuesta == 1 && item_id != 0){//si pudo agregar el item agregar las opciones
						//recorrer y recolectar los items del formulario con sus categorias
						Opcion opcion;
						for (int j = 1; j <= Integer.parseInt(request.getParameter("n_opciones")); j++) {
							opcion = new Opcion(9999, item_id, request.getParameter("opc_descripcion_"+i+"_"+j), escala.getValores().get(j - 1).getVal_valor());
							respuesta = new Conexion().agregarOpcion(opcion);
							if(respuesta == 2){//break loop i y j y el if
								i = 99999;
								j = 99999;
							}
						}
					}
					else{//break loop i y el if
						i = 99999;
					}
				}
				
				//si existe un instrumento antiguo con el mismo rol actualizar fecha_fin del instrumento antiguo + estado del instrumento antiguo a inactivo
				//un procedimiento almacenado en la base de datos realiza esta operación
			}
			//si no ha podido insertar
			if(respuesta == 2){
				//abortar nuevo instrumento + mensaje no se ha podido ingresar el nuevo instrumento
			
			}
			
			//redireccionar a la vista de menú de rúbricas	
			
			request.getRequestDispatcher("MenuRubricaControlador").forward(request, response);
		}
	}

}
