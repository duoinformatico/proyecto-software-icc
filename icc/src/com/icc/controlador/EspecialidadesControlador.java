package com.icc.controlador;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.icc.utilidades.Conexion;




/**
 * Servlet implementation class EspecialidadesControlador
 */
@WebServlet("/EspecialidadesControlador")
public class EspecialidadesControlador extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EspecialidadesControlador() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(request.getParameter("aca_rut_boton") != null){
			request.setAttribute("aca_rut",request.getParameter("aca_rut_boton"));
			request.getRequestDispatcher("ajax/boton_especialidad.jsp").forward(request, response);
		}
		if(request.getParameter("aca_rut") != null){
			String rut = request.getParameter("aca_rut");
			request.setAttribute("especialidadList",new Conexion().getEspecilidades(rut));
			request.getRequestDispatcher("ajax/especialidades.jsp").forward(request, response);
		}
	}
}
