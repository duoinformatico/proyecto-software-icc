package com.icc.controlador;
import com.icc.modelo.Empresa;
import com.icc.utilidades.Conexion;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class FormularioEmpresaControlador
 */
@WebServlet("/FormularioEmpresaControlador")
public class FormularioEmpresaControlador extends HttpServlet {
	private static final long serialVersionUID = 1L;
	String mensaje="";
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FormularioEmpresaControlador() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("empresaList",new Conexion().getEmpresas() );
		request.getRequestDispatcher("vistas_admin/empresas.jsp").forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		String rut = request.getParameter("rut");
	    String comuna = request.getParameter("comuna");
		String nombre = request.getParameter("nombre");
		String direccion = request.getParameter("direccion");
		String telefono = request.getParameter("telefono");
		String celular = request.getParameter("celular");
		String descripcion = request.getParameter("descripcion");
		String email = request.getParameter("email");
		String web= request.getParameter("web");
		int exito = 0;
		try {	
			
			int i = new Conexion().AgregarEmpresa(new Empresa(rut,Integer.parseInt(comuna),nombre,direccion,telefono,celular,descripcion,email,web));
			if(i == 1)
			{
				mensaje = "La empresa se registró correctamente";
				request.setAttribute("mensaje", mensaje);
				exito = 1;
				request.setAttribute("exito", exito);
				request.setAttribute("empresaList",new Conexion().getEmpresas());
				request.getRequestDispatcher("vistas_admin/empresas.jsp").forward(request, response);
			}
			else{
				mensaje = "Hubo un error al registrar la empresa ";
				request.setAttribute("mensaje", mensaje);
				exito = 0;
				request.setAttribute("exito", exito);
				request.setAttribute("empresaList",new Conexion().getEmpresas());
				request.getRequestDispatcher("vistas_admin/empresas.jsp").forward(request, response);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}