package com.icc.controlador;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.icc.modelo.Alumno;
import com.icc.modelo.EvaluacionAcademico;
import com.icc.modelo.EvaluacionSupervisor;
import com.icc.modelo.Instrumento;
import com.icc.modelo.Practica;
import com.icc.utilidades.Conexion;

/**
 * Servlet implementation class MenuEvaluacionControlador
 */
@WebServlet("/MenuEvaluacionControlador")
public class MenuEvaluacionControlador extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MenuEvaluacionControlador() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("doGet MenuEvaluacionControlador");
		String tipo = (String) request.getSession().getAttribute("tipo");
		String rut = (String) request.getSession().getAttribute("rut");
		ArrayList<EvaluacionSupervisor> evaluaciones_supervisor = new ArrayList<>();
		ArrayList<EvaluacionAcademico> evaluaciones_academico = new ArrayList<>();
		ArrayList<Practica> practicas = new ArrayList<>();
		ArrayList<Alumno> alumnos = new ArrayList<>();
		ArrayList<Instrumento> instrumentos = new ArrayList<>();
		if(tipo == "admin"){
			
		}
		if(tipo == "alumno"){
			
		}
		if(tipo == "supervisor"){
			evaluaciones_supervisor = new Conexion().getEvaluadoresSupervisor(rut);
			for(int i=0; i< evaluaciones_supervisor.size(); i++) {
	            practicas.add(new Conexion().getPractica(evaluaciones_supervisor.get(i).getPra_id()));
	            instrumentos.add(new Conexion().getInstrumento(evaluaciones_supervisor.get(i).getRol_id()));
	        }
			for(int i=0; i< practicas.size(); i++) {
	            alumnos.add(new Conexion().getDatosAlumno(practicas.get(i).getAlu_rut()));
	        }
			
			request.setAttribute("evaluacionesList", evaluaciones_supervisor);
			request.setAttribute("practicasList", practicas);
			request.setAttribute("alumnosList", alumnos);
			request.setAttribute("instrumentosList", instrumentos);
			request.setAttribute("asignaturasList", new Conexion().getGestiones() );
			request.setAttribute("empresasList", new Conexion().getEmpresas());
			request.getRequestDispatcher("/vistas_supervisor/alumnos_asignados.jsp").forward(request, response);
			
		}
		if(tipo == "academico" || tipo == "administrador-academico"){
			evaluaciones_academico = new Conexion().getEvaluadoresAcademico(rut);
			for(int i=0; i< evaluaciones_academico.size(); i++) {
	            practicas.add(new Conexion().getPractica(evaluaciones_academico.get(i).getPra_id()));
	            instrumentos.add(new Conexion().getInstrumento(evaluaciones_academico.get(i).getRol_id()));
	        }
			for(int i=0; i< practicas.size(); i++) {
	            alumnos.add(new Conexion().getDatosAlumno(practicas.get(i).getAlu_rut()));
	        }
			
			request.setAttribute("evaluacionesList", evaluaciones_academico);
			request.setAttribute("practicasList", practicas);
			request.setAttribute("alumnosList", alumnos);
			request.setAttribute("instrumentosList", instrumentos);
			request.setAttribute("asignaturasList", new Conexion().getGestiones() );
			request.setAttribute("empresasList", new Conexion().getEmpresas());
			request.getRequestDispatcher("/vistas_academico/alumnos_asignados.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("doPost MenuEvaluacionControlador");
		String tipo = (String) request.getSession().getAttribute("tipo");
		String rut = (String) request.getSession().getAttribute("rut");
		//ArrayList<EvaluacionAlumno> evaluaciones_alumno = new ArrayList<>();
		ArrayList<EvaluacionSupervisor> evaluaciones_supervisor = new ArrayList<>();
		ArrayList<EvaluacionAcademico> evaluaciones_academico = new ArrayList<>();
		ArrayList<Practica> practicas = new ArrayList<>();
		ArrayList<Alumno> alumnos = new ArrayList<>();
		ArrayList<Instrumento> instrumentos = new ArrayList<>();
		if(tipo == "admin"){
			
		}
		if(tipo == "alumnos"){
			
		}
		if(tipo == "supervisor"){
			evaluaciones_supervisor = new Conexion().getEvaluadoresSupervisor(rut);
			for(int i=0; i< evaluaciones_supervisor.size(); i++) {
	            practicas.add(new Conexion().getPractica(evaluaciones_supervisor.get(i).getPra_id()));
	            instrumentos.add(new Conexion().getInstrumento(evaluaciones_supervisor.get(i).getRol_id()));
	        }
			for(int i=0; i< practicas.size(); i++) {
	            alumnos.add(new Conexion().getDatosAlumno(practicas.get(i).getAlu_rut()));
	        }
			
			request.setAttribute("evaluacionesList", evaluaciones_supervisor);
			request.setAttribute("practicasList", practicas);
			request.setAttribute("alumnosList", alumnos);
			request.setAttribute("instrumentosList", instrumentos);
			request.setAttribute("asignaturasList", new Conexion().getGestiones() );
			request.setAttribute("empresasList", new Conexion().getEmpresas());
			request.getRequestDispatcher("/vistas_supervisor/alumnos_asignados.jsp").forward(request, response);
			
		}
		if(tipo == "academico" || tipo == "administrador-academico"){
			evaluaciones_academico = new Conexion().getEvaluadoresAcademico(rut);
			for(int i=0; i< evaluaciones_academico.size(); i++) {
	            practicas.add(new Conexion().getPractica(evaluaciones_academico.get(i).getPra_id()));
	            instrumentos.add(new Conexion().getInstrumento(evaluaciones_academico.get(i).getRol_id()));
	        }
			for(int i=0; i< practicas.size(); i++) {
	            alumnos.add(new Conexion().getDatosAlumno(practicas.get(i).getAlu_rut()));
	        }
			
			request.setAttribute("evaluacionesList", evaluaciones_academico);
			request.setAttribute("practicasList", practicas);
			request.setAttribute("alumnosList", alumnos);
			request.setAttribute("instrumentosList", instrumentos);
			request.setAttribute("asignaturasList", new Conexion().getGestiones() );
			request.setAttribute("empresasList", new Conexion().getEmpresas());
			request.getRequestDispatcher("/vistas_academico/alumnos_asignados.jsp").forward(request, response);
		}
	}

}
