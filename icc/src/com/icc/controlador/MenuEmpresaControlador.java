package com.icc.controlador;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.icc.utilidades.Conexion;

/**
 * Servlet implementation class MenuEmpresaControlador
 */
@WebServlet("/MenuEmpresaControlador")
public class MenuEmpresaControlador extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MenuEmpresaControlador() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("comunaList", new Conexion().getComunas());
		request.setAttribute("regionList", new Conexion().getRegiones());
		request.getRequestDispatcher("ajax/formulario_empresa.jsp").forward(request, response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		request.setAttribute("empresaList",new Conexion().getEmpresas() );
		request.getRequestDispatcher("/vistas_admin/empresas.jsp").forward(request, response);
	}

}
