package com.icc.controlador;

import java.io.IOException;




import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;










import com.icc.modelo.Academico;
import com.icc.modelo.Especialidad;
import com.icc.utilidades.Conexion;
import com.icc.utilidades.EmailUtilidades;
import com.icc.utilidades.PasswordGenerador;

/**
 * Servlet implementation class FormularioAcademicoControlador
 */
@WebServlet("/FormularioAcademicoControlador")
public class FormularioAcademicoControlador extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private String host;
	private String port;
	private String user;
	private String pass; 
    String mensaje="";
    int exito = 0;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FormularioAcademicoControlador() {
        super();
    }
    
    public void init() {
		ServletContext context = getServletContext();
		host = context.getInitParameter("host");
		port = context.getInitParameter("port");
		user = context.getInitParameter("user");
		pass = context.getInitParameter("pass");
	}
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("academicosList",new Conexion().getAcademicos());
		request.getRequestDispatcher("/vistas_admin/academicos.jsp").forward(request, response);
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException  {
		String rut = request.getParameter("rut");
		String dep_id = request.getParameter("departamento");
		String password = PasswordGenerador.getPassword(
				  PasswordGenerador.MINUSCULAS+
				  PasswordGenerador.MAYUSCULAS+
				  PasswordGenerador.NUMEROS,10);
		String nombres = request.getParameter("nombre");
		String paterno = request.getParameter("paterno");
		String materno = request.getParameter("materno");
		String email = request.getParameter("email");
		String telefono = request.getParameter("telefono");
		String celular = request.getParameter("celular");
		String estado = request.getParameter("estado");
		//int actual_especialidad = Integer.parseInt(request.getParameter("actual_especialidades"));
		String[] codigos_especialidades = request.getParameterValues("especialidad");//recibe el select multiple
		ArrayList<Especialidad> especialidades = new ArrayList<Especialidad>();
		String enunciado = "";
		int ban1=1;
		int ban2=1;
		int ban3=1;
		if(new Conexion().verificar_usuario(rut, "Supervisor",0) == 1)
		{
			try {
				password = new Conexion().getClave(rut, "Supervisor");
			} catch (SQLException e) {
				e.printStackTrace();
			}
			ban1=0;
		}
		if(new Conexion().verificar_usuario(rut, "Alumno",0) == 1)
		{
			try {
				password = new Conexion().getClave(rut, "Alumno");
			} catch (SQLException e) {
				e.printStackTrace();
			}
			ban2=0;
		}
		if(new Conexion().verificar_usuario(rut, "Administrador",0) == 1 )
		{
			try {
				password = new Conexion().getClave(rut, "Administrador");
			} catch (SQLException e) {
				e.printStackTrace();
			}
			ban3=0;
		}
		try 
		{
			Academico aca = new Academico(rut,Integer.parseInt(dep_id),password,nombres,paterno,materno,email,telefono,celular,estado);
			if(ban1==0 || ban2==0 || ban3==0)
			{
				enunciado = "Buen día "+nombres+" "+paterno+" "+materno+" se ha agregado a su cuenta el perfil de tipo Académico "
						+ "en el Sistema de Control y Seguimiento de Prácticas Profesionales y Operativas "
						+ "de la Escuela de Ingeniería en Construcción de la Universidad del Bío-Bío,  para volver a hacer uso del sistema recuerde ingresar al siguiente enlace http://146.83.193.68:8080/icc/ y auntentifiquese "
						+ "con su rut: "+rut+" y su antigua clave de acceso (Fue entregada al registrarlo en el sistema en el perfil anterior). Esperamos que tenga una buena semana. "
						+ "consultas a este mismo correo, atentamente el administrador ";
			}else{
				enunciado = "Bienvenido "+nombres+" "+paterno+" "+materno+" al Sistema de Control y seguimiento de prácticas Profesionales y Operativas "
						+ "de la Escuela de Ingeniería en Construcción de la Universidad del Bío-Bío, su perfil actual es de tipo Académico y para hacer uso del sistema ingrese al siguiente enlace http://146.83.193.68:8080/icc/ y auntentifiquese "
						+ "con su rut: "+rut+" y su clave de acceso: "+password+". \n Una vez logueado le aconsejamos cambiar su clave de aceso, "
						+ "esperamos que tenga una buena semana. "
						+ "consultas a este mismo correo, atentamente el administrador.";
			}
			String asunto ="Bienvenido";
			request.setAttribute("asunto", asunto);
			request.setAttribute("email",email);
			request.setAttribute("enunciado", enunciado);

			if(new Conexion().AgregarAcademico(aca) == 1)
			{	if(codigos_especialidades != null){
					for(int i=0;i < codigos_especialidades.length;i++) 
					{
						int codigo = Integer.parseInt(codigos_especialidades[i]);
						especialidades.add(new Especialidad(codigo));
					}
					for(int i=0;i<especialidades.size();i++)
					{
						int id =especialidades.get(i).getEsp_id();
						new Conexion().agregarEspecialidadAcademico(id,aca.getAca_rut());
					}
				}				
				
				try {
					EmailUtilidades.sendEmail(host, port, user, pass, email, asunto,
							enunciado);
					
					// se cambian los estados de los perfiles, esto solo pasará muy rara vez , una vez avanzado el uso del sw
					if(ban1==0)
					{
						new Conexion().ModificarEstadoUsuario(rut,"inactivo","Supervisor");
					}
					if(ban2==0)
					{
						new Conexion().ModificarEstadoUsuario(rut,"inactivo","Alumno");
					}
					mensaje = "El Académico fue agregado al sistema exitosamente "
					+ "y se le envío un correo con su clave de acceso.";
					exito = 1;
				} catch (Exception ex) {
					ex.printStackTrace();
					new Conexion().eliminarAcademico(rut);// al eliminar el academico se borran las tuplas de la tabla especialidad_academico
					mensaje = "Hubo un error al enviar: " + ex.getMessage();
					exito = 0;
				} finally {	
					request.setAttribute("exito", exito);
					request.setAttribute("mensaje", mensaje);
					request.setAttribute("academicosList",new Conexion().getAcademicos());
					request.getRequestDispatcher("/vistas_admin/academicos.jsp").forward(request, response);
				}
			}
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

}
