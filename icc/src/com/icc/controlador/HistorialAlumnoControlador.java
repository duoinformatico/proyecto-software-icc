package com.icc.controlador;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.icc.modelo.Alumno;
import com.icc.modelo.EvaluacionAlumno;
import com.icc.modelo.Instrumento;
import com.icc.modelo.Practica;
import com.icc.utilidades.Conexion;

/**
 * Servlet implementation class HistorialAlumnoControlador
 */
@WebServlet("/HistorialAlumno")
public class HistorialAlumnoControlador extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HistorialAlumnoControlador() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String rut = (String) request.getSession().getAttribute("rut");
		String tipo = (String) request.getSession().getAttribute("tipo");
		ArrayList<EvaluacionAlumno> evaluaciones_alumno = new ArrayList<>();
		ArrayList<Practica> practicas = new ArrayList<>();
		ArrayList<Practica> estados_informe = new ArrayList<Practica>();
		ArrayList<Alumno> alumnos = new ArrayList<>();
		ArrayList<Instrumento> instrumentos = new ArrayList<>();
		
		if(tipo == "alumno"){
			evaluaciones_alumno = new Conexion().getEvaluadoresAlumno(rut);//obtiene todas las autoevaluaciones sin evaluar del alumno que se ha autentificado
			for(int i=0; i< evaluaciones_alumno.size(); i++) {
	            practicas.add(new Conexion().getPractica(evaluaciones_alumno.get(i).getPra_id()));
	            // se agregan los estados de los informes de las practicas
	            Practica temp = new Practica(evaluaciones_alumno.get(i).getPra_id(),new Conexion().getEstadoInforme(evaluaciones_alumno.get(i).getPra_id()));
				estados_informe.add(temp);
				System.out.println("Bean Estado"+estados_informe.get(i).getBean_uno());
				instrumentos.add(new Conexion().getInstrumento(evaluaciones_alumno.get(i).getRol_id()));
		        
	        }
			
			request.setAttribute("evaluacionesList", evaluaciones_alumno);
			request.setAttribute("practicasList", practicas);
			request.setAttribute("alumnosList", alumnos);
			request.setAttribute("estadosList", estados_informe);
			request.setAttribute("instrumentosList", instrumentos);
			request.setAttribute("asignaturasList", new Conexion().getGestiones() );
			request.setAttribute("empresasList", new Conexion().getEmpresas());
			//request.setAttribute("evaluadoresList", new Conexion().getEvaluadoresAlumno(rut));		
		}
		//temporal
		request.setAttribute("practicaList", new Conexion().getHistorial(rut));
		response.setHeader("Cache-control", "no-cache, no-store, must-revalidate");
		request.getRequestDispatcher("/vistas_alumno/historial.jsp").forward(request, response);
	}

}
