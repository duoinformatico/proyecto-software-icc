package com.icc.controlador;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.icc.modelo.Academico;
import com.icc.modelo.Administrador;
import com.icc.modelo.Alumno;
import com.icc.modelo.Asignatura;
import com.icc.modelo.Bitacora;
import com.icc.modelo.CursaPlanEstudio;
import com.icc.modelo.Empresa;
import com.icc.modelo.EvaluacionAcademico;
import com.icc.modelo.EvaluacionAlumno;
import com.icc.modelo.EvaluacionSupervisor;
import com.icc.modelo.InscripcionAsignatura;
import com.icc.modelo.Instrumento;
import com.icc.modelo.Plan_estudio;
import com.icc.modelo.Practica;
import com.icc.modelo.RolGestion;
import com.icc.modelo.Supervisor;
import com.icc.utilidades.Conexion;

/**
 * Servlet implementation class AjaxControlador
 */
@WebServlet("/AjaxControlador")
public class AjaxControlador extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AjaxControlador() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		if(request.getParameter("empresas_alumnos") != null){
			request.setAttribute("empresaList", new Conexion().getEmpresas());
			request.getRequestDispatcher("ajax/lista_empresas.jsp").forward(request, response);
		}	
		
		if(request.getParameter("region") != null){
			System.out.println("\ndoGET AjaxControlador\n\nParametro Get: region="+request.getParameter("region"));
			request.setAttribute("provinciaList", new Conexion().getProvincias(request.getParameter("region")));
			request.getRequestDispatcher("ajax/select_provincias.jsp").forward(request, response);
		}		
		
		if(request.getParameter("provincia") != null){
			System.out.println("\ndoGET AjaxControlador\n\nParametro Get: provincia="+request.getParameter("provincia"));
			request.setAttribute("comunaList", new Conexion().getComunas(request.getParameter("provincia")));
			request.getRequestDispatcher("ajax/select_comunas.jsp").forward(request, response);
		}
		if(request.getParameter("region_modif") != null){
			System.out.println("\ndoGET AjaxControlador\n\nParametro Get: region="+request.getParameter("region_modif"));
			request.setAttribute("provinciaList", new Conexion().getProvincias(request.getParameter("region_modif")));
			request.getRequestDispatcher("ajax/select_provincias_modif.jsp").forward(request, response);
		}		
		
		if(request.getParameter("provincia_modif") != null){
			System.out.println("\ndoGET AjaxControlador\n\nParametro Get: provincia="+request.getParameter("provincia_modif"));
			request.setAttribute("comunaList", new Conexion().getComunas(request.getParameter("provincia_modif")));
			request.getRequestDispatcher("ajax/select_comunas_modif.jsp").forward(request, response);
		}
		
		if(request.getParameter("region_2") != null){
			System.out.println("\ndoGET AjaxControlador\n\nParametro Get: region="+request.getParameter("region_2"));
			request.setAttribute("provinciaList", new Conexion().getProvincias(request.getParameter("region_2")));
			request.getRequestDispatcher("ajax/select_2.jsp").forward(request, response);
		}		
		
		if(request.getParameter("provincia_2") != null){
			System.out.println("\ndoGET AjaxControlador\n\nParametro Get: provincia="+request.getParameter("provincia_2"));
			request.setAttribute("comunaList", new Conexion().getComunas(request.getParameter("provincia_2")));
			request.getRequestDispatcher("ajax/select_comunas_2.jsp").forward(request, response);
		}
		
		if(request.getParameter("empresa") != null){
			System.out.println("\ndoGET AjaxControlador\n\nParametro Get: empresa="+request.getParameter("empresa"));
			request.setAttribute("supervisorList", new Conexion().getSupervisores(request.getParameter("empresa")));
			request.getRequestDispatcher("ajax/select_supervisores.jsp").forward(request, response);
		}
		if(request.getParameter("giro") != null){
			System.out.println("\ndoGET AjaxControlador\n\nParametro Get: giro="+request.getParameter("giro")+" num "+request.getParameter("num_giro"));
			request.setAttribute("rubrosList", new Conexion().getRubros());
			request.setAttribute("numero_rubro", request.getParameter("num_giro"));
			request.getRequestDispatcher("ajax/select_rubro.jsp").forward(request, response);
		}
		
		if(request.getParameter("carrera_rubrica") != null){//permite actualizar select de asignaturas en config crear rubrica
			System.out.println("\ndoGET AjaxControlador\n\nParametro Get: carrera_rubrica="+request.getParameter("carrera_rubrica"));
			request.setAttribute("asignaturasList", new Conexion().getAsignaturasTipoGestion(Integer.parseInt(request.getParameter("carrera_rubrica"))));
			request.getRequestDispatcher("ajax/select_asignatura.jsp").forward(request, response);
		}
		
		if(request.getParameter("asignatura_gestion") != null && request.getParameter("plan_gestion") != null){//permite actualizar select de roles en config crear rubrica
			System.out.println("\ndoGET AjaxControlador\n\nParametro Get: asignatura_gestion="+request.getParameter("asignatura_gestion"));
			request.setAttribute("rolesList", new Conexion().getRoles(Integer.parseInt(request.getParameter("asignatura_gestion")),Integer.parseInt(request.getParameter("plan_gestion"))));
			request.getRequestDispatcher("ajax/select_roles.jsp").forward(request, response);
		}
		
		if(request.getParameter("rut_asignaturas_registradas") != null){//permite consultar y mostrar todas las asignaturas registradas por un alumno en el sistema
			System.out.println("\ndoGET AjaxControlador\n\nParametro Get: rut_asignaturas_registradas = "+request.getParameter("rut_asignaturas_registradas"));
			request.setAttribute("InscripcionesList", new Conexion().getInscripciones(request.getParameter("rut_asignaturas_registradas")));
			request.getRequestDispatcher("ajax/inscripciones_registradas.jsp").forward(request, response);
		}
		
		if(request.getParameter("ins_id") != null && request.getParameter("rut_evaluador") != null && request.getParameter("practica_id") != null && request.getParameter("rol_id") != null){
			System.out.println("\ndoGET AjaxControlador\nVer Rúbrica para Evaluación\nParametro Get: ins_id="+request.getParameter("ins_id"));
			request.setAttribute("instrumento", new Conexion().getInstrumento(request.getParameter("ins_id")));
			request.setAttribute("escala", new Conexion().getEscala(Integer.parseInt(request.getParameter("ins_id"))));
			request.setAttribute("categoriasList", new Conexion().getCategorias());
			request.setAttribute("rut", request.getParameter("rut_evaluador"));
			request.setAttribute("pra_id", request.getParameter("practica_id"));
			request.setAttribute("rol_id", request.getParameter("rol_id"));
			request.setAttribute("tipo", (String) request.getSession().getAttribute("tipo"));
			if(request.getSession().getAttribute("tipo").equals("alumno")){//si es alumno mostrar listado de areas para que seleccione donde trabajó
				request.setAttribute("areasPracticaList", new Conexion().getAreas());
				request.setAttribute("funcionesPracticaList", new Conexion().getFunciones());
			}
			else{//si no es alumno consultar por las areas que ha seleccionado
				//mostrar areas que el alumno seleccionó
				request.setAttribute("areasPracticaList", new Conexion().getAreas());
				request.setAttribute("areasAlumnoList", new Conexion().getAreasPractica(Integer.parseInt(request.getParameter("practica_id"))));
				request.setAttribute("funcionesPracticaList", new Conexion().getFunciones());
				request.setAttribute("funcionesAlumnoList", new Conexion().getFuncionesPractica(Integer.parseInt(request.getParameter("practica_id"))));
				
			}
			request.getRequestDispatcher("ajax/instrumento.jsp").forward(request, response);
		}
		
		if(request.getParameter("tipo_eval") != null && request.getParameter("evaluador_rut") != null && request.getParameter("id_practica") != null
		   && request.getParameter("id_rol") != null && request.getParameter("eva_fecha") != null){//redirecciona mostrando una rubrica de evaluacion con su detalle
			//tipo_eval, evaluador_rut, id_practica, id_rol, eva_fecha
			
			System.out.println("\ndoGET AjaxControlador\nVer Rúbrica con Detalle de Evaluación\nParametro Get: Tipo Evaluador: "+request.getParameter("tipo_eval")+" Rut: "+ request.getParameter("evaluador_rut")+" Practica: "+request.getParameter("id_practica")+" Rol: "+request.getParameter("id_rol")+" Fecha: "+request.getParameter("eva_fecha"));
			Instrumento instrumento =  new Conexion().getInstrumento(Integer.parseInt(request.getParameter("id_rol")), (String)request.getParameter("eva_fecha"));
			request.setAttribute("instrumento", instrumento);
			request.setAttribute("escala", new Conexion().getEscala(instrumento.getIns_id()));
			request.setAttribute("categoriasList", new Conexion().getCategorias());
			
			//identificacion del tipo de evaluador "alumno" "academico" "supervisor"
			if(request.getParameter("tipo_eval").equals("alumno")){
				EvaluacionAlumno evaluacion_alumno;
				evaluacion_alumno = new Conexion().getEvaluacionAlumnoID(request.getParameter("evaluador_rut"),Integer.parseInt(request.getParameter("id_practica")),request.getParameter("eva_fecha"),Integer.parseInt(request.getParameter("id_rol")));
				request.setAttribute("evaluacion", evaluacion_alumno);
				request.setAttribute("tipo", (String) request.getSession().getAttribute("tipo"));
				request.setAttribute("detalleEvaluacion", new Conexion().getDetalleEvaluacionAlumno(request.getParameter("evaluador_rut"), Integer.parseInt(request.getParameter("id_practica")), request.getParameter("eva_fecha"), Integer.parseInt(request.getParameter("id_rol"))));
				Alumno alumno = new Conexion().getDatosAlumno(evaluacion_alumno.getAlu_rut());
				request.setAttribute("evaluador",""+alumno.getAlu_nombres()+" "+alumno.getAlu_apellido_p()+" "+alumno.getAlu_apellido_m());
			}
			
			if(request.getParameter("tipo_eval").equals("supervisor")){
				EvaluacionSupervisor evaluacion_supervisor = new Conexion().getEvaluacionSupervisorID(request.getParameter("evaluador_rut"), Integer.parseInt(request.getParameter("id_practica")), request.getParameter("eva_fecha"), Integer.parseInt(request.getParameter("id_rol")));
				request.setAttribute("evaluacion", evaluacion_supervisor);
				request.setAttribute("tipo", (String) request.getSession().getAttribute("tipo"));
				request.setAttribute("detalleEvaluacion", new Conexion().getDetalleEvaluacionSupervisor(request.getParameter("evaluador_rut"), Integer.parseInt(request.getParameter("id_practica")), request.getParameter("eva_fecha"), Integer.parseInt(request.getParameter("id_rol"))));
				Supervisor supervisor = new Conexion().getSupervisor(evaluacion_supervisor.getSup_rut());
				request.setAttribute("evaluador",""+supervisor.getSup_nombres()+" "+supervisor.getSup_apellido_p()+" "+supervisor.getSup_apellido_m());
			}
			
			if(request.getParameter("tipo_eval").equals("academico")){
				EvaluacionAcademico evaluacion_academico = new Conexion().getEvaluacionAcademicoID(request.getParameter("evaluador_rut"), Integer.parseInt(request.getParameter("id_practica")), request.getParameter("eva_fecha"), Integer.parseInt(request.getParameter("id_rol")));
				request.setAttribute("evaluacion", evaluacion_academico);
				request.setAttribute("tipo", (String) request.getSession().getAttribute("tipo"));
				request.setAttribute("detalleEvaluacion", new Conexion().getDetalleEvaluacionAcademico(request.getParameter("evaluador_rut"), Integer.parseInt(request.getParameter("id_practica")), request.getParameter("eva_fecha"), Integer.parseInt(request.getParameter("id_rol"))));
				Academico academico = new Conexion().getAcademico(evaluacion_academico.getAca_rut());
				request.setAttribute("evaluador",""+academico.getAca_nombres()+" "+academico.getAca_apellido_p()+" "+academico.getAca_apellido_m());
			}
			//ver detalle de areas donde trabajó el alumno
			request.setAttribute("areasPracticaList", new Conexion().getAreas());
			request.setAttribute("areasAlumnoList", new Conexion().getAreasPractica(Integer.parseInt(request.getParameter("id_practica"))));
			request.setAttribute("funcionesPracticaList", new Conexion().getFunciones());
			request.setAttribute("funcionesAlumnoList", new Conexion().getFuncionesPractica(Integer.parseInt(request.getParameter("id_practica"))));
			
			request.getRequestDispatcher("ajax/evaluacion_detalle.jsp").forward(request, response);
		}
		
		if(request.getParameter("id_instrumento") != null && request.getParameter("tipo_evaluador") != null){//redirecciona para solo mostrar un instrumento de evaluacion
			System.out.println("\ndoGET AjaxControlador\nVer Rúbrica para Prueba\nParametro Get: ins_id ="+request.getParameter("id_instrumento"));
			request.setAttribute("instrumento", new Conexion().getInstrumento(request.getParameter("id_instrumento")));
			request.setAttribute("escala", new Conexion().getEscala(Integer.parseInt(request.getParameter("id_instrumento"))));
			request.setAttribute("categoriasList", new Conexion().getCategorias());
			if(request.getParameter("tipo_evaluador").equals("alumno")){//si es alumno mostrar listado de areas para que seleccione donde trabajó
				request.setAttribute("areasPracticaList", new Conexion().getAreas());
				request.setAttribute("funcionesPracticaList", new Conexion().getFunciones());
			}
			request.getRequestDispatcher("ajax/instrumento_lectura.jsp").forward(request, response);
		}
		
		
		if(request.getParameter("agnio") != null && request.getParameter("periodo") != null){//perimite realizar la carga de inscripciones de asignaturas desde la BD de la UBB
			if(request.getSession().getAttribute("tipo").equals("admin")  || request.getSession().getAttribute("tipo").equals("administrador-academico")){
				
				request.setAttribute("asignaturasList", new Conexion().getGestiones());
				String agnio = request.getParameter("agnio"); 
				String periodo = request.getParameter("periodo");
				ArrayList<InscripcionAsignatura> inscripciones = new Conexion().getInscripcionesUBB(agnio, periodo );
				java.sql.Connection conexion = new Conexion().ConexionBdRetorna();//utilizado para abrir una conexion que realiza multiples transacciones, la variable conexion debe pasarse como parametro a la funcion que realiza cambios en la BD
				int contador_null = 0;
				int contador_reprobado = 0;
				int contador_aprobado = 0;
				int contador_pendiente = 0;
				int existe = 0;//obtendrá un valor para saber si existe o no la inscripccion de asignatura en la BD local comparado con la que exite en la BD UBB
				int existe_alumno;//obtendrá un valor para saber si existe o no el Alumno en la BD local comparado con la que exite en la BD UBB
				ResultSet resultado = new Conexion().getResultado();
				
				for (int i = 0; i < inscripciones.size(); i++) {
					
					existe = new Conexion().getInscripcionID(conexion,inscripciones.get(i));
					
					existe_alumno = new Conexion().verificar_usuario_con_parametros(conexion,resultado,inscripciones.get(i).getAlu_rut(), "Alumno");
					int k =0;
					if(!(inscripciones.get(i).getIns_estado() == null)){//si el estado de la inscripcion es distinto de null o vacio
						
						if(inscripciones.get(i).getIns_estado().trim().equals("APROBADA")){
							contador_aprobado+=1;
							if(existe == 0 && existe_alumno == 1){//que no exista la inscripcion pero si exista el alumno y el plan de estudio, insertar
								
								try {
									k = new Conexion().agregarInscripcionAsignatura(conexion, inscripciones.get(i));
								} catch (SQLException e) {
									e.printStackTrace();
								}
								if(k == 1){
									System.out.println(""+(i+1)+" - RUT: "+inscripciones.get(i).getAlu_rut()+" - Inscripción registrada correctamente.");
								}
								else{
									System.out.println(""+(i+1)+" - RUT: "+inscripciones.get(i).getAlu_rut()+" - Inscripción no registrada ERROR.");
								}
								
								
							}
							if(existe == 1 && existe_alumno == 1){//existe la inscripcion y existe el alumno y el plan de estudio, actualizar
								
								try {
									k = new Conexion().actualizarInscripcionAsignatura(conexion, inscripciones.get(i));
								} catch (SQLException e) {
									e.printStackTrace();
								}
								if(k == 1){
									System.out.println(""+(i+1)+" - RUT: "+inscripciones.get(i).getAlu_rut()+" - Inscripción Actualizada correctamente.");
								}
								else{
									System.out.println(""+(i+1)+" - RUT: "+inscripciones.get(i).getAlu_rut()+" - Inscripción no Actualizada ERROR.");
								}
							}
							
						}
						if(inscripciones.get(i).getIns_estado().trim().equals("REPROBADA")){
							contador_reprobado+=1;
							if(existe == 0 && existe_alumno == 1){
								try {
									k = new Conexion().agregarInscripcionAsignatura(conexion, inscripciones.get(i));
								} catch (SQLException e) {
									e.printStackTrace();
								}
								if(k == 1){
									System.out.println(""+(i+1)+" - RUT: "+inscripciones.get(i).getAlu_rut()+" - Inscripción registrada correctamente.");
								}
								else{
									System.out.println(""+(i+1)+" - RUT: "+inscripciones.get(i).getAlu_rut()+" - Inscripción no registrada ERROR.");
								}
							}
							if(existe == 1 && existe_alumno == 1){//existe la inscripcion y existe el alumno y el plan de estudio, actualizar
								
								try {
									k = new Conexion().actualizarInscripcionAsignatura(conexion, inscripciones.get(i));
								} catch (SQLException e) {
									e.printStackTrace();
								}
								if(k == 1){
									System.out.println(""+(i+1)+" - RUT: "+inscripciones.get(i).getAlu_rut()+" - Inscripción Actualizada correctamente.");
								}
								else{
									System.out.println(""+(i+1)+" - RUT: "+inscripciones.get(i).getAlu_rut()+" - Inscripción no Actualizada ERROR.");
								}
								
							}
							
						}
						if(inscripciones.get(i).getIns_estado().trim().equals("PENDIENTE")){
							contador_pendiente+=1;
							if(existe == 0 && existe_alumno == 1){
								try {
									k = new Conexion().agregarInscripcionAsignatura(conexion, inscripciones.get(i));
								} catch (SQLException e) {
									e.printStackTrace();
								}
								if(k == 1){
									System.out.println(""+(i+1)+" - RUT: "+inscripciones.get(i).getAlu_rut()+" - Inscripción registrada correctamente.");
								}
								else{
									System.out.println(""+(i+1)+" - RUT: "+inscripciones.get(i).getAlu_rut()+" - Inscripción no registrada ERROR.");
								}
							}
						}
						if(existe == 1 && existe_alumno == 1){//existe la inscripcion y existe el alumno y el plan de estudio, actualizar
							
							try {
								k = new Conexion().actualizarInscripcionAsignatura(conexion, inscripciones.get(i));
							} catch (SQLException e) {
								e.printStackTrace();
							}
							if(k == 1){
								System.out.println(""+(i+1)+" - RUT: "+inscripciones.get(i).getAlu_rut()+" - Inscripción Actualizada correctamente.");
							}
							else{
								System.out.println(""+(i+1)+" - RUT: "+inscripciones.get(i).getAlu_rut()+" - Inscripción no Actualizada ERROR.");
							}
							
						}
						if(inscripciones.get(i).getIns_estado().trim().equals("INSCRITA")){
							contador_pendiente+=1;
							if(existe == 0 && existe_alumno == 1){
								try {
									k = new Conexion().agregarInscripcionAsignatura(conexion, inscripciones.get(i));
								} catch (SQLException e) {
									e.printStackTrace();
								}
								if(k == 1){
									System.out.println(""+(i+1)+" - RUT: "+inscripciones.get(i).getAlu_rut()+" - Inscripción registrada correctamente.");
								}
								else{
									System.out.println(""+(i+1)+" - RUT: "+inscripciones.get(i).getAlu_rut()+" - Inscripción no registrada ERROR.");
								}
							}
						}
						if(existe == 1 && existe_alumno == 1){//existe la inscripcion y existe el alumno y el plan de estudio, actualizar
							
							try {
								k = new Conexion().actualizarInscripcionAsignatura(conexion, inscripciones.get(i));
							} catch (SQLException e) {
								e.printStackTrace();
							}
							if(k == 1){
								System.out.println(""+(i+1)+" - RUT: "+inscripciones.get(i).getAlu_rut()+" - Inscripción Actualizada correctamente.");
							}
							else{
								System.out.println(""+(i+1)+" - RUT: "+inscripciones.get(i).getAlu_rut()+" - Inscripción no Actualizada ERROR.");
							}
							
						}
					}
					else{
						contador_null+=1;
					}
					
				}
				
				request.setAttribute("InscripcionesList", inscripciones);
				
				System.out.println("NULL: "+contador_null+" | Aprobados: "+contador_aprobado+" | Reprobados: "+contador_reprobado+" | Pendientes: "+contador_pendiente);
				request.setAttribute("exito", 1);
				request.setAttribute("mensaje", "La carga de Inscripciones se ha actualizado correctamente");
				try {
					conexion.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
				request.getRequestDispatcher("/ajax/inscripciones_ubb.jsp").forward(request, response);
			}
		}
		
		if(request.getParameter("agnio_alumno") != null && request.getParameter("periodo_alumno") != null){//permite realizar la carga de alumnos + cursa_plan_estudio desde la BD de la UBB
			if(request.getSession().getAttribute("tipo").equals("admin")  || request.getSession().getAttribute("tipo").equals("administrador-academico")){
					int contador =0;
					String agnio = request.getParameter("agnio_alumno"); 
					String periodo = request.getParameter("periodo_alumno");
				    ArrayList<Alumno> alumnos = new Conexion().CargaAlumnos(agnio.toString(),periodo.toString());
				    ArrayList<CursaPlanEstudio> cursa_plan = new Conexion().CargaCursaPlan(agnio.toString(),periodo.toString(), new Conexion().getPlanEstudioVigente());
				    String cur_estado = "";
		    		boolean cur_vigente = false;
			    	java.sql.Connection conexion = new Conexion().ConexionBdRetorna();
			    	int valido =0;
		    		for(int i=0; i <alumnos.size();i++)
				    {
				    	// aqui se comparan los estados para luego actualizar o insertar
				    	if(alumnos.get(i).getAlu_estado().equals("REGULAR"))
				    	{
				    		alumnos.get(i).setAlu_estado("activo");
				    		cur_estado = "regular";
				    		cur_vigente = true;
				    	}
				    	if(alumnos.get(i).getAlu_estado().equals("NO VIGENTE") || alumnos.get(i).getAlu_estado().equals("RENUNCIA") 
				    		|| alumnos.get(i).getAlu_estado().equals("PERDIDA CARRERA") || alumnos.get(i).getAlu_estado().equals("SIN INSCRIPCION PERIODO ACADEMICO VIGENTE")
				    		|| alumnos.get(i).getAlu_estado().equals("TITULADO") || alumnos.get(i).getAlu_estado().equals("FALLECIMIENTO") 
				    		|| alumnos.get(i).getAlu_estado().equals("RETIRO") || alumnos.get(i).getAlu_estado().equals("PENDIENTE"))
				    	{
				    		alumnos.get(i).setAlu_estado("inactivo");
				    		cur_estado = "irregular";
				    		cur_vigente = false;
				    	}
				    	
						try 
						{
							ResultSet resultado = new Conexion().getResultado();
							valido= new Conexion().verificar_usuario_con_parametros(conexion,resultado,alumnos.get(i).getAlu_rut(), "Alumno");
							if(valido == 0)
							{
								System.out.println("Alumno registrado correctamente");
								if(new Conexion().agregarAlumnoConParametros(conexion,alumnos.get(i)) == 1)
						    	{
						    		contador++;
						    		System.out.println("contador "+contador);
						    		System.out.println("Alumno registrado correctamente");
						    		int j= new Conexion().agregarCursaPlanConParametros(conexion,cursa_plan.get(i), cur_estado, cur_vigente);
						    		if(j == 1)
						    		{
						    			System.out.println("cursa plan estudo registrado correctamente");
						    		}else
						    		{
						    			System.out.println("cursa plan estudo no registrado correctamente");
						    		}
						    	}
							}else
							{
								contador++;
								// al reconocer alumno registrado en el sistema, se actualizan sus datos 
					    		int p = new Conexion().actualizarEstadoAlumnoConParametros(conexion,alumnos.get(i).getAlu_rut(),alumnos.get(i).getAlu_estado());
					    		if(p==1)
					    		{
					    			System.out.println("Alumno Actualizado Correctamente");
					    		}
					    		int m = new Conexion().actualizarCursaPlanAlumnoConParametros(conexion,cursa_plan.get(i).getPlan_id(),
					    				cursa_plan.get(i).getAlu_rut(),cursa_plan.get(i).getCur_agnio_ingreso(),
					    				cursa_plan.get(i).getCur_semestre(), cur_vigente,cur_estado);
					    		if(m == 1)
					    		{
					    			System.out.println("Datos Cursa Plan Alumno Actualizado Correctamente");
					    		}
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
				    }
				    request.setAttribute("exito", 1);
					request.setAttribute("mensaje", "La carga de Alumnos y sus situaciones académicas se han actualizado correctamente");
					System.out.println("contador "+contador);
					request.setAttribute("agnio_alumno", agnio.toString());
					request.setAttribute("periodo_alumno", periodo.toString());
					request.setAttribute("asignaturasList", new Conexion().getGestiones());	
					request.getRequestDispatcher("/vistas_admin/conexion_ubb.jsp").forward(request, response);
			}
		}
		
		if(request.getParameter("emp_rut") != null){
			Empresa empresa = new Conexion().getEmpresa(request.getParameter("emp_rut"));
			request.setAttribute("EMPRESA", empresa);
			request.setAttribute("COMUNA", new Conexion().getComuna(empresa.getCom_id()));
			request.getRequestDispatcher("ajax/empresa.jsp").forward(request, response);
		}
		
		if(request.getParameter("aca_rut") != null){
			Academico academico = new Conexion().getAcademico(request.getParameter("aca_rut"));
			request.setAttribute("ACADEMICO", academico);
			request.setAttribute("DEPARTAMENTO", new Conexion().getDepartamento(academico.getDep_id()));
			request.setAttribute("especialidadList",new Conexion().getEspecilidades(academico.getAca_rut()));
			request.getRequestDispatcher("ajax/academico.jsp").forward(request, response);
		}
		
		if(request.getParameter("adm_rut") != null){
			Administrador administrador = new Conexion().getAdministrador(request.getParameter("adm_rut"));
			request.setAttribute("ADMINISTRADOR", administrador);
			request.getRequestDispatcher("ajax/administrador.jsp").forward(request, response);
		}
		
		if(request.getParameter("sup_rut") != null){
			Supervisor supervisor = new Conexion().getSupervisor(request.getParameter("sup_rut"));
			request.setAttribute("SUPERVISOR", supervisor);
			request.setAttribute("empresa", new Conexion().getEmpresaSupervisor(supervisor.getSup_rut()));
			request.getRequestDispatcher("ajax/supervisor.jsp").forward(request, response);
		}
		
		if(request.getParameter("pra_id") != null){
			Practica practica = new Conexion().getPractica(Integer.parseInt(request.getParameter("pra_id")));
			request.setAttribute("supervisor", new Conexion().getSupervisor(practica.getSup_rut()));
			request.setAttribute("profesor", new Conexion().getAcademico(practica.getAca_rut()));
			request.setAttribute("alumno", new Conexion().getDatosAlumno(practica.getAlu_rut()));
			request.setAttribute("empresa", new Conexion().getEmpresa(practica.getEmp_rut()));
			request.setAttribute("comuna", new Conexion().getComuna(practica.getCom_id()));
			request.setAttribute("asignatura", new Conexion().getAsignatura(practica.getAsi_codigo(), practica.getPlan_id()));
			request.setAttribute("HISTORIAL", practica);
			request.getRequestDispatcher("ajax/practica.jsp").forward(request, response);
		}
		
		if(request.getParameter("bit_id") != null){//permite visualizar una bitacora para su edición
			int bit_id = Integer.parseInt(request.getParameter("bit_id"));
			//actualizar parametro dinamico segun maya curricularrequest.setAttribute("asignaturasList", new Conexion().getAsignaturas(1));//actualizar parametro dinamico segun maya curricular
			Bitacora bitacora = new Conexion().getBitacoraAlumno(bit_id);
			Practica practica = new Conexion().getPractica(bitacora.getPra_id());
			request.setAttribute("asignaturasList", new Conexion().getAsignaturas(practica.getPlan_id(),bit_id));
			request.setAttribute("bitacora", bitacora);
			request.setAttribute("plan_id", practica.getPlan_id());
			request.setAttribute("actual_asignatura",(9 - new Conexion().getActualAsignaturas(bitacora.getBit_id())));
			request.setAttribute("actual_imagen", new Conexion().getActualImagenes(bitacora.getBit_id()));
			request.getRequestDispatcher("ajax/bitacora.jsp").forward(request, response);
		}
		
		if(request.getParameter("n_asignatura") != null){//permite cargar el select de asignatura
			//request.setAttribute("asignatura", new Conexion().getAsignatura(practica.getAsi_codigo(), practica.getPlan_id()));
			int n_asignatura = Integer.parseInt(request.getParameter("n_asignatura"));
			int bit_id = Integer.parseInt(request.getParameter("bit_id_asignatura"));
			int plan_id = Integer.parseInt(request.getParameter("plan_id_asignatura"));
			request.setAttribute("n_asignatura", n_asignatura);
			request.setAttribute("asignaturasList", new Conexion().getAsignaturas(plan_id,bit_id));
			request.getRequestDispatcher("ajax/select_asignaturas.jsp").forward(request, response);
		}
		
		if(request.getParameter("n_giros") != null){//permite cargar el select de rubro y giro
			//request.setAttribute("asignatura", new Conexion().getAsignatura(practica.getAsi_codigo(), practica.getPlan_id()));
			int n_giros = Integer.parseInt(request.getParameter("n_giros"));
			request.setAttribute("n_giros", n_giros);
			request.setAttribute("girosList", new Conexion().getGiros());
			request.getRequestDispatcher("ajax/select_giro_rubro.jsp").forward(request, response);
		}

		if(request.getParameter("n_imagenes") != null){//permite cargar el select de imagen
			//request.setAttribute("asignatura", new Conexion().getAsignatura(practica.getAsi_codigo(), practica.getPlan_id()));
			int n_imagenes = Integer.parseInt(request.getParameter("n_imagenes"));
			request.setAttribute("n_imagenes", n_imagenes);
			request.getRequestDispatcher("ajax/input_imagen.jsp").forward(request, response);
		}
		
		if(request.getParameter("n_especialidad") != null){//permite cargar el select de especialidad
			int n_especialidad = Integer.parseInt(request.getParameter("n_especialidad"));
			request.setAttribute("n_especialidad", n_especialidad);
			request.setAttribute("especialidadList", new Conexion().getEspecialidades());
			request.getRequestDispatcher("ajax/select_especialidades.jsp").forward(request, response);
		}
		
		if(request.getParameter("plan_id") != null){// permite visualizar un formulario de registro de plan o modificaci�n
			String plan_id = request.getParameter("plan_id");
			//request.setAttribute("id", plan_id);
			Plan_estudio plan = new Conexion().getPlan(Integer.parseInt(plan_id));
			request.setAttribute("plan", plan);
			request.setAttribute("carrera", new Conexion().getCarrera(plan.getCar_codigo()));
			//request.setAttribute("asignaturaList", new Conexion().getAsignaturas(plan.getPlan_id()));
			request.getRequestDispatcher("ajax/plan.jsp").forward(request, response);
		}
		
		if(request.getParameter("cantidad_semestres") != null){// permite visualizar una tabla para indicar la cantidad de asignaturas por semestre, en la creacion de un plan de estudio
			int semestres = Integer.parseInt(request.getParameter("cantidad_semestres"));
			request.setAttribute("semestres", semestres);
			request.getRequestDispatcher("ajax/semestres_plan.jsp").forward(request, response);
		}
		
		if(request.getParameter("bit_id_obs")!= null)
		{
			String bit_id = request.getParameter("bit_id_obs");
			Bitacora bitacora = new Conexion().getBitacoraAlumno(Integer.parseInt(bit_id));
			request.setAttribute("bitacora",bitacora);
			Practica practica = new Conexion().getPractica(bitacora.getPra_id());
			request.setAttribute("alumno", new Conexion().getDatosAlumno(practica.getAlu_rut()));
			request.getRequestDispatcher("ajax/formulario_observacion.jsp").forward(request, response);
		}
		
		if(request.getParameter("pra_id_informe")!= null)
		{
			String pra_id = request.getParameter("pra_id_informe");
			Practica practica = new Conexion().getPractica(Integer.parseInt(pra_id));
			request.setAttribute("practica", practica);
			request.setAttribute("alumno", new Conexion().getDatosAlumno(practica.getAlu_rut()));
			request.getRequestDispatcher("ajax/formulario_informe.jsp").forward(request, response);
		}
		
		if(request.getParameter("practica") != null && request.getParameter("asignatura") != null && request.getParameter("plan") != null){
			//permite desplegar el formulario de asignacion de academicos evaluadores para el administrador, segun la practica, la asignatura y el plan de estudio
			Supervisor supervisor;
			Academico profesor_guia;
			Empresa empresa;		
			Practica practica;
			Alumno alumno;
			Asignatura gestion_actual;
			ArrayList<RolGestion> roles = new ArrayList<>();
			
			practica = new Conexion().getPractica(Integer.parseInt(request.getParameter("practica")));
            supervisor = new Conexion().getSupervisor(practica.getSup_rut());
            profesor_guia = new Conexion().getAcademico(practica.getAca_rut());
            alumno = new Conexion().getDatosAlumno(practica.getAlu_rut());
            empresa = new Conexion().getEmpresa(practica.getEmp_rut());
            
            gestion_actual = new Conexion().getAsignatura(practica.getAsi_codigo(), practica.getPlan_id());
            roles = new Conexion().getRoles(gestion_actual.getAsi_codigo(), gestion_actual.getPlan_id());
			
			/*System.out.println("\nPracticas Finalizadas = "+practicas.size()+" | Supervisores = "+supervisores.size()+" | Profesores Guia = "+profesores_guias.size()
								+" | Alumnos = "+alumnos.size()+" | Empresas = "+empresas.size());*/
			
			
			request.setAttribute("academicosList", new Conexion().getAcademicosTotalEvaluaciones());
			request.setAttribute("practica", practica);
			request.setAttribute("supervisor", supervisor);
			request.setAttribute("profesor", profesor_guia);
			request.setAttribute("alumnos", alumno);
			request.setAttribute("empresa", empresa);
			request.setAttribute("gestion", gestion_actual);			
			request.setAttribute("asignaturasList", new Conexion().getGestiones());
			//cambiar numero del if en caso de que se cambien los tipos de asignaturas (5 == a gestion profesional)
			if(gestion_actual.getAsi_tp_id()== 5){//si es gestion profesional mostrar formulario con comisión de defensa oral
				request.setAttribute("formulario", "profesional");
				System.out.println("\ndoGET AjaxControlador - javascript AsignarComision => es gestion profesional");
				
				for(int k=0;k<roles.size();k++){
					
					String descripcion = roles.get(k).getRol_nombre();
					
					if(descripcion.equals("Profesor Revisor - Lector Gestión Operativa I")){
						request.setAttribute("rol_bitacora", roles.get(k).getRol_id());
					}
					
					if(descripcion.equals("Profesor Revisor - Lector Gestión Operativa II")){
						request.setAttribute("rol_bitacora", roles.get(k).getRol_id());
					}
					
					if(descripcion.equals("Profesor Revisor - Lector Gestión Operativa III")){
						request.setAttribute("rol_bitacora", roles.get(k).getRol_id());					
					}
					
					if(descripcion.equals("Profesor Revisor - Lector Gestión Profesional")){
						request.setAttribute("rol_bitacora", roles.get(k).getRol_id());
					}
					
					if(descripcion.equals("Jefe Comisión Gestión Profesional")){
						request.setAttribute("rol_comision", roles.get(k).getRol_id());
					}
				}
			}
			else{
				request.setAttribute("formulario", "operativa");
				System.out.println("\ndoGET AjaxControlador - javascript AsignarComision => es gestion operativa");
				for(int k=0;k<roles.size();k++){
					
					//String descripcion = new Conexion().getParticipante(roles.get(k).getRol_id());
					String descripcion = roles.get(k).getRol_nombre();
					
					if(descripcion.equals("Profesor Revisor - Lector Gestión Operativa I")){
						request.setAttribute("rol_bitacora", roles.get(k).getRol_id());
					}
					
					if(descripcion.equals("Profesor Revisor - Lector Gestión Operativa II")){
						request.setAttribute("rol_bitacora", roles.get(k).getRol_id());
					}
					
					if(descripcion.equals("Profesor Revisor - Lector Gestión Operativa III")){
						request.setAttribute("rol_bitacora", roles.get(k).getRol_id());					
					}
					
					if(descripcion.equals("Profesor Revisor - Lector Gestión Profesional")){
						request.setAttribute("rol_bitacora", roles.get(k).getRol_id());
					}
				}
			}
			request.getRequestDispatcher("ajax/formulario_comision.jsp").forward(request, response);
		}
		
		if(request.getParameter("n_academico") != null){//permite cargar el select de academicos para la comision evaluadora de la gestion profesional
			int n_academico= Integer.parseInt(request.getParameter("n_academico"));
			System.out.println("numero asig "+n_academico);
			request.setAttribute("n_academico", n_academico);
			request.setAttribute("academicosList", new Conexion().getAcademicos());
			request.getRequestDispatcher("ajax/select_academicos.jsp").forward(request, response);
			
		}
		if(request.getParameter("rut_confirmacion_pass") != null && request.getParameter("nombres") != null 
		   && request.getParameter("apellido_p") != null && request.getParameter("apellido_m") != null 
		   && request.getParameter("tipo") != null){
			String rut= request.getParameter("rut_confirmacion_pass");
			String nombres = request.getParameter("nombres");
			String apellido_p = request.getParameter("apellido_p");
			String apellido_m = request.getParameter("apellido_m");
			String tipo = request.getParameter("tipo");
			String correo = request.getParameter("correo");
			request.setAttribute("rut", rut);
			request.setAttribute("tipo", tipo);
			request.setAttribute("correo", correo);
			request.setAttribute("mensaje", "¿ Está seguro que desea generar una nueva Contraseña para que el usuario"
					+ " "+nombres+" "+apellido_p+" "+apellido_m+" acceda al sistema ?");
			request.getRequestDispatcher("ajax/mensaje_confirmacion_clave.jsp").forward(request, response);
		}
		if(request.getParameter("tipo_informe")!= null)//permite saber que informe se cargara
		{
			
			String tipo = request.getParameter("tipo_informe");
			if(tipo.equals("gestiones"))
			{
				request.setAttribute("tipo", 1);
				request.setAttribute("practicaList",new Conexion().getAsignaturasTipoPractica());
				request.getRequestDispatcher("ajax/informes.jsp").forward(request, response);
			}
			if(tipo.equals("regiones"))
			{
				request.setAttribute("tipo", 2);
				request.getRequestDispatcher("ajax/informes.jsp").forward(request, response);
			}
			if(tipo.equals("comunas"))
			{
				request.setAttribute("tipo", 3);
				request.getRequestDispatcher("ajax/informes.jsp").forward(request, response);
			}
			if(tipo.equals("empresas"))
			{
				request.setAttribute("tipo", 4);
				request.setAttribute("practicaList",new Conexion().getAsignaturasTipoPractica());
				request.getRequestDispatcher("ajax/informes.jsp").forward(request, response);
			}
			if(tipo.equals("obras"))
			{
				request.setAttribute("tipo", 5);
				request.setAttribute("practicaList",new Conexion().getAsignaturasTipoPractica());
				request.getRequestDispatcher("ajax/informes.jsp").forward(request, response);
			}
			if(tipo.equals("supervisores"))
			{
				request.setAttribute("tipo", 6);
				request.setAttribute("practicaList",new Conexion().getAsignaturasTipoPractica());
				request.getRequestDispatcher("ajax/informes.jsp").forward(request, response);
			}
			if(tipo.equals("areas"))
			{
				request.setAttribute("tipo", 7);
				request.setAttribute("practicaList",new Conexion().getAsignaturasTipoPractica());
				request.getRequestDispatcher("ajax/informes.jsp").forward(request, response);
			}
			if(tipo.equals("academicos"))
			{
				request.setAttribute("tipo", 8);
				request.setAttribute("practicaList",new Conexion().getAsignaturasTipoPractica());
				request.getRequestDispatcher("ajax/informes.jsp").forward(request, response);
			}
			if(tipo.equals("notas"))
			{
				request.setAttribute("tipo", 9);
				request.setAttribute("practicaList",new Conexion().getAsignaturasTipoPractica());
				request.getRequestDispatcher("ajax/informes.jsp").forward(request, response);
			}
		}
		if(request.getParameter("rut_usuario")!= null && request.getParameter("tipo_usuario")!= null)//permite obtener la informacion de un usuario para la recuperacion de la password mediante el rut y tipo de usuario
		{
			String rut = request.getParameter("rut_usuario");
			String tipo = request.getParameter("tipo_usuario");
			if(tipo.equals("Alumno"))
			{
				if(new Conexion().verificar_usuario(rut,"Alumno",1) == 1)
				{
					Alumno alumno = new Conexion().getDatosAlumno(rut);
					request.setAttribute("alumno", alumno);
					request.setAttribute("tipo_num", 1);
					request.setAttribute("tipo", tipo);
				}else{
					request.setAttribute("tipo_num", 0);
				}
			}
			if(tipo.equals("Administrador"))
			{
				if(new Conexion().verificar_usuario(rut,"Administrador",1) == 1)
				{
					if(new Conexion().verificar_usuario(rut,"Academico",1) == 1)
					{
						Administrador admin = new Conexion().getAdministrador(rut);
						request.setAttribute("administrador", admin);
						request.setAttribute("tipo_num", 1);
						request.setAttribute("tipo", tipo+" - Academico");
					}
					else{
						Administrador admin = new Conexion().getAdministrador(rut);
						request.setAttribute("administrador", admin);
						request.setAttribute("tipo_num", 1);
						request.setAttribute("tipo", tipo);
					}
				}else{
					request.setAttribute("tipo_num", 0);
				}
			}
			if(tipo.equals("Supervisor"))
			{
				if(new Conexion().verificar_usuario(rut,"Supervisor",1) == 1)
				{
					Supervisor supervisor = new Conexion().getSupervisor(rut);
					request.setAttribute("supervisor", supervisor);
					request.setAttribute("tipo_num", 1);
					request.setAttribute("tipo", tipo);
				}else{
					request.setAttribute("tipo_num", 0);
				}
			}
			if(tipo.equals("Academico"))
			{
				if(new Conexion().verificar_usuario(rut,"Academico",1) == 1)
				{
					if(new Conexion().verificar_usuario(rut,"Administrador",1) == 1)
					{
						Academico academico = new Conexion().getAcademico(rut);
						request.setAttribute("academico", academico);
						request.setAttribute("tipo_num", 1);
						request.setAttribute("tipo", "Academico"+" - Administrador");
					}
					else{
						Academico academico = new Conexion().getAcademico(rut);
						request.setAttribute("academico", academico);
						request.setAttribute("tipo_num", 1);
						request.setAttribute("tipo",  "Academico");
					}	
				}else{
					request.setAttribute("tipo_num", 0);
				}
			}
			
			request.getRequestDispatcher("ajax/info_usuario.jsp").forward(request, response);
		}
		
		if(request.getParameter("rubrica_id") != null)//permite redireccionar para preparar la configuracion del formulario de registro de una nueva rúbrica
		{
			request.getRequestDispatcher("vistas_admin/nueva_rubrica").forward(request, response);
		}
	}
}
