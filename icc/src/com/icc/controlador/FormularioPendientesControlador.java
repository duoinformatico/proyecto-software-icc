package com.icc.controlador;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.icc.modelo.Asignatura;
import com.icc.modelo.Comuna;
import com.icc.modelo.Practica;
import com.icc.modelo.Provincia;
import com.icc.modelo.Region;
import com.icc.modelo.RolGestion;
import com.icc.utilidades.Conexion;

/**
 * Servlet implementation class FormularioPendientesControlador
 */
@WebServlet("/FormularioPendientesControlador")
public class FormularioPendientesControlador extends HttpServlet {
	private static final long serialVersionUID = 1L;
	String mensaje="";
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FormularioPendientesControlador() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("pra_id");
		try {		
			Practica pendiente = new Conexion().getPractica(Integer.parseInt(id));
			request.setAttribute("id", id);
			request.setAttribute("supervisor", new Conexion().getSupervisor(pendiente.getSup_rut()));
			request.setAttribute("alumno", new Conexion().getDatosAlumno(pendiente.getAlu_rut()));
			request.setAttribute("academicosList", new Conexion().getAcademicos());
			request.setAttribute("academico_elegido", new Conexion().getAcademico(pendiente.getAca_rut()));
			request.setAttribute("pendiente", pendiente);
			request.setAttribute("empresaList", new Conexion().getEmpresas());
			Comuna comuna = new Conexion().getComuna(pendiente.getCom_id());
			request.setAttribute("comuna", comuna);
			Provincia provincia = new Conexion().getProvincia(comuna.getPro_id());
			request.setAttribute("provincia", provincia);
			Region region = new Conexion().getRegion(provincia.getReg_id());
			request.setAttribute("region", region);
			request.setAttribute("comunaList", new Conexion().getComunas());
			request.setAttribute("regionList", new Conexion().getRegiones());
			request.setAttribute("provinciaList", new Conexion().getProvincias());
			request.setAttribute("gestionesList", new Conexion().getGestiones());
			request.setAttribute("obrasList", new Conexion().getObras());
			request.setAttribute("planesList", new Conexion().getPlanes());
			request.getRequestDispatcher("ajax/formulario_pendiente.jsp").forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String accion = request.getParameter("accion");
		String id = request.getParameter("id");
		String empresa = request.getParameter("empresa");
		String supervisor = request.getParameter("supervisor");
		String obra = request.getParameter("obra");
		String alumno = request.getParameter("alumno");
		String asignatura = request.getParameter("asignatura");
		String plan = request.getParameter("plan");
		String fecha_ins = request.getParameter("fecha_ins");
		String nombre_obra = request.getParameter("obra_nombre");
		String comuna= request.getParameter("comuna");
		String direccion= request.getParameter("direccion");
		String tareas = request.getParameter("tareas");
		String inicio = request.getParameter("inicio");
		String termino = request.getParameter("termino");
		String horas = request.getParameter("horas");
		String profesor_guia = request.getParameter("profesor");
		//int ban1=0,ban2=0,ban3=0;
		int exito = 0;
		if (accion == null) {
		    System.out.println("No fue seleccionado nada");
		} else if (accion.equals("aprobar")) {
			
			try
			{	
				Practica practica = new Practica(Integer.parseInt(id),empresa,supervisor,Integer.parseInt(comuna),Integer.parseInt(obra),
						alumno,Integer.parseInt(asignatura),Integer.parseInt(plan),fecha_ins,nombre_obra,direccion,tareas,inicio,termino
						,Integer.parseInt(horas),"Aceptada",profesor_guia);
				System.out.println("profesor "+profesor_guia);
				int i = new Conexion().ModificarPractica(practica,2);
				if(i == 1)
				{
					try {
						int plan_estudio_ingresado = practica.getPlan_id();
						int codigo_asignatura_ingresado = practica.getAsi_codigo();
						ArrayList<Asignatura> lista_gestiones = new Conexion().getAsignaturas();
						int pra_id = new Conexion().getPracticaId(empresa,supervisor,alumno,Integer.parseInt(asignatura),Integer.parseInt(obra),Integer.parseInt(comuna),Integer.parseInt(plan));

						int ban=1;
						for(int j=0;i<lista_gestiones.size() && ban==1;j++)
						{
							if(codigo_asignatura_ingresado == lista_gestiones.get(j).getAsi_codigo())
							{
								ban=0;
								ArrayList<RolGestion> roles = new Conexion().getRoles(codigo_asignatura_ingresado,plan_estudio_ingresado);
								for(int k=0;k<roles.size();k++)
								{
									String descripcion = roles.get(k).getRol_nombre();
									//se agregan los evaluadores profesor guia, estudiante y supervisor
									// si es que se llegara a agregar otro rol, se debe agregar las lineas de codigo para agregar una nueva comisi�n
									if(descripcion.equals("Profesor Guía Gestión Operativa I"))
									{
										new Conexion().crearProfesorGuiaEvaluacion(practica.getAca_rut(),pra_id,roles.get(k).getRol_id());
										//ban1=1;
									}
									if(descripcion.equals("Supervisor en Obra Gestión Operativa I"))
									{
										new Conexion().crearSupervisorEvaluacion(practica.getSup_rut(),pra_id,roles.get(k).getRol_id());
										//ban2=1;
									}
									if(descripcion.equals("Estudiante Gestión Operativa I"))
									{
										new Conexion().crearAlumnoEvaluacion(practica.getAlu_rut(),pra_id,roles.get(k).getRol_id());
										//ban3=1;
									}
									if(descripcion.equals("Profesor Guía Gestión Operativa II"))
									{
										new Conexion().crearProfesorGuiaEvaluacion(practica.getAca_rut(),pra_id,roles.get(k).getRol_id());
										//ban1=1;
									}
									if(descripcion.equals("Supervisor en Obra Gestión Operativa II"))
									{
										new Conexion().crearSupervisorEvaluacion(practica.getSup_rut(),pra_id,roles.get(k).getRol_id());
										//ban2=1;
									}
									if(descripcion.equals("Estudiante Gestión Operativa II"))
									{
										new Conexion().crearAlumnoEvaluacion(practica.getAlu_rut(),pra_id,roles.get(k).getRol_id());
										//ban3=1;
									}
									if(descripcion.equals("Profesor Guía Gestión Operativa III"))
									{
										new Conexion().crearProfesorGuiaEvaluacion(practica.getAca_rut(),pra_id,roles.get(k).getRol_id());
										//ban1=1;
									}
									if(descripcion.equals("Supervisor en Obra Gestión Operativa III"))
									{
										new Conexion().crearSupervisorEvaluacion(practica.getSup_rut(),pra_id,roles.get(k).getRol_id());
										//ban2=1;
									}
									if(descripcion.equals("Estudiante Gestión Operativa III"))
									{
										new Conexion().crearAlumnoEvaluacion(practica.getAlu_rut(),pra_id,roles.get(k).getRol_id());
										//ban3=1;
									}
									if(descripcion.equals("Profesor Guía Gestión Profesional"))
									{
										System.out.println("aca "+practica.getAca_rut());
										new Conexion().crearProfesorGuiaEvaluacion(practica.getAca_rut(),pra_id,roles.get(k).getRol_id());
										//ban1=1;
									}
									if(descripcion.equals("Supervisor en Obra Gestión Profesional"))
									{
										new Conexion().crearSupervisorEvaluacion(practica.getSup_rut(),pra_id,roles.get(k).getRol_id());
										//ban2=1;
									}
									if(descripcion.equals("Estudiante Gestión Profesional"))
									{
										new Conexion().crearAlumnoEvaluacion(practica.getAlu_rut(),pra_id,roles.get(k).getRol_id());
										//ban3=1;
									}
								}
							}
						}
						mensaje = "La práctica fue Aceptada y se realizarón los cambios correctamente";
						request.setAttribute("mensaje", mensaje);
						exito = 1;
						request.setAttribute("exito", exito);
						request.setAttribute("pendientesList", new Conexion().getpendientes());
						request.getRequestDispatcher("vistas_admin/pendientes.jsp").forward(request, response);
					} catch (Exception e) {
						e.printStackTrace();
						mensaje = "Hubo un error al Modificar lo datos de la práctica";
						request.setAttribute("mensaje", mensaje);
						exito = 0;
						request.setAttribute("exito", exito);
						request.setAttribute("pendientesList", new Conexion().getpendientes());
						request.getRequestDispatcher("vistas_admin/pendientes.jsp").forward(request, response);
					}
				}
				else{
					mensaje = "Hubo un error al Modificar lo datos de la práctica";
					request.setAttribute("mensaje", mensaje);
					exito = 0;
					request.setAttribute("exito", exito);
					request.setAttribute("pendientesList", new Conexion().getpendientes());
					request.getRequestDispatcher("vistas_admin/pendientes.jsp").forward(request, response);
				}
			} catch (Exception e) 
			{
				e.printStackTrace();
			}
		    System.out.println("aprobar");
		} else if (accion.equals("rechazar")) {
		    
			try
			{	
				int i = new Conexion().ModificarPractica(new Practica(Integer.parseInt(id),empresa,profesor_guia,supervisor,Integer.parseInt(comuna),Integer.parseInt(obra),
						alumno,Integer.parseInt(asignatura),Integer.parseInt(plan),fecha_ins,nombre_obra,direccion,tareas,inicio,termino
						,Integer.parseInt(horas),"Rechazada"),2);
				if(i == 1)
				{
					mensaje = "La práctica fue Rechazada y se realizarón los cambios correctamente";
					request.setAttribute("mensaje", mensaje);
					request.setAttribute("pendientesList", new Conexion().getpendientes());
					request.getRequestDispatcher("vistas_admin/pendientes.jsp").forward(request, response);
				}
				else{
					mensaje = "Hubo un error al Modificar la practica";
					request.setAttribute("mensaje", mensaje);
					request.setAttribute("pendientesList", new Conexion().getpendientes());
					request.getRequestDispatcher("vistas_admin/pendientes.jsp").forward(request, response);
				}
			} catch (Exception e) 
			{
				e.printStackTrace();
			}
			System.out.println("rechazar");
		} else {
			request.getSession().invalidate();
			request.getRequestDispatcher("index.jsp").forward(request, response);
		}
	}

}
