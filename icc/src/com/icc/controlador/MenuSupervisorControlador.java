package com.icc.controlador;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.icc.utilidades.Conexion;
import com.icc.utilidades.PasswordGenerador;

/**
 * Servlet implementation class MenuSupervisorControlador
 */
@WebServlet("/MenuSupervisorControlador")
public class MenuSupervisorControlador extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MenuSupervisorControlador() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("clave", PasswordGenerador.getPassword(
				  PasswordGenerador.MINUSCULAS+
				  PasswordGenerador.MAYUSCULAS+
				  PasswordGenerador.NUMEROS,10));
		request.setAttribute("empresasList", new Conexion().getEmpresas());
		request.getRequestDispatcher("ajax/formulario_supervisor.jsp").forward(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setAttribute("SupervisoresList",new Conexion().getSupervisores() );
		request.getRequestDispatcher("/vistas_admin/supervisores.jsp").forward(request, response);
	}
}
