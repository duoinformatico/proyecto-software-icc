package com.icc.controlador;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.icc.utilidades.Conexion;


/**
 * Servlet implementation class LoginControlador
 */
@WebServlet("/LoginControlador")
public class LoginControlador extends HttpServlet {
	private static final long serialVersionUID = 1L;
    int valor;   
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginControlador() {
        super();
    }


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String rut = request.getParameter("rut");
		String pass = request.getParameter("pass");
		int ban=1;
		try {
			valor = new Conexion().verificar_alumno(rut,pass);
			if(valor == 1)
			{
				response.setHeader("Cache-control", "no-cache, no-store, must-revalidate");
				request.getSession().setAttribute("auth", true);
			    request.getSession().setAttribute("rut", rut);
			    request.getSession().setAttribute("tipo", "alumno");
			    request.setAttribute("datos", new Conexion().getDatosAlumno(rut));
			    request.getSession().setAttribute("usuario", new Conexion().getIdentificacion(rut, "alumno") );
				ban=0;
				int cantidad = new Conexion().getTotalPracticasSemestre(rut);
				request.setAttribute("cantidad", cantidad);
				if(cantidad != 0)
				{
					request.setAttribute("practicaList", new Conexion().getPracticasSemestre(rut));
				}
			    request.getRequestDispatcher("vistas_alumno/index_alumno.jsp").forward(request, response);
				
			}
		    valor = new Conexion().verificar_administrador(rut,pass);
			if(valor == 1 && ban==1)
			{
				if(new Conexion().verificar_academico(rut, pass) == 1)
				{
					response.setHeader("Cache-control", "no-cache, no-store, must-revalidate");
					request.getSession().setAttribute("auth", true);
				    request.getSession().setAttribute("rut", rut);
				    request.getSession().setAttribute("tipo", "administrador-academico");
				    request.getSession().setAttribute("usuario", new Conexion().getIdentificacion(rut, "administrador") );
				    request.setAttribute("total_estudiantes", new Conexion().getTotalEstudiantes("academico", rut));
				    request.setAttribute("total_pendientes", new Conexion().getTotalPracticas());
				    ban=0;
					request.getRequestDispatcher("vistas_admin_academico/index_admin_aca.jsp").forward(request, response);
					
				}else{
					response.setHeader("Cache-control", "no-cache, no-store, must-revalidate");
					request.getSession().setAttribute("auth", true);
				    request.getSession().setAttribute("rut", rut);
				    request.getSession().setAttribute("tipo", "admin");
				    request.getSession().setAttribute("usuario", new Conexion().getIdentificacion(rut, "administrador") );
				    request.setAttribute("total", new Conexion().getTotalPracticas());
				    request.setAttribute("datos", new Conexion().getNombresAdmin(rut));
				    ban=0;
					request.getRequestDispatcher("vistas_admin/index_administrador.jsp").forward(request, response);
				}
			}
			valor = new Conexion().verificar_supervisor(rut,pass);
			if(valor == 1 && ban==1)
			{
				response.setHeader("Cache-control", "no-cache, no-store, must-revalidate");
				request.getSession().setAttribute("auth", true);
			    request.getSession().setAttribute("rut", rut);
			    request.getSession().setAttribute("tipo", "supervisor");
			    request.setAttribute("total", new Conexion().getTotalEstudiantes("supervisor", rut));
			    request.setAttribute("datos", new Conexion().getSupervisor(rut));
			    request.getSession().setAttribute("usuario", new Conexion().getIdentificacion(rut, "supervisor") );
				ban=0;
			    request.getRequestDispatcher("vistas_supervisor/index_supervisor.jsp").forward(request, response);
				
			}
			valor = new Conexion().verificar_academico(rut,pass);
			if(valor == 1 && ban==1)
			{
				if(new Conexion().verificar_administrador(rut, pass)== 1)
				{
					response.setHeader("Cache-control", "no-cache, no-store, must-revalidate");
					request.getSession().setAttribute("auth", true);
				    request.getSession().setAttribute("rut", rut);
				    request.getSession().setAttribute("tipo", "administrador-academico");
				    request.setAttribute("total_pendientes", new Conexion().getTotalPracticas());
				    request.setAttribute("total_estudiantes", new Conexion().getTotalEstudiantes("academico", rut));
				    request.getSession().setAttribute("usuario", new Conexion().getIdentificacion(rut, "academico") );
				    request.getRequestDispatcher("vistas_admin_academico/index_admin_aca.jsp").forward(request, response);
					ban=0;
				}else{
					response.setHeader("Cache-control", "no-cache, no-store, must-revalidate");
					request.getSession().setAttribute("auth", true);
				    request.getSession().setAttribute("rut", rut);
				    request.getSession().setAttribute("tipo", "academico");
				    request.setAttribute("total", new Conexion().getTotalEstudiantes("academico", rut));
				    request.setAttribute("datos", new Conexion().getAcademico(rut));
				    request.getSession().setAttribute("usuario", new Conexion().getIdentificacion(rut, "academico") );
					request.getRequestDispatcher("vistas_academico/index_academico.jsp").forward(request, response);
					ban=0;
				}
			}		
			if(ban==1)
			{
				request.getRequestDispatcher("index.jsp").forward(request, response);
			}
		}
		catch(Exception e)
		{		
			e.printStackTrace();
		}
	}
}
