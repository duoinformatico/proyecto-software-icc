package com.icc.controlador;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.icc.utilidades.Conexion;

/**
 * Servlet implementation class MenuCambioClaveControlador
 */
@WebServlet("/MenuCambioClave")
public class MenuCambioClaveControlador extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MenuCambioClaveControlador() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String tipo = (String) request.getSession().getAttribute("tipo");
		if(tipo.equals("admin"))
		{
			request.getRequestDispatcher("/vistas_admin/cambio_clave.jsp").forward(request, response);
		}
		if(tipo.equals("academico"))
		{
			request.getRequestDispatcher("/vistas_academico/cambio_clave.jsp").forward(request, response);
		}
		if(tipo.equals("supervisor"))
		{
			request.getRequestDispatcher("/vistas_supervisor/cambio_clave.jsp").forward(request, response);
		}
		if(tipo.equals("alumno"))
		{
			request.getRequestDispatcher("/vistas_alumno/cambio_clave.jsp").forward(request, response);
		}
		if(tipo.equals("administrador-academico"))
		{
			request.getRequestDispatcher("/vistas_admin/cambio_clave.jsp").forward(request, response);
		}
			
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String rut = (String) request.getSession().getAttribute("rut");
		String tipo = (String) request.getSession().getAttribute("tipo");
		int ban1=0;
		int ban2=0;
		int ban3=0;
		int ban4=0;
		
		if(tipo.equals("admin"))
		{
			String actual = request.getParameter("actual");
			String nueva = request.getParameter("nueva");
			String contrasegnia = "";
			try 
			{
				if(new Conexion().verificar_clave(rut,actual,tipo) == 1)
				{
					contrasegnia = new Conexion().getClave(rut,"Administrador");
					new Conexion().modificarClaveUsuario(rut, "Administrador", nueva);
					ban1=1;
					if(new Conexion().verificar_usuario(rut, "Academico",0)==1)
					{
						contrasegnia = new Conexion().getClave(rut,"Academico");
						new Conexion().modificarClaveUsuario(rut, "Academico", nueva);
						ban2=1;
						
						if(new Conexion().verificar_usuario(rut, "Supervisor",0)==1)
						{
							contrasegnia = new Conexion().getClave(rut,"Supervisor");
							new Conexion().modificarClaveUsuario(rut, "Supervisor", nueva);
							ban3=1;
						}
						if(new Conexion().verificar_usuario(rut, "Alumno",0)==1)
						{
							contrasegnia = new Conexion().getClave(rut,"Alumno");
							new Conexion().modificarClaveUsuario(rut, "Alumno", nueva);
							ban4=1;
						}
					}
					
					int bandera =1;
					request.setAttribute("bandera", bandera);
					String mensaje = "Se ha modificado con exito la clave de acceso al sistema.";
					request.setAttribute("mensaje", mensaje);
					request.getRequestDispatcher("/vistas_admin/cambio_clave.jsp").forward(request, response);
				}else
				{
					int bandera =2;
					request.setAttribute("bandera", bandera);
					String mensaje = "La clave de acceso actual no coincide con lo ingresado en el campo.";
					request.setAttribute("mensaje", mensaje);
					request.getRequestDispatcher("/vistas_admin/cambio_clave.jsp").forward(request, response);
				}
			} catch (Exception e) {
				if(ban1==1)
				{
					new Conexion().modificarClaveUsuario(rut, "Administrador", contrasegnia);
				}
				if(ban2==1)
				{
					new Conexion().modificarClaveUsuario(rut, "Academico", contrasegnia);
				}
				if(ban3==1)
				{
					new Conexion().modificarClaveUsuario(rut, "Supervisor", contrasegnia);
				}
				if(ban4==1)
				{
					new Conexion().modificarClaveUsuario(rut, "Alumno", contrasegnia);
				}
			}
		}
		if(tipo.equals("administrador-academico"))
		{
			String actual = request.getParameter("actual");
			String nueva = request.getParameter("nueva");
			String contrasegnia = "";
			try 
			{
				if(new Conexion().verificar_clave(rut,actual,tipo) == 1)
				{
					contrasegnia = new Conexion().getClave(rut,"Administrador");
					new Conexion().modificarClaveUsuario(rut, "Administrador", nueva);
					ban1=1;
					contrasegnia = new Conexion().getClave(rut,"Academico");
					new Conexion().modificarClaveUsuario(rut, "Academico", nueva);
					ban2=1;
					
					if(new Conexion().verificar_usuario(rut, "Supervisor",0)==1)
					{
						contrasegnia = new Conexion().getClave(rut,"Supervisor");
						new Conexion().modificarClaveUsuario(rut, "Supervisor", nueva);
						ban3=1;
					}
					if(new Conexion().verificar_usuario(rut, "Alumno",0)==1)
					{
						contrasegnia = new Conexion().getClave(rut,"Alumno");
						new Conexion().modificarClaveUsuario(rut, "Alumno", nueva);
						ban4=1;
					}
					int bandera =1;
					request.setAttribute("bandera", bandera);
					String mensaje = "Se ha modificado con exito la clave de acceso al sistema.";
					request.setAttribute("mensaje", mensaje);
					request.getRequestDispatcher("/vistas_admin/cambio_clave.jsp").forward(request, response);
				}else
				{
					int bandera =2;
					request.setAttribute("bandera", bandera);
					String mensaje = "La clave de acceso actual no coincide con lo ingresado en el campo.";
					request.setAttribute("mensaje", mensaje);
					request.getRequestDispatcher("/vistas_admin/cambio_clave.jsp").forward(request, response);
				}
					
			}
			catch (Exception e) {
				if(ban1==1)
				{
					new Conexion().modificarClaveUsuario(rut, "Administrador", contrasegnia);
				}
				if(ban2==1)
				{
					new Conexion().modificarClaveUsuario(rut, "Academico", contrasegnia);
				}
				if(ban3==1)
				{
					new Conexion().modificarClaveUsuario(rut, "Supervisor", contrasegnia);
				}
				if(ban4==1)
				{
					new Conexion().modificarClaveUsuario(rut, "Alumno", contrasegnia);
				}
			}
		}
		if(tipo.equals("supervisor"))
		{
			System.out.println("entrando");
			String actual = request.getParameter("actual");
			String nueva = request.getParameter("nueva");
			String contrasegnia = "";
			try 
			{
				if(new Conexion().verificar_clave(rut,actual,tipo) == 1)
				{
					contrasegnia = new Conexion().getClave(rut,"Supervisor");
					new Conexion().modificarClaveUsuario(rut, "Supervisor", nueva);
					ban1=1;
					if(new Conexion().verificar_usuario(rut, "Academico",0)==1)
					{
						contrasegnia = new Conexion().getClave(rut,"Academico");
						new Conexion().modificarClaveUsuario(rut, "Academico", nueva);
						ban2=1;
					}
					if(new Conexion().verificar_usuario(rut, "Administrador",0)==1)
					{
						contrasegnia = new Conexion().getClave(rut,"Administrador");
						new Conexion().modificarClaveUsuario(rut, "Administrador", nueva);
						ban3=1;
					}
					if(new Conexion().verificar_usuario(rut, "Alumno",0)==1)
					{
						contrasegnia = new Conexion().getClave(rut,"Alumno");
						new Conexion().modificarClaveUsuario(rut, "Alumno", nueva);
						ban4=1;
					}
					int bandera =1;
					request.setAttribute("bandera", bandera);
					String mensaje = "Se ha modificado con exito la clave de acceso al sistema.";
					request.setAttribute("mensaje", mensaje);
					request.getRequestDispatcher("/vistas_supervisor/cambio_clave.jsp").forward(request, response);
				}else
				{
					int bandera =2;
					request.setAttribute("bandera", bandera);
					String mensaje = "La clave de acceso actual no coincide con lo ingresado en el campo.";
					request.setAttribute("mensaje", mensaje);
					request.getRequestDispatcher("/vistas_supervisor/cambio_clave.jsp").forward(request, response);
				}
			} catch (Exception e) {
				if(ban1==1)
				{
					new Conexion().modificarClaveUsuario(rut, "Supervisor", contrasegnia);
				}
				if(ban2==1)
				{
					new Conexion().modificarClaveUsuario(rut, "Academico", contrasegnia);
				}
				if(ban3==1)
				{
					new Conexion().modificarClaveUsuario(rut, "Administrador", contrasegnia);
				}
				if(ban4==1)
				{
					new Conexion().modificarClaveUsuario(rut, "Alumno", contrasegnia);
				}
			}
		}
		if(tipo.equals("academico"))
		{
			String actual = request.getParameter("actual");
			String nueva = request.getParameter("nueva");
			String contrasegnia = "";
			try 
			{
				if(new Conexion().verificar_clave(rut,actual,tipo) == 1)
				{
					contrasegnia = new Conexion().getClave(rut,"Academico");
					new Conexion().modificarClaveUsuario(rut, "Academico", nueva);
					ban1=1;
					if(new Conexion().verificar_usuario(rut, "Supervisor",0)==1)
					{
						contrasegnia = new Conexion().getClave(rut,"Supervisor");
						new Conexion().modificarClaveUsuario(rut, "Supervisor", nueva);
						ban2=1;
					}
					if(new Conexion().verificar_usuario(rut, "Administrador",0)==1)
					{
						contrasegnia = new Conexion().getClave(rut,"Administrador");
						new Conexion().modificarClaveUsuario(rut, "Administrador", nueva);
						ban3=1;
					}
					if(new Conexion().verificar_usuario(rut, "Alumno",0)==1)
					{
						contrasegnia = new Conexion().getClave(rut,"Alumno");
						new Conexion().modificarClaveUsuario(rut, "Alumno", nueva);
						ban4=1;
					}
					int bandera =1;
					request.setAttribute("bandera", bandera);
					String mensaje = "Se ha modificado con exito la clave de acceso al sistema.";
					request.setAttribute("mensaje", mensaje);
					request.getRequestDispatcher("/vistas_academico/cambio_clave.jsp").forward(request, response);
				}else
				{
					int bandera =2;
					request.setAttribute("bandera", bandera);
					String mensaje = "La clave de acceso actual no coincide con lo ingresado en el campo.";
					request.setAttribute("mensaje", mensaje);
					request.getRequestDispatcher("/vistas_academico/cambio_clave.jsp").forward(request, response);
				}
			} catch (Exception e) {
				if(ban1==1)
				{
					new Conexion().modificarClaveUsuario(rut, "Academico", contrasegnia);
				}
				if(ban2==1)
				{
					new Conexion().modificarClaveUsuario(rut, "Supervisor", contrasegnia);
				}
				if(ban3==1)
				{
					new Conexion().modificarClaveUsuario(rut, "Administrador", contrasegnia);
				}
				if(ban4==1)
				{
					new Conexion().modificarClaveUsuario(rut, "Alumno", contrasegnia);
				}
			}
		}
		if(tipo.equals("alumno"))
		{
			String actual = request.getParameter("actual");
			String nueva = request.getParameter("nueva");
			String contrasegnia = "";
			try 
			{
				if(new Conexion().verificar_clave(rut,actual,tipo) == 1)
				{
					contrasegnia = new Conexion().getClave(rut,"Alumno");
					new Conexion().modificarClaveUsuario(rut, "Alumno", nueva);
					ban1=1;
					if(new Conexion().verificar_usuario(rut, "Supervisor",0)==1)
					{
						contrasegnia = new Conexion().getClave(rut,"Supervisor");
						new Conexion().modificarClaveUsuario(rut, "Supervisor", nueva);
						ban2=1;
					}
					if(new Conexion().verificar_usuario(rut, "Administrador",0)==1)
					{
						contrasegnia = new Conexion().getClave(rut,"Administrador");
						new Conexion().modificarClaveUsuario(rut, "Administrador", nueva);
						ban3=1;
					}
					if(new Conexion().verificar_usuario(rut, "Academico",0)==1)
					{
						contrasegnia = new Conexion().getClave(rut,"Academico");
						new Conexion().modificarClaveUsuario(rut, "Academico", nueva);
						ban4=1;
					}
					int bandera =1;
					request.setAttribute("bandera", bandera);
					String mensaje = "Se ha modificado con exito la clave de acceso al sistema.";
					request.setAttribute("mensaje", mensaje);
					request.getRequestDispatcher("/vistas_alumno/cambio_clave.jsp").forward(request, response);
				}else
				{
					int bandera =2;
					request.setAttribute("bandera", bandera);
					String mensaje = "La clave de acceso actual no coincide con lo ingresado en el campo.";
					request.setAttribute("mensaje", mensaje);
					request.getRequestDispatcher("/vistas_alumno/cambio_clave.jsp").forward(request, response);
				}
			} catch (Exception e) {
				if(ban1==1)
				{
					new Conexion().modificarClaveUsuario(rut, "Alumno", contrasegnia);
				}
				if(ban2==1)
				{
					new Conexion().modificarClaveUsuario(rut, "Supervisor", contrasegnia);
				}
				if(ban3==1)
				{
					new Conexion().modificarClaveUsuario(rut, "Administrador", contrasegnia);
				}
				if(ban4==1)
				{
					new Conexion().modificarClaveUsuario(rut, "Academico", contrasegnia);
				}
			}
		}
	}

}
