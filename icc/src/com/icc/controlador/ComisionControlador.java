package com.icc.controlador;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.icc.modelo.Academico;
import com.icc.modelo.Alumno;
import com.icc.modelo.Empresa;
//import com.icc.modelo.Evaluadores;
//import com.icc.modelo.Instrumento;
import com.icc.modelo.Practica;
import com.icc.modelo.Supervisor;
import com.icc.utilidades.Conexion;

/**
 * Servlet implementation class ComisionControlador
 */
@WebServlet("/ComisionControlador")
public class ComisionControlador extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ComisionControlador() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("doGet ComisionControlador");
		// muestra las gestiones a las que se le puede asignar comision
		String tipo = (String) request.getSession().getAttribute("tipo");
		//String rut = (String) request.getSession().getAttribute("rut");
		ArrayList<Supervisor> supervisores = new ArrayList<>();
		ArrayList<Academico> profesores_guias = new ArrayList<>();
		ArrayList<Empresa> empresas = new ArrayList<>();		
		ArrayList<Practica> practicas = new ArrayList<>();
		ArrayList<Alumno> alumnos = new ArrayList<>();
		
		if(tipo == "admin" || tipo == "administrador-academico"){
			practicas = new Conexion().getPracticasFinalizadas(tipo);//practicas finalizadas + informe subido
			for(int i=0; i< practicas.size(); i++) {
	            supervisores.add(new Conexion().getSupervisor(practicas.get(i).getSup_rut()));
	            profesores_guias.add(new Conexion().getAcademico(practicas.get(i).getAca_rut()));
	            alumnos.add(new Conexion().getDatosAlumno(practicas.get(i).getAlu_rut()));
	            empresas.add(new Conexion().getEmpresa(practicas.get(i).getEmp_rut()));
	        }
			
			System.out.println("\nPracticas Finalizadas = "+practicas.size()+" | Supervisores = "+supervisores.size()+" | Profesores Guia = "+profesores_guias.size()
								+" | Alumnos = "+alumnos.size()+" | Empresas = "+empresas.size());
			
			
			request.setAttribute("academicosList", new Conexion().getAcademicos());
			request.setAttribute("practicasList", practicas);
			request.setAttribute("supervisoresList", supervisores);
			request.setAttribute("profesoresList", profesores_guias);
			request.setAttribute("alumnosList", alumnos);
			request.setAttribute("empresasList", empresas);
			request.setAttribute("asignaturasList", new Conexion().getGestiones());
			request.getRequestDispatcher("/vistas_admin/asignar_comision.jsp").forward(request, response);
			
		}
		if(tipo == "alumno"){

		}
		if(tipo == "supervisor"){
			
		}
		if(tipo == "academico"){

		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// metodo post que registra una comision
		String rut_lector = request.getParameter("lector");
		int rol_id_lector = Integer.parseInt(request.getParameter("rol_bitacora"));
		int pra_id = Integer.parseInt(request.getParameter("pra_id"));		
		
		try {
			new Conexion().crearProfesorLectorEvaluacion(rut_lector, pra_id, rol_id_lector);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}		
		
		if(!request.getParameter("rol_comision").equals("0")){//si es gestion profesional obtener rut academicos comisión + id del rol de comision
			int rol_id_comision = Integer.parseInt(request.getParameter("rol_comision"));
			//ArrayList<String> rut_comision = new ArrayList<>();
			String[] comision = request.getParameterValues("comision[]");//recibe el select multiple
			for (int i = 0; i < comision.length; i++) {
				System.out.println("Academico comisión:"+comision[i]);
				try {
					new Conexion().crearProfesorComisionEvaluacion(comision[i], pra_id, rol_id_comision);
					
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		//update de estado práctica a comisionada
		new Conexion().updatePracticaComisionada(pra_id);		
		
		//redireccion
		String tipo = (String) request.getSession().getAttribute("tipo");
		//String rut = (String) request.getSession().getAttribute("rut");
		ArrayList<Supervisor> supervisores = new ArrayList<>();
		ArrayList<Academico> profesores_guias = new ArrayList<>();
		ArrayList<Empresa> empresas = new ArrayList<>();		
		ArrayList<Practica> practicas = new ArrayList<>();
		ArrayList<Alumno> alumnos = new ArrayList<>();
		
		if(tipo == "admin" || tipo.equals("administrador-academico")){
			practicas = new Conexion().getPracticasFinalizadas(tipo);
			for(int i=0; i< practicas.size(); i++) {
	            supervisores.add(new Conexion().getSupervisor(practicas.get(i).getSup_rut()));
	            profesores_guias.add(new Conexion().getAcademico(practicas.get(i).getAca_rut()));
	            alumnos.add(new Conexion().getDatosAlumno(practicas.get(i).getAlu_rut()));
	            empresas.add(new Conexion().getEmpresa(practicas.get(i).getEmp_rut()));
	        }
			
			System.out.println("\nPracticas Finalizadas = "+practicas.size()+" | Supervisores = "+supervisores.size()+" | Profesores Guia = "+profesores_guias.size()
								+" | Alumnos = "+alumnos.size()+" | Empresas = "+empresas.size());
			
			
			request.setAttribute("academicosList", new Conexion().getAcademicos());
			request.setAttribute("practicasList", practicas);
			request.setAttribute("supervisoresList", supervisores);
			request.setAttribute("profesoresList", profesores_guias);
			request.setAttribute("alumnosList", alumnos);
			request.setAttribute("empresasList", empresas);
			request.setAttribute("asignaturasList", new Conexion().getGestiones());
			request.getRequestDispatcher("/vistas_admin/asignar_comision.jsp").forward(request, response);
			
		}
		if(tipo == "alumno"){

		}
		if(tipo == "supervisor"){
			
		}
		if(tipo == "academico"){

		}
	}

}
