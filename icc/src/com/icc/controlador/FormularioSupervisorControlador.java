package com.icc.controlador;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.icc.modelo.Supervisor;
import com.icc.utilidades.Conexion;
import com.icc.utilidades.EmailUtilidades;
import com.icc.utilidades.PasswordGenerador;

/**
 * Servlet implementation class FormularioSupervisorControlador
 */
@WebServlet("/FormularioSupervisorControlador")
public class FormularioSupervisorControlador extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private String host;
	private String port;
	private String user;
	private String pass;  
	private String mensaje ="";
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FormularioSupervisorControlador() {
        super();
    }
    
    public void init() {
		// reads SMTP server setting from web.xml file
		ServletContext context = getServletContext();
		host = context.getInitParameter("host");
		port = context.getInitParameter("port");
		user = context.getInitParameter("user");
		pass = context.getInitParameter("pass");
	}
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("SupervisoresList",new Conexion().getSupervisores() );
		request.getRequestDispatcher("/vistas_admin/supervisores.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String rut = request.getParameter("rut");
		String password = PasswordGenerador.getPassword(
						  PasswordGenerador.MINUSCULAS+
						  PasswordGenerador.MAYUSCULAS+
						  PasswordGenerador.NUMEROS,10);
		String nombres = request.getParameter("nombre");
		String paterno = request.getParameter("paterno");
		String materno = request.getParameter("materno");
		String email = request.getParameter("email");
		String telefono = request.getParameter("telefono");
		String celular = request.getParameter("celular");
		String profesion = request.getParameter("profesion");
		String cargo = request.getParameter("cargo");
		String estado = request.getParameter("estado");
		String empresa = request.getParameter("empresa");
		String enunciado = "";
		int ban1=1;
		int ban2=1;
		int ban3=1;
		int exito = 0;
		if(new Conexion().verificar_usuario(rut, "Academico",0) == 1)
		{
			try {
				password = new Conexion().getClave(rut, "Academico");
			} catch (SQLException e) {
				e.printStackTrace();
			}
			ban1=0;
		}
		if(new Conexion().verificar_usuario(rut, "Alumno",0) == 1)
		{
			try {
				password = new Conexion().getClave(rut, "Alumno");
			} catch (SQLException e) {
				e.printStackTrace();
			}
			ban2=0;
		}
		if(new Conexion().verificar_usuario(rut, "Administrador",0) == 1 )
		{
			try {
				password = new Conexion().getClave(rut, "Administrador");
			} catch (SQLException e) {
				e.printStackTrace();
			}
			ban3=0;
		}
		try 
		{
			Supervisor sup = new Supervisor(rut,password,nombres,paterno,materno,email,telefono,celular,profesion,cargo,estado);		
			if(ban1==0 || ban2==0 || ban3==0)
			{
				enunciado = "Buen día "+nombres+" "+paterno+" "+materno+" se ha agregado a su cuenta el perfil de tipo Supervisor "
						+ "en el Sistema de Control y Seguimiento de Prácticas Profesionales y Operativas "
						+ "de la Escuela de Ingeniería en Construcción de la Universidad del Bío-Bío,  para volver a hacer uso del sistema recuerde ingresar al siguiente enlace http://146.83.193.68:8080/icc/ y auntentifiquese "
						+ "con su rut: "+rut+" y su antigua clave de acceso (Fue entregada al registrarlo en el sistema en el perfil anterior). Esperamos que tenga una buena semana. "
						+ "consultas a este mismo correo, atentamente el administrador ";
			}else{
				enunciado = "Bienvenido "+nombres+" "+paterno+" "+materno+" al Sistema de Control y seguimiento de prácticas Profesionales y Operativas "
						+ "de la Escuela de Ingeniería en Construcción de la Universidad del Bío-Bío, su perfil actual es de tipo Supervisor y para hacer uso del sistema ingrese al siguiente enlace http://146.83.193.68:8080/icc/ y auntentifiquese "
						+ "con su rut: "+rut+" y su clave de acceso: "+password+". \n Una vez logueado le aconsejamos cambiar su clave de acceso, "
						+ "esperamos que tenga una buena semana. "
						+ "consultas a este mismo correo, atentamente el administrador.";
			}
			String asunto ="Bienvenido "+nombres+" "+paterno+" "+materno+"";
			request.setAttribute("asunto", asunto);
			request.setAttribute("email",email);
			request.setAttribute("enunciado", enunciado);
			
			if(new Conexion().agregarSupervisor(sup) == 1)
			{
				new Conexion().agregarHistorialSupervisor(empresa,sup.getSup_rut());
				/* estos 3 if son para controlar a futuro que no haya un supervisor con perfil alumno, academico o admin
				si llegase a ocurrir alguno de los casos , los perfiles existentes se vuelven inactivos.
				*/
				
				try {
					EmailUtilidades.sendEmailSupervisor(host, port, user, pass, email, asunto,
							enunciado);
					
					// se cambian los estados de los perfiles, esto solo pasará muy rara vez , una vez avanzado el uso del sw
					if(ban1==0)
					{
						new Conexion().ModificarEstadoUsuario(rut,"inactivo","Academico");
					}
					if(ban2==0)
					{
						new Conexion().ModificarEstadoUsuario(rut,"inactivo","Alumno");
					}
					if(ban3==0)
					{
						new Conexion().ModificarEstadoUsuario(rut,"inactivo","Administrador");
					}
					
					mensaje = "El supervisor fue agregado al sistema exitosamente "
					+ "y se le envío un correo con su clave de acceso.";
					exito = 1;
					request.setAttribute("exito", exito);
				} catch (Exception ex) {
					ex.printStackTrace();
					mensaje = "Hubo un error al enviar: " + ex.getMessage();
					exito = 0;
					request.setAttribute("exito", exito);
					new Conexion().eliminarSupervisor(rut);
				} finally {
					request.setAttribute("mensaje", mensaje);
					request.setAttribute("SupervisoresList",new Conexion().getSupervisores() );
					getServletContext().getRequestDispatcher("/vistas_admin/supervisores.jsp").forward(request, response);
				}
			}	
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

}
