package com.icc.controlador;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.icc.modelo.Area;
import com.icc.utilidades.Conexion;

/**
 * Servlet implementation class FormularioAreaControlador
 */
@WebServlet("/FormularioAreaControlador")
public class FormularioAreaControlador extends HttpServlet {
	private static final long serialVersionUID = 1L;
    String mensaje="";
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FormularioAreaControlador() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("areasList",new Conexion().getAreas() );
		request.getRequestDispatcher("/vistas_admin/areas.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		String nombre = request.getParameter("nombre");
		int exito = 0;
		try {	
			int i = new Conexion().agregarArea(new Area(nombre));
			if(i == 1)
			{
				mensaje = "La Area fue registrada correctamente";
				request.setAttribute("mensaje", mensaje);
				exito = 1;
				request.setAttribute("exito", exito);
				request.setAttribute("areasList", new Conexion().getAreas());
				request.getRequestDispatcher("vistas_admin/areas.jsp").forward(request, response);
			}
			else{
				mensaje = "Hubo un error al registrar la nueva Area";
				request.setAttribute("mensaje", mensaje);
				exito = 0;
				request.setAttribute("exito", exito);
				request.setAttribute("areasList", new Conexion().getAreas());
				request.getRequestDispatcher("vistas_admin/areas.jsp").forward(request, response);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
