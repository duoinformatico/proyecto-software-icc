package com.icc.controlador;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.icc.modelo.Area;
import com.icc.utilidades.Conexion;

/**
 * Servlet implementation class ModificarEspecialidadControlador
 */
@WebServlet("/ModificarEspecialidadControlador")
public class ModificarEspecialidadControlador extends HttpServlet {
	private static final long serialVersionUID = 1L;
    String mensaje="";    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ModificarEspecialidadControlador() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String esp_id = request.getParameter("esp_id");
		request.setAttribute("id", esp_id);
		request.setAttribute("especialidad", new Conexion().getEspecialidad(Integer.parseInt(esp_id)));
		request.getRequestDispatcher("ajax/modificar_especialidad.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("esp_id_m");
		String nombre = request.getParameter("nombre_m");
		int exito = 0;
		try {	
			int i = new Conexion().ModificarDatosArea(new Area(Integer.parseInt(id),nombre));
			if(i == 1)
			{
				mensaje = "Los datos de la especialidad fueron modificados correctamente";
				request.setAttribute("mensaje", mensaje);
				exito = 1;
				request.setAttribute("exito", exito);
				request.setAttribute("especialidadList", new Conexion().getEspecialidades());
				request.getRequestDispatcher("vistas_admin/especialidades.jsp").forward(request, response);
			}
			else{
				mensaje = "Hubo un error al modificar los datos de especialidad";
				request.setAttribute("mensaje", mensaje);
				exito = 0;
				request.setAttribute("exito", exito);
				request.setAttribute("especialidadList", new Conexion().getEspecialidades());
				request.getRequestDispatcher("vistas_admin/especialidades.jsp").forward(request, response);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
