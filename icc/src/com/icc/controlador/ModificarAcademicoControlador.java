package com.icc.controlador;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.icc.modelo.Academico;
import com.icc.utilidades.Conexion;


/**
 * Servlet implementation class ModificarAcademicoControlador
 */
@WebServlet("/ModificarAcademicoControlador")
public class ModificarAcademicoControlador extends HttpServlet {
	private static final long serialVersionUID = 1L;
    String mensaje="";
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ModificarAcademicoControlador() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Academico academico = new Conexion().getAcademico(request.getParameter("aca_rut"));
		request.setAttribute("academico", academico);
		if(academico.getAca_telefono() == null || academico.getAca_telefono().equals("") || academico.getAca_telefono().equals("''"))
		{
			int bandera = 1;
			request.setAttribute("bandera", bandera);
		}else{
			String codigo = academico.getAca_telefono().charAt(4)+""+academico.getAca_telefono().charAt(5);
			request.setAttribute("codigo", codigo);
			String telefono = "";
			for(int i=0;i<academico.getAca_telefono().length();i++)
			{
				if(i>6)
				{
					telefono= telefono+""+academico.getAca_telefono().charAt(i);
				}
			}
			int bandera = 0;
			request.setAttribute("bandera", bandera);
			request.setAttribute("telefono", telefono);
		}
		request.setAttribute("departamento", new Conexion().getDepartamento(academico.getDep_id()));
		request.getRequestDispatcher("ajax/modificar_academico.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String rut = request.getParameter("rut_modif");
		String dep_id = request.getParameter("departamento_modif");
		String nombres = request.getParameter("nombre_modif");
		String paterno = request.getParameter("paterno_modif");
		String materno = request.getParameter("materno_modif");
		String email = request.getParameter("email_modif");
		String telefono = request.getParameter("telefono_modif");
		String celular = request.getParameter("celular_modif");
		String estado = request.getParameter("estado_modif");
		int exito = 0;
		try
		{
			int i =new Conexion().ModificarDatosAcademico(new Academico(rut,Integer.parseInt(dep_id),nombres,paterno,materno,email,telefono,celular,estado),1);
			if(i==1)
			{
				mensaje = "La información del académico se Modificó correctamente";	
				request.setAttribute("mensaje", mensaje);
				exito = 1;
				request.setAttribute("exito", exito);
				request.getRequestDispatcher("menuAcademicoServlet.do").forward(request, response);
			}
			else{
				mensaje= "Hubo un error al modificar la información del académico ";
				request.setAttribute("mensaje", mensaje);
				exito = 0;
				request.setAttribute("exito", exito);
				request.getRequestDispatcher("menuAcademicoServlet.do").forward(request, response);
			}
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

}
