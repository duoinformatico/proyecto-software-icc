package com.icc.controlador;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.icc.modelo.Especialidad;
import com.icc.utilidades.Conexion;

/**
 * Servlet implementation class FormularioEspecialidadesControlador
 */
@WebServlet("/FormularioEspecialidadesControlador")
public class FormularioEspecialidadesControlador extends HttpServlet {
	private static final long serialVersionUID = 1L;
    String mensaje="";
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FormularioEspecialidadesControlador() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("especialidadList", new Conexion().getEspecialidades());
		request.getRequestDispatcher("/vistas_admin/especialidades.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String nombre = request.getParameter("nombre");
		int exito = 0;
		try {	
			int i = new Conexion().agregarEspecialidad(new Especialidad(nombre));
			if(i == 1)
			{
				mensaje = "La especialidad fue registrada correctamente";
				request.setAttribute("mensaje", mensaje);
				exito = 1;
				request.setAttribute("exito", exito);
				request.setAttribute("especialidadList", new Conexion().getEspecialidades());
				request.getRequestDispatcher("vistas_admin/especialidades.jsp").forward(request, response);
			}
			else{
				mensaje = "Hubo un error al registrar la nueva Especialidad";
				request.setAttribute("mensaje", mensaje);
				exito = 0;
				request.setAttribute("exito", exito);
				request.setAttribute("especialidadList", new Conexion().getEspecialidades());
				request.getRequestDispatcher("vistas_admin/especialidades.jsp").forward(request, response);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
