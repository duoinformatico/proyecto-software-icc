package com.icc.controlador;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.icc.modelo.Academico;
import com.icc.modelo.Administrador;
import com.icc.modelo.Alumno;
import com.icc.modelo.Comuna;
import com.icc.modelo.Direccion;
import com.icc.modelo.Provincia;
import com.icc.modelo.Region;
import com.icc.modelo.Supervisor;
import com.icc.utilidades.Conexion;

/**
 * Servlet implementation class PerfilAdminControlador
 */
@WebServlet("/PerfilesUsuariosControlador")
public class PerfilesUsuariosControlador extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PerfilesUsuariosControlador() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String rut = (String) request.getSession().getAttribute("rut");
		String tipo = (String) request.getSession().getAttribute("tipo");
		request.setCharacterEncoding("UTF-8");
		if(tipo == "admin")
		{
			Administrador administrador = new Conexion().getAdministrador(rut);
			request.setAttribute("administrador", administrador);
			if(administrador.getAdm_telefono() == null || administrador.getAdm_telefono().equals("") || administrador.getAdm_telefono().equals("''"))
			{
				int bandera = 1;
				request.setAttribute("bandera", bandera);
				
			}else{
				String codigo = administrador.getAdm_telefono().charAt(4)+""+administrador.getAdm_telefono().charAt(5);
				request.setAttribute("codigo", codigo);
				String telefono = "";
				for(int i=0;i<administrador.getAdm_telefono().length();i++)
				{
					if(i>6)
					{
						telefono= telefono+""+administrador.getAdm_telefono().charAt(i);
					}
				}
				int bandera = 0;
				request.setAttribute("bandera", bandera);
				request.setAttribute("telefono", telefono);
			}
			request.getRequestDispatcher("/vistas_admin/formulario_perfil.jsp").forward(request, response);
		}
		if(tipo == "administrador-academico")
		{
			Administrador administrador = new Conexion().getAdministrador(rut);
			request.setAttribute("administrador", administrador);
			if(administrador.getAdm_telefono() == null || administrador.getAdm_telefono().equals("") || administrador.getAdm_telefono().equals("''"))
			{
				int bandera = 1;
				request.setAttribute("bandera", bandera);
				
			}else{
				String codigo = administrador.getAdm_telefono().charAt(4)+""+administrador.getAdm_telefono().charAt(5);
				request.setAttribute("codigo", codigo);
				String telefono = "";
				for(int i=0;i<administrador.getAdm_telefono().length();i++)
				{
					if(i>6)
					{
						telefono= telefono+""+administrador.getAdm_telefono().charAt(i);
					}
				}
				int bandera = 0;
				request.setAttribute("bandera", bandera);
				request.setAttribute("telefono", telefono);
			}
			request.getRequestDispatcher("/vistas_admin/formulario_perfil.jsp").forward(request, response);
		}
		if(tipo == "alumno")
		{
			Alumno alumno = new Conexion().getDatosAlumno(rut);
			request.setAttribute("alumno", alumno);
			if(alumno.getAlu_telefono() == null || alumno.getAlu_telefono().equals("") || alumno.getAlu_telefono().equals("''"))
			{
				int bandera = 1;
				request.setAttribute("bandera", bandera);
				
			}else{
				String codigo = alumno.getAlu_telefono().charAt(4)+""+alumno.getAlu_telefono().charAt(5);
				request.setAttribute("codigo", codigo);
				String telefono = "";
				for(int i=0;i<alumno.getAlu_telefono().length();i++)
				{
					if(i>6)
					{
						telefono= telefono+""+alumno.getAlu_telefono().charAt(i);
					}
				}
				int bandera = 0;
				request.setAttribute("bandera", bandera);
				request.setAttribute("telefono", telefono);
			}
			int contador = new Conexion().contarDirecciones(rut);
			if(contador == 0)
			{
				request.setAttribute("contador", 0);
			}
			if(contador == 1)
			{
				request.setAttribute("contador", 1);
			}
			if(contador == 2)
			{
				ArrayList<Direccion> direccion = new Conexion().getDirecciones(rut);
				request.setAttribute("direccionList", direccion);
				for(int i=0;i<2;i++)
				{
					if(direccion.get(i).getDir_tp_id() == 1)
					{
						Comuna comuna_aca = new Conexion().getComuna(direccion.get(i).getCom_id());
						request.setAttribute("comuna_aca", comuna_aca);
						Provincia provincia_aca = new Conexion().getProvincia(comuna_aca.getPro_id());
						request.setAttribute("provincia_aca", provincia_aca);
						Region region_aca = new Conexion().getRegion(provincia_aca.getReg_id());
						request.setAttribute("region_aca", region_aca);
					}
					if(direccion.get(i).getDir_tp_id() == 2)
					{
						Comuna comuna_res = new Conexion().getComuna(direccion.get(i).getCom_id());
						request.setAttribute("comuna_res", comuna_res);
						Provincia provincia_res = new Conexion().getProvincia(comuna_res.getPro_id());
						request.setAttribute("provincia_res", provincia_res);
						Region region_res = new Conexion().getRegion(provincia_res.getReg_id());
						request.setAttribute("region_res", region_res);
					}
				}
				request.setAttribute("contador", 2);
			}		
			request.setAttribute("comunaList", new Conexion().getComunas());
			request.setAttribute("regionList", new Conexion().getRegiones());
			request.setAttribute("provinciaList", new Conexion().getProvincias());
			request.getRequestDispatcher("/vistas_alumno/formulario_perfil.jsp").forward(request, response);
		}
		if(tipo == "academico")
		{
			Academico academico = new Conexion().getAcademico(rut);
			request.setAttribute("academico", academico );
			if(academico.getAca_telefono() == null || academico.getAca_telefono().equals("") || academico.getAca_telefono().equals("''"))
			{
				int bandera = 1;
				request.setAttribute("bandera", bandera);
				
			}else{
				String codigo = academico.getAca_telefono().charAt(4)+""+academico.getAca_telefono().charAt(5);
				request.setAttribute("codigo", codigo);
				String telefono = "";
				for(int i=0;i<academico.getAca_telefono().length();i++)
				{
					if(i>6)
					{
						telefono= telefono+""+academico.getAca_telefono().charAt(i);
					}
				}
				int bandera = 0;
				request.setAttribute("bandera", bandera);
				request.setAttribute("telefono", telefono);
			}
			request.getRequestDispatcher("/vistas_academico/formulario_perfil.jsp").forward(request, response);
		
		}
		if(tipo == "supervisor")
		{
			Supervisor supervisor = new Conexion().getSupervisor(rut);
			request.setAttribute("supervisor", supervisor);
			if(supervisor.getSup_telefono() == null || supervisor.getSup_telefono().equals("") || supervisor.getSup_telefono().equals("''"))
			{
				int bandera = 1;
				request.setAttribute("bandera", bandera);
				
			}else{
				String codigo = supervisor.getSup_telefono().charAt(4)+""+supervisor.getSup_telefono().charAt(5);
				request.setAttribute("codigo", codigo);
				String telefono = "";
				for(int i=0;i<supervisor.getSup_telefono().length();i++)
				{
					if(i>6)
					{
						telefono= telefono+""+supervisor.getSup_telefono().charAt(i);
					}
				}
				int bandera = 0;
				request.setAttribute("bandera", bandera);
				request.setAttribute("telefono", telefono);
			}
			request.getRequestDispatcher("/vistas_supervisor/formulario_perfil.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String rut = (String) request.getSession().getAttribute("rut");
		String tipo = (String) request.getSession().getAttribute("tipo");
		request.setCharacterEncoding("UTF-8");
		if(tipo == "admin")
		{
			String telefono = request.getParameter("telefono");
			String celular = request.getParameter("celular");
			String cargo = request.getParameter("cargo");
			try 
			{
				
				int i = new Conexion().ModificarDatosAdministrador(new Administrador(rut,telefono,celular,cargo),2);
				if(i==1)
				{
					Administrador administrador = new Conexion().getAdministrador(rut);
					request.setAttribute("administrador", administrador);
					if(administrador.getAdm_telefono() == null || administrador.getAdm_telefono().equals("") || administrador.getAdm_telefono().equals("''"))
					{
						int bandera = 1;
						request.setAttribute("bandera", bandera);
						
					}else{
						String codigo = administrador.getAdm_telefono().charAt(4)+""+administrador.getAdm_telefono().charAt(5);
						request.setAttribute("codigo", codigo);
						String telefono1 = "";
						for(int i1=0;i1<administrador.getAdm_telefono().length();i1++)
						{
							if(i1>6)
							{
								telefono1= telefono1+""+administrador.getAdm_telefono().charAt(i1);
							}
						}
						int bandera = 0;
						request.setAttribute("bandera", bandera);
						request.setAttribute("telefono", telefono1);
					}
					request.setAttribute("exito", 1);
					request.getRequestDispatcher("/vistas_admin/formulario_perfil.jsp").forward(request, response);
				}
				
			}catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		if(tipo == "administrador-academico")
		{
			String telefono = request.getParameter("telefono");
			String celular = request.getParameter("celular");
			String cargo = request.getParameter("cargo");
			try 
			{
				
				int i = new Conexion().ModificarDatosAdministrador(new Administrador(rut,telefono,celular,cargo),2);
				int j = new Conexion().ModificarDatosAcademico(new Academico(rut,telefono,celular),2);
				if(i==1 && j==1)
				{
					Administrador administrador = new Conexion().getAdministrador(rut);
					request.setAttribute("administrador", administrador);
					if(administrador.getAdm_telefono() == null || administrador.getAdm_telefono().equals("") || administrador.getAdm_telefono().equals("''"))
					{
						int bandera = 1;
						request.setAttribute("bandera", bandera);
						
					}else{
						String codigo = administrador.getAdm_telefono().charAt(4)+""+administrador.getAdm_telefono().charAt(5);
						request.setAttribute("codigo", codigo);
						String telefono1 = "";
						for(int i1=0;i1<administrador.getAdm_telefono().length();i1++)
						{
							if(i1>6)
							{
								telefono1= telefono1+""+administrador.getAdm_telefono().charAt(i1);
							}
						}
						int bandera = 0;
						request.setAttribute("bandera", bandera);
						request.setAttribute("telefono", telefono1);
					}
					request.setAttribute("exito", 1);
					request.getRequestDispatcher("/vistas_admin/formulario_perfil.jsp").forward(request, response);
				}
				
			}catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		
		if(tipo == "alumno")
		{
			String telefono = request.getParameter("telefono");
			String celular = request.getParameter("celular");
			String academica = request.getParameter("academica");
			String residencial = request.getParameter("residencial");
			String comuna = request.getParameter("comuna");
			String comuna_2 = request.getParameter("comuna_2");
			try 
			{
				int i = new Conexion().ModificarDatosAlumno(new Alumno(rut,celular,telefono));
				int j = new Conexion().ModificarDireccionesAlumno(rut,academica,Integer.parseInt(comuna),1);
				int k = new Conexion().ModificarDireccionesAlumno(rut,residencial,Integer.parseInt(comuna_2),2);
				
				if(i==1 && j==1 && k==1)
				{
					Alumno alumno = new Conexion().getDatosAlumno(rut);
					request.setAttribute("alumno", alumno);
					if(alumno.getAlu_telefono() == null || alumno.getAlu_telefono().equals("") || alumno.getAlu_telefono().equals("''"))
					{
						int bandera = 1;
						request.setAttribute("bandera", bandera);
						
					}else{
						String codigo = alumno.getAlu_telefono().charAt(4)+""+alumno.getAlu_telefono().charAt(5);
						request.setAttribute("codigo", codigo);
						String telefono_get = "";
						for(int l=0;l<alumno.getAlu_telefono().length();l++)
						{
							if(l>6)
							{
								telefono_get= telefono_get+""+alumno.getAlu_telefono().charAt(i);
							}
						}
						int bandera = 0;
						request.setAttribute("bandera", bandera);
						request.setAttribute("telefono", telefono_get);
					}
					int contador = new Conexion().contarDirecciones(rut);
					if(contador == 0)
					{
						request.setAttribute("contador", 0);
					}
					if(contador == 1)
					{
						request.setAttribute("contador", 1);
					}
					if(contador == 2)
					{
						ArrayList<Direccion> direccion = new Conexion().getDirecciones(rut);
						request.setAttribute("direccionList", direccion);
						for(int i2=0;i2<2;i2++)
						{
							if(direccion.get(i2).getDir_tp_id() == 1)
							{
								Comuna comuna_aca = new Conexion().getComuna(direccion.get(i2).getCom_id());
								request.setAttribute("comuna_aca", comuna_aca);
								Provincia provincia_aca = new Conexion().getProvincia(comuna_aca.getPro_id());
								request.setAttribute("provincia_aca", provincia_aca);
								Region region_aca = new Conexion().getRegion(provincia_aca.getReg_id());
								request.setAttribute("region_aca", region_aca);
							}
							if(direccion.get(i2).getDir_tp_id() == 2)
							{
								Comuna comuna_res = new Conexion().getComuna(direccion.get(i2).getCom_id());
								request.setAttribute("comuna_res", comuna_res);
								Provincia provincia_res = new Conexion().getProvincia(comuna_res.getPro_id());
								request.setAttribute("provincia_res", provincia_res);
								Region region_res = new Conexion().getRegion(provincia_res.getReg_id());
								request.setAttribute("region_res", region_res);
							}
						}
						request.setAttribute("contador", 2);
					}
					request.setAttribute("exito", 1);
					request.setAttribute("comunaList", new Conexion().getComunas());
					request.setAttribute("regionList", new Conexion().getRegiones());
					request.setAttribute("provinciaList", new Conexion().getProvincias());
					request.getRequestDispatcher("/vistas_alumno/formulario_perfil.jsp").forward(request, response);
				}
				
			}catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		if(tipo == "academico")
		{
			String telefono = request.getParameter("telefono");
			String celular = request.getParameter("celular");
			try 
			{
				int i = new Conexion().ModificarDatosAcademico(new Academico(rut,telefono,celular),2);
				if(i==1)
				{
					request.setAttribute("exito", 1);
					Academico academico = new Conexion().getAcademico(rut);
					request.setAttribute("academico", academico );
					if(academico.getAca_telefono() == null || academico.getAca_telefono().equals("") || academico.getAca_telefono().equals("''"))
					{
						int bandera = 1;
						request.setAttribute("bandera", bandera);
						
					}else{
						String codigo = academico.getAca_telefono().charAt(4)+""+academico.getAca_telefono().charAt(5);
						request.setAttribute("codigo", codigo);
						String telefono1 = "";
						for(int i1=0;i1<academico.getAca_telefono().length();i1++)
						{
							if(i1>6)
							{
								telefono1= telefono1+""+academico.getAca_telefono().charAt(i1);
							}
						}
						int bandera = 0;
						request.setAttribute("bandera", bandera);
						request.setAttribute("telefono", telefono1);
					}
					request.getRequestDispatcher("/vistas_academico/formulario_perfil.jsp").forward(request, response);
				}
				
			}catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		if(tipo == "supervisor")
		{
			String telefono = request.getParameter("telefono");
			String celular = request.getParameter("celular");
			String profesion = request.getParameter("profesion");
			String cargo = request.getParameter("cargo");
			try 
			{
				int i = new Conexion().ModificarDatosSupervisor(new Supervisor(rut,telefono,celular,profesion,cargo),2);
				if(i==1)
				{
					request.setAttribute("exito", 1);
					Supervisor supervisor = new Conexion().getSupervisor(rut);
					request.setAttribute("supervisor", supervisor);
					if(supervisor.getSup_telefono() == null || supervisor.getSup_telefono().equals("") || supervisor.getSup_telefono().equals("''"))
					{
						int bandera = 1;
						request.setAttribute("bandera", bandera);
						
					}else{
						String codigo = supervisor.getSup_telefono().charAt(4)+""+supervisor.getSup_telefono().charAt(5);
						request.setAttribute("codigo", codigo);
						String telefono1 = "";
						for(int i1=0;i1<supervisor.getSup_telefono().length();i1++)
						{
							if(i1>6)
							{
								telefono1= telefono1+""+supervisor.getSup_telefono().charAt(i1);
							}
						}
						int bandera = 0;
						request.setAttribute("bandera", bandera);
						request.setAttribute("telefono", telefono1);
					}
					request.getRequestDispatcher("/vistas_supervisor/formulario_perfil.jsp").forward(request, response);
				}
				
			}catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		
		
	}

}
