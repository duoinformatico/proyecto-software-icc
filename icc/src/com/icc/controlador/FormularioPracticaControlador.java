package com.icc.controlador;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.icc.modelo.Comuna;
import com.icc.modelo.EvaluacionAcademico;
import com.icc.modelo.Practica;
import com.icc.modelo.Provincia;
import com.icc.modelo.Region;
import com.icc.utilidades.Conexion;

/**
 * Servlet implementation class FormularioPracticaControlador
 */
@WebServlet("/FormularioPracticaControlador")
public class FormularioPracticaControlador extends HttpServlet {
	private static final long serialVersionUID = 1L;
     String mensaje="";
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FormularioPracticaControlador() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("pra_id");
		try {	
			Practica practica = new Conexion().getPractica(Integer.parseInt(id));
			request.setAttribute("id", id);
			request.setAttribute("supervisor", new Conexion().getSupervisor(practica.getSup_rut()));
			request.setAttribute("profesor", new Conexion().getAcademico(practica.getAca_rut()));
			request.setAttribute("profesorList", new Conexion().getAcademicos());
			request.setAttribute("alumno", new Conexion().getDatosAlumno(practica.getAlu_rut()));
			request.setAttribute("practica", practica);
			request.setAttribute("empresaList", new Conexion().getEmpresas());
			Comuna comuna = new Conexion().getComuna(practica.getCom_id());
			request.setAttribute("comuna", comuna);
			Provincia provincia = new Conexion().getProvincia(comuna.getPro_id());
			request.setAttribute("provincia", provincia);
			Region region = new Conexion().getRegion(provincia.getReg_id());
			request.setAttribute("region", region);
			request.setAttribute("comunaList", new Conexion().getComunas());
			request.setAttribute("regionList", new Conexion().getRegiones());
			request.setAttribute("provinciaList", new Conexion().getProvincias());
			request.setAttribute("gestionesList", new Conexion().getGestiones());
			request.setAttribute("obrasList", new Conexion().getObras());
			request.setAttribute("planesList", new Conexion().getPlanes());
			request.getRequestDispatcher("ajax/formulario_practica.jsp").forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		String id = request.getParameter("id");
		String empresa = request.getParameter("empresa");
		String profesor = request.getParameter("profesor");
		String profesor_actual = request.getParameter("profesor_actual");
		String supervisor = request.getParameter("supervisor");
		String obra = request.getParameter("obra");
		String alumno = request.getParameter("alumno");
		String asignatura = request.getParameter("asignatura");
		String plan = request.getParameter("plan");
		String fecha_ins = request.getParameter("fecha_ins");
		String nombre_obra = request.getParameter("obra_nombre");
		String comuna= request.getParameter("comuna");
		String direccion= request.getParameter("direccion");
		String tareas = request.getParameter("tareas");
		String inicio = request.getParameter("inicio");
		String termino = request.getParameter("termino");
		String horas = request.getParameter("horas");
		String estado = request.getParameter("estado");
		int exito = 0;
		try
		{	
			int i = new Conexion().ModificarPractica(new Practica(Integer.parseInt(id),empresa,profesor,supervisor,Integer.parseInt(comuna),Integer.parseInt(obra),
					alumno,Integer.parseInt(asignatura),Integer.parseInt(plan),fecha_ins,nombre_obra,direccion,tareas,inicio,termino
					,Integer.parseInt(horas),estado),2);
			if(i == 1)
			{
				if(!profesor_actual.equals(profesor))
				{
					EvaluacionAcademico evaluacion = new Conexion().getEvaluacion(Integer.parseInt(id),profesor_actual);
					int j = new Conexion().modificarEvaluador(profesor_actual,profesor,Integer.parseInt(id),evaluacion.getFecha(),evaluacion.getRol_id());
					if(j == 1)
					{
						mensaje = "Los datos de la práctica fueron Modificados correctamente";
						request.setAttribute("mensaje", mensaje);
						exito = 1;
						request.setAttribute("exito", exito);
						request.getRequestDispatcher("MenuPracticas").forward(request, response);
					}
				}else{
					mensaje = "Los datos de la práctica fueron Modificados correctamente";
					request.setAttribute("mensaje", mensaje);
					exito = 1;
					request.setAttribute("exito", exito);
					request.getRequestDispatcher("MenuPracticas").forward(request, response);
				}
			}
			else{
				mensaje = "Hubo un error al Modificar los datos de la práctica";
				request.setAttribute("mensaje", mensaje);
				exito = 0;
				request.setAttribute("exito", exito);
				request.getRequestDispatcher("MenuPracticas").forward(request, response);
			}
		} catch (Exception e) 
		{
			e.printStackTrace();
			mensaje = "Hubo un error al Modificar los datos de la práctica";
			request.setAttribute("mensaje", mensaje);
			exito = 0;
			request.setAttribute("exito", exito);
			request.getRequestDispatcher("MenuPracticas").forward(request, response);
		}
	}
}
