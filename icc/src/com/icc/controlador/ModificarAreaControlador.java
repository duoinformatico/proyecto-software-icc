package com.icc.controlador;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.icc.modelo.Area;
import com.icc.utilidades.Conexion;

/**
 * Servlet implementation class ModificarAreaControlador
 */
@WebServlet("/ModificarAreaControlador")
public class ModificarAreaControlador extends HttpServlet {
	private static final long serialVersionUID = 1L;
    String mensaje="";
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ModificarAreaControlador() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String area_id = request.getParameter("area_id");
		request.setAttribute("id", area_id);
		request.setAttribute("area", new Conexion().getArea(Integer.parseInt(area_id)));
		request.getRequestDispatcher("ajax/modificar_area.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("area_id_m");
		String nombre = request.getParameter("nombre_m");
		int exito = 0;
		try {	
			int i = new Conexion().ModificarDatosArea(new Area(Integer.parseInt(id),nombre));
			if(i == 1)
			{
				mensaje = "Los datos del Area fueron modificados correctamente";
				request.setAttribute("mensaje", mensaje);
				exito = 1;
				request.setAttribute("exito", exito);
				request.setAttribute("areasList", new Conexion().getAreas());
				request.getRequestDispatcher("vistas_admin/areas.jsp").forward(request, response);
			}
			else{
				mensaje = "Hubo un error al modificar los datos del Area";
				request.setAttribute("mensaje", mensaje);
				exito = 0;
				request.setAttribute("exito", exito);
				request.setAttribute("areasList", new Conexion().getAreas());
				request.getRequestDispatcher("vistas_admin/areas.jsp").forward(request, response);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
