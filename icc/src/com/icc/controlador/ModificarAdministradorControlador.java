package com.icc.controlador;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.icc.modelo.Administrador;
import com.icc.utilidades.Conexion;

/**
 * Servlet implementation class ModificarAdministradorControlador
 */
@WebServlet("/ModificarAdministradorControlador")
public class ModificarAdministradorControlador extends HttpServlet {
	private static final long serialVersionUID = 1L;
    String mensaje="";
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ModificarAdministradorControlador() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Administrador administrador = new Conexion().getAdministrador(request.getParameter("adm_rut"));
		request.setAttribute("administrador", administrador);
		if(administrador.getAdm_telefono() == null || administrador.getAdm_telefono().equals("") || administrador.getAdm_telefono().equals("''"))
		{
			int bandera = 1;
			request.setAttribute("bandera", bandera);
		}else{
			String codigo = administrador.getAdm_telefono().charAt(4)+""+administrador.getAdm_telefono().charAt(5);
			request.setAttribute("codigo", codigo);
			String telefono = "";
			for(int i=0;i<administrador.getAdm_telefono().length();i++)
			{
				if(i>6)
				{
					telefono= telefono+""+administrador.getAdm_telefono().charAt(i);
				}
			}
			int bandera = 0;
			request.setAttribute("bandera", bandera);
			request.setAttribute("telefono", telefono);
		}
		
		request.getRequestDispatcher("ajax/modificar_administrador.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String rut = request.getParameter("rut_modif");
		String nombres = request.getParameter("nombre_modif");
		String paterno = request.getParameter("paterno_modif");
		String materno = request.getParameter("materno_modif");
		String email = request.getParameter("email_modif");
		String telefono = request.getParameter("telefono_modif");
		String celular = request.getParameter("celular_modif");
		String cargo = request.getParameter("cargo_modif");
		String estado = request.getParameter("estado_modif");
		int exito = 0;
		try {	
			int i = new Conexion().ModificarDatosAdministrador(new Administrador(rut,nombres,paterno,materno,email,telefono,celular,cargo,estado),1);
			if(i == 1)
			{
				mensaje = "El  Administrador fue modificado correctamente";
				request.setAttribute("mensaje", mensaje);
				exito = 1;
				request.setAttribute("exito", exito);
				request.getRequestDispatcher("menuAdministradorServlet.do").forward(request, response);
			}
			else{
				mensaje = "Hubo un error al registrar el nuevo Administrador";
				request.setAttribute("mensaje", mensaje);
				exito = 0;
				request.setAttribute("exito", exito);
				request.getRequestDispatcher("menuAdministradorServlet.do").forward(request, response);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
