package com.icc.controlador;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.icc.modelo.Asignatura;
import com.icc.modelo.Categoria;
import com.icc.modelo.Escala;
import com.icc.modelo.RolGestion;
import com.icc.utilidades.Conexion;

/**
 * Servlet implementation class MenuRubricaControlador
 */
@WebServlet("/MenuRubricaControlador")
public class MenuRubricaControlador extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MenuRubricaControlador() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//Consultar por Escalas
		ArrayList<Escala>escalas = new ArrayList<>();
		escalas = new Conexion().getEscala();
		request.setAttribute("escalasList", escalas);
		
		//Consultar por Categorias
		ArrayList<Categoria>categorias= new ArrayList<>();
		categorias = new Conexion().getCategorias();
		request.setAttribute("categoriasList", categorias);
		
		//Consultar por Roles
		ArrayList<RolGestion>roles= new ArrayList<>();
		roles = new Conexion().getRoles();
		request.setAttribute("rolesList", roles);
		
		//Consultar por Asignaturas de Tipo Práctica
		ArrayList<Asignatura>gestiones= new ArrayList<>();
		gestiones = new Conexion().getGestiones();
		request.setAttribute("gestionesList", gestiones);
		
		//Evaluar si existen las condiciones para crear una rúbrica
		if(escalas.size() > 0 && categorias.size() > 2 && roles.size() > 0 && gestiones.size() > 0){
			request.setAttribute("exito", 1);
			request.setAttribute("mensaje", "Actualmente es posible crear una rúbrica de Evaluación nueva para el sistema.");
		}
		else{
			request.setAttribute("exito", 0);
			request.setAttribute("mensaje", "No es posible crear una rúbrica de Evaluación.\nEl sistema no reune los requerimientos mínimos. Revise escalas, categorías, roles y asignaturas de tipo gestión.");
		}
		
		
		//consultar por asignaturas de tipo practicas profesionales + sus instrumentos
		request.setAttribute("asignaturasList", new Conexion().getInstrumentos());
		
		//Consultar por instrumentos de evaluacion
		//request.setAttribute("instrumentosList", new Conexion().getAllInstrumentos());
		
		//Se envian las carreras registradas
		
		request.setAttribute("carrerasList", new Conexion().getCarreras());
		request.getRequestDispatcher("/vistas_admin/rubricas.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Consultar por Escalas
		ArrayList<Escala>escalas = new ArrayList<>();
		escalas = new Conexion().getEscala();
		request.setAttribute("escalasList", escalas);
		
		//Consultar por Categorias
		ArrayList<Categoria>categorias= new ArrayList<>();
		categorias = new Conexion().getCategorias();
		request.setAttribute("categoriasList", categorias);
		
		//Consultar por Roles
		ArrayList<RolGestion>roles= new ArrayList<>();
		roles = new Conexion().getRoles();
		request.setAttribute("rolesList", roles);
		
		//Consultar por Asignaturas de Tipo Práctica
		ArrayList<Asignatura>gestiones= new ArrayList<>();
		gestiones = new Conexion().getGestiones();
		request.setAttribute("gestionesList", gestiones);
		
		//Evaluar si existen las condiciones para crear una rúbrica
		if(escalas.size() > 0 && categorias.size() > 2 && roles.size() > 0 && gestiones.size() > 0){
			request.setAttribute("exito", 1);
			request.setAttribute("mensaje", "Actualmente es posible crear una rúbrica de Evaluación nueva para el sistema.");
		}
		else{
			request.setAttribute("exito", 0);
			request.setAttribute("mensaje", "No es posible crear una rúbrica de Evaluación.\nEl sistema no reune los requerimientos mínimos. Revise escalas, categorías, roles y asignaturas de tipo gestión.");
		}
		
		
		//Consultar por instrumentos de evaluacion
		
		
		//Se envian las carreras registradas
		
		request.setAttribute("carrerasList", new Conexion().getCarreras());
		request.getRequestDispatcher("/vistas_admin/rubricas.jsp").forward(request, response);
	}

}
