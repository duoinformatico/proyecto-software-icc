package com.icc.controlador;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.icc.modelo.Practica;
import com.icc.utilidades.Conexion;

/**
 * Servlet implementation class MenuSeguirAlumnos
 */
@WebServlet("/MenuSeguirAlumnos")
public class MenuSeguirAlumnos extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MenuSeguirAlumnos() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String tipo = (String) request.getSession().getAttribute("tipo");
		String rut = (String) request.getSession().getAttribute("rut");
		ArrayList<Practica> practicas = new ArrayList<>();
		ArrayList<Practica> estados_informe = new ArrayList<Practica>();
		if(tipo == "supervisor")
		{
			practicas = new Conexion().getPracticasSegunRut(tipo, rut);
			request.setAttribute("practicaList", practicas);
			for(int i=0;i<practicas.size();i++)
			{
				Practica temp = new Practica(practicas.get(i).getPra_id(),new Conexion().getEstadoInforme(practicas.get(i).getPra_id()));
				estados_informe.add(temp);
			}
			request.setAttribute("estadosList", estados_informe);
			request.getRequestDispatcher("/vistas_supervisor/seguir_alumnos.jsp").forward(request, response);
		}
		if(tipo == "academico" || tipo == "administrador-academico")
		{
			practicas = new Conexion().getPracticasSegunRut(tipo, rut);
			request.setAttribute("practicaList", practicas);
			for(int i=0;i<practicas.size();i++)
			{
				Practica temp = new Practica(practicas.get(i).getPra_id(),new Conexion().getEstadoInforme(practicas.get(i).getPra_id()));
				estados_informe.add(temp);
			}
			request.setAttribute("estadosList", estados_informe);
			request.getRequestDispatcher("/vistas_academico/seguir_alumnos.jsp").forward(request, response);
		}
	}
}
