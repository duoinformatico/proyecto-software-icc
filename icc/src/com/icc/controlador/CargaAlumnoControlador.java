package com.icc.controlador;

import java.io.IOException;
import java.util.Calendar;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.icc.utilidades.Conexion;

/**
 * Servlet implementation class CargaAlumnoControlador
 */
@WebServlet("/CargaAlumnoControlador")
public class CargaAlumnoControlador extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CargaAlumnoControlador() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		Calendar calendario = Calendar.getInstance();
		Integer agnio =calendario.get(Calendar.YEAR);
		int mes = calendario.get(Calendar.MONTH);
		Integer periodo;
		// redirecciona  hacia las vistas  de carga de alumnos 
		if(mes > 7)
		{
			periodo = 2;
		}else{
			periodo = 1;
		}
		String tipo = (String) request.getSession().getAttribute("tipo");
		if(tipo == "admin" || tipo == "administrador-academico"){
			//request.setAttribute("agnio_alumno", 2012);
			//request.setAttribute("periodo_alumno", 1);
			request.setAttribute("agnio_alumno", agnio.toString());;
			request.setAttribute("periodo_alumno", periodo.toString());
			request.setAttribute("asignaturasList", new Conexion().getGestiones());
			//request.setAttribute("exito", 1);//sin mensaje
			//request.setAttribute("mensaje", "Hola nada");
			//request.setAttribute("InscripcionesList", new Conexion().getInscripcionesUBB("2014", "1"));
			request.getRequestDispatcher("/vistas_admin/conexion_ubb.jsp").forward(request, response);
		}
		
	}
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//String csvFile = "E:/archivos/inscripcion_asignaturas.txt";
		
	}

}
