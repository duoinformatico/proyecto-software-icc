package com.icc.controlador;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.icc.modelo.Plan_estudio;
import com.icc.utilidades.Conexion;

/**
 * Servlet implementation class FormularioPlanesEstudioControlador
 */
@WebServlet("/FormularioPlanesEstudioControlador")
public class FormularioPlanesEstudioControlador extends HttpServlet {
	private static final long serialVersionUID = 1L;
    String mensaje="";
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FormularioPlanesEstudioControlador() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("planesList", new Conexion().getPlanes());
		request.getRequestDispatcher("/vistas_admin/planes.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String nombre = request.getParameter("nombre");
		String carrera = request.getParameter("carrera");

		ArrayList<Integer> asignaturas = new ArrayList<Integer>();
		int etapa = Integer.parseInt(request.getParameter("etapa"));
		int total = 0;		
		
		if(etapa == 1){//etapa de la creacion del plan de estudio
			//definido nombre, carrera semestres y cantidad de asignaturas por semestres
			//envio a un nuevo formulario para crear asignaturas del plan de estudio
			int semestre = Integer.parseInt(request.getParameter("semestre"));
			for (int i = 1; i <= semestre; i++) {
				asignaturas.add(Integer.parseInt(request.getParameter("asignaturas_"+i)));
				total = total + Integer.parseInt(request.getParameter("asignaturas_"+i));
			}
			
			request.setAttribute("nombre", nombre);
			request.setAttribute("carrera", Integer.parseInt(carrera));
			request.setAttribute("semestres", semestre);
			request.setAttribute("asignaturas_semestre", asignaturas);
			request.setAttribute("max", Collections.max(asignaturas));
			request.setAttribute("total", total);
			request.setAttribute("asignatura_tipo", new Conexion().getAsignaturasTipo());
			request.getRequestDispatcher("vistas_admin/crear_plan.jsp").forward(request, response);
		}
		
		if(etapa == 2){//etapa de guardado del plan de estudio y sus asignaturas
			//envio a la vista de listado de planes de estudio	
			
			try {	
			int i = new Conexion().agregarPlan(new Plan_estudio(nombre,Integer.parseInt(carrera)));
			if(i == 1)
			{	//si el plan fue ingresado, ahora se ingresan las asignaturas
				//obtener id ultimo plan agregado
				int plan_id = new Conexion().getUltimoPlanId();
				total = Integer.parseInt(request.getParameter("total"));
				for (int j = 1; j <= total; j++) {
					new Conexion().agregarAsignatura(Integer.parseInt(request.getParameter("codigo_"+j)), plan_id, Integer.parseInt(request.getParameter("tipo_"+j)),
													request.getParameter("nombre_"+j), Integer.parseInt(request.getParameter("horas_"+j)), Integer.parseInt(request.getParameter("semestre_"+j)),
													Integer.parseInt(request.getParameter("creditos_"+j))
													);
				}
				mensaje = "El nuevo Plan fue registrado correctamente";
				request.setAttribute("mensaje", mensaje);
				request.setAttribute("exito", 1);
				request.setAttribute("planesList", new Conexion().getPlanes());
				request.getRequestDispatcher("vistas_admin/planes.jsp").forward(request, response);
			}
			else{
				mensaje = "Hubo un error al registrar el nuevo Plan";
				request.setAttribute("mensaje", mensaje);
				request.setAttribute("exito", 0);
				request.setAttribute("planesList", new Conexion().getPlanes());
				request.getRequestDispatcher("vistas_admin/planes.jsp").forward(request, response);
			}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		
		if(etapa == 3){//etapa de la creacion del plan de estudio
			//definicion de roles evaluadores y ponderaciones de practicas
			
		}
	}

}
