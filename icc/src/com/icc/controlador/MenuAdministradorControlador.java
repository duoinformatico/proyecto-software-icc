package com.icc.controlador;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.icc.utilidades.Conexion;
import com.icc.utilidades.PasswordGenerador;

/**
 * Servlet implementation class MenuAdministradorControlador
 */
@WebServlet("/MenuAdministradorControlador")
public class MenuAdministradorControlador extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MenuAdministradorControlador() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("clave", PasswordGenerador.getPassword(
				  PasswordGenerador.MINUSCULAS+
				  PasswordGenerador.MAYUSCULAS+
				  PasswordGenerador.NUMEROS,10));
		request.getRequestDispatcher("ajax/formulario_administrador.jsp").forward(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("administradoresList",new Conexion().getAdministradores() );
		request.getRequestDispatcher("/vistas_admin/administradores.jsp").forward(request, response);
	}
}
