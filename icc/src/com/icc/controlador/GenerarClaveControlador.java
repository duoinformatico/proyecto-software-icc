package com.icc.controlador;

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.icc.modelo.Academico;
import com.icc.modelo.Administrador;
import com.icc.modelo.Alumno;
import com.icc.modelo.Supervisor;
import com.icc.utilidades.Conexion;
import com.icc.utilidades.EmailUtilidades;
import com.icc.utilidades.PasswordGenerador;

/**
 * Servlet implementation class GenerarClaveControlador
 */
@WebServlet("/GenerarClaveControlador")
public class GenerarClaveControlador extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private String host;
	private String port;
	private String user;
	private String pass;
    /**
     * @see HttpServlet#HttpServlet()
     */
	public void init() {
		ServletContext context = getServletContext();
		host = context.getInitParameter("host");
		port = context.getInitParameter("port");
		user = context.getInitParameter("user");
		pass = context.getInitParameter("pass");
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		String destinatario = request.getParameter("correo");
		String asunto ="Nueva clave de Acceso Generada";
		String rut = request.getParameter("rut");
		String clave = PasswordGenerador.getPassword(
				  PasswordGenerador.MINUSCULAS+
				  PasswordGenerador.MAYUSCULAS+
				  PasswordGenerador.NUMEROS,10);
		int ban1=0;
		int ban2=0;
		int ban3=0;
		int ban4=0;
		String nombres = "";
		String paterno = "";
		String materno = "";
		String mensaje = "";
		String contrasegnia = "";
		try {
			if(new Conexion().verificar_usuario(rut, "Administrador",0)==1)
			{
				Administrador administrador = new Conexion().getAdministrador(rut);
				nombres = administrador.getAdm_nombres();
				paterno = administrador.getAdm_apellido_p();
				materno = administrador.getAdm_apellido_m();
				contrasegnia = new Conexion().getClave(rut,"Administrador");
				new Conexion().modificarClaveUsuario(rut, "Administrador", clave);
				ban1=1;
			}
			if(new Conexion().verificar_usuario(rut, "Academico",0)==1)
			{
				Academico academico = new Conexion().getAcademico(rut);
				nombres = academico.getAca_nombres();
				paterno = academico.getAca_apellido_p();
				materno = academico.getAca_apellido_m();
				contrasegnia = new Conexion().getClave(rut,"Academico");
				new Conexion().modificarClaveUsuario(rut, "Academico", clave);
				ban2=1;
			}
			if(new Conexion().verificar_usuario(rut, "Supervisor",0)==1)
			{
				Supervisor supervisor = new Conexion().getSupervisor(rut);
				nombres = supervisor.getSup_nombres();
				paterno = supervisor.getSup_apellido_p();
				materno = supervisor.getSup_apellido_m();
				contrasegnia = new Conexion().getClave(rut,"Supervisor");
				new Conexion().modificarClaveUsuario(rut, "Supervisor", clave);
				ban3=1;
			}
			if(new Conexion().verificar_usuario(rut, "Alumno",0)==1)
			{
				Alumno alumno = new Conexion().getDatosAlumno(rut);
				nombres = alumno.getAlu_nombres();
				paterno = alumno.getAlu_apellido_p();
				materno = alumno.getAlu_apellido_m();
				contrasegnia = new Conexion().getClave(rut,"Alumno");
				new Conexion().modificarClaveUsuario(rut, "Alumno", clave);
				ban4=1;
			}
			String contenido = "Buen día "+nombres+" "+paterno+" "+materno+" se ha generado una nueva clave de acceso para su cuenta "
					+ "en el Sistema de Control y Seguimiento de Prácticas Profesionales y Operativas "
					+ "de la Escuela de Ingeniería en Construcción de la Universidad del Bío-Bío, la proxima vez que quiera hacer uso de nuestra plataforma recuerde ingresar al siguiente enlace http://146.83.193.68:8080/icc/ y auntentifíquese "
					+ "con su rut: "+rut+" y su nueva clave de acceso : "+clave+". \n Una vez logueado le aconsejamos cambiar su clave de acceso. \n Esperamos que tenga una buena semana, "
					+ "consultas a este mismo correo \n atentamente el administrador ";
			if(ban3==1)
			{
				EmailUtilidades.sendEmailSupervisor(host, port, user, pass, destinatario, asunto,
						contenido);
			}else{
				EmailUtilidades.sendEmail(host, port, user, pass, destinatario, asunto,
						contenido);
			}
			
			mensaje = "Fue Generada una nueva contraseña para el Usuario "+nombres+" "+paterno+" "+materno+"";
			request.setAttribute("mensaje", mensaje);
			request.setAttribute("bandera", 1);
			} catch (Exception ex) 
			{
				if(ban1==1)
				{
					new Conexion().modificarClaveUsuario(rut, "Administrador", contrasegnia);
				}
				if(ban2==1)
				{
					new Conexion().modificarClaveUsuario(rut, "Academico", contrasegnia);
				}
				if(ban3==1)
				{
					new Conexion().modificarClaveUsuario(rut, "Supervisor", contrasegnia);
				}
				if(ban4==1)
				{
					new Conexion().modificarClaveUsuario(rut, "Alumno", contrasegnia);
				}
				ex.printStackTrace();
				mensaje = "Hubo un error al enviar: " + ex.getMessage();
				request.setAttribute("mensaje", mensaje);
				request.setAttribute("bandera", 2);
			} finally {
				request.getRequestDispatcher("/vistas_admin/recuperar_password.jsp").forward(request, response);
			}
	}
}
