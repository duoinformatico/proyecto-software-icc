package com.icc.controlador;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.icc.modelo.Supervisor;
import com.icc.utilidades.Conexion;

/**
 * Servlet implementation class ModificarSupervisorControlador
 */
@WebServlet("/ModificarSupervisorControlador")
public class ModificarSupervisorControlador extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private String mensaje="";
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ModificarSupervisorControlador() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Supervisor supervisor = new Conexion().getSupervisor(request.getParameter("sup_rut"));
		request.setAttribute("supervisor", supervisor);
		if(supervisor.getSup_telefono() == null || supervisor.getSup_telefono().equals("") || supervisor.getSup_telefono().equals("''"))
		{
			int bandera = 1;
			request.setAttribute("bandera", bandera);
		}else{
			String codigo = supervisor.getSup_telefono().charAt(4)+""+supervisor.getSup_telefono().charAt(5);
			request.setAttribute("codigo", codigo);
			String telefono = "";
			for(int i=0;i<supervisor.getSup_telefono().length();i++)
			{
				if(i>6)
				{
					telefono= telefono+""+supervisor.getSup_telefono().charAt(i);
				}
			}
			int bandera = 0;
			request.setAttribute("bandera", bandera);
			request.setAttribute("telefono", telefono);
		}
		request.setAttribute("empresa", new Conexion().getEmpresaSupervisor(supervisor.getSup_rut()));
		request.setAttribute("empresasList", new Conexion().getEmpresas());
		request.getRequestDispatcher("ajax/modificar_supervisor.jsp").forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String rut = request.getParameter("rut_modif");
		String nombres = request.getParameter("nombre_modif");
		String paterno = request.getParameter("paterno_modif");
		String materno = request.getParameter("materno_modif");
		String email = request.getParameter("email_modif");
		String telefono = request.getParameter("telefono_modif");
		String celular = request.getParameter("celular_modif");
		String empresa = request.getParameter("empresa_modif");
		String empresa_ultima = request.getParameter("empresa_ultima");
		String profesion = request.getParameter("profesion_modif");
		String cargo = request.getParameter("cargo_modif");
		String estado = request.getParameter("estado_modif");
		int exito = 0;
		try {		
			int i = new Conexion().ModificarDatosSupervisor(new Supervisor(rut,nombres,paterno,materno,email,telefono,celular,profesion,cargo,estado),1);
			if(i == 1)
			{
				if(!empresa.equals(empresa_ultima))
				{
					new Conexion().agregarHistorialSupervisor(empresa, rut);
				}	
				mensaje = "Los datos del supervisor se modificaron correctamente";
				request.setAttribute("mensaje", mensaje);
				exito = 1;
				request.setAttribute("exito", exito);
				request.getRequestDispatcher("menuSupervisorServlet.do").forward(request, response);
			}
			else{
				mensaje = "Hubo un error al modificar los datos del supervisor";
				request.setAttribute("mensaje", mensaje);
				exito = 0;
				request.setAttribute("exito", exito);
				request.getRequestDispatcher("menuSupervisorServlet.do").forward(request, response);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
