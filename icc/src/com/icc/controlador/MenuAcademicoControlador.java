package com.icc.controlador;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.icc.utilidades.Conexion;
import com.icc.utilidades.PasswordGenerador;

/**
 * Servlet implementation class MenuAcademicoControlador
 */
@WebServlet("/MenuAcademicoControlador")
public class MenuAcademicoControlador extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MenuAcademicoControlador() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("clave", PasswordGenerador.getPassword(
									  PasswordGenerador.MINUSCULAS+
									  PasswordGenerador.MAYUSCULAS+
									  PasswordGenerador.NUMEROS,10));
		request.setAttribute("departamentos", new Conexion().getDepartamentos());
		request.setAttribute("especialidadList", new Conexion().getEspecialidades());
		request.getRequestDispatcher("ajax/formulario_academico.jsp").forward(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("academicosList",new Conexion().getAcademicos());
		request.getRequestDispatcher("/vistas_admin/academicos.jsp").forward(request, response);
	}
}
