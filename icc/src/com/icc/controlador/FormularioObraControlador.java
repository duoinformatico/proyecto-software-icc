package com.icc.controlador;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.icc.modelo.Obra_tipo;
import com.icc.utilidades.Conexion;

/**
 * Servlet implementation class FormularioObraControlador
 */
@WebServlet("/FormularioObraControlador")
public class FormularioObraControlador extends HttpServlet {
	private static final long serialVersionUID = 1L;
	String mensaje="";
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FormularioObraControlador() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String nombre = request.getParameter("nombre");
		try
		{
			int i =new Conexion().AgregarObra(new Obra_tipo(nombre));
			if(i==1)
			{
				mensaje = "La obra se registr� correctamente";
				request.setAttribute("mensaje", mensaje);
				request.getRequestDispatcher("/vistas_admin/resultado_empresa.jsp").forward(request, response);
			}
			else{
				mensaje= "Hubo un error al registrar la obra ";
				request.setAttribute("mensaje", mensaje);
				request.getRequestDispatcher("/vistas_admin/resultado_empresa.jsp").forward(request, response);
			}
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

}
