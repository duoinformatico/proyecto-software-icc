package com.icc.controlador;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.icc.utilidades.Conexion;

/**
 * Servlet implementation class MenuPracticasControlador
 */
@WebServlet("/MenuPracticas")
public class MenuPracticasControlador extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MenuPracticasControlador() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    //muestra el historico de practicas para el admin
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("practicaList", new Conexion().getPracticas());
		request.getRequestDispatcher("/vistas_admin/practicas.jsp").forward(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("practicaList", new Conexion().getPracticas());
		request.getRequestDispatcher("/vistas_admin/practicas.jsp").forward(request, response);
	}
}
