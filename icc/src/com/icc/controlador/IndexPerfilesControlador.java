package com.icc.controlador;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.icc.utilidades.Conexion;

/**
 * Servlet implementation class IndexAdmin
 */
@WebServlet("/IndexPerfiles")
public class IndexPerfilesControlador extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public IndexPerfilesControlador() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String rut = (String) request.getSession().getAttribute("rut");
		String tipo = (String) request.getSession().getAttribute("tipo");
		
		if(tipo == "admin")
		{
			request.setAttribute("total", new Conexion().getTotalPracticas());
		    request.setAttribute("datos", new Conexion().getNombresAdmin(rut));
			request.getRequestDispatcher("/vistas_admin/index_administrador.jsp").forward(request, response);
		}
		if(tipo == "supervisor")
		{
			request.setAttribute("total", new Conexion().getTotalEstudiantes("supervisor", rut));
		    request.setAttribute("datos", new Conexion().getSupervisor(rut));
		    request.getRequestDispatcher("vistas_supervisor/index_supervisor.jsp").forward(request, response);
		}
		if(tipo == "academico")
		{
			request.setAttribute("total", new Conexion().getTotalEstudiantes("academico", rut));
		    request.setAttribute("datos", new Conexion().getAcademico(rut));
		    request.getRequestDispatcher("vistas_academico/index_academico.jsp").forward(request, response);
		}
		if(tipo == "alumno")
		{
			int cantidad = new Conexion().getTotalPracticasSemestre(rut);
			request.setAttribute("cantidad", cantidad);
			if(cantidad != 0)
			{
				request.setAttribute("practicaList", new Conexion().getPracticasSemestre(rut));
			}
			request.getRequestDispatcher("vistas_alumno/index_alumno.jsp").forward(request, response);
		}
		if(tipo == "administrador-academico")
		{
			 request.setAttribute("total_estudiantes", new Conexion().getTotalEstudiantes("academico", rut));
			 request.setAttribute("total_pendientes", new Conexion().getTotalPracticas());
			 request.getRequestDispatcher("vistas_admin_academico/index_admin_aca.jsp").forward(request, response);
		}
	}
}
