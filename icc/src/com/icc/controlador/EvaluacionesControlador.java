package com.icc.controlador;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.icc.modelo.Academico;
import com.icc.modelo.Alumno;
import com.icc.modelo.Evaluacion;
import com.icc.modelo.EvaluacionAcademico;
import com.icc.modelo.EvaluacionAlumno;
import com.icc.modelo.EvaluacionSupervisor;
import com.icc.modelo.Instrumento;
import com.icc.modelo.Practica;
import com.icc.modelo.Supervisor;
import com.icc.utilidades.Conexion;

/**
 * Servlet implementation class EvaluacionesControlador
 */
@WebServlet("/EvaluacionesControlador")
public class EvaluacionesControlador extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EvaluacionesControlador() {
        super();
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("doGet EvaluacionesControlador");
		String tipo = (String) request.getSession().getAttribute("tipo");
		String rut = (String) request.getSession().getAttribute("rut");
		ArrayList<EvaluacionAlumno> evaluaciones_alumno = new ArrayList<>();
		ArrayList<EvaluacionSupervisor> evaluaciones_supervisor = new ArrayList<>();
		ArrayList<EvaluacionAcademico> evaluaciones_academico = new ArrayList<>();
		ArrayList<Practica> practicas = new ArrayList<>();
		ArrayList<Alumno> alumnos = new ArrayList<>();
		ArrayList<Supervisor> supervisores = new ArrayList<>();
		ArrayList<Academico> academicos = new ArrayList<>();
		ArrayList<Practica> estados_informe = new ArrayList<Practica>();
		ArrayList<Instrumento> instrumentos = new ArrayList<>();
		if(tipo == "admin" || tipo == "administrador-academico"){
			practicas = new Conexion().getPracticas(tipo);//obtiene todas las practicas
			for(int i=0; i< practicas.size(); i++) {//obtiene todos los evaluadores y evaluaciones
				evaluaciones_alumno = new Conexion().getEvaluacionesAlumno(practicas.get(i).getPra_id(), evaluaciones_alumno);
	            evaluaciones_supervisor = new Conexion().getEvaluacionesSupervisor(practicas.get(i).getPra_id(), evaluaciones_supervisor);
	            evaluaciones_academico = new Conexion().getEvaluacionesAcademico(practicas.get(i).getPra_id(), evaluaciones_academico);
	            alumnos.add(new Conexion().getDatosAlumno(practicas.get(i).getAlu_rut()));
	            Practica temp = new Practica(practicas.get(i).getPra_id(),new Conexion().getEstadoInforme(practicas.get(i).getPra_id()));
				estados_informe.add(temp);
	        }
			
			for(int i=0; i< evaluaciones_supervisor.size(); i++) {//obtiene todos los evaluadores y evaluaciones
				supervisores.add(new Conexion().getSupervisor(evaluaciones_supervisor.get(i).getSup_rut()));  
	        }
			
			for(int i=0; i< evaluaciones_academico.size(); i++) {//obtiene todos los evaluadores y evaluaciones
				academicos.add(new Conexion().getAcademico(evaluaciones_academico.get(i).getAca_rut()));
	        }
			
			
			
			//instrumentos = new Conexion().getInstrumentos(1);//modificar para que sea dinamico con diferentes planes de estudio
			instrumentos = new Conexion().getAllInstrumentos();
			
			System.out.println("Cantidad Practicas: "+practicas.size());
			System.out.println("Cantidad Evaluaciones: "+(evaluaciones_alumno.size() + evaluaciones_supervisor.size() + evaluaciones_academico.size()));
			System.out.println("Cantidad Alumnos: "+alumnos.size());
			System.out.println("Cantidad Instrumentos: "+instrumentos.size());
			
			request.setAttribute("alumnosList", alumnos);
			request.setAttribute("academicosList", academicos);
			request.setAttribute("supervisoresList", supervisores);
			request.setAttribute("instrumentosList", instrumentos);
			request.setAttribute("practicaList",practicas);
			request.setAttribute("evaluacionesAlumnoList", evaluaciones_alumno);
			request.setAttribute("evaluacionesSupervisorList", evaluaciones_supervisor);
			request.setAttribute("evaluacionesAcademicoList", evaluaciones_academico);	
			request.setAttribute("empresasList", new Conexion().getEmpresas());
			request.setAttribute("asignaturasList", new Conexion().getGestiones() );
			request.setAttribute("estadosList", estados_informe);
			request.getRequestDispatcher("/vistas_admin/evaluaciones.jsp").forward(request, response);
		}
		if(tipo == "alumno"){
			practicas = new Conexion().getPracticasSegunRut(tipo, rut);
			for(int i=0; i< practicas.size(); i++) {//obtiene todos los evaluadores y evaluaciones
	            evaluaciones_alumno = new Conexion().getEvaluacionesAlumno(practicas.get(i).getPra_id(), evaluaciones_alumno);
	            evaluaciones_supervisor = new Conexion().getEvaluacionesSupervisor(practicas.get(i).getPra_id(), evaluaciones_supervisor);
	            evaluaciones_academico = new Conexion().getEvaluacionesAcademico(practicas.get(i).getPra_id(), evaluaciones_academico);
	            Practica temp = new Practica(practicas.get(i).getPra_id(),new Conexion().getEstadoInforme(practicas.get(i).getPra_id()));
				estados_informe.add(temp);
	        }
			for(int i=0; i< evaluaciones_supervisor.size(); i++) {//obtiene todos los evaluadores y evaluaciones
				supervisores.add(new Conexion().getSupervisor(evaluaciones_supervisor.get(i).getSup_rut()));  
	        }
			
			for(int i=0; i< evaluaciones_academico.size(); i++) {//obtiene todos los evaluadores y evaluaciones
				academicos.add(new Conexion().getAcademico(evaluaciones_academico.get(i).getAca_rut()));
	        }
			
			//instrumentos = new Conexion().getInstrumentos(1);//modificar para que sea dinamico con diferentes planes de estudio y diferentes estados de instrumentos
			instrumentos = new Conexion().getAllInstrumentos();
			
			System.out.println("Cantidad Practicas: "+practicas.size());
			System.out.println("Cantidad Evaluaciones: "+(evaluaciones_alumno.size() + evaluaciones_supervisor.size() + evaluaciones_academico.size()));		
			System.out.println("Cantidad Instrumentos: "+instrumentos.size());
			
			request.setAttribute("instrumentosList", instrumentos);
			request.setAttribute("practicaList",new Conexion().getPracticasSegunRut(tipo, rut));
			request.setAttribute("evaluacionesAlumnoList", evaluaciones_alumno);
			request.setAttribute("evaluacionesSupervisorList", evaluaciones_supervisor);
			request.setAttribute("evaluacionesAcademicoList", evaluaciones_academico);
			request.setAttribute("academicosList", academicos);
			request.setAttribute("supervisoresList", supervisores);
			request.setAttribute("empresasList", new Conexion().getEmpresas());
			request.setAttribute("asignaturasList", new Conexion().getGestiones() );
			request.setAttribute("estadosList", estados_informe);
			request.getRequestDispatcher("/vistas_alumno/evaluaciones.jsp").forward(request, response);
		}
		if(tipo == "supervisor"){
			evaluaciones_supervisor = new Conexion().getEvaluadoresSupervisor(rut);
			for(int i=0; i< evaluaciones_supervisor.size(); i++) {
	            practicas.add(new Conexion().getPractica(evaluaciones_supervisor.get(i).getPra_id()));
	            instrumentos.add(new Conexion().getInstrumento(evaluaciones_supervisor.get(i).getRol_id()));
	        }
			for(int i=0; i< practicas.size(); i++) {
	            alumnos.add(new Conexion().getDatosAlumno(practicas.get(i).getAlu_rut()));
	        }
			
			request.setAttribute("evaluacionesList", evaluaciones_supervisor);
			request.setAttribute("practicasList", practicas);
			request.setAttribute("alumnosList", alumnos);
			request.setAttribute("instrumentosList", instrumentos);
			request.setAttribute("asignaturasList", new Conexion().getGestiones() );
			request.setAttribute("empresasList", new Conexion().getEmpresas());
			request.getRequestDispatcher("/vistas_supervisor/alumnos_asignados.jsp").forward(request, response);
			
		}
		if(tipo == "academico"){
			evaluaciones_academico = new Conexion().getEvaluadoresAcademico(rut);
			for(int i=0; i< evaluaciones_academico.size(); i++) {
	            practicas.add(new Conexion().getPractica(evaluaciones_academico.get(i).getPra_id()));
	            instrumentos.add(new Conexion().getInstrumento(evaluaciones_academico.get(i).getRol_id()));
	        }
			for(int i=0; i< practicas.size(); i++) {
	            alumnos.add(new Conexion().getDatosAlumno(practicas.get(i).getAlu_rut()));
	        }
			
			request.setAttribute("evaluacionesList", evaluaciones_academico);
			request.setAttribute("practicasList", practicas);
			request.setAttribute("alumnosList", alumnos);
			request.setAttribute("instrumentosList", instrumentos);
			request.setAttribute("asignaturasList", new Conexion().getGestiones() );
			request.setAttribute("empresasList", new Conexion().getEmpresas());
			request.getRequestDispatcher("/vistas_academico/alumnos_asignados.jsp").forward(request, response);
		}
	}
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("doPost EvaluacionesControlador");
		String tipo = (String) request.getSession().getAttribute("tipo");
		String rut = (String) request.getSession().getAttribute("rut");
		//ArrayList<Evaluadores> evaluadores = new ArrayList<>();
		ArrayList<Evaluacion> evaluaciones = new ArrayList<>();
		ArrayList<Practica> practicas = new ArrayList<>();
		//ArrayList<Alumno> alumnos = new ArrayList<>();
		//ArrayList<Instrumento> instrumentos = new ArrayList<>();
		//ArrayList<Integer> eval_clave = new ArrayList<>();
		if(tipo == "admin"){
			
		}
		if(tipo == "alumno"){
			practicas = new Conexion().getPracticasSegunRut(tipo, rut);
			for(int i=0; i< practicas.size(); i++) {
	            evaluaciones = new Conexion().getEvaluaciones(practicas.get(i).getPra_id(), evaluaciones);
	        }
			System.out.println("Cantidad Practicas: "+practicas.size());
			System.out.println("Cantidad Evaluaciones: "+evaluaciones.size());
			request.setAttribute("practicaList",new Conexion().getPracticasSegunRut(tipo, rut));
			request.setAttribute("evaluacionesList", evaluaciones);
			request.setAttribute("empresasList", new Conexion().getEmpresas());
			request.setAttribute("asignaturasList", new Conexion().getGestiones() );
			request.getRequestDispatcher("/vistas_alumno/evaluaciones.jsp").forward(request, response);
		}
	}

}
