package com.icc.controlador.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet Filter implementation class SupervisorFiltro
 */
@WebFilter("/SupervisorFiltro")
public class SupervisorFiltro implements Filter {

    /**
     * Default constructor. 
     */
    public SupervisorFiltro() {
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest requestMod = ((HttpServletRequest) request);
		if(verifica_tipo(requestMod)== "supervisor")
		{
			chain.doFilter(request, response);		
		}
		if(verifica_tipo(requestMod)== "alumno")
		{
			HttpServletResponse httpResponse = (HttpServletResponse) response;
        	httpResponse.sendRedirect("IndexPerfiles");
		}
		if(verifica_tipo(requestMod)== "academico")
		{
			HttpServletResponse httpResponse = (HttpServletResponse) response;
        	httpResponse.sendRedirect("IndexPerfiles");
		}
		if(verifica_tipo(requestMod)== "admin")
		{
			HttpServletResponse httpResponse = (HttpServletResponse) response;
        	httpResponse.sendRedirect("IndexPerfiles");
		}
		if(verifica_tipo(requestMod)== "null")
		{
			HttpServletResponse httpResponse = (HttpServletResponse) response;
        	httpResponse.sendRedirect("index.jsp");
		}
	}
	private String verifica_tipo(HttpServletRequest request)
	{
        if (request.getSession().getAttribute("tipo") == "alumno") {
            return "alumno";
        }
        if (request.getSession().getAttribute("tipo") == "admin") {
            return "admin";
        }
        if (request.getSession().getAttribute("tipo") == "supervisor") {
            return "supervisor";
        }
        if (request.getSession().getAttribute("tipo") == "academico") {
            return "academico";
        }
        if (request.getSession().getAttribute("tipo") == "administrador-academico") {
            return "administrador-academico";
        }
		return "null";
    }
	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
	}

}
