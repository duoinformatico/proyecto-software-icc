package com.icc.controlador;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.icc.modelo.Funcion;
import com.icc.utilidades.Conexion;

/**
 * Servlet implementation class FormularioFuncionControlador
 */
@WebServlet("/FormularioFuncionControlador")
public class FormularioFuncionControlador extends HttpServlet {
	private static final long serialVersionUID = 1L;
    String mensaje="";
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FormularioFuncionControlador() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("funcionesList",new Conexion().getFunciones() );
		request.getRequestDispatcher("/vistas_admin/funciones.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String nombre = request.getParameter("nombre");
		int exito = 0;
		try {	
			int i = new Conexion().agregarFuncion(new Funcion(nombre));
			if(i == 1)
			{
				mensaje = "La Función fue registrada correctamente";
				request.setAttribute("mensaje", mensaje);
				exito = 1;
				request.setAttribute("exito", exito);
				request.setAttribute("funcionesList", new Conexion().getFunciones());
				request.getRequestDispatcher("vistas_admin/funciones.jsp").forward(request, response);
			}
			else{
				mensaje = "Hubo un error al registrar la nueva Función";
				request.setAttribute("mensaje", mensaje);
				exito = 0;
				request.setAttribute("exito", exito);
				request.setAttribute("funcionesList", new Conexion().getFunciones());
				request.getRequestDispatcher("vistas_admin/funciones.jsp").forward(request, response);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
