package com.icc.controlador;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.icc.modelo.Comuna;
import com.icc.modelo.Empresa;
import com.icc.modelo.Provincia;
import com.icc.modelo.Region;
import com.icc.utilidades.Conexion;

/**
 * Servlet implementation class ModificarEmpresaControlador
 */
@WebServlet("/ModificarEmpresaControlador")
public class ModificarEmpresaControlador extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private String mensaje="";
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ModificarEmpresaControlador() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Empresa empresa = new Conexion().getEmpresa(request.getParameter("emp_rut"));
		request.setAttribute("empresa", empresa);
		if(empresa.getEmp_telefono() == null || empresa.getEmp_telefono().equals("") || empresa.getEmp_telefono().equals("''"))
		{
			int bandera = 1;
			request.setAttribute("bandera", bandera);
		}else{
			String codigo = empresa.getEmp_telefono().charAt(4)+""+empresa.getEmp_telefono().charAt(5);
			request.setAttribute("codigo", codigo);
			String telefono = "";
			for(int i=0;i<empresa.getEmp_telefono().length();i++)
			{
				if(i>6)
				{
					telefono= telefono+""+empresa.getEmp_telefono().charAt(i);
				}
			}
			int bandera = 0;
			request.setAttribute("bandera", bandera);
			request.setAttribute("telefono", telefono);
		}
		
		Comuna comuna = new Conexion().getComuna(empresa.getCom_id());
		request.setAttribute("comuna", comuna);
		Provincia provincia = new Conexion().getProvincia(comuna.getPro_id());
		request.setAttribute("provincia", provincia);
		Region region = new Conexion().getRegion(provincia.getReg_id());
		request.setAttribute("region", region);
		request.setAttribute("comunaList", new Conexion().getComunas());
		request.setAttribute("regionList", new Conexion().getRegiones());
		request.setAttribute("provinciaList", new Conexion().getProvincias());
		request.getRequestDispatcher("ajax/modificar_empresa.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String rut = request.getParameter("rut_modif");
	    String comuna = request.getParameter("comuna_modif");
		String nombre = request.getParameter("nombre_modif");
		String direccion = request.getParameter("direccion_modif");
		String telefono = request.getParameter("telefono_modif");
		String celular = request.getParameter("celular_modif");
		String descripcion = request.getParameter("descripcion_modif");
		String email = request.getParameter("email_modif");
		String web= request.getParameter("web_modif");
		int exito = 0;
		try {	
			
			int i = new Conexion().ModificarDatosEmpresa(new Empresa(rut,Integer.parseInt(comuna),nombre,direccion,telefono,celular,descripcion,email,web));
			if(i == 1)
			{
				mensaje = "Los datos de la empresa se modificaron correctamente";
				request.setAttribute("mensaje", mensaje);
				exito = 1;
				request.setAttribute("exito", exito);
				request.getRequestDispatcher("menuEmpresaServlet.do").forward(request, response);
			}
			else{
				mensaje = "Hubo un error al modificar los datos de la empresa ";
				request.setAttribute("mensaje", mensaje);
				exito = 0;
				request.setAttribute("exito", exito);
				request.getRequestDispatcher("menuEmpresaServlet.do").forward(request, response);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
