package com.icc.controlador;


import java.io.IOException;
import java.sql.SQLException;
import java.util.Calendar;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.icc.modelo.Alumno;
import com.icc.modelo.Bitacora;
import com.icc.modelo.Imagen;
import com.icc.modelo.Practica;
import com.icc.utilidades.Conexion;
import com.icc.utilidades.Herramientas;

/**
 * Servlet implementation class FormularioBitacoraControlador
 */
@WebServlet("/FormularioBitacoraControlador")
@MultipartConfig
public class FormularioBitacoraControlador extends HttpServlet {
	private static final long serialVersionUID = 1L;
	String mensaje = "";
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FormularioBitacoraControlador() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String tipo = (String) request.getSession().getAttribute("tipo");
		String plan_id = request.getParameter("plan_id");
		String bit_id = request.getParameter("bit_id");
		if(tipo=="alumno")
		{
			Bitacora bitacora = new Conexion().getBitacoraAlumno(Integer.parseInt(bit_id));
			Practica practica = new Conexion().getPractica(bitacora.getPra_id());
			Alumno alumno = new Conexion().getDatosAlumno(practica.getAlu_rut());		
			request.setAttribute("bitacora", bitacora);
			request.setAttribute("mensaje", "Esta es la información de tu bitácora sr/a "+alumno.getAlu_nombres()+" "+alumno.getAlu_apellido_p()+" "+alumno.getAlu_apellido_m()+"");
			request.setAttribute("asignaturaBitacoraList", new Conexion().getAsignaturasBitacora(Integer.parseInt(plan_id),Integer.parseInt(bit_id)));
			request.setAttribute("observacionList", new Conexion().getObservaciones(Integer.parseInt(bit_id)));
			request.setAttribute("imagenList", new Conexion().getImagenes(Integer.parseInt(bit_id)));
		}
		if(tipo=="supervisor")
		{
			Bitacora bitacora = new Conexion().getBitacoraAlumno(Integer.parseInt(bit_id));
			Practica practica = new Conexion().getPractica(bitacora.getPra_id());
			Alumno alumno = new Conexion().getDatosAlumno(practica.getAlu_rut());
			
			request.setAttribute("bitacora", bitacora);
			request.setAttribute("mensaje", "Bitácora de el/la estudiante "+alumno.getAlu_nombres()+" "+alumno.getAlu_apellido_p()+" "+alumno.getAlu_apellido_m()+"");
			request.setAttribute("asignaturaBitacoraList", new Conexion().getAsignaturasBitacora(Integer.parseInt(plan_id),Integer.parseInt(bit_id)));
			request.setAttribute("observacionList", new Conexion().getObservaciones(Integer.parseInt(bit_id)));
			request.setAttribute("imagenList", new Conexion().getImagenes(Integer.parseInt(bit_id)));
		}
		if(tipo=="academico" || tipo == "administrador-academico")
		{
			Bitacora bitacora = new Conexion().getBitacoraAlumno(Integer.parseInt(bit_id));
			Practica practica = new Conexion().getPractica(bitacora.getPra_id());
			Alumno alumno = new Conexion().getDatosAlumno(practica.getAlu_rut());
			
			request.setAttribute("bitacora", bitacora);
			request.setAttribute("mensaje", "Bitácora de el/la estudiante "+alumno.getAlu_nombres()+" "+alumno.getAlu_apellido_p()+" "+alumno.getAlu_apellido_m()+"");
			request.setAttribute("asignaturaBitacoraList", new Conexion().getAsignaturasBitacora(Integer.parseInt(plan_id),Integer.parseInt(bit_id)));
			request.setAttribute("observacionList", new Conexion().getObservaciones(Integer.parseInt(bit_id)));
			request.setAttribute("imagenList", new Conexion().getImagenes(Integer.parseInt(bit_id)));
		}
		
		request.getRequestDispatcher("ajax/consultar_bitacora.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		System.out.println(""+request.getCharacterEncoding());
		String descripcion = Herramientas.getContentTextArea(request.getPart("descripcion"));	
		System.out.println("descri "+descripcion);
		request.setCharacterEncoding("UTF-8");
		System.out.println("descri "+descripcion);
		//sacando valor de cuantas imagenes y asignaturas hay actualmente.
		int actual_imagenes = Integer.parseInt(Herramientas.getcontentPartText(request.getPart("actual_imagenes")));
		
		//sacando id de practica y bitacora.
		int bit_id = Integer.parseInt(Herramientas.getcontentPartText(request.getPart("bit_id")));
		int pra_id = Integer.parseInt(Herramientas.getcontentPartText(request.getPart("pra_id")));
		
		// pasando asignaturas tributadas a la bd
		
		String[] asignaturas = request.getParameterValues("asignatura");//recibe el select multiple
		Practica practica = new Conexion().getPractica(pra_id);
		int exito = 1;
		if(asignaturas != null)
		{
			for (int i = 0; i < asignaturas.length; i++) {
				try {
					int codigo = Integer.parseInt(asignaturas[i]);
					new Conexion().agregarAsignaturaBitacora(codigo,bit_id,practica.getPlan_id());
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		int cantidad_imagenes = new Conexion().getActualImagenes(bit_id);
		// guardando imagenes en el servidor y guardando url 
		/*if(cantidad_imagenes != actual_imagenes)		{
			Calendar calendario = Calendar.getInstance();
			int hora =calendario.get(Calendar.HOUR_OF_DAY);
			int minutos = calendario.get(Calendar.MINUTE);
			int segundos = calendario.get(Calendar.SECOND);
			int milisegundos = calendario.get(Calendar.MILLISECOND);
			String fileName = "/usr/share/tomcat7/webapps/icc/WebContent/fotos/practica"+pra_id+"bitacora"+bit_id+"hora"+hora+"_"+minutos+"_"+segundos+"_"+milisegundos+".png";
			System.out.println("filename"+fileName);
			
			String fileName_dos = "practica"+pra_id+"bitacora"+bit_id+"hora"+hora+"_"+minutos+"_"+segundos+"_"+milisegundos+".png";
			System.out.println("filename"+fileName_dos);
			boolean ok = Herramientas.guardarImagen(request.getPart("imagen_"+i+"").getInputStream(), fileName);
			if (ok == true){
	            try {
	            	new Conexion().agregarImagen(new Imagen(fileName_dos));
	            	new Conexion().agregarImagenBitacora(bit_id, new Conexion().getImagen(fileName_dos));
	            	System.out.println("imagen agregada completamente");
				} catch (Exception e) {
					e.printStackTrace();
				}
	        }
			if(ok =false){
	        	System.out.println("error al insertar");
	        }
			*/
			int ban = 1;
			int contador =0;
			for(int i=(cantidad_imagenes+1);i<=actual_imagenes && ban ==1;i++) 
			{
				contador+=1;
				Calendar calendario = Calendar.getInstance();
				int hora =calendario.get(Calendar.HOUR_OF_DAY);
				int minutos = calendario.get(Calendar.MINUTE);
				int segundos = calendario.get(Calendar.SECOND);
				int milisegundos = calendario.get(Calendar.MILLISECOND);
				//String fileName = "/home/camilo/Escritorio/fotos/practica"+pra_id+"bitacora"+bit_id+"hora"+hora+"_"+minutos+"_"+segundos+"_"+milisegundos+".png";
				String fileName = "/mnt/nas1/practica"+pra_id+"bitacora"+bit_id+"hora"+hora+"_"+minutos+"_"+segundos+"_"+milisegundos+".png";
				//String fileName = "E:/archivos/practica"+pra_id+"bitacora"+bit_id+"hora"+hora+"_"+minutos+"_"+segundos+"_"+milisegundos+".png";
				String fileName_dos = "practica"+pra_id+"bitacora"+bit_id+"hora"+hora+"_"+minutos+"_"+segundos+"_"+milisegundos+".png";
				System.out.println(""+request.getPart("imagen_"+i+"").getContentType().contains("image"));
				System.out.println(""+request.getPart("imagen_"+i+"").getContentType().contains("zip"));
				System.out.println(""+request.getPart("imagen_"+i+"").getContentType().contains("rar"));
				System.out.println(""+request.getPart("imagen_"+i+"").getContentType());
				System.out.println(""+request.getPart("imagen_"+i+"").getContentType().startsWith("image"));
				if(request.getPart("imagen_"+i+"").getSize() > 0)
				{
					if(request.getPart("imagen_"+i+"").getSize() < 524288)
					{
						if(request.getPart("imagen_"+i+"").getContentType().contains("image"))
						{
							boolean ok = Herramientas.guardar(request.getPart("imagen_"+i+"").getInputStream(), fileName);
							if (ok == true){
					            try {
					            	new Conexion().agregarImagen(new Imagen(fileName_dos));
					            	new Conexion().agregarImagenBitacora(bit_id, new Conexion().getImagen(fileName_dos));
					            	System.out.println("imagen agregada completamente");
								} catch (Exception e) {
									e.printStackTrace();
								}
					        }
							if(ok ==false){
					        	System.out.println("error al insertar");
					        	mensaje = "Los cambios en su bitácora no han podido ser  realizados ";
								request.setAttribute("mensaje", mensaje);
								exito = 0;
								request.setAttribute("exito", exito);
					        }
						}else{
							ban =0;
							if(contador == 1)
							{
								mensaje = "Su imagen no pudo ser guardada en el sistema ya que el tipo de archivo no es válido, recuerde que solo"
								+ " aceptamos imagenes de tipo png y jpg";
								request.setAttribute("mensaje", mensaje);
								exito = 0;
								request.setAttribute("exito", exito);
							}
							if(contador > 1)
							{
								mensaje = "Su imagen número #"+contador+" no pudo ser guardada en el sistema ya que el archivo no es válido, "
										+ "recuerde que solo aceptamos imagenes de formato png y jpg";
										request.setAttribute("mensaje", mensaje);
										exito = 0;
										request.setAttribute("exito", exito);
							}
						}
					}else{
						ban =0;
						if(contador == 1)
						{
							mensaje = "Su imagen no pudo ser guardada en el sistema ya que supera los 500 kilobytes, intente nuevamente "
							+ " con un valor menor al indicado";
							request.setAttribute("mensaje", mensaje);
							exito = 0;
							request.setAttribute("exito", exito);
						}
						if(contador > 1)
						{
							mensaje = "Su imagen número #"+contador+" no pudo ser guardada en el sistema ya que supera los 500 kilobytes, "
									+ "intente nuevamente con un valor menor al indicado";
									request.setAttribute("mensaje", mensaje);
									exito = 0;
									request.setAttribute("exito", exito);
						}
					}
				}else{
					if(contador == 1)
					{
						mensaje = " No seleccionó ningún archivo para subir al sistema, intente nuevamente";
						request.setAttribute("mensaje", mensaje);
						exito = 0;
						request.setAttribute("exito", exito);
					}
					if(contador > 1)
					{
						mensaje = " No seleccionó ningún archivo para subir al sistema en el campo número #"+contador+", intente nuevamente";
						request.setAttribute("mensaje", mensaje);
						exito = 0;
						request.setAttribute("exito", exito);
					}
					
				}
				
				
			}
				//para linux
				/*
				Calendar calendario = Calendar.getInstance();
				int hora =calendario.get(Calendar.HOUR_OF_DAY);
				int minutos = calendario.get(Calendar.MINUTE);
				int segundos = calendario.get(Calendar.SECOND);
				int milisegundos = calendario.get(Calendar.MILLISECOND);
				String fileName = "/home/camilo/Escritorio/fotos/practica"+pra_id+"bitacora"+bit_id+"hora"+hora+"_"+minutos+"_"+segundos+"_"+milisegundos+".png";
				System.out.println("filename"+fileName);
				
				String fileName_dos = "practica"+pra_id+"bitacora"+bit_id+"hora"+hora+"_"+minutos+"_"+segundos+"_"+milisegundos+".png";
				System.out.println("filename"+fileName_dos);
				boolean ok = Herramientas.guardarImagen(request.getPart("imagen_"+i+"").getInputStream(), fileName);
				if (ok == true){
		            try {
		            	new Conexion().agregarImagen(new Imagen(fileName_dos));
		            	new Conexion().agregarImagenBitacora(bit_id, new Conexion().getImagen(fileName_dos));
		            	System.out.println("imagen agregada completamente");
					} catch (Exception e) {
						e.printStackTrace();
					}
		        }
				if(ok =false){
		        	System.out.println("error al insertar");
		        }
             
		}
		*/
		//registrando cambios en la tabla bd
		try 
		{
			request.setCharacterEncoding("UTF-8");
			int i = new Conexion().escribirBitacora(new Bitacora(bit_id,descripcion));
			if(i == 1)
			{
				request.setAttribute("bitacoraList",new Conexion().getBitacorasAlumno(pra_id));
				request.setAttribute("practica", new Conexion().getPractica(pra_id));
				if(exito == 1)
				{
					mensaje = "Los cambios en su bitácora han sido realizados correctamente ";
					request.setAttribute("mensaje", mensaje);
					exito = 1;
					request.setAttribute("exito", exito);
				}
				request.getRequestDispatcher("vistas_alumno/bitacoras_alumno.jsp").forward(request, response);	
			}
			else{
				request.setAttribute("bitacoraList",new Conexion().getBitacorasAlumno(pra_id));
				request.setAttribute("practica", new Conexion().getPractica(pra_id));
				request.getRequestDispatcher("vistas_alumno/bitacoras_alumno.jsp").forward(request, response);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
