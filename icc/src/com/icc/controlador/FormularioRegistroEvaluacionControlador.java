package com.icc.controlador;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.icc.modelo.Alumno;
import com.icc.modelo.DetalleEvaluacionAcademico;
import com.icc.modelo.DetalleEvaluacionAlumno;
import com.icc.modelo.DetalleEvaluacionSupervisor;
import com.icc.modelo.EvaluacionAcademico;
import com.icc.modelo.EvaluacionAlumno;
import com.icc.modelo.EvaluacionSupervisor;
import com.icc.modelo.Instrumento;
import com.icc.modelo.Practica;
import com.icc.utilidades.Conexion;

/**
 * Servlet implementation class FormularioRegistroEvaluacionControlador
 */
@WebServlet("/FormularioRegistroEvaluacionControlador")
public class FormularioRegistroEvaluacionControlador extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FormularioRegistroEvaluacionControlador() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("\ndoPost FormularioRegistroEvaluacionControlador");
		String tipo = (String) request.getSession().getAttribute("tipo");
		String rut = (String) request.getSession().getAttribute("rut");
		String nombre_items = request.getParameter("nombre_items");
		int pra_id = Integer.parseInt(request.getParameter("pra_id"));
		int rol_id = Integer.parseInt(request.getParameter("rol_id"));
		String[] arrayItems = nombre_items.split("-");
		
		//Variables para enviar a EvaluacionesControlador(redireccion)
		ArrayList<EvaluacionAlumno> evaluaciones_alumno = new ArrayList<>();
		ArrayList<EvaluacionSupervisor> evaluaciones_supervisor = new ArrayList<>();
		ArrayList<EvaluacionAcademico> evaluaciones_academico = new ArrayList<>();
		
		ArrayList<Practica> practicas = new ArrayList<>();
		ArrayList<Practica> estados_informe = new ArrayList<Practica>();
		ArrayList<Instrumento> instrumentos = new ArrayList<>();
		ArrayList<Alumno> alumnos = new ArrayList<>();
				
		if(tipo == "admin"){
			//request.getRequestDispatcher("MenuEvaluacionControlador").forward(request, response);
			
		}
		
		if(tipo == "alumno"){
			DetalleEvaluacionAlumno evaluacion_alumno;
			String fecha;
			//Actualizacion de evaluacion + fecha + hora + observacion + cambio de estado
			fecha = new Conexion().realizarEvaluacionAlumno(rut, pra_id, rol_id, Double.parseDouble(request.getParameter("nota")), request.getParameter("observacion"));
			// En este momento tenemos un array en el que cada elemento es un nombre de un input de la evaluacion.
			//insercion de los detalles de evaluacion
			for (int i = 0; i < arrayItems.length; i++) {
				evaluacion_alumno = new DetalleEvaluacionAlumno(rut, pra_id, fecha, rol_id, new Conexion().getOpc_id(Integer.parseInt(arrayItems[i]), Double.parseDouble(request.getParameter(arrayItems[i]))), Integer.parseInt(arrayItems[i]));
										/*String alu_rut, int pra_id, String eva_fecha, int rol_id, int opc_id, int item_id*/
				
				new Conexion().agregarDetalleEvaluacionAlumno(evaluacion_alumno);
				System.out.println(arrayItems[i]);
			}
			//Insercion de las areas de trabajo
			String[] areas = request.getParameterValues("area");//recibe el select multiple de areas seleccionadas
			for (int i = 0; i < areas.length; i++) {
				System.out.println("Areas:"+areas[i]);
					new Conexion().agregarAreaPractica(pra_id, Integer.parseInt(areas[i]));
			}
			
			//Insercion de las funciones de trabajo
			String[] funciones = request.getParameterValues("funcion");//recibe el select multiple de funciones seleccionadas
			for (int i = 0; i < funciones.length; i++) {
				System.out.println("Funciones:"+areas[i]);
					new Conexion().agregarFuncionPractica(pra_id, Integer.parseInt(funciones[i]));
			}
			
			//Redirección a Mis evaluaciones
			practicas = new Conexion().getPracticasSegunRut(tipo, rut);
			for(int i=0; i< practicas.size(); i++) {//obtiene todos los evaluadores y evaluaciones
	            evaluaciones_alumno = new Conexion().getEvaluacionesAlumno(practicas.get(i).getPra_id(), evaluaciones_alumno);
	            evaluaciones_supervisor = new Conexion().getEvaluacionesSupervisor(practicas.get(i).getPra_id(), evaluaciones_supervisor);
	            evaluaciones_academico = new Conexion().getEvaluacionesAcademico(practicas.get(i).getPra_id(), evaluaciones_academico);
	            Practica temp = new Practica(practicas.get(i).getPra_id(),new Conexion().getEstadoInforme(practicas.get(i).getPra_id()));
				estados_informe.add(temp);
	        }
			instrumentos = new Conexion().getInstrumentos(1);//modificar para que sea dinamico con diferentes planes de estudio y diferentes estados de instrumentos
	        
			
			System.out.println("Cantidad Practicas: "+practicas.size());
			System.out.println("Cantidad Evaluaciones: "+(evaluaciones_alumno.size() + evaluaciones_supervisor.size() + evaluaciones_academico.size()));		
			
			request.setAttribute("instrumentosList", instrumentos);
			request.setAttribute("practicaList",new Conexion().getPracticasSegunRut(tipo, rut));
			request.setAttribute("evaluacionesAlumnoList", evaluaciones_alumno);
			request.setAttribute("evaluacionesSupervisorList", evaluaciones_supervisor);
			request.setAttribute("evaluacionesAcademicoList", evaluaciones_academico);
			request.setAttribute("empresasList", new Conexion().getEmpresas());
			request.setAttribute("asignaturasList", new Conexion().getGestiones() );
			request.setAttribute("estadosList", estados_informe);
			request.getRequestDispatcher("/vistas_alumno/evaluaciones.jsp").forward(request, response);
			
		}
		
		if(tipo == "academico" || tipo == "administrador-academico"){
			DetalleEvaluacionAcademico evaluacion_academico;
			String fecha;
			//Actualizacion de evaluacion + fecha + hora + observacion + cambio de estado
			fecha = new Conexion().realizarEvaluacionAcademico(rut, pra_id, rol_id, Double.parseDouble(request.getParameter("nota")), request.getParameter("observacion"));
			// En este momento tenemos un array en el que cada elemento es un nombre de un input de la evaluacion.
			//insercion de los detalles de evaluacion
			for (int i = 0; i < arrayItems.length; i++) {
				evaluacion_academico = new DetalleEvaluacionAcademico(rut, pra_id, fecha, rol_id, new Conexion().getOpc_id(Integer.parseInt(arrayItems[i]), Double.parseDouble(request.getParameter(arrayItems[i]))), Integer.parseInt(arrayItems[i]));
										/*String alu_rut, int pra_id, String eva_fecha, int rol_id, int opc_id, int item_id*/
				new Conexion().agregarDetalleEvaluacionAcademico(evaluacion_academico);
				System.out.println(arrayItems[i]);
			}			
			//redireccion
			evaluaciones_academico = new Conexion().getEvaluadoresAcademico(rut);
			for(int i=0; i< evaluaciones_academico.size(); i++) {
	            practicas.add(new Conexion().getPractica(evaluaciones_academico.get(i).getPra_id()));
	            instrumentos.add(new Conexion().getInstrumento(evaluaciones_academico.get(i).getRol_id()));
	        }
			for(int i=0; i< practicas.size(); i++) {
	            alumnos.add(new Conexion().getDatosAlumno(practicas.get(i).getAlu_rut()));
	        }
			
			request.setAttribute("evaluacionesList", evaluaciones_academico);
			request.setAttribute("practicasList", practicas);
			request.setAttribute("alumnosList", alumnos);
			request.setAttribute("instrumentosList", instrumentos);
			request.setAttribute("asignaturasList", new Conexion().getGestiones() );
			request.setAttribute("empresasList", new Conexion().getEmpresas());
			request.getRequestDispatcher("/vistas_academico/alumnos_asignados.jsp").forward(request, response);
			
		}
		
		if(tipo == "supervisor"){
			DetalleEvaluacionSupervisor evaluacion_supervisor;
			String fecha;
			//Actualizacion de evaluacion + fecha + hora + observacion + cambio de estado
			fecha = new Conexion().realizarEvaluacionSupervisor(rut, pra_id, rol_id, Double.parseDouble(request.getParameter("nota")), request.getParameter("observacion"));
			// En este momento tenemos un array en el que cada elemento es un nombre de un input de la evaluacion.
			//insercion de los detalles de evaluacion
			for (int i = 0; i < arrayItems.length; i++) {
				evaluacion_supervisor = new DetalleEvaluacionSupervisor(rut, pra_id, fecha, rol_id, new Conexion().getOpc_id(Integer.parseInt(arrayItems[i]), Double.parseDouble(request.getParameter(arrayItems[i]))), Integer.parseInt(arrayItems[i]));
										/*String alu_rut, int pra_id, String eva_fecha, int rol_id, int opc_id, int item_id*/
				
				new Conexion().agregarDetalleEvaluacionSupervisor(evaluacion_supervisor);
				System.out.println(arrayItems[i]);
			}
			request.getRequestDispatcher("MenuEvaluacionControlador").forward(request, response);
		}		
		
	}

}
