package com.icc.controlador;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.ArrayList;
import java.util.Calendar;

import javax.servlet.annotation.MultipartConfig;

import com.icc.modelo.Alumno;
import com.icc.modelo.EvaluacionAlumno;
import com.icc.modelo.Instrumento;
import com.icc.modelo.Practica;
import com.icc.utilidades.Conexion;
import com.icc.utilidades.Herramientas;

/**
 * Servlet implementation class FormularioInformeControlador
 */
@WebServlet("/FormularioInformeControlador")
@MultipartConfig
public class FormularioInformeControlador extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FormularioInformeControlador() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String fileName = request.getParameter("fileName");
        if(fileName == null || fileName.equals("")){
            throw new ServletException("El nombre del archivo no puede ser nulo o vacío");
        }

        //File file = new File("E:/archivos"+File.separator+fileName);
        //File file = new File("/home/camilo/Escritorio/fotos"+File.separator+fileName);
        File file = new File("/mnt/nas1"+File.separator+fileName);


        if(!file.exists()){
            throw new ServletException("El archivo no existe en el servidor");
        }
        System.out.println("File location on server::"+file.getAbsolutePath());
        ServletContext ctx = getServletContext();
        InputStream fis = new FileInputStream(file);
        String mimeType = ctx.getMimeType(file.getAbsolutePath());
        response.setContentType(mimeType != null? mimeType:"application/octet-stream");
        response.setContentLength((int) file.length());
        response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
 
        ServletOutputStream os       = response.getOutputStream();
        byte[] bufferData = new byte[1024];
        int read=0;
        while((read = fis.read(bufferData))!= -1){
            os.write(bufferData, 0, read);
        }
        os.flush();
        os.close();
        fis.close();
        System.out.println("Archivo descargado exitosamente");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//subiendo informe al servidor
		int pra_id = Integer.parseInt(Herramientas.getcontentPartText(request.getPart("pra_id")));
		Practica practica = new Conexion().getPractica(pra_id);
		Alumno alumno = new Conexion().getDatosAlumno(practica.getAlu_rut());
		Calendar calendario = Calendar.getInstance();
		int hora =calendario.get(Calendar.HOUR_OF_DAY);
		int minutos = calendario.get(Calendar.MINUTE);
		int segundos = calendario.get(Calendar.SECOND);
		int exito = 3;
		String mensaje = "";
		String fileName = "";
		String fileName_dos = "";
		
		
		if( request.getPart("archivo").getSize() > 0)
		{
			System.out.println("tamaño "+request.getPart("archivo").getSize());
			if(request.getPart("archivo").getSize() < 15728640)
			{
				if(request.getPart("archivo").getContentType().contains("zip") || request.getPart("archivo").getContentType().contains("rar"))
				{
					if(request.getPart("archivo").getContentType().contains("zip"))
					{
						//fileName = "/home/camilo/Escritorio/fotos/InformeFinal_"+alumno.getAlu_rut()+"_hora"+hora+"_"+minutos+"_"+segundos+".zip";
						fileName = "/mnt/nas1/InformeFinal_"+alumno.getAlu_rut()+"_hora"+hora+"_"+minutos+"_"+segundos+".zip";
						
						//fileName = "E:/archivos/InformeFinal_"+alumno.getAlu_rut()+"_hora"+hora+"_"+minutos+"_"+segundos+".zip";
						fileName_dos = "InformeFinal_"+alumno.getAlu_rut()+"_hora"+hora+"_"+minutos+"_"+segundos+".zip";
					}
					if(request.getPart("archivo").getContentType().contains("rar"))
					{
						//fileName = "/home/camilo/Escritorio/fotos/InformeFinal_"+alumno.getAlu_rut()+"_hora"+hora+"_"+minutos+"_"+segundos+".rar";
						fileName = "/mnt/nas1/InformeFinal_"+alumno.getAlu_rut()+"_hora"+hora+"_"+minutos+"_"+segundos+".rar";
						
						//fileName = "E:/archivos/InformeFinal_"+alumno.getAlu_rut()+"_hora"+hora+"_"+minutos+"_"+segundos+".rar";
						fileName_dos = "InformeFinal_"+alumno.getAlu_rut()+"_hora"+hora+"_"+minutos+"_"+segundos+".rar";
					}
					System.out.println("stream "+request.getPart("archivo").getInputStream());
					boolean ok = Herramientas.guardar(request.getPart("archivo").getInputStream(), fileName);
					if (ok == true){
			            try {
			            	new Conexion().ModificarInformePractica(pra_id,fileName_dos);
			            	System.out.println("Informe agregado completamente");
			            	mensaje = "El informe fue subido al sistema exitosamente";
							request.setAttribute("mensaje", mensaje);
							exito = 1;
							request.setAttribute("exito", exito);
						} catch (Exception e) {
							e.printStackTrace();
						}
			        }
					if(ok =false){
			        	System.out.println("error al insertar");
			        	mensaje = "El informe no ha podido ser subido al sistema ";
						request.setAttribute("mensaje", mensaje);
						exito = 0;
						request.setAttribute("exito", exito);
			        }
				}else{
						mensaje = "Su informe no pudo ser guardado en el sistema ya que el tipo de archivo no es válido, recuerde que solo"
						+ " aceptamos informes comprimidos en formato .zip o .rar";
						request.setAttribute("mensaje", mensaje);
						exito = 0;
						request.setAttribute("exito", exito);
				}
			}else{
				mensaje = "Su informe no pudo ser guardado en el sistema ya que supera los  15 Megabytes, intente nuevamente"
						+ "con un archivo de menor peso";
				request.setAttribute("mensaje", mensaje);
				exito = 0;
				request.setAttribute("exito", exito);
			}
		}else{
			mensaje = " No seleccionó ningún archivo para subir al sistema, intente nuevamente";
			request.setAttribute("mensaje", mensaje);
			exito = 0;
			request.setAttribute("exito", exito);
		}
		
		
        
		// cargando info de practicas
		String rut = (String) request.getSession().getAttribute("rut");
		String tipo = (String) request.getSession().getAttribute("tipo");
		ArrayList<EvaluacionAlumno> evaluaciones_alumno = new ArrayList<>();
		ArrayList<Practica> practicas = new ArrayList<>();
		ArrayList<Practica> estados_informe = new ArrayList<Practica>();
		ArrayList<Alumno> alumnos = new ArrayList<>();
		ArrayList<Instrumento> instrumentos = new ArrayList<>();
		
		if(tipo == "alumno"){
			/*evaluaciones_alumno = new Conexion().getEvaluadoresAlumno(rut);
			for(int i=0; i< evaluadores.size(); i++) {
	            eval_clave.add(evaluadores.get(i).getEval_clave());
	            
	            System.out.println("\neval_clave = "+ evaluadores.get(i).getEval_clave());
	        }
			evaluaciones = new Conexion().getEvaluaciones(eval_clave);
			for(int i=0; i< evaluaciones.size(); i++) {
	            practicas.add(new Conexion().getPractica(evaluaciones.get(i).getPra_id()));
	            // se agregan los estados de los informes de las practicas
	            Practica temp = new Practica(evaluaciones.get(i).getPra_id(),new Conexion().getEstadoInforme(evaluaciones.get(i).getPra_id()));
				estados_informe.add(temp);
	        }
			for(int i=0; i< practicas.size(); i++) {
	            alumnos.add(new Conexion().getDatosAlumno(practicas.get(i).getAlu_rut()));
	        }
			for(int i=0; i< evaluadores.size(); i++) {
	            instrumentos.add(new Conexion().getInstrumento(evaluadores.get(i).getRol_id()));
	        }*/
			
			evaluaciones_alumno = new Conexion().getEvaluadoresAlumno(rut);//obtiene todas las autoevaluaciones sin evaluar del alumno que se ha autentificado
			for(int i=0; i< evaluaciones_alumno.size(); i++) {
	            practicas.add(new Conexion().getPractica(evaluaciones_alumno.get(i).getPra_id()));
	            // se agregan los estados de los informes de las practicas
	            Practica temp = new Practica(evaluaciones_alumno.get(i).getPra_id(),new Conexion().getEstadoInforme(evaluaciones_alumno.get(i).getPra_id()));
				estados_informe.add(temp);
				instrumentos.add(new Conexion().getInstrumento(evaluaciones_alumno.get(i).getRol_id()));
			    
	        }
			
			request.setAttribute("evaluacionesList", evaluaciones_alumno);
			request.setAttribute("practicasList", practicas);
			request.setAttribute("alumnosList", alumnos);
			request.setAttribute("estadosList", estados_informe);
			request.setAttribute("instrumentosList", instrumentos);
			request.setAttribute("asignaturasList", new Conexion().getGestiones() );
			request.setAttribute("empresasList", new Conexion().getEmpresas());	
		}
		
		request.setAttribute("practicaList",new Conexion().getHistorial(rut));
		response.setHeader("Cache-control", "no-cache, no-store, must-revalidate");
		request.getRequestDispatcher("/vistas_alumno/historial.jsp").forward(request, response);
	}

}
