package com.icc.controlador;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.icc.modelo.Plan_estudio;
import com.icc.utilidades.Conexion;

/**
 * Servlet implementation class ModificarPlanControlador
 */
@WebServlet("/ModificarPlanControlador")
public class ModificarPlanControlador extends HttpServlet {
	private static final long serialVersionUID = 1L;
    String mensaje=""; 
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ModificarPlanControlador() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String plan_id = request.getParameter("plan_id");
		request.setAttribute("id", plan_id);
		request.setAttribute("carrerasList", new Conexion().getCarreras());
		request.setAttribute("plan", new Conexion().getPlan(Integer.parseInt(plan_id)));
		request.getRequestDispatcher("ajax/modificar_plan.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String id = request.getParameter("plan_id");
		String nombre = request.getParameter("nombre");
		String carrera = request.getParameter("carrera");
		String estado = request.getParameter("estado");
		boolean vigente=true;
		
		if(estado.equals("activo"))
		{
			vigente=true;
		}else{
			if(estado.equals("inactivo"))
			{
				vigente=false;
			}
		}
		
		try {	
			int i = new Conexion().ModificarDatosPlan(new Plan_estudio(Integer.parseInt(id),Integer.parseInt(carrera),nombre,estado,vigente));
			if(i == 1)
			{
				mensaje = "El nuevo Plan fue modificado correctamente";
				request.setAttribute("mensaje", mensaje);
				request.setAttribute("planesList", new Conexion().getPlanes());
				request.getRequestDispatcher("vistas_admin/planes.jsp").forward(request, response);
			}
			else{
				mensaje = "Hubo un error al modificar el Plan";
				request.setAttribute("mensaje", mensaje);
				request.setAttribute("planesList", new Conexion().getPlanes());
				request.getRequestDispatcher("vistas_admin/planes.jsp").forward(request, response);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

}
