package com.icc.controlador;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.icc.modelo.Administrador;
import com.icc.utilidades.Conexion;
import com.icc.utilidades.PasswordGenerador;


/**
 * Servlet implementation class FormularioAdministrador
 */

@WebServlet("/FormularioAdministradorControlador")
public class FormularioAdministradorControlador extends HttpServlet {
	private static final long serialVersionUID = 1L;
	String mensaje="";
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FormularioAdministradorControlador() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("administradoresList",new Conexion().getAdministradores() );
		request.getRequestDispatcher("/vistas_admin/administradores.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String rut = request.getParameter("rut");
		String pass = PasswordGenerador.getPassword(
					  PasswordGenerador.MINUSCULAS+
					  PasswordGenerador.MAYUSCULAS+
					  PasswordGenerador.NUMEROS,10);
		String nombres = request.getParameter("nombre");
		String paterno = request.getParameter("paterno");
		String materno = request.getParameter("materno");
		String email = request.getParameter("email");
		String telefono = request.getParameter("telefono");
		String celular = request.getParameter("celular");
		String cargo = request.getParameter("cargo");
		String estado = request.getParameter("estado");
		//int ban1=1;
		int ban2=1;
		int ban3=1;
		int exito =0;
		if(new Conexion().verificar_usuario(rut, "Academico",0) == 1)
		{
			try {
				pass = new Conexion().getClave(rut, "Academico");
			} catch (SQLException e) {
				e.printStackTrace();
			}
			//ban1=0;
		}
		if(new Conexion().verificar_usuario(rut, "Alumno",0) == 1)
		{
			try {
				pass = new Conexion().getClave(rut, "Alumno");
			} catch (SQLException e) {
				e.printStackTrace();
			}
			ban2=0;
		}
		if(new Conexion().verificar_usuario(rut, "Supervisor",0) == 1 )
		{
			try {
				pass = new Conexion().getClave(rut, "Supervisor");
			} catch (SQLException e) {
				e.printStackTrace();
			}
			ban3=0;
		}
		try {	
			
			int i = new Conexion().agregarAdministrador(new Administrador(rut,pass,nombres,paterno,materno,email,telefono,celular,cargo,estado));
			if(i == 1)
			{
				/* estos dos if son para controlar a futuro que no haya un admin con perfil alumno y sup,
				 * si llegase a ocurrir alguno de los casos , los perfiles existentes se vuelven inactivos.
				 *  El academico no es considerado ya que si puede haber un admin con perfil de academico
				 */

				if(ban2==0)
				{
					new Conexion().ModificarEstadoUsuario(rut,"inactivo","Alumno");
				}
				if(ban3==0)
				{
					new Conexion().ModificarEstadoUsuario(rut,"inactivo","Supervisor");
				}
				mensaje = "El nuevo Administrador fue registrado correctamente";
				request.setAttribute("mensaje", mensaje);
				exito = 1;
				request.setAttribute("exito", exito);
				request.setAttribute("administradoresList",new Conexion().getAdministradores());
				request.getRequestDispatcher("vistas_admin/administradores.jsp").forward(request, response);
			}
			else{
				mensaje = "Hubo un error al registrar el nuevo Administrador";
				request.setAttribute("mensaje", mensaje);
				exito = 0;
				request.setAttribute("exito", exito);
				request.setAttribute("administradoresList",new Conexion().getAdministradores());
				request.getRequestDispatcher("vistas_admin/administradores.jsp").forward(request, response);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
