package com.icc.controlador;

import java.io.IOException;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


//import com.icc.modelo.Asignatura;
import com.icc.modelo.InscripcionAsignatura;
import com.icc.modelo.Practica;
//import com.icc.modelo.RolGestion;
import com.icc.utilidades.Conexion;

/**
 * Servlet implementation class FormularioPreInscripcionControlador
 */
@WebServlet("/FormularioPreInscripcionControlador")
public class FormularioPreInscripcionControlador extends HttpServlet {
	private static final long serialVersionUID = 1L;
	String mensaje="";
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FormularioPreInscripcionControlador() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {		
			//java.text.DateFormat fecha = new SimpleDateFormat("dd/MM/y");
			request.setCharacterEncoding("UTF-8");
			String rut = (String) request.getSession().getAttribute("rut");
			InscripcionAsignatura inscripcion = new Conexion().getInscripcion(rut); 
			request.setAttribute("inscripcion", inscripcion);
			request.setAttribute("asignatura", new Conexion().getAsignatura(inscripcion.getAsi_codigo(),inscripcion.getPlan_id()));
			request.setAttribute("empresasList", new Conexion().getEmpresas());
			//quizas se ocupa request.setAttribute("fecha",fecha.format(new java.util.Date()));
			request.setAttribute("comunaList", new Conexion().getComunas());
			// quizas se ocupa  request.setAttribute("gestionesList", new Conexion().getGestionesPosibles(codigo));
			request.setAttribute("regionList", new Conexion().getRegiones());
			request.setAttribute("obrasList", new Conexion().getObras());
			request.setAttribute("academicosList", new Conexion().getAcademicos());
			request.setAttribute("plan", new Conexion().getPlan(inscripcion.getPlan_id()));
			// quizas se ocupa  request.setAttribute("planesList", new Conexion().getPlanes());
			request.setAttribute("rut", request.getSession().getAttribute("rut"));
			response.setHeader("Cache-control", "no-cache, no-store, must-revalidate");
			request.getRequestDispatcher("/vistas_alumno/formulario_pre_inscripcion.jsp").forward(request, response);
			
			/*
			<select class="form-control" id="asignatura" name="asignatura">				
			<c:forEach var="asignaturas" items="${gestionesList}" >
					<option value="${asignaturas.asi_codigo}">${asignaturas.asi_nombre}</option>
			</c:forEach>
			<option selected disabled selected="selected">Seleccione Tipo de Gesti�n...</option>
			</select>
			
			<select class="form-control" id="plan" name="plan">				
									<c:forEach var="planes" items="${planesList}" >
											<option value="${planes.plan_id}">${planes.plan_nombre}</option>
									</c:forEach>
									<option selected disabled selected="selected">Seleccione Plan de Estudio...</option>	
								</select>			
								
			*/
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String empresa = request.getParameter("empresa");
		String supervisor = request.getParameter("supervisor");
		String obra = request.getParameter("obra");
		String alumno = request.getParameter("alumno");
		String asignatura = request.getParameter("gestion");
		String plan = request.getParameter("plan");
		String fecha_ins = request.getParameter("fecha_ins");
		String nombre_obra = request.getParameter("nombre_obra");
		String comuna= request.getParameter("comuna");
		String direccion= request.getParameter("direccion");
		String tareas = request.getParameter("tareas");
		String inicio = request.getParameter("inicio");
		String termino = request.getParameter("termino");
		String horas = request.getParameter("horas");
		String profesor_guia = request.getParameter("profesor");
		request.setCharacterEncoding("UTF-8");
		response.setHeader("Cache-control", "no-cache, no-store, must-revalidate");
		try
		{	
			int total = new Conexion().getTotalPracticasSemestre(alumno);
			if( total == 0)
			{
				Practica practica = new Practica(empresa,supervisor,Integer.parseInt(comuna),Integer.parseInt(obra),alumno,Integer.parseInt(asignatura),Integer.parseInt(plan),fecha_ins,nombre_obra,direccion,tareas,inicio,termino,Integer.parseInt(horas), profesor_guia);
				int i = new Conexion().AgregarPractica(practica);
				if(i == 1)
				{
					mensaje = "La Pre-Inscripción fue registrada correctamente, ahora debe esperar que el administrador la revise";
					request.setAttribute("mensaje", mensaje);
					request.setAttribute("exito", 1);
					String rut = (String) request.getSession().getAttribute("rut");
					request.setAttribute("practicaList",new Conexion().getHistorial(rut));
					request.getRequestDispatcher("vistas_alumno/historial.jsp").forward(request, response);
				}
				else{
					mensaje = "Hubo un error al registrar la Pre-Inscripción";
					request.setAttribute("mensaje", mensaje);
					request.setAttribute("exito", 0);
					String rut = (String) request.getSession().getAttribute("rut");
					request.setAttribute("practicaList",new Conexion().getHistorial(rut));
					request.getRequestDispatcher("vistas_alumno/historial.jsp").forward(request, response);
				}
			}else{
				mensaje = "La Pre-Inscripción no se ha registrado ya que usted tiene inscrita otra práctica";
				request.setAttribute("mensaje", mensaje);
				request.setAttribute("exito", 0);
				String rut = (String) request.getSession().getAttribute("rut");
				request.setAttribute("practicaList",new Conexion().getHistorial(rut));
				request.getRequestDispatcher("vistas_alumno/historial.jsp").forward(request, response);
			}
			
		} catch (Exception e) 
		{
			e.printStackTrace();
		}
	}

}
