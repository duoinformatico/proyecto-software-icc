package com.icc.modelo;

public class RolGestion {
	int asi_codigo;
	int plan_id;
	int rol_id;
	double rol_ponderacion;
	String rol_nombre;
	public RolGestion(int asi_codigo, int plan_id, int rol_id,
			double rol_ponderacion, String rol_nombre) {
		super();
		this.asi_codigo = asi_codigo;
		this.plan_id = plan_id;
		this.rol_id = rol_id;
		this.rol_ponderacion = rol_ponderacion;
		this.rol_nombre = rol_nombre;
	}
	public int getAsi_codigo() {
		return asi_codigo;
	}
	public void setAsi_codigo(int asi_codigo) {
		this.asi_codigo = asi_codigo;
	}
	public int getPlan_id() {
		return plan_id;
	}
	public void setPlan_id(int plan_id) {
		this.plan_id = plan_id;
	}
	public int getRol_id() {
		return rol_id;
	}
	public void setRol_id(int rol_id) {
		this.rol_id = rol_id;
	}
	public double getRol_ponderacion() {
		return rol_ponderacion;
	}
	public void setRol_ponderacion(double rol_ponderacion) {
		this.rol_ponderacion = rol_ponderacion;
	}
	public String getRol_nombre() {
		return rol_nombre;
	}
	public void setRol_nombre(String rol_nombre) {
		this.rol_nombre = rol_nombre;
	}
	
}
