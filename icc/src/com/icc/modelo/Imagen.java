package com.icc.modelo;

public class Imagen {
	int img_id;
	String img_url;
	String img_fecha;
	String img_hora;
	
	public Imagen(String img_url) {
		super();
		this.img_url = img_url;
	}
	public Imagen(int img_id, String img_url, String img_fecha, String img_hora) {
		super();
		this.img_id = img_id;
		this.img_url = img_url;
		this.img_fecha = img_fecha;
		this.img_hora = img_hora;
	}
	
	public int getImg_id() {
		return img_id;
	}
	public void setImg_id(int img_id) {
		this.img_id = img_id;
	}
	public String getImg_url() {
		return img_url;
	}
	public void setImg_url(String img_url) {
		this.img_url = img_url;
	}
	public String getImg_fecha() {
		return img_fecha;
	}
	public void setImg_fecha(String img_fecha) {
		this.img_fecha = img_fecha;
	}
	public String getImg_hora() {
		return img_hora;
	}
	public void setImg_hora(String img_hora) {
		this.img_hora = img_hora;
	}
	
}
