package com.icc.modelo;

public class Empresa {
	String emp_rut;
	int com_id;
	String emp_nombre;
	String emp_direccion;
	String emp_telefono;
	String emp_celular;
	String emp_descripcion;
	String emp_email;
	String emp_web;
	String bean_uno;
	// constructor para generar informe de empresas sacando total de alumnos
	public Empresa(String emp_nombre, String bean_uno) {
		super();
		this.emp_nombre = emp_nombre;
		this.bean_uno = bean_uno;
	}
	// constructor para registrar una empresa ene l sistema
	public Empresa(String emp_rut, int com_id, String emp_nombre,
			String emp_direccion, String emp_telefono, String emp_celular,
			String emp_descripcion, String emp_email, String emp_web) 
	{
		super();
		this.emp_rut = emp_rut;
		this.com_id = com_id;
		this.emp_nombre = emp_nombre;
		this.emp_direccion = emp_direccion;
		this.emp_telefono = emp_telefono;
		this.emp_celular = emp_celular;
		this.emp_descripcion = emp_descripcion;
		this.emp_email = emp_email;
		this.emp_web = emp_web;
	}

	public Empresa()
	{
		
	}
	public String getBean_uno() {
		return bean_uno;
	}
	public void setBean_uno(String bean_uno) {
		this.bean_uno = bean_uno;
	}
	public String getEmp_rut() {
		return emp_rut;
	}
	public void setEmp_rut(String emp_rut) {
		this.emp_rut = emp_rut;
	}
	public int getCom_id() {
		return com_id;
	}
	public void setCom_id(int com_id) {
		this.com_id = com_id;
	}
	public String getEmp_nombre() {
		return emp_nombre;
	}
	public void setEmp_nombre(String emp_nombre) {
		this.emp_nombre = emp_nombre;
	}
	public String getEmp_direccion() {
		return emp_direccion;
	}
	public void setEmp_direccion(String emp_direccion) {
		this.emp_direccion = emp_direccion;
	}
	public String getEmp_telefono() {
		return emp_telefono;
	}
	public void setEmp_telefono(String emp_telefono) {
		this.emp_telefono = emp_telefono;
	}
	public String getEmp_celular() {
		return emp_celular;
	}
	public void setEmp_celular(String emp_celular) {
		this.emp_celular = emp_celular;
	}
	public String getEmp_descripcion() {
		return emp_descripcion;
	}
	public void setEmp_descripcion(String emp_descripcion) {
		this.emp_descripcion = emp_descripcion;
	}
	public String getEmp_email() {
		return emp_email;
	}
	public void setEmp_email(String emp_email) {
		this.emp_email = emp_email;
	}
	public String getEmp_web() {
		return emp_web;
	}
	public void setEmp_web(String emp_web) {
		this.emp_web = emp_web;
	}
}
