package com.icc.modelo;

public class Especialidad {
	int esp_id;
	String esp_nombre;
	
	public Especialidad(int esp_id, String esp_nombre) {
		super();
		this.esp_id = esp_id;
		this.esp_nombre = esp_nombre;
	}
	public Especialidad(String esp_nombre) {
		super();
		this.esp_nombre = esp_nombre;
	}
	public Especialidad(int esp_id) {
		super();
		this.esp_id = esp_id;
	}
	public int getEsp_id() {
		return esp_id;
	}
	public void setEsp_id(int esp_id) {
		this.esp_id = esp_id;
	}
	public String getEsp_nombre() {
		return esp_nombre;
	}
	public void setEsp_nombre(String esp_nombre) {
		this.esp_nombre = esp_nombre;
	}
	
}
