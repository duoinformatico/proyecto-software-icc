package com.icc.modelo;

public class Area {
	int area_id;
	String area_nombre;
	String bean_dos;
	//constructor para generar informes de area
	public Area(String area_nombre, String bean_dos) {
		super();
		this.area_nombre = area_nombre;
		this.bean_dos = bean_dos;
	}
	// constructor para agregar nueva area
	public Area(String area_nombre) {
		super();
		this.area_nombre = area_nombre;
	}
	//Constructor para modificar un area
	public Area(int area_id, String area_nombre) {
		super();
		this.area_id = area_id;
		this.area_nombre = area_nombre;
	}
	public String getBean_dos() {
		return bean_dos;
	}
	public void setBean_dos(String bean_dos) {
		this.bean_dos = bean_dos;
	}
	public int getArea_id() {
		return area_id;
	}
	public void setArea_id(int area_id) {
		this.area_id = area_id;
	}
	public String getArea_nombre() {
		return area_nombre;
	}
	public void setArea_nombre(String area_nombre) {
		this.area_nombre = area_nombre;
	}
	
}
