package com.icc.modelo;


public class Comuna {
	int com_id;
	int pro_id;
	String com_nombre;
	String bean_dos;
	//constructor para crear generar informe de comuna
	public Comuna(String com_nombre, String bean_dos) {
		super();
		this.com_nombre = com_nombre;
		this.bean_dos = bean_dos;
	}
	
	public Comuna(int com_id, int pro_id, String com_nombre) {
		super();
		this.com_id = com_id;
		this.pro_id = pro_id;
		this.com_nombre = com_nombre;
	}
	
	public Comuna(){
		
	}
	
	public String getBean_dos() {
		return bean_dos;
	}
	public void setBean_dos(String bean_dos) {
		this.bean_dos = bean_dos;
	}
	public int getCom_id() {
		return com_id;
	}
	public void setCom_id(int com_id) {
		this.com_id = com_id;
	}
	public int getPro_id() {
		return pro_id;
	}
	public void setPro_id(int pro_id) {
		this.pro_id = pro_id;
	}
	public String getCom_nombre() {
		return com_nombre;
	}
	public void setCom_nombre(String com_nombre) {
		this.com_nombre = com_nombre;
	}
}
