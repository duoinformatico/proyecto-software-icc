package com.icc.modelo;

public class RolTipoParticipantes {
	int rol_id;
	String rol_descripcion;
	public RolTipoParticipantes(int rol_id, String rol_descripcion) {
		super();
		this.rol_id = rol_id;
		this.rol_descripcion = rol_descripcion;
	}
	public int getRol_id() {
		return rol_id;
	}
	public void setRol_id(int rol_id) {
		this.rol_id = rol_id;
	}
	public String getRol_descripcion() {
		return rol_descripcion;
	}
	public void setRol_descripcion(String rol_descripcion) {
		this.rol_descripcion = rol_descripcion;
	}
	
	
}
