package com.icc.modelo;

public class Supervisor {
	String sup_rut;
	String sup_password;
	String sup_nombres;
	String sup_apellido_p;
	String sup_apellido_m;
	String sup_email;
	String sup_telefono;
	String sup_celular;
	String sup_profesion;
	String sup_cargo;
	String sup_estado;
	String bean_uno;
	// constructor para generar informes
	public Supervisor(String sup_nombres, String bean_uno) {
		super();
		this.sup_nombres = sup_nombres;
		this.bean_uno = bean_uno;
	}
	
	public Supervisor(String sup_rut, String sup_password, String sup_nombres,
			String sup_apellido_p, String sup_apellido_m, String sup_email,
			String sup_telefono, String sup_celular, String sup_profesion,
			String sup_cargo, String sup_estado) {
		super();
		this.sup_rut = sup_rut;
		this.sup_password = sup_password;
		this.sup_nombres = sup_nombres;
		this.sup_apellido_p = sup_apellido_p;
		this.sup_apellido_m = sup_apellido_m;
		this.sup_email = sup_email;
		this.sup_telefono = sup_telefono;
		this.sup_celular = sup_celular;
		this.sup_profesion = sup_profesion;
		this.sup_cargo = sup_cargo;
		this.sup_estado = sup_estado;
	}
	
	//constructor para modificar supervisor
	public Supervisor(String sup_rut, String sup_nombres,
			String sup_apellido_p, String sup_apellido_m, String sup_email,
			String sup_telefono, String sup_celular, String sup_profesion,
			String sup_cargo, String sup_estado) {
		super();
		this.sup_rut = sup_rut;
		this.sup_nombres = sup_nombres;
		this.sup_apellido_p = sup_apellido_p;
		this.sup_apellido_m = sup_apellido_m;
		this.sup_email = sup_email;
		this.sup_telefono = sup_telefono;
		this.sup_celular = sup_celular;
		this.sup_profesion = sup_profesion;
		this.sup_cargo = sup_cargo;
		this.sup_estado = sup_estado;
	}
	// constructor para modificar perfil
	public Supervisor(String sup_rut, String sup_telefono, String sup_celular,
			String sup_profesion, String sup_cargo) {
		super();
		this.sup_rut = sup_rut;
		this.sup_telefono = sup_telefono;
		this.sup_celular = sup_celular;
		this.sup_profesion = sup_profesion;
		this.sup_cargo = sup_cargo;
	}
	
	public String getSup_rut() {
		return sup_rut;
	}
	public void setSup_rut(String sup_rut) {
		this.sup_rut = sup_rut;
	}
	public String getSup_password() {
		return sup_password;
	}
	public void setSup_password(String sup_password) {
		this.sup_password = sup_password;
	}
	public String getSup_nombres() {
		return sup_nombres;
	}
	public void setSup_nombres(String sup_nombres) {
		this.sup_nombres = sup_nombres;
	}
	public String getSup_apellido_p() {
		return sup_apellido_p;
	}
	public void setSup_apellido_p(String sup_apellido_p) {
		this.sup_apellido_p = sup_apellido_p;
	}
	public String getSup_apellido_m() {
		return sup_apellido_m;
	}
	public void setSup_apellido_m(String sup_apellido_m) {
		this.sup_apellido_m = sup_apellido_m;
	}
	public String getSup_email() {
		return sup_email;
	}
	public void setSup_email(String sup_email) {
		this.sup_email = sup_email;
	}
	public String getSup_telefono() {
		return sup_telefono;
	}
	public void setSup_telefono(String sup_telefono) {
		this.sup_telefono = sup_telefono;
	}
	public String getSup_celular() {
		return sup_celular;
	}
	public void setSup_celular(String sup_celular) {
		this.sup_celular = sup_celular;
	}
	public String getSup_profesion() {
		return sup_profesion;
	}
	public void setSup_profesion(String sup_profesion) {
		this.sup_profesion = sup_profesion;
	}
	public String getSup_cargo() {
		return sup_cargo;
	}
	public void setSup_cargo(String sup_cargo) {
		this.sup_cargo = sup_cargo;
	}
	public String getSup_estado() {
		return sup_estado;
	}
	public void setSup_estado(String sup_estado) {
		this.sup_estado = sup_estado;
	}

	public String getBean_uno() {
		return bean_uno;
	}

	public void setBean_uno(String bean_uno) {
		this.bean_uno = bean_uno;
	}
	
}
