package com.icc.modelo;

public class AreaPractica {
	int pra_id;
	int area_id;
	public AreaPractica(int pra_id, int area_id) {
		super();
		this.pra_id = pra_id;
		this.area_id = area_id;
	}
	public int getPra_id() {
		return pra_id;
	}
	public void setPra_id(int pra_id) {
		this.pra_id = pra_id;
	}
	public int getArea_id() {
		return area_id;
	}
	public void setArea_id(int area_id) {
		this.area_id = area_id;
	}
	
}
