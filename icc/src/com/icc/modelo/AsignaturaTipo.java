package com.icc.modelo;

public class AsignaturaTipo {
	int asi_tp_id;
	String asi_tp_nombre;	
	
	public AsignaturaTipo(int asi_tp_id, String asi_tp_nombre) {
		super();
		this.asi_tp_id = asi_tp_id;
		this.asi_tp_nombre = asi_tp_nombre;
	}
	
	public int getAsi_tp_id() {
		return asi_tp_id;
	}
	public void setAsi_tp_id(int asi_tp_id) {
		this.asi_tp_id = asi_tp_id;
	}
	public String getAsi_tp_nombre() {
		return asi_tp_nombre;
	}
	public void setAsi_tp_nombre(String asi_tp_nombre) {
		this.asi_tp_nombre = asi_tp_nombre;
	}
	
	
}
