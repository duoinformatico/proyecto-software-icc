package com.icc.modelo;

import java.util.ArrayList;

public class Instrumento {
	int ins_id;
	int esc_id;
	int rol_id;
	String ins_nombre;
	int ins_num_item;
	double ins_max_puntaje;
	String ins_descripcion;
	String ins_fecha_ini;
	String ins_fecha_fin;
	String ins_estado;
	
	ArrayList<Item> items;//join - relacion 'un instrumento tiene muchos items'
	
	
	
	public Instrumento(int ins_id, int esc_id, int rol_id, String ins_nombre, int ins_num_item,
			double ins_max_puntaje, String ins_descripcion, String ins_estado,
			ArrayList<Item> items) {
		super();
		this.ins_id = ins_id;
		this.esc_id = esc_id;
		this.rol_id = rol_id;
		this.ins_nombre = ins_nombre;
		this.ins_num_item = ins_num_item;
		this.ins_max_puntaje = ins_max_puntaje;
		this.ins_descripcion = ins_descripcion;
		this.ins_estado = ins_estado;
		this.items = items;
	}

	public Instrumento(int ins_id, int esc_id, int rol_id, String ins_nombre, int ins_num_item,
			double ins_max_puntaje, ArrayList<Item> items) {
		super();
		this.ins_id = ins_id;
		this.esc_id = esc_id;
		this.rol_id = rol_id;
		this.ins_nombre = ins_nombre;
		this.ins_num_item = ins_num_item;
		this.ins_max_puntaje = ins_max_puntaje;
		this.items = items;
	}	
	
	public Instrumento(int ins_id, int esc_id, int rol_id, String ins_nombre, int ins_num_item,
			double ins_max_puntaje) {
		super();
		this.ins_id = ins_id;
		this.esc_id = esc_id;
		this.rol_id = rol_id;
		this.ins_nombre = ins_nombre;
		this.ins_num_item = ins_num_item;
		this.ins_max_puntaje = ins_max_puntaje;
	}
	
	//constructor utilizado para poder agregar un instrumento a la BD
	public Instrumento(int esc_id, int rol_id, String ins_nombre,
			int ins_num_item, double ins_max_puntaje, String ins_descripcion) {
		super();
		this.esc_id = esc_id;
		this.rol_id = rol_id;
		this.ins_nombre = ins_nombre;
		this.ins_num_item = ins_num_item;
		this.ins_max_puntaje = ins_max_puntaje;
		this.ins_descripcion = ins_descripcion;
	}

	public Instrumento(int ins_id, int esc_id, int rol_id, String ins_nombre,
			int ins_num_item, double ins_max_puntaje, String ins_descripcion,
			String ins_fecha_ini, String ins_fecha_fin, String ins_estado,
			ArrayList<Item> items) {
		super();
		this.ins_id = ins_id;
		this.esc_id = esc_id;
		this.rol_id = rol_id;
		this.ins_nombre = ins_nombre;
		this.ins_num_item = ins_num_item;
		this.ins_max_puntaje = ins_max_puntaje;
		this.ins_descripcion = ins_descripcion;
		this.ins_fecha_ini = ins_fecha_ini;
		this.ins_fecha_fin = ins_fecha_fin;
		this.ins_estado = ins_estado;
		this.items = items;
	}
	
	public Instrumento(int ins_id, int esc_id, int rol_id, String ins_nombre,
			int ins_num_item, double ins_max_puntaje, String ins_descripcion,
			String ins_fecha_ini, String ins_fecha_fin, String ins_estado) {
		super();
		this.ins_id = ins_id;
		this.esc_id = esc_id;
		this.rol_id = rol_id;
		this.ins_nombre = ins_nombre;
		this.ins_num_item = ins_num_item;
		this.ins_max_puntaje = ins_max_puntaje;
		this.ins_descripcion = ins_descripcion;
		this.ins_fecha_ini = ins_fecha_ini;
		this.ins_fecha_fin = ins_fecha_fin;
		this.ins_estado = ins_estado;
	}

	public int getIns_id() {
		return ins_id;
	}

	public void setIns_id(int ins_id) {
		this.ins_id = ins_id;
	}

	public int getEsc_id() {
		return esc_id;
	}

	public void setEsc_id(int esc_id) {
		this.esc_id = esc_id;
	}

	public int getRol_id() {
		return rol_id;
	}

	public void setRol_id(int rol_id) {
		this.rol_id = rol_id;
	}

	public String getIns_nombre() {
		return ins_nombre;
	}

	public void setIns_nombre(String ins_nombre) {
		this.ins_nombre = ins_nombre;
	}

	public int getIns_num_item() {
		return ins_num_item;
	}

	public void setIns_num_item(int ins_num_item) {
		this.ins_num_item = ins_num_item;
	}

	public double getIns_max_puntaje() {
		return ins_max_puntaje;
	}

	public void setIns_max_puntaje(double ins_max_puntaje) {
		this.ins_max_puntaje = ins_max_puntaje;
	}
	public ArrayList<Item> getItems() {
		return items;
	}
	public void setItems(ArrayList<Item> items) {
		this.items = items;
	}
	public String getIns_descripcion() {
		return ins_descripcion;
	}
	public void setIns_descripcion(String ins_descripcion) {
		this.ins_descripcion = ins_descripcion;
	}
	public String getIns_estado() {
		return ins_estado;
	}
	public void setIns_estado(String ins_estado) {
		this.ins_estado = ins_estado;
	}
	public String getIns_fecha_ini() {
		return ins_fecha_ini;
	}
	public void setIns_fecha_ini(String ins_fecha_ini) {
		this.ins_fecha_ini = ins_fecha_ini;
	}
	public String getIns_fecha_fin() {
		return ins_fecha_fin;
	}
	public void setIns_fecha_fin(String ins_fecha_fin) {
		this.ins_fecha_fin = ins_fecha_fin;
	}	
	
}
