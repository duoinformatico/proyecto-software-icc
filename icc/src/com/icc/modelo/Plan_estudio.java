package com.icc.modelo;

public class Plan_estudio {
	int plan_id;
	int car_codigo;
	String plan_nombre;
	String plan_fecha;
	String plan_estado;
	boolean plan_vigente;
	
	public Plan_estudio(int plan_id, String plan_nombre) {
		super();
		this.plan_id = plan_id;
		this.plan_nombre = plan_nombre;
	}
	
	public Plan_estudio(int plan_id ,int car_codigo, String plan_nombre, String plan_estado,
			boolean plan_vigente) {
		super();
		this.plan_id = plan_id;
		this.car_codigo = car_codigo;
		this.plan_nombre = plan_nombre;
		this.plan_estado = plan_estado;
		this.plan_vigente = plan_vigente;
	}

	public Plan_estudio(int plan_id, int car_codigo, String plan_nombre,
			String plan_fecha, String plan_estado, boolean plan_vigente) {
		super();
		this.plan_id = plan_id;
		this.car_codigo = car_codigo;
		this.plan_nombre = plan_nombre;
		this.plan_fecha = plan_fecha;
		this.plan_estado = plan_estado;
		this.plan_vigente = plan_vigente;
	}
	public Plan_estudio(int plan_id, int car_codigo, String plan_nombre,
			String plan_fecha, String plan_estado) {
		super();
		this.plan_id = plan_id;
		this.car_codigo = car_codigo;
		this.plan_nombre = plan_nombre;
		this.plan_fecha = plan_fecha;
		this.plan_estado = plan_estado;
	}
	public Plan_estudio(String plan_nombre,int car_codigo) {
		super();
		this.car_codigo = car_codigo;
		this.plan_nombre = plan_nombre;
	}
	public int getPlan_id() {
		return plan_id;
	}
	public void setPlan_id(int plan_id) {
		this.plan_id = plan_id;
	}
	public int getCar_codigo() {
		return car_codigo;
	}
	public void setCar_codigo(int car_codigo) {
		this.car_codigo = car_codigo;
	}
	public String getPlan_nombre() {
		return plan_nombre;
	}
	public void setPlan_nombre(String plan_nombre) {
		this.plan_nombre = plan_nombre;
	}
	public String getPlan_fecha() {
		return plan_fecha;
	}
	public void setPlan_fecha(String plan_fecha) {
		this.plan_fecha = plan_fecha;
	}
	public String getPlan_estado() {
		return plan_estado;
	}
	public void setPlan_estado(String plan_estado) {
		this.plan_estado = plan_estado;
	}
	public boolean isPlan_vigente() {
		return plan_vigente;
	}
	public void setPlan_vigente(boolean plan_vigente) {
		this.plan_vigente = plan_vigente;
	}
	
}
