package com.icc.modelo;

public class FuncionPractica {
	int pra_id;
	int fun_id;
		
	public FuncionPractica(int pra_id, int fun_id) {
		super();
		this.pra_id = pra_id;
		this.fun_id = fun_id;
	}

	public int getPra_id() {
		return pra_id;
	}

	public void setPra_id(int pra_id) {
		this.pra_id = pra_id;
	}

	public int getFun_id() {
		return fun_id;
	}

	public void setFun_id(int fun_id) {
		this.fun_id = fun_id;
	}
	
	
}
