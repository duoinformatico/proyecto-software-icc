package com.icc.modelo;

public class Opcion {
	int opc_id;
	int item_id;
	String opc_descripcion;
	double opc_valor;
	
	public Opcion(int opc_id, int item_id, String opc_descripcion,
			double opc_valor) {
		super();
		this.opc_id = opc_id;
		this.item_id = item_id;
		this.opc_descripcion = opc_descripcion;
		this.opc_valor = opc_valor;
	}
	
	public int getOpc_id() {
		return opc_id;
	}
	public void setOpc_id(int opc_id) {
		this.opc_id = opc_id;
	}
	public int getItem_id() {
		return item_id;
	}
	public void setItem_id(int item_id) {
		this.item_id = item_id;
	}
	public String getOpc_descripcion() {
		return opc_descripcion;
	}
	public void setOpc_descripcion(String opc_descripcion) {
		this.opc_descripcion = opc_descripcion;
	}
	public double getOpc_valor() {
		return opc_valor;
	}
	public void setOpc_valor(double opc_valor) {
		this.opc_valor = opc_valor;
	}
}
