package com.icc.modelo;

public class Evaluacion {
	int eva_id;
	int eval_clave;
	int pra_id;
	String eva_nombre;
	double eva_nota;
	String eva_observacion;
	String eva_fecha;
	String eva_hora;
	String eva_estado;
	public Evaluacion(int eva_id, int eval_clave, int pra_id,
			String eva_nombre, double eva_nota, String eva_observacion,
			String eva_fecha, String eva_hora, String eva_estado) {
		super();
		this.eva_id = eva_id;
		this.eval_clave = eval_clave;
		this.pra_id = pra_id;
		this.eva_nombre = eva_nombre;
		this.eva_nota = eva_nota;
		this.eva_observacion = eva_observacion;
		this.eva_fecha = eva_fecha;
		this.eva_hora = eva_hora;
		this.eva_estado = eva_estado;
	}
	public int getEva_id() {
		return eva_id;
	}
	public void setEva_id(int eva_id) {
		this.eva_id = eva_id;
	}
	public int getEval_clave() {
		return eval_clave;
	}
	public void setEval_clave(int eval_clave) {
		this.eval_clave = eval_clave;
	}
	public int getPra_id() {
		return pra_id;
	}
	public void setPra_id(int pra_id) {
		this.pra_id = pra_id;
	}
	public String getEva_nombre() {
		return eva_nombre;
	}
	public void setEva_nombre(String eva_nombre) {
		this.eva_nombre = eva_nombre;
	}
	public double getEva_nota() {
		return eva_nota;
	}
	public void setEva_nota(double eva_nota) {
		this.eva_nota = eva_nota;
	}
	public String getEva_observacion() {
		return eva_observacion;
	}
	public void setEva_observacion(String eva_observacion) {
		this.eva_observacion = eva_observacion;
	}
	public String getEva_fecha() {
		return eva_fecha;
	}
	public void setEva_fecha(String eva_fecha) {
		this.eva_fecha = eva_fecha;
	}
	public String getEva_hora() {
		return eva_hora;
	}
	public void setEva_hora(String eva_hora) {
		this.eva_hora = eva_hora;
	}
	public String getEva_estado() {
		return eva_estado;
	}
	public void setEva_estado(String eva_estado) {
		this.eva_estado = eva_estado;
	}
	
}
