package com.icc.modelo;

public class Academico {
	String aca_rut;
	int dep_id;
	String aca_password;
	String aca_nombres;
	String aca_apellido_p;
	String aca_apellido_m;
	String aca_email;
	String aca_telefono;
	String aca_celular;
	String aca_estado;
	int aca_n_especialidad;
	int bean_uno;//para obtener el numero de evaluaciones activas
	String bean_dos;
	
	public Academico()
	{
		
	}
	// constructor para generar informe de academicos
	public Academico(String aca_nombres, String bean_dos) {
		super();
		this.aca_nombres = aca_nombres;
		this.bean_dos = bean_dos;
	}
	public Academico(String aca_rut, int dep_id, String aca_password,
			String aca_nombres, String aca_apellido_p, String aca_apellido_m,
			String aca_email, String aca_telefono, String aca_celular,
			String aca_estado) {
		super();
		this.aca_rut = aca_rut;
		this.dep_id = dep_id;
		this.aca_password = aca_password;
		this.aca_nombres = aca_nombres;
		this.aca_apellido_p = aca_apellido_p;
		this.aca_apellido_m = aca_apellido_m;
		this.aca_email = aca_email;
		this.aca_telefono = aca_telefono;
		this.aca_celular = aca_celular;
		this.aca_estado = aca_estado;
	}
	
	//constructor para modificar academico y mostras datos de academico
	public Academico(String aca_rut, int dep_id,
			String aca_nombres, String aca_apellido_p, String aca_apellido_m,
			String aca_email, String aca_telefono, String aca_celular,
			String aca_estado) {
		super();
		this.aca_rut = aca_rut;
		this.dep_id = dep_id;
		this.aca_nombres = aca_nombres;
		this.aca_apellido_p = aca_apellido_p;
		this.aca_apellido_m = aca_apellido_m;
		this.aca_email = aca_email;
		this.aca_telefono = aca_telefono;
		this.aca_celular = aca_celular;
		this.aca_estado = aca_estado;
	}
	//crea un obj academico + bean_uno con total de evaluaciones activas
	public Academico(String aca_rut, int dep_id, String aca_password,
			String aca_nombres, String aca_apellido_p, String aca_apellido_m,
			String aca_email, String aca_telefono, String aca_celular,
			String aca_estado, int aca_n_especialidad, int bean_uno) {
		super();
		this.aca_rut = aca_rut;
		this.dep_id = dep_id;
		this.aca_password = aca_password;
		this.aca_nombres = aca_nombres;
		this.aca_apellido_p = aca_apellido_p;
		this.aca_apellido_m = aca_apellido_m;
		this.aca_email = aca_email;
		this.aca_telefono = aca_telefono;
		this.aca_celular = aca_celular;
		this.aca_estado = aca_estado;
		this.aca_n_especialidad = aca_n_especialidad;
		this.bean_uno = bean_uno;
	}
	// constructor para modificar perfil
	public Academico(String aca_rut, String aca_telefono, String aca_celular) {
		super();
		this.aca_rut = aca_rut;
		this.aca_telefono = aca_telefono;
		this.aca_celular = aca_celular;
	}
	public String getAca_rut() {
		return aca_rut;
	}
	public void setAca_rut(String aca_rut) {
		this.aca_rut = aca_rut;
	}
	public int getDep_id() {
		return dep_id;
	}
	public void setDep_id(int dep_id) {
		this.dep_id = dep_id;
	}
	public String getAca_password() {
		return aca_password;
	}
	public void setAca_password(String aca_password) {
		this.aca_password = aca_password;
	}
	public String getAca_nombres() {
		return aca_nombres;
	}
	public void setAca_nombres(String aca_nombres) {
		this.aca_nombres = aca_nombres;
	}
	public String getAca_apellido_p() {
		return aca_apellido_p;
	}
	public void setAca_apellido_p(String aca_apellido_p) {
		this.aca_apellido_p = aca_apellido_p;
	}
	public String getAca_apellido_m() {
		return aca_apellido_m;
	}
	public void setAca_apellido_m(String aca_apellido_m) {
		this.aca_apellido_m = aca_apellido_m;
	}
	public String getAca_email() {
		return aca_email;
	}
	public void setAca_email(String aca_email) {
		this.aca_email = aca_email;
	}
	public String getAca_telefono() {
		return aca_telefono;
	}
	public void setAca_telefono(String aca_telefono) {
		this.aca_telefono = aca_telefono;
	}
	public String getAca_celular() {
		return aca_celular;
	}
	public void setAca_celular(String aca_celular) {
		this.aca_celular = aca_celular;
	}
	public String getAca_estado() {
		return aca_estado;
	}
	public void setAca_estado(String aca_estado) {
		this.aca_estado = aca_estado;
	}
	public int getAca_n_especialidad() {
		return aca_n_especialidad;
	}
	public void setAca_n_especialidad(int aca_n_especialidad) {
		this.aca_n_especialidad = aca_n_especialidad;
	}
	public int getBean_uno() {
		return bean_uno;
	}
	public void setBean_uno(int bean_uno) {
		this.bean_uno = bean_uno;
	}
	public String getBean_dos() {
		return bean_dos;
	}
	public void setBean_dos(String bean_dos) {
		this.bean_dos = bean_dos;
	}
}
