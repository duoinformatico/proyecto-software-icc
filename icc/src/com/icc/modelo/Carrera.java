package com.icc.modelo;

public class Carrera {
	int car_codigo;
	String car_nombre;
	String car_sede;
	
	public Carrera(int car_codigo, String car_nombre, String car_sede) {
		super();
		this.car_codigo = car_codigo;
		this.car_nombre = car_nombre;
		this.car_sede = car_sede;
	}
	public int getCar_codigo() {
		return car_codigo;
	}
	public void setCar_codigo(int car_codigo) {
		this.car_codigo = car_codigo;
	}
	public String getCar_nombre() {
		return car_nombre;
	}
	public void setCar_nombre(String car_nombre) {
		this.car_nombre = car_nombre;
	}
	public String getCar_sede() {
		return car_sede;
	}
	public void setCar_sede(String car_sede) {
		this.car_sede = car_sede;
	}
	
}
