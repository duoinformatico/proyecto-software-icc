package com.icc.modelo;

public class Evaluadores {
	int eval_clave;
	int asi_codigo;
	int plan_id;
	int rol_id;
	String aca_rut;
	String sup_rut;
	String alu_rut;
	String eval_fecha;
	String eval_estado;
	public Evaluadores(int eval_clave, int asi_codigo, int plan_id, int rol_id,
			String aca_rut, String sup_rut, String alu_rut, String eval_fecha,
			String eval_estado) {
		super();
		this.eval_clave = eval_clave;
		this.asi_codigo = asi_codigo;
		this.plan_id = plan_id;
		this.rol_id = rol_id;
		this.aca_rut = aca_rut;
		this.sup_rut = sup_rut;
		this.alu_rut = alu_rut;
		this.eval_fecha = eval_fecha;
		this.eval_estado = eval_estado;
	}
	public int getEval_clave() {
		return eval_clave;
	}
	public void setEval_clave(int eval_clave) {
		this.eval_clave = eval_clave;
	}
	public int getAsi_codigo() {
		return asi_codigo;
	}
	public void setAsi_codigo(int asi_codigo) {
		this.asi_codigo = asi_codigo;
	}
	public int getPlan_id() {
		return plan_id;
	}
	public void setPlan_id(int plan_id) {
		this.plan_id = plan_id;
	}
	public int getRol_id() {
		return rol_id;
	}
	public void setRol_id(int rol_id) {
		this.rol_id = rol_id;
	}
	public String getAca_rut() {
		return aca_rut;
	}
	public void setAca_rut(String aca_rut) {
		this.aca_rut = aca_rut;
	}
	public String getSup_rut() {
		return sup_rut;
	}
	public void setSup_rut(String sup_rut) {
		this.sup_rut = sup_rut;
	}
	public String getAlu_rut() {
		return alu_rut;
	}
	public void setAlu_rut(String alu_rut) {
		this.alu_rut = alu_rut;
	}
	public String getEval_fecha() {
		return eval_fecha;
	}
	public void setEval_fecha(String eval_fecha) {
		this.eval_fecha = eval_fecha;
	}
	public String getEval_estado() {
		return eval_estado;
	}
	public void setEval_estado(String eval_estado) {
		this.eval_estado = eval_estado;
	}
	
}
