package com.icc.modelo;

public class HistorialSupervisor {
	String sup_rut;
	String emp_rut;
	String his_fecha;
	String his_estado;
	boolean his_vigencia;
	
	
	public HistorialSupervisor(String sup_rut, String emp_rut,
			String his_estado, boolean his_vigencia) {
		super();
		this.sup_rut = sup_rut;
		this.emp_rut = emp_rut;
		this.his_estado = his_estado;
		this.his_vigencia = his_vigencia;
	}
	public HistorialSupervisor(String sup_rut, String emp_rut,
			String his_fecha, String his_estado, boolean his_vigencia) {
		super();
		this.sup_rut = sup_rut;
		this.emp_rut = emp_rut;
		this.his_fecha = his_fecha;
		this.his_estado = his_estado;
		this.his_vigencia = his_vigencia;
	}
	public String getSup_rut() {
		return sup_rut;
	}
	public void setSup_rut(String sup_rut) {
		this.sup_rut = sup_rut;
	}
	public String getEmp_rut() {
		return emp_rut;
	}
	public void setEmp_rut(String emp_rut) {
		this.emp_rut = emp_rut;
	}
	public String getHis_fecha() {
		return his_fecha;
	}
	public void setHis_fecha(String his_fecha) {
		this.his_fecha = his_fecha;
	}
	public String getHis_estado() {
		return his_estado;
	}
	public void setHis_estado(String his_estado) {
		this.his_estado = his_estado;
	}
	public boolean isHis_vigencia() {
		return his_vigencia;
	}
	public void setHis_vigencia(boolean his_vigencia) {
		this.his_vigencia = his_vigencia;
	}
	
}
