package com.icc.modelo;

public class DetalleEvaluacion {
	int eva_id;
	int item_id;
	int opc_id;
	public DetalleEvaluacion(int eva_id, int item_id, int opc_id) {
		super();
		this.eva_id = eva_id;
		this.item_id = item_id;
		this.opc_id = opc_id;
	}
	public int getEva_id() {
		return eva_id;
	}
	public void setEva_id(int eva_id) {
		this.eva_id = eva_id;
	}
	public int getItem_id() {
		return item_id;
	}
	public void setItem_id(int item_id) {
		this.item_id = item_id;
	}
	public int getOpc_id() {
		return opc_id;
	}
	public void setOpc_id(int opc_id) {
		this.opc_id = opc_id;
	}
	

}
