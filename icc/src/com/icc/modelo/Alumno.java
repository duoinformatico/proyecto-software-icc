package com.icc.modelo;

public class Alumno {
	String alu_rut;
	String alu_password;
	String alu_nombres;
	String alu_apellido_p;
	String alu_apellido_m;
	String alu_email;
	String alu_celular;
	String alu_telefono;
	String alu_estado;
	int bean;
	
	public Alumno(String alu_rut) {
		super();
		this.alu_rut = alu_rut;
	}
	// Constructor para modificar informacion personal
	public Alumno(String alu_rut,String alu_celular, String alu_telefono) {
		super();
		this.alu_rut = alu_rut;
		this.alu_celular = alu_celular;
		this.alu_telefono = alu_telefono;
	}
	// para CARGAR ALUMNO
		public Alumno(String alu_rut, String alu_password, String alu_nombres,
				String alu_apellido_p, String alu_apellido_m, String alu_email,
				String alu_estado, int bean) {
			super();
			this.alu_rut = alu_rut;
			this.alu_password = alu_password;
			this.alu_nombres = alu_nombres;
			this.alu_apellido_p = alu_apellido_p;
			this.alu_apellido_m = alu_apellido_m;
			this.alu_email = alu_email;
			this.alu_estado = alu_estado;
		}
	//Constructor para obtener info del alumno
	public Alumno(String alu_rut, String alu_nombres, String alu_apellido_p,
			String alu_apellido_m, String alu_email, String alu_celular,
			String alu_telefono) {
		super();
		this.alu_rut = alu_rut;
		this.alu_nombres = alu_nombres;
		this.alu_apellido_p = alu_apellido_p;
		this.alu_apellido_m = alu_apellido_m;
		this.alu_email = alu_email;
		this.alu_celular = alu_celular;
		this.alu_telefono = alu_telefono;
	}
	// para insertar alumno
	public Alumno(String alu_rut, String alu_nombres, String alu_apellido_p,
			String alu_apellido_m, String alu_email) {
		super();
		this.alu_rut = alu_rut;
		this.alu_nombres = alu_nombres;
		this.alu_apellido_p = alu_apellido_p;
		this.alu_apellido_m = alu_apellido_m;
		this.alu_email = alu_email;
	}
	
	public String getAlu_rut() {
		return alu_rut;
	}
	
	public void setAlu_rut(String alu_rut) {
		this.alu_rut = alu_rut;
	}
	public String getAlu_password() {
		return alu_password;
	}
	public void setAlu_password(String alu_password) {
		this.alu_password = alu_password;
	}
	public String getAlu_nombres() {
		return alu_nombres;
	}
	public void setAlu_nombres(String alu_nombres) {
		this.alu_nombres = alu_nombres;
	}
	public String getAlu_apellido_p() {
		return alu_apellido_p;
	}
	public void setAlu_apellido_p(String alu_apellido_p) {
		this.alu_apellido_p = alu_apellido_p;
	}
	public String getAlu_apellido_m() {
		return alu_apellido_m;
	}
	public void setAlu_apellido_m(String alu_apellido_m) {
		this.alu_apellido_m = alu_apellido_m;
	}
	public String getAlu_email() {
		return alu_email;
	}
	public void setAlu_email(String alu_email) {
		this.alu_email = alu_email;
	}
	public String getAlu_celular() {
		return alu_celular;
	}
	public void setAlu_celular(String alu_celular) {
		this.alu_celular = alu_celular;
	}
	public String getAlu_telefono() {
		return alu_telefono;
	}
	public void setAlu_telefono(String alu_telefono) {
		this.alu_telefono = alu_telefono;
	}
	public String getAlu_estado() {
		return alu_estado;
	}
	public void setAlu_estado(String alu_estado) {
		this.alu_estado = alu_estado;
	}
	
}
