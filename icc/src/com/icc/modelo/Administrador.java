package com.icc.modelo;

public class Administrador {
	String adm_rut;
	String adm_password;
	String adm_nombres;
	String adm_apellido_p;
	String adm_apellido_m;
	String adm_email;
	String adm_telefono;
	String adm_celular;
	String adm_cargo;
	String adm_estado;
	
	public Administrador(String adm_rut, String adm_telefono,
			String adm_celular, String adm_cargo) {
		super();
		this.adm_rut = adm_rut;
		this.adm_telefono = adm_telefono;
		this.adm_celular = adm_celular;
		this.adm_cargo = adm_cargo;
	}
	public Administrador(String adm_rut, String adm_password,
			String adm_nombres, String adm_apellido_p, String adm_apellido_m,
			String adm_email, String adm_telefono, String adm_celular,
			String adm_cargo, String adm_estado) {
		super();
		this.adm_rut = adm_rut;
		this.adm_password = adm_password;
		this.adm_nombres = adm_nombres;
		this.adm_apellido_p = adm_apellido_p;
		this.adm_apellido_m = adm_apellido_m;
		this.adm_email = adm_email;
		this.adm_telefono = adm_telefono;
		this.adm_celular = adm_celular;
		this.adm_cargo = adm_cargo;
		this.adm_estado = adm_estado;
	}
	public Administrador(String adm_rut,String adm_nombres, String adm_apellido_p,
			String adm_apellido_m,String adm_email, String adm_telefono, String adm_celular,
			String adm_cargo, String adm_estado) {
		super();
		this.adm_rut = adm_rut;
		this.adm_nombres = adm_nombres;
		this.adm_apellido_p = adm_apellido_p;
		this.adm_apellido_m = adm_apellido_m;
		this.adm_email = adm_email;
		this.adm_telefono = adm_telefono;
		this.adm_celular = adm_celular;
		this.adm_cargo = adm_cargo;
		this.adm_estado = adm_estado;
	}

	public String getAdm_rut() {
		return adm_rut;
	}

	public void setAdm_rut(String adm_rut) {
		this.adm_rut = adm_rut;
	}

	public String getAdm_password() {
		return adm_password;
	}

	public void setAdm_password(String adm_password) {
		this.adm_password = adm_password;
	}

	public String getAdm_nombres() {
		return adm_nombres;
	}

	public void setAdm_nombres(String adm_nombres) {
		this.adm_nombres = adm_nombres;
	}

	public String getAdm_apellido_p() {
		return adm_apellido_p;
	}

	public void setAdm_apellido_p(String adm_apellido_p) {
		this.adm_apellido_p = adm_apellido_p;
	}

	public String getAdm_apellido_m() {
		return adm_apellido_m;
	}

	public void setAdm_apellido_m(String adm_apellido_m) {
		this.adm_apellido_m = adm_apellido_m;
	}

	public String getAdm_email() {
		return adm_email;
	}

	public void setAdm_email(String adm_email) {
		this.adm_email = adm_email;
	}

	public String getAdm_telefono() {
		return adm_telefono;
	}

	public void setAdm_telefono(String adm_telefono) {
		this.adm_telefono = adm_telefono;
	}

	public String getAdm_celular() {
		return adm_celular;
	}

	public void setAdm_celular(String adm_celular) {
		this.adm_celular = adm_celular;
	}

	public String getAdm_cargo() {
		return adm_cargo;
	}

	public void setAdm_cargo(String adm_cargo) {
		this.adm_cargo = adm_cargo;
	}

	public String getAdm_estado() {
		return adm_estado;
	}

	public void setAdm_estado(String adm_estado) {
		this.adm_estado = adm_estado;
	}
	
	
	

}
