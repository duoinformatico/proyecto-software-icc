package com.icc.modelo;

public class Observacion {
	int obs_id;
	String obs_texto;
	String obs_fecha;
	String obs_hora;
	
	public Observacion(int obs_id, String obs_texto, String obs_fecha,
			String obs_hora) {
		super();
		this.obs_id = obs_id;
		this.obs_texto = obs_texto;
		this.obs_fecha = obs_fecha;
		this.obs_hora = obs_hora;
	}
	
	public int getObs_id() {
		return obs_id;
	}
	public void setObs_id(int obs_id) {
		this.obs_id = obs_id;
	}
	public String getObs_texto() {
		return obs_texto;
	}
	public void setObs_texto(String obs_texto) {
		this.obs_texto = obs_texto;
	}
	public String getObs_fecha() {
		return obs_fecha;
	}
	public void setObs_fecha(String obs_fecha) {
		this.obs_fecha = obs_fecha;
	}
	public String getObs_hora() {
		return obs_hora;
	}
	public void setObs_hora(String obs_hora) {
		this.obs_hora = obs_hora;
	}
	
}
