package com.icc.modelo;

public class DetalleEvaluacionAlumno {
	String alu_rut;
	int pra_id;
	String eva_fecha;
	int rol_id;
	int opc_id;
	int item_id;
		
	public DetalleEvaluacionAlumno(String alu_rut, int pra_id,
			String eva_fecha, int rol_id, int opc_id, int item_id) {
		super();
		this.alu_rut = alu_rut;
		this.pra_id = pra_id;
		this.eva_fecha = eva_fecha;
		this.rol_id = rol_id;
		this.opc_id = opc_id;
		this.item_id = item_id;
	}
	public String getAlu_rut() {
		return alu_rut;
	}
	public void setAlu_rut(String alu_rut) {
		this.alu_rut = alu_rut;
	}
	public int getPra_id() {
		return pra_id;
	}
	public void setPra_id(int pra_id) {
		this.pra_id = pra_id;
	}
	public String getEva_fecha() {
		return eva_fecha;
	}
	public void setEva_fecha(String eva_fecha) {
		this.eva_fecha = eva_fecha;
	}
	public int getRol_id() {
		return rol_id;
	}
	public void setRol_id(int rol_id) {
		this.rol_id = rol_id;
	}
	public int getOpc_id() {
		return opc_id;
	}
	public void setOpc_id(int opc_id) {
		this.opc_id = opc_id;
	}
	public int getItem_id() {
		return item_id;
	}
	public void setItem_id(int item_id) {
		this.item_id = item_id;
	}
}
