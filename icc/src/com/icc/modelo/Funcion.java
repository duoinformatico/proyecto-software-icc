package com.icc.modelo;

public class Funcion {
	int fun_id;
	String fun_nombre;
	
	public Funcion(int fun_id, String fun_nombre) {
		super();
		this.fun_id = fun_id;
		this.fun_nombre = fun_nombre;
	}
	public Funcion(String fun_nombre) {
		super();
		this.fun_nombre = fun_nombre;
	}
	public int getFun_id() {
		return fun_id;
	}

	public void setFun_id(int fun_id) {
		this.fun_id = fun_id;
	}

	public String getFun_nombre() {
		return fun_nombre;
	}

	public void setFun_nombre(String fun_nombre) {
		this.fun_nombre = fun_nombre;
	}
	
	
}
