package com.icc.modelo;

public class InscripcionAsignatura {
	String alu_rut;
	int asi_codigo;
	int plan_id;
	String ins_fecha;
	int ins_semestre;
	int ins_agnio;
	Double ins_nota;
	String ins_estado;
	
	public InscripcionAsignatura(String alu_rut, int asi_codigo, int plan_id,
			String ins_fecha) {
		super();
		this.alu_rut = alu_rut;
		this.asi_codigo = asi_codigo;
		this.plan_id = plan_id;
		this.ins_fecha = ins_fecha;
	}
	
	
	
	public InscripcionAsignatura(String alu_rut, int asi_codigo, int plan_id,
			String ins_fecha, int ins_semestre, Double ins_nota,
			String ins_estado) {
		super();
		this.alu_rut = alu_rut;
		this.asi_codigo = asi_codigo;
		this.plan_id = plan_id;
		this.ins_fecha = ins_fecha;
		this.ins_semestre = ins_semestre;
		this.ins_nota = ins_nota;
		this.ins_estado = ins_estado;
	}



	public InscripcionAsignatura(String alu_rut, int asi_codigo, int plan_id,
			String ins_fecha, int ins_semestre, int ins_agnio, Double ins_nota,
			String ins_estado) {
		super();
		this.alu_rut = alu_rut;
		this.asi_codigo = asi_codigo;
		this.plan_id = plan_id;
		this.ins_fecha = ins_fecha;
		this.ins_semestre = ins_semestre;
		this.ins_agnio = ins_agnio;
		this.ins_nota = ins_nota;
		this.ins_estado = ins_estado;
	}

	public String getAlu_rut() {
		return alu_rut;
	}
	public void setAlu_rut(String alu_rut) {
		this.alu_rut = alu_rut;
	}
	public int getAsi_codigo() {
		return asi_codigo;
	}
	public void setAsi_codigo(int asi_codigo) {
		this.asi_codigo = asi_codigo;
	}
	public int getPlan_id() {
		return plan_id;
	}
	public void setPlan_id(int plan_id) {
		this.plan_id = plan_id;
	}
	public String getIns_fecha() {
		return ins_fecha;
	}
	public void setIns_fecha(String ins_fecha) {
		this.ins_fecha = ins_fecha;
	}
	public int getIns_semestre() {
		return ins_semestre;
	}
	public void setIns_semestre(int ins_semestre) {
		this.ins_semestre = ins_semestre;
	}
	public int getIns_agnio() {
		return ins_agnio;
	}
	public void setIns_agnio(int ins_agnio) {
		this.ins_agnio = ins_agnio;
	}
	public Double getIns_nota() {
		return ins_nota;
	}
	public void setIns_nota(Double ins_nota) {
		this.ins_nota = ins_nota;
	}
	public String getIns_estado() {
		return ins_estado;
	}
	public void setIns_estado(String ins_estado) {
		this.ins_estado = ins_estado;
	}
	
}
