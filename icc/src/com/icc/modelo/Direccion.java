package com.icc.modelo;

public class Direccion {
	String alu_rut;
	int com_id;
	int dir_tp_id;
	String dir_calle;
	int dir_numero;
	String dir_sector;
	// constructor para insertar o modificar direcciond e alumno
	public Direccion(String alu_rut, int com_id, int dir_tp_id,
			String dir_calle, int dir_numero , String dir_sector) {
		super();
		this.alu_rut = alu_rut;
		this.com_id = com_id;
		this.dir_tp_id = dir_tp_id;
		this.dir_calle = dir_calle;
		this.dir_numero = dir_numero;
		this.dir_sector = dir_sector;
	}
	public String getAlu_rut() {
		return alu_rut;
	}
	public void setAlu_rut(String alu_rut) {
		this.alu_rut = alu_rut;
	}
	public int getCom_id() {
		return com_id;
	}
	public void setCom_id(int com_id) {
		this.com_id = com_id;
	}
	public int getDir_tp_id() {
		return dir_tp_id;
	}
	public void setDir_tp_id(int dir_tp_id) {
		this.dir_tp_id = dir_tp_id;
	}
	public String getDir_calle() {
		return dir_calle;
	}
	public void setDir_calle(String dir_calle) {
		this.dir_calle = dir_calle;
	}
	public int getDir_numero() {
		return dir_numero;
	}
	public void setDir_numero(int dir_numero) {
		this.dir_numero = dir_numero;
	}
	public String getDir_sector() {
		return dir_sector;
	}
	public void setDir_sector(String dir_sector) {
		this.dir_sector = dir_sector;
	}
	
}
