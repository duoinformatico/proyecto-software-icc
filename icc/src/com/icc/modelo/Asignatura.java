package com.icc.modelo;

import java.util.ArrayList;

public class Asignatura {
	int asi_codigo;
	int plan_id;
	int asi_tp_id;
	String asi_nombre;
	int asi_horas;
	int asi_semestre;
	int asi_sct;
	String bean1;
	String bean2;
	ArrayList<Instrumento> instrumentos;

	// constructor para generar informe de empresa usando gestiones
	public Asignatura(int asi_codigo, String bean1, String bean2) {
		super();
		this.asi_codigo = asi_codigo;
		this.bean1 = bean1;
		this.bean2 = bean2;
	}

	public Asignatura(int asi_codigo, int plan_id, int asi_tp_id,
			String asi_nombre) {
		super();
		this.asi_codigo = asi_codigo;
		this.plan_id = plan_id;
		this.asi_tp_id = asi_tp_id;
		this.asi_nombre = asi_nombre;
	}
	
	public Asignatura(int asi_codigo, String asi_nombre, String bean1,
			String bean2) {
		super();
		this.asi_codigo = asi_codigo;
		this.asi_nombre = asi_nombre;
		this.bean1 = bean1;
		this.bean2 = bean2;
	}

	public Asignatura(int asi_codigo, String asi_nombre) {
		super();
		this.asi_codigo = asi_codigo;
		this.asi_nombre = asi_nombre;
	}
	
	public Asignatura(int asi_codigo) {
		super();
		this.asi_codigo = asi_codigo;
	}
	
	
	
	public Asignatura(int asi_codigo, int plan_id, int asi_tp_id,
			String asi_nombre, int asi_horas, int asi_semestre, int asi_sct) {
		super();
		this.asi_codigo = asi_codigo;
		this.plan_id = plan_id;
		this.asi_tp_id = asi_tp_id;
		this.asi_nombre = asi_nombre;
		this.asi_horas = asi_horas;
		this.asi_semestre = asi_semestre;
		this.asi_sct = asi_sct;
	}

	public Asignatura(int asi_codigo, int plan_id, int asi_tp_id,
			String asi_nombre, ArrayList<Instrumento> instrumentos) {
		super();
		this.asi_codigo = asi_codigo;
		this.plan_id = plan_id;
		this.asi_tp_id = asi_tp_id;
		this.asi_nombre = asi_nombre;
		this.instrumentos = instrumentos;
	}

	public ArrayList<Instrumento> getInstrumentos() {
		return instrumentos;
	}

	public void setInstrumentos(ArrayList<Instrumento> instrumentos) {
		this.instrumentos = instrumentos;
	}
	public int getAsi_codigo() {
		return asi_codigo;
	}
	public void setAsi_codigo(int asi_codigo) {
		this.asi_codigo = asi_codigo;
	}
	public int getPlan_id() {
		return plan_id;
	}
	public void setPlan_id(int plan_id) {
		this.plan_id = plan_id;
	}
	public int getAsi_tp_id() {
		return asi_tp_id;
	}
	public void setAsi_tp_id(int asi_tp_id) {
		this.asi_tp_id = asi_tp_id;
	}
	public String getAsi_nombre() {
		return asi_nombre;
	}
	public void setAsi_nombre(String asi_nombre) {
		this.asi_nombre = asi_nombre;
	}
	public String getBean1() {
		return bean1;
	}
	public void setBean1(String bean1) {
		this.bean1 = bean1;
	}
	
	public int getAsi_horas() {
		return asi_horas;
	}

	public void setAsi_horas(int asi_horas) {
		this.asi_horas = asi_horas;
	}

	public int getAsi_semestre() {
		return asi_semestre;
	}

	public void setAsi_semestre(int asi_semestre) {
		this.asi_semestre = asi_semestre;
	}

	public int getAsi_sct() {
		return asi_sct;
	}

	public void setAsi_sct(int asi_sct) {
		this.asi_sct = asi_sct;
	}

	public String getBean2() {
		return bean2;
	}

	public void setBean2(String bean2) {
		this.bean2 = bean2;
	}
}
