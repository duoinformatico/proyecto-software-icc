package com.icc.modelo;

public class Provincia {
	int pro_id;
	int reg_id;
	String pro_nombre;
	public Provincia(int pro_id, int reg_id, String pro_nombre) {
		super();
		this.pro_id = pro_id;
		this.reg_id = reg_id;
		this.pro_nombre = pro_nombre;
	}
	public Provincia() {
		
	}
	public int getPro_id() {
		return pro_id;
	}
	public void setPro_id(int pro_id) {
		this.pro_id = pro_id;
	}
	public int getReg_id() {
		return reg_id;
	}
	public void setReg_id(int reg_id) {
		this.reg_id = reg_id;
	}
	public String getPro_nombre() {
		return pro_nombre;
	}
	public void setPro_nombre(String pro_nombre) {
		this.pro_nombre = pro_nombre;
	}
	
}
