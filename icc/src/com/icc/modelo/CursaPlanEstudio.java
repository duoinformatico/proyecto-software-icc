package com.icc.modelo;

public class CursaPlanEstudio {
	int plan_id;
	String alu_rut;
	int cur_agnio_ingreso;
	int cur_semestre;
	String cur_estado;
	boolean cur_vigente;
	
	public CursaPlanEstudio(int plan_id, String alu_rut, int cur_agnio_ingreso,
			int cur_semestre) {
		super();
		this.plan_id = plan_id;
		this.alu_rut = alu_rut;
		this.cur_agnio_ingreso = cur_agnio_ingreso;
		this.cur_semestre = cur_semestre;
	}
	public int getPlan_id() {
		return plan_id;
	}
	public void setPlan_id(int plan_id) {
		this.plan_id = plan_id;
	}
	public String getAlu_rut() {
		return alu_rut;
	}
	public void setAlu_rut(String alu_rut) {
		this.alu_rut = alu_rut;
	}
	public int getCur_agnio_ingreso() {
		return cur_agnio_ingreso;
	}
	public void setCur_agnio_ingreso(int cur_agnio_ingreso) {
		this.cur_agnio_ingreso = cur_agnio_ingreso;
	}
	public int getCur_semestre() {
		return cur_semestre;
	}
	public void setCur_semestre(int cur_semestre) {
		this.cur_semestre = cur_semestre;
	}
	public String getCur_estado() {
		return cur_estado;
	}
	public void setCur_estado(String cur_estado) {
		this.cur_estado = cur_estado;
	}
	public boolean isCur_vigente() {
		return cur_vigente;
	}
	public void setCur_vigente(boolean cur_vigente) {
		this.cur_vigente = cur_vigente;
	}
	
}
