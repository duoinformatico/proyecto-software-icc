package com.icc.modelo;

import java.util.ArrayList;

public class Item {
	int item_id;
	int ins_id;
	int esc_id;
	int cat_id;
	String item_nombre;
	ArrayList<Opcion> opciones;//join - relacion 'un item tiene muchas opciones'
	
	public Item(int item_id, int ins_id, int esc_id, int cat_id,
			String item_nombre, ArrayList<Opcion> opciones) {
		super();
		this.item_id = item_id;
		this.ins_id = ins_id;
		this.esc_id = esc_id;
		this.cat_id = cat_id;
		this.item_nombre = item_nombre;
		this.opciones = opciones;
	}

	public Item(int item_id, int ins_id, int esc_id, int cat_id,
			String item_nombre) {
		super();
		this.item_id = item_id;
		this.ins_id = ins_id;
		this.esc_id = esc_id;
		this.cat_id = cat_id;
		this.item_nombre = item_nombre;
	}

	public int getItem_id() {
		return item_id;
	}

	public void setItem_id(int item_id) {
		this.item_id = item_id;
	}

	public int getIns_id() {
		return ins_id;
	}

	public void setIns_id(int ins_id) {
		this.ins_id = ins_id;
	}

	public int getEsc_id() {
		return esc_id;
	}

	public void setEsc_id(int esc_id) {
		this.esc_id = esc_id;
	}

	public int getCat_id() {
		return cat_id;
	}

	public void setCat_id(int cat_id) {
		this.cat_id = cat_id;
	}

	public String getItem_nombre() {
		return item_nombre;
	}

	public void setItem_nombre(String item_nombre) {
		this.item_nombre = item_nombre;
	}
	public ArrayList<Opcion> getOpciones() {
		return opciones;
	}

	public void setOpciones(ArrayList<Opcion> opciones) {
		this.opciones = opciones;
	}
}
