package com.icc.modelo;

public class Rubro {
	int rub_id;
	String rub_nombre;
	String rub_padre;
	
	public Rubro(int rub_id, String rub_nombre, String rub_padre) {
		super();
		this.rub_id = rub_id;
		this.rub_nombre = rub_nombre;
		this.rub_padre = rub_padre;
	}
	public int getRub_id() {
		return rub_id;
	}
	public void setRub_id(int rub_id) {
		this.rub_id = rub_id;
	}
	public String getRub_nombre() {
		return rub_nombre;
	}
	public void setRub_nombre(String rub_nombre) {
		this.rub_nombre = rub_nombre;
	}
	public String getRub_padre() {
		return rub_padre;
	}
	public void setRub_padre(String rub_padre) {
		this.rub_padre = rub_padre;
	}
	
}
