package com.icc.modelo;

import java.util.Date;

public class Bitacora {
	int bit_id;
	Date bit_fecha_ini;
	int pra_id;
	String bit_hora_edicion;
	String bit_fecha_edicion;
	String bit_descripcion;
	boolean bit_firma;
	Date bit_fecha_fin;
	int bit_n_asignatura;
	int bit_n_imagen;
	int bean_uno;
	String bean_dos;
	
	public Bitacora(int bit_id, Date bit_fecha_ini, int pra_id,
			String bit_hora_edicion, String bit_fecha_edicion,
			String bit_descripcion, boolean bit_firma, Date bit_fecha_fin,
			int bit_n_asignatura,int bit_n_imagen) {
		super();
		this.bit_id = bit_id;
		this.bit_fecha_ini = bit_fecha_ini;
		this.pra_id = pra_id;
		this.bit_hora_edicion = bit_hora_edicion;
		this.bit_fecha_edicion = bit_fecha_edicion;
		this.bit_descripcion = bit_descripcion;
		this.bit_firma = bit_firma;
		this.bit_fecha_fin = bit_fecha_fin;
		this.bit_n_asignatura = bit_n_asignatura;
		this.bit_n_imagen = bit_n_imagen;
	}
	//constructor para cargar datos a informe de biatcoras
	public Bitacora(int bean_uno,Date bit_fecha_ini, Date bit_fecha_fin, String bean_dos,String bit_descripcion) 
	{
		super();
		this.bean_uno = bean_uno;
		this.bit_fecha_ini = bit_fecha_ini;
		this.bit_fecha_fin = bit_fecha_fin;
		this.bean_dos = bean_dos;
		this.bit_descripcion = bit_descripcion;
		
	}
	public Bitacora(int bit_id, String bit_descripcion) {
		super();
		this.bit_id = bit_id;
		this.bit_descripcion = bit_descripcion;
	}


	public int getBit_id() {
		return bit_id;
	}

	public Bitacora(int bit_id, Date bit_fecha_ini, Date bit_fecha_fin, int pra_id,
			String bit_fecha_edicion, int bean_uno) {
		super();
		this.bit_id = bit_id;
		this.bit_fecha_ini = bit_fecha_ini;
		this.pra_id = pra_id;
		this.bit_fecha_edicion = bit_fecha_edicion;
		this.bit_fecha_fin = bit_fecha_fin;
		this.bean_uno = bean_uno;
	}
	
	
	public int getBit_n_asignatura() {
		return bit_n_asignatura;
	}
	public void setBit_n_asignatura(int bit_n_asignatura) {
		this.bit_n_asignatura = bit_n_asignatura;
	}
	public int getBit_n_imagen() {
		return bit_n_imagen;
	}
	public void setBit_n_imagen(int bit_n_imagen) {
		this.bit_n_imagen = bit_n_imagen;
	}
	public int getBean_uno() {
		return bean_uno;
	}
	public void setBean_uno(int bean_uno) {
		this.bean_uno = bean_uno;
	}
	public void setBit_id(int bit_id) {
		this.bit_id = bit_id;
	}
	public Date getBit_fecha_ini() {
		return bit_fecha_ini;
	}
	public void setBit_fecha_ini(Date bit_fecha_ini) {
		this.bit_fecha_ini = bit_fecha_ini;
	}
	public int getPra_id() {
		return pra_id;
	}
	public void setPra_id(int pra_id) {
		this.pra_id = pra_id;
	}
	public String getBit_hora_edicion() {
		return bit_hora_edicion;
	}
	public void setBit_hora_edicion(String bit_hora_edicion) {
		this.bit_hora_edicion = bit_hora_edicion;
	}
	public String getBit_fecha_edicion() {
		return bit_fecha_edicion;
	}
	public void setBit_fecha_edicion(String bit_fecha_edicion) {
		this.bit_fecha_edicion = bit_fecha_edicion;
	}
	public String getBit_descripcion() {
		return bit_descripcion;
	}
	public void setBit_descripcion(String bit_descripcion) {
		this.bit_descripcion = bit_descripcion;
	}
	public boolean isBit_firma() {
		return bit_firma;
	}
	public void setBit_firma(boolean bit_firma) {
		this.bit_firma = bit_firma;
	}
	public Date getBit_fecha_fin() {
		return bit_fecha_fin;
	}
	public void setBit_fecha_fin(Date bit_fecha_fin) {
		this.bit_fecha_fin = bit_fecha_fin;
	}
	public String getBean_dos() {
		return bean_dos;
	}
	public void setBean_dos(String bean_dos) {
		this.bean_dos = bean_dos;
	}
	
}
