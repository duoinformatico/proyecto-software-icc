package com.icc.modelo;

public class EvaluacionAcademico {
	String aca_rut;
	int pra_id;
	String eva_fecha;
	int rol_id;
	String eva_nombre;
	Double eva_nota;
	String eva_observacion;
	String eva_hora;
	String eva_estado;	
	
	public EvaluacionAcademico(String aca_rut, int pra_id, String eva_fecha,
			int rol_id, String eva_nombre, Double eva_nota,
			String eva_observacion, String eva_hora, String eva_estado) {
		super();
		this.aca_rut = aca_rut;
		this.pra_id = pra_id;
		this.eva_fecha = eva_fecha;
		this.rol_id = rol_id;
		this.eva_nombre = eva_nombre;
		this.eva_nota = eva_nota;
		this.eva_observacion = eva_observacion;
		this.eva_hora = eva_hora;
		this.eva_estado = eva_estado;
	}
	
	public String getAca_rut() {
		return aca_rut;
	}
	public void setAca_rut(String aca_rut) {
		this.aca_rut = aca_rut;
	}
	public int getPra_id() {
		return pra_id;
	}
	public void setPra_id(int pra_id) {
		this.pra_id = pra_id;
	}
	public String getFecha() {
		return eva_fecha;
	}
	public void setFecha(String eva_fecha) {
		this.eva_fecha = eva_fecha;
	}
	public int getRol_id() {
		return rol_id;
	}
	public void setRol_id(int rol_id) {
		this.rol_id = rol_id;
	}
	public String getEva_nombre() {
		return eva_nombre;
	}
	public void setEva_nombre(String eva_nombre) {
		this.eva_nombre = eva_nombre;
	}
	public Double getEva_nota() {
		return eva_nota;
	}
	public void setEva_nota(Double eva_nota) {
		this.eva_nota = eva_nota;
	}
	public String getEva_observacion() {
		return eva_observacion;
	}
	public void setEva_observacion(String eva_observacion) {
		this.eva_observacion = eva_observacion;
	}
	public String getHora() {
		return eva_hora;
	}
	public void setHora(String eva_hora) {
		this.eva_hora = eva_hora;
	}
	public String getEva_estado() {
		return eva_estado;
	}
	public void setEva_estado(String eva_estado) {
		this.eva_estado = eva_estado;
	}
	
	
}
