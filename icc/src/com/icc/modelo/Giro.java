package com.icc.modelo;

public class Giro {
	int gir_id;
	int rub_id;
	String gir_codigo;
	String gir_nombre;
	
	public Giro(int gir_id, int rub_id, String gir_codigo, String gir_nombre) {
		super();
		this.gir_id = gir_id;
		this.rub_id = rub_id;
		this.gir_codigo = gir_codigo;
		this.gir_nombre = gir_nombre;
	}
	public int getGir_id() {
		return gir_id;
	}
	public void setGir_id(int gir_id) {
		this.gir_id = gir_id;
	}
	public int getRub_id() {
		return rub_id;
	}
	public void setRub_id(int rub_id) {
		this.rub_id = rub_id;
	}
	public String getGir_codigo() {
		return gir_codigo;
	}
	public void setGir_codigo(String gir_codigo) {
		this.gir_codigo = gir_codigo;
	}
	public String getGir_nombre() {
		return gir_nombre;
	}
	public void setGir_nombre(String gir_nombre) {
		this.gir_nombre = gir_nombre;
	}
	
}
