package com.icc.modelo;

public class Obra_tipo {
	int obr_id;
	String obr_nombre;
	public Obra_tipo(int obr_id, String obr_nombre) {
		super();
		this.obr_id = obr_id;
		this.obr_nombre = obr_nombre;
	}
	public Obra_tipo(String obr_nombre) {
		super();
		this.obr_nombre = obr_nombre;
	}
	public int getObr_id() {
		return obr_id;
	}
	public void setObr_id(int obr_id) {
		this.obr_id = obr_id;
	}
	public String getObr_nombre() {
		return obr_nombre;
	}
	public void setObr_nombre(String obr_nombre) {
		this.obr_nombre = obr_nombre;
	}
	
}
