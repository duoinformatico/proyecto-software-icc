package com.icc.modelo;

public class Practica {
	int pra_id;
	String emp_rut;
	String sup_rut;
	int com_id;
	int obr_id;
	String alu_rut;
	String aca_rut;
	int asi_codigo;
	int plan_id;
	String ins_fecha;
	String pra_obra_nombre;
	String pra_direccion;
	String pra_tareas;
	String pra_fecha_ini;
	String pra_fecha_fin;
	String pra_fecha_ins;
	int pra_horas;
	String pra_informe;
	Double pra_nota;
	String pra_estado;
	int bean_uno;
	String bean_dos;
	public Practica(int pra_id, int bean_uno) {
		super();
		this.pra_id = pra_id;
		this.bean_uno = bean_uno;
	}
	//constructor para generar informe segun gestion
	public Practica(String alu_rut, String bean_dos) {
		super();
		this.alu_rut = alu_rut;
		this.bean_dos = bean_dos;
	}
	
	public Practica(int pra_id, String emp_rut, String sup_rut, int com_id,
			int obr_id, String alu_rut, String aca_rut, int asi_codigo,
			int plan_id, String ins_fecha, String pra_obra_nombre,
			String pra_direccion, String pra_tareas, String pra_fecha_ini,
			String pra_fecha_fin, String pra_fecha_ins, int pra_horas,
			String pra_informe, String pra_estado) {
		super();
		this.pra_id = pra_id;
		this.emp_rut = emp_rut;
		this.sup_rut = sup_rut;
		this.com_id = com_id;
		this.obr_id = obr_id;
		this.alu_rut = alu_rut;
		this.aca_rut = aca_rut;
		this.asi_codigo = asi_codigo;
		this.plan_id = plan_id;
		this.ins_fecha = ins_fecha;
		this.pra_obra_nombre = pra_obra_nombre;
		this.pra_direccion = pra_direccion;
		this.pra_tareas = pra_tareas;
		this.pra_fecha_ini = pra_fecha_ini;
		this.pra_fecha_fin = pra_fecha_fin;
		this.pra_fecha_ins = pra_fecha_ins;
		this.pra_horas = pra_horas;
		this.pra_informe = pra_informe;
		this.pra_estado = pra_estado;
	}
	
	// Constructor para modificar historial practicas y validar pendientes 
	public Practica(int pra_id, String emp_rut, String aca_rut, String sup_rut, int com_id,
			int obr_id, String alu_rut, int asi_codigo, int plan_id,
			String ins_fecha, String pra_obra_nombre, String pra_direccion,
			String pra_tareas, String pra_fecha_ini, String pra_fecha_fin,
			int pra_horas, String pra_estado) {
		super();
		this.pra_id = pra_id;
		this.emp_rut = emp_rut;
		this.aca_rut = aca_rut;
		this.sup_rut = sup_rut;
		this.com_id = com_id;
		this.obr_id = obr_id;
		this.alu_rut = alu_rut;
		this.asi_codigo = asi_codigo;
		this.plan_id = plan_id;
		this.ins_fecha = ins_fecha;
		this.pra_obra_nombre = pra_obra_nombre;
		this.pra_direccion = pra_direccion;
		this.pra_tareas = pra_tareas;
		this.pra_fecha_ini = pra_fecha_ini;
		this.pra_fecha_fin = pra_fecha_fin;
		this.pra_horas = pra_horas;
		this.pra_estado = pra_estado;
	}
	// Constructor para formulario validar pendientes 
		public Practica(int pra_id, String emp_rut, String sup_rut, int com_id,
				int obr_id, String alu_rut, int asi_codigo, int plan_id,
				String ins_fecha, String pra_obra_nombre, String pra_direccion,
				String pra_tareas, String pra_fecha_ini, String pra_fecha_fin,
				int pra_horas, String pra_estado, String aca_rut) {
			super();
			this.pra_id = pra_id;
			this.emp_rut = emp_rut;
			this.sup_rut = sup_rut;
			this.com_id = com_id;
			this.obr_id = obr_id;
			this.alu_rut = alu_rut;
			this.asi_codigo = asi_codigo;
			this.plan_id = plan_id;
			this.ins_fecha = ins_fecha;
			this.pra_obra_nombre = pra_obra_nombre;
			this.pra_direccion = pra_direccion;
			this.pra_tareas = pra_tareas;
			this.pra_fecha_ini = pra_fecha_ini;
			this.pra_fecha_fin = pra_fecha_fin;
			this.pra_horas = pra_horas;
			this.pra_estado = pra_estado;
			this.aca_rut = aca_rut;
		}
	//constructor para formulario preInscripcion
	public Practica(String emp_rut, String sup_rut, int com_id, int obr_id,
			String alu_rut, int asi_codigo, int plan_id, String ins_fecha,
			String pra_obra_nombre, String pra_direccion, String pra_tareas,
			String pra_fecha_ini, String pra_fecha_fin, int pra_horas,String aca_rut) {
		super();
		this.emp_rut = emp_rut;
		this.sup_rut = sup_rut;
		this.com_id = com_id;
		this.obr_id = obr_id;
		this.alu_rut = alu_rut;
		this.asi_codigo = asi_codigo;
		this.plan_id = plan_id;
		this.ins_fecha = ins_fecha;
		this.pra_obra_nombre = pra_obra_nombre;
		this.pra_direccion = pra_direccion;
		this.pra_tareas = pra_tareas;
		this.pra_fecha_ini = pra_fecha_ini;
		this.pra_fecha_fin = pra_fecha_fin;
		this.pra_horas = pra_horas;
		this.aca_rut = aca_rut;
	}
	//constructor para mostrar una practica especifica
	public Practica(int pra_id, String emp_rut, String sup_rut, int com_id,
			int obr_id, String alu_rut, int asi_codigo, int plan_id,
			String ins_fecha, String pra_obra_nombre, String pra_fecha_ins,String pra_direccion,
			String pra_tareas, String pra_fecha_ini, String pra_fecha_fin,
			int pra_horas, String pra_informe, String pra_estado,String aca_rut) {
		super();
		this.pra_id = pra_id;
		this.emp_rut = emp_rut;
		this.sup_rut = sup_rut;
		this.com_id = com_id;
		this.obr_id = obr_id;
		this.alu_rut = alu_rut;
		this.asi_codigo = asi_codigo;
		this.plan_id = plan_id;
		this.ins_fecha = ins_fecha;
		this.pra_obra_nombre = pra_obra_nombre;
		this.pra_direccion = pra_direccion;
		this.pra_tareas = pra_tareas;
		this.pra_fecha_ini = pra_fecha_ini;
		this.pra_fecha_fin = pra_fecha_fin;
		this.pra_horas = pra_horas;
		this.aca_rut = aca_rut;
		this.pra_informe = pra_informe;
		this.pra_estado = pra_estado;
	}
	//Constructor para ocupar en las descargas de informes
	public Practica(int pra_id,String emp_rut, String alu_rut, String ins_fecha,
			String pra_obra_nombre, String pra_estado, String pra_informe) {
		super();
		this.pra_id = pra_id;
		this.emp_rut = emp_rut;
		this.alu_rut = alu_rut;
		this.ins_fecha = ins_fecha;
		this.pra_obra_nombre = pra_obra_nombre;
		this.pra_estado = pra_estado;
		this.pra_informe = pra_informe;
	}
	// Constructor para mostrar las practicas y mostrar las pendientes
	public Practica(int pra_id,String emp_rut, String alu_rut, String ins_fecha,
			String pra_obra_nombre, String pra_estado) {
		super();
		this.pra_id = pra_id;
		this.emp_rut = emp_rut;
		this.alu_rut = alu_rut;
		this.ins_fecha = ins_fecha;
		this.pra_obra_nombre = pra_obra_nombre;
		this.pra_estado = pra_estado;
	}
    //Constructor para historial de alumno
	public Practica(String emp_rut, String sup_rut, int obr_id, int asi_codigo,
			String pra_obra_nombre, String pra_fecha_ini, String pra_fecha_fin,
			String pra_estado) {
		super();
		this.emp_rut = emp_rut;
		this.sup_rut = sup_rut;
		this.obr_id = obr_id;
		this.asi_codigo = asi_codigo;
		this.pra_obra_nombre = pra_obra_nombre;
		this.pra_fecha_ini = pra_fecha_ini;
		this.pra_fecha_fin = pra_fecha_fin;
		this.pra_estado = pra_estado;
	}
	//Constructor para bitacoras
	public Practica(int pra_id,String emp_rut, String sup_rut, int obr_id, int asi_codigo,
			String pra_obra_nombre, String pra_fecha_ini, String pra_fecha_fin,String pra_informe,
			String pra_estado) {
		super();
		this.pra_id = pra_id;
		this.emp_rut = emp_rut;
		this.sup_rut = sup_rut;
		this.obr_id = obr_id;
		this.asi_codigo = asi_codigo;
		this.pra_obra_nombre = pra_obra_nombre;
		this.pra_fecha_ini = pra_fecha_ini;
		this.pra_fecha_fin = pra_fecha_fin;
		this.pra_informe = pra_informe;
		this.pra_estado = pra_estado;
	}

	
	public Practica(int pra_id, String emp_rut, int com_id, int obr_id,
			int asi_codigo, int plan_id, String pra_obra_nombre,
			String pra_informe, String pra_estado) {
		super();
		this.pra_id = pra_id;
		this.emp_rut = emp_rut;
		this.com_id = com_id;
		this.obr_id = obr_id;
		this.asi_codigo = asi_codigo;
		this.plan_id = plan_id;
		this.pra_obra_nombre = pra_obra_nombre;
		this.pra_informe = pra_informe;
		this.pra_estado = pra_estado;
	}
	
	public Practica(int pra_id, String emp_rut, int com_id, int obr_id,
			int asi_codigo, int plan_id, String pra_obra_nombre,
			String pra_informe, Double pra_nota,String pra_estado) {
		super();
		this.pra_id = pra_id;
		this.emp_rut = emp_rut;
		this.com_id = com_id;
		this.obr_id = obr_id;
		this.asi_codigo = asi_codigo;
		this.plan_id = plan_id;
		this.pra_obra_nombre = pra_obra_nombre;
		this.pra_informe = pra_informe;
		this.pra_nota = pra_nota;
		this.pra_estado = pra_estado;
	}
	
	public Practica(int pra_id, String alu_rut,String emp_rut, int com_id, int obr_id,
			int asi_codigo, int plan_id, String pra_obra_nombre,
			String pra_informe, Double pra_nota,String pra_estado) {
		super();
		this.pra_id = pra_id;
		this.alu_rut = alu_rut;
		this.emp_rut = emp_rut;
		this.com_id = com_id;
		this.obr_id = obr_id;
		this.asi_codigo = asi_codigo;
		this.plan_id = plan_id;
		this.pra_obra_nombre = pra_obra_nombre;
		this.pra_informe = pra_informe;
		this.pra_nota = pra_nota;
		this.pra_estado = pra_estado;
	}
	public String getAca_rut() {
		return aca_rut;
	}
	public void setAca_rut(String aca_rut) {
		this.aca_rut = aca_rut;
	}
	public String getPra_fecha_ins() {
		return pra_fecha_ins;
	}
	public void setPra_fecha_ins(String pra_fecha_ins) {
		this.pra_fecha_ins = pra_fecha_ins;
	}
	public int getPra_id() {
		return pra_id;
	}
	public void setPra_id(int pra_id) {
		this.pra_id = pra_id;
	}
	public String getEmp_rut() {
		return emp_rut;
	}
	public void setEmp_rut(String emp_rut) {
		this.emp_rut = emp_rut;
	}
	public String getSup_rut() {
		return sup_rut;
	}
	public void setSup_rut(String sup_rut) {
		this.sup_rut = sup_rut;
	}
	public int getCom_id() {
		return com_id;
	}
	public void setCom_id(int com_id) {
		this.com_id = com_id;
	}
	public int getObr_id() {
		return obr_id;
	}
	public void setObr_id(int obr_id) {
		this.obr_id = obr_id;
	}
	public String getAlu_rut() {
		return alu_rut;
	}
	public void setAlu_rut(String alu_rut) {
		this.alu_rut = alu_rut;
	}
	public int getAsi_codigo() {
		return asi_codigo;
	}
	public void setAsi_codigo(int asi_codigo) {
		this.asi_codigo = asi_codigo;
	}
	public int getPlan_id() {
		return plan_id;
	}
	public void setPlan_id(int plan_id) {
		this.plan_id = plan_id;
	}
	public String getIns_fecha() {
		return ins_fecha;
	}
	public void setIns_fecha(String ins_fecha) {
		this.ins_fecha = ins_fecha;
	}
	public String getPra_obra_nombre() {
		return pra_obra_nombre;
	}
	public void setPra_obra_nombre(String pra_obra_nombre) {
		this.pra_obra_nombre = pra_obra_nombre;
	}
	public String getPra_direccion() {
		return pra_direccion;
	}
	public void setPra_direccion(String pra_direccion) {
		this.pra_direccion = pra_direccion;
	}
	public String getPra_tareas() {
		return pra_tareas;
	}
	public void setPra_tareas(String pra_tareas) {
		this.pra_tareas = pra_tareas;
	}
	public String getPra_fecha_ini() {
		return pra_fecha_ini;
	}
	public void setPra_fecha_ini(String pra_fecha_ini) {
		this.pra_fecha_ini = pra_fecha_ini;
	}
	public String getPra_fecha_fin() {
		return pra_fecha_fin;
	}
	public void setPra_fecha_fin(String pra_fecha_fin) {
		this.pra_fecha_fin = pra_fecha_fin;
	}
	public int getPra_horas() {
		return pra_horas;
	}
	public void setPra_horas(int pra_horas) {
		this.pra_horas = pra_horas;
	}
	public String getPra_informe() {
		return pra_informe;
	}
	public void setPra_informe(String pra_informe) {
		this.pra_informe = pra_informe;
	}	
	public Double getPra_nota() {
		return pra_nota;
	}
	public void setPra_nota(Double pra_nota) {
		this.pra_nota = pra_nota;
	}
	public String getPra_estado() {
		return pra_estado;
	}
	public void setPra_estado(String pra_estado) {
		this.pra_estado = pra_estado;
	}
	public int getBean_uno() {
		return bean_uno;
	}
	public void setBean_uno(int bean_uno) {
		this.bean_uno = bean_uno;
	}
	public String getBean_dos() {
		return bean_dos;
	}
	public void setBean_dos(String bean_dos) {
		this.bean_dos = bean_dos;
	}
	
}
