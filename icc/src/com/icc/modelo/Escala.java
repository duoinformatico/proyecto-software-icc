package com.icc.modelo;

import java.util.ArrayList;

public class Escala {
	int esc_id;
	String esc_nombre;
	String esc_descripcion;
	int esc_num_opciones;
	double esc_min_valor;
	double esc_max_valor;
	double esc_intervalo;
	double esc_exigencia;
	ArrayList<Valores> valores;
	
	public Escala(int esc_id, String esc_nombre, String esc_descripcion,
			int esc_num_opciones, double esc_min_valor, double esc_max_valor,
			double esc_intervalo) {
		super();
		this.esc_id = esc_id;
		this.esc_nombre = esc_nombre;
		this.esc_descripcion = esc_descripcion;
		this.esc_num_opciones = esc_num_opciones;
		this.esc_min_valor = esc_min_valor;
		this.esc_max_valor = esc_max_valor;
		this.esc_intervalo = esc_intervalo;
	}	
	
	public Escala(int esc_id, String esc_nombre, String esc_descripcion,
			int esc_num_opciones, double esc_min_valor, double esc_max_valor,
			double esc_intervalo, ArrayList<Valores> valores) {
		super();
		this.esc_id = esc_id;
		this.esc_nombre = esc_nombre;
		this.esc_descripcion = esc_descripcion;
		this.esc_num_opciones = esc_num_opciones;
		this.esc_min_valor = esc_min_valor;
		this.esc_max_valor = esc_max_valor;
		this.esc_intervalo = esc_intervalo;
		this.valores = valores;
	}
	
	public Escala(int esc_id, String esc_nombre, String esc_descripcion,
			int esc_num_opciones, double esc_min_valor, double esc_max_valor,
			double esc_intervalo, double esc_exigencia,
			ArrayList<Valores> valores) {
		super();
		this.esc_id = esc_id;
		this.esc_nombre = esc_nombre;
		this.esc_descripcion = esc_descripcion;
		this.esc_num_opciones = esc_num_opciones;
		this.esc_min_valor = esc_min_valor;
		this.esc_max_valor = esc_max_valor;
		this.esc_intervalo = esc_intervalo;
		this.esc_exigencia = esc_exigencia;
		this.valores = valores;
	}

	public int getEsc_id() {
		return esc_id;
	}
	public void setEsc_id(int esc_id) {
		this.esc_id = esc_id;
	}
	public String getEsc_nombre() {
		return esc_nombre;
	}
	public void setEsc_nombre(String esc_nombre) {
		this.esc_nombre = esc_nombre;
	}
	public String getEsc_descripcion() {
		return esc_descripcion;
	}
	public void setEsc_descripcion(String esc_descripcion) {
		this.esc_descripcion = esc_descripcion;
	}
	public int getEsc_num_opciones() {
		return esc_num_opciones;
	}
	public void setEsc_num_opciones(int esc_num_opciones) {
		this.esc_num_opciones = esc_num_opciones;
	}
	public double getEsc_min_valor() {
		return esc_min_valor;
	}
	public void setEsc_min_valor(double esc_min_valor) {
		this.esc_min_valor = esc_min_valor;
	}
	public double getEsc_max_valor() {
		return esc_max_valor;
	}
	public void setEsc_max_valor(double esc_max_valor) {
		this.esc_max_valor = esc_max_valor;
	}
	public double getEsc_intervalo() {
		return esc_intervalo;
	}
	public void setEsc_intervalo(double esc_intervalo) {
		this.esc_intervalo = esc_intervalo;
	}
	public ArrayList<Valores> getValores() {
		return valores;
	}
	public void setValores(ArrayList<Valores> valores) {
		this.valores = valores;
	}
	public double getEsc_exigencia() {
		return esc_exigencia;
	}
	public void setEsc_exigencia(double esc_exigencia) {
		this.esc_exigencia = esc_exigencia;
	}
	
	

}
