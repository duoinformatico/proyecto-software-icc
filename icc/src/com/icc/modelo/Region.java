package com.icc.modelo;

public class Region {
	 int reg_id;
	 int pais_id;
	 String reg_nombre;
	 String reg_simbolo;
	 
	public Region(int reg_id, int pais_id, String reg_nombre, String reg_simbolo) {
		super();
		this.reg_id = reg_id;
		this.pais_id = pais_id;
		this.reg_nombre = reg_nombre;
		this.reg_simbolo = reg_simbolo;
	}	
	public Region(int reg_id, int pais_id, String reg_nombre) {
		super();
		this.reg_id = reg_id;
		this.pais_id = pais_id;
		this.reg_nombre = reg_nombre;
	}
	public Region() {
		
	}

	public int getReg_id() {
		return reg_id;
	}

	public void setReg_id(int reg_id) {
		this.reg_id = reg_id;
	}

	public int getPais_id() {
		return pais_id;
	}

	public void setPais_id(int pais_id) {
		this.pais_id = pais_id;
	}

	public String getReg_nombre() {
		return reg_nombre;
	}

	public void setReg_nombre(String reg_nombre) {
		this.reg_nombre = reg_nombre;
	}

	public String getReg_simbolo() {
		return reg_simbolo;
	}

	public void setReg_simbolo(String reg_simbolo) {
		this.reg_simbolo = reg_simbolo;
	}
	
}
