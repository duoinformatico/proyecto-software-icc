package com.icc.modelo;

public class EspecialidadAcademico {
	int esp_id;
	String aca_rut;
	public EspecialidadAcademico(int esp_id, String aca_rut) {
		super();
		this.esp_id = esp_id;
		this.aca_rut = aca_rut;
	}
	public int getEsp_id() {
		return esp_id;
	}
	public void setEsp_id(int esp_id) {
		this.esp_id = esp_id;
	}
	public String getAca_rut() {
		return aca_rut;
	}
	public void setAca_rut(String aca_rut) {
		this.aca_rut = aca_rut;
	}
	
}
