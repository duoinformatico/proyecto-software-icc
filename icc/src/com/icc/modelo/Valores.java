package com.icc.modelo;

public class Valores {
	int val_id;
	int esc_id;
	String val_nombre;
	double val_valor;
	
	public Valores(int val_id, int esc_id, String val_nombre, double val_valor) {
		super();
		this.val_id = val_id;
		this.esc_id = esc_id;
		this.val_nombre = val_nombre;
		this.val_valor = val_valor;
	}
	
	
	public int getVal_id() {
		return val_id;
	}
	public void setVal_id(int val_id) {
		this.val_id = val_id;
	}
	public int getEsc_id() {
		return esc_id;
	}
	public void setEsc_id(int esc_id) {
		this.esc_id = esc_id;
	}
	public String getVal_nombre() {
		return val_nombre;
	}
	public void setVal_nombre(String val_nombre) {
		this.val_nombre = val_nombre;
	}
	public double getVal_valor() {
		return val_valor;
	}
	public void setVal_valor(double val_valor) {
		this.val_valor = val_valor;
	}
	
}
