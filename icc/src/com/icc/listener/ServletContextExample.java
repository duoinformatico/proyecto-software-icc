package com.icc.listener;

import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;


import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * Application Lifecycle Listener implementation class ServletContextExample
 *
 */
@WebListener
public class ServletContextExample implements ServletContextListener {

    /**
     * Default constructor. 
     */
    public ServletContextExample() {
        // TODO Auto-generated constructor stub
    }

	/**
     * @see ServletContextListener#contextDestroyed(ServletContextEvent)
     */
    public void contextDestroyed(ServletContextEvent arg0) {
        // TODO Auto-generated method stub
    	Enumeration<Driver> drivers = DriverManager.getDrivers();
        while (drivers.hasMoreElements()) {
            Driver driver = drivers.nextElement();
            try {
            	System.out.println("good");
                DriverManager.deregisterDriver(driver);
                
            } catch (SQLException e) {
               System.out.println("error al deregistrar");
            }

        }
    	/*
    	try {
    		 System.out.println("good");
            java.sql.Driver Driver = DriverManager.getDriver("jdbc:postgresql://localhost:5432/");
            DriverManager.deregisterDriver(Driver);
        } catch (SQLException ex) {
            //logger.info("Could not deregister driver:".concat(ex.getMessage()));
        	 System.out.println("error al derregistrar");
        } */
    }

	/**
     * @see ServletContextListener#contextInitialized(ServletContextEvent)
     */
    public void contextInitialized(ServletContextEvent arg0) {
        // TODO Auto-generated method stub
    }
	
}
