package com.icc.utilidades;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import com.icc.modelo.Academico;
import com.icc.modelo.Administrador;
import com.icc.modelo.Alumno;
import com.icc.modelo.Area;
import com.icc.modelo.AreaPractica;
import com.icc.modelo.Asignatura;
import com.icc.modelo.AsignaturaTipo;
import com.icc.modelo.Bitacora;
import com.icc.modelo.Carrera;
import com.icc.modelo.Categoria;
import com.icc.modelo.Comuna;
import com.icc.modelo.CursaPlanEstudio;
import com.icc.modelo.Departamento;
import com.icc.modelo.DetalleEvaluacion;
import com.icc.modelo.DetalleEvaluacionAcademico;
import com.icc.modelo.DetalleEvaluacionAlumno;
import com.icc.modelo.DetalleEvaluacionSupervisor;
import com.icc.modelo.Direccion;
import com.icc.modelo.Empresa;
import com.icc.modelo.Escala;
import com.icc.modelo.Especialidad;
import com.icc.modelo.EspecialidadAcademico;
import com.icc.modelo.Evaluacion;
import com.icc.modelo.EvaluacionAcademico;
import com.icc.modelo.EvaluacionAlumno;
import com.icc.modelo.EvaluacionSupervisor;
import com.icc.modelo.Evaluadores;
import com.icc.modelo.Funcion;
import com.icc.modelo.FuncionPractica;
import com.icc.modelo.Giro;
import com.icc.modelo.Imagen;
import com.icc.modelo.InscripcionAsignatura;
import com.icc.modelo.Instrumento;
import com.icc.modelo.Item;
import com.icc.modelo.Obra_tipo;
import com.icc.modelo.Observacion;
import com.icc.modelo.Opcion;
import com.icc.modelo.Plan_estudio;
import com.icc.modelo.Practica;
import com.icc.modelo.Provincia;
import com.icc.modelo.Region;
import com.icc.modelo.RolGestion;
import com.icc.modelo.RolTipoParticipantes;
import com.icc.modelo.Rubro;
import com.icc.modelo.Supervisor;
import com.icc.modelo.Valores;

public class Conexion {
	private java.sql.Connection conexion=null; 
	private java.sql.Connection conexion2=null; 
	private Statement consulta;
	private Statement consulta_dos;
	private Statement consulta_tres;
	private Supervisor supervisor;
	private ResultSet resultado;
	private ResultSet resultado_uno;
	private ResultSet resultado_dos;
	private ResultSet resultado_tres;
	private RolTipoParticipantes tipo;
	private ArrayList<Carrera> carreras;
	private Departamento departamento;
	private Practica pendiente;
	private Bitacora bitacora;
	private Alumno alumno;
	private Area area;
	private Funcion funcion;
	private Administrador administrador;
	private Asignatura asignatura;
	private Empresa empresa;
	private Comuna comuna;
	private Provincia provincia;
	private Region region;
	private Academico academico;
	private Instrumento instrumento;
	private Escala escala;
	private Plan_estudio plan;
	private Carrera carrera;
	private Observacion observacion;
	private Especialidad especialidad;
	private Evaluacion evaluacion;
	private EvaluacionAlumno evaluacion_alumno;
	private EvaluacionSupervisor evaluacion_supervisor;
	private EvaluacionAcademico evaluacion_academico;
	private Evaluadores evaluador;
	private ArrayList<CursaPlanEstudio> cursa_planes;
	private ArrayList<Alumno> alumnos;
	private ArrayList<Giro> giros;
	private ArrayList<Rubro> rubros;
	private ArrayList<Imagen> imagenes;
	private ArrayList<Observacion> observaciones;
	private ArrayList<Practica> practicas;
	private InscripcionAsignatura inscripcion;
	private ArrayList<Direccion> direcciones;
	private ArrayList<Especialidad> especialidades; 
	private ArrayList<EspecialidadAcademico> especialidades_academico;
	private ArrayList<Bitacora> bitacoras;
	private ArrayList<Empresa> empresas;
	private ArrayList<Region> regiones;
	private ArrayList<Provincia> provincias;
	private ArrayList<Comuna> comunas;
	private ArrayList<Administrador> administradores;
	private ArrayList<Departamento> departamentos;
	private ArrayList<Supervisor> supervisores;
	private ArrayList<Academico> academicos;
	private ArrayList<Practica> pendientes;
	private ArrayList<Escala> escalas;
	private ArrayList<Asignatura> gestiones;
	private ArrayList<Asignatura> instrumentos_asignatura;
	private ArrayList<Instrumento> instrumentos;
	private ArrayList<Evaluadores> evaluadores;
	private ArrayList<EvaluacionAlumno> evaluaciones_alumno;
	private ArrayList<EvaluacionSupervisor> evaluaciones_supervisor;
	private ArrayList<EvaluacionAcademico> evaluaciones_academico;
	private ArrayList<Evaluacion> evaluaciones;
	private ArrayList<Item> items;
	private ArrayList<Categoria> categorias;
	private ArrayList<Opcion> opciones;
	private ArrayList<Valores> valores;
	private ArrayList<Obra_tipo> obra_tipos;
	private ArrayList<Area> areas;
	private ArrayList<AreaPractica> areas_practica;
	private ArrayList<Funcion> funciones;
	private ArrayList<FuncionPractica> funciones_practica;
	private ArrayList<Plan_estudio> planes;
	private ArrayList<AsignaturaTipo> tipos_asignatura;
	private ArrayList<InscripcionAsignatura> inscripciones;
	
	public Statement getConsulta() {
		return consulta;
	}
	public void setConsulta(Statement consulta) {
		this.consulta = consulta;
	}
	public ResultSet getResultado() {
		return resultado;
	}
	public void setResultado(ResultSet resultado) {
		this.resultado = resultado;
	}
	public void ConexionBd()
	{
		try 
		{
			Class.forName("org.postgresql.Driver");
			//conexion = java.sql.DriverManager.getConnection("jdbc:postgresql://localhost:5538/icc","ico_usad","facd2905");
			//conexion = java.sql.DriverManager.getConnection("jdbc:postgresql://localhost:5432/icc_nov","postgres","postgresqlarchlinux");
			//conexion = java.sql.DriverManager.getConnection("jdbc:postgresql://localhost:5432/icc_final2","postgres","root");

			consulta = conexion.createStatement();	
			consulta_dos = conexion.createStatement();	
			consulta_tres = conexion.createStatement();
		} catch (Exception e) {
		}		
	}
	public Connection ConexionBdRetorna()
	{
		try 
		{
			Class.forName("org.postgresql.Driver");
			//conexion = java.sql.DriverManager.getConnection("jdbc:postgresql://localhost:5538/icc","ico_usad","facd2905");
			//conexion = java.sql.DriverManager.getConnection("jdbc:postgresql://localhost:5432/icc_nov","postgres","postgresqlarchlinux");
			//conexion = java.sql.DriverManager.getConnection("jdbc:postgresql://localhost:5432/icc_final2","postgres","root");

			consulta = conexion.createStatement();	
			consulta_dos = conexion.createStatement();	
			consulta_tres = conexion.createStatement();
		} catch (Exception e) {
		}
		return conexion;		
	}
	public void ConexionBd2()
	{
		try
		{
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			//String connectionUrl = "jdbc:sqlserver://PERSONAL\\SQLEXPRESS;database=SAISTECH;user=sa;password=sa";
			/*String connectionString = "jdbc:jtds:sqlserver://146.83.195.109/academia";
			ResultSet rs = null;
			Statement stmt = null;

			try{

			    Class.forName("net.sourceforge.jtds.jdbc.Driver");
			    Connection conn = DriverManager.getConnection(connectionString,"web_practica_eic","6Gloseplo");

			    stmt = conn.createStatement();
			    System.out.println("exito");
			}catch(Exception e){
				e.printStackTrace();
			}*/
			String connectionUrl = "jdbc:sqlserver://146.83.195.109;database=academia;user=web_practica_eic;password=6Gloseplo";
            conexion2 = DriverManager.getConnection(connectionUrl);
            System.out.println("CONEXION EXITOSA A LA BD");

            consulta = conexion2.createStatement();	
			consulta_dos = conexion2.createStatement();	
			consulta_tres = conexion2.createStatement();

		}
		catch (ClassNotFoundException ex) {
			System.out.println("Error en el driver");
		} catch(SQLException e){
			System.out.println("Error en la conexion");
			e.printStackTrace();
		}	
	}
	public void CerrarConexionBd() throws SQLException
	{
		conexion.close();
	}
	public void CerrarConexionBd2() throws SQLException
	{
		conexion2.close();
	}
	public int verificar_clave(String rut, String clave, String tipo)
	{
		try
		{
			ConexionBd();
			PreparedStatement enunciado = null;
			PreparedStatement enunciado_dos = null;
			int ban=0;
			if(tipo.equals("admin"))
			{
				enunciado = conexion.prepareStatement("select count(ADM_RUT) AS VALIDO FROM ADMINISTRADOR "
						+ "WHERE ADM_RUT = ? AND ADM_PASSWORD = ?;");
			}
			if(tipo.equals("administrador-academico"))
			{
				enunciado = conexion.prepareStatement("select count(ADM_RUT) AS VALIDO FROM ADMINISTRADOR "
						+ "WHERE ADM_RUT = ? AND ADM_PASSWORD = ?;");
				enunciado_dos = conexion.prepareStatement("select count(ACA_RUT) AS VALIDO FROM ACADEMICO "
						+ "WHERE ACA_RUT = ? AND ACA_PASSWORD = ?;");
				ban=1;
			}
			if(tipo.equals("supervisor"))
			{
				enunciado = conexion.prepareStatement("select count(SUP_RUT) AS VALIDO FROM SUPERVISOR "
						+ "WHERE SUP_RUT = ? AND SUP_PASSWORD = ?;");
			}
			if(tipo.equals("academico"))
			{
				enunciado = conexion.prepareStatement("select count(ACA_RUT) AS VALIDO FROM ACADEMICO "
						+ "WHERE ACA_RUT = ? AND ACA_PASSWORD = ?;");
			}
			if(tipo.equals("alumno"))
			{
				enunciado = conexion.prepareStatement("select count(ALU_RUT) AS VALIDO FROM ALUMNO "
						+ "WHERE ALU_RUT = ? AND ALU_PASSWORD = ?;");
			}
			if(ban==0)
			{
				enunciado.setString(1, rut);
				enunciado.setString(2, clave);
				resultado = enunciado.executeQuery();
				resultado.next();
				int valido= resultado.getInt("VALIDO");
				if(valido==1)
				{
					return 1;
				}else{
					return 0;
				}
			}
			if(ban==1)
			{
				enunciado.setString(1, rut);
				enunciado.setString(2, clave);
				resultado = enunciado.executeQuery();
				resultado.next();
				int valido= resultado.getInt("VALIDO");
				
				enunciado_dos.setString(1, rut);
				enunciado_dos.setString(2, clave);
				resultado_dos = enunciado_dos.executeQuery();
				resultado_dos.next();
				int valido_dos= resultado_dos.getInt("VALIDO");
				if(valido==1 && valido_dos==1)
				{
					return 1;
				}else{
					return 0;
				}
			}
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return 0;
		}
		return 5;
	}
	public int verificar_alumno(String rut, String clave)
	{
		try
		{
			ConexionBd();
			PreparedStatement enunciado = conexion.prepareStatement("select count(ALU_RUT) AS VALIDO FROM ALUMNO "
					+ "WHERE ALU_RUT = ? AND ALU_PASSWORD = ? AND ALU_ESTADO = 'activo';");
			enunciado.setString(1, rut);
			enunciado.setString(2, clave);
			resultado = enunciado.executeQuery();
			resultado.next();
			int valido= resultado.getInt("VALIDO");
			if(valido==1)
			{
				return 1;
			}else{
				return 0;
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return 0;
		}
	}
	
	public int verificar_administrador(String rut,String clave)
	{
		try
		{
			ConexionBd();		
			PreparedStatement enunciado = conexion.prepareStatement("select count(ADM_RUT) AS VALIDO FROM ADMINISTRADOR "
					+ "WHERE ADM_RUT = ? and ADM_PASSWORD = ? AND ADM_ESTADO = 'activo';");
			enunciado.setString(1, rut);
			enunciado.setString(2, clave);
			resultado = enunciado.executeQuery();
			resultado.next();
			int valido= resultado.getInt("VALIDO");
			if(valido==1)
			{
				return 1;
			}else{
				return 0;
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return 0;
		}
	}
	public int verificar_supervisor(String rut, String clave)
	{
		try
		{
			ConexionBd();
			PreparedStatement enunciado = conexion.prepareStatement("select count(SUP_RUT) AS VALIDO FROM SUPERVISOR "
					+ "WHERE SUP_RUT = ? and SUP_PASSWORD = ? AND SUP_ESTADO = 'activo';");
			enunciado.setString(1, rut);
			enunciado.setString(2, clave);
			resultado = enunciado.executeQuery();
			resultado.next();
			int valido= resultado.getInt("VALIDO");
			if(valido==1)
			{
				return 1;
			}else{
				return 0;
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return 0;
		}
	}
	public int verificar_academico(String rut, String clave)
	{
		try
		{
			ConexionBd();
			PreparedStatement enunciado = conexion.prepareStatement("select count(ACA_RUT) AS VALIDO FROM ACADEMICO "
					+ "WHERE ACA_RUT = ? and ACA_PASSWORD = ? AND ACA_ESTADO = 'activo';");
			enunciado.setString(1, rut);
			enunciado.setString(2, clave);
			resultado = enunciado.executeQuery();
			resultado.next();
			int valido= resultado.getInt("VALIDO");
			if(valido==1)
			{
				return 1;
			}else{
				return 0;
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return 0;
		}
	}
	
	public String getIdentificacion(String rut, String tipo){//retorna en una cadena nombres + apellidos de un usuario autentificado
		String nombre_usuario = "";
		try
		{
			ConexionBd();
			if(tipo == "administrador"){
				PreparedStatement enunciado = conexion.prepareStatement("SELECT adm_nombres, adm_apellido_p, adm_apellido_m "
												+ "FROM administrador "
												+ "WHERE adm_rut = ? ");
				enunciado.setString(1, rut);
				resultado = enunciado.executeQuery();
				resultado.next();
				nombre_usuario = resultado.getString("adm_nombres");
				nombre_usuario += " "+resultado.getString("adm_apellido_p");
				nombre_usuario += " "+resultado.getString("adm_apellido_m");
				System.out.println("Login "+tipo+" :"+nombre_usuario+" ("+rut+")");
				
			}
			
			if(tipo == "alumno"){
				PreparedStatement enunciado = conexion.prepareStatement("SELECT alu_nombres, alu_apellido_p, alu_apellido_m "
						+ "FROM ALUMNO "
						+ "WHERE alu_rut = ? ");
				enunciado.setString(1, rut);
				System.out.println("datos"+enunciado.toString() );
				resultado = enunciado.executeQuery();
				resultado.next();
				nombre_usuario = resultado.getString("alu_nombres");
				nombre_usuario += " "+resultado.getString("alu_apellido_p");
				nombre_usuario += " "+resultado.getString("alu_apellido_m");
				System.out.println("Login "+tipo+" :"+nombre_usuario+" ("+rut+")");
				
			}
			
			if(tipo == "academico"){
				PreparedStatement enunciado = conexion.prepareStatement("SELECT aca_nombres, aca_apellido_p, aca_apellido_m "
						+ "FROM academico "
						+ "WHERE aca_rut = ? ");
				enunciado.setString(1, rut);
				resultado = enunciado.executeQuery();
				resultado.next();
				nombre_usuario = resultado.getString("aca_nombres");
				nombre_usuario += " "+resultado.getString("aca_apellido_p");
				nombre_usuario += " "+resultado.getString("aca_apellido_m");
				System.out.println("Login "+tipo+" :"+nombre_usuario+" ("+rut+")");
				
			}
			
			if(tipo == "supervisor"){
				PreparedStatement enunciado = conexion.prepareStatement("SELECT sup_nombres, sup_apellido_p, sup_apellido_m "
						+ "FROM SUPERVISOR "
						+ "WHERE sup_rut = ? ");
				enunciado.setString(1, rut);
				resultado = enunciado.executeQuery();
				resultado.next();
				nombre_usuario = resultado.getString("sup_nombres");
				nombre_usuario += " "+resultado.getString("sup_apellido_p");
				nombre_usuario += " "+resultado.getString("sup_apellido_m");
				System.out.println("Login "+tipo+" :"+nombre_usuario+" ("+rut+")");
				
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return nombre_usuario;
	}
	
	public int AgregarPractica(Practica practica)
	{
		try
		{
			ConexionBd();
			PreparedStatement enunciado = conexion.prepareStatement("insert into practica(emp_rut,sup_rut,com_id,obr_id,alu_rut,asi_codigo,"
					+ "plan_id,ins_fecha,pra_obra_nombre,pra_direccion,pra_tareas,pra_fecha_ini,pra_fecha_fin,pra_horas,pra_estado,aca_rut)"
					+ " values(?,?,?,?,?,?,?,CAST(? AS DATE),?,?,?,CAST(? AS DATE),CAST(? AS DATE),?,'Pendiente',?)");
			enunciado.setString(1, practica.getEmp_rut() );
			enunciado.setString(2, practica.getSup_rut());
			enunciado.setInt(3, practica.getCom_id());
			enunciado.setInt(4, practica.getObr_id());
			enunciado.setString(5, practica.getAlu_rut());
			enunciado.setInt(6, practica.getAsi_codigo());
			enunciado.setInt(7, practica.getPlan_id());
			enunciado.setString(8, practica.getIns_fecha());
			enunciado.setString(9, practica.getPra_obra_nombre());
			enunciado.setString(10, practica.getPra_direccion());
			enunciado.setString(11, practica.getPra_tareas());
			enunciado.setString(12, practica.getPra_fecha_ini());
			enunciado.setString(13, practica.getPra_fecha_fin());
			enunciado.setInt(14, practica.getPra_horas());
			enunciado.setString(15, practica.getAca_rut());
			/*
			PreparedStatement enunciado = conexion.prepareStatement("insert into practica(emp_rut,sup_rut,aca_rut,com_id,obr_id,alu_rut,asi_codigo,"
					+ "plan_id,ins_fecha,pra_obra_nombre,pra_fecha_ins,pra_direccion,pra_tareas,pra_fecha_ini,pra_fecha_fin,pra_horas,pra_estado)"
					+ " values(?,?,?,?,?,?,?,?,CAST(? AS DATE),?,current_date,?,?,CAST(? AS DATE),CAST(? AS DATE),?,'Pendiente')");
			enunciado.setString(1, practica.getEmp_rut() );
			enunciado.setString(2, practica.getSup_rut());
			enunciado.setString(3, practica.getAca_rut());
			enunciado.setInt(4, practica.getCom_id());
			enunciado.setInt(5, practica.getObr_id());
			enunciado.setString(6, practica.getAlu_rut());
			enunciado.setInt(7, practica.getAsi_codigo());
			enunciado.setInt(8, practica.getPlan_id());
			enunciado.setString(9, practica.getIns_fecha());
			enunciado.setString(10, practica.getPra_obra_nombre());
			enunciado.setString(11, practica.getPra_direccion());
			enunciado.setString(12, practica.getPra_tareas());
			enunciado.setString(13, practica.getPra_fecha_ini());
			enunciado.setString(14, practica.getPra_fecha_fin());
			enunciado.setInt(15, practica.getPra_horas());
			*/
			enunciado.execute();			
			conexion.close();
			return 1;
		}
		catch(SQLException e)
		{
			e.printStackTrace();
			return 2;
		}
	}
	public int agregarAlumnoAutoEvaluador(int codigo_asignatura, int id_plan , int rol_id, String alu_rut)
	{
		
		return 0;
	}
	public int AgregarEmpresa(Empresa empresa)
	{
		try
		{
			ConexionBd();
			PreparedStatement enunciado = conexion.prepareStatement("insert into empresa values(?,?,?,?,?,?,?,?,?)");
			enunciado.setString(1, empresa.getEmp_rut() );
			enunciado.setInt(2, empresa.getCom_id());
			enunciado.setString(3, empresa.getEmp_nombre());
			enunciado.setString(4, empresa.getEmp_direccion());
			enunciado.setString(5, empresa.getEmp_telefono());
			enunciado.setString(6, empresa.getEmp_celular());
			enunciado.setString(7, empresa.getEmp_descripcion());
			enunciado.setString(8, empresa.getEmp_email());
			enunciado.setString(9, empresa.getEmp_web());
			enunciado.execute();			
			conexion.close();
			return 1;
		}
		catch(SQLException e)
		{
			e.printStackTrace();
			return 2;
		}
	}
	public int agregarCursaPlan(CursaPlanEstudio cursa, String cur_estado, boolean cur_vigente)
	{
		try
		{
			ConexionBd();
			PreparedStatement enunciado = conexion.prepareStatement("insert into CURSA_PLAN_ESTUDIO(PLAN_ID,ALU_RUT,CUR_AGNIO_INGRESO,CUR_SEMESTRE,CUR_ESTADO,CUR_VIGENTE) values(?,?,?,?,?,?)");
			enunciado.setInt(1, cursa.getPlan_id());
			enunciado.setString(2, cursa.getAlu_rut());
			enunciado.setInt(3, cursa.getCur_agnio_ingreso());
			enunciado.setInt(4, cursa.getCur_semestre());
			enunciado.setString(5, cur_estado);
			enunciado.setBoolean(6, cur_vigente);
			enunciado.execute();			
			conexion.close();
			return 1;
		}
		catch(SQLException e)
		{
			e.printStackTrace();
			return 2;
		}
	}
	public int agregarCursaPlanConParametros(Connection conexion_alumno,CursaPlanEstudio cursa, String cur_estado, boolean cur_vigente)
	{
		try
		{
			PreparedStatement enunciado = conexion_alumno.prepareStatement("insert into CURSA_PLAN_ESTUDIO(PLAN_ID,ALU_RUT,CUR_AGNIO_INGRESO,CUR_SEMESTRE,CUR_ESTADO,CUR_VIGENTE) values(?,?,?,?,?,?)");
			enunciado.setInt(1, cursa.getPlan_id());
			enunciado.setString(2, cursa.getAlu_rut());
			enunciado.setInt(3, cursa.getCur_agnio_ingreso());
			enunciado.setInt(4, cursa.getCur_semestre());
			enunciado.setString(5, cur_estado);
			enunciado.setBoolean(6, cur_vigente);
			enunciado.execute();			
			return 1;
		}
		catch(SQLException e)
		{
			e.printStackTrace();
			return 2;
		}
	}
	public int agregarAlumno(Alumno alumno)
	{
		try
		{
			ConexionBd();
			PreparedStatement enunciado = conexion.prepareStatement("insert into ALUMNO(ALU_RUT,ALU_PASSWORD,ALU_NOMBRES,ALU_APELLIDO_P,ALU_APELLIDO_M,ALU_EMAIL,ALU_ESTADO) values(?,?,?,?,?,?,?)");
			enunciado.setString(1, alumno.getAlu_rut());
			enunciado.setString(2, alumno.getAlu_password());
			enunciado.setString(3, alumno.getAlu_nombres());
			enunciado.setString(4, alumno.getAlu_apellido_p());
			enunciado.setString(5, alumno.getAlu_apellido_m());
			enunciado.setString(6, alumno.getAlu_email());
			enunciado.setString(7, alumno.getAlu_estado());
			enunciado.execute();			
			conexion.close();
			return 1;
		}
		catch(SQLException e)
		{
			return 2;
		}
	}
	public int agregarAlumnoConParametros(Connection conexion_alumno,Alumno alumno)
	{
		try
		{
			System.out.println(" rut "+alumno.getAlu_rut());
			PreparedStatement enunciado = conexion_alumno.prepareStatement("insert into ALUMNO(ALU_RUT,ALU_PASSWORD,ALU_NOMBRES,ALU_APELLIDO_P,ALU_APELLIDO_M,ALU_EMAIL,ALU_ESTADO) values(?,?,?,?,?,?,?)");
			enunciado.setString(1, alumno.getAlu_rut());
			enunciado.setString(2, alumno.getAlu_password());
			enunciado.setString(3, alumno.getAlu_nombres());
			enunciado.setString(4, alumno.getAlu_apellido_p());
			enunciado.setString(5, alumno.getAlu_apellido_m());
			enunciado.setString(6, alumno.getAlu_email());
			enunciado.setString(7, alumno.getAlu_estado());
			enunciado.execute();			
			return 1;
		}
		catch(SQLException e)
		{
			e.printStackTrace();
			return 2;
		}
	}
	public int agregarAdministrador(Administrador administrador)
	{
		try
		{
			ConexionBd();
			PreparedStatement enunciado = conexion.prepareStatement("insert into Administrador values(?,?,?,?,?,?,?,?,?,?)");
			enunciado.setString(1, administrador.getAdm_rut() );
			enunciado.setString(2, administrador.getAdm_password());
			enunciado.setString(3, administrador.getAdm_nombres());
			enunciado.setString(4, administrador.getAdm_apellido_p());
			enunciado.setString(5, administrador.getAdm_apellido_m());
			enunciado.setString(6, administrador.getAdm_email());
			enunciado.setString(7, administrador.getAdm_telefono());
			enunciado.setString(8, administrador.getAdm_celular());
			enunciado.setString(9, administrador.getAdm_cargo());
			enunciado.setString(10, administrador.getAdm_estado());
			enunciado.execute();			
			conexion.close();
			return 1;
		}
		catch(SQLException e)
		{
			e.printStackTrace();
			return 2;
		}
	}
	public int agregarSupervisor(Supervisor supervisor)
	{
		try
		{
			ConexionBd();
			PreparedStatement enunciado = conexion.prepareStatement("insert into Supervisor values(?,?,?,?,?,?,?,?,?,?,?)");
			enunciado.setString(1, supervisor.getSup_rut() );
			enunciado.setString(2, supervisor.getSup_password());
			enunciado.setString(3, supervisor.getSup_nombres());
			enunciado.setString(4, supervisor.getSup_apellido_p());
			enunciado.setString(5, supervisor.getSup_apellido_m());
			enunciado.setString(6, supervisor.getSup_email());
			enunciado.setString(7, supervisor.getSup_telefono());
			enunciado.setString(8, supervisor.getSup_celular());
			enunciado.setString(9, supervisor.getSup_profesion());
			enunciado.setString(10, supervisor.getSup_cargo());
			enunciado.setString(11, supervisor.getSup_estado());
			enunciado.execute();			
			conexion.close();
			return 1;
		}
		catch(SQLException e)
		{
			e.printStackTrace();
			return 0;
		}
	}
	public int AgregarObra(Obra_tipo obra) throws SQLException
	{
		try 
		{
			ConexionBd();
			PreparedStatement enunciado = conexion.prepareStatement("insert into obra_tipo(obr_nombre) values(?)");
			enunciado.setString(1, obra.getObr_nombre());
			enunciado.execute();
			conexion.close();
			return 1;
		}catch(SQLException e)
		{
			e.printStackTrace();
			return 2;
		}
	}
	
	public int agregarPlan(Plan_estudio plan) throws SQLException
	{
		try 
		{
			ConexionBd();
			PreparedStatement enunciado = conexion.prepareStatement("insert into plan_estudio(car_codigo,plan_nombre,plan_fecha,plan_estado,plan_vigente) values(?,?,current_date,'activo',true)");
			enunciado.setInt(1, plan.getCar_codigo());
			enunciado.setString(2, plan.getPlan_nombre());
			enunciado.execute();
			conexion.close();
			return 1;
		}catch(SQLException e)
		{
			e.printStackTrace();
			return 2;
		}
	}
	
	public int getUltimoPlanId() throws SQLException
	{
		ConexionBd();
		try {
			resultado = consulta.executeQuery("select max(plan_id) as plan_id from plan_estudio");
			resultado.next();

			CerrarConexionBd();
			return  resultado.getInt("plan_id");
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	public int agregarInscripcionAsignatura(InscripcionAsignatura inscripcion) throws SQLException
	{
		try 
		{
			ConexionBd();
			PreparedStatement enunciado = conexion.prepareStatement("insert into inscripcion_asignatura(alu_rut, asi_codigo, plan_id, ins_fecha, ins_semestre, ins_nota, ins_estado) values(?,?,?,to_date(?, 'YYYY-MM-DD'),?,?,?)");
			enunciado.setString(1, inscripcion.getAlu_rut());
			enunciado.setInt(2, inscripcion.getAsi_codigo());
			enunciado.setInt(3, inscripcion.getPlan_id());
			enunciado.setString(4, inscripcion.getIns_fecha());
			enunciado.setInt(5, inscripcion.getIns_semestre());
			enunciado.setDouble(6, inscripcion.getIns_nota());
			enunciado.setString(7, inscripcion.getIns_estado().trim());
			enunciado.execute();
			conexion.close();
			return 1;
		}catch(SQLException e)
		{
			e.printStackTrace();
			return 2;
		}
	}
	
	
	
	public int agregarInscripcionAsignatura(Connection conexion, InscripcionAsignatura inscripcion) throws SQLException
	{
		try 
		{
			PreparedStatement enunciado = conexion.prepareStatement("insert into inscripcion_asignatura(alu_rut, asi_codigo, plan_id, ins_fecha, ins_semestre, ins_nota, ins_estado) values(?,?,?,to_date(?, 'YYYY-MM-DD'),?,?,?)");
			enunciado.setString(1, inscripcion.getAlu_rut());
			enunciado.setInt(2, inscripcion.getAsi_codigo());
			enunciado.setInt(3, inscripcion.getPlan_id());
			enunciado.setString(4, inscripcion.getIns_fecha());
			enunciado.setInt(5, inscripcion.getIns_semestre());
			enunciado.setDouble(6, inscripcion.getIns_nota());
			enunciado.setString(7, inscripcion.getIns_estado().trim());
			enunciado.execute();

			return 1;
		}catch(SQLException e)
		{
			e.printStackTrace();
			return 2;
		}
	}
	
	public int actualizarInscripcionAsignatura(Connection conexion, InscripcionAsignatura inscripcion) throws SQLException
	{
		try 
		{
			PreparedStatement enunciado = conexion.prepareStatement("UPDATE inscripcion_asignatura SET ins_semestre = ?, ins_nota = ?, ins_estado = ? WHERE alu_rut = ? AND asi_codigo = ? AND plan_id = ? AND ins_fecha = CAST(? AS DATE)");
			
			enunciado.setInt(1, inscripcion.getIns_semestre());
			enunciado.setDouble(2, inscripcion.getIns_nota());
			enunciado.setString(3, inscripcion.getIns_estado().trim());
			enunciado.setString(4, inscripcion.getAlu_rut());
			enunciado.setInt(5, inscripcion.getAsi_codigo());
			enunciado.setInt(6, inscripcion.getPlan_id());
			enunciado.setString(7, inscripcion.getIns_fecha());
			enunciado.execute();
			return 1;
		}catch(SQLException e)
		{
			e.printStackTrace();
			return 2;
		}
	}
	
	
	
	public int agregarImagen(Imagen imagen) throws SQLException
	{
		try 
		{
			ConexionBd();
			PreparedStatement enunciado = conexion.prepareStatement("insert into imagen(img_url,img_fecha,img_hora) values(?,current_date,current_time)");
			enunciado.setString(1, imagen.getImg_url());
			enunciado.execute();
			conexion.close();
			return 1;
		}catch(SQLException e)
		{
			e.printStackTrace();
			return 2;
		}
	}
	//agrega datos a la tabla imagen_bitacora
	public int agregarImagenBitacora(int bit_id, int img_id) throws SQLException
	{
		try 
		{
			ConexionBd();
			PreparedStatement enunciado = conexion.prepareStatement("insert into imagen_bitacora(img_id,bit_id) values(?,?)");
			enunciado.setInt(1, img_id);
			enunciado.setInt(2, bit_id);
			enunciado.execute();
			conexion.close();
			return 1;
		}catch(SQLException e)
		{
			e.printStackTrace();
			return 2;
		}
	}
	public void agregarHistorialSupervisor(String emp_rut,String sup_rut) throws SQLException
	{
		try 
		{
			ConexionBd();
			PreparedStatement enunciado = conexion.prepareStatement("insert into historial_supervisor(sup_rut,emp_rut,his_fecha,his_estado,his_vigencia) values(?,?,current_date,'activo',true)");
			enunciado.setString(1, sup_rut);
			enunciado.setString(2, emp_rut);
			enunciado.execute();
			conexion.close();
		}catch(SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	public void agregarAsignatura(int asi_codigo, int plan_id, int asi_tp_id, String asi_nombre, int asi_horas, int asi_semestre, int asi_sct) throws SQLException
	{
		try 
		{
			ConexionBd();
			PreparedStatement enunciado = conexion.prepareStatement("insert into asignatura(asi_codigo,plan_id,asi_tp_id,asi_nombre,asi_horas,asi_semestre,asi_sct) values(?,?,?,?,?,?,?)");
			enunciado.setInt(1, asi_codigo);
			enunciado.setInt(2, plan_id);
			enunciado.setInt(3, asi_tp_id);
			enunciado.setString(4, asi_nombre);
			enunciado.setInt(5, asi_horas);
			enunciado.setInt(6, asi_semestre);
			enunciado.setInt(7, asi_sct);
			enunciado.execute();
			conexion.close();
			
		}catch(SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	
	// m�todo que registra una asignatura de una bitacora en la tabla asignatura_bitacora. 
	public void agregarAsignaturaBitacora(int codigo_asignatura, int bit_id, int plan_id) throws SQLException
	{
		try 
		{
			ConexionBd();
			PreparedStatement enunciado = conexion.prepareStatement("insert into asignatura_bitacora(bit_id,asi_codigo,plan_id) values(?,?,?)");
			enunciado.setInt(1, bit_id);
			enunciado.setInt(2, codigo_asignatura);
			enunciado.setInt(3, plan_id);
			enunciado.execute();
			conexion.close();
			
		}catch(SQLException e)
		{
			e.printStackTrace();
		}
	}
	public void agregarProfesorGuiaEvaluador(int asignatura, int plan_id, int rol_id,String aca_rut) throws SQLException
	{
		try 
		{
			ConexionBd();
			PreparedStatement enunciado = conexion.prepareStatement("insert into evaluadores(asi_codigo,plan_id,rol_id,aca_rut,eval_fecha,eval_estado) values(?,?,?,?,current_date,'Activo')");
			enunciado.setInt(1, asignatura);
			enunciado.setInt(2, plan_id);
			enunciado.setInt(3, rol_id);
			enunciado.setString(4, aca_rut);
			enunciado.execute();
			conexion.close();
			
		}catch(SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	
	public int agregarEspecialidad( Especialidad especialidad) throws SQLException
	{
		try 
		{
			ConexionBd();
			PreparedStatement enunciado = conexion.prepareStatement("insert into Especialidad(esp_nombre) values(?)");
			enunciado.setString(1, especialidad.getEsp_nombre());
			enunciado.execute();
			conexion.close();
			return 1;
		}catch(SQLException e)
		{
			e.printStackTrace();
			return 2;
		}
	}
	public int agregarArea(Area area) throws SQLException
	{
		try 
		{
			ConexionBd();
			PreparedStatement enunciado = conexion.prepareStatement("insert into area(area_nombre) values(?)");
			enunciado.setString(1, area.getArea_nombre());
			enunciado.execute();
			conexion.close();
			return 1;
		}catch(SQLException e)
		{
			e.printStackTrace();
			return 2;
		}
	}
	public int agregarFuncion(Funcion funcion) throws SQLException
	{
		try 
		{
			ConexionBd();
			PreparedStatement enunciado = conexion.prepareStatement("insert into funcion(fun_nombre) values(?)");
			enunciado.setString(1, funcion.getFun_nombre());
			enunciado.execute();
			conexion.close();
			return 1;
		}catch(SQLException e)
		{
			e.printStackTrace();
			return 2;
		}
	}
	public int agregarEspecialidadAcademico( int id, String rut) throws SQLException
	{
		try 
		{
			ConexionBd();
			PreparedStatement enunciado = conexion.prepareStatement("insert into especialidad_academico(esp_id,aca_rut) values(?,?)");
			enunciado.setInt(1, id);
			enunciado.setString(2, rut);
			enunciado.execute();
			conexion.close();
			return 1;
		}catch(SQLException e)
		{
			e.printStackTrace();
			return 2;
		}
	}
	
	public void agregarObservacion(String observacion) throws SQLException
	{
		try 
		{
			ConexionBd();
			PreparedStatement enunciado = conexion.prepareStatement("insert into observacion(obs_texto,obs_fecha,obs_hora) values(?,current_date,current_time)");
			enunciado.setString(1, observacion);
			enunciado.execute();
			conexion.close();
		}catch(SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	public void agregarObservacionBitacora(int bit_id,int obs_id) throws SQLException
	{
		try 
		{
			ConexionBd();
			PreparedStatement enunciado = conexion.prepareStatement("insert into observacion_bitacora(bit_id,obs_id) values(?,?)");
			enunciado.setInt(1, bit_id);
			enunciado.setInt(2, obs_id);
			enunciado.execute();
			conexion.close();
		}catch(SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	public void agregarAlumnoEvaluador(int asignatura, int plan_id, int rol_id,String alu_rut) throws SQLException
	{
		try 
		{
			ConexionBd();
			PreparedStatement enunciado = conexion.prepareStatement("insert into evaluadores(asi_codigo,plan_id,rol_id,alu_rut,eval_fecha,eval_estado) values(?,?,?,?,current_date,'Activo')");
			enunciado.setInt(1, asignatura);
			enunciado.setInt(2, plan_id);
			enunciado.setInt(3, rol_id);
			enunciado.setString(4, alu_rut);
			enunciado.execute();
			conexion.close();
		}catch(SQLException e)
		{
			e.printStackTrace();
		}
	}
	public void crearProfesorGuiaEvaluacion(String aca_rut,int pra_id,int rol_id)
	{
		try 
		{
			ConexionBd();
			PreparedStatement enunciado = conexion.prepareStatement("insert into evaluacion_academico(aca_rut,pra_id,eva_fecha,rol_id,eva_nombre,eva_estado) values(?,?,current_date,?,'Profesor Guía','Sin Evaluar')");
			enunciado.setString(1, aca_rut);
			enunciado.setInt(2, pra_id);
			enunciado.setInt(3, rol_id);
			enunciado.execute();	
			conexion.close();		
		}catch(SQLException e)
		{
			e.printStackTrace();		
		}
	}
	
	public void crearProfesorLectorEvaluacion(String aca_rut,int pra_id,int rol_id) throws SQLException
	{
		try 
		{
			ConexionBd();
			PreparedStatement enunciado = conexion.prepareStatement("insert into evaluacion_academico(aca_rut,pra_id,eva_fecha,rol_id,eva_nombre,eva_estado) values(?,?,current_date,?,'Evaluación Bitácora','Sin Evaluar')");
			enunciado.setString(1, aca_rut);
			enunciado.setInt(2, pra_id);
			enunciado.setInt(3, rol_id);
			enunciado.execute();	
			conexion.close();			
			
		}catch(SQLException e)
		{
			e.printStackTrace();
		}	
	}
	
	public void crearProfesorComisionEvaluacion(String aca_rut,int pra_id,int rol_id) throws SQLException
	{			
		try 
		{
			ConexionBd();
			PreparedStatement enunciado = conexion.prepareStatement("insert into evaluacion_academico(aca_rut,pra_id,eva_fecha,rol_id,eva_nombre,eva_estado) values(?,?,current_date,?,'Evaluación Comisión','Sin Evaluar')");
			enunciado.setString(1, aca_rut);
			enunciado.setInt(2, pra_id);
			enunciado.setInt(3, rol_id);
			enunciado.execute();	
			conexion.close();
			
			
		}catch(SQLException e)
		{
			e.printStackTrace();
		}		
	}
	
	public void crearSupervisorEvaluacion(String sup_rut,int pra_id,int rol_id)
	{
		try 
		{
			ConexionBd();

				PreparedStatement enunciado = conexion.prepareStatement("insert into evaluacion_supervisor(sup_rut,pra_id,eva_fecha,rol_id,eva_nombre,eva_estado) values(?,?,current_date,?,'Evaluación Supervisor','Sin Evaluar')");
				enunciado.setString(1, sup_rut);
				enunciado.setInt(2, pra_id);
				enunciado.setInt(3, rol_id);
				enunciado.execute();	
				conexion.close();
		}catch(SQLException e)
		{
			e.printStackTrace();
		}
	}
	public void crearAlumnoEvaluacion(String alu_rut,int pra_id,int rol_id)
	{
		try 
		{
			ConexionBd();
			PreparedStatement enunciado = conexion.prepareStatement("insert into evaluacion_alumno(alu_rut,pra_id,eva_fecha,rol_id,eva_nombre,eva_estado) values(?,?,current_date,?,'Autoevaluación','Sin Evaluar')");
			enunciado.setString(1, alu_rut);
			enunciado.setInt(2, pra_id);
			enunciado.setInt(3, rol_id);
			enunciado.execute();	
			conexion.close();
		}catch(SQLException e)
		{
			e.printStackTrace();
		}
	}
	public int AgregarAcademico(Academico academico) throws SQLException
	{
		try {
			ConexionBd();
			PreparedStatement enunciado = conexion.prepareStatement("insert into academico(aca_rut,dep_id,aca_password,aca_nombres,aca_apellido_p,aca_apellido_m,aca_email,aca_telefono,aca_celular,aca_n_especialidad,aca_estado) values(?,?,?,?,?,?,?,?,?,4,?)");
			enunciado.setString(1, academico.getAca_rut() );
			enunciado.setInt(2, academico.getDep_id());
			enunciado.setString(3, academico.getAca_password());
			enunciado.setString(4, academico.getAca_nombres());
			enunciado.setString(5, academico.getAca_apellido_p());
			enunciado.setString(6, academico.getAca_apellido_m());
			enunciado.setString(7, academico.getAca_email());
			enunciado.setString(8, academico.getAca_telefono());
			enunciado.setString(9, academico.getAca_celular());
			enunciado.setString(10, academico.getAca_estado());
			enunciado.execute();			
			conexion.close();
			return 1;
		} catch (SQLException e) {
			e.printStackTrace();
			return 2;
		}
	}
	
	public int agregarInstrumento(Instrumento instrumento)
	{
		try 
		{
			ConexionBd();
			PreparedStatement enunciado = conexion.prepareStatement("insert into instrumento(esc_id, rol_id, ins_nombre, ins_num_item, ins_max_puntaje, ins_descripcion, ins_fecha_ini, ins_estado) values(?,?,?,?,?,?, CURRENT_DATE, 'activo')");
			enunciado.setInt(1, instrumento.getEsc_id());
			enunciado.setInt(2, instrumento.getRol_id());
			enunciado.setString(3, instrumento.getIns_nombre());
			enunciado.setInt(4, instrumento.getIns_num_item());
			enunciado.setDouble(5, instrumento.getIns_max_puntaje());
			enunciado.setString(6, instrumento.getIns_descripcion());
			enunciado.execute();
			conexion.close();
			return 1;
		}catch(SQLException e)
		{
			e.printStackTrace();
			return 2;
		}
	}
	
	public int agregarItem(Item item)
	{
		try 
		{
			ConexionBd();
			PreparedStatement enunciado = conexion.prepareStatement("insert into item(ins_id, esc_id, cat_id, item_nombre) values(?,?,?,?)");
			enunciado.setInt(1, item.getIns_id());
			enunciado.setInt(2, item.getEsc_id());
			enunciado.setInt(3, item.getCat_id());
			enunciado.setString(4,item.getItem_nombre());
			enunciado.execute();
			conexion.close();
			return 1;
		}catch(SQLException e)
		{
			e.printStackTrace();
			return 2;
		}
	}
	
	public int agregarOpcion(Opcion opcion)
	{
		try 
		{
			ConexionBd();
			PreparedStatement enunciado = conexion.prepareStatement("insert into opcion(item_id, opc_descripcion, opc_valor) values(?,?,?)");
			enunciado.setInt(1, opcion.getItem_id());
			enunciado.setString(2, opcion.getOpc_descripcion());
			enunciado.setDouble(3, opcion.getOpc_valor());
			enunciado.execute();
			conexion.close();
			return 1;
		}catch(SQLException e)
		{
			e.printStackTrace();
			return 2;
		}
	}
	
	public ArrayList<Empresa> getEmpresas()
	{
		ConexionBd();
		try 
		{
			empresas = new ArrayList<Empresa>();
			resultado = consulta.executeQuery("select * from empresa");
			while(resultado.next())
			{
				empresas.add(new Empresa(resultado.getString("emp_rut"),Integer.parseInt(resultado.getString("com_id")),resultado.getString("emp_nombre"),resultado.getString("emp_direccion"),resultado.getString("emp_telefono"),resultado.getString("emp_celular"),resultado.getString("emp_descripcion"),resultado.getString("emp_email"),resultado.getString("emp_web")));
			}
			CerrarConexionBd();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return empresas;
	}
	public ArrayList<Bitacora> getBitacorasAlumno(int pra_id) {
		
		ConexionBd();
		try 
		{
			bitacoras = new ArrayList<Bitacora>();
			resultado = consulta.executeQuery("select bit_id,to_char( bit_fecha_ini,'dd-MM-yyyy') as inicio ,to_char( bit_fecha_fin,'dd-MM-yyyy') as termino, pra_id, to_char(bit_fecha_edicion,'dd-mm-yyyy') as ultima_edicion from bitacora where pra_id = "+pra_id+"");
			while(resultado.next())
			{
				
				String fecha = resultado.getString("termino");
				Date date = new SimpleDateFormat("dd-MM-yyyy").parse(fecha);
				String fecha2 = resultado.getString("inicio");
				Date date2 = new SimpleDateFormat("dd-MM-yyyy").parse(fecha2);
				
				resultado_dos = consulta_dos.executeQuery("select count(*) as valor from bitacora "
						+ "where bit_id = "+resultado.getInt("bit_id")+" and current_date <= '"+resultado.getString("termino")+"' "
						+ "and current_date >= '"+resultado.getString("inicio")+"' ");
				resultado_dos.next();
				bitacoras.add(new Bitacora(resultado.getInt("bit_id"),date2,date,resultado.getInt("pra_id"),resultado.getString("ultima_edicion"),resultado_dos.getInt("valor")));
			}
			CerrarConexionBd();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bitacoras;
	}
	
	public ArrayList<Bitacora> getBitacorasDescargar(int pra_id,int plan_id) {
		
		ConexionBd();
		try 
		{
			bitacoras = new ArrayList<Bitacora>();
			resultado = consulta.executeQuery("select * from bitacora where pra_id = "+pra_id+"");
			int i=0;
			String tributadas ="";
			
			while(resultado.next())
			{
				i=i+1;
				resultado_dos = consulta_dos.executeQuery("select * from asignatura_bitacora where bit_id = "+resultado.getInt("bit_id")+"");
				while(resultado_dos.next())
				{
					resultado_tres = consulta_tres.executeQuery("select * from asignatura where asi_codigo = "+Integer.parseInt(resultado_dos.getString("asi_codigo"))+" and plan_id= "+plan_id+" ");
					resultado_tres.next();
					tributadas= tributadas+" - "+resultado_tres.getString("asi_nombre");
				}
				bitacoras.add(new Bitacora(i,resultado.getDate("bit_fecha_ini"),resultado.getDate("bit_fecha_fin"),tributadas,resultado.getString("bit_descripcion")));
			}
			CerrarConexionBd();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bitacoras;
	}

	public Bitacora getBitacoraAlumno(int bit_id) {//obtiene una bitacora de un alumno segun su id
		
		ConexionBd();
		try 
		{
			resultado = consulta.executeQuery("SELECT bit_id, to_char( bit_fecha_ini,'dd-MM-yyyy') as inicio, "
											+ "pra_id, to_char( bit_fecha_fin,'dd-MM-yyyy') as termino, "
											+ "to_char(bit_fecha_edicion,'dd-mm-yyyy') as ultima_edicion, "
											+ "bit_hora_edicion, bit_firma, bit_descripcion,"
											+ " bit_n_imagen,bit_n_asignatura "
											+ "FROM bitacora "
											+ "WHERE bit_id = "+bit_id);
			while(resultado.next())
			{
				
				String fecha = resultado.getString("termino");
				Date date = new SimpleDateFormat("dd-MM-yyyy").parse(fecha);
				String fecha2 = resultado.getString("inicio");
				Date date2 = new SimpleDateFormat("dd-MM-yyyy").parse(fecha2);
				//String fecha3 = resultado.getString("ultima_edicion");
				//Date date3 = new SimpleDateFormat("dd-MM-yyyy").parse(fecha3);
				
				bitacora = new Bitacora(bit_id, date2, resultado.getInt("pra_id"), resultado.getString("bit_hora_edicion"), 
										resultado.getString("ultima_edicion"), resultado.getString("bit_descripcion"), resultado.getBoolean("bit_firma"),
										date,resultado.getInt("bit_n_imagen"),resultado.getInt("bit_n_asignatura"));
				}
			CerrarConexionBd();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bitacora;
	}
	
	public int getPracticaId(String emp_rut, String sup_rut, String alu_rut, int asi_codigo, int obr_id, int com_id, int plan)
	{
		ConexionBd();
		int pra_id=0;
		try
		{
			resultado = consulta.executeQuery("select max(pra_id) as ultima from practica "
			+ "where emp_rut ='"+emp_rut+"' and sup_rut='"+sup_rut+"' and alu_rut='"+alu_rut+"' "
			+ "and asi_codigo="+asi_codigo+" and obr_id= "+obr_id+" and com_id="+com_id+" and plan_id="+plan+" ");
			resultado.next();	
			pra_id= resultado.getInt("ultima");
			System.out.println("id practica2 : "+pra_id);
			CerrarConexionBd();
		}catch(SQLException e){
			e.printStackTrace();
		}	
		return pra_id;
	}
	public int getTotalPracticas()
	{
		int total = 0;
		ConexionBd();
		try
		{
			resultado = consulta.executeQuery("select count(*) as total from practica where pra_estado='Pendiente' ");
			resultado.next();	
			total= resultado.getInt("total");
			CerrarConexionBd();
		}catch(SQLException e){
			e.printStackTrace();
		}	
		return total;
	}
	
	public int getLastInstrumentoID()
	{
		ConexionBd();
		try
		{
			resultado = consulta.executeQuery("select MAX(ins_id) as ins_id from instrumento");
			resultado.next();			
			CerrarConexionBd();
			return resultado.getInt("ins_id");
		}catch(SQLException e){
			e.printStackTrace();
		}	
		return 0;
	}
	
	public int getLastItemID()
	{
		ConexionBd();
		try
		{
			resultado = consulta.executeQuery("select MAX(item_id) as item_id from item");
			resultado.next();			
			CerrarConexionBd();
			return resultado.getInt("item_id");
		}catch(SQLException e){
			e.printStackTrace();
		}	
		return 0;
	}
	
	public int getEstadoInforme(int pra_id)
	{
		int total = 0;
		ConexionBd();
		try
		{
			resultado = consulta.executeQuery("select count(*) as total from practica where pra_id = "+pra_id+" and pra_informe is not null");
			resultado.next();	
			total= resultado.getInt("total");
			CerrarConexionBd();
		}catch(SQLException e){
			e.printStackTrace();
		}	
		return total;
	}
	public EvaluacionAcademico getEvaluacion(int pra_id, String aca_rut)
	{
		ConexionBd();
		try
		{
			resultado = consulta.executeQuery("select * from evaluacion_academico where pra_id = "+pra_id+""
					+ " and eva_nombre = 'Profesor Guía' and aca_rut = '"+aca_rut+"' ");
			resultado.next();	
			evaluacion_academico = new EvaluacionAcademico(resultado.getString("aca_rut"),resultado.getInt("pra_id"),
					resultado.getString("eva_fecha"),resultado.getInt("rol_id"),resultado.getString("eva_nombre"),
					resultado.getDouble("eva_nota"),resultado.getString("eva_observacion"),resultado.getString("eva_hora"),
					resultado.getString("eva_estado"));
			CerrarConexionBd();
		}catch(SQLException e){
			e.printStackTrace();
		}	
		return evaluacion_academico;
	}
	public Evaluacion getEvaluacionID(int eva_id)
	{
		ConexionBd();
		try
		{
			resultado = consulta.executeQuery("SELECT  EV.eva_id, EV.eval_clave, EV.pra_id, EV.eva_nombre, trunc( CAST(EV.eva_nota as NUMERIC), 1) AS eva_nota, EV.eva_observacion, "
						+ "EV.eva_fecha, EV.eva_hora, EV.eva_estado "
						+ "FROM evaluacion EV "
						+ "WHERE EV.eva_id = "+eva_id);
			resultado.next();	
			evaluacion = new Evaluacion(resultado.getInt("eva_id"),resultado.getInt("eval_clave"),
					resultado.getInt("pra_id"),resultado.getString("eva_nombre"),resultado.getDouble("eva_nota"),
					resultado.getString("eva_observacion"),resultado.getString("eva_fecha"),
					resultado.getString("eva_hora"),resultado.getString("eva_estado"));
			CerrarConexionBd();
			System.out.println("\nEv ID: "+evaluacion.getEva_id()+" - Pra_id: "+evaluacion.getPra_id()+" - Nota: "+evaluacion.getEva_nota());
		}catch(SQLException e){
			e.printStackTrace();
		}	
		return evaluacion;
	}
	
	public EvaluacionAlumno getEvaluacionAlumnoID(String alu_rut, int pra_id, String eva_fecha, int rol_id)//obtiene una evaluacion por su clave primaria
	{
		ConexionBd();
		try
		{
			resultado = consulta.executeQuery("SELECT alu_rut, pra_id, eva_fecha, rol_id, eva_nombre, eva_nota, eva_observacion, eva_hora, eva_estado "
											+ "FROM evaluacion_alumno "
											+ "WHERE alu_rut = '"+alu_rut+"' AND pra_id = "+pra_id+" AND eva_fecha = '"+eva_fecha+"' AND rol_id = "+rol_id);
			resultado.next();
			evaluacion_alumno = new EvaluacionAlumno(
													resultado.getString("alu_rut"),
													resultado.getInt("pra_id"), 
													resultado.getString("eva_fecha"),
											 		resultado.getInt("rol_id"), 
											 		resultado.getString("eva_nombre"), 
											 		resultado.getDouble("eva_nota"),
											 		resultado.getString("eva_observacion"), 
											 		resultado.getString("eva_hora"), 
											 		resultado.getString("eva_estado")
													);
			CerrarConexionBd();
			//System.out.println("\nEv ID: "+evaluacion.getEva_id()+" - Pra_id: "+evaluacion.getPra_id()+" - Nota: "+evaluacion.getEva_nota());
		}catch(SQLException e){
			e.printStackTrace();
		}	
		return evaluacion_alumno;
	}
	
	public EvaluacionSupervisor getEvaluacionSupervisorID(String sup_rut, int pra_id, String eva_fecha, int rol_id)//obtiene una evaluacion por su clave primaria
	{
		ConexionBd();
		try
		{
			resultado_uno = consulta.executeQuery("SELECT  * "
						+ "FROM evaluacion_supervisor "
						+ "WHERE sup_rut = '"+sup_rut+"' AND pra_id = "+pra_id+" AND eva_fecha = '"+eva_fecha+"' AND rol_id = "+rol_id);
			resultado_uno.next();	
			evaluacion_supervisor = new EvaluacionSupervisor(resultado_uno.getString("sup_rut"), resultado_uno.getInt("pra_id"), resultado_uno.getString("eva_fecha"),
											 		resultado_uno.getInt("rol_id"), resultado_uno.getString("eva_nombre"), resultado_uno.getDouble("eva_nota"),
											 		resultado_uno.getString("eva_observacion"), resultado_uno.getString("eva_hora"), resultado_uno.getString("eva_estado")
													);
			CerrarConexionBd();
			//System.out.println("\nEv ID: "+evaluacion.getEva_id()+" - Pra_id: "+evaluacion.getPra_id()+" - Nota: "+evaluacion.getEva_nota());
		}catch(SQLException e){
			e.printStackTrace();
		}	
		return evaluacion_supervisor;
	}
	
	public EvaluacionAcademico getEvaluacionAcademicoID(String aca_rut, int pra_id, String eva_fecha, int rol_id)//obtiene una evaluacion por su clave primaria
	{
		ConexionBd();
		try
		{
			resultado_uno = consulta.executeQuery("SELECT  * "
						+ "FROM evaluacion_academico "
						+ "WHERE aca_rut = '"+aca_rut+"' AND pra_id = "+pra_id+" AND eva_fecha = '"+eva_fecha+"' AND rol_id = "+rol_id);
			resultado_uno.next();	
			evaluacion_academico = new EvaluacionAcademico(resultado_uno.getString("aca_rut"), resultado_uno.getInt("pra_id"), resultado_uno.getString("eva_fecha"),
											 		resultado_uno.getInt("rol_id"), resultado_uno.getString("eva_nombre"), resultado_uno.getDouble("eva_nota"),
											 		resultado_uno.getString("eva_observacion"), resultado_uno.getString("eva_hora"), resultado_uno.getString("eva_estado")
													);
			CerrarConexionBd();
			//System.out.println("\nEv ID: "+evaluacion.getEva_id()+" - Pra_id: "+evaluacion.getPra_id()+" - Nota: "+evaluacion.getEva_nota());
		}catch(SQLException e){
			e.printStackTrace();
		}	
		return evaluacion_academico;
	}
	
	public int getTotalEstudiantes(String tipo , String rut)//devuelve el numero de estudiantes que tipo de usuario tiene a  cargo
	{
		int total = 0;
		ConexionBd();
		try
		{
			if(tipo == "supervisor")
			{
				resultado = consulta.executeQuery("select count(*) as total from practica "
						+ "where sup_rut = '"+rut+"' and (pra_estado = 'Aceptada' or pra_estado = 'Finalizada') ");
				resultado.next();	
				total= resultado.getInt("total");
				CerrarConexionBd();
			}
			if(tipo == "academico")
			{
				resultado = consulta.executeQuery("select count(*) as total from practica "
						+ "where aca_rut = '"+rut+"' and (pra_estado = 'Aceptada' or pra_estado = 'Finalizada') ");
				resultado.next();	
				total= resultado.getInt("total");
				CerrarConexionBd();
			}
		}catch(SQLException e){
			e.printStackTrace();
		}	
		return total;
	}
	
	public Observacion obtenerObservacion(String obs)
	{
		ConexionBd();
		try
		{
			resultado = consulta.executeQuery("select * from observacion where obs_texto = '"+obs+"' and obs_fecha = current_date ");
			resultado.next();	
		    observacion = new Observacion(resultado.getInt("obs_id"),obs,resultado.getString("obs_fecha"),resultado.getString("obs_hora"));
			CerrarConexionBd();
		}catch(SQLException e){
			e.printStackTrace();
		}	
		return observacion;
	}
	
	public int verificar_usuario(String rut, String tipo, int opcion)
	{
		if(tipo.equals("Alumno"))
		{
			try
			{
				PreparedStatement enunciado = null;
				ConexionBd();
				if(opcion==0)
				{
					enunciado = conexion.prepareStatement("select count(ALU_RUT) AS VALIDO FROM ALUMNO "
							+ "WHERE ALU_RUT = ? ");
				}
				if(opcion==1)
				{
					enunciado = conexion.prepareStatement("select count(ALU_RUT) AS VALIDO FROM ALUMNO "
							+ "WHERE ALU_RUT = ? AND ALU_ESTADO = 'activo'");
				}
				enunciado.setString(1, rut);
				resultado = enunciado.executeQuery();
				resultado.next();
				int valido= resultado.getInt("VALIDO");
				if(valido==1)
				{
					return 1;
				}else{
					return 0;
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
				return 0;
			}
		}
		if(tipo.equals("Administrador"))
		{
			try
			{
				ConexionBd();
				PreparedStatement enunciado = null;
				if(opcion==0)
				{
					enunciado = conexion.prepareStatement("select count(ADM_RUT) AS VALIDO FROM ADMINISTRADOR "
							+ "WHERE ADM_RUT = ?;");
				}
				if(opcion==1)
				{
					enunciado = conexion.prepareStatement("select count(ADM_RUT) AS VALIDO FROM ADMINISTRADOR "
							+ "WHERE ADM_RUT = ? AND ADM_ESTADO = 'activo';");
				}
				
				enunciado.setString(1, rut);
				resultado = enunciado.executeQuery();
				resultado.next();
				int valido= resultado.getInt("VALIDO");
				if(valido==1)
				{
					return 1;
				}else{
					return 0;
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
				return 0;
			}
		}
		if(tipo.equals("Supervisor"))
		{
			try
			{
				ConexionBd();
				PreparedStatement enunciado = null;
				if(opcion==0)
				{
				    enunciado = conexion.prepareStatement("select count(SUP_RUT) AS VALIDO FROM SUPERVISOR "
							+ "WHERE SUP_RUT = ?;");
				}
				if(opcion==1)
				{
					enunciado = conexion.prepareStatement("select count(SUP_RUT) AS VALIDO FROM SUPERVISOR "
							+ "WHERE SUP_RUT = ? AND SUP_ESTADO = 'activo';");
				}
				
				enunciado.setString(1, rut);
				resultado = enunciado.executeQuery();
				resultado.next();
				int valido= resultado.getInt("VALIDO");
				if(valido==1)
				{
					return 1;
				}else{
					return 0;
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
				return 0;
			}
		}
		if(tipo.equals("Academico"))
		{
			try
			{
				ConexionBd();
				PreparedStatement enunciado = null;
				if(opcion==0)
				{
				    enunciado = conexion.prepareStatement("select count(ACA_RUT) AS VALIDO FROM ACADEMICO "
							+ "WHERE ACA_RUT = ?;");
				}
				if(opcion==1)
				{
					enunciado = conexion.prepareStatement("select count(ACA_RUT) AS VALIDO FROM ACADEMICO "
							+ "WHERE ACA_RUT = ? AND ACA_ESTADO = 'activo';");
				}
				
				enunciado.setString(1, rut);
				resultado = enunciado.executeQuery();
				resultado.next();
				int valido= resultado.getInt("VALIDO");
				if(valido==1)
				{
					return 1;
				}else{
					return 0;
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
				return 0;
			}
		}
		return 0;
	}
	public int verificar_usuario_con_parametros(Connection conexion_alumno,ResultSet resultado_alumno,String rut, String tipo)
	{
		if(tipo.equals("Alumno"))
		{
			try
			{
				PreparedStatement enunciado = null;
				
				enunciado = conexion_alumno.prepareStatement("select count(ALU_RUT) AS VALIDO FROM ALUMNO "
							+ "WHERE ALU_RUT = ? ");
				enunciado.setString(1, rut);
				resultado_alumno = enunciado.executeQuery();
				resultado_alumno.next();
				int valido= resultado_alumno.getInt("VALIDO");
				if(valido==1)
				{
					return 1;
				}else{
					return 0;
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
				return 0;
			}
		}
		return 0;
	}
	public String getNombresAdmin(String rut)
	{
		String nombres ="";
		ConexionBd();
		try
		{
			resultado = consulta.executeQuery("select adm_nombres ,adm_apellido_p , adm_apellido_m from administrador where adm_rut = '"+rut+"' ");
			resultado.next();	
			nombres= ""+resultado.getString("adm_nombres")+" "+resultado.getString("adm_apellido_p")+" "+resultado.getString("adm_apellido_m");
			CerrarConexionBd();
		}catch(SQLException e){
			e.printStackTrace();
		}	
		return nombres;
	}
	
	public String getNombresSupervisor(String rut)
	{
		String nombres ="";
		ConexionBd();
		try
		{
			resultado = consulta.executeQuery("select sup_nombres ,sup_apellido_p , sup_apellido_m from supervisor where sup_rut = '"+rut+"' ");
			resultado.next();	
			nombres= ""+resultado.getString("sup_nombres")+" "+resultado.getString("sup_apellido_p")+" "+resultado.getString("sup_apellido_m");
			CerrarConexionBd();
		}catch(SQLException e){
			e.printStackTrace();
		}	
		return nombres;
	}
	public String getClave(String rut,String tipo) throws SQLException
	{
		String clave ="";
		ConexionBd();
		if(tipo.equals("Academico"))
		{
			try
			{
				resultado = consulta.executeQuery("select aca_password from academico where aca_rut = '"+rut+"' ");
				resultado.next();	
				clave= ""+resultado.getString("aca_password");
			}catch(SQLException e){
				e.printStackTrace();
			}	
		}
		if(tipo.equals("Administrador"))
		{
			try
			{
				resultado = consulta.executeQuery("select adm_password from administrador where adm_rut = '"+rut+"' ");
				resultado.next();	
				clave= ""+resultado.getString("adm_password");
			}catch(SQLException e){
				e.printStackTrace();
			}
		}
		if(tipo.equals("Alumno"))
		{
			try
			{
				resultado = consulta.executeQuery("select alu_password from alumno where alu_rut = '"+rut+"' ");
				resultado.next();	
				clave= ""+resultado.getString("alu_password");
			}catch(SQLException e){
				e.printStackTrace();
			}
		}
		if(tipo.equals("Supervisor"))
		{
			try
			{
				resultado = consulta.executeQuery("select sup_password from supervisor where sup_rut = '"+rut+"' ");
				resultado.next();	
				clave= ""+resultado.getString("sup_password");
			}catch(SQLException e){
				e.printStackTrace();
			}	
		}
		CerrarConexionBd();
		return clave;
	}
	public Alumno getDatosAlumno(String rut)
	{
		ConexionBd();
		try
		{
			System.out.println("Rut Alumno "+rut);
			resultado = consulta.executeQuery("select alu_nombres ,alu_apellido_p , alu_apellido_m, alu_email, alu_celular, alu_telefono from alumno where alu_rut = '"+rut+"' ");
			resultado.next();	
			alumno = new Alumno(rut,resultado.getString("alu_nombres"),resultado.getString("alu_apellido_p"),resultado.getString("alu_apellido_m"),resultado.getString("alu_email"),resultado.getString("alu_celular"),resultado.getString("alu_telefono"));
		CerrarConexionBd();
		}catch(SQLException e){
			e.printStackTrace();
		}	
		return alumno;
	}
	
	public int getImagen(String url)
	{
		int id = 0;
		ConexionBd();
		try
		{
			System.out.println("url "+url);
			resultado = consulta.executeQuery("select img_id,img_fecha,img_hora from imagen where img_url = '"+url+"' ");
			resultado.next();
			id = resultado.getInt("img_id");
			CerrarConexionBd();
		}catch(SQLException e){
			e.printStackTrace();
		}	
		return id;
	}
	public Empresa getEmpresa(String emp_rut)
	{
		ConexionBd();
		try
		{
			resultado = consulta.executeQuery("select * from empresa where emp_rut = '"+emp_rut+"' ");
			resultado.next();	
			empresa = new Empresa(emp_rut,resultado.getInt("com_id"),resultado.getString("emp_nombre"),
					resultado.getString("emp_direccion"),resultado.getString("emp_telefono"),resultado.getString("emp_celular"),
					resultado.getString("emp_descripcion"),resultado.getString("emp_email"),resultado.getString("emp_web"));
			CerrarConexionBd();
		}catch(SQLException e){
			e.printStackTrace();
		}	
		return empresa;
	}
	public Academico getAcademico(String aca_rut)
	{
		ConexionBd();
		try
		{
			//System.out.println();
			resultado = consulta.executeQuery("select * from academico where aca_rut = '"+aca_rut+"' ");
			resultado.next();	
			academico = new Academico(aca_rut,resultado.getInt("dep_id"),resultado.getString("aca_nombres"),
					resultado.getString("aca_apellido_p"),resultado.getString("aca_apellido_m"),resultado.getString("aca_email"),
					resultado.getString("aca_telefono"),resultado.getString("aca_celular"),resultado.getString("aca_estado"));
			CerrarConexionBd();
		}catch(SQLException e){
			e.printStackTrace();
		}	
		return academico;
	}
	public Administrador getAdministrador(String adm_rut)
	{
		ConexionBd();
		try
		{
			resultado = consulta.executeQuery("select * from administrador where adm_rut = '"+adm_rut+"' ");
			resultado.next();	
			administrador = new Administrador(adm_rut,resultado.getString("adm_nombres"),resultado.getString("adm_apellido_p"),
					resultado.getString("adm_apellido_m"),resultado.getString("adm_email"),resultado.getString("adm_telefono"),
					resultado.getString("adm_celular"),resultado.getString("adm_cargo"),resultado.getString("adm_estado"));
			CerrarConexionBd();
		}catch(SQLException e){
			e.printStackTrace();
		}	
		return administrador;
	}
	public Comuna getComuna(int com_id)
	{
		ConexionBd();
		try
		{
			resultado = consulta.executeQuery("select * from comuna where com_id = "+com_id+" ");
			resultado.next();	
			comuna = new Comuna(resultado.getInt("com_id"),resultado.getInt("pro_id"), resultado.getString("com_nombre"));
			CerrarConexionBd();
		}catch(SQLException e){
			e.printStackTrace();
		}	
		return comuna;
	}
	public Provincia getProvincia(int pro_id)
	{
		ConexionBd();
		try
		{
			resultado = consulta.executeQuery("select * from provincia where pro_id = "+pro_id+" ");
			resultado.next();	
			provincia = new Provincia(pro_id,resultado.getInt("reg_id"),resultado.getString("pro_nombre"));
			CerrarConexionBd();
		}catch(SQLException e){
			e.printStackTrace();
		}	
		return provincia;
	}
	public Region getRegion(int reg_id)
	{
		ConexionBd();
		try
		{
			resultado = consulta.executeQuery("select * from region where reg_id = "+reg_id+" ");
			resultado.next();	
			region = new Region(reg_id,resultado.getInt("pais_id"),resultado.getString("reg_nombre"));
			CerrarConexionBd();
		}catch(SQLException e){
			e.printStackTrace();
		}	
		return region;
	}
	public Departamento getDepartamento(int dep_id)
	{
		ConexionBd();
		try
		{
			resultado = consulta.executeQuery("select * from departamento where dep_id = '"+dep_id+"' ");
			resultado.next();	
			departamento = new Departamento(dep_id,resultado.getString("dep_nombre"));
			CerrarConexionBd();
		}catch(SQLException e){
			e.printStackTrace();
		}	
		return departamento;
	}
	
	public int getInscripcionID(Connection conexion,InscripcionAsignatura inscripcion)//PK alu_rut, asi_codigo, plan_id, ins_fecha 
	{
		int valor = 0;
		PreparedStatement enunciado = null;
		try
		{
			enunciado = conexion.prepareStatement("select count(*) AS cuenta "
					+ "FROM inscripcion_asignatura "
					+ "WHERE alu_rut = ? AND asi_codigo = ? AND plan_id = ? AND ins_fecha = CAST(? AS DATE)");
			
			enunciado.setString(1, inscripcion.getAlu_rut());
			enunciado.setInt(2, inscripcion.getAsi_codigo());
			enunciado.setInt(3, inscripcion.getPlan_id());
			enunciado.setString(4, inscripcion.getIns_fecha());
			resultado = enunciado.executeQuery();
			resultado.next();
			if(resultado.getInt("cuenta") != 0){
				valor = 1;
			}
			else{
				valor = 0;
			}
		}catch(SQLException e){
			e.printStackTrace();
		}	
		return valor;
	}
	
	public InscripcionAsignatura getInscripcion(String rut) 
	{
		ConexionBd();
		try
		{
			resultado = consulta.executeQuery("select asi_codigo,plan_id,ins_fecha from inscripcion_asignatura "
					+ "where alu_rut = '"+rut+"' and  ins_fecha = (SELECT  max(i2.ins_fecha) FROM inscripcion_asignatura i2 "
					+ "where alu_rut = '"+rut+"')");
			resultado.next();
			inscripcion = new InscripcionAsignatura(rut,resultado.getInt("asi_codigo"),resultado.getInt("plan_id"),resultado.getString("ins_fecha"));
			CerrarConexionBd();
		}catch(SQLException e){
			e.printStackTrace();
		}	
		return inscripcion;
	}
	
	public ArrayList<InscripcionAsignatura> getInscripciones(String rut) 
	{
		ConexionBd();
		inscripciones = new ArrayList<InscripcionAsignatura>();
		try
		{
			resultado = consulta.executeQuery("SELECT * "
											+ "FROM inscripcion_asignatura "
											+ "WHERE alu_rut ='"+rut+"' ");
			
			while(resultado.next()){
				inscripciones.add(new InscripcionAsignatura(
															rut,resultado.getInt("asi_codigo"),resultado.getInt("plan_id"),
															resultado.getString("ins_fecha"), resultado.getInt("ins_semestre"), resultado.getDouble("ins_nota"),
															resultado.getString("ins_estado")
															)
								);
				
			}			
			CerrarConexionBd();
		}catch(SQLException e){
			e.printStackTrace();
		}	
		return inscripciones;
	}
	
	public ArrayList<InscripcionAsignatura> getInscripcionesUBB(String agnio, String periodo)//obtiene todas las inscripciones de gestiones operativas o profesionales de la Universidad
	{
		ConexionBd2();
		inscripciones = new ArrayList<InscripcionAsignatura>();
		try
		{
			resultado = consulta.executeQuery("execute sp_web_web_practica_eic_recupera_alumno_inscripcion_sr '"+agnio+"', '"+periodo+"';");
			while(resultado.next()){
				String rut_incompleto = resultado.getString("ALU_RUT");
				String dv = Herramientas.calcularDV(rut_incompleto);
				String rut_completo = Herramientas.Formatear(rut_incompleto+dv);
				inscripciones.add(new InscripcionAsignatura(rut_completo,resultado.getInt("ASI_CODIGO"),resultado.getInt("PLAN_ID"),resultado.getString("INS_FECHA"),resultado.getInt("INS_SEMESTRE"),resultado.getInt("INS_AGNIO"),resultado.getDouble("INS_NOTA"), resultado.getString("INS_ESTADO") ));
			}
			CerrarConexionBd2();
		}catch(SQLException e){
			e.printStackTrace();
		}	
		return inscripciones;
	}
	
	public Asignatura getAsignatura(int codigo, int plan) {
		
		ConexionBd();
		try
		{
			resultado = consulta.executeQuery("select * from asignatura where asi_codigo = "+codigo+" and plan_id= "+plan+" ");
			resultado.next();
			asignatura = new Asignatura(codigo, plan,resultado.getInt("asi_tp_id"),resultado.getString("asi_nombre"));
			CerrarConexionBd();
		}catch(SQLException e){
			e.printStackTrace();
		}	
		return asignatura;
	}
	public Plan_estudio getPlan(int plan_id) {
		
		ConexionBd();
		try
		{
			resultado = consulta.executeQuery("select * from plan_estudio where plan_id = "+plan_id+" ");
			resultado.next();
			plan = new Plan_estudio(resultado.getInt("plan_id"),resultado.getInt("car_codigo"),
					      resultado.getString("plan_nombre"),resultado.getString("plan_fecha"),
					      resultado.getString("plan_estado"));
			CerrarConexionBd();
		}catch(SQLException e){
			e.printStackTrace();
		}	
		return plan;
	}
	public Especialidad getEspecialidad(int esp_id) // se obtiene los datos de una especialidad especifica con una id de especialidad
	{	
		ConexionBd();
		try
		{
			resultado = consulta.executeQuery("select * from especialidad where esp_id = "+esp_id+" ");
			resultado.next();
			especialidad = new Especialidad(esp_id,resultado.getString("esp_nombre"));
			CerrarConexionBd();
		}catch(SQLException e){
			e.printStackTrace();
		}	
		return especialidad;
	}
	public Area getArea(int area_id) // se obtiene los datos de un area especifica con una id de area
	{	
		ConexionBd();
		try
		{
			resultado = consulta.executeQuery("select * from area where area_id = "+area_id+" ");
			resultado.next();
			area = new Area(area_id,resultado.getString("area_nombre"));
			CerrarConexionBd();
		}catch(SQLException e){
			e.printStackTrace();
		}	
		return area;
	}
	public Funcion getFuncion(int funcion_id) // se obtiene los datos de uns funcion especifica 
	{	
		ConexionBd();
		try
		{
			resultado = consulta.executeQuery("select * from funcion where fun_id = "+funcion_id+" ");
			resultado.next();
			funcion = new Funcion(funcion_id,resultado.getString("fun_nombre"));
			CerrarConexionBd();
		}catch(SQLException e){
			e.printStackTrace();
		}	
		return funcion;
	}
	public int getPlanEstudioVigente() // obtiene ultimo plan de estudio vigente
	{	
		ConexionBd();
		int plan_id = 0;
		try
		{
			resultado = consulta.executeQuery("select plan_id from plan_estudio where plan_vigente = true ");
			resultado.next();
			plan_id = resultado.getInt("plan_id");
			CerrarConexionBd();
		}catch(SQLException e){
			e.printStackTrace();
		}	
		return plan_id;
	}
	public ArrayList<Asignatura> getInstrumentos()//obtiene todos los instrumentos a partir de las asignaturas de tipo gestion(prácticas)
	{
		ConexionBd();
		Statement query = null;
		try {
			query = conexion.createStatement();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}//permitirá hacer una consulta un vez se hayan obtenido las asignaturas de gestion operativas y profesional
		ResultSet resultado_instrumentos;//almacenará los resultados de instrumentos por gestion
		try 
		{
			instrumentos_asignatura = new ArrayList<Asignatura>();
			instrumentos = new ArrayList<Instrumento>();
			resultado = consulta.executeQuery("SELECT * FROM asignatura WHERE asi_tp_id = 4 OR asi_tp_id = 5");
			while(resultado.next())
			{	
				instrumentos = new ArrayList<Instrumento>();
				resultado_instrumentos = query.executeQuery("SELECT I.ins_id, I.esc_id, I.rol_id, I.ins_nombre, I.ins_num_item, I.ins_max_puntaje, I.ins_descripcion, I.ins_fecha_ini, I.ins_fecha_fin, I.ins_estado "
														  + "FROM instrumento I, rol_gestion R "
														  + "WHERE I.rol_id = R.rol_id AND R.asi_codigo = "+resultado.getString("asi_codigo")+" AND R.plan_id = "+resultado.getString("plan_id")+" "
														  + "ORDER BY I.rol_id ASC");
				while(resultado_instrumentos.next()){
					instrumentos.add(new Instrumento(resultado_instrumentos.getInt("ins_id"),resultado_instrumentos.getInt("esc_id"), resultado_instrumentos.getInt("rol_id"),resultado_instrumentos.getString("ins_nombre"),
													 resultado_instrumentos.getInt("ins_num_item"),resultado_instrumentos.getDouble("ins_max_puntaje"),resultado_instrumentos.getString("ins_nombre"),
													 resultado_instrumentos.getString("ins_fecha_ini"),resultado_instrumentos.getString("ins_fecha_fin"),resultado_instrumentos.getString("ins_estado")
													 )
									);
													/*int ins_id, int esc_id, int rol_id, String ins_nombre,
													int ins_num_item, double ins_max_puntaje, String ins_descripcion,
													String ins_fecha_ini, String ins_fecha_fin, String ins_estado,
													ArrayList<Item> items*/
				}
				
				instrumentos_asignatura.add(new Asignatura(resultado.getInt("asi_codigo"), resultado.getInt("plan_id"), resultado.getInt("asi_tp_id"),resultado.getString("asi_nombre"), instrumentos
														  )
										   
										   );
			}
			CerrarConexionBd();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return instrumentos_asignatura;
	}
	
	public Instrumento getInstrumento(String ins_id)//obtiene solo un instrumento por su id
	{
		ConexionBd();
		items = new ArrayList<Item>();
		try 
		{
			resultado = consulta.executeQuery("SELECT ins_id,esc_id,rol_id,ins_nombre,ins_num_item,ins_max_puntaje FROM instrumento WHERE ins_id = "+ins_id);
			while(resultado.next())
			{				
				items = new ArrayList<Item>();
				items = this.getItem(ins_id);
				instrumento = new Instrumento(resultado.getInt("ins_id"),resultado.getInt("esc_id"),
												 resultado.getInt("rol_id"),resultado.getString("ins_nombre"),
												 resultado.getInt("ins_num_item"),resultado.getDouble("ins_max_puntaje"),
												 items
												 );
												/*int ins_id, int esc_id, int rol_id, String ins_nombre,
												int ins_num_item, double ins_max_puntaje, String ins_descripcion,
												String ins_fecha_ini, String ins_fecha_fin, String ins_estado,
												ArrayList<Item> items*/
								
			}
			CerrarConexionBd();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return instrumento;
	}
	
	public Instrumento getInstrumento(int rol_id, String eva_fecha)//obtiene solo un instrumento por el rol y por la fecha en que está vigente la rúbrica de evaluacion
	{
		ConexionBd();
		items = new ArrayList<Item>();
		try 
		{
			resultado_tres = consulta_dos.executeQuery("SELECT COUNT(*) AS cuenta "
											+ "FROM instrumento "
											+ "WHERE rol_id = "+rol_id+" AND ins_fecha_ini <= '"+eva_fecha+"' AND ins_fecha_fin > '"+eva_fecha+"'");
			resultado_tres.next();
			if(resultado_tres.getInt("cuenta") == 1){//significa que es una rubrica antigua con la que se evaluó
				resultado = consulta.executeQuery("SELECT ins_id,esc_id,rol_id,ins_nombre,ins_num_item,ins_max_puntaje "
												+ "FROM instrumento "
												+ "WHERE rol_id = "+rol_id+" AND ins_fecha_ini <= '"+eva_fecha+"' AND ins_fecha_fin > '"+eva_fecha+"'");
			}
			else{//sino significa que la evaluación fue realizada con la rubrica actualmente activa
				resultado = consulta.executeQuery("SELECT ins_id,esc_id,rol_id,ins_nombre,ins_num_item,ins_max_puntaje "
												+ "FROM instrumento "
												+ "WHERE rol_id = "+rol_id+" AND ins_fecha_ini <= '"+eva_fecha+"' AND ins_estado = 'activo' ");
			}
			while(resultado.next())
			{				
				items = new ArrayList<Item>();
				items = this.getItem(resultado.getString("ins_id"));
				instrumento = new Instrumento(resultado.getInt("ins_id"),resultado.getInt("esc_id"),
											 resultado.getInt("rol_id"),resultado.getString("ins_nombre"),
											 resultado.getInt("ins_num_item"),resultado.getDouble("ins_max_puntaje"),
											 items
												 );
												/*int ins_id, int esc_id, int rol_id, String ins_nombre,
												int ins_num_item, double ins_max_puntaje, String ins_descripcion,
												String ins_fecha_ini, String ins_fecha_fin, String ins_estado,
												ArrayList<Item> items*/
			}
			CerrarConexionBd();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return instrumento;
	}
	
	public Instrumento getInstrumento(int rol_id)//obtiene solo un instrumento por la identificacion de un rol_gestion o evaluador
	{
		ConexionBd();
		items = new ArrayList<Item>();
		try 
		{
			resultado = consulta.executeQuery("SELECT ins_id,esc_id,rol_id,ins_nombre,ins_num_item,ins_max_puntaje "
											+ "FROM instrumento "
											+ "WHERE rol_id = "+rol_id+"and ins_estado = 'activo'");
			while(resultado.next())
			{				
				items = new ArrayList<Item>();
				items = this.getItem(resultado.getString("ins_id"));
				instrumento = new Instrumento(resultado.getInt("ins_id"),resultado.getInt("esc_id"),
												 resultado.getInt("rol_id"),resultado.getString("ins_nombre"),
												 resultado.getInt("ins_num_item"),resultado.getDouble("ins_max_puntaje"),
												 items
												 );
												/*int ins_id, int esc_id, int rol_id, String ins_nombre,
												int ins_num_item, double ins_max_puntaje, String ins_descripcion,
												String ins_fecha_ini, String ins_fecha_fin, String ins_estado,
												ArrayList<Item> items*/
								
			}
			CerrarConexionBd();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return instrumento;
	}
	
	public ArrayList<Instrumento> getInstrumentos(int plan_id)
	//es una copia del metodo anterior pero trae todos los instrumentos activos de un plan de estudio
	//
	{
		String id;
		instrumentos = new ArrayList<Instrumento>();
		ConexionBd();
		items = new ArrayList<Item>();
		try 
		{
			resultado = consulta.executeQuery("SELECT I.ins_id, I.esc_id, I.rol_id, I.ins_nombre, I.ins_num_item, I.ins_max_puntaje "
											+ "FROM instrumento I, rol_gestion R "
											+ "WHERE I.ins_estado = 'activo' AND I.rol_id = R.rol_id AND R.plan_id = "+plan_id+" ");
			while(resultado.next())
			{	
				id = resultado.getString("ins_id");
				items = new ArrayList<Item>();
				items = this.getItem(id);
				instrumentos.add( new Instrumento(Integer.parseInt(id),resultado.getInt("esc_id"),
												 resultado.getInt("rol_id"),resultado.getString("ins_nombre"),
												 resultado.getInt("ins_num_item"),resultado.getDouble("ins_max_puntaje"),
												 items
												 )
								);
												/*int ins_id, int esc_id, int rol_id, String ins_nombre,
												int ins_num_item, double ins_max_puntaje, String ins_descripcion,
												String ins_fecha_ini, String ins_fecha_fin, String ins_estado,
												ArrayList<Item> items*/
								
			}
			CerrarConexionBd();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return instrumentos;
	}
	
	public ArrayList<Instrumento> getAllInstrumentos()
	//es una copia del metodo anterior pero trae todos los instrumentos
	//
	{
		String id;
		instrumentos = new ArrayList<Instrumento>();
		ConexionBd();
		items = new ArrayList<Item>();
		try 
		{
			resultado = consulta.executeQuery("SELECT * "
											+ "FROM instrumento I");
			while(resultado.next())
			{	
				id = resultado.getString("ins_id");
				items = new ArrayList<Item>();
				items = this.getItem(id);
				instrumentos.add( new Instrumento(Integer.parseInt(id),resultado.getInt("esc_id"), resultado.getInt("rol_id"),resultado.getString("ins_nombre"),
												 resultado.getInt("ins_num_item"),resultado.getDouble("ins_max_puntaje"), resultado.getString("ins_descripcion"),
												 resultado.getString("ins_fecha_ini"),resultado.getString("ins_fecha_fin"),resultado.getString("ins_estado"),
												 items
												 )
								);
			}
			CerrarConexionBd();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return instrumentos;
	}
	
	public ArrayList<Item> getItem(String ins_id)//obtiene todos los items pertenecientes a un instrumento
	{
		ConexionBd();
		items = new ArrayList<Item>();
		opciones = new ArrayList<Opcion>();
		try 
		{
			//resultado_uno = consulta.executeQuery("SELECT I.item_id, I.ins_id, I.esc_id, I.cat_id, I.item_nombre FROM item I, categoria C WHERE I.ins_id = "+ins_id+" ORDER BY I.cat_id ASC");
			resultado_uno = consulta.executeQuery("SELECT * FROM item WHERE ins_id = "+ins_id+" ORDER BY cat_id ASC");
			while(resultado_uno.next())
			{				
				opciones = new ArrayList<Opcion>();
				opciones = this.getOpcion(resultado_uno.getString("item_id"));
				items.add(new Item(resultado_uno.getInt("item_id"),resultado_uno.getInt("ins_id"),
								   resultado_uno.getInt("esc_id"),resultado_uno.getInt("cat_id"),
								   resultado_uno.getString("item_nombre"), opciones
									)
							);
			}
			CerrarConexionBd();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return items;
	}
	
	public ArrayList<Opcion> getOpcion(String item_id)//obtiene todos los items segun un instrumento
	{
		ConexionBd();
		opciones = new ArrayList<Opcion>();
		try 
		{
			resultado_dos = consulta.executeQuery("SELECT opc_id, item_id, opc_descripcion, opc_valor FROM opcion WHERE item_id = "+item_id+" ORDER BY opc_valor ASC");
			//resultado_dos = consulta.executeQuery("SELECT * FROM opcion WHERE item_id = "+item_id+" ORDER BY opc_valor ASC");
			while(resultado_dos.next())
			{				
				opciones.add(new Opcion(resultado_dos.getInt("opc_id"),resultado_dos.getInt("item_id"),
							 			resultado_dos.getString("opc_descripcion"),resultado_dos.getDouble("opc_valor")
										)
								);
			}
			CerrarConexionBd();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return opciones;
	}
	
	public Escala getEscalaByID(int esc_id)//obtiene la escala apartir de un ID de ESCALA esc_id
	{
		ConexionBd();
		try 
		{
			resultado = consulta.executeQuery("SELECT E.esc_id, E.esc_nombre, E.esc_descripcion, E.esc_num_opciones, E.esc_min_valor, E.esc_max_valor, E.esc_intervalo "
											+ "FROM escala E, instrumento I "
											+ "WHERE E.esc_id = "+esc_id);
			resultado.next();						
			valores = this.getValores(resultado.getInt("esc_id"));
			escala = new Escala(resultado.getInt("esc_id"),resultado.getString("esc_nombre"),
								 resultado.getString("esc_descripcion"),resultado.getInt("esc_num_opciones"),
								 resultado.getDouble("esc_min_valor"),resultado.getDouble("esc_max_valor"),
								 resultado.getInt("esc_intervalo"), valores
								 );
								
			
			CerrarConexionBd();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return escala;
	}
	
	public Escala getEscala(int ins_id)//obtiene la escala apartir de un instrumento
	{
		ConexionBd();
		try 
		{
			resultado = consulta.executeQuery("SELECT E.esc_id, E.esc_nombre, E.esc_descripcion, E.esc_num_opciones, E.esc_min_valor, E.esc_max_valor, E.esc_intervalo "
											+ "FROM escala E, instrumento I "
											+ "WHERE E.esc_id = I.esc_id AND I.ins_id = "+ins_id);
			resultado.next();						
			valores = this.getValores(resultado.getInt("esc_id"));
			escala = new Escala(resultado.getInt("esc_id"),resultado.getString("esc_nombre"),
								 resultado.getString("esc_descripcion"),resultado.getInt("esc_num_opciones"),
								 resultado.getDouble("esc_min_valor"),resultado.getDouble("esc_max_valor"),
								 resultado.getInt("esc_intervalo"), valores
								 );
								
			
			CerrarConexionBd();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return escala;
	}
	
	public ArrayList<Escala> getEscala()//obtiene la escalas registradas en la BD
	{
		/**
	     * Metodo que permite obtener todas las escalas registradas en la Base de Datos
	     * @param Sin parámetros.
	     * @author Camilo
	     */
		escalas = new ArrayList<Escala>();
		ConexionBd();
		try 
		{
			resultado = consulta.executeQuery("SELECT E.esc_id, E.esc_nombre, E.esc_descripcion, E.esc_num_opciones, E.esc_min_valor, E.esc_max_valor, E.esc_exigencia, E.esc_intervalo "
											+ "FROM escala E ");
			while(resultado.next()){
				valores = this.getValores(resultado.getInt("esc_id"));
				escalas.add(new Escala(resultado.getInt("esc_id"),resultado.getString("esc_nombre"),
							 resultado.getString("esc_descripcion"),resultado.getInt("esc_num_opciones"),
							 resultado.getDouble("esc_min_valor"),resultado.getDouble("esc_max_valor"),
							 resultado.getInt("esc_intervalo"), resultado.getInt("esc_exigencia"), valores
							 )						
							);
			}			
			CerrarConexionBd();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return escalas;
		/**
	     * 
	     * @return El número de ítems (números aleatorios) de que consta la serie
	     */
	}
	
	public ArrayList<Valores> getValores(int esc_id)//obtiene todos los valores correspondientes a una escala
	{
		ConexionBd();
		valores = new ArrayList<Valores>();
		try 
		{
			resultado_uno = consulta.executeQuery("SELECT val_id, esc_id, val_nombre, trunc(CAST(val_valor as NUMERIC)) AS val_valor "
											+ "FROM valores "
											+ "WHERE esc_id = "+esc_id);
			while(resultado_uno.next()){						
			//System.out.println("\n Val_valor: "+resultado_uno.getDouble("val_valor"));
			valores.add(new Valores(resultado_uno.getInt("val_id"),resultado_uno.getInt("esc_id"),
									 resultado_uno.getString("val_nombre"),resultado_uno.getInt("val_valor")
								 )
						);
			}
								
			
			//CerrarConexionBd();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return valores;
	}
	
	public ArrayList<Categoria> getCategorias()//obtiene todos las categorias de items
	{
		ConexionBd();
		categorias = new ArrayList<Categoria>();
		try 
		{
			resultado_uno = consulta.executeQuery("SELECT * "
												+ "FROM categoria");
			while(resultado_uno.next()){						
			
			categorias.add(new Categoria(resultado_uno.getInt("cat_id"), resultado_uno.getString("cat_nombre"),
										 resultado_uno.getString("cat_descripcion")
								 		)
						);
			}
								
			
			CerrarConexionBd();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return categorias;
	}
	
	
	public Evaluadores getEvaluador(int eval_clave)//obtiene el evaluador por eval_clave (sirve para ver el detalle de la evaluacion)
	{
		ConexionBd();
		try 
		{
			resultado_uno = consulta.executeQuery("SELECT  E.eval_clave, E.asi_codigo, E.plan_id, E.rol_id, E.aca_rut, E.sup_rut, "
												+ "E.alu_rut, E.eval_fecha, E.eval_estado "
												+ "FROM evaluadores E "
												+ "WHERE eval_clave = "+eval_clave);
			resultado_uno.next();						
			
			evaluador = new Evaluadores(resultado_uno.getInt("eval_clave"), resultado_uno.getInt("asi_codigo"), resultado_uno.getInt("plan_id"),
									 		resultado_uno.getInt("rol_id"), resultado_uno.getString("aca_rut"), resultado_uno.getString("sup_rut"),
									 		 resultado_uno.getString("alu_rut"), resultado_uno.getString("eval_fecha"), resultado_uno.getString("eval_estado")
								 );

			CerrarConexionBd();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return evaluador;
	}
	
	public ArrayList<Evaluadores> getEvaluadores(String rut)//obtiene todos los evaluadores correspondiente al usuario que se ha autentificado (academico)
	{
		ConexionBd();
		evaluadores = new ArrayList<Evaluadores>();
		try 
		{
			resultado_uno = consulta.executeQuery("SELECT  E.eval_clave, E.asi_codigo, E.plan_id, E.rol_id, E.aca_rut, E.sup_rut, "
												+ "E.alu_rut, E.eval_fecha, E.eval_estado "
												+ "FROM evaluadores E "
												+ "WHERE aca_rut = '"+rut+"' AND E.eval_estado = 'Activo'");
			while(resultado_uno.next()){						
			
			evaluadores.add(new Evaluadores(resultado_uno.getInt("eval_clave"), resultado_uno.getInt("asi_codigo"), resultado_uno.getInt("plan_id"),
									 		resultado_uno.getInt("rol_id"), resultado_uno.getString("aca_rut"), resultado_uno.getString("sup_rut"),
									 		 resultado_uno.getString("alu_rut"), resultado_uno.getString("eval_fecha"), resultado_uno.getString("eval_estado")
								 )
						);
			}
			CerrarConexionBd();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return evaluadores;
	}
	
	public ArrayList<EvaluacionAcademico> getEvaluadoresAcademico(String rut)//obtiene todos los evaluadores correspondiente al supervisor que se ha autentificado
	{
		ConexionBd();
		evaluaciones_academico = new ArrayList<EvaluacionAcademico>();
		try 
		{
			resultado = consulta.executeQuery("SELECT * "
												+ "FROM evaluacion_academico E "
												+ "WHERE aca_rut = '"+rut+"'");
			while(resultado.next()){						
			
			evaluaciones_academico.add(new EvaluacionAcademico(
													resultado.getString("aca_rut"),
													resultado.getInt("pra_id"), 
													resultado.getString("eva_fecha"),
											 		resultado.getInt("rol_id"), 
											 		resultado.getString("eva_nombre"), 
											 		resultado.getDouble("eva_nota"),
											 		resultado.getString("eva_observacion"), 
											 		resultado.getString("eva_hora"), 
											 		resultado.getString("eva_estado")
													)
							);
			}				
			CerrarConexionBd();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return evaluaciones_academico;
	}
	
	public ArrayList<EvaluacionSupervisor> getEvaluadoresSupervisor(String rut)//obtiene todos los evaluadores correspondiente al supervisor que se ha autentificado
	{
		ConexionBd();
		evaluaciones_supervisor = new ArrayList<EvaluacionSupervisor>();
		try 
		{
			resultado = consulta.executeQuery("SELECT * "
												+ "FROM evaluacion_supervisor E "
												+ "WHERE sup_rut = '"+rut+"'");
			while(resultado.next()){						
			
			evaluaciones_supervisor.add(new EvaluacionSupervisor(
													resultado.getString("sup_rut"),
													resultado.getInt("pra_id"), 
													resultado.getString("eva_fecha"),
											 		resultado.getInt("rol_id"), 
											 		resultado.getString("eva_nombre"), 
											 		resultado.getDouble("eva_nota"),
											 		resultado.getString("eva_observacion"), 
											 		resultado.getString("eva_hora"), 
											 		resultado.getString("eva_estado")
													)
							);
			}
								
			
			CerrarConexionBd();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return evaluaciones_supervisor;
	}
	public ArrayList<EvaluacionAlumno> getEvaluadoresAlumno(String rut)//obtiene todos los evaluadores correspondiente al alumno que se ha autentificado
	{
		ConexionBd();
		evaluaciones_alumno = new ArrayList<EvaluacionAlumno>();
		try 
		{
			resultado_uno = consulta.executeQuery("SELECT * "
												+ "FROM evaluacion_alumno E "
												+ "WHERE E.alu_rut = '"+rut+"' AND E.eva_estado = 'Sin Evaluar'");
			while(resultado_uno.next()){						
			
				evaluaciones_alumno.add(new EvaluacionAlumno(
														resultado_uno.getString("alu_rut"), resultado_uno.getInt("pra_id"), resultado_uno.getString("eva_fecha"),
												 		resultado_uno.getInt("rol_id"), resultado_uno.getString("eva_nombre"), resultado_uno.getDouble("eva_nota"),
												 		resultado_uno.getString("eva_observacion"), resultado_uno.getString("eva_hora"), resultado_uno.getString("eva_estado")
								 						)
										);
			}
			CerrarConexionBd();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return evaluaciones_alumno;
	}
	
	public ArrayList<Asignatura> getAsignaturasBitacora(int plan_id, int bit_id)//obtiene las asignaturas que se han tributado  a cierta bitacora
	{
		ConexionBd();
		gestiones = new ArrayList<Asignatura>();
		try 
		{
			resultado_uno = consulta.executeQuery("SELECT asi_codigo "
												+ "FROM asignatura_bitacora "
												+ "WHERE bit_id = "+bit_id+" AND plan_id = "+plan_id+" ");
			while(resultado_uno.next())
			{						
				int codigo_asignatura= resultado_uno.getInt("asi_codigo");
				resultado_dos = consulta_dos.executeQuery("select asi_nombre from asignatura where asi_codigo = "+codigo_asignatura+" ");
				resultado_dos.next();
				gestiones.add(new Asignatura(codigo_asignatura,resultado_dos.getString("asi_nombre")));
			}

			CerrarConexionBd();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return gestiones;
	}
	
	public ArrayList<Practica> getAlumnosEnPractica(String estado, int asi_codigo)
	{
		ConexionBd();
		practicas = new ArrayList<Practica>();
		try
		{
			if(estado.equals("activa"))
			{
				resultado = consulta.executeQuery("select alu_nombres || ' ' || alu_apellido_p || ' ' || alu_apellido_m as alumno "
						+ ", asi_nombre from practica as p left join alumno as a on a.alu_rut = p.alu_rut "
						+ "left join asignatura as asi on asi.asi_codigo = p.asi_codigo "
						+ "where (p.pra_estado = 'Aceptada' or p.pra_estado = 'Finalizada' or p.pra_estado = 'Evaluada' or p.pra_estado = 'Comisionada') "
						+ "and p.asi_codigo = "+asi_codigo+"");
			}
			if(estado.equals("inactiva"))
			{
				resultado = consulta.executeQuery("select alu_nombres || ' ' || alu_apellido_p || ' ' || alu_apellido_m as alumno "
						+ ", asi_nombre from practica as p left join alumno as a on a.alu_rut = p.alu_rut "
						+ "left join asignatura as asi on asi.asi_codigo = p.asi_codigo "
						+ "where (p.pra_estado = 'Aprobada' or p.pra_estado = 'Reprobada') "
						+ "and p.asi_codigo = "+asi_codigo+"");
			}
			while(resultado.next())
			{	
				practicas.add(new Practica(resultado.getString("alumno"),resultado.getString("asi_nombre")));
			}
			if(practicas.isEmpty())
			{
				practicas.add(new Practica("Sin Registros","Sin Registros"));
			}
			CerrarConexionBd();
		}catch(SQLException e){
			e.printStackTrace();
		}	
		return practicas;
	}
	public ArrayList<Practica> getRegionesPracticas(String estado)
	{
		ConexionBd();
		practicas = new ArrayList<Practica>();
		System.out.println(estado);
		try
		{
			if(estado.equals("activa"))
			{
				resultado = consulta.executeQuery("select r.reg_nombre as region, alu_nombres || ' ' || alu_apellido_p || ' ' || alu_apellido_m as alumno from practica as p"
						+ " left join comuna as c on c.com_id = p.com_id "
						+ "left join provincia as pr on c.pro_id = pr.pro_id "
						+ "left join region as r on pr.reg_id = r.reg_id "
						+ "left join alumno as a on p.alu_rut = a.alu_rut "
						+ "where p.pra_estado = 'Aceptada' or p.pra_estado = 'Finalizada' or "
						+ "p.pra_estado = 'Evaluada' or p.pra_estado = 'Comisionada' "
						+ "order by com_nombre asc");
			}
			if(estado.equals("inactiva"))
			{
				resultado = consulta.executeQuery("select r.reg_nombre as region, alu_nombres || ' ' || alu_apellido_p || ' ' || alu_apellido_m as alumno from practica as p"
						+ " left join comuna as c on c.com_id = p.com_id "
						+ "left join provincia as pr on c.pro_id = pr.pro_id "
						+ "left join region as r on pr.reg_id = r.reg_id "
						+ "left join alumno as a on p.alu_rut = a.alu_rut "
						+ "where p.pra_estado = 'Aprobada' or p.pra_estado = 'Reprobada'");
			}
			while(resultado.next())
			{	
				practicas.add(new Practica(resultado.getString("alumno"),resultado.getString("region")));
			}
			if(practicas.isEmpty())
			{
				practicas.add(new Practica("Sin Registros","Sin Registros"));
			}
			CerrarConexionBd();
		}catch(SQLException e){
			e.printStackTrace();
		}	
		return practicas;
	}
	
	public ArrayList<Practica> getListadoAlumnosEmpresasPracticas(String estado,int codigo)
	{
		ConexionBd();
		practicas = new ArrayList<Practica>();
		try
		{
			if(estado.equals("activa"))
			{
				resultado = consulta.executeQuery("select  alu_nombres || ' ' || alu_apellido_p || ' ' || alu_apellido_m as alumno "
						+ ", emp.emp_nombre as empresa from practica "
						+ "as p left join alumno as a on p.alu_rut = a.alu_rut  "
						+ "left join empresa as emp on p.emp_rut = emp.emp_rut "
						+ "where p.pra_estado = 'Aceptada' or p.pra_estado = 'Finalizada' or  p.pra_estado = 'Evaluada' "
						+ "or p.pra_estado = 'Comisionada' and p.asi_codigo = "+codigo+" "
						+ "order by empresa asc");
			}
			if(estado.equals("inactiva"))
			{
				resultado = consulta.executeQuery("select  alu_nombres || ' ' || alu_apellido_p || ' ' || alu_apellido_m as alumno "
						+ ", emp.emp_nombre as empresa from practica "
						+ "as p left join alumno as a on p.alu_rut = a.alu_rut  "
						+ "left join empresa as emp on p.emp_rut = emp.emp_rut  "
						+ "where p.pra_estado = 'Aprobada' or p.pra_estado = 'Reprobada' "
						+ "and p.asi_codigo = "+codigo+" "
						+ "order by empresa asc");
			}
			while(resultado.next())
			{	
				practicas.add(new Practica(resultado.getString("alumno"),resultado.getString("empresa")));
			}
			if(practicas.isEmpty())
			{
				practicas.add(new Practica("Sin Registros","Sin Registros"));
			}
			CerrarConexionBd();
		}catch(SQLException e){
			e.printStackTrace();
		}	
		return practicas;
	}
	public ArrayList<Practica> getListadoAlumnosTipoObraPracticas(int codigo)
	{
		ConexionBd();
		practicas = new ArrayList<Practica>();
		try
		{
				resultado = consulta.executeQuery("select  alu_nombres || ' ' || alu_apellido_p || ' ' || alu_apellido_m as alumno "
						+ ", t.obr_nombre as obra from practica "
						+ "as p left join alumno as a on p.alu_rut = a.alu_rut  "
						+ "left join obra_tipo as t on p.obr_id = t.obr_id "
						+ "where p.asi_codigo = "+codigo+" "
						+ "order by obra asc");
			while(resultado.next())
			{	
				practicas.add(new Practica(resultado.getString("alumno"),resultado.getString("obra")));
			}
			if(practicas.isEmpty())
			{
				practicas.add(new Practica("Sin Registros","Sin Registros"));
			}
			CerrarConexionBd();
		}catch(SQLException e){
			e.printStackTrace();
		}	
		return practicas;
	}
	public ArrayList<Practica> getListadoAlumnosSupervisorPracticas(String estado,int codigo)
	{
		ConexionBd();
		practicas = new ArrayList<Practica>();
		try
		{
			if(estado.equals("activa"))
			{

				resultado = consulta.executeQuery("select  s.sup_nombres || ' ' || s.sup_apellido_p || ' ' || s.sup_apellido_m as supervisor, "
						+ "alu_nombres || ' ' || alu_apellido_p || ' ' || alu_apellido_m as alumno from practica "
						+ "as p left join alumno as a on p.alu_rut = a.alu_rut  "
						+ "left join supervisor as s on p.sup_rut = s.sup_rut "
						+ "where p.asi_codigo = "+codigo+"  and (p.pra_estado = 'Aceptada' or p.pra_estado = 'Finalizada' or "
						+ "p.pra_estado = 'Evaluada' or p.pra_estado = 'Comisionada') "
						+ "order by supervisor asc");
			}
			if(estado.equals("inactiva"))
			{

				resultado = consulta.executeQuery("select  s.sup_nombres || ' ' || s.sup_apellido_p || ' ' || s.sup_apellido_m as supervisor, "
						+ "alu_nombres || ' ' || alu_apellido_p || ' ' || alu_apellido_m as alumno from practica as p "
						+ "left join alumno as a on p.alu_rut = a.alu_rut  "
						+ "left join supervisor as s on p.sup_rut = s.sup_rut "
						+ "where p.asi_codigo = "+codigo+" and  (p.pra_estado = 'Aprobada' or p.pra_estado = 'Reprobada') "
						+ "order by supervisor asc");
			}
			while(resultado.next())
			{	
				practicas.add(new Practica(resultado.getString("supervisor"),resultado.getString("alumno")));
			}
			if(practicas.isEmpty())
			{
				practicas.add(new Practica("Sin Registros","Sin Registros"));
			}
			CerrarConexionBd();
		}catch(SQLException e){
			e.printStackTrace();
		}	
		return practicas;
	}
	public ArrayList<Practica> getListadoAlumnosAreasPracticas(String estado,int codigo)
	{
		ConexionBd();
		practicas = new ArrayList<Practica>();
		try
		{
			if(estado.equals("activa"))
			{

				resultado = consulta.executeQuery("select a.area_nombre as area, "
					+ "al.alu_nombres || ' ' || al.alu_apellido_p || ' ' || al.alu_apellido_m as alumno "
					+ "from practica as p "
					+ "left join area_practica as ap on p.pra_id = ap.pra_id "
					+ "left join area as a on ap.area_id = a.area_id "
					+ "left join alumno as al on p.alu_rut = al.alu_rut "
					+ "where p.asi_codigo = "+codigo+"  and (p.pra_estado = 'Aceptada' or p.pra_estado = 'Finalizada' or "
					+ "p.pra_estado = 'Evaluada' or p.pra_estado = 'Comisionada') "
					+ "order by area asc");
			}
			if(estado.equals("inactiva"))
			{
				resultado = consulta.executeQuery("select a.area_nombre as area, "
					+ "al.alu_nombres || ' ' || al.alu_apellido_p || ' ' || al.alu_apellido_m as alumno "
					+ "from practica as p "
					+ "left join area_practica as ap on p.pra_id = ap.pra_id "
					+ "left join area as a on ap.area_id = a.area_id "
					+ "left join alumno as al on p.alu_rut = al.alu_rut "
					+ "where p.asi_codigo = "+codigo+"  and (p.pra_estado = 'Aprobada' or p.pra_estado = 'Reprobada') "
					+ "order by area asc");
			}
			while(resultado.next())
			{	
				practicas.add(new Practica(resultado.getString("area"),resultado.getString("alumno")));
			}
			if(practicas.isEmpty())
			{
				practicas.add(new Practica("Sin Registros","Sin Registros"));
			}
			CerrarConexionBd();
		}catch(SQLException e){
			e.printStackTrace();
		}	
		return practicas;
	}
	public ArrayList<Practica> getListadoAlumnosAcademicoPracticas(int codigo)
	{
		ConexionBd();
		practicas = new ArrayList<Practica>();
		try
		{
			resultado = consulta.executeQuery("select a.aca_nombres || ' ' || a.aca_apellido_p || ' ' ||  a.aca_apellido_m as academico, "
					+ "al.alu_nombres || ' ' || al.alu_apellido_p || ' ' || al.alu_apellido_m as alumno from practica as p "
					+ "left join alumno as al on p.alu_rut = al.alu_rut "
					+ "left join academico as a on p.aca_rut = a.aca_rut "
					+ "where p.asi_codigo = "+codigo+"  and (p.pra_estado = 'Aceptada' or p.pra_estado = 'Finalizada' or "
					+ "p.pra_estado = 'Evaluada' or p.pra_estado = 'Comisionada') order by academico asc");
			while(resultado.next())
			{	
				practicas.add(new Practica(resultado.getString("academico"),resultado.getString("alumno")));
			}
			if(practicas.isEmpty())
			{
				practicas.add(new Practica("Sin Registros","Sin Registros"));
			}
			CerrarConexionBd();
		}catch(SQLException e){
			e.printStackTrace();
		}	
		return practicas;
	}
	public ArrayList<Practica> getListadoAlumnosNotasPracticas(int agnio, int semestre,int codigo)
	{
		ConexionBd();
		practicas = new ArrayList<Practica>();
		try
		{
			if(semestre == 1)
			{
				resultado = consulta.executeQuery("select a.alu_nombres || ' ' || a.alu_apellido_p || ' ' || a.alu_apellido_m as alumno "
						+ ",p.pra_nota as nota from practica as p "
						+ "left join alumno as a on p.alu_rut = a.alu_rut "
						+ "where  extract(year from p.pra_fecha_ins) ="+agnio+" and extract(month from p.pra_fecha_ins) <= 7 "
						+ "and asi_codigo =  "+codigo+" and (p.pra_estado = 'Aprobada' or p.pra_estado = 'Reprobada')");
			}
			if(semestre == 2)
			{
				resultado = consulta.executeQuery("select a.alu_nombres || ' ' || a.alu_apellido_p || ' ' || a.alu_apellido_m as alumno "
						+ ",p.pra_nota as nota from practica as p "
						+ "left join alumno as a on p.alu_rut = a.alu_rut "
						+ "where  extract(year from p.pra_fecha_ins) ="+agnio+" and extract(month from p.pra_fecha_ins) > 7 "
						+ "and asi_codigo =  "+codigo+" and (p.pra_estado = 'Aprobada' or p.pra_estado = 'Reprobada')");
			}
			
			while(resultado.next())
			{	
				practicas.add(new Practica(resultado.getString("alumno"),resultado.getString("nota")));
			}
			if(practicas.isEmpty())
			{
				practicas.add(new Practica("Sin Registros","Sin Registros"));
			}
			CerrarConexionBd();
		}catch(SQLException e){
			e.printStackTrace();
		}	
		return practicas;
	}
	public ArrayList<Practica> getAlumnoComunasPracticas(String estado)
	{
		ConexionBd();
		practicas = new ArrayList<Practica>();
		try
		{
			if(estado.equals("activa"))
			{
				resultado = consulta.executeQuery("select c.com_nombre as comuna, alu_nombres || ' ' || alu_apellido_p || ' ' || alu_apellido_m as alumno from practica as p "
						+ "left join comuna as c on c.com_id = p.com_id "
						+ "left join alumno as a on p.alu_rut = a.alu_rut "
						+ "where p.pra_estado = 'Aceptada' or p.pra_estado = 'Finalizada' or "
						+ "p.pra_estado = 'Evaluada' or p.pra_estado = 'Comisionada' "
						+ "order by comuna asc");
			}
			if(estado.equals("inactiva"))
			{
				resultado = consulta.executeQuery("select c.com_nombre as comuna, alu_nombres || ' ' || alu_apellido_p || ' ' || alu_apellido_m as alumno from practica as p"
						+ " left join comuna as c on c.com_id = p.com_id "
						+ "left join alumno as a on p.alu_rut = a.alu_rut "
						+ "where p.pra_estado = 'Aprobada' or p.pra_estado = 'Reprobada'"
						+ "order by comuna asc");
			}
			while(resultado.next())
			{	
				practicas.add(new Practica(resultado.getString("alumno"),resultado.getString("comuna")));
			}
			if(practicas.isEmpty())
			{
				practicas.add(new Practica("Sin Registros","Sin Registros"));
			}
			CerrarConexionBd();
		}catch(SQLException e){
			e.printStackTrace();
		}	
		return practicas;
	}
	public ArrayList<Comuna> getComunasPracticas(String estado)
	{
		ConexionBd();
		comunas = new ArrayList<Comuna>();
		try
		{
			if(estado.equals("activa"))
			{
				resultado = consulta.executeQuery("select c.com_nombre as comuna , (select count(*) from practica as p2 where p2.com_id = p.com_id) as contador from comuna as c "
						+ "left join practica as p on p.com_id = c.com_id "
						+ "where p.pra_estado = 'Aceptada' or p.pra_estado = 'Finalizada' or "
						+ "p.pra_estado = 'Evaluada' or p.pra_estado = 'Comisionada' "
						+ "group by c.com_nombre,p.com_id "
						+ "order by c.com_nombre asc");
			}
			if(estado.equals("inactiva"))
			{
				resultado = consulta.executeQuery("select c.com_nombre as comuna, (select count(*) from practica as p2 where p2.com_id = p.com_id) as contador from comuna as c "
						+ "left join practica as p on p.com_id = c.com_id  "
						+ "where p.pra_estado = 'Aprobada' or p.pra_estado = 'Reprobada'"
						+ "group by c.com_nombre,p.com_id "
						+ "order by c.com_nombre asc");
			}
			while(resultado.next())
			{	
					comunas.add(new Comuna(resultado.getString("comuna"),resultado.getString("contador")));
			}
			if(comunas.isEmpty())
			{
				comunas.add(new Comuna("Sin Registros","Sin Registros"));
			}
			CerrarConexionBd();
		}catch(SQLException e){
			e.printStackTrace();
		}	
		return comunas;
	}
	
	public ArrayList<Practica> getTotalTipoObraPracticas(int asi_codigo)
	{
		ConexionBd();
		practicas = new ArrayList<Practica>();
		try
		{
			
			resultado = consulta.executeQuery("select t.obr_nombre as nombre, "
					+ "(select count(*) from practica as p where p.obr_id = t.obr_id and p.asi_codigo = "+asi_codigo+") as contador "
					+ "from obra_tipo as t ");
			while(resultado.next())
			{	
				practicas.add(new Practica(resultado.getString("nombre"),resultado.getString("contador")));
			}
			
			CerrarConexionBd();
		}catch(SQLException e){
			e.printStackTrace();
		}
		return practicas;
	}
	
	public ArrayList<Area>getTotalAreasPracticas(String estado,int asi_codigo)
	{
		ConexionBd();
		areas = new ArrayList<Area>();
		try
		{
			if(estado.equals("activa"))
			{
				resultado = consulta.executeQuery("select a.area_nombre as nombre, "
						+ "(select count(*) from area_practica as ap  "
						+ "left join practica as p on ap.pra_id = p.pra_id "
						+ "where ap.area_id = a.area_id and p.asi_codigo = 120067 and "
						+ "(p.pra_estado = 'Aceptada' or p.pra_estado = 'Finalizada' or  p.pra_estado = 'Evaluada' "
						+ "or p.pra_estado = 'Comisionada')) as contador  from area as a ");
			}
			if(estado.equals("inactiva"))
			{
				resultado = consulta.executeQuery("select a.area_nombre as nombre, "
						+ "(select count(*) from area_practica as ap  "
						+ "left join practica as p on ap.pra_id = p.pra_id "
						+ "where ap.area_id = a.area_id and p.asi_codigo = 120067 and "
						+ "(p.pra_estado = 'Aceptada' or p.pra_estado = 'Finalizada')) as contador  from area as a ");
			}
			while(resultado.next())
			{	
				areas.add(new Area(resultado.getString("nombre"),resultado.getString("contador")));
			}
			CerrarConexionBd();
		}catch(SQLException e){
			e.printStackTrace();
		}	
		return areas;
	}
	public ArrayList<Academico>getTotalAcademicoPracticas(int asi_codigo)
	{
		ConexionBd();
		academicos = new ArrayList<Academico>();
		try
		{
			resultado = consulta.executeQuery("select a.aca_nombres || ' ' || a.aca_apellido_p || ' ' || a.aca_apellido_m as nombre, "
					+ "(select count(*) from practica as p  where p.aca_rut = a.aca_rut and p.asi_codigo = "+asi_codigo+" "
					+ "and (p.pra_estado = 'Aceptada' or p.pra_estado = 'Finalizada' or  p.pra_estado = 'Evaluada' "
					+ "or p.pra_estado = 'Comisionada')) as contador  from academico as a ");
			while(resultado.next())
			{	
				academicos.add(new Academico(resultado.getString("nombre"),resultado.getString("contador")));
			}
			CerrarConexionBd();
		}catch(SQLException e){
			e.printStackTrace();
		}	
		return academicos;
	}
	public ArrayList<Supervisor> getTotalSupervisorPracticas(String estado,int asi_codigo)
	{
		ConexionBd();
		supervisores = new ArrayList<Supervisor>();
		try
		{
			if(estado.equals("activa"))
			{
				resultado = consulta.executeQuery("select s.sup_nombres || ' ' || s.sup_apellido_p || ' ' || s.sup_apellido_m as nombre, "
						+ "(select count(*) from practica as p where p.sup_rut = s.sup_rut and p.asi_codigo = "+asi_codigo+" and "
						+ "(p.pra_estado = 'Aceptada' or p.pra_estado = 'Finalizada' or "
						+ "p.pra_estado = 'Evaluada' or p.pra_estado = 'Comisionada')) as contador "
						+ "from supervisor as s ");
			}
			if(estado.equals("inactiva"))
			{
				resultado = consulta.executeQuery("select s.sup_nombres || ' ' || s.sup_apellido_p || ' ' || s.sup_apellido_m as nombre, "
						+ "(select count(*) from practica as p where p.sup_rut = s.sup_rut and p.asi_codigo = "+asi_codigo+" and "
						+ "(p.pra_estado = 'Aprobada' or p.pra_estado = 'Reprobada' )) as contador "
						+ "from supervisor as s ");
			}
			while(resultado.next())
			{	
				System.out.println(resultado.getString("nombre"));
				supervisores.add(new Supervisor(resultado.getString("nombre"),resultado.getString("contador")));
			}
			CerrarConexionBd();
		}catch(SQLException e){
			e.printStackTrace();
		}	
		return supervisores;
	}
	public ArrayList<Empresa> getTotalEmpresasPracticas(String estado, int asi_codigo)
	{
		ConexionBd();
		empresas = new ArrayList<Empresa>();
		try
		{
			if(estado.equals("activa"))
			{
				resultado = consulta.executeQuery("select e.emp_nombre as nombre, "
						+ "(select count(*) from practica as p where p.emp_rut = e.emp_rut and p.asi_codigo = "+asi_codigo+" "
						+ "and p.pra_estado = 'Aceptada' or p.pra_estado = 'Finalizada' or "
						+ "p.pra_estado = 'Evaluada' or p.pra_estado = 'Comisionada') as contador "
						+ "from empresa as e ");
			}
			if(estado.equals("inactiva"))
			{
				resultado = consulta.executeQuery("select e.emp_nombre as nombre, "
						+ "(select count(*) from practica as p where p.emp_rut = e.emp_rut and p.asi_codigo = "+asi_codigo+" "
						+ "and p.pra_estado = 'Aprobada' or p.pra_estado = 'Reprobada') as contador "
						+ "from empresa as e ");
			}
			while(resultado.next())
			{	
				empresas.add(new Empresa(resultado.getString("nombre"),resultado.getString("contador")));
			}
			CerrarConexionBd();
		}catch(SQLException e){
			e.printStackTrace();
		}	
		return empresas;
	}
	public ArrayList<Alumno> CargaAlumnos(String agnio, String periodo)// obtiene los alumnos de la bd de la universidad y los devuelve como array
	{
		ConexionBd2();
		alumnos = new ArrayList<Alumno>();
		int cont = 0;
		try 
		{
			resultado_uno = consulta.executeQuery("execute sp_web_web_practica_eic_recupera_alumno_carrera_sr '"+agnio+"', '"+periodo+"'");
			while(resultado_uno.next())
			{	
				cont++;
				String pass = PasswordGenerador.getPassword(
						  PasswordGenerador.MINUSCULAS+
						  PasswordGenerador.MAYUSCULAS+
						  PasswordGenerador.NUMEROS,10);
				String rut = ""+resultado_uno.getInt("ALU_RUT")+""+resultado_uno.getString("COM_ID");
				alumnos.add(new Alumno(Herramientas.Formatear(rut),pass,resultado_uno.getString("ALU_NOMBRES"),resultado_uno.getString("ALU_APELLIDO_P"),resultado_uno.getString("ALU_APELLIDO_M"),resultado_uno.getString("ALU_EMAIL"),resultado_uno.getString("ALU_ESTADO"),1));
			}
			CerrarConexionBd2();
		} catch (Exception e) 
		{
			e.printStackTrace();
		}
		System.out.println("total alumnos "+cont);
		return alumnos;
	}
	// obtiene datos de tabla cursa_plan_estudio y los devuelve como arrayList
	public ArrayList<CursaPlanEstudio> CargaCursaPlan(String agnio, String periodo, int plan_id)
	{
		ConexionBd2();
		cursa_planes = new ArrayList<CursaPlanEstudio>();
		try 
		{
			resultado_uno = consulta.executeQuery("execute sp_web_web_practica_eic_recupera_alumno_carrera_sr '"+agnio+"', '"+periodo+"'");
			while(resultado_uno.next())
			{	
				String rut = ""+resultado_uno.getInt("ALU_RUT")+""+resultado_uno.getString("COM_ID");
				cursa_planes.add(new CursaPlanEstudio(plan_id,Herramientas.Formatear(rut),resultado_uno.getInt("CUR_AGNIO_INGRESO"),resultado_uno.getInt("CUR_SEMESTRE")));
			}
			CerrarConexionBd2();
		} catch (Exception e) 
		{
			e.printStackTrace();
		}
		return cursa_planes;
	}
	
	public ArrayList<Observacion> getObservaciones(int bit_id)//obtiene las observaciones que se han hecho a cierta bitacora
	{
		ConexionBd();
		observaciones = new ArrayList<Observacion>();
		try 
		{
			resultado_uno = consulta.executeQuery("SELECT obs_id "
												+ "FROM observacion_bitacora "
												+ "WHERE bit_id = "+bit_id+"");
			while(resultado_uno.next())
			{						
				int codigo_observacion= resultado_uno.getInt("obs_id");
				resultado_dos = consulta_dos.executeQuery("select * from observacion where obs_id = "+codigo_observacion+" ");
				resultado_dos.next();
				observaciones.add(new Observacion(codigo_observacion,resultado_dos.getString("obs_texto"),resultado_dos.getString("obs_fecha"),resultado_dos.getString("obs_hora")));
			}
			CerrarConexionBd();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return observaciones;
	}
	
	public ArrayList<Direccion> getDirecciones(String rut)//obtiene las direcciones segun el rut del alumno
	{
		ConexionBd();
		direcciones = new ArrayList<Direccion>();
		try 
		{
			resultado_uno = consulta.executeQuery("SELECT * "
												+ "FROM direccion "
												+ "WHERE alu_rut = '"+rut+"'");
			while(resultado_uno.next())
			{						
				direcciones.add(new Direccion(rut,resultado_uno.getInt("com_id"),resultado_uno.getInt("dir_tp_id")
						,resultado_uno.getString("dir_calle"),resultado_uno.getInt("dir_numero"),resultado_uno.getString("dir_sector")));
			}
			CerrarConexionBd();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return direcciones;
	}
	
	public ArrayList<Giro> getGiros()//obtiene los giros registrados en el sistema
	{
		ConexionBd();
		giros = new ArrayList<Giro>();
		try 
		{
			resultado = consulta.executeQuery("SELECT * FROM GIRO");
			while(resultado.next())
			{						
				giros.add(new Giro(resultado.getInt("gir_id"),resultado.getInt("rub_id"),
						resultado.getString("gir_codigo"),resultado.getString("gir_nombre")));
			}
			CerrarConexionBd();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return giros;
	}
	public ArrayList<Rubro> getRubros()//obtiene los rubros registrados en el sistema
	{
		ConexionBd();
		rubros = new ArrayList<Rubro>();
		try 
		{
			resultado = consulta.executeQuery("SELECT * FROM RUBRO");
			while(resultado.next())
			{						
				rubros.add(new Rubro(resultado.getInt("rub_id"),resultado.getString("rub_nombre"),resultado.getString("rub_padre")));
			}
			CerrarConexionBd();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rubros;
	}
	
	public ArrayList<Imagen> getImagenes(int bit_id)//obtiene las url de las imagenes  que se han subido a cierta bitacora
	{
		ConexionBd();
		imagenes = new ArrayList<Imagen>();
		try 
		{
			resultado_uno = consulta.executeQuery("SELECT img_id "
												+ "FROM imagen_bitacora "
												+ "WHERE bit_id = "+bit_id+"");
			while(resultado_uno.next())
			{						
				int codigo_imagen= resultado_uno.getInt("img_id");
				resultado_dos = consulta_dos.executeQuery("select * from imagen where img_id = "+codigo_imagen+" ");
				resultado_dos.next();
				imagenes.add(new Imagen(codigo_imagen,resultado_dos.getString("img_url"),resultado_dos.getString("img_fecha"),resultado_dos.getString("img_hora")));
			}
			CerrarConexionBd();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return imagenes;
	}
	
	public ArrayList<Evaluacion> getEvaluaciones(ArrayList<Integer> eval_clave)//obtiene todas las evaluaciones correspondiente al usuario que se ha autentificado
	{																		   //solo entrega las evaluaciones cuyas practicas estén finalizadas
		ConexionBd();
		evaluaciones = new ArrayList<Evaluacion>();
		try 
		{
			Iterator<Integer> iterador = eval_clave.iterator();
			while(iterador.hasNext()){
				int elemento = iterador.next();				
				resultado_dos = consulta.executeQuery("SELECT  EV.eva_id, EV.eval_clave, EV.pra_id, EV.eva_nombre, trunc( CAST(EV.eva_nota as NUMERIC), 1) AS eva_nota, EV.eva_observacion, "
						+ "EV.eva_fecha, EV.eva_hora, EV.eva_estado "
						+ "FROM evaluacion EV, practica P "
						+ "WHERE (P.pra_estado <> 'Aceptada' || P.pra_estado <> 'Pendiente') AND P.pra_id = EV.pra_id AND EV.eval_clave = "+elemento);
				
				while(resultado_dos.next()){						
					
				evaluaciones.add(new Evaluacion(resultado_dos.getInt("eva_id"), resultado_dos.getInt("eval_clave"), resultado_dos.getInt("pra_id"),
												resultado_dos.getString("eva_nombre"), resultado_dos.getDouble("eva_nota"), resultado_dos.getString("eva_observacion"),
												resultado_dos.getString("eva_fecha"), resultado_dos.getString("eva_hora"),resultado_dos.getString("eva_estado")
												)
								);
				}
				
			}		
			CerrarConexionBd();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return evaluaciones;
	}
	
	public ArrayList<Evaluacion> getEvaluaciones(int pra_id, ArrayList<Evaluacion> evaluaciones)//obtiene todas las evaluaciones correspondiente al usuario que se ha autentificado(resumen de evaluaciones de un alumno)
	{
		ConexionBd();
		try 
		{
				resultado_dos = consulta.executeQuery("SELECT  EV.eva_id, EV.eval_clave, EV.pra_id, EV.eva_nombre, trunc( CAST(EV.eva_nota as NUMERIC), 1) AS eva_nota, EV.eva_observacion, "
						+ "EV.eva_fecha, EV.eva_hora, EV.eva_estado "
						+ "FROM evaluacion EV "
						+ "WHERE EV.pra_id = "+pra_id);
						//+ "WHERE EV.pra_id = "+pra_id+" AND EV.eva_estado = 'Evaluado'" );
				
				while(resultado_dos.next()){						
					
				evaluaciones.add(new Evaluacion(resultado_dos.getInt("eva_id"), resultado_dos.getInt("eval_clave"), resultado_dos.getInt("pra_id"),
												resultado_dos.getString("eva_nombre"), resultado_dos.getDouble("eva_nota"), resultado_dos.getString("eva_observacion"),
												resultado_dos.getString("eva_fecha"), resultado_dos.getString("eva_hora"),resultado_dos.getString("eva_estado")
												)
								);
				}
				

	
			CerrarConexionBd();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return evaluaciones;
	}
	
	public ArrayList<EvaluacionAlumno> getEvaluacionesAlumno(int pra_id, ArrayList<EvaluacionAlumno> evaluaciones_alumno)//obtiene todas las evaluaciones correspondiente al usuario que se ha autentificado(resumen de evaluaciones de un alumno)
	{
		ConexionBd();
		try 
		{
				resultado_dos = consulta.executeQuery("SELECT EV.alu_rut, EV.pra_id, EV.eva_fecha, EV.rol_id, EV.eva_nombre, trunc( CAST(EV.eva_nota as NUMERIC), 1) AS eva_nota, EV.eva_hora, EV.eva_observacion, EV.eva_estado "
						+ "FROM evaluacion_alumno EV "
						+ "WHERE EV.pra_id = "+pra_id);
						//+ "WHERE EV.pra_id = "+pra_id+" AND EV.eva_estado = 'Evaluado'" );
				
				while(resultado_dos.next()){						
					
				evaluaciones_alumno.add(new EvaluacionAlumno(
												resultado_dos.getString("alu_rut"), resultado_dos.getInt("pra_id"), resultado_dos.getString("eva_fecha"),
												resultado_dos.getInt("rol_id"), resultado_dos.getString("eva_nombre"), resultado_dos.getDouble("eva_nota"), 
												resultado_dos.getString("eva_observacion"), resultado_dos.getString("eva_hora"),resultado_dos.getString("eva_estado")
												)
								);				
				}
				

	
			CerrarConexionBd();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return evaluaciones_alumno;
	}
	
	public ArrayList<EvaluacionSupervisor> getEvaluacionesSupervisor(int pra_id, ArrayList<EvaluacionSupervisor> evaluaciones_supervisor)//obtiene todas las evaluaciones correspondiente al usuario que se ha autentificado(resumen de evaluaciones de un supervisor)
	{
		ConexionBd();
		try 
		{
				resultado_dos = consulta.executeQuery("SELECT EV.sup_rut, EV.pra_id, EV.eva_fecha, EV.rol_id, EV.eva_nombre, trunc( CAST(EV.eva_nota as NUMERIC), 1) AS eva_nota, EV.eva_hora, EV.eva_observacion, EV.eva_estado "
						+ "FROM evaluacion_supervisor EV "
						+ "WHERE EV.pra_id = "+pra_id);
						//+ "WHERE EV.pra_id = "+pra_id+" AND EV.eva_estado = 'Evaluado'" );
				
				while(resultado_dos.next()){						
					
				evaluaciones_supervisor.add(new EvaluacionSupervisor(
												resultado_dos.getString("sup_rut"), resultado_dos.getInt("pra_id"), resultado_dos.getString("eva_fecha"),
												resultado_dos.getInt("rol_id"), resultado_dos.getString("eva_nombre"), resultado_dos.getDouble("eva_nota"), 
												resultado_dos.getString("eva_observacion"), resultado_dos.getString("eva_hora"),resultado_dos.getString("eva_estado")
												)
								);				
				}
				

	
			CerrarConexionBd();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return evaluaciones_supervisor;
	}
	
	public ArrayList<EvaluacionAcademico> getEvaluacionesAcademico(int pra_id, ArrayList<EvaluacionAcademico> evaluaciones_academico)//obtiene todas las evaluaciones correspondiente al usuario que se ha autentificado(resumen de evaluaciones de un academico)
	{
		ConexionBd();
		try 
		{
				resultado_dos = consulta.executeQuery("SELECT EV.aca_rut, EV.pra_id, EV.eva_fecha, EV.rol_id, EV.eva_nombre, trunc( CAST(EV.eva_nota as NUMERIC), 1) AS eva_nota, EV.eva_hora, EV.eva_observacion, EV.eva_estado "
						+ "FROM evaluacion_academico EV "
						+ "WHERE EV.pra_id = "+pra_id);
						//+ "WHERE EV.pra_id = "+pra_id+" AND EV.eva_estado = 'Evaluado'" );
				
				while(resultado_dos.next()){						
					
				evaluaciones_academico.add(new EvaluacionAcademico(
												resultado_dos.getString("aca_rut"), resultado_dos.getInt("pra_id"), resultado_dos.getString("eva_fecha"),
												resultado_dos.getInt("rol_id"), resultado_dos.getString("eva_nombre"), resultado_dos.getDouble("eva_nota"), 
												resultado_dos.getString("eva_observacion"), resultado_dos.getString("eva_hora"),resultado_dos.getString("eva_estado")
												)
								);				
				}
				

	
			CerrarConexionBd();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return evaluaciones_academico;
	}
	
	public ArrayList<Evaluadores> getEvaluadores(int pra_id, ArrayList<Evaluadores> evaluadores)//obtiene todas los evaluadores de una práctica(sirve para ver detalle de evaluacion alumno y admin)
	{
		ConexionBd();
		try 
		{
				resultado_uno = consulta.executeQuery("SELECT DISTINCT(E.eval_clave), E.asi_codigo, E.plan_id, E.rol_id, E.aca_rut, E.sup_rut, E.alu_rut, E.eval_fecha, E.eval_estado "
						+ "FROM evaluadores E, practica P, evaluacion EV "
						+ "WHERE E.eval_clave = EV.eval_clave AND EV.pra_id = "+pra_id);
						//+ "WHERE EV.pra_id = "+pra_id+" AND EV.eva_estado = 'Evaluado'" );
				String nulo = "";
				String aca_rut = "";
				String sup_rut = "";
				String alu_rut = "";
				while(resultado_uno.next()){						
					
					aca_rut = resultado_uno.getString("aca_rut");
					if(resultado_uno.wasNull()){
						aca_rut = nulo;
					}else{
						//aca_rut = resultado_uno.getString("aca_rut");
					}
					
					sup_rut = resultado_uno.getString("sup_rut");
					if(resultado_uno.wasNull()){
						sup_rut = nulo;
					}else{
						//sup_rut = resultado_uno.getString("sup_rut");
					}
					alu_rut = resultado_uno.getString("alu_rut");
					if(resultado_uno.wasNull()){
						alu_rut = nulo;
					}else{
						//alu_rut = resultado_uno.getString("alu_rut");
					}
					evaluadores.add(new Evaluadores(resultado_uno.getInt("eval_clave"), resultado_uno.getInt("asi_codigo"), resultado_uno.getInt("plan_id"),
				 									resultado_uno.getInt("rol_id"),
				 									aca_rut, sup_rut,	alu_rut, 
				 									resultado_uno.getString("eval_fecha"), resultado_uno.getString("eval_estado")
									 				)
									);
				}
				

	
			CerrarConexionBd();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return evaluadores;
	}
	
	public int agregarDetalleEvaluacion(DetalleEvaluacion detalle)
	{
		try
		{
			ConexionBd();
			PreparedStatement enunciado = conexion.prepareStatement("insert into detalle_evaluacion values(?,?,?)");
			enunciado.setInt(1, detalle.getEva_id());
			enunciado.setInt(2, detalle.getItem_id());
			enunciado.setInt(3, detalle.getOpc_id());
			enunciado.execute();			
			conexion.close();
			return 1;
		}
		catch(SQLException e)
		{
			e.printStackTrace();
			return 2;
		}
	}
	
	public int agregarDetalleEvaluacionAlumno(DetalleEvaluacionAlumno detalle)
	{
		try
		{
			ConexionBd();
			PreparedStatement enunciado = conexion.prepareStatement("insert into detalle_evaluacion_alumno(alu_rut,pra_id,eva_fecha,rol_id,opc_id,item_id) values(?,?,'"+detalle.getEva_fecha()+"',?,?,?)");
			enunciado.setString(1, detalle.getAlu_rut());
			enunciado.setInt(2, detalle.getPra_id());
			enunciado.setInt(3, detalle.getRol_id());
			enunciado.setInt(4, detalle.getOpc_id());
			enunciado.setInt(5, detalle.getItem_id());
			enunciado.execute();			
			conexion.close();
			return 1;
		}
		catch(SQLException e)
		{
			e.printStackTrace();
			return 2;
		}
	}
	
	public int agregarDetalleEvaluacionSupervisor(DetalleEvaluacionSupervisor detalle)
	{
		try
		{
			ConexionBd();
			PreparedStatement enunciado = conexion.prepareStatement("insert into detalle_evaluacion_supervisor(sup_rut,pra_id,eva_fecha,rol_id,opc_id,item_id) values(?,?,'"+detalle.getEva_fecha()+"',?,?,?)");
			enunciado.setString(1, detalle.getSup_rut());
			enunciado.setInt(2, detalle.getPra_id());
			enunciado.setInt(3, detalle.getRol_id());
			enunciado.setInt(4, detalle.getOpc_id());
			enunciado.setInt(5, detalle.getItem_id());
			enunciado.execute();			
			conexion.close();
			return 1;
		}
		catch(SQLException e)
		{
			e.printStackTrace();
			return 2;
		}
	}
	
	public int agregarDetalleEvaluacionAcademico(DetalleEvaluacionAcademico detalle)
	{
		try
		{
			ConexionBd();
			PreparedStatement enunciado = conexion.prepareStatement("insert into detalle_evaluacion_academico(aca_rut,pra_id,eva_fecha,rol_id,opc_id,item_id) values(?,?,'"+detalle.getEva_fecha()+"',?,?,?)");
			enunciado.setString(1, detalle.getAca_rut());
			enunciado.setInt(2, detalle.getPra_id());
			enunciado.setInt(3, detalle.getRol_id());
			enunciado.setInt(4, detalle.getOpc_id());
			enunciado.setInt(5, detalle.getItem_id());
			enunciado.execute();			
			conexion.close();
			return 1;
		}
		catch(SQLException e)
		{
			e.printStackTrace();
			return 2;
		}
	}
	
	public ArrayList<DetalleEvaluacionAlumno> getDetalleEvaluacionAlumno(String alu_rut, int pra_id, String eva_fecha, int rol_id)
	{	
		ArrayList<DetalleEvaluacionAlumno> detalle_evaluacion_alumno = new ArrayList<DetalleEvaluacionAlumno>();
		try
		{
			ConexionBd();
			resultado_uno = consulta.executeQuery("SELECT * "
												+ "FROM detalle_evaluacion_alumno "
												+ "WHERE alu_rut = '"+alu_rut+"' AND pra_id = "+pra_id+" AND eva_fecha = '"+eva_fecha+"' AND rol_id = "+rol_id);
			while(resultado_uno.next()){				
				detalle_evaluacion_alumno.add(new DetalleEvaluacionAlumno(
																		  resultado_uno.getString("alu_rut"), resultado_uno.getInt("pra_id"), 
																		  resultado_uno.getString("eva_fecha"), resultado_uno.getInt("rol_id"),
																		  resultado_uno.getInt("opc_id"), resultado_uno.getInt("item_id")
																		  )
											);
			}
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
		return detalle_evaluacion_alumno;
	}
	
	public ArrayList<DetalleEvaluacionSupervisor> getDetalleEvaluacionSupervisor(String sup_rut, int pra_id, String eva_fecha, int rol_id)
	{	
		ArrayList<DetalleEvaluacionSupervisor> detalle_evaluacion_supervisor = new ArrayList<DetalleEvaluacionSupervisor>();
		try
		{
			ConexionBd();
			resultado_uno = consulta.executeQuery("SELECT * "
												+ "FROM detalle_evaluacion_supervisor "
												+ "WHERE sup_rut = '"+sup_rut+"' AND pra_id = "+pra_id+" AND eva_fecha = '"+eva_fecha+"' AND rol_id = "+rol_id);
			while(resultado_uno.next()){				
				detalle_evaluacion_supervisor.add(new DetalleEvaluacionSupervisor(
																		  resultado_uno.getString("sup_rut"), resultado_uno.getInt("pra_id"), 
																		  resultado_uno.getString("eva_fecha"), resultado_uno.getInt("rol_id"),
																		  resultado_uno.getInt("opc_id"), resultado_uno.getInt("item_id")
																		  )
											);
			}
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
		return detalle_evaluacion_supervisor;
	}
	
	public ArrayList<DetalleEvaluacionAcademico> getDetalleEvaluacionAcademico(String aca_rut, int pra_id, String eva_fecha, int rol_id)
	{	
		ArrayList<DetalleEvaluacionAcademico> detalle_evaluacion_academico = new ArrayList<DetalleEvaluacionAcademico>();
		try
		{
			ConexionBd();
			resultado_uno = consulta.executeQuery("SELECT * "
												+ "FROM detalle_evaluacion_academico "
												+ "WHERE aca_rut = '"+aca_rut+"' AND pra_id = "+pra_id+" AND eva_fecha = '"+eva_fecha+"' AND rol_id = "+rol_id);
			while(resultado_uno.next()){				
				detalle_evaluacion_academico.add(new DetalleEvaluacionAcademico(
																		  resultado_uno.getString("aca_rut"), resultado_uno.getInt("pra_id"), 
																		  resultado_uno.getString("eva_fecha"), resultado_uno.getInt("rol_id"),
																		  resultado_uno.getInt("opc_id"), resultado_uno.getInt("item_id")
																		  )
											);
			}
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
		return detalle_evaluacion_academico;
	}
	
	public int getOpc_id(int item_id, double opc_valor)//obtiene el id de una opción dado un item y el valor marcado en la rubrica
	{
		ConexionBd();
		int opc_id = 0;
		try 
		{
			resultado_uno = consulta.executeQuery("SELECT opc_id FROM opcion WHERE item_id = "+item_id+" AND opc_valor= "+opc_valor);
			while(resultado_uno.next()){				
				opc_id =(resultado_uno.getInt("opc_id"));
			}
					
			CerrarConexionBd();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return opc_id;
	}
	
	public int realizarEvaluacion(int eva_id, double eva_nota, String eva_observacion) {
		try {
			ConexionBd();
			PreparedStatement enunciado = conexion.prepareStatement("UPDATE evaluacion SET "
																  + " eva_nota = round( CAST("+eva_nota+" as NUMERIC), 1) , eva_observacion = '"+eva_observacion+"',"
																  + " eva_fecha = CURRENT_DATE, eva_hora = LOCALTIME, eva_estado = 'Evaluado'"
																  + " WHERE eva_id = '"+eva_id+"'");
			enunciado.execute();			
			conexion.close();
			return 1;
		} catch (SQLException e) {
			e.printStackTrace();
			return 2;
		}
	}
	
	public String realizarEvaluacionAlumno(String alu_rut, int pra_id, int rol_id, double eva_nota, String eva_observacion) {
		try {
			ConexionBd();
			PreparedStatement enunciado = conexion.prepareStatement("UPDATE evaluacion_alumno SET "
																  + " eva_nota = round( CAST("+eva_nota+" as NUMERIC), 1) , eva_observacion = '"+eva_observacion+"',"
																  + " eva_fecha = CURRENT_DATE, eva_hora = LOCALTIME, eva_estado = 'Evaluado'"
																  + " WHERE alu_rut = '"+alu_rut+"' AND pra_id = "+pra_id+" AND rol_id = "+rol_id);
			enunciado.execute();			
			conexion.close();
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date date = new Date();
			System.out.println(dateFormat.format(date));
			return ""+dateFormat.format(date);
		} catch (SQLException e) {
			e.printStackTrace();
			return "Error";
		}
	}
	
	public String realizarEvaluacionSupervisor(String sup_rut, int pra_id, int rol_id, double eva_nota, String eva_observacion) {
		try {
			ConexionBd();
			PreparedStatement enunciado = conexion.prepareStatement("UPDATE evaluacion_supervisor SET "
																  + " eva_nota = round( CAST("+eva_nota+" as NUMERIC), 1) , eva_observacion = '"+eva_observacion+"',"
																  + " eva_fecha = CURRENT_DATE, eva_hora = LOCALTIME, eva_estado = 'Evaluado'"
																  + " WHERE sup_rut = '"+sup_rut+"' AND pra_id = "+pra_id+" AND rol_id = "+rol_id);
			enunciado.execute();			
			conexion.close();
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date date = new Date();
			System.out.println(dateFormat.format(date));
			return ""+dateFormat.format(date);
		} catch (SQLException e) {
			e.printStackTrace();
			return "Error";
		}
	}
	
	public String realizarEvaluacionAcademico(String aca_rut, int pra_id, int rol_id, double eva_nota, String eva_observacion) {
		try {
			ConexionBd();
			PreparedStatement enunciado = conexion.prepareStatement("UPDATE evaluacion_academico SET "
																  + " eva_nota = round( CAST("+eva_nota+" as NUMERIC), 1) , eva_observacion = '"+eva_observacion+"',"
																  + " eva_fecha = CURRENT_DATE, eva_hora = LOCALTIME, eva_estado = 'Evaluado'"
																  + " WHERE aca_rut = '"+aca_rut+"' AND pra_id = "+pra_id+" AND rol_id = "+rol_id);
			enunciado.execute();			
			conexion.close();
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date date = new Date();
			System.out.println(dateFormat.format(date));
			return ""+dateFormat.format(date);
		} catch (SQLException e) {
			e.printStackTrace();
			return "Error";
		}
	}
	
	public Supervisor getSupervisor(String rut) 
	{
		ConexionBd();
		try
		{
			resultado = consulta.executeQuery("select * from supervisor where sup_rut = '"+rut+"' ");
			resultado.next();
			supervisor = new Supervisor(rut,resultado.getString("sup_password"),resultado.getString("sup_nombres"),resultado.getString("sup_apellido_p"),
					resultado.getString("sup_apellido_m"),resultado.getString("sup_email"),resultado.getString("sup_telefono"),
					resultado.getString("sup_celular"),resultado.getString("sup_profesion"),resultado.getString("sup_cargo"),
					resultado.getString("sup_estado"));
			CerrarConexionBd();
		}catch(SQLException e){
			e.printStackTrace();
		}	
		return supervisor;
	}
	
	public Carrera getCarrera(int car_codigo) 
	{
		ConexionBd();
		try
		{
			resultado = consulta.executeQuery("select * from carrera where car_codigo = "+car_codigo+" ");
			resultado.next();
			carrera = new Carrera(car_codigo,resultado.getString("car_nombre"),resultado.getString("car_sede"));
			CerrarConexionBd();
		}catch(SQLException e){
			e.printStackTrace();
		}	
		return carrera;
	}
	
	public ArrayList<Asignatura> getGestionesPosibles(int codigo)
	{
		ConexionBd();
		try 
		{
			gestiones = new ArrayList<Asignatura>();
			resultado = consulta.executeQuery("select asi_codigo,asi_nombre from asignatura where asi_nombre like 'Gestion %';");
			while(resultado.next())
			{
				if(resultado.getInt("asi_codigo")> codigo)
				{
					gestiones.add(new Asignatura(resultado.getInt("asi_codigo"),resultado.getString("asi_nombre")));
				}
				
			}
			CerrarConexionBd();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return gestiones;
	}
	public ArrayList<Asignatura> getGestiones()
	{
		ConexionBd();
		try 
		{
			gestiones = new ArrayList<Asignatura>();
			resultado = consulta.executeQuery("select asi_codigo,asi_nombre from asignatura where asi_nombre like 'Gestion %';");
			while(resultado.next())
			{
					gestiones.add(new Asignatura(resultado.getInt("asi_codigo"),resultado.getString("asi_nombre")));	
			}
			CerrarConexionBd();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return gestiones;
	}
	
	public ArrayList<Asignatura> getAsignaturas(int plan_id,int bit_id)//o
	{
		try 
		{
			/*
			 * 
			
			ConexionBd();
			resultado_uno = consulta.executeQuery("SELECT asi_codigo "
												+ "FROM asignatura_bitacora "
												+ "WHERE bit_id = "+bit_id+" AND plan_id = "+plan_id+" ");
			while(resultado_uno.next())
			{						
				int ban=1;
				int codigo_asignatura= resultado_uno.getInt("asi_codigo");
				for(int i=0;i<asignaturas_tributadas.size() && ban ==1;i++)
				{
					if(asignaturas_tributadas.get(i).getAsi_codigo()== codigo_asignatura)
					{
						ban=0;
					}
				}
				if(ban==1)
				{
					resultado_dos = consulta_dos.executeQuery("select asi_nombre from asignatura where asi_codigo = "+codigo_asignatura+" ");
					resultado_dos.next();
					gestiones.add(new Asignatura(codigo_asignatura,resultado_dos.getString("asi_nombre")));
				}	
			}

			 */
			gestiones = new ArrayList<Asignatura>();
			ArrayList<Asignatura> asignaturas_tributadas = new ArrayList<Asignatura>();
			
			ConexionBd();
			resultado_dos = consulta_dos.executeQuery("select * from asignatura_bitacora "
					+ "where bit_id = "+bit_id+" and plan_id = "+plan_id+" ");
			while(resultado_dos.next())
			{
				asignaturas_tributadas.add(new Asignatura(resultado_dos.getInt("asi_codigo")));
			}
			CerrarConexionBd();
			
			ConexionBd();
			resultado = consulta.executeQuery("select * from asignatura where plan_id = "+plan_id);
			while(resultado.next())
			{
				int ban=1;
				int codigo_asignatura= resultado.getInt("asi_codigo");
				for(int i=0;i<asignaturas_tributadas.size() && ban ==1;i++)
				{
					if(asignaturas_tributadas.get(i).getAsi_codigo()== codigo_asignatura)
					{
						ban=0;
					}
				}
				if(ban==1)
				{
					resultado_dos = consulta_dos.executeQuery("select asi_nombre from asignatura where asi_codigo = "+codigo_asignatura+" ");
					resultado_dos.next();
					gestiones.add(new Asignatura(codigo_asignatura,resultado_dos.getString("asi_nombre")));
				}	
					
			}
			CerrarConexionBd();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return gestiones;
	}
	
	public Empresa getEmpresaSupervisor(String rut)
	{
		ConexionBd();
		try 
		{
			resultado = consulta.executeQuery("select emp_rut from historial_supervisor as h where h.sup_rut = '"+rut+"' and h.his_fecha = (select max(h2.his_fecha)from historial_supervisor as h2 where h2.sup_rut = '"+rut+"')");
			resultado.next();
			
			String rut_empresa = resultado.getString("emp_rut");
			resultado_dos = consulta_dos.executeQuery("select * from empresa where emp_rut = '"+rut_empresa+"' ");
			resultado_dos.next();
			
			empresa = new Empresa(rut_empresa,resultado_dos.getInt("com_id"),resultado_dos.getString("emp_nombre"),
					resultado_dos.getString("emp_direccion"),resultado_dos.getString("emp_telefono"),resultado_dos.getString("emp_celular"),
					resultado_dos.getString("emp_descripcion"),resultado_dos.getString("emp_email"),resultado_dos.getString("emp_web"));
			CerrarConexionBd();
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return empresa;
	}
	
	public ArrayList<Especialidad> getEspecilidades(String rut)
	{
		ConexionBd();
		try 
		{
			especialidades_academico = new ArrayList<EspecialidadAcademico>();
			resultado = consulta.executeQuery("select esp_id,aca_rut from especialidad_academico where aca_rut = '"+rut+"' ;");
			while(resultado.next())
			{
				System.out.println("esp_id "+resultado.getInt("esp_id")+" aca_rut "+resultado.getString("aca_rut"));
				especialidades_academico.add(new EspecialidadAcademico(resultado.getInt("esp_id"),resultado.getString("aca_rut")));	
			}
			CerrarConexionBd();
			
			especialidades = new ArrayList<Especialidad>();
			
			ConexionBd();
			for(int i=0;i<especialidades_academico.size();i++)
			{
				resultado = consulta.executeQuery("select esp_id,esp_nombre from especialidad where esp_id = "+especialidades_academico.get(i).getEsp_id()+" ;");
				resultado.next();
				especialidades.add(new Especialidad(resultado.getInt("esp_id"),resultado.getString("esp_nombre")));
			}
			CerrarConexionBd();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return especialidades;
	}
	public ArrayList<Especialidad> getEspecialidades()
	{
		ConexionBd();
		try 
		{
			especialidades = new ArrayList<Especialidad>();
			ConexionBd();
			resultado = consulta.executeQuery("select esp_id,esp_nombre from especialidad ;");
			while(resultado.next())
			{
				especialidades.add(new Especialidad(resultado.getInt("esp_id"),resultado.getString("esp_nombre")));
			}
			CerrarConexionBd();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return especialidades;
	}
	public Practica getPractica(int id)
	{
		ConexionBd();
		try 
		{
			resultado = consulta.executeQuery("select emp_rut,sup_rut,com_id,obr_id,alu_rut,asi_codigo,plan_id,ins_fecha,pra_obra_nombre, pra_fecha_ins,"
					+ "pra_direccion,pra_tareas,to_char( pra_fecha_ini,'dd-mm-yyyy') as inicio ,to_char( pra_fecha_fin,'dd-mm-yyyy') as termino,"
					+ "pra_horas,pra_informe,pra_estado, aca_rut "
					+ "from practica where pra_id = "+id+" ");
			resultado.next();
			pendiente = new Practica(id, resultado.getString("emp_rut"), resultado.getString("sup_rut"), resultado.getInt("com_id"),
					    resultado.getInt("obr_id"),resultado.getString("alu_rut"), resultado.getInt("asi_codigo"), resultado.getInt("plan_id"),
					    resultado.getString("ins_fecha"),resultado.getString("pra_obra_nombre"), resultado.getString("pra_fecha_ins"),resultado.getString("pra_direccion"),
					    resultado.getString("pra_tareas"),resultado.getString("inicio"), resultado.getString("termino"), 
					    resultado.getInt("pra_horas"), resultado.getString("pra_informe"), resultado.getString("pra_estado")
					    ,resultado.getString("aca_rut"));
			CerrarConexionBd();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return pendiente;
	}
	
	public ArrayList<Practica> getHistorial(String rut) {
		ConexionBd();
		try 
		{
			practicas = new ArrayList<Practica>();
			resultado = consulta.executeQuery("select p.pra_id,p.emp_rut || ' ' || e.emp_nombre as empresa,"
					+ "p.sup_rut|| ' ' ||s.sup_nombres|| ' ' ||s.sup_apellido_p|| ' ' ||s.sup_apellido_m as supervisor,"
					+ "p.obr_id,p.asi_codigo,p.pra_obra_nombre, "
					+ "p.pra_fecha_ini,p.pra_fecha_fin,p.pra_estado , p.pra_informe "
					+ "from practica p left join empresa e on p.emp_rut = e.emp_rut "
					+ "left join supervisor s on p.sup_rut = s.sup_rut where p.alu_rut = '"+rut+"' ");
			while(resultado.next())
			{
				practicas.add(new Practica(resultado.getInt("pra_id"),resultado.getString("empresa"), resultado.getString("supervisor"),
					    resultado.getInt("obr_id"), resultado.getInt("asi_codigo"),resultado.getString("pra_obra_nombre"),
					    resultado.getString("pra_fecha_ini"), resultado.getString("pra_fecha_fin"), resultado.getString("pra_informe"),
					    resultado.getString("pra_estado")));
			}
			
			CerrarConexionBd();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return practicas;
	}
	//muestra todas las practicas excepto las pendientes
	public ArrayList<Practica> getPracticas() {
		ConexionBd();
		try 
		{
			practicas = new ArrayList<Practica>();
			resultado = consulta.executeQuery("select p.pra_id, a.alu_nombres || ' ' || a.alu_apellido_p || ' '|| a.alu_apellido_m as alumno,"
					+ " p.ins_fecha,e.emp_nombre as empresa,p.pra_obra_nombre, p.pra_estado  "
					+ "from practica  as p left join alumno as a on p.alu_rut = a.alu_rut "
					+ "left join empresa as e on e.emp_rut = p.emp_rut where p.pra_estado <> 'Pendiente' ");
			while(resultado.next())
			{
				practicas.add(new Practica(resultado.getInt("pra_id"),resultado.getString("empresa"),resultado.getString("alumno"),
					    resultado.getString("ins_fecha"),resultado.getString("pra_obra_nombre"), 
					    resultado.getString("pra_estado")));
			}
			CerrarConexionBd();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return practicas;
	}
	public int getTotalPracticasSemestre(String rut) {
		ConexionBd();
		int total=0;
		try 
		{
			resultado = consulta.executeQuery("select count(*) as total from practica where alu_rut = '"+rut+"' and (pra_estado = 'Aceptada' or pra_estado = 'Finalizada' or pra_estado = 'Comisionada' or pra_estado = 'Pendiente')  ");
			resultado.next();
			total = resultado.getInt("total");
			CerrarConexionBd();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return total;
	}
	public ArrayList<Practica> getPracticasSemestre(String rut) {
		ConexionBd();
		try 
		{
			practicas = new ArrayList<Practica>();
			resultado = consulta.executeQuery("select p.pra_id, a.alu_nombres || ' ' || a.alu_apellido_p || ' '|| a.alu_apellido_m as alumno,"
					+ " p.ins_fecha,e.emp_nombre as empresa,p.pra_obra_nombre, p.pra_estado  "
					+ "from practica  as p left join alumno as a on p.alu_rut = a.alu_rut "
					+ "left join empresa as e on e.emp_rut = p.emp_rut where p.alu_rut = '"+rut+"'"
					+ " and (pra_estado <> 'Aceptada' or pra_estado <> 'Finalizada' or  pra_estado <> 'Comisionada') ");
			while(resultado.next())
			{
				practicas.add(new Practica(resultado.getInt("pra_id"),resultado.getString("empresa"),resultado.getString("alumno"),
					    resultado.getString("ins_fecha"),resultado.getString("pra_obra_nombre"), 
					    resultado.getString("pra_estado")));
			}
			CerrarConexionBd();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return practicas;
	}
	public ArrayList<Practica> getPracticasFinalizadas(String tipo) {
		ConexionBd();
		try 
		{
			practicas = new ArrayList<Practica>();
			if(tipo.equals("admin") || tipo.equals("administrador-academico"))//obtiene todas las prácticas de todos los alumnos y que esten en estados de finalizacion
			{						
				resultado = consulta.executeQuery("SELECT * "
						+ "FROM practica "
						+ "WHERE pra_estado = 'Finalizada' AND pra_informe IS NOT NULL");
						//+ "WHERE pra_estado = 'Finalizada' or pra_estado = 'Comisionada'");
						//descomentar para validar que el informe esté subido y se pueda asignar profesor lector
						//+ "WHERE pra_informe IS NOT NULL AND (pra_estado = 'Finalizada' or pra_estado = 'Comisionada')");
				while(resultado.next())
				{						
					practicas.add(
							new Practica(resultado.getInt("pra_id"),resultado.getString("emp_rut"),resultado.getString("sup_rut"),resultado.getInt("com_id"),
						    resultado.getInt("obr_id"),resultado.getString("alu_rut"),resultado.getString("aca_rut"),resultado.getInt("asi_codigo"),resultado.getInt("plan_id"),
						    resultado.getString("ins_fecha"),resultado.getString("pra_obra_nombre"),resultado.getString("pra_direccion"),resultado.getString("pra_tareas"),
						    resultado.getString("pra_fecha_ini"),resultado.getString("pra_fecha_fin"),resultado.getString("pra_fecha_ins"),resultado.getInt("pra_horas"),
						    resultado.getString("pra_informe"),resultado.getString("pra_estado"))
							);
				}
			}
			if(tipo.equals("supervisor"))
			{

			}
			if(tipo.equals("academico"))
			{

			}
			if(tipo.equals("alumno"))//obtiene todas las prácticas del alumno que se autentifica y que esten en estados de finalizacion
			{
				
			}
			CerrarConexionBd();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return practicas;
	}
	
	public ArrayList<Practica> getPracticas(String tipo) {//obtiene todas las prácticas de todos los alumnos y que esten en estados de finalizacion,comisionadas,evaluadas
		ConexionBd();
		try 
		{
			practicas = new ArrayList<Practica>();
			if(tipo.equals("admin") || tipo.equals("administrador-academico"))//obtiene todas las prácticas de todos los alumnos y que esten en estados de finalizacion
			{						
				resultado = consulta.executeQuery("SELECT * "
						+ "FROM practica "
						+ "WHERE pra_estado = 'Finalizada' OR pra_estado = 'Comisionada' OR pra_estado = 'Evaluada'");
						//+ "WHERE pra_nota IS NOT NULL AND (pra_estado = 'Finalizada' OR pra_estado = 'Comisionada' OR pra_estado = 'Evaluada')");
						//+ "WHERE pra_estado = 'Finalizada' or pra_estado = 'Comisionada'");
						//descomentar para validar que el informe esté subido y se pueda asignar profesor lector
						//+ "WHERE pra_informe IS NOT NULL AND (pra_estado = 'Finalizada' or pra_estado = 'Comisionada')");
				while(resultado.next())
				{						
					practicas.add(
							new Practica(resultado.getInt("pra_id"),resultado.getString("alu_rut"),resultado.getString("emp_rut"),resultado.getInt("com_id"),
								    resultado.getInt("obr_id"),resultado.getInt("asi_codigo"),resultado.getInt("plan_id"),
								    resultado.getString("pra_obra_nombre"),resultado.getString("pra_informe"),resultado.getDouble("pra_nota"),
								    resultado.getString("pra_estado"))
									);
				}
			}
			CerrarConexionBd();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return practicas;
	}
	
	public ArrayList<Practica> getPracticasSegunRut(String tipo, String rut) {
		ConexionBd();
		try 
		{
			practicas = new ArrayList<Practica>();
			if(tipo.equals("supervisor"))
			{
				resultado = consulta.executeQuery("select p.pra_id, a.alu_nombres || ' ' || a.alu_apellido_p || ' '|| a.alu_apellido_m as alumno,"
						+ " p.ins_fecha,e.emp_nombre as empresa,p.pra_obra_nombre, p.pra_estado,p.pra_informe  "
						+ "from practica  as p left join alumno as a on p.alu_rut = a.alu_rut "
						+ "left join empresa as e on e.emp_rut = p.emp_rut  where sup_rut = '"+rut+"' and (p.pra_estado = 'Aceptada' or p.pra_estado = 'Finalizada') ");
				while(resultado.next())
				{
					practicas.add(new Practica(resultado.getInt("pra_id"),resultado.getString("empresa"),resultado.getString("alumno"),
						    resultado.getString("ins_fecha"),resultado.getString("pra_obra_nombre"), 
						    resultado.getString("pra_estado"),resultado.getString("pra_informe")));
				}
			}
			if(tipo.equals("academico") || tipo.equals("administrador-academico"))
			{
				resultado = consulta.executeQuery("select p.pra_id, a.alu_nombres || ' ' || a.alu_apellido_p || ' '|| a.alu_apellido_m as alumno,"
						+ " p.ins_fecha,e.emp_nombre as empresa,p.pra_obra_nombre, p.pra_estado,p.pra_informe  "
						+ "from practica  as p left join alumno as a on p.alu_rut = a.alu_rut "
						+ "left join empresa as e on e.emp_rut = p.emp_rut  where aca_rut = '"+rut+"' and (p.pra_estado = 'Aceptada' or p.pra_estado = 'Finalizada') ");
				while(resultado.next())
				{
					practicas.add(new Practica(resultado.getInt("pra_id"),resultado.getString("empresa"),resultado.getString("alumno"),
						    resultado.getString("ins_fecha"),resultado.getString("pra_obra_nombre"), 
						    resultado.getString("pra_estado"),resultado.getString("pra_informe")));
				}
			}
			if(tipo.equals("alumno"))//obtiene todas las prácticas del alumno que se autentifica y que esten en estados de finalizacion
			{
				/*resultado = consulta.executeQuery("select p.pra_id, a.alu_nombres || ' ' || a.alu_apellido_p || ' '|| a.alu_apellido_m as alumno,"
						+ " p.ins_fecha,e.emp_nombre as empresa, p.asi_codigo ,p.pra_obra_nombre, p.pra_estado  "
						+ "from practica  as p left join alumno as a on p.alu_rut = a.alu_rut "
						+ "left join empresa as e on e.emp_rut = p.emp_rut"*/
						
				resultado = consulta.executeQuery("SELECT * "
						+ "FROM practica "						
						+ "where alu_rut = '"+rut+"' and (pra_estado <> 'Aceptada' or pra_estado <> 'Pendiente')");
				while(resultado.next())
				{					
					practicas.add(
							new Practica(resultado.getInt("pra_id"),resultado.getString("emp_rut"),resultado.getInt("com_id"),
						    resultado.getInt("obr_id"),resultado.getInt("asi_codigo"),resultado.getInt("plan_id"),
						    resultado.getString("pra_obra_nombre"),resultado.getString("pra_informe"),resultado.getDouble("pra_nota"),
						    resultado.getString("pra_estado"))
							);
				}
			}
			CerrarConexionBd();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return practicas;
	}
	
	public ArrayList<Area> getAreas()//obtiene todas las areas disponibles
	{
		ConexionBd();
		try 
		{
			areas = new ArrayList<Area>();
			resultado = consulta.executeQuery("SELECT * FROM area");
			while(resultado.next())
			{
				areas.add(new Area(resultado.getInt("area_id"),resultado.getString("area_nombre")));
			}
			CerrarConexionBd();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return areas;
	}
	
	public ArrayList<AreaPractica> getAreasPractica(int pra_id)// obtiene todas las areas seleccionadas por un alumno de una práctica
	{
		ConexionBd();
		try 
		{
			areas_practica = new ArrayList<AreaPractica>();
			resultado = consulta.executeQuery("select * from area_practica where pra_id = "+pra_id);
			while(resultado.next())
			{
				areas_practica.add(new AreaPractica(resultado.getInt("pra_id"),resultado.getInt("area_id")));
			}
			CerrarConexionBd();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return areas_practica;
	}
	
	public int agregarAreaPractica(int pra_id, int area_id)// inserta una nueva area de práctica al momento de la auto evaluacion
	{
		ConexionBd();
		try
		{
			ConexionBd();
			PreparedStatement enunciado = conexion.prepareStatement("insert into area_practica(pra_id,area_id) values(?,?)");
			enunciado.setInt(1, pra_id);
			enunciado.setInt(2, area_id);
			enunciado.execute();			
			conexion.close();
			return 1;
		}
		catch(SQLException e)
		{
			e.printStackTrace();
			return 2;
		}
	}
	
	public ArrayList<Funcion> getFunciones()//obtiene todas las funciones disponibles
	{
		ConexionBd();
		try 
		{
			funciones = new ArrayList<Funcion>();
			resultado = consulta.executeQuery("SELECT * FROM funcion");
			while(resultado.next())
			{
				funciones.add(new Funcion(resultado.getInt("fun_id"),resultado.getString("fun_nombre")));
			}
			CerrarConexionBd();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return funciones;
	}
	
	public ArrayList<FuncionPractica> getFuncionesPractica(int pra_id)// obtiene todas las funciones seleccionadas por un alumno de una práctica
	{
		ConexionBd();
		try 
		{
			funciones_practica = new ArrayList<FuncionPractica>();
			resultado = consulta.executeQuery("select * from funcion_practica where pra_id = "+pra_id);
			while(resultado.next())
			{
				funciones_practica.add(new FuncionPractica(resultado.getInt("pra_id"),resultado.getInt("fun_id")));
			}
			CerrarConexionBd();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return funciones_practica;
	}
	
	public int agregarFuncionPractica(int pra_id, int fun_id)// inserta una nueva area de práctica al momento de la auto evaluacion
	{
		ConexionBd();
		try
		{
			ConexionBd();
			PreparedStatement enunciado = conexion.prepareStatement("insert into funcion_practica(pra_id,fun_id) values(?,?)");
			enunciado.setInt(1, pra_id);
			enunciado.setInt(2, fun_id);
			enunciado.execute();			
			conexion.close();
			return 1;
		}
		catch(SQLException e)
		{
			e.printStackTrace();
			return 2;
		}
	}
	
	public ArrayList<Obra_tipo> getObras()
	{
		ConexionBd();
		try 
		{
			obra_tipos = new ArrayList<Obra_tipo>();
			resultado = consulta.executeQuery("select * from obra_tipo ");
			while(resultado.next())
			{
				obra_tipos.add(new Obra_tipo(resultado.getInt("obr_id"),resultado.getString("obr_nombre")));
			}
			CerrarConexionBd();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return obra_tipos;
	}
	public ArrayList<Carrera> getCarreras()
	{
		ConexionBd();
		try 
		{
			carreras = new ArrayList<Carrera>();
			resultado = consulta.executeQuery("select * from carrera");
			while(resultado.next())
			{
				carreras.add(new Carrera(resultado.getInt("car_codigo"),resultado.getString("car_nombre"),resultado.getString("car_sede")));
			}
			CerrarConexionBd();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return carreras;
	}
	public ArrayList<Plan_estudio> getPlanes()
	{
		ConexionBd();
		try 
		{
			planes = new ArrayList<Plan_estudio>();
			resultado = consulta.executeQuery("select * from plan_estudio ");
			while(resultado.next())
			{
				planes.add(new Plan_estudio(resultado.getInt("plan_id"),resultado.getInt("car_codigo"),
				           resultado.getString("plan_nombre"),resultado.getString("plan_fecha"),
				           resultado.getString("plan_estado"),resultado.getBoolean("plan_vigente")));
			}
			CerrarConexionBd();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return planes;
	}
	
	public ArrayList<Practica> getpendientes()
	{
		ConexionBd();
		try 
		{
			pendientes = new ArrayList<Practica>();
			resultado = consulta.executeQuery("select p.pra_id,p.alu_rut || ' '|| a.alu_nombres || ' ' || a.alu_apellido_p || ' '|| a.alu_apellido_m as alumno,"
					+ " p.ins_fecha,p.emp_rut || ' ' || e.emp_nombre as empresa,p.pra_obra_nombre, p.pra_estado  "
					+ "from practica  as p left join alumno as a on p.alu_rut = a.alu_rut "
					+ "left join empresa as e on e.emp_rut = p.emp_rut where pra_estado = 'Pendiente' ");
			while(resultado.next())
			{
				pendientes.add(new Practica(resultado.getInt("pra_id"),resultado.getString("empresa"),resultado.getString("alumno"),resultado.getString("ins_fecha"),resultado.getString("pra_obra_nombre"),resultado.getString("pra_estado")));
			}
			CerrarConexionBd();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return pendientes;
	}
	public ArrayList<Academico> getAcademicos()
	{
		ConexionBd();
		try 
		{
			academicos = new ArrayList<Academico>();
			resultado = consulta.executeQuery("select * from academico");
			while(resultado.next())
			{
				academicos.add(new Academico(resultado.getString("aca_rut"),Integer.parseInt(resultado.getString("dep_id")),resultado.getString("aca_password"),resultado.getString("aca_nombres"),resultado.getString("aca_apellido_p"),resultado.getString("aca_apellido_m"),resultado.getString("aca_email"),resultado.getString("aca_telefono"),resultado.getString("aca_celular"),resultado.getString("aca_estado")));
			}
			CerrarConexionBd();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return academicos;
	}
	
	public ArrayList<Academico> getAcademicosTotalEvaluaciones()//obtiene todos los academicos mas el total de evaluaiones activas(agregado en bean_uno)
	{
		ConexionBd();
		try 
		{
			academicos = new ArrayList<Academico>();
			resultado = consulta.executeQuery("select * from academico");
			while(resultado.next())
			{
				resultado_uno = consulta_dos.executeQuery("select count(*) as cuenta from evaluacion_academico where aca_rut = '"+resultado.getString("aca_rut")+"' and eva_estado = 'Sin Evaluar' and eva_nombre <> 'Profesor Guía' ");
				resultado_uno.next();
				academicos.add(new Academico(resultado.getString("aca_rut"),Integer.parseInt(resultado.getString("dep_id")),
											resultado.getString("aca_password"),resultado.getString("aca_nombres"),
											resultado.getString("aca_apellido_p"),resultado.getString("aca_apellido_m"),
											resultado.getString("aca_email"),resultado.getString("aca_telefono"),
											resultado.getString("aca_celular"),resultado.getString("aca_estado"),
											Integer.parseInt(resultado.getString("aca_n_especialidad")),Integer.parseInt(resultado_uno.getString("cuenta"))
											));
			}
			CerrarConexionBd();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return academicos;
	}
	
	public ArrayList<Asignatura> getAsignaturas()//obtiene solo asignaturas de tipo gestion operativa y profesional
	{
		ConexionBd();
		try 
		{
			instrumentos_asignatura = new ArrayList<Asignatura>();
			resultado = consulta.executeQuery("SELECT * FROM asignatura WHERE asi_tp_id = 4 OR asi_tp_id = 5");
			while(resultado.next())
			{	
				instrumentos_asignatura.add(new Asignatura(
															resultado.getInt("asi_codigo"), resultado.getInt("plan_id"), resultado.getInt("asi_tp_id"),
															resultado.getString("asi_nombre"), resultado.getInt("asi_horas"), resultado.getInt("asi_semestre"), resultado.getInt("asi_sct")
														  )
											);
			}
			CerrarConexionBd();
		}catch(SQLException e){
			e.printStackTrace();
		}	
		return instrumentos_asignatura;
	}
	
	public ArrayList<Asignatura> getAsignaturasTipoGestion(int car_codigo)//obtiene solo asignaturas de tipo gestion operativa y profesional, de una carrera
	{
		ConexionBd();
		try 
		{
			instrumentos_asignatura = new ArrayList<Asignatura>();
			resultado = consulta.executeQuery("SELECT A.asi_codigo, A.plan_id,  A.asi_tp_id, A.asi_nombre, A.asi_horas, A.asi_semestre, A.asi_sct "
											+ "FROM asignatura A, plan_estudio P "
											+ "WHERE (A.asi_tp_id = 4 OR A.asi_tp_id = 5) AND P.car_codigo = "+car_codigo);
			while(resultado.next())
			{	
				instrumentos_asignatura.add(new Asignatura(
															resultado.getInt("asi_codigo"), resultado.getInt("plan_id"), resultado.getInt("asi_tp_id"),
															resultado.getString("asi_nombre"), resultado.getInt("asi_horas"), resultado.getInt("asi_semestre"), resultado.getInt("asi_sct")
														  )
											);
			}
			CerrarConexionBd();
		}catch(SQLException e){
			e.printStackTrace();
		}	
		return instrumentos_asignatura;
	}
	/*public ArrayList<Asignatura> getAsignaturas(int plan_id)
	{
		ConexionBd();
		try 
		{
			instrumentos_asignatura = new ArrayList<Asignatura>();
			resultado = consulta.executeQuery("SELECT * FROM asignatura WHERE plan_id = "+plan_id+" ");
			while(resultado.next())
			{
				instrumentos_asignatura.add(new Asignatura(resultado.getInt("asi_codigo"),plan_id,resultado.getInt("asi_tp_id"),resultado.getString("asi_nombre")));
			}
			CerrarConexionBd();
		}catch(SQLException e){
			e.printStackTrace();
		}	
		return instrumentos_asignatura;
	}*/
	public ArrayList<Asignatura> getTodasAsignaturas()
	{
		ConexionBd();
		try 
		{
			instrumentos_asignatura = new ArrayList<Asignatura>();
			resultado = consulta.executeQuery("SELECT a.asi_codigo as codigo, a.asi_nombre as nombre, at.asi_tp_nombre as tipo, p.plan_nombre as plan FROM asignatura as a "
					+ "left join asignatura_tipo as at on at.asi_tp_id = a.asi_tp_id "
					+ "left join plan_estudio as p on p.plan_id = a.plan_id ");
			while(resultado.next())
			{
				instrumentos_asignatura.add(new Asignatura(resultado.getInt("codigo"),resultado.getString("nombre"), resultado.getString("tipo"), resultado.getString("plan")));
			}
			CerrarConexionBd();
		}catch(SQLException e){
			e.printStackTrace();
		}	
		return instrumentos_asignatura;
	}
	
	public ArrayList<AsignaturaTipo> getAsignaturasTipo()//obtiene todos los tipos de asignaturas
	{
		ConexionBd();
		try 
		{
			tipos_asignatura = new ArrayList<AsignaturaTipo>();
			resultado = consulta.executeQuery("SELECT * FROM asignatura_tipo");
			while(resultado.next())
			{
				tipos_asignatura.add(new AsignaturaTipo(resultado.getInt("asi_tp_id"),resultado.getString("asi_tp_nombre")));
			}
			CerrarConexionBd();
		}catch(SQLException e){
			e.printStackTrace();
		}	
		return tipos_asignatura;
	}
	public ArrayList<Asignatura> getAsignaturasTipoPractica()//obtiene todos las asignatura de tipo practica
	{
		ConexionBd();
		try 
		{
			gestiones = new ArrayList<Asignatura>();
			resultado = consulta.executeQuery("select a.asi_codigo as codigo,a.asi_nombre as nombre, p.plan_nombre as plan from asignatura as a "
					+ "left join plan_estudio as p on a.plan_id = p.plan_id "
					+ "where a.asi_tp_id = 4 or a.asi_tp_id = 5 order by plan asc");
			while(resultado.next())
			{
				gestiones.add(new Asignatura(resultado.getInt("codigo"),resultado.getString("nombre"),resultado.getString("plan")));
			}
			CerrarConexionBd();
		}catch(SQLException e){
			e.printStackTrace();
		}	
		return gestiones;
	}
	public ArrayList<RolGestion> getRoles(int codigo_asignatura, int plan)
	{
		ConexionBd();
		ArrayList<RolGestion> roles = new ArrayList<RolGestion>();
		try 
		{
			resultado = consulta.executeQuery("SELECT * FROM ROL_GESTION WHERE ASI_CODIGO = "+codigo_asignatura+" and plan_id = "+plan+" ");
			while(resultado.next())
			{
				//System.out.println("asi "+resultado.getInt("asi_codigo")+" "+resultado.getInt("plan_id")+" "+resultado.getInt("rol_id"));
				roles.add(new RolGestion(resultado.getInt("asi_codigo"),resultado.getInt("plan_id"),resultado.getInt("rol_id"),resultado.getDouble("rol_ponderacion"),resultado.getString("rol_nombre")));
			}			
			CerrarConexionBd();
		}catch(SQLException e){
			e.printStackTrace();
		}	
		return roles;
	}
	
	public ArrayList<RolGestion> getRoles()//Obtiene todos los roles registrados
	{
		ConexionBd();
		ArrayList<RolGestion> roles = new ArrayList<RolGestion>();
		try 
		{
			resultado = consulta.executeQuery("SELECT * FROM rol_gestion");
			while(resultado.next())
			{
				System.out.println("asi "+resultado.getInt("asi_codigo")+" "+resultado.getInt("plan_id")+" "+resultado.getInt("rol_id"));
				roles.add(new RolGestion(resultado.getInt("asi_codigo"),resultado.getInt("plan_id"),resultado.getInt("rol_id"),resultado.getDouble("rol_ponderacion"),resultado.getString("rol_nombre")));
			}			
			CerrarConexionBd();
		}catch(SQLException e){
			e.printStackTrace();
		}	
		return roles;
	}
	
	public String getParticipante (int rol_id)
	{
		ConexionBd();
		//ArrayList<RolTipoParticipantes> roles_tipo = new ArrayList<RolTipoParticipantes>();
		try 
		{
			resultado = consulta.executeQuery("SELECT * FROM ROL_TIPO_PARTICIPANTES WHERE ROL_ID = "+rol_id+" ");
			resultado.next();
			tipo=  new RolTipoParticipantes(resultado.getInt("rol_id"),resultado.getString("rol_descripcion"));
			CerrarConexionBd();
		}catch(SQLException e){
			e.printStackTrace();
		}	
		return tipo.getRol_descripcion();
	}
	
	public void updatePracticaComisionada(int pra_id)//una vez que se han asignado los profesores evaluadores cambia el estado de la practica
	{
		ConexionBd();
		try 
		{
			PreparedStatement enunciado = conexion.prepareStatement("UPDATE practica set pra_estado = 'Comisionada' WHERE pra_id = "+pra_id);
			enunciado.execute();
			CerrarConexionBd();
		}catch(SQLException e){
			e.printStackTrace();
		}	
	}
	
	public ArrayList<Supervisor> getSupervisores()
	{
		ConexionBd();
		try 
		{
			supervisores = new ArrayList<Supervisor>();
			resultado = consulta.executeQuery("select * from supervisor");
			while(resultado.next())
			{
				supervisores.add(new Supervisor(resultado.getString("sup_rut"),resultado.getString("sup_password"),resultado.getString("sup_nombres"),resultado.getString("sup_apellido_p"),resultado.getString("sup_apellido_m"),resultado.getString("sup_email"),resultado.getString("sup_telefono"),resultado.getString("sup_celular"),resultado.getString("sup_profesion"),resultado.getString("sup_cargo"),resultado.getString("sup_estado")));
			}
			CerrarConexionBd();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return supervisores;
	}
	
	public ArrayList<Supervisor> getSupervisores(String emp_rut)//obtiene todos los supervisores segun la empresa
	{
		ConexionBd();
		try 
		{
			supervisores = new ArrayList<Supervisor>();
			resultado = consulta.executeQuery("select * "
											+ "from supervisor S, historial_supervisor H "
											+ "where S.sup_rut = H.sup_rut and H.emp_rut = '"+emp_rut+"'");
			while(resultado.next())
			{
				supervisores.add(new Supervisor(resultado.getString("sup_rut"),resultado.getString("sup_password"),resultado.getString("sup_nombres"),resultado.getString("sup_apellido_p"),resultado.getString("sup_apellido_m"),resultado.getString("sup_email"),resultado.getString("sup_telefono"),resultado.getString("sup_celular"),resultado.getString("sup_profesion"),resultado.getString("sup_cargo"),resultado.getString("sup_estado")));
			}
			CerrarConexionBd();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return supervisores;
	}
	
	public ArrayList<Administrador> getAdministradores()
	{
		ConexionBd();
		try 
		{
			administradores = new ArrayList<Administrador>();
			resultado = consulta.executeQuery("select * from administrador ");
			while(resultado.next())
			{
				administradores.add(new Administrador(resultado.getString("adm_rut"),resultado.getString("adm_password"),resultado.getString("adm_nombres"),resultado.getString("adm_apellido_p"),resultado.getString("adm_apellido_m"),resultado.getString("adm_email"),resultado.getString("adm_telefono"),resultado.getString("adm_celular"),resultado.getString("adm_cargo"),resultado.getString("adm_estado")));
			}
			CerrarConexionBd();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return administradores;
	}
	
	public ArrayList<Region> getRegiones()
	{
		ConexionBd();
		try {
			regiones = new ArrayList<Region>();
			resultado = consulta.executeQuery("select * from region");
			while(resultado.next())
			{
				regiones.add(new Region(resultado.getInt("reg_id"),resultado.getInt("pais_id"),resultado.getString("reg_nombre"), resultado.getString("reg_simbolo")));
			}
			CerrarConexionBd();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return regiones;
	}
	
	public ArrayList<Provincia> getProvincias()
	{
		ConexionBd();
		try {
			provincias = new ArrayList<Provincia>();
			resultado = consulta.executeQuery("select * from provincia");
			while(resultado.next())
			{
				provincias.add(new Provincia(resultado.getInt("pro_id"),resultado.getInt("reg_id"),resultado.getString("pro_nombre")));
			}
			CerrarConexionBd();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return provincias;
	}
	
	public ArrayList<Provincia> getProvincias(String reg_id)//obtiene las comunas segun region
	{
		ConexionBd();
		try {
			provincias = new ArrayList<Provincia>();
			resultado = consulta.executeQuery("select * from provincia where reg_id = "+Integer.parseInt(reg_id));
			while(resultado.next())
			{
				provincias.add(new Provincia(resultado.getInt("pro_id"),resultado.getInt("reg_id"),resultado.getString("pro_nombre")));
			}
			CerrarConexionBd();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return provincias;
	}
	
	public ArrayList<Comuna> getComunas()
	{
		ConexionBd();
		try {
			comunas = new ArrayList<Comuna>();
			resultado = consulta.executeQuery("select * from comuna");
			while(resultado.next())
			{
				comunas.add(new Comuna(resultado.getInt("com_id"),resultado.getInt("pro_id"),resultado.getString("com_nombre")));
			}
			CerrarConexionBd();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return comunas;
	}
	
	public ArrayList<Comuna> getComunas(String pro_id)//obtiene todas las comunas segun una provincia
	{
		ConexionBd();
		try {
			comunas = new ArrayList<Comuna>();
			resultado = consulta.executeQuery("select * from comuna where pro_id = "+pro_id);
			while(resultado.next())
			{
				comunas.add(new Comuna(resultado.getInt("com_id"),resultado.getInt("pro_id"),resultado.getString("com_nombre")));
			}
			CerrarConexionBd();
		} catch (Exception e) {
			e.printStackTrace();

		}
		return comunas;
	}
	
	public ArrayList<Departamento> getDepartamentos()
	{
		ConexionBd();
		try{
			departamentos = new ArrayList<Departamento>();
			resultado = consulta.executeQuery("select * from departamento");
			while(resultado.next())
			{
				departamentos.add(new Departamento(resultado.getInt("dep_id"),resultado.getString("dep_nombre")));
			}	
			CerrarConexionBd();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return departamentos;
	}
	public void eliminarEmpresa(String rut) 
	{
		ConexionBd();
		try {
			Statement s = conexion.createStatement();
			s.executeUpdate("delete from empresa where emp_rut ='"+rut+"'");
			CerrarConexionBd();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void eliminarAcademico(String rut) 
	{
		ConexionBd();
		try {
			Statement s = conexion.createStatement();
			s.executeUpdate("delete from academico where aca_rut ='"+rut+"'");
			CerrarConexionBd();
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}
	public void eliminarEvaluacion(String rut, int pra_id ,int rol_id, int opcion) 
	{
		ConexionBd();
		try {
			if(opcion==1)
			{
				Statement s = conexion.createStatement();
				s.executeUpdate("delete from evaluacion_academico where aca_rut ='"+rut+"' and pra_id = "+pra_id+" and rol_id="+rol_id+" ");
			}
			if(opcion==2)
			{
				Statement s = conexion.createStatement();
				s.executeUpdate("delete from evaluacion_supervisor where sup_rut ='"+rut+"' and pra_id = "+pra_id+" and rol_id="+rol_id+" ");
			}
			if(opcion==3)
			{
				Statement s = conexion.createStatement();
				s.executeUpdate("delete from evaluacion_alumno where alu_rut ='"+rut+"' and pra_id = "+pra_id+" and rol_id="+rol_id+" ");
			}
			CerrarConexionBd();
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}
	public void eliminarSupervisor(String rut) 
	{
		ConexionBd();
		try {
			Statement s = conexion.createStatement();
			s.executeUpdate("delete from supervisor where sup_rut ='"+rut+"'");
			CerrarConexionBd();
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}
	public void eliminarAdministrador(String rut) 
	{
		ConexionBd();
		try {
			Statement s = conexion.createStatement();
			s.executeUpdate("delete from administrador where adm_rut ='"+rut+"'");
			CerrarConexionBd();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}
	public int ModificarPractica(Practica practica, int opcion) {
		try {
			ConexionBd();
			if(opcion == 1)
			{
				PreparedStatement enunciado = conexion.prepareStatement(" update practica set emp_rut= '"+practica.getEmp_rut()+"', aca_rut = '"+practica.getAca_rut()+"' , sup_rut='"+practica.getSup_rut()+"', "
						+ "com_id="+practica.getCom_id()+", obr_id= "+practica.getObr_id()+", asi_codigo= "+practica.getAsi_codigo()+", pra_obra_nombre= '"+practica.getPra_obra_nombre()+"', "
						+ "pra_direccion= '"+practica.getPra_direccion()+"', pra_tareas= '"+practica.getPra_tareas()+"', pra_fecha_ini= CAST('"+practica.getPra_fecha_ini()+"' AS DATE), "
						+ "pra_fecha_fin = CAST('"+practica.getPra_fecha_fin()+"' AS DATE), pra_horas= "+practica.getPra_horas()+" where pra_id= "+practica.getPra_id()+" ;");
				enunciado.execute();	
			}
			if(opcion == 2)
			{
				PreparedStatement enunciado = conexion.prepareStatement(" update practica set emp_rut= '"+practica.getEmp_rut()+"', aca_rut = '"+practica.getAca_rut()+"' , sup_rut='"+practica.getSup_rut()+"', "
						+ "com_id="+practica.getCom_id()+", obr_id= "+practica.getObr_id()+", asi_codigo= "+practica.getAsi_codigo()+", pra_obra_nombre= '"+practica.getPra_obra_nombre()+"', "
						+ "pra_direccion= '"+practica.getPra_direccion()+"', pra_tareas= '"+practica.getPra_tareas()+"', pra_fecha_ini= CAST('"+practica.getPra_fecha_ini()+"' AS DATE), "
						+ "pra_fecha_fin = CAST('"+practica.getPra_fecha_fin()+"' AS DATE), pra_horas= "+practica.getPra_horas()+" , pra_estado= '"+practica.getPra_estado()+"' where pra_id= "+practica.getPra_id()+" ;");
				enunciado.execute();
			}
			
			conexion.close();
			return 1;
		} catch (SQLException e) {
			e.printStackTrace();
			return 2;
		}
	}
	public int ModificarInformePractica(int pra_id, String url) {
		try {
			ConexionBd();
			PreparedStatement enunciado = conexion.prepareStatement(" update practica set pra_informe= '"+url+"' where pra_id= "+pra_id+" ;");
			enunciado.execute();			
			conexion.close();
			return 1;
		} catch (SQLException e) {
			e.printStackTrace();
			return 2;
		}
	}
	public int ModificarDatosAlumno(Alumno alumno) {
		try {
			ConexionBd();
			PreparedStatement enunciado = conexion.prepareStatement("update alumno set alu_celular ='"+alumno.getAlu_celular()+"',"
					+ " alu_telefono = '"+alumno.getAlu_telefono()+"' where alu_rut= '"+alumno.getAlu_rut()+"' ;");
			enunciado.execute();			
			conexion.close();
			return 1;
		} catch (SQLException e) {
			e.printStackTrace();
			return 2;
		}
	}
	public int contarDirecciones(String rut)
	{
		try {
			ConexionBd();
			resultado = consulta.executeQuery("select count(*) as contador from direccion where alu_rut = '"+rut+"' ");
			resultado.next();
			int contador = resultado.getInt("contador");
			conexion.close();
			return contador;
		}catch (SQLException e) {
			e.printStackTrace();
			return 0;
		}
	}
	public int contarRegion(String estado,int codigo)
	{
		try {
			ConexionBd();
			if(estado.equals("activa"))
			{
				resultado = consulta.executeQuery("select count(*) as contador from practica as p "
						+ "left join comuna as c on c.com_id = p.com_id "
						+ "left join provincia as pr on pr.pro_id = c.pro_id "
						+ "left join region as r on r.reg_id = pr.reg_id "
						+ "where r.reg_id = "+codigo+" and (p.pra_estado = 'Aceptada' or p.pra_estado = 'Finalizada' or "
						+ "p.pra_estado = 'Evaluada' or p.pra_estado = 'Comisionada')");
			}
			if(estado.equals("inactiva"))
			{
				resultado = consulta.executeQuery("select count(*) as contador from practica as p "
						+ "left join comuna as c on c.com_id = p.com_id "
						+ "left join provincia as pr on pr.pro_id = c.pro_id "
						+ "left join region as r on r.reg_id = pr.reg_id "
						+ "where r.reg_id = "+codigo+" and (p.pra_estado = 'Aprobada' or p.pra_estado = 'Reprobada' ) ");
			}
			
			resultado.next();
			int contador = resultado.getInt("contador");
			conexion.close();
			return contador;
		}catch (SQLException e) {
			e.printStackTrace();
			return 0;
		}
	}
	public int contarAlumnosEnGestiones(int asi_codigo, String estado)
	{
		try {
			ConexionBd();
			if(estado.equals("activa"))
			{
				resultado = consulta.executeQuery("select count(*) as contador from practica as p where p.asi_codigo = "+asi_codigo+" "
						+ "and (p.pra_estado = 'Aceptada' or p.pra_estado = 'Finalizada' or "
						+ "p.pra_estado = 'Evaluada' or p.pra_estado = 'Comisionada') ");
			}
			if(estado.equals("inactiva"))
			{
				resultado = consulta.executeQuery("select count(*) as contador from practica as p where p.asi_codigo = "+asi_codigo+" "
						+ "and (p.pra_estado = 'Aprobada' or p.pra_estado = 'Reprobada') ");
			}
			resultado.next();
			int contador = resultado.getInt("contador");
			conexion.close();
			return contador;
		}catch (SQLException e) {
			e.printStackTrace();
			return 0;
		}
	}
	public String getInfoNotas(int tipo, int agnio,int semestre,int asi_codigo)
	{
		try {
			ConexionBd();
			if(tipo==1)
			{
				if(semestre == 1)
				{
					resultado = consulta.executeQuery("select avg(p.pra_nota) as nota from practica as p where "
							+ "extract(year from p.pra_fecha_ins) ="+agnio+" and extract(month from p.pra_fecha_ins) <= 7 "
							+ "and asi_codigo = "+asi_codigo+" and (p.pra_estado = 'Aprobada' or p.pra_estado = 'Reprobada') ");
				}
				if(semestre == 2)
				{
					resultado = consulta.executeQuery("select avg(p.pra_nota) as nota from practica as p where "
							+ "extract(year from p.pra_fecha_ins) ="+agnio+" and extract(month from p.pra_fecha_ins) > 7 "
							+ "and asi_codigo = "+asi_codigo+" and (p.pra_estado = 'Aprobada' or p.pra_estado = 'Reprobada') ");
				}
			}
			if(tipo==2)
			{
				if(semestre == 1)
				{
					resultado = consulta.executeQuery("select max(p.pra_nota) as nota from practica as p where "
							+ "extract(year from p.pra_fecha_ins) ="+agnio+" and extract(month from p.pra_fecha_ins) <= 7 "
							+ "and asi_codigo = "+asi_codigo+" and (p.pra_estado = 'Aprobada' or p.pra_estado = 'Reprobada') ");
				}
				if(semestre == 2)
				{
					resultado = consulta.executeQuery("select max(p.pra_nota) as nota from practica as p where "
							+ "extract(year from p.pra_fecha_ins) ="+agnio+" and extract(month from p.pra_fecha_ins) > 7 "
							+ "and asi_codigo = "+asi_codigo+" and (p.pra_estado = 'Aprobada' or p.pra_estado = 'Reprobada') ");
				}
			}
			if(tipo==3)
			{
				if(semestre == 1)
				{
					resultado = consulta.executeQuery("select min(p.pra_nota) as nota from practica as p where "
							+ "extract(year from p.pra_fecha_ins) ="+agnio+" and extract(month from p.pra_fecha_ins) <= 7 "
							+ "and asi_codigo = "+asi_codigo+" and (p.pra_estado = 'Aprobada' or p.pra_estado = 'Reprobada') ");
				}
				if(semestre == 2)
				{
					resultado = consulta.executeQuery("select min(p.pra_nota) as nota from practica as p where "
							+ "extract(year from p.pra_fecha_ins) ="+agnio+" and extract(month from p.pra_fecha_ins) > 7 "
							+ "and asi_codigo = "+asi_codigo+" and (p.pra_estado = 'Aprobada' or p.pra_estado = 'Reprobada') ");
				}
			}
			resultado.next();
			String nota = resultado.getString("nota");
			conexion.close();
			if(nota == null)
			{
				nota = "Sin Resultados";
			}
			return nota;
		}catch (SQLException e) {
			e.printStackTrace();
			return "vacio";
		}
	}
	
	public int ModificarDireccionesAlumno(String rut,String direccion, int cod_comuna, int tipo) {
		try {
			int contador = contarDirecciones(rut);
			if(contador == 0)
			{
				int posicion = direccion.indexOf("#");
				String calle = "";
				String numero = "";
				String sector = "";
				for(int i=0;i<posicion;i++)
				{
					calle=calle+""+direccion.charAt(i);
				}
				int ban=1;
				for(int j=posicion+1;j<direccion.length()&&ban==1;j++)
				{
					if(!Character.isWhitespace(direccion.charAt(j)))
					{
						numero=numero+""+direccion.charAt(j);
					}else{
						posicion=j;
						ban=0;
					}
				}
				System.out.println("posicion "+posicion);
				for(int k=posicion+1;k<direccion.length();k++)
				{
					if(!Character.isWhitespace(direccion.charAt(k)) && direccion.charAt(k) != '-')
					{
						sector=sector+""+direccion.charAt(k);
					}
				}
				ConexionBd();
				PreparedStatement enunciado_dos = conexion.prepareStatement("insert into direccion values('"+rut+"',"+cod_comuna+","+tipo+",'"+calle.replaceAll("\\s+","")+"',"+numero+", '"+sector.replaceAll("\\s+"," ")+"');");
				enunciado_dos.execute();			
				conexion.close();
			}
			if(contador == 1)
			{
				ArrayList<Direccion> direcciones = new Conexion().getDirecciones(rut);
				if(direcciones.get(0).getDir_tp_id() == tipo)
				{
					int posicion = direccion.indexOf("#");
					String calle = "";
					String numero = "";
					String sector = "";
					for(int i=0;i<posicion-1;i++)
					{
						calle=calle+""+direccion.charAt(i);
					}
					for(int i=posicion+1;i<direccion.length();i++)
					{
						if(direccion.charAt(i) != ' ')
						{
							numero=numero+""+direccion.charAt(i);
						}
					}
					ConexionBd();
					PreparedStatement enunciado_dos = conexion.prepareStatement("update direccion set com_id = "+cod_comuna+", "
							+ "  dir_calle = '"+calle.replaceAll("\\s+"," ")+"', dir_numero = "+numero+", dir_sector='"+sector.replaceAll("\\s+"," ")+"' where alu_rut = '"+rut+"' and dir_tp_id = "+tipo+";");
					enunciado_dos.execute();			
					conexion.close();
				}else{
					int posicion = direccion.indexOf("#");
					String calle = "";
					String numero = "";
					String sector = "";
					for(int i=0;i<posicion;i++)
					{
						calle=calle+""+direccion.charAt(i);
					}
					System.out.println(direccion);
					System.out.println(calle);
					int ban=1;
					for(int j=posicion+1;j<direccion.length()&&ban==1;j++)
					{
						if(!Character.isWhitespace(direccion.charAt(j)))
						{
							numero=numero+""+direccion.charAt(j);
						}else{
							posicion=j;
							ban=0;
						}
					}
					System.out.println("posicion "+posicion);
					for(int k=posicion+1;k<direccion.length();k++)
					{
						if(!Character.isWhitespace(direccion.charAt(k)) && direccion.charAt(k) != '-')
						{
							sector=sector+""+direccion.charAt(k);
						}
					}
					System.out.println(numero);
					System.out.println(sector);
					
					ConexionBd();
					PreparedStatement enunciado_dos = conexion.prepareStatement("insert into direccion values('"+rut+"',"+cod_comuna+","+tipo+",'"+calle.replaceAll("\\s+"," ")+"',"+numero+", '"+sector.replaceAll("\\s+"," ")+"');");
					enunciado_dos.execute();			
					conexion.close();
				}
			}
			if(contador == 2)
			{
				ArrayList<Direccion> direcciones = new Conexion().getDirecciones(rut);
				for(int i=0; i<direcciones.size();i++)
				{
					if(direcciones.get(i).getDir_tp_id() == tipo)
					{
						int posicion = direccion.indexOf("#");
						String calle = "";
						String numero = "";
						String sector = "";
						for(int j=0;j<posicion-1;j++)
						{
							calle=calle+""+direccion.charAt(j);
						}
						System.out.println(direccion);
						System.out.println(calle);
						int ban=1;
						for(int j=posicion+1;j<direccion.length()&&ban==1;j++)
						{
							if(!Character.isWhitespace(direccion.charAt(j)))
							{
								numero=numero+""+direccion.charAt(j);
							}else{
								posicion=j;
								ban=0;
							}
						}
						System.out.println("posicion "+posicion);
						for(int k=posicion+1;k<direccion.length();k++)
						{
							if(!Character.isWhitespace(direccion.charAt(k)) && direccion.charAt(k) != '-')
							{
								sector=sector+""+direccion.charAt(k);
							}
						}
						System.out.println(numero);
						System.out.println(sector);
						
						ConexionBd();
						PreparedStatement enunciado_dos = conexion.prepareStatement("update direccion set com_id = "+cod_comuna+", "
								+ " dir_calle = '"+calle.replaceAll("\\s+"," ")+"', dir_numero = "+numero+" , dir_sector='"+sector.replaceAll("\\s+"," ")+"' where alu_rut = '"+rut+"' and dir_tp_id = "+tipo+" ;");
						enunciado_dos.execute();			
						conexion.close();
					}
				}
			}
			return 1;
		} catch (SQLException e) {
			e.printStackTrace();
			return 2;
		}
	}
	// Modifica datos de un academico recibiendo los datos de un academico
	public int ModificarDatosAcademico(Academico academico, int opcion) {
		try {
			ConexionBd();
			if(opcion == 1)
			{
				PreparedStatement enunciado = conexion.prepareStatement("update academico set  aca_nombres = '"+academico.getAca_nombres()+"' ,"
						  + " aca_apellido_p = '"+academico.getAca_apellido_p()+"' , aca_apellido_m = '"+academico.getAca_apellido_m()+"' , "
						  + "aca_email = '"+academico.getAca_email()+"' , aca_telefono = '"+academico.getAca_telefono()+"' , "
						  + "aca_celular = '"+academico.getAca_celular()+"' , aca_estado = '"+academico.getAca_estado()+"' "
						  + "where aca_rut= '"+academico.getAca_rut()+"' ;");
				enunciado.execute();	
			}
			if(opcion == 2)
			{
				PreparedStatement enunciado = conexion.prepareStatement("update academico set aca_telefono = '"+academico.getAca_telefono()+"' , "
						  + "aca_celular = '"+academico.getAca_celular()+"' "
						  + "where aca_rut= '"+academico.getAca_rut()+"' ;");
				enunciado.execute();	
			}
			conexion.close();
			return 1;
		} catch (SQLException e) {
			e.printStackTrace();
			return 2;
		}
	}
	// Modifica datos de un administardor recibiendo los datos de un admin , segun su rut
	public int ModificarDatosAdministrador(Administrador administrador, int opcion) {
		try {
			ConexionBd();
			if(opcion == 1)
			{
				PreparedStatement enunciado = conexion.prepareStatement("update administrador set adm_nombres = '"+administrador.getAdm_nombres()+"', "
						+ "adm_apellido_p = '"+administrador.getAdm_apellido_p()+"' , adm_apellido_m = '"+administrador.getAdm_apellido_m()+"' , "
						+ "adm_telefono = '"+administrador.getAdm_telefono()+"' , adm_email = '"+administrador.getAdm_email()+"' , "
						+ "adm_celular = '"+administrador.getAdm_celular()+"' , "
						+ "adm_estado = '"+administrador.getAdm_estado()+"' "
						+ " where adm_rut= '"+administrador.getAdm_rut()+"' ;");
				enunciado.execute();	
			}
			if(opcion == 2)
			{
				PreparedStatement enunciado = conexion.prepareStatement("update administrador set adm_telefono = '"+administrador.getAdm_telefono()+"', "
						+ "adm_celular = '"+administrador.getAdm_celular()+"'  where adm_rut= '"+administrador.getAdm_rut()+"' ;");
				enunciado.execute();	
			}
			conexion.close();
			return 1;
		} catch (SQLException e) {
			e.printStackTrace();
			return 2;
		}
	}
	public int ModificarDatosPlan(Plan_estudio plan) {
		try {
			ConexionBd();
			PreparedStatement enunciado = conexion.prepareStatement("update plan_estudio set car_codigo = "+plan.getCar_codigo()+" "
					+ ", plan_nombre = '"+plan.getPlan_nombre()+"' , plan_estado = '"+plan.getPlan_estado()+"' , plan_vigente = "+plan.isPlan_vigente()+" "
					+ "  where plan_id= "+plan.getPlan_id()+" ;");
			enunciado.execute();			
			conexion.close();
			return 1;
		} catch (SQLException e) {
			e.printStackTrace();
			return 2;
		}
	}
	//modifica datos de una especialidad
	public int ModificarDatosEspecialidad(Especialidad especialidad) {
		try {
			ConexionBd();
			PreparedStatement enunciado = conexion.prepareStatement("update especialidad set esp_nombre = '"+especialidad.getEsp_nombre()+"' "
					+ "where esp_id= "+especialidad.getEsp_id()+" ;");
			enunciado.execute();			
			conexion.close();
			return 1;
		} catch (SQLException e) {
			e.printStackTrace();
			return 2;
		}
	}
	//modifica datos de un area
	public int ModificarDatosArea(Area area) {
		try {
			ConexionBd();
			PreparedStatement enunciado = conexion.prepareStatement("update area set area_nombre = '"+area.getArea_nombre()+"' "
					+ "where area_id= "+area.getArea_id()+" ;");
			enunciado.execute();			
			conexion.close();
			return 1;
		} catch (SQLException e) {
			e.printStackTrace();
			return 2;
		}
	}
	//modifica datos de una funcion
	public int ModificarDatosFuncion(Funcion funcion) {
		try {
			ConexionBd();
			PreparedStatement enunciado = conexion.prepareStatement("update funcion set fun_nombre = '"+funcion.getFun_nombre()+"' "
					+ "where fun_id= "+funcion.getFun_id()+" ;");
			enunciado.execute();			
			conexion.close();
			return 1;
		} catch (SQLException e) {
			e.printStackTrace();
			return 2;
		}
	}
	// Modifica datos de una empresa recibiendo los datos de una empresa , segun su rut
	public int ModificarDatosEmpresa(Empresa empresa) {
		try {
			ConexionBd();
			PreparedStatement enunciado = conexion.prepareStatement("update empresa set emp_nombre = '"+empresa.getEmp_nombre()+"' ,"
					  + " emp_direccion = '"+empresa.getEmp_direccion()+"' , emp_email = '"+empresa.getEmp_email()+"' , "
					  + "emp_telefono = '"+empresa.getEmp_telefono()+"' , emp_celular = '"+empresa.getEmp_celular()+"' ,"
					  + " emp_descripcion = '"+empresa.getEmp_descripcion()+"', emp_web = '"+empresa.getEmp_web()+"' ,"
					  + " com_id = "+empresa.getCom_id()+" "
					  + "where emp_rut= '"+empresa.getEmp_rut()+"' ;");
			enunciado.execute();	
			conexion.close();
			return 1;
		} catch (SQLException e) {
			e.printStackTrace();
			return 2;
		}
	}
	// Modifica datos de un administrador recibiendo los datos de un supervisor , segun su rut
	public int ModificarDatosSupervisor(Supervisor supervisor, int valor) {
		try {
			ConexionBd();
			if(valor == 1)
			{
				PreparedStatement enunciado = conexion.prepareStatement("update supervisor set  sup_nombres = '"+supervisor.getSup_nombres()+"' ,"
						  + " sup_apellido_p = '"+supervisor.getSup_apellido_p()+"' , sup_apellido_m = '"+supervisor.getSup_apellido_m()+"' , "
						  + "sup_email = '"+supervisor.getSup_email()+"' , sup_telefono = '"+supervisor.getSup_telefono()+"' , "
						  + "sup_celular = '"+supervisor.getSup_celular()+"' , sup_estado = '"+supervisor.getSup_estado()+"' , "
						  + "sup_profesion = '"+supervisor.getSup_profesion()+"' , sup_cargo = '"+supervisor.getSup_cargo()+"' "
						  + "where sup_rut= '"+supervisor.getSup_rut()+"' ;");
				enunciado.execute();
			}
			if(valor == 2)
			{
				PreparedStatement enunciado = conexion.prepareStatement("update supervisor set sup_telefono = '"+supervisor.getSup_telefono()+"' , "
						  + "sup_celular = '"+supervisor.getSup_celular()+"' , "
						  + "sup_profesion = '"+supervisor.getSup_profesion()+"' , sup_cargo = '"+supervisor.getSup_cargo()+"' "
						  + "where sup_rut= '"+supervisor.getSup_rut()+"' ;");
				enunciado.execute();
			}
			conexion.close();
			return 1;
		} catch (SQLException e) {
			e.printStackTrace();
			return 2;
		}
	}
	public int modificarEvaluador(String profesor_actual,String profesor_nuevo,int pra_id,String fecha,int rol ) {
		try {
			ConexionBd();
			PreparedStatement enunciado = conexion.prepareStatement("update evaluacion_academico set aca_rut = '"+profesor_nuevo+"' "
					  + "where pra_id = "+pra_id+" and aca_rut = '"+profesor_actual+"' and eva_fecha = '"+fecha+"' and rol_id = "+rol+"  ;");
			enunciado.execute();			
			conexion.close();
			return 1;
		} catch (SQLException e) {
			e.printStackTrace();
			return 2;
		}
	}
	public int escribirBitacora(Bitacora bitacora) 
	{
		try {
			ConexionBd();

			PreparedStatement enunciado = conexion.prepareStatement("update bitacora set bit_descripcion = '"+bitacora.getBit_descripcion()+"' ,"
					  + " bit_hora_edicion = current_time , bit_fecha_edicion = current_date, bit_firma = true "
					  + " where bit_id = "+bitacora.getBit_id()+" ;");
			enunciado.execute();	
			conexion.close();
			return 1;
		} catch (SQLException e) {
			e.printStackTrace();
			return 2;
		}
	}
	
	public int getActualAsignaturas(int bit_id) 
	{
		try {
			ConexionBd();
			resultado = consulta.executeQuery("select count(*) as cuenta from asignatura_bitacora "
					+ "where bit_id = "+bit_id+" ;");
			resultado.next();
			int contador = resultado.getInt("cuenta");
			CerrarConexionBd();
			return contador;
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		}
	}
	public int getActualImagenes(int bit_id) 
	{
		try {
			ConexionBd();
			resultado = consulta.executeQuery("select count(*) as cuenta from imagen_bitacora "
					+ "where bit_id = "+bit_id+" ;");
			resultado.next();
			int contador = resultado.getInt("cuenta");
			CerrarConexionBd();
			return contador;
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	public void ModificarEstadoUsuario(String rut,String estado,String tipo) {
		try {
			ConexionBd();
			if(tipo.equals("Alumno"))
			{
				PreparedStatement enunciado = conexion.prepareStatement("update alumno set alu_estado = '"+estado+"'  "
						+ "where alu_rut= '"+rut+"' ;");
				enunciado.execute();			
				conexion.close();
			}
			if(tipo.equals("Administrador"))
			{
				PreparedStatement enunciado = conexion.prepareStatement("update administrador set adm_estado = '"+estado+"'  "
						+ "where adm_rut= '"+rut+"' ;");
				enunciado.execute();			
				conexion.close();
			}
			if(tipo.equals("Supervisor"))
			{
				PreparedStatement enunciado = conexion.prepareStatement("update supervisor set sup_estado = '"+estado+"'  "
						+ "where sup_rut= '"+rut+"' ;");
				enunciado.execute();			
				conexion.close();
			}
			if(tipo.equals("Academico"))
			{
				PreparedStatement enunciado = conexion.prepareStatement("update academico set aca_estado = '"+estado+"'  "
						+ "where aca_rut= '"+rut+"' ;");
				enunciado.execute();			
				conexion.close();
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	// modifica datos de tabla alumno cuando ya se encuentran los datos registrados
	public int actualizarEstadoAlumno(String rut,String estado)
	{
		try {
			ConexionBd();
			System.out.println("rut "+rut);
			System.out.println("estado "+estado);
			
			 if(conexion!=null){
				 PreparedStatement enunciado = conexion.prepareStatement("update alumno set alu_estado = ? "
						  + "where alu_rut = ? ;");
				 enunciado.setString(1, rut);
				 enunciado.setString(2, estado);
				 enunciado.execute();
				 conexion.close();				
				 return 1;
			 }else{ System.out.println("conexion not initialized (null)");             
			 	return 0;
			 }
		} catch (SQLException e) {
			
			return 0;
		}catch (Exception ex){

			return 0;
		}
	}
	// modifica datos de tabla alumno cuando ya se encuentran los datos registrados pero recibieno parametros
		public int actualizarEstadoAlumnoConParametros(Connection conexion_update,String rut,String estado)
		{
			try {
					 PreparedStatement enunciado = conexion_update.prepareStatement("update alumno set alu_estado = ? "
							  + "where alu_rut = ? ;");
					 enunciado.setString(1, rut);
					 enunciado.setString(2, estado);
					 enunciado.execute();			
					 return 1;
			}catch (SQLException e) {
				return 0;
			}catch (Exception ex){
				return 0;
			}
		}
	// modifica datos de tabla cursa_plan_estudio cuando ya se encuentran los datos registrados
	public int actualizarCursaPlanAlumno(int plan_id,String alu_rut, int agnio_ingreso, int cur_semestre, boolean vigente, String estado)
	{
		try {
			ConexionBd();
			PreparedStatement enunciado = conexion.prepareStatement("update cursa_plan_estudio set cur_estado = '"+estado+"', "
					  + " cur_vigente = "+vigente+" where alu_rut = '"+alu_rut+"' and plan_id = "+plan_id+" "
					  + "and cur_agnio_ingreso = "+agnio_ingreso+" and cur_semestre = "+cur_semestre+";");
			enunciado.execute();	
			conexion.close();
			return 1;
		} catch (SQLException e) {

			return 0;
		}
	}
		// modifica datos de tabla cursa_plan_estudio cuando ya se encuentran los datos registrados pero recibiendo parametros
		public int actualizarCursaPlanAlumnoConParametros(Connection conexion_update,int plan_id,String alu_rut, int agnio_ingreso, int cur_semestre, boolean vigente, String estado)
		{
			try {
				PreparedStatement enunciado = conexion_update.prepareStatement("update cursa_plan_estudio set cur_estado = '"+estado+"', "
						  + " cur_vigente = "+vigente+" where alu_rut = '"+alu_rut+"' and plan_id = "+plan_id+" "
						  + "and cur_agnio_ingreso = "+agnio_ingreso+" and cur_semestre = "+cur_semestre+";");
				enunciado.execute();	
				return 1;
			} catch (SQLException e) {
				e.printStackTrace();
				return 0;
			}
		}
	public void modificarClaveUsuario(String rut, String tipo, String clave) 
	{
		if(tipo.equals("Administrador"))
		{
			try {
				ConexionBd();
				PreparedStatement enunciado = conexion.prepareStatement("update administrador set adm_password = '"+clave+"' "
						  + "where adm_rut = '"+rut+"' ;");
				enunciado.execute();			
				conexion.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		if(tipo.equals("Alumno"))
		{
			try {
				ConexionBd();
				PreparedStatement enunciado = conexion.prepareStatement("update alumno set alu_password = '"+clave+"' "
						  + "where alu_rut = '"+rut+"' ;");
				enunciado.execute();			
				conexion.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		if(tipo.equals("Academico"))
		{
			try {
				ConexionBd();
				PreparedStatement enunciado = conexion.prepareStatement("update academico set aca_password = '"+clave+"' "
						  + "where aca_rut = '"+rut+"' ;");
				enunciado.execute();			
				conexion.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		if(tipo.equals("Supervisor"))
		{
			try {
				ConexionBd();
				PreparedStatement enunciado = conexion.prepareStatement("update supervisor set sup_password = '"+clave+"' "
						  + "where sup_rut = '"+rut+"' ;");
				enunciado.execute();			
				conexion.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
	
