package com.icc.utilidades;



import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.Part;



public class Herramientas {

	public static String Formatear(String rut){
		int cont=0;
        String format;
        if(rut.length() == 0){
            return "";
        }else{
            rut = rut.replace(".", "");
            rut = rut.replace("-", "");
            format = "-"+rut.substring(rut.length()-1);
            for(int i = rut.length()-2;i>=0;i--){
                format = rut.substring(i, i+1)+format;
                cont++;
                if(cont == 3 && i != 0){
                    format = "."+format;
                    cont = 0;
                }
            }
            return format;
        }
	}
    public static String getcontentPartText(Part input) {
        Scanner sc = null;
        String content = null;
        try {
            sc = new Scanner(input.getInputStream(), "UTF-8");
            if (sc.hasNext()) {
                content = sc.nextLine();
            } else {
                content = "";
            }
            sc.close();
            return content;
        } catch (IOException ex) {
            Logger.getLogger(Herramientas.class.getName()).log(Level.SEVERE, ex.getMessage());
        }finally{
            sc.close();
        }
        return content;
    }

    public static String getContentTextArea(Part input) {
        Scanner sc = null;
        StringBuilder sb = null;
        try {
            sc = new Scanner(input.getInputStream(), "Cp1252");
            sb = new StringBuilder("");
            while (sc.hasNext()) {
                sb.append(sc.nextLine());
                sb.append("\n");
            }
        } catch (IOException ex) {
            Logger.getLogger(Herramientas.class.getName()).log(Level.SEVERE, ex.getMessage());
            sb = null;
        }finally{
            sc.close();
        }
        if (sb == null){
            return null;
        }else{
            return sb.toString();
        }
    }
    public static boolean guardar(InputStream input, String fileName)
            throws ServletException {
        FileOutputStream output = null;
        boolean ok = false;
        try {
            output = new FileOutputStream(fileName);
            int leido = 0;
            leido = input.read();
            while (leido != -1) {
                output.write(leido);
                leido = input.read();
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Herramientas.class.getName()).log(Level.SEVERE, ex.getMessage());
        } catch (IOException ex) {
            Logger.getLogger(Herramientas.class.getName()).log(Level.SEVERE, ex.getMessage());
        } finally {
            try {
                output.flush();
                output.close();
                input.close();
                ok = true;
            } catch (IOException ex) {
                Logger.getLogger(Herramientas.class.getName()).log(Level.SEVERE, "Error cerrando flujo de salida", ex);
            }
        }
        return ok;
    }
    
    public static String calcularDV(String vrut)
    {
        String rut = vrut.trim();

        int cantidad = rut.length();
        int factor = 2;
        int suma = 0;
        String verificador = "";

        for(int i = cantidad; i > 0; i--)
        {
            if(factor > 7)
            {
                factor = 2;
            }
            suma += (Integer.parseInt(rut.substring((i-1), i)))*factor;
            factor++;

        }
        verificador = String.valueOf(11 - suma%11);
        
        if(verificador.equals("10"))
        {
            return "K";
        }
        else
        {
            if(verificador.equals("11"))
            {
                return "0";
            }
            else
            {
            	return verificador;
            }
        }
               
    }  

}
