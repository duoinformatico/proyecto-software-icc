<%@ page language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<link rel="stylesheet" href="css/alertify.core.css" />
<link rel="stylesheet" href="css/alertify.default.css" id="toggleCSS" />
<script src="js/alertify.min.js"></script>
<script type="text/javascript" src="js/validaciones_admin.js"></script>
<script src="js/bootstrap-datepicker.js"></script>
<div id="myModal1" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	 <div class="modal-dialog modal-lg">
	    <div class="modal-content">
	    	<div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		        <h4 class="modal-title" id="myModalLabel">Pr�ctica de: ${alumno.alu_nombres} ${alumno.alu_apellido_p} ${alumno.alu_apellido_m}</h4>
		    </div>
		    <div class="modal-body">
		    	<form class="form-horizontal" id="formulario_practica" role="form" method="POST" action="practicaServlet.do">
					<h3>Informaci�n Principal</h3>
					
					<input type="hidden" name="id" id="id" value="${id}">
					<div class="form-group">
					<label class="col-xs-3 control-label" for="empresa">Empresa:</label>
						<div class="col-xs-9">
							<select class="form-control" id="empresa" name="empresa">				
								<c:forEach var="empresas" items="${empresaList}" >
									<c:if test="${empresas.emp_rut == practica.emp_rut}" >
										<option value="${empresas.emp_rut}" selected="selected">${empresas.emp_nombre}</option>
									</c:if>
								</c:forEach>
							</select>
						</div>
					</div>	
					<div id="div_supervisor">
							<div class="form-group ">
							<label class="col-xs-3 control-label" for="supervisor">Supervisores Activos :</label>
								<div class="col-xs-9">
									<select class="form-control" id="supervisor" name="supervisor">										
										<option value="${supervisor.sup_rut}">${supervisor.sup_nombres} ${supervisor.sup_apellido_p} ${supervisor.sup_apellido_m}</option>							
									</select>
								</div>
							</div>
					</div>
					<div class="form-group">
							<label class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1  control-label" for="profesor">Profesor Gu�a:</label>
							<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
								<select class="form-control" id="profesor" name="profesor">				
								<c:forEach var="profesores" items="${profesorList}" >
									<c:if test="${profesores.aca_rut == profesor.aca_rut}" >
										<option value="${profesor.aca_rut}" selected="selected">${profesor.aca_nombres} ${profesor.aca_apellido_p} ${profesor.aca_apellido_m}</option>
									</c:if>
									<c:if test="${profesores.aca_rut != profesor.aca_rut}" >
										<option value="${profesores.aca_rut}">${profesores.aca_nombres} ${profesores.aca_apellido_p} ${profesores.aca_apellido_m}</option>
									</c:if>
								</c:forEach>
							</select>					
							</div>
					</div>					
					<div class="form-group">
					<label class="col-xs-3 control-label" for="obra">Obra:</label>
						<div class="col-xs-9">
							<select class="form-control" id="obra" name="obra">	
								<c:forEach var="obras" items="${obrasList}" >
									<c:if test="${obras.obr_id == practica.obr_id}" >
										<option value="${obras.obr_id}" selected="selected">${obras.obr_nombre}</option>
									</c:if>
									<c:if test="${obras.obr_id != practica.obr_id}" >
										<option value="${obras.obr_id}">${obras.obr_nombre}</option>
									</c:if>
								</c:forEach>
							</select>
						</div>
					</div>
					<script type="text/javascript">
					$(document).ready(function () {
						$('#alumno').bind('copy paste drop', function (e) {
					           e.preventDefault();
					    });       
					    $("#alumno").focusin(function() {
				        	$("#alumno").attr('autocomplete', 'off');
				        });
				        $("#alumno").focusout(function() {
				        	$("#alumno").removeAttr('autocomplete');
				        });
						$('#obra_nombre').bind('copy paste drop', function (e) {
					           e.preventDefault();
					    });       
					    $("#obra_nombre").focusin(function() {
				        	$("#obra_nombre").attr('autocomplete', 'off');
				        });
				        $("#obra_nombre").focusout(function() {
				        	$("#obra_nombre").removeAttr('autocomplete');
				        });
				        $('#direccion').bind('copy paste drop', function (e) {
					           e.preventDefault();
					        });
				        $("#direccion").focusin(function() {
				        	$("#direccion").attr('autocomplete', 'off');
				        });
				        $("#direccion").focusout(function() {
				        	$("#direccion").removeAttr('autocomplete');
				        });
				        $('#tareas').bind('copy paste drop', function (e) {
					           e.preventDefault();
					        });
				        $("#tareas").focusin(function() {
				        	$("#tareas").attr('autocomplete', 'off');
				        });
				        $("#tareas").focusout(function() {
				        	$("#tareas").removeAttr('autocomplete');
				        });
				        $('#inicio').bind('copy paste drop', function (e) {
					           e.preventDefault();
					        });
				        $("#inicio").focusin(function() {
				        	$("#inicio").attr('readonly', 'readonly');
				        	$("#inicio").attr('autocomplete', 'off');
				        });
				        $("#inicio").focusout(function() {
				        	$("#inicio").removeAttr('readonly');
				        	$("#inicio").removeAttr('autocomplete');
				        });
				        $('#termino').bind('copy paste drop', function (e) {
					           e.preventDefault();
					        });
				        $("#termino").focusin(function() {
				        	$("#termino").attr('readonly', 'readonly');
				        	$("#termino").attr('autocomplete', 'off');
				        });
				        $("#termino").focusout(function() {
				        	$("#termino").removeAttr('readonly');
				        	$("#termino").removeAttr('autocomplete');
				        });
				        $('#horas').bind('copy paste drop', function (e) {
					           e.preventDefault();
					        });
				        $("#horas").focusin(function() {
				        	$("#horas").attr('autocomplete', 'off');
				        });
				        $("#horas").focusout(function() {
				        	$("#horas").removeAttr('autocomplete');
				        });
					});
				    </script>
					
					<div class="form-group">
					<label class="col-xs-3 control-label" for="asignatura">Asignatura:</label>
						<div class="col-xs-9">
							<select class="form-control" id="asignatura" name="asignatura">				
								<c:forEach var="asignaturas" items="${gestionesList}" >
									<c:if test="${asignaturas.asi_codigo == practica.asi_codigo}" >
										<option value="${asignaturas.asi_codigo}" selected="selected">${asignaturas.asi_nombre}</option>
									</c:if>
								</c:forEach>
							</select>
						</div>
					</div>
					<div class="form-group ">
					<label class="col-xs-3 control-label" for="plan">Plan de Estudio:</label>
						<div class="col-xs-9">
							<select class="form-control" id="plan" name="plan">				
								<c:forEach var="planes" items="${planesList}" >
									<c:if test="${planes.plan_id == practica.plan_id}" >
										<option value="${planes.plan_id}" selected="selected">${planes.plan_nombre}</option>
									</c:if>
								</c:forEach>
							</select>						
						</div>
					</div>
					<h3>Informaci�n de Obra</h3>
					<div class="form-group ">
					<label class="col-xs-3 control-label" for="obra_nombre">Nombre Obra:</label>
						<div class="col-xs-9">
							<input type="text" class="form-control" id="obra_nombre" name="obra_nombre" value="${practica.pra_obra_nombre}" onkeypress="return funcionMaestra(event,'nombres','1')">						
						</div>
					</div>
					<div class="form-group">
						<label class="col-xs-3 control-label" for="region">Regi�n:</label>
						<div class="col-xs-9">							
							<select class="form-control" id="region" name="region" required>
							<option value="">Seleccione Regi�n...</option>
							<c:forEach var="regiones" items="${regionList}" >
								<c:if test="${regiones.reg_id == region.reg_id}" >
										<option value="${region.reg_id}" selected="selected">${region.reg_nombre}</option>
								</c:if>
								<c:if test="${regiones.reg_id != region.reg_id}" >
									<option value="${regiones.reg_id}">${regiones.reg_nombre}</option>
								</c:if>
							</c:forEach>
							</select>							
						</div>
						</div>
						<div id="div_provincia">
							<div class="form-group">
							<label class="col-xs-3 control-label" for="provincia">Provincia:</label>
							<div class="col-xs-9">							
								<select class="form-control" id="provincia" name="provincia" required>
									<option value="${provincia.pro_id}" selected="selected">${provincia.pro_nombre}</option>							
								</select>					
							</div>
						</div>
						</div>
						<div id="div_comuna">
							<div class="form-group">
								<label class="col-xs-3 control-label" for="comuna">Comuna:</label>
								<div class="col-xs-9">
									<select class="form-control" id="comuna" name="comuna" required>
										<option value="${comuna.com_id}" selected="selected">${comuna.com_nombre}</option>
									</select>							
								</div>
							</div>
						</div>
					<div class="form-group ">
					<label class="col-xs-3 control-label" for="direccion">Direcci�n de Obra:</label>
						<div class="col-xs-9">
							<input type="text" class="form-control" id="direccion" name="direccion" value="${practica.pra_direccion}" onkeypress="return funcionMaestra(event,'dire','1')">						
						</div>
					</div>
					<div class="form-group ">
					<label class="col-xs-3 control-label" for="tareas">Labor a Desarrollar:</label>
						<div class="col-xs-9">
							<textarea class="form-control" id="tareas" name="tareas" onkeypress="return funcionMaestra(event,'descri','2')">${practica.pra_tareas}</textarea>					
						</div>
					</div>
					<script type="text/javascript">
				            // When the document is ready
				            $(document).ready(function () { 
				            	
				                $('#termino').datepicker({
				                	
				                    format: "dd-mm-yyyy"
				                });  
				            });
				    </script>
					<div class="form-group ">
					<label class="col-xs-3 control-label" for="inicio">Fecha Inicio Pr�ctica:</label>
						<div class="col-xs-9">
							<input type="text" class="form-control" id="inicio" name="inicio" value="${practica.pra_fecha_ini}" readonly="readonly">						
						</div>
					</div>
					<div class="form-group ">
					<label class="col-xs-3 control-label" for="termino">Fecha T�rmino Pr�ctica:</label>
						<div class="col-xs-9">
							<div class='input-group date'>
			                    <input type='text' class="form-control"  id="termino" name="termino"  value="${practica.pra_fecha_fin}" readonly="readonly"/>
			                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
			                    </span>
			                </div>
						</div>
					</div>
					<div class="form-group ">
					<label class="col-xs-3 control-label" for="horas">Cantidad de Horas:</label>
						<div class="col-xs-9">
							<input type="text" class="form-control" id="horas" name="horas" value="${practica.pra_horas}" onkeypress="return funcionMaestra(event,'num','1')">						
						</div>
					</div>
					<div class="form-group ">
					<label class="col-xs-3 control-label" for="estado">Estado:</label>
						<div class="col-xs-9">
							<select class="form-control" id="estado" name="estado">	
								<c:if test="${practica.pra_estado == 'Aceptada'}" >
									<option value="Aceptada" selected="selected">Aceptada</option>
									<option value="Finalizada">Finalizada</option>
								</c:if>
								<c:if test="${practica.pra_estado == 'Rechazada'}" >
									<option value="Rechazada" selected="selected">Rechazada</option>
								</c:if>
								<c:if test="${practica.pra_estado == 'Finalizada'}" >
									<option value="Finalizada">Finalizada</option>
								</c:if>
							</select>						
						</div>
					</div>
					<button type="submit" class="btn btn-primary  btn-xg" id="accion" name="accion" onclick="return validar_practica(event,2)">Modificar Pr�ctica</button>
						<input type="hidden" id="profesor_actual" name="profesor_actual" value="${profesor.aca_rut}">
						<input type="hidden" id="supervisor_actual" name="supervisor_actual" value="${supervisor.sup_rut}">
						<input type="hidden" class="form-control" id="fecha_ins" name="fecha_ins" readonly="readonly" value="${practica.ins_fecha}" >
						<input type="hidden" class="form-control" id="alumno" name="alumno" value="${practica.alu_rut}" readonly="readonly">
				</form>
		    </div>
		</div>
	</div>
</div>
