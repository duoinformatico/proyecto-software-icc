<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<c:forEach var="i" begin="1" end="5" step="1">
	<c:if test="${ n_academico == i}">
		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
			<select required class="form-control" id="academico_${i}" name="academico_${i}">	
				<option value="">Seleccione Académico...</option>
					<c:forEach var="academico" items="${academicosList}" >
							<option value="${academico.aca_rut}">${academico.aca_nombres} ${academico.aca_apellido_p} ${academico.aca_apellido_m}</option>
					</c:forEach>
			</select>
		</div>
	</c:if>
</c:forEach>