<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<c:if test="${tipo_num == 0}" >
	<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3">			
		<div class="alert alert-danger alert-dismissible" role="alert">
		  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		  <span class="glyphicon glyphicon-remove"></span>
		  <strong>Error</strong> No se Encontraron Resultados.
		</div>
	</div>
</c:if>

<c:if test="${tipo == 'Administrador' || tipo == 'Administrador - Academico'}" >
	<c:if test="${tipo_num != 0}" >
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xs-offset-0 col-sm-offset-0 col-md-offset-0 col-lg-offset-0">
	  		<div class="table-responsive">
				<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered table-hover" id="datatable">
					<thead>
					  <tr >
					  	 <th>RUT</th>
					     <th>Nombre</th>
					     <th>Tipo Usuario</th>
					     <th>E-mail</th>
					     <th>Acciones</th>
					  </tr>
			        </thead>
			        <tbody>
			        <tr>
			        	<td>${administrador.adm_rut}</td>
			        	<td>${administrador.adm_nombres} ${administrador.adm_apellido_p} ${administrador.adm_apellido_m}</td>
			        	<c:if test="${tipo == 'Administrador'}" >
				        	<td>${tipo}</td>
			        	</c:if>
			        	<c:if test="${tipo == 'Administrador - Academico'}" >
			        		<td>Administrador - Acad�mico</td>
			        	</c:if>
			        	<td>${administrador.adm_email}</td>
			        	<td><button class="btn btn-primary btn-xs" onclick="cambiarPass('${administrador.adm_nombres}','${administrador.adm_apellido_p}','${administrador.adm_apellido_m}','${administrador.adm_email}','${tipo}')"><span class="glyphicon glyphicon-edit"></span> Generar Contrase�a</button></td>
			        </tr>		        		
			        </tbody>
			    </table>
			</div>
	  	</div>
	 </c:if>
</c:if>
<c:if test="${tipo == 'Academico' || tipo == 'Academico - Administrador'}" >
	<c:if test="${tipo_num != 0}" >
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xs-offset-0 col-sm-offset-0 col-md-offset-0 col-lg-offset-0">
		  		<div class="table-responsive">
					<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered table-hover" id="datatable">
						<thead>
						  <tr >
						  	 <th>RUT</th>
						     <th>Nombre</th>
						     <th>Tipo Usuario</th>
						     <th>E-mail</th>
						     <th>Acciones</th>
						  </tr>
				        </thead>
				        <tbody>
				        <tr>
				        	<td>${academico.aca_rut}</td>
				        	<td>${academico.aca_nombres} ${academico.aca_apellido_p} ${academico.aca_apellido_m}</td>
				        	<c:if test="${tipo == 'Academico'}" >
				        		<td>Acad�mico</td>
				        	</c:if>
				        	<c:if test="${tipo == 'Academico - Administrador'}" >
				        		<td>Acad�mico - Administrador</td>
				        	</c:if>
				        	<td>${academico.aca_email}</td>
				        	<td><button class="btn btn-primary btn-xs" onclick="cambiarPass('${academico.aca_nombres}','${academico.aca_apellido_p}','${academico.aca_apellido_m}','${academico.aca_email}','${tipo}')"><span class="glyphicon glyphicon-edit"></span> Generar Contrase�a</button></td>
				        </tr>		        		
				        </tbody>
				    </table>
				</div>
		  	</div>
	</c:if>
</c:if>
<c:if test="${tipo == 'Alumno'}" >
	<c:if test="${tipo_num != 0}" >
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xs-offset-0 col-sm-offset-0 col-md-offset-0 col-lg-offset-0">
	  		<div class="table-responsive">
				<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered table-hover" id="datatable">
					<thead>
					  <tr >
					  	 <th>RUT</th>
					     <th>Nombre</th>
					     <th>Tipo Usuario</th>
					     <th>E-mail</th>
					     <th>Acciones</th>
					  </tr>
			        </thead>
			        <tbody>
			        <tr>
			        	<td>${alumno.alu_rut}</td>
			        	<td>${alumno.alu_nombres} ${alumno.alu_apellido_p} ${alumno.alu_apellido_m}</td>
			        	<td>${tipo}</td>
			        	<td>${alumno.alu_email}</td>
			        	<td><button class="btn btn-primary btn-xs" onclick="cambiarPass('${alumno.alu_nombres}','${alumno.alu_apellido_p}','${alumno.alu_apellido_m}','${alumno.alu_email}','${tipo}')"><span class="glyphicon glyphicon-edit"></span> Generar Contrase�a</button></td>
			        </tr>		        		
			        </tbody>
			    </table>
			</div>
	  </div>
	</c:if>
</c:if>
<c:if test="${tipo == 'Supervisor'}" >
	<c:if test="${tipo_num != 0}" >
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xs-offset-0 col-sm-offset-0 col-md-offset-0 col-lg-offset-0">
	 		<div class="table-responsive">
			<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered table-hover" id="datatable">
				<thead>
				  <tr >
				  	 <th>RUT</th>
				     <th>Nombre</th>
				     <th>Tipo Usuario</th>
				     <th>E-mail</th>
				     <th>Acciones</th>
				  </tr>
		        </thead>
		        <tbody>
		        <tr>
		        	<td>${supervisor.sup_rut}</td>
		        	<td>${supervisor.sup_nombres} ${supervisor.sup_apellido_p} ${supervisor.sup_apellido_m}</td>
		        	<td>${tipo}</td>
		        	<td>${supervisor.sup_email}</td>
		        	<td><button class="btn btn-primary btn-xs" onclick="cambiarPass('${supervisor.sup_nombres}','${supervisor.sup_apellido_p}','${supervisor.sup_apellido_m}','${supervisor.sup_email}','${tipo}')"><span class="glyphicon glyphicon-edit"></span> Generar Contrase�a</button></td>
		        </tr>		        		
		        </tbody>
		    </table>
			</div>
	 	</div>
	 </c:if>
</c:if>