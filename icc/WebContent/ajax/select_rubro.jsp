<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<div class="form-group">
	<div class="col-xs-9">	
		<select class="form-control" id="rubro_${numero_rubro}" name="rubro_${numero_rubro}" required>
			<option value="">Seleccione Rubro...</option>
			<c:forEach var="rubro" items="${rubrosList}" >
				<option value="${rubro.rub_id}">${rubro.rub_nombre}</option>
			</c:forEach>
		</select>							
	</div>
</div>