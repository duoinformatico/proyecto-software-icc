<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<div class="form-group">
	<label class="col-xs-3 control-label" for="rol">Rol Evaluador:</label>
	<div class="col-xs-9">							
		<select class="form-control" id="rol" name="rol" required>
		<option value="">Seleccione Rol Evaluador...</option>
		<c:forEach var="rol" items="${rolesList}" >
			<option value="${rol.rol_id}">${rol.rol_nombre} - Ponderación Decimal (${rol.rol_ponderacion})</option>
		</c:forEach>
		</select>							
	</div>
</div>