<%@ page language="java" contentType="text/html;" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
	        <h4 class="modal-title" id="myModalLabel">Detalle de Evaluación. Evaluador : ${evaluador} <b>NOTA: ${evaluacion.eva_nota}</b></h4>
	    </div>
	      <div class="modal-body">
	      	<legend>${instrumento.ins_nombre}</legend>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 columna">
			<form class="form-horizontal" id="formulario_evaluacion_supervisor" role="form" method="POST" action="">
			<div class="table-responsive">
				<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered" id="datatable">
					<thead>
					  <tr >
					     <th colspan="2">Evaluación</th>
					     <th colspan=${escala.esc_num_opciones}>Criterios de Evaluación</th>
					  </tr>
			        </thead>
			        <tbody>
			        	<tr>
			        		<td colspan="2"><b>Indivudual</b></td>
			        		<c:forEach items="${escala.valores}" var="valores" >
			        			<td align="center">${valores.val_nombre}</td>
			        		</c:forEach>        		
						</tr>
						<tr>
							<td><b>Categoría</b></td>
							<td><b>Concepto</b></td>
			        		<c:forEach items="${escala.valores}" var="valores" >
			        		<fmt:parseNumber var="valor" pattern="#" integerOnly="true" value="${valores.val_valor}" />
			        			<td align="center">${valor}</td>
			        		</c:forEach>  
						</tr>
						<c:forEach items="${instrumento.items}" var="items" varStatus="stat">
						<tr>
			        			<c:forEach items="${categoriasList}" var="categoria" >
			        				<c:if test="${items.cat_id == categoria.cat_id}">
			        					<td><b>${categoria.cat_nombre}</b></td>
			        				</c:if>			        				
			        			</c:forEach>
			        			<td><p class="text-left">${items.item_nombre}</p></td>
			        			<c:forEach items="${items.opciones}" var="opciones" >
			        				<c:set var="flag_marcado" value="0"/>
			        				<c:forEach items="${detalleEvaluacion}" var="detalle" >
					        			<c:if test="${detalle.item_id == opciones.item_id && detalle.opc_id == opciones.opc_id }">
					        				<c:set var="flag_marcado" value="1"/>
					        			</c:if>
				        			</c:forEach>
				        			<c:if test="${flag_marcado == 1}">
				        				<td class="vert-align" align="center" bgcolor="#0066FF">
				        					<input style="padding: 50px;"data-toggle="tooltip" data-placement="bottom" title="${opciones.opc_descripcion}" type="radio" id="${opciones.item_id}" name="${opciones.item_id}" value="${opciones.opc_valor}" required onchange='calcularPuntaje()' checked>
				        				</td>
				        			</c:if>
				        			<c:if test="${flag_marcado != 1}">
				        				<td class="vert-align" align="center">
				        					<input style="padding: 50px;"data-toggle="tooltip" data-placement="bottom" title="${opciones.opc_descripcion}" type="radio" id="${opciones.item_id}" name="${opciones.item_id}" value="${opciones.opc_valor}" required onchange='calcularPuntaje()'>
				        				</td>
				        			</c:if>			        				
			        			</c:forEach>
			        	</tr>
			        		<c:set var="cadena_items" value="${stat.first ? '' : cadena_items}${items.item_id}-" />
			        	</c:forEach>
	        	</tbody>
		    </table>
			</div>

		    <div class="form-group">
					<label class="control-label">Áreas en que Trabajó el Alumno durante la Práctica</label>
			</div>			
			<div class="table-responsive">
				<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-hover">
					<thead>
						<tr>
							<td align="center">Área</td>
						</tr>
					</thead>
					<c:forEach var="areaAlumno" items="${areasAlumnoList}" >
					<tr>
						<c:forEach var="area" items="${areasPracticaList}" >
							<c:if test="${areaAlumno.area_id == area.area_id}">
								<td align="center">${area.area_nombre}</td>
							</c:if>														
						</c:forEach>
					</tr>				
					</c:forEach>
				</table>							
			</div>
			
			<div class="form-group">
					<label class="control-label">Funciones que realizó el Alumno durante la Práctica</label>
			</div>			
			<div class="table-responsive">
				<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-hover">
					<thead>
						<tr>
							<td align="center">Funciones</td>
						</tr>
					</thead>
					<c:forEach var="funcionAlumno" items="${funcionesAlumnoList}" >
					<tr>
						<c:forEach var="funcion" items="${funcionesPracticaList}" >
							<c:if test="${funcionAlumno.fun_id == funcion.fun_id}">
								<td align="center">${funcion.fun_nombre}</td>
							</c:if>														
						</c:forEach>
					</tr>				
					</c:forEach>
				</table>							
			</div>
			
			<label class="control-label" for="observacion">Observaciones:</label>
			<textarea class="form-control no_resize" id="observacion" name="observacion" readonly onkeypress="return funcionMaestra(event,'descri','2')">${evaluacion.eva_observacion }</textarea>					
			<input type="hidden" name="num_items" value="${fn:length(instrumento.items)}">
			<input type="hidden" id="max_puntaje" name="max_puntaje" value="${instrumento.ins_max_puntaje}">
			<input type="hidden" id="nombre_items" name="nombre_items" value="${cadena_items}">
			</form>
	      </div>
	      <div class="modal-footer">
	     </div>
    </div>
  </div>
</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	var nombre_items = $("#nombre_items").val();
	var items = nombre_items.split("-");
	//alert("Cantidad Items Rubrica = "+items.length+"\nElementos"+items);
	for(var i=0;i <= items.length; i++){
		$("#items["+i+"]").tooltip({
	        placement : 'bottom'
	    });
		//alert($("#items["+i+"]"));
		$("input[name='"+items[i]+"']").tooltip({
        	placement : 'bottom'
    	});
		//$("input[name='"+items[i]+"']").attr('disabled', 'disabled');
	}
});
</script>