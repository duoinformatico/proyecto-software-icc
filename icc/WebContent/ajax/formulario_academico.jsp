<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<link rel="stylesheet" href="css/alertify.core.css" />
<link rel="stylesheet" href="css/alertify.default.css" id="toggleCSS" />
<script src="js/alertify.min.js"></script>
<script type="text/javascript" src="js/validar_rut.js"></script>
<script type="text/javascript" src="js/validaciones_admin.js"></script>

<div id="myModal_registro" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	 <div class="modal-dialog modal-lg">
	    <div class="modal-content">
	    	<div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		        <h4 class="modal-title" id="myModalLabel">Formulario de Inscripción de Nuevo Académico</h4>
		    </div>
		    <div class="modal-body">
		    	<form class="form-horizontal" role="form" id="formulario_academico" method="POST" action="academicoServlet.do">
					<div class="form-group">
					<label class="col-xs-3 control-label" for="rut">Rut:</label>
						<div class="col-xs-9">
							<input type="tel" class="form-control" id="rut" name="rut" placeholder="16.765.136-4"
							 ondrop="return false;" onChange="Rut(this.value)" onkeypress="return funcionMaestra(event, 'rut', 1)" >
						</div>
					</div>
					<div class="form-group">
					<label class="col-xs-3 control-label" for="departamento">Departamento:</label>
						<div class="col-xs-9">
						<c:set var="lista" value="${departamentos}"></c:set>	
							<select class="form-control" id="departamento" name="departamento">
							<c:forEach items="${lista}" var="departamento">
								<option value="${departamento.dep_id}">${departamento.dep_nombre}</option>
							</c:forEach>
							</select>							
						</div>
					</div>
					<script type="text/javascript">
					$(document).ready(function () {
							$('#rut').bind('copy paste drop', function (e) {
					           e.preventDefault();
					        });       
					        $("#rut").focusin(function() {
					        	$("#rut").attr('autocomplete', 'off');
					        	$("#rut").attr('maxlength', '9');
					        });
					        $("#rut").focusout(function() {
					        	$("#rut").removeAttr('autocomplete');
					        	$("#rut").removeAttr('maxlength');
					        });
							$('#nombre').bind('copy paste drop', function (e) {
					           e.preventDefault();
					        });       
					        $("#nombre").focusin(function() {
					        	$("#nombre").attr('autocomplete', 'off');
					        });
					        $("#nombre").focusout(function() {
					        	$("#nombre").removeAttr('autocomplete');
					        });
					        $('#paterno').bind('copy paste drop', function (e) {
						           e.preventDefault();
						        });
					        $("#paterno").focusin(function() {
					        	$("#paterno").attr('autocomplete', 'off');
					        });
					        $("#paterno").focusout(function() {
					        	$("#paterno").removeAttr('autocomplete');
					        });
					        $('#materno').bind('copy paste drop', function (e) {
						           e.preventDefault();
						        });
					        $("#materno").focusin(function() {
					        	$("#materno").attr('autocomplete', 'off');
					        });
					        $("#materno").focusout(function() {
					        	$("#materno").removeAttr('autocomplete');
					        });
					        $('#email').bind('copy paste drop', function (e) {
						           e.preventDefault();
						        });
					        $("#email").focusin(function() {
					        	$("#email").attr('autocomplete', 'off');
					        });
					        $("#email").focusout(function() {
					        	$("#email").removeAttr('autocomplete');
					        });
					        $('#telefono').bind('copy paste drop', function (e) {
						           e.preventDefault();
						        });
					        $("#telefono").focusin(function() {
					        	$("#telefono").attr('autocomplete', 'off');
					        	if($("#codigo").val() == ""){
					        		$("#telefono").attr('readonly', 'readonly');
					        		$("#codigo").focus();
					        	}
					        	else{
					        		$("#telefono").removeAttr('readonly');
					        	}
					        	if($("#codigo").val() != ""){
					        		if($("#codigo").val() == 2){
					        			$("#telefono").attr('maxlength', '8');
					        		}
					        		else{
					        			$("#telefono").attr('maxlength', '7');
					        		}
					        	}					        	
					        });
							$("#codigo").change(function(){					        	
								if($("#codigo").val() != ""){
					        		$("#telefono").focus();
					        		$("#telefono").val("");
					        	}					        	
					        });
					        $("#telefono").change(function(){					        	
					        	
					        	if($("#telefono").val().length == 8 && $("#codigo").val() == 2){
					        		$("#telefono").val("+56-"+$("#codigo").val()+"-"+$("#telefono").val());
					        	}
					        	if($("#telefono").val().length == 7 && $("#codigo").val() != 2){
					        		$("#telefono").val("+56-"+$("#codigo").val()+"-"+$("#telefono").val());
					        	}
					        });					        	
					        
					        $("#telefono").focusout(function() {
					        	$("#telefono").removeAttr('autocomplete');
					        	$("#telefono").removeAttr('maxlength');
					        	if($("#codigo").val() == ""){
					        		$("#telefono").attr('readonly', 'readonly');
					        		$("#codigo").focus();
					        	}
					        	else{
					        		$("#telefono").removeAttr('readonly');
					        	}
					        	if($("#telefono").val().length == 8 && $("#codigo").val() == 2){
					        		$("#telefono").val("+56-"+$("#codigo").val()+"-"+$("#telefono").val());
					        	}
					        	if($("#telefono").val().length < 8 && $("#codigo").val() == 2){
					        		$("#telefono").val("");
					        	}
					        	if($("#telefono").val().length == 7 && $("#codigo").val() != 2){
					        		$("#telefono").val("+56-"+$("#codigo").val()+"-"+$("#telefono").val());
					        	}
					        	if($("#telefono").val().length < 7 && $("#codigo").val() != 2){
					        		$("#telefono").val("");
					        	}
					        });
					        $('#celular').bind('copy paste drop', function (e) {
						           e.preventDefault();
						        });
					        $("#celular").focusin(function() {
					        	$("#celular").attr('autocomplete', 'off');
					        	$("#celular").attr('maxlength', '8');					        	
					        });
					       $("#celular").focusout(function() {
					        	$("#celular").removeAttr('autocomplete');
					        	$("#celular").removeAttr('maxlength');
					        	if($("#celular").val().length < 8){
					        		$("#celular").val("");
					        	}
					        	if($("#celular").val().length == 8){
					        		$("#celular").val("+569"+$("#celular").val());
					        	}
					        });
							/*$("#celular").bind("input",function(){
					        	if($("#celular").val().length == 8){
					        		$("#celular").val("+569"+$("#celular").val());
					        	}
					        });*/
					        $('#estado').bind('copy paste drop', function (e) {
						           e.preventDefault();
						        });
					        $("#estado").focusin(function() {
					        	$("#estado").attr('readonly', 'readonly');
					        	$("#estado").attr('autocomplete', 'off');
					        });
					        $("#estado").focusout(function() {
					        	$("#estado").removeAttr('readonly');
					        	$("#estado").removeAttr('autocomplete');
					        });
				      });
				    </script>

					<div class="form-group">
						<label class="col-xs-3 control-label" for="nombre">Nombres:</label>
						<div class="col-xs-9">
							<input type="text" class="form-control" id="nombre" name="nombre" placeholder="Pablo Ignacio" onkeypress="return funcionMaestra(event,'nombres','1')">
						</div>	
					</div>
					<div class="form-group">
						<label class="col-xs-3 control-label" for="paterno">Apellido Paterno:</label>
						<div class="col-xs-9">
							<input type="text" class="form-control" id="paterno" name="paterno" placeholder="Saavedra" onkeypress="return funcionMaestra(event,'nombres','1')">
						</div>	
					</div>
					<div class="form-group">
						<label class="col-xs-3 control-label" for="materno">Apellido Materno:</label>
						<div class="col-xs-9">
							<input type="text" class="form-control" id="materno" name="materno" placeholder="Arevalo" onkeypress="return funcionMaestra(event,'nombres','1')">
						</div>	
					</div>	
					<div class="alert alert-warning alert-dismissible" role="alert">
					  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					  <span class="glyphicon glyphicon-remove"></span>
					  <strong>Advertencia</strong> Es muy importante que el email ingresado sea correcto, por favor verificar cuidadosamente al momento de ingresar.
					</div>
					<div class="form-group">
						<label class="col-xs-3 control-label" for="email">E-Mail:</label>
						<div class="col-xs-9">
							<div class="input-group">
								<input type="email" class="form-control" id="email" name="email" placeholder=" ejemplo@ejempo.com" onkeypress="return funcionMaestra(event,'email','2')"> 
								<span class="input-group-addon">
									@
								</span>
							</div>						
						</div>
					</div>
					<div class="form-group">
						<label class="col-xs-3 control-label" for="telefono">Teléfono:</label>
						<div class="col-xs-4">	
							<select class="form-control" id="codigo" name="codigo">
								<option value="">Código Area...</option>
								<optgroup label="Zona Norte">
									<option value="58">(58) Arica-Parinacota</option>
									<option value="57">(57) Iquique-Tamarugal</option>
									<option value="55">(55) Tocopilla-Loa-Antofagasta</option>
									<option value="51">(51) Huasco</option>
									<option value="52">(52) Chañaral-Copiapó</option>
									<option value="51">(51) Elqui</option>
									<option value="53">(53) Choapa-Limarí</option>
								</optgroup>
								<optgroup label="Zona Centro">
									<option value="32">(32) Isla de Pascua-Valparaíso-Marga Marga(Quilpué-Villa Alemana)</option>
									<option value="33">(33) Petorca-Quillota-Marga Marga(Limache y Olmué)</option>
									<option value="34">(34) Los Andes-San Felipe de Aconcagua</option>
									<option value="35">(35) San Antonio</option>
									<option value="2"> ( 2) Chacabuco-Cordillera-Maipo-Melipilla-Santiago-Talagante</option>
									<option value="72">(72) Cachapoal-ardenal Caro-Colchagua</option>
									<option value="71">(71) Talca</option>
									<option value="73">(73) Cauquenes-Linares</option>
									<option value="75">(75) Curicó</option>
								</optgroup>
								<optgroup label="Zona Sur">
									<option value="41">(41) Arauco-Concepción </option>
									<option value="42">(42) Ñuble</option>
									<option value="43">(43) Bío-Bío</option>
									<option value="45">(45) Cautín-Malleco</option>
									<option value="63">(63) Ranco-Valdivia</option>
									<option value="64">(64) Osorno</option>
									<option value="65">(65) Chiloé-Llanquihue-Palena </option>
									<option value="67">(67) Aisén-Capitán Prat-Coihaique-General Carrera</option>
									<option value="68">(68) Aisén(Puyuhuapi)</option>
									<option value="61">(61) Antártica Chilena-Magallanes-Tierra del Fuego-Última Esperanza</option>
								</optgroup>
							</select>							
						</div>
						<div class="col-xs-5">
							<div class="input-group">
								<input type="tel" class="form-control" id="telefono" name="telefono" placeholder="Ej:2323421" onkeypress="return funcionMaestra(event,'fijo',1)" >
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-phone-alt"></span>
								</span>
							</div>	
						</div>	
					</div>
					<div class="form-group">
						<label class="col-xs-3 control-label" for="celular">Celular:</label>
						<div class="col-xs-9">
							<div class="input-group">
								<input type="tel" class="form-control" id="celular" name="celular" placeholder="Ej: +56998345670" onkeypress="return funcionMaestra(event,'celular',1)">
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-phone"></span>
								</span>
							</div>	
						</div>	
					</div>
					<div class="form-group">
						<label class="col-xs-3 control-label" for="estado">Estado:</label>
						<div class="col-xs-9">
							<input type="text" class="form-control" id="estado" name="estado" value="activo" readonly="readonly">
						</div>	
					</div>
					<div class="form-group">
						<label class="col-xs-6 col-sm-6 col-md-6 col-lg-6 control-label">Especialidades:</label>
						<label class="col-xs-6 col-sm-6 col-md-6 col-lg-6 control-label">Especialidades que posee el nuevo Académico</label>			
					</div>
					<div class="form-group">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						    <select class="form-control" multiple="multiple" id="especialidad" name="especialidad">
								<c:forEach var="especialidad" items="${especialidadList}" >
									<option value="${especialidad.esp_id}">${especialidad.esp_nombre}</option>
								</c:forEach>
						    </select>
					    </div>
					 </div>
					 <script type="text/javascript">
					 	var ultima_seleccion_valida = null;
						$(document).ready(function(){	
							$('#especialidad').multiSelect();					
							
							$('#especialidad').change(function(event) {
							  if ($(this).val().length > 4) {
							    alert('Solo puede seleccionar un maximo de 4 especialidades');
							    $(this).multiSelect('deselect_all');
							    //$(this).multiSelect('deselect', $(this)[$(this).size()-1]);
							  } else {
								  //alert($(this).val());
								  //alert($(this).multiSelect('deselect', $(this)[1].val()));
								  //alert($("#especialidad").val());
								  ultima_seleccion_valida = $(this).val();
							  }
							});				
						});
					 </script>
					
					
					<div class="form-group">
						<div class="col-xs-9 col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3">
							<div id="div_especialidad_1">
							</div>

							<div id="div_especialidad_2">
							</div>

							<div id="div_especialidad_3">
							</div>

							<div id="div_especialidad_4">
							</div>
						</div>
					</div>
					<div class="col-xs-offset-2 col-sm-offset-2 col-md-offset-1 col-lg-offset-1">
						
						<button type="submit" id="boton" onclick="return validar_academico(event,1)" class="btn btn-primary">Registrar Académico</button>
						
						<input type="hidden" id="num_especialidades" name="num_especialidades" value="4">
						<input type="hidden" id="actual_especialidades" name="actual_especialidades" value="0">
					</div>
					</form>
		    </div>
		</div>
	</div>
</div>