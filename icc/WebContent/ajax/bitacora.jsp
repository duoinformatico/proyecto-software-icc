<%@ page language="java"  pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<script type="text/javascript" src="js/validaciones_alumno.js"></script>


<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
	        <h4 class="modal-title" id="myModalLabel">Bitácora || Desde ${bitacora.bit_fecha_ini}  hasta ${bitacora.bit_fecha_fin}</h4>
	    </div>
	      <div class="modal-body">
	      							        			
	        <legend>Ultima Edición: ${bitacora.bit_fecha_edicion} a las ${bitacora.bit_hora_edicion}</legend>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 columna">
			<form class="form-horizontal" id="#" role="form" method="POST" enctype="multipart/form-data" action="formularioBitacoraServlet.do">
				<script type="text/javascript">
					$(document).ready(function () {
							$('#descripcion').bind('drop', function (e) {
						           e.preventDefault();
						        });
					        $("#descripcion").focusin(function() {
					        	$("#descripcion").attr('autocomplete', 'off');
					        });
					        $("#descripcion").focusout(function() {
					        	$("#descripcion").removeAttr('autocomplete');
					        });
					});
				</script>
				<label class="control-label" for="descripcion">Descripción Bitácora:</label>
				<textarea class="form-control no_resize" id="descripcion" rows="6" name="descripcion" placeholder="- Ejemplo:Día Lunes trabajé supervizando obra." onkeypress="return funcionMaestra(event,'descri','2')">${bitacora.bit_descripcion}</textarea>
				
				<br>
				<div class="alert alert-warning alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				  <span class="glyphicon glyphicon-warning-sign form-control-feedback"></span><strong> Advertencia!</strong> Una vez guardada la bitácora usted no podrá deshacer los cambios de las asignaturas tributadas.(Solo puede tributar un máximo de 9 Asignaturas por bitácora semanalmente)
				</div>
				<div class="form-group">
					<label class="control-label" for="asignatura">Tributación de Asignaturas:</label><br>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					    <select class="form-control" multiple="multiple" id="asignatura" name="asignatura">
							<c:forEach var="asignatura" items="${asignaturasList}" >
								<option value="${asignatura.asi_codigo}">${asignatura.asi_nombre}</option>
							</c:forEach>
					    </select>
				    </div>
				 </div>				 
				 <script type="text/javascript">
				 	var ultima_seleccion_valida = null;
					$(document).ready(function(){	
						$('#asignatura').multiSelect();					
						
						$('#asignatura').change(function(event) {
						  if ($(this).val().length > $('#actual_asignatura').val()) {
						    alert('Le Quedan por Tributar '+$('#actual_asignatura').val()+' Asignaturas');
						    $(this).multiSelect('deselect_all');
						    //$(this).multiSelect('deselect', $(this)[$(this).size()-1]);
						  } else {
							  //alert($(this).val());
							  //alert($(this).multiSelect('deselect', $(this)[1].val()));
							  //alert($("#comision").val());
							  ultima_seleccion_valida = $(this).val();
						  }
						});				
					});
				 </script>			
				<div class="alert alert-warning alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				  <span class="glyphicon glyphicon-warning-sign form-control-feedback"></span><strong> Advertencia!</strong> El peso máximo por imagen no puede exceder los 500 KiloBytes y además deben venir en alguno de estos dos formatos (.png .jpg). 
				</div>	
				<div class="form-group">
					<label class="col-xs-6 col-sm-6 col-md-6 col-lg-6 control-label" for="descripcion">Adjuntar Imagénes:</label>
					<button type="button" id="imagen" name="imagen" value="imagen" class="btn btn-primary  btn-xs"  onclick='agregarImagen()'><span class="glyphicon glyphicon-plus"></span> Agregar Imagen</button>
					<button type="button" id="imagen" name="imagen" value="imagen" class="btn btn-danger  btn-xs"  onclick='quitarImagen()'><span class="glyphicon glyphicon-minus"></span> Quitar Imagen</button>
				</div>
				<div class="form-group">	
					<div id="div_imagen_1"></div>
					<div id="div_imagen_2"></div>
					<div id="div_imagen_3"></div>
					<div id="div_imagen_4"></div>
					<div id="div_imagen_5"></div>
					<div id="div_imagen_6"></div>
					<div id="div_imagen_7"></div>
					<div id="div_imagen_8"></div>
					<div id="div_imagen_9"></div>
					<div id="div_imagen_10"></div>
				</div>
				<button type="submit" class="btn btn-primary" onclick="return validar_bitacora(event)"><span class="glyphicon glyphicon-floppy-disk"></span> Guardar</button>
				<input type="hidden" id="bit_id" name="bit_id" value="${bitacora.bit_id}">
				<input type="hidden" id="pra_id" name="pra_id" value="${bitacora.pra_id}">
				<input type="hidden" id="plan_id" name="plan_id" value="${plan_id}">
				<input type="hidden" id="num_imagenes" name="num_imagenes" value="${bitacora.bit_n_imagen}">
				<input type="hidden" id="actual_imagenes" name="actual_imagenes" value="${actual_imagen}">
				<input type="hidden" id="actual_asignatura" name="actual_asignatura" value="${actual_asignatura}">
			</form>
	      </div>
	      <div class="modal-footer">
	      <!-- 
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	        <button type="button" class="btn btn-primary">Guardar</button>
	        -->
	     </div>
    </div>
  </div>
</div>
</div>
