<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<script type="text/javascript" src="js/validaciones_admin.js"></script>
<div id="myModal_modif" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	 <div class="modal-dialog modal-lg">
	    <div class="modal-content">
	    	<div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		        <h4 class="modal-title" id="myModalLabel">Modificar Plan de Estudio</h4>
		    </div>
		    <div class="modal-body">
		    	<form class="form-horizontal" role="form" method="POST" action="modificarPlanServlet.do">
		    		<input type="hidden" name="plan_id" id="plan_id" value="${id}">
					<div class="form-group">
					<label class="col-xs-3 control-label" for="nombre">Nombre:</label>
						<div class="col-xs-9">
							<input type="text" class="form-control" id="nombre" name="nombre" value="${plan.plan_nombre}" placeholder="Ej: Malla 2015 Ing Cosntrucción" onkeypress="return funcionMaestra(event,'nombres','1')">
						</div>
					</div>			
					<div class="form-group">
						<label class="col-xs-3 control-label" for="carrera">Carrera:</label>
						<div class="col-xs-9">							
							<select class="form-control" id="carrera" name="carrera" required>
							<option value="">Seleccione Carrera...</option>
							<c:forEach var="carrera" items="${carrerasList}" >							
								<c:if test="${carrera.car_codigo == plan.car_codigo}" >
									<option value="${carrera.car_codigo}" selected="selected">${carrera.car_nombre}</option>
								</c:if>
								<c:if test="${carrera.car_codigo != plan.car_codigo}" >
									<option value="${carrera.car_codigo}">${carrera.car_nombre}</option>
								</c:if>
							</c:forEach>
							</select>							
						</div>
					</div>
					<div class="form-group">
						<label class="col-xs-3 control-label" for="estado">Estado</label>
						<div class="col-xs-9">
							<select  class="form-control" id="estado" name="estado" size="1">
								<c:if test="${plan.plan_estado == 'activo'}" >			
									<option value="activo" selected="selected">Activo</option>
								</c:if>
								<c:if test="${plan.plan_estado != 'activo'}" >			
									<option value="activo">Activo</option>
								</c:if>
								<c:if test="${plan.plan_estado == 'inactivo'}" >
									<option value="inactivo" selected="selected">Inactivo</option>
								</c:if>
								<c:if test="${plan.plan_estado != 'inactivo'}" >
									<option value="inactivo">Inactivo</option>
								</c:if>
							</select>
						</div>	
					</div>
					<div class="col-xs-offset-4 col-sm-offset-4 col-md-offset-4 col-lg-offset-4">
						<button type="submit"  class="btn btn-primary">Guardar Cambios</button>
					</div>					
				</form>
		    </div>
		</div>
	</div>
</div>