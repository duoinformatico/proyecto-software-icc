<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
			 <div class="modal-dialog modal-lg">
			    <div class="modal-content">
			    	<div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				        <h4 class="modal-title" id="myModalLabel">Detalle Empresa</h4>
				    </div>
					    <div class="modal-body">
						    <form class="form-horizontal">
						    	<div class="form-group">
										<label class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1  control-label" for="rut_detalle">Rut:</label>
										<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
											<input type="text" class="form-control" id="rut_detalle" name="rut_detalle" value="${EMPRESA.emp_rut}" readonly="readonly">
										</div>
								</div>
								<div class="form-group">
										<label class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1  control-label" for="comuna_detalle">Comuna:</label>
										<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
											<input type="text" class="form-control" id="comuna_detalle" name="comuna_detalle" value="${COMUNA.com_nombre}" readonly="readonly">
										</div>
								</div>
								<div class="form-group ">
										<label class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1  control-label" for="nombre_detalle">Nombre:</label>
										<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
											<input type="text" class="form-control" id="nombre_detalle" name="nombre_detalle" value="${EMPRESA.emp_nombre}" readonly="readonly">
										</div>
								</div>
								<div class="form-group">
										<label class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1  control-label" for="direccion_detalle">Dirección:</label>
										<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
											<input type="text" class="form-control" id="direccion_detalle" name="direccion_detalle" value="${EMPRESA.emp_direccion}" readonly="readonly">
										</div>
								</div>
								<div class="form-group">
										<label class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1  control-label" for="telefono_detalle">Teléfono Fijo:</label>
										<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
											<input type="text" class="form-control" id="telefono_detalle" name="telefono_detalle" value="${EMPRESA.emp_telefono}" readonly="readonly">
										</div>
								</div>
								<div class="form-group">
										<label class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1  control-label" for="celular_detalle">Celular:</label>
										<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
											<input type="tel" maxlength="20" class="form-control" id="celular_detalle" name="celular_detalle" value="${EMPRESA.emp_celular}" readonly="readonly">
										</div>
								</div>
								<div class="form-group">
										<label class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 control-label" for="descripcion_detalle">Descripción:</label>
										<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
											<textarea class="form-control no_resize" id="descripcion_detalle name="descripcion_detalle" readonly="readonly">${EMPRESA.emp_descripcion}</textarea>
										</div>
								</div>
								<div class="form-group ">
										<label class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1  control-label" for="email_detalle">Email:</label>
										<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
											<input type="email" class="form-control" id="email_detalle" name="email_detalle" value="${EMPRESA.emp_email}" readonly="readonly">						
										</div>
								</div>
								<div class="form-group">
										<label class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1  control-label" for="web_detalle">Pagina Web:</label>
										<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
											<input type="url" class="form-control" id="web_detalle" name="web_detalle" value="${EMPRESA.emp_web}" readonly="readonly">
										</div>
								</div>
					    
					    </form>
					   </div>
				    </div>
				</div>
			</div>
</div>
		