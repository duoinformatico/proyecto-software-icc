
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page language="java" pageEncoding="UTF-8"%>
<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
	        <h4 class="modal-title" id="myModalLabel">Rúbrica</h4>
	    </div>
	      <div class="modal-body">
	      	<legend>${instrumento.ins_nombre}</legend>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 columna">
			<form class="form-horizontal" id="formulario_evaluacion_supervisor" role="form" method="POST" action="FormularioRegistroEvaluacionControlador">
			<div class="table-responsive">
				<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-hover" id="datatable">
					<thead>
					  <tr >
					     <th colspan="2">Evaluación</th>
					     <th colspan=${escala.esc_num_opciones}>Criterios de Evaluación</th>
					  </tr>
			        </thead>
			        <tbody>
			        	<tr>
			        		<td colspan="2"><b>Indivudual</b></td>
			        		<c:forEach items="${escala.valores}" var="valores" >
			        			<td align="center">${valores.val_nombre}</td>
			        		</c:forEach>        		
						</tr>
						<tr>
							<td><b>Categoría</b></td>
							<td><b>Concepto</b></td>
			        		<c:forEach items="${escala.valores}" var="valores" >
			        		<fmt:parseNumber var="valor" pattern="#" integerOnly="true" value="${valores.val_valor}" />
			        			<td align="center">${valor}</td>
			        		</c:forEach>  
						</tr>
						<c:forEach items="${instrumento.items}" var="items" varStatus="stat">
						<tr>
			        			<c:forEach items="${categoriasList}" var="categoria" >
			        				<c:if test="${items.cat_id == categoria.cat_id}">
			        					<td><b>${categoria.cat_nombre}</b></td>
			        				</c:if>			        				
			        			</c:forEach>
			        			<td><p class="text-left">${items.item_nombre}</p></td>
			        			<c:forEach items="${items.opciones}" var="opciones" >
			        				
				        			<td class="vert-align" align="center">
				        					<input style="padding: 50px;"data-toggle="tooltip" data-placement="bottom" title="${opciones.opc_descripcion}" type="radio" id="${opciones.item_id}" name="${opciones.item_id}" value="${opciones.opc_valor}" required onchange='calcularPuntaje()'>
				        			</td>			        				
			        			</c:forEach>
			        	</tr>
			        		<c:set var="cadena_items" value="${stat.first ? '' : cadena_items}${items.item_id}-" />
			        	</c:forEach>
	        	</tbody>
		    </table>
			</div>
			<script type="text/javascript">
			$(document).ready(function () {
					$('#observacion').bind('copy paste drop', function (e) {
			           e.preventDefault();
			        });       
			        $("#observacion").focusin(function() {
			        	$("#observacion").attr('autocomplete', 'off');
			        });
			        $("#observacion").focusout(function() {
			        	$("#observacion").removeAttr('autocomplete');
			        });
			});
		    </script>
		    <c:if test="${tipo == 'alumno'}">
			<div class="form-group">
				<label class="control-label">Indique las Áreas en que Trabajó durante la Práctica</label>
			</div>			
			<div class="form-group">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				    <select class="form-control" multiple="multiple" id="area" name="area">
						<c:forEach var="area" items="${areasPracticaList}" >
								<option value="${area.area_id}">${area.area_nombre}</option>						
						</c:forEach>
				    </select>
			    </div>
			 </div>
			 <div class="form-group">
				<label class="control-label">Indique las Funciones que realizó durante la Práctica</label>
			</div>			
			<div class="form-group">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				    <select class="form-control" multiple="multiple" id="funcion" name="funcion">
						<c:forEach var="funcion" items="${funcionesPracticaList}" >
								<option value="${funcion.fun_id}">${funcion.fun_nombre}</option>						
						</c:forEach>
				    </select>
			    </div>
			 </div>
			 <script type="text/javascript">
			 	var ultima_seleccion_valida = null;
				$(document).ready(function(){	
					$('#area').multiSelect({ keepOrder: true });					
					
					$('#area').change(function(event) {						
						if ($(this).val().length > 5) {
						  alert('Solo puede seleccionar un maximo de 5 areas');
						  var x = $('#area').val();
						  $(this).multiSelect('deselect', x[x.length - 1]);
						}
						else {
						 ultima_seleccion_valida = $(this).val();
						}
					});
					
					$('#funcion').multiSelect({ keepOrder: true });					
					
					$('#funcion').change(function(event) {						
						if ($(this).val().length > 5) {
						  alert('Solo puede seleccionar un maximo de 5 funciones');
						  var x = $('#funcion').val();
						  $(this).multiSelect('deselect', x[x.length - 1]);
						}
						else {
						 ultima_seleccion_valida = $(this).val();
						}
					});	
				});
			 </script>
			 </c:if>
			 <c:if test="${tipo != 'alumno'}">
				<div class="form-group">
					<label class="control-label">Áreas en que Trabajó el Alumno durante la Práctica</label>
				</div>			
				<div class="table-responsive">
					<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-hover">
						<thead>
							<tr>
								<td align="center">Área</td>
							</tr>
						</thead>
						<c:forEach var="areaAlumno" items="${areasAlumnoList}" >
						<tr>
							<c:forEach var="area" items="${areasPracticaList}" >
								<c:if test="${areaAlumno.area_id == area.area_id}">
									<td align="center">${area.area_nombre}</td>
								</c:if>														
							</c:forEach>
						</tr>				
						</c:forEach>
					</table>							
				</div>
				
				<div class="form-group">
					<label class="control-label">Funciones que realizó el Alumno durante la Práctica</label>
			</div>			
			<div class="table-responsive">
				<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-hover">
					<thead>
						<tr>
							<td align="center">Funciones</td>
						</tr>
					</thead>
					<c:forEach var="funcionAlumno" items="${funcionesAlumnoList}" >
					<tr>
						<c:forEach var="funcion" items="${funcionesPracticaList}" >
							<c:if test="${funcionAlumno.fun_id == funcion.fun_id}">
								<td align="center">${funcion.fun_nombre}</td>
							</c:if>														
						</c:forEach>
					</tr>				
					</c:forEach>
				</table>							
			</div>
			 </c:if>
			<label class="control-label" for="observacion">Observaciones a la Evaluación y/o al desempeño del Estudiante:</label>
			<textarea class="form-control no_resize" id="observacion" name="observacion" placeholder="Observaciones..." onkeypress="return funcionMaestra(event,'descri','1')"></textarea>			
       		<br>
       		<div class="form-group">
			<label class="col-xs-1 control-label" for="puntaje">Puntaje:</label>
				<div class="col-xs-2">
					<input type="text" maxlength="4" class="form-control" id="puntaje" name="puntaje" value="0" readonly="readonly">
				</div>
			<c:if test="${tipo != 'alumno'}">
				<label class="col-xs-1 control-label" for="nota">Nota:</label>
				<div class="col-xs-2">
						<input type="text" maxlength="4" class="form-control" id="nota" name="nota" value="1.0" readonly="readonly"></td>
					</div>
				</div>
			</c:if>
			<c:if test="${tipo == 'alumno'}">
				<div class="col-xs-2">
						<input type="hidden" maxlength="4" class="form-control" id="nota" name="nota" value="1.0" readonly="readonly"></td>
					</div>
				</div>
			</c:if>			
			<div class="alert alert-warning alert-dismissible" role="alert">
			  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			  <span class="glyphicon glyphicon-warning-sign form-control-feedback"></span><strong> Advertencia!</strong> Una vez enviada la evaluación esta no podrá ser modificada.
			</div>
			<button type="submit" class="btn btn-primary" onclick="return validar_evaluacion()">Registrar Evaluación</button>
			<input type="hidden" name="num_items" value="${fn:length(instrumento.items)}">
			<input type="hidden" id="max_puntaje" name="max_puntaje" value="${instrumento.ins_max_puntaje}">
			<input type="hidden" id="nombre_items" name="nombre_items" value="${cadena_items}">
			<input type="hidden" name="pra_id" value="${pra_id}">
			<input type="hidden" name="rut" value="${rut}">
			<input type="hidden" name="rol_id" value="${rol_id}">
			<input type="hidden" name="tipo" value="${tipo}">
			</form>
	      </div>
	      <div class="modal-footer">
	      <!-- 
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	        <button type="button" class="btn btn-primary">Guardar</button>
	        -->
	     </div>
    </div>
  </div>
</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	var nombre_items = $("#nombre_items").val();
	var items = nombre_items.split("-");
	//alert("Cantidad Items Rubrica = "+items.length+"\nElementos"+items);
	for(var i=0;i <= items.length; i++){
		$("#items["+i+"]").tooltip({
	        placement : 'bottom'
	    });
		//alert($("#items["+i+"]"));
		$("input[name='"+items[i]+"']").tooltip({
        	placement : 'bottom'
    	});
	}
});
</script>