<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<div class="form-group">
	<label class="col-xs-3 control-label" for="asignatura">Asignatura:</label>
	<div class="col-xs-9">							
		<select class="form-control" id="asignatura" name="asignatura" required>
		<option value="">Seleccione Asignatura...</option>
		<c:forEach var="asignatura" items="${asignaturasList}" >
			<option value="${asignatura.asi_codigo}-${asignatura.plan_id}">${asignatura.asi_nombre} (${asignatura.asi_codigo}-${asignatura.plan_id})</option>
		</c:forEach>
		</select>							
	</div>
</div>
<script type="text/javascript">
$(document).ready(function() {
	$("#asignatura").change(actualizarListaRoles);
});
</script>
