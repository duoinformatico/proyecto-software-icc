<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<link rel="stylesheet" href="css/alertify.core.css" />
<link rel="stylesheet" href="css/alertify.default.css" id="toggleCSS" />
<script src="js/alertify.min.js"></script>
<script type="text/javascript" src="js/validaciones_admin.js"></script>
<div id="myModal_modif" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	 <div class="modal-dialog modal-lg">
	    <div class="modal-content">
	    	<div class="modal-header">
	    		<script type="text/javascript">
					$('#salir').click(function() {	 
				    	      location.reload();
					});
				</script>
		        <button type="button" id="salir" name="salir" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		        <h4 class="modal-title" id="myModalLabel">Modificar Académico</h4>
		    </div>
		    <div class="modal-body">
		    	<form class="form-horizontal" id="formulario_academico_modif" role="form" method="POST" action="modificarAcademicoServlet.do">
					<div class="form-group">
					<label class="col-xs-3 control-label" for="rut_modif">Rut:</label>
						<div class="col-xs-9">
							<input type="text" class="form-control" id="rut_modif" name="rut_modif" placeholder="Rut Académico" value="${academico.aca_rut}" readonly="readonly">
						</div>
					</div>
					<div class="form-group">
					<label class="col-xs-3 control-label" for="departamento_modif">Depto:</label>
						<div class="col-xs-9">
							<select class="form-control" id="departamento_modif" name="departamento_modif">			
									<option value="${departamento.dep_id}">${departamento.dep_nombre}</option>
							</select>
						</div>
					</div>
					<script type="text/javascript">
					$(document).ready(function () {
							$('#nombre_modif').bind('copy paste drop', function (e) {
					           e.preventDefault();
					        });       
					        $("#nombre_modif").focusin(function() {
					        	$("#nombre_modif").attr('autocomplete', 'off');
					        });
					        $("#nombre_modif").focusout(function() {
					        	$("#nombre_modif").removeAttr('autocomplete');
					        });
					        $('#paterno_modif').bind('copy paste drop', function (e) {
						           e.preventDefault();
						        });
					        $("#paterno_modif").focusin(function() {
					        	$("#paterno_modif").attr('autocomplete', 'off');
					        });
					        $("#paterno_modif").focusout(function() {
					        	$("#paterno_modif").removeAttr('autocomplete');
					        });
					        $('#materno_modif').bind('copy paste drop', function (e) {
						           e.preventDefault();
						        });
					        $("#materno_modif").focusin(function() {
					        	$("#materno_modif").attr('autocomplete', 'off');
					        });
					        $("#materno_modif").focusout(function() {
					        	$("#materno_modif").removeAttr('autocomplete');
					        });
					        $('#email_modif').bind('copy paste drop', function (e) {
						           e.preventDefault();
						        });
					        $("#email_modif").focusin(function() {
					        	$("#email_modif").attr('autocomplete', 'off');
					        });
					        $("#email_modif").focusout(function() {
					        	$("#email_modif").removeAttr('autocomplete');
					        });
					        $('#telefono_modif').bind('copy paste drop', function (e) {
						           e.preventDefault();
						        });
					        $("#telefono_modif").focusin(function() {
					        	$("#telefono_modif").attr('autocomplete', 'off');
					        	if($("#codigo_modif").val() == ""){
					        		$("#telefono_modif").attr('readonly', 'readonly');
					        		$("#codigo_modif").focus();
					        	}
					        	else{
					        		$("#telefono_modif").removeAttr('readonly');
					        	}
					        	if($("#codigo_modif").val() != ""){
					        		if($("#codigo_modif").val() == 2){
					        			$("#telefono_modif").attr('maxlength', '8');
					        		}
					        		else{
					        			$("#telefono_modif").attr('maxlength', '7');
					        		}
					        	}					        	
					        });
							$("#codigo_modif").change(function(){					        	
								if($("#codigo_modif").val() != ""){
					        		$("#telefono_modif").focus();
					        		$("#telefono_modif").val("");
					        	}					        	
					        });
					        $("#telefono_modif").change(function(){					        	
					        	
					        	if($("#telefono_modif").val().length == 8 && $("#codigo_modif").val() == 2){
					        		$("#telefono_modif").val("+56-"+$("#codigo_modif").val()+"-"+$("#telefono_modif").val());
					        	}
					        	if($("#telefono_modif").val().length == 7 && $("#codigo_modif").val() != 2){
					        		$("#telefono_modif").val("+56-"+$("#codigo_modif").val()+"-"+$("#telefono_modif").val());
					        	}
					        });					        	
					        
					        $("#telefono_modif").focusout(function() {
					        	$("#telefono_modif").removeAttr('autocomplete');
					        	$("#telefono_modif").removeAttr('maxlength');
					        	if($("#codigo_modif").val() == ""){
					        		$("#telefono_modif").attr('readonly', 'readonly');
					        		$("#codigo_modif").focus();
					        	}
					        	else{
					        		$("#telefono_modif").removeAttr('readonly');
					        	}
					        	if($("#telefono_modif").val().length == 8 && $("#codigo_modif").val() == 2){
					        		$("#telefono_modif").val("+56-"+$("#codigo_modif").val()+"-"+$("#telefono_modif").val());
					        	}
					        	if($("#telefono_modif").val().length < 8 && $("#codigo_modif").val() == 2){
					        		$("#telefono_modif").val("");
					        	}
					        	if($("#telefono_modif").val().length == 7 && $("#codigo_modif").val() != 2){
					        		$("#telefono_modif").val("+56-"+$("#codigo_modif").val()+"-"+$("#telefono_modif").val());
					        	}
					        	if($("#telefono_modif").val().length < 7 && $("#codigo_modif").val() != 2){
					        		$("#telefono_modif").val("");
					        	}
					        });
					        $('#celular_modif').bind('copy paste drop', function (e) {
						           e.preventDefault();
						        });
					        $("#celular_modif").focusin(function() {
					        	$("#celular_modif").attr('autocomplete', 'off');
					        	$("#celular_modif").attr('maxlength', '8');					        	
					        });
					       $("#celular_modif").focusout(function() {
					        	$("#celular_modif").removeAttr('autocomplete');
					        	$("#celular_modif").removeAttr('maxlength');
					        	if($("#celular_modif").val().length < 8){
					        		$("#celular_modif").val("");
					        	}
					        	if($("#celular_modif").val().length == 8){
					        		$("#celular_modif").val("+569"+$("#celular_modif").val());
					        	}
					        });
					        $('#estado_modif').bind('copy paste drop', function (e) {
						           e.preventDefault();
						        });
					        $("#estado_modif").focusin(function() {
					        	$("#estado_modif").attr('readonly', 'readonly');
					        	$("#estado_modif").attr('autocomplete', 'off');
					        });
					        $("#estado_modif").focusout(function() {
					        	$("#estado_modif").removeAttr('readonly');
					        	$("#estado_modif").removeAttr('autocomplete');
					        });
				      });
				    </script>
					<div class="form-group">
						<label class="col-xs-3 control-label" for="nombre_modif">Nombres</label>
						<div class="col-xs-9">
							<input type="text" class="form-control" id="nombre_modif" name="nombre_modif" placeholder="Nombres" value="${academico.aca_nombres}" onkeypress="return funcionMaestra(event,'nombres','1')">
						</div>	
					</div>
					<div class="form-group">
						<label class="col-xs-3 control-label" for="paterno_modif">Apellido Paterno</label>
						<div class="col-xs-9">
							<input type="text" class="form-control" id="paterno_modif" name="paterno_modif" placeholder="Apellido Paterno" value="${academico.aca_apellido_p}" onkeypress="return funcionMaestra(event,'nombres','1')">
						</div>	
					</div>
					<div class="form-group">
						<label class="col-xs-3 control-label" for="materno_modif">Apellido Materno</label>
						<div class="col-xs-9">
							<input type="text" class="form-control" id="materno_modif" name="materno_modif" placeholder="Apellido Paterno" value="${academico.aca_apellido_m}" onkeypress="return funcionMaestra(event,'nombres','1')">
						</div>	
					</div>
					<div class="alert alert-warning alert-dismissible" role="alert">
					  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					  <span class="glyphicon glyphicon-remove"></span>
					  <strong>Advertencia</strong> Es muy importante que el email modificado sea correcto, por favor verificar cuidadosamente al momento de ingresar.
					</div>
					<div class="form-group">
						<label class="col-xs-3 control-label" for="email_modif">E-Mail:</label>
						<div class="col-xs-9">
							<div class="input-group">
								<input type="email" class="form-control" id="email_modif" name="email_modif" placeholder=" ejemplo@ejempo.com" value="${academico.aca_email}" onkeypress="return funcionMaestra(event,'email','2')"> 
								<span class="input-group-addon">
									@
								</span>
							</div>						
						</div>
					</div>
					<div class="form-group">
						<label class="col-xs-3 control-label" for="telefono_modif">Teléfono:</label>
						<div class="col-xs-4">	
							<select class="form-control" id="codigo_modif" name="codigo_modif">
								<option value="">Código Area...</option>
								<optgroup label="Zona Norte">
									<c:if test="${codigo == 58}" >			
										<option value="58" selected="selected">(58) Arica-Parinacota</option>
									</c:if>
									<c:if test="${codigo != 58}" >			
										<option value="58">(58) Arica-Parinacota</option>
									</c:if>
									<c:if test="${codigo == 57}" >		
										<option value="57" selected="selected">(57) Iquique-Tamarugal</option>	
									</c:if>
									<c:if test="${codigo != 57}" >			
										<option value="57">(57) Iquique-Tamarugal</option>
									</c:if>
									<c:if test="${codigo == 55}" >			
										<option value="55" selected="selected">(55) Tocopilla-Loa-Antofagasta</option>
									</c:if>
									<c:if test="${codigo != 55}" >			
										<option value="55">(55) Tocopilla-Loa-Antofagasta</option>
									</c:if>
									<c:if test="${codigo == 51}" >			
										<option value="51" selected="selected">(51) Huasco</option>
									</c:if>
									<c:if test="${codigo != 51}" >			
										<option value="51">(51) Huasco</option>
									</c:if>
									<c:if test="${codigo == 52}" >			
										<option value="52" selected="selected">(52) Chañaral-Copiapó</option>
									</c:if>
									<c:if test="${codigo != 52}" >			
										<option value="52">(52) Chañaral-Copiapó</option>
									</c:if>
									<c:if test="${codigo == 51}" >			
										<option value="51" selected="selected">(51) Elqui</option>
									</c:if>
									<c:if test="${codigo != 51}" >			
										<option value="51">(51) Elqui</option>
									</c:if>
									<c:if test="${codigo == 53}" >			
										<option value="53" selected="selected">(53) Choapa-Limarí</option>
									</c:if>
									<c:if test="${codigo != 53}" >			
										<option value="53">(53) Choapa-Limarí</option>
									</c:if>
								</optgroup>
								<optgroup label="Zona Centro">
									<c:if test="${codigo == 32}" >			
										<option value="32" selected="selected">(73)>(32) Isla de Pascua-Valparaíso-Marga Marga(Quilpué-Villa Alemana)</option>
									</c:if>
									<c:if test="${codigo != 32}" >			
										<option value="32">(32) Isla de Pascua-Valparaíso-Marga Marga(Quilpué-Villa Alemana)</option>
									</c:if>
									<c:if test="${codigo == 33}" >		
										<option value="33" selected="selected">(73)>(33) Petorca-Quillota-Marga Marga(Limache y Olmué)</option>
									</c:if>
									<c:if test="${codigo != 33}" >			
										<option value="33">(33) Petorca-Quillota-Marga Marga(Limache y Olmué)</option>
									</c:if>
									<c:if test="${codigo == 34}" >			
										<option value="34" selected="selected">(73)>(34) Los Andes-San Felipe de Aconcagua</option>
									</c:if>
									<c:if test="${codigo != 34}" >			
										<option value="34">(34) Los Andes-San Felipe de Aconcagua</option>
									</c:if>
									<c:if test="${codigo == 35}" >		
										<option value="35" selected="selected">(73)>(35) San Antonio</option>	
									</c:if>
									<c:if test="${codigo != 35}" >			
										<option value="35">(35) San Antonio</option>
									</c:if>
									<c:if test="${codigo == 2}" >			
										<option value="2" selected="selected">(73)> ( 2) Chacabuco-Cordillera-Maipo-Melipilla-Santiago-Talagante</option>
									</c:if>
									<c:if test="${codigo != 2}" >			
										<option value="2"> ( 2) Chacabuco-Cordillera-Maipo-Melipilla-Santiago-Talagante</option>
									</c:if>
									<c:if test="${codigo == 72}" >			
										<option value="72" selected="selected">(73)>(72) Cachapoal-ardenal Caro-Colchagua</option>
									</c:if>
									<c:if test="${codigo != 72}" >			
										<option value="72">(72) Cachapoal-ardenal Caro-Colchagua</option>
									</c:if>
									<c:if test="${codigo == 71}" >			
										<option value="71" selected="selected">(73)>(71) Talca</option>
									</c:if>
									<c:if test="${codigo != 71}" >			
										<option value="71">(71) Talca</option>
									</c:if>
									<c:if test="${codigo == 73}" >			
										<option value="73" selected="selected">(73) Cauquenes-Linares</option>
									</c:if>
									<c:if test="${codigo != 73}" >				
										<option value="73">(73) Cauquenes-Linares</option>
									</c:if>
									<c:if test="${codigo == 75}" >			
										<option value="75" selected="selected">(75) Curicó</option> 
									</c:if>
									<c:if test="${codigo != 75}" >			
										<option value="75">(75) Curicó</option>
									</c:if>
									
								</optgroup>
								<optgroup label="Zona Sur">
									<c:if test="${codigo == 41}" >			
										<option value="41" selected="selected">(41) Arauco-Concepción </option>
									</c:if>
									<c:if test="${codigo != 41}" >			
										<option value="41">(41) Arauco-Concepción </option>
									</c:if>
									<c:if test="${codigo == 42}" >			
										<option value="42" selected="selected">(42) Ñuble</option>
									</c:if>
									<c:if test="${codigo != 42}" >			
										<option value="42">(42) Ñuble</option>
									</c:if>
									<c:if test="${codigo == 43}" >			
										<option value="43" selected="selected">(43) Bío-Bío</option>
									</c:if>
									<c:if test="${codigo != 43}" >			
										<option value="43">(43) Bío-Bío</option>
									</c:if>
									<c:if test="${codigo == 45}" >			
										<option value="45" selected="selected">(45) Cautín-Malleco</option>
									</c:if>
									<c:if test="${codigo != 45}" >			
										<option value="45">(45) Cautín-Malleco</option>
									</c:if>
									<c:if test="${codigo == 63}" >		
										<option value="63" selected="selected">(63) Ranco-Valdivia</option>
									</c:if>
									<c:if test="${codigo != 63}" >			
										<option value="63">(63) Ranco-Valdivia</option>
									</c:if>
									<c:if test="${codigo == 64}" >			
										<option value="64" selected="selected">(64) Osorno</option>
									</c:if>
									<c:if test="${codigo != 64}" >			
										<option value="64">(64) Osorno</option>
									</c:if>
									<c:if test="${codigo == 65}" >			
										<option value="65" selected="selected">(65) Chiloé-Llanquihue-Palena </option>
									</c:if>
									<c:if test="${codigo != 65}" >			
										<option value="65">(65) Chiloé-Llanquihue-Palena </option>
									</c:if>
									<c:if test="${codigo == 67}" >			
										<option value="67" selected="selected">(67) Aisén-Capitán Prat-Coihaique-General Carrera</option>
									</c:if>
									<c:if test="${codigo != 67}" >			
										<option value="67">(67) Aisén-Capitán Prat-Coihaique-General Carrera</option>
									</c:if>
									<c:if test="${codigo == 68}" >			
										<option value="68" selected="selected">(68) Aisén(Puyuhuapi)</option>
									</c:if>
									<c:if test="${codigo != 68}" >			
										<option value="68">(68) Aisén(Puyuhuapi)</option>
									</c:if>
									<c:if test="${codigo == 61}" >			
										<option value="61" selected="selected">(61) Antártica Chilena-Magallanes-Tierra del Fuego-Última Esperanza</option>
									</c:if>
									<c:if test="${codigo != 61}" >			
										<option value="61">(61) Antártica Chilena-Magallanes-Tierra del Fuego-Última Esperanza</option>
									</c:if>
								</optgroup>	
							</select>							
						</div>
						<div class="col-xs-5">
							<div class="input-group">
								<c:if test="${bandera == 0}" >
									<input type="tel" class="form-control" id="telefono_modif" name="telefono_modif" placeholder="Ej:2323421" value="+56-${codigo}-${telefono}" onkeypress="return funcionMaestra(event,'fijo',1)" >			
								</c:if>
								<c:if test="${bandera == 1}" >	
									<input type="tel" class="form-control" id="telefono_modif" name="telefono_modif" placeholder="Ej:2323421" value="" onkeypress="return funcionMaestra(event,'fijo',1)" >		
								</c:if>
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-phone-alt"></span>
								</span>
							</div>	
						</div>	
					</div>
					<div class="form-group">
						<label class="col-xs-3 control-label" for="celular_modif">Celular:</label>
						<div class="col-xs-9">
							<div class="input-group">
								<input type="tel" class="form-control" id="celular_modif" name="celular_modif" placeholder="Ej: +56998345670" value="${academico.aca_celular}" onkeypress="return funcionMaestra(event,'celular',1)">
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-phone"></span>
								</span>
							</div>	
						</div>	
					</div>
					<div class="form-group">
						<label class="col-xs-3 control-label" for="estado_modif">Estado</label>
						<div class="col-xs-9">
							<select class="form-control" id="estado_modif" name="estado_modif">	
								<c:if test="${academico.aca_estado == 'activo'}" >			
									<option value="activo" selected="selected">Activo</option>
								</c:if>
								<c:if test="${academico.aca_estado != 'activo'}" >			
									<option value="activo">Activo</option>
								</c:if>
								<c:if test="${academico.aca_estado == 'inactivo'}" >
									<option value="inactivo" selected="selected">Inactivo</option>
								</c:if>
								<c:if test="${academico.aca_estado != 'inactivo'}" >
									<option value="inactivo">Inactivo</option>
								</c:if>
							</select>						
				
						</div>	
					</div>
					<div class="col-xs-offset-4 col-sm-offset-4 col-md-offset-4 col-lg-offset-4">
						<button type="submit" onclick="return validar_academico(event,2)" class="btn btn-primary">Modificar Académico</button>
					</div>
				</form>
		    </div>
	    </div>
	 </div>
</div>
