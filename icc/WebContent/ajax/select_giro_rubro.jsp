<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<c:forEach var="i" begin="1" end="3" step="1">
	<c:if test="${n_giros == i}">
			<br>
			<select required class="form-control" id="giro_${i}" name="giro_${i}">	
				<option value="">Seleccione Giro...</option>
				<c:forEach var="giro" items="${girosList}" >
					<option value="${giro.gir_id}">${giro.gir_nombre}</option>
				</c:forEach>
			</select>
			<br>
			<div id="div_rubro_${i}">
				<select required class="form-control" id="rubro_${i}" name="rubro_${i}">	
					<option value="">Seleccione Rubro...</option>
				</select>
			</div>
	</c:if>
</c:forEach>
