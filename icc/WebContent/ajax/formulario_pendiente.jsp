<%@ page language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<script type="text/javascript" src="js/validaciones_admin.js"></script>
<script src="js/alertify.min.js"></script>
<link rel="stylesheet" href="css/alertify.core.css" />
<link rel="stylesheet" href="css/alertify.default.css" id="toggleCSS" />

<div id="myModal2" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	 <div class="modal-dialog modal-lg">
	    <div class="modal-content">
	    	<div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		        <h4 class="modal-title" id="myModalLabel">Pr�ctica de: ${alumno.alu_nombres} ${alumno.alu_apellido_p} ${alumno.alu_apellido_m}</h4>
		    </div>
		    <div class="modal-body">
		    	<form class="form-horizontal" id="formulario_practica" role="form" method="POST" action="pendienteServlet.do">
					<div class="col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3">
						<h3>Informaci�n Principal</h3>
					</div>
					<input type="hidden" name="id" id="id" value="${id}">
					<div class="form-group">
					<label class="col-xs-3 control-label" for="empresa">Empresa:</label>
						<div class="col-xs-9">
							<select class="form-control" id="empresa" name="empresa">				
								<c:forEach var="empresas" items="${empresaList}" >
									<c:if test="${empresas.emp_rut == pendiente.emp_rut}" >
										<option value="${empresas.emp_rut}" selected="selected">${empresas.emp_nombre}</option>
									</c:if>
									<c:if test="${empresas.emp_rut != pendiente.emp_rut}" >
										<option value="${empresas.emp_rut}">${empresas.emp_nombre}</option>
									</c:if>
								</c:forEach>
							</select>
						</div>
					</div>	
					<div id="div_supervisor">
							<div class="form-group ">
							<label class="col-xs-3 control-label" for="supervisor">Supervisores Activos :</label>
								<div class="col-xs-9">
									<select class="form-control" id="supervisor" name="supervisor">										
										<option value="${supervisor.sup_rut}">${supervisor.sup_nombres} ${supervisor.sup_apellido_p} ${supervisor.sup_apellido_m}</option>							
									</select>
								</div>
							</div>
					</div>
					<div class="form-group">
						<label class="col-xs-3 control-label" for="profesor">Profesor Gu�a:</label>
						<div class="col-xs-9">
							<select class="form-control" id="profesor" name="profesor">	
								<c:forEach var="academico" items="${academicosList}" >
									<c:if test="${academico.aca_rut == academico_elegido.aca_rut}" >
										<option value="${academico.aca_rut}" selected="selected">${academico.aca_nombres} ${academico.aca_apellido_p} ${academico.aca_apellido_m}</option>
									</c:if>
									<c:if test="${academico.aca_rut != academico_elegido.aca_rut}" >
										<option value="${academico.aca_rut}">${academico.aca_nombres} ${academico.aca_apellido_p} ${academico.aca_apellido_m}</option>
									</c:if>
								</c:forEach>
							</select>
						</div>						
					</div>					
					<div class="form-group">
					<label class="col-xs-3 control-label" for="obra">Obra:</label>
						<div class="col-xs-9">
							<select class="form-control" id="obra" name="obra">	
								<c:forEach var="obras" items="${obrasList}" >
									<c:if test="${obras.obr_id == pendiente.obr_id}" >
										<option value="${obras.obr_id}" selected="selected">${obras.obr_nombre}</option>
									</c:if>
									<c:if test="${obras.obr_id != pendiente.obr_id}" >
										<option value="${obras.obr_id}">${obras.obr_nombre}</option>
									</c:if>
								</c:forEach>
							</select>
						</div>
					</div>
					<script type="text/javascript">
					$(document).ready(function () {
						$('#alumno').bind('copy paste drop', function (e) {
					           e.preventDefault();
					    });       
					    $("#alumno").focusin(function() {
				        	$("#alumno").attr('autocomplete', 'off');
				        });
				        $("#alumno").focusout(function() {
				        	$("#alumno").removeAttr('autocomplete');
				        });
						$('#obra_nombre').bind('copy paste drop', function (e) {
					           e.preventDefault();
					    });       
					    $("#obra_nombre").focusin(function() {
				        	$("#obra_nombre").attr('autocomplete', 'off');
				        });
				        $("#obra_nombre").focusout(function() {
				        	$("#obra_nombre").removeAttr('autocomplete');
				        });
				        $('#direccion').bind('copy paste drop', function (e) {
					           e.preventDefault();
					        });
				        $("#direccion").focusin(function() {
				        	$("#direccion").attr('autocomplete', 'off');
				        });
				        $("#direccion").focusout(function() {
				        	$("#direccion").removeAttr('autocomplete');
				        });
				        $('#tareas').bind('copy paste drop', function (e) {
					           e.preventDefault();
					        });
				        $("#tareas").focusin(function() {
				        	$("#tareas").attr('autocomplete', 'off');
				        });
				        $("#tareas").focusout(function() {
				        	$("#tareas").removeAttr('autocomplete');
				        });
				        $('#inicio').bind('copy paste drop', function (e) {
					           e.preventDefault();
					        });
				        $("#inicio").focusin(function() {
				        	$("#inicio").attr('readonly', 'readonly');
				        	$("#inicio").attr('autocomplete', 'off');
				        });
				        $("#inicio").focusout(function() {
				        	$("#inicio").removeAttr('readonly');
				        	$("#inicio").removeAttr('autocomplete');
				        });
				        $('#termino').bind('copy paste drop', function (e) {
					           e.preventDefault();
					        });
				        $("#termino").focusin(function() {
				        	$("#termino").attr('readonly', 'readonly');
				        	$("#termino").attr('autocomplete', 'off');
				        });
				        $("#termino").focusout(function() {
				        	$("#termino").removeAttr('readonly');
				        	$("#termino").removeAttr('autocomplete');
				        });
				        $('#horas').bind('copy paste drop', function (e) {
					           e.preventDefault();
					        });
				        $("#horas").focusin(function() {
				        	$("#horas").attr('autocomplete', 'off');
				        });
				        $("#horas").focusout(function() {
				        	$("#horas").removeAttr('autocomplete');
				        });
					});
				    </script>
					<div class="form-group">
					<label class="col-xs-3 control-label" for="asignatura">Asignatura:</label>
						<div class="col-xs-9">
							<select class="form-control" id="asignatura" name="asignatura">				
								<c:forEach var="asignaturas" items="${gestionesList}" >
									<c:if test="${asignaturas.asi_codigo == pendiente.asi_codigo}" >
										<option value="${asignaturas.asi_codigo}" selected="selected">${asignaturas.asi_nombre}</option>
									</c:if>
								</c:forEach>
							</select>
						</div>
					</div>
					<div class="form-group ">
					<label class="col-xs-3 control-label" for="plan">Plan de Estudio:</label>
						<div class="col-xs-9">
							<select class="form-control" id="plan" name="plan">				
								<c:forEach var="planes" items="${planesList}" >
									<c:if test="${planes.plan_id == pendiente.plan_id}" >
										<option value="${planes.plan_id}" selected="selected">${planes.plan_nombre}</option>
									</c:if>
								</c:forEach>
							</select>						
						</div>
					</div>
					<div class="col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3">
						<h3>Informaci�n de Obra</h3>
					</div>
					<div class="form-group ">
					<label class="col-xs-3 control-label" for="obra_nombre">Nombre Obra:</label>
						<div class="col-xs-9">
							<input type="text" class="form-control" id="obra_nombre" name="obra_nombre" value="${pendiente.pra_obra_nombre}" onkeypress="return funcionMaestra(event,'nombres','1')">						
						</div>
					</div>
					<div class="form-group">
						<label class="col-xs-3 control-label" for="region">Regi�n:</label>
							<div class="col-xs-9">							
								<select class="form-control" id="region" name="region" required>
								<option value="">Seleccione Regi�n...</option>
								<c:forEach var="regiones" items="${regionList}" >
									<c:if test="${regiones.reg_id == region.reg_id}" >
											<option value="${region.reg_id}" selected="selected">${region.reg_nombre}</option>
									</c:if>
									<c:if test="${regiones.reg_id != region.reg_id}" >
										<option value="${regiones.reg_id}">${regiones.reg_nombre}</option>
									</c:if>
								</c:forEach>
								</select>							
							</div>
						</div>
						<div id="div_provincia">
							<div class="form-group">
							<label class="col-xs-3 control-label" for="provincia">Provincia:</label>
								<div class="col-xs-9">							
									<select class="form-control" id="provincia" name="provincia" required>
									<option value="">Seleccione Provincia...</option>
									<c:forEach var="provincias" items="${provinciaList}" >
										<c:if test="${provincias.pro_id == provincia.pro_id}" >
											<option value="${provincia.pro_id}" selected="selected">${provincia.pro_nombre}</option>
										</c:if>
										<c:if test="${provincias.pro_id != provincia.pro_id}" >
											<option value="${provincias.pro_id}">${provincias.pro_nombre}</option>
										</c:if>
									</c:forEach>							
									</select>					
								</div>
							</div>
						</div>
						<div id="div_comuna">
							<div class="form-group">
								<label class="col-xs-3 control-label" for="comuna">Comuna:</label>
								<div class="col-xs-9">
									<select class="form-control" id="comuna" name="comuna" required>
									<option value="">Seleccione Comuna...</option>
									<c:forEach var="comunas" items="${comunaList}" >
										<c:if test="${comunas.com_id == comuna.com_id}" >
										<option value="${comuna.com_id}" selected="selected">${comuna.com_nombre}</option>
										</c:if>
										<c:if test="${comunas.com_id != comuna.com_id}" >
											<option value="${comunas.com_id}">${comunas.com_nombre}</option>
										</c:if>
									</c:forEach>	
									</select>							
								</div>
							</div>
						</div>
					<div class="form-group ">
					<label class="col-xs-3 control-label" for="direccion">Direcci�n de Obra:</label>
						<div class="col-xs-9">
							<input type="text" class="form-control" id="direccion" name="direccion" value="${pendiente.pra_direccion}" onkeypress="return funcionMaestra(event,'dire','1')">						
						</div>
					</div>
					<div class="form-group ">
					<label class="col-xs-3 control-label" for="tareas">Labor a Desarrollar:</label>
						<div class="col-xs-9">
							<textarea class="form-control" id="tareas" name="tareas" onkeypress="return funcionMaestra(event,'descri','2')">${pendiente.pra_tareas}</textarea>					
						</div>
					</div>
					<script type="text/javascript">
				            // When the document is ready
				            $(document).ready(function () { 
				                $('#termino').datepicker({
				                	format: "dd-mm-yyyy",
				                    weekStart: 1,
				                    autoclose: true,
				                    orientation: "auto bottom",
				                    startDate: "+0d",
				                    endDate: "+6w"
				                });  
								$('#inicio').datepicker({
									format: "dd-mm-yyyy",
				                    weekStart: 1,
				                    autoclose: true,
				                    orientation: "auto bottom",
				                    startDate: "+0d",
					                endDate: "+10m"
				                }); 
				            });
				    </script>
					<div class="form-group ">
					<label class="col-xs-3 control-label" for="inicio">Fecha Inicio Pr�ctica:</label>
						<div class="col-xs-9">
							<div class='input-group date'>
			                    <input type='text' class="form-control"  id="inicio" name="inicio"  value="${pendiente.pra_fecha_ini}" readonly="readonly"/>
			                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
			                    </span>
			                </div>
						</div>
					</div>
					<div class="form-group ">
					<label class="col-xs-3 control-label" for="termino">Fecha T�rmino Pr�ctica:</label>
						<div class="col-xs-9">
							<div class='input-group date'>
			                    <input type='text' class="form-control"  id="termino" name="termino"  value="${pendiente.pra_fecha_fin}" readonly="readonly"/>
			                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
			                    </span>
			                </div>
						</div>
					</div>
					<div class="form-group ">
					<label class="col-xs-3 control-label" for="horas">Cantidad de Horas:</label>
						<div class="col-xs-9">
							<input type="text" class="form-control" id="horas" name="horas" value="${pendiente.pra_horas}" onkeypress="return funcionMaestra(event,'num','1')">						
						</div>
					</div>
						<button type="submit"  onclick="return validar_practica(event,1)" class="btn btn-primary  btn-xg">Aceptar Pre-Inscripci�n</button>
						<button type="submit" onclick="return validar_practica(event,3)" class="btn btn-danger  btn-xg">Rechazar Pre-Inscripci�n</button>
					
					<br>
					<div class="col-xs-offset-4 col-sm-offset-4 col-md-offset-4 col-lg-offset-4">
						<input type="hidden" class="form-control" id="fecha_ins" name="fecha_ins" readonly="readonly" value="${pendiente.ins_fecha}" >
						<input type="hidden" class="form-control" id="alumno" name="alumno" value="${pendiente.alu_rut}" readonly="readonly">
						<input type="hidden" class="form-control" id="accion" name="accion" value="" readonly="readonly">
					</div>
				</form>
		    </div>
		   </div>
	 </div>
</div>
