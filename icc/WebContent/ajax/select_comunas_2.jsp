<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<div class="form-group">
	<label class="col-xs-3 control-label" for="comuna_2">Comuna Direcci�n Residencial:</label>
	<div class="col-xs-9">		
		<select class="form-control" id="comuna_2" name="comuna_2" required>
			<option value="">Seleccione Comuna...</option>
			<c:forEach var="comunas" items="${comunaList}" >
				<option value="${comunas.com_id}">${comunas.com_nombre}</option>
			</c:forEach>
		</select>							
	</div>
</div>