<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<script type="text/javascript" src="js/validaciones_alumno.js"></script>

<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	 <div class="modal-dialog modal-lg">
	    <div class="modal-content">
	    	<div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		        <h4 class="modal-title" id="myModalLabel">Especialidades Académico</h4>
		    </div>
		    <div class="modal-body">
			    <form class="form-horizontal" id="especialidades" role="form" method="POST" action="#">	    	
			    	<c:forEach items="${especialidadList}" var="especialidad" >
			    		<input type="text" class="form-control"  readonly="readonly" value="${especialidad.esp_nombre}" >
			    		<br>
			    	</c:forEach>
			    	
			    </form>
			</div>
		</div>
	</div>
</div>
	