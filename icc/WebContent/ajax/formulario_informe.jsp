<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<div id="myModalInforme" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
	        <h4 class="modal-title" id="myModalLabel">Informe Final de Obra ${practica.pra_obra_nombre} </h4>
	    </div>
	      <div class="modal-body">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 columna">
			<form class="form-horizontal" id="#" role="form" method="POST" enctype="multipart/form-data" action="FormularioInformeControlador">
				<div class="alert alert-warning alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				  <span class="glyphicon glyphicon-warning-sign form-control-feedback"></span><strong> Advertencia!</strong> El peso máximo por informe no puede exceder los 15 MegaBytes y además debe venir en alguno de estos dos formatos (.zip .rar). 
				</div>	
				
				<div class="form-group">
					<label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label" for="imagen_1">Adjuntar Informe:</label>	
					<input type="file" id="archivo" name="archivo">
				</div>		
				<input type="hidden" id="pra_id" name="pra_id" value="${practica.pra_id}">
				<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-floppy-disk"></span> Guardar</button>			
			</form>
	      </div>
	      <div class="modal-footer">
	      <!-- 
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	        <button type="button" class="btn btn-primary">Guardar</button>
	        -->
	     </div>
    </div>
  </div>
</div>
</div>
