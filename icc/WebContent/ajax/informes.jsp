<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<c:if test="${tipo == 1}" >
	<form class="form-horizontal" role="form" method="POST" action="GenerarInformesControlador">
		<legend>Informe Por Gestiones</legend>
		<div class="form-group ">
			<label class="col-xs-3 control-label" for="practica">Seleccione el tipo de Pr�ctica que desea consultar:</label>
			<div class="col-xs-9">
				<select class="form-control" id="practica" name="practica">				
					<c:forEach var="practicas" items="${practicaList}" >
							<option value="${practicas.asi_codigo}">${practicas.bean1} - ${practicas.bean2}</option>
					</c:forEach>
				</select>						
			</div>
		</div>
		<div class="form-group ">
			<label class="col-xs-3 control-label" for="informe">Consultar Pr�cticas de Estado:</label>
			<div class="col-xs-9">
				<select class="form-control" id="informe" name="informe">			
						<option value="inactiva">Inactiva</option>
						<option value="activa">Activa</option>	
				</select>						
			</div>
		</div>
		<button type="submit" class="btn btn-primary">Generar Informe</button>	
		<input type="hidden" class="form-control" id="tipo_informe" name="tipo_informe" value="1">	
	</form>
</c:if>
<c:if test="${tipo == 2}" >
	<form class="form-horizontal" role="form" method="POST" action="GenerarInformesControlador">
		<legend>Informe De estudiantes por Regi�n</legend>
			<div class="form-group ">
			<input type="hidden" class="form-control" id="tipo_informe" name="tipo_informe" value="2">	
			</div>
			<div class="form-group ">
				<label class="col-xs-3 control-label" for="informe">Consultar Pr�cticas de Estado:</label>
				<div class="col-xs-9">
					<select class="form-control" id="informe" name="informe">			
							<option value="inactiva">Inactiva</option>
							<option value="activa">Activa</option>	
					</select>						
				</div>
			</div>
			<button type="submit" class="btn btn-primary">Generar Informe</button>
	</form>
</c:if>
<c:if test="${tipo == 3}" >
	<form class="form-horizontal" role="form" method="POST" action="GenerarInformesControlador">
		<legend>Informe De estudiantes por Comuna</legend>
			<div class="form-group ">
			<input type="hidden" class="form-control" id="tipo_informe" name="tipo_informe" value="3">	
			</div>
			<div class="form-group ">
				<label class="col-xs-3 control-label" for="informe">Consultar Pr�cticas de Estado:</label>
				<div class="col-xs-9">
					<select class="form-control" id="informe" name="informe">			
							<option value="inactiva">Inactiva</option>
							<option value="activa">Activa</option>	
					</select>						
				</div>
			</div>
			<button type="submit" name="accion" value="total" class="btn btn-primary">Generar Informe Total de Alumnos Por Comuna</button>
			<button type="submit" name="accion" value="listado" class="btn btn-primary">Generar Listado de Alumnos por Comuna</button>
	</form>
</c:if>
<c:if test="${tipo == 4}" >
	<form class="form-horizontal" role="form" method="POST" action="GenerarInformesControlador">
		<legend>Informe De Empresas por Pr�ctica</legend>
			<div class="form-group ">
			<input type="hidden" class="form-control" id="tipo_informe" name="tipo_informe" value="4">	
			</div>
			<div class="form-group ">
				<label class="col-xs-3 control-label" for="practica">Seleccione el tipo de Pr�ctica que desea consultar:</label>
				<div class="col-xs-9">
					<select class="form-control" id="practica" name="practica">				
						<c:forEach var="practicas" items="${practicaList}" >
								<option value="${practicas.asi_codigo}">${practicas.bean1} - ${practicas.bean2}</option>
						</c:forEach>
					</select>						
				</div>
			</div>
			<div class="form-group ">
				<label class="col-xs-3 control-label" for="informe">Consultar Pr�cticas de Estado:</label>
				<div class="col-xs-9">
					<select class="form-control" id="informe" name="informe">			
							<option value="inactiva">Inactiva</option>
							<option value="activa">Activa</option>	
					</select>						
				</div>
			</div>
			<button type="submit" name="accion" value="total" class="btn btn-primary">Generar Informe Total de Alumnos en la Empresa</button>
			<button type="submit" name="accion" value="listado" class="btn btn-primary">Generar Listado de Alumnos por Empresa</button>
	</form>
</c:if>
<c:if test="${tipo == 5}" >
	<form class="form-horizontal" role="form" method="POST" action="GenerarInformesControlador">
		<legend>Informe de Estudiantes por Tipo de Obra para cada Pr�ctica</legend>
			<div class="form-group ">
			<input type="hidden" class="form-control" id="tipo_informe" name="tipo_informe" value="5">	
			</div>
			<div class="form-group ">
				<label class="col-xs-3 control-label" for="practica">Seleccione el tipo de Pr�ctica que desea consultar:</label>
				<div class="col-xs-9">
					<select class="form-control" id="practica" name="practica">				
						<c:forEach var="practicas" items="${practicaList}" >
								<option value="${practicas.asi_codigo}">${practicas.bean1} - ${practicas.bean2}</option>
						</c:forEach>
					</select>						
				</div>
			</div>
			<button type="submit" name="accion" value="total" class="btn btn-primary">Generar Informe Total de Alumnos por Tipo de Obra</button>
			<button type="submit" name="accion" value="listado" class="btn btn-primary">Generar Listado de Alumnos por Tipo de Obra</button>
	</form>
</c:if>
<c:if test="${tipo == 6}" >
	<form class="form-horizontal" role="form" method="POST" action="GenerarInformesControlador">
		<legend>Informe de Estudiantes por Supervisor para cada Pr�ctica</legend>
			<div class="form-group ">
			<input type="hidden" class="form-control" id="tipo_informe" name="tipo_informe" value="6">	
			</div>
			<div class="form-group ">
				<label class="col-xs-3 control-label" for="practica">Seleccione el tipo de Pr�ctica que desea consultar:</label>
				<div class="col-xs-9">
					<select class="form-control" id="practica" name="practica">				
						<c:forEach var="practicas" items="${practicaList}" >
								<option value="${practicas.asi_codigo}">${practicas.bean1} - ${practicas.bean2}</option>
						</c:forEach>
					</select>						
				</div>
			</div>
			<div class="form-group ">
				<label class="col-xs-3 control-label" for="informe">Consultar Pr�cticas de Estado:</label>
				<div class="col-xs-9">
					<select class="form-control" id="informe" name="informe">			
							<option value="inactiva">Inactiva</option>
							<option value="activa">Activa</option>	
					</select>						
				</div>
			</div>
			<button type="submit" name="accion" value="total" class="btn btn-primary">Generar Informe Total de Alumnos por Supervisor</button>
			<button type="submit" name="accion" value="listado" class="btn btn-primary">Generar Listado de Alumnos por Supervisor</button>
	</form>
</c:if>
<c:if test="${tipo == 7}" >
	<form class="form-horizontal" role="form" method="POST" action="GenerarInformesControlador">
		<legend>Informe de Estudiantes por Area seg�n Tipo de Pr�ctica</legend>
			<div class="form-group ">
			<input type="hidden" class="form-control" id="tipo_informe" name="tipo_informe" value="7">	
			</div>
			<div class="form-group ">
				<label class="col-xs-3 control-label" for="practica">Seleccione el tipo de Pr�ctica que desea consultar:</label>
				<div class="col-xs-9">
					<select class="form-control" id="practica" name="practica">				
						<c:forEach var="practicas" items="${practicaList}" >
								<option value="${practicas.asi_codigo}">${practicas.bean1} - ${practicas.bean2}</option>
						</c:forEach>
					</select>						
				</div>
			</div>
			<div class="form-group ">
				<label class="col-xs-3 control-label" for="informe">Consultar Pr�cticas de Estado:</label>
				<div class="col-xs-9">
					<select class="form-control" id="informe" name="informe">			
							<option value="inactiva">Inactiva</option>
							<option value="activa">Activa</option>	
					</select>						
				</div>
			</div>
			<button type="submit" name="accion" value="total" class="btn btn-primary">Generar Informe Total de Alumnos por Area</button>
			<button type="submit" name="accion" value="listado" class="btn btn-primary">Generar Listado de Alumnos por Area</button>
	</form>
</c:if>
<c:if test="${tipo == 8}" >
	<form class="form-horizontal" role="form" method="POST" action="GenerarInformesControlador">
		<legend>Informe de Estudiantes por Acad�mico seg�n Tipo de Pr�ctica</legend>
			<div class="form-group ">
			<input type="hidden" class="form-control" id="tipo_informe" name="tipo_informe" value="8">	
			</div>
			<div class="form-group ">
				<label class="col-xs-3 control-label" for="practica">Seleccione el tipo de Pr�ctica que desea consultar:</label>
				<div class="col-xs-9">
					<select class="form-control" id="practica" name="practica">				
						<c:forEach var="practicas" items="${practicaList}" >
								<option value="${practicas.asi_codigo}">${practicas.bean1} - ${practicas.bean2}</option>
						</c:forEach>
					</select>						
				</div>
			</div>
			<button type="submit" name="accion" value="total" class="btn btn-primary">Generar Informe Total de Alumnos por Supervisor</button>
			<button type="submit" name="accion" value="listado" class="btn btn-primary">Generar Listado de Alumnos por Supervisor</button>
	</form>
</c:if>
<c:if test="${tipo == 9}" >
	<form class="form-horizontal" role="form" method="POST" action="GenerarInformesControlador">
		<legend>Informe de Notas de Estudiantes por semestre seg�n Pr�ctica</legend>
			<div class="form-group ">
			<input type="hidden" class="form-control" id="tipo_informe" name="tipo_informe" value="9">	
			</div>
			<div class="form-group ">
				<label class="col-xs-3 control-label" for="practica">Seleccione el tipo de Pr�ctica que desea consultar:</label>
				<div class="col-xs-9">
					<select class="form-control" id="practica" name="practica">				
						<c:forEach var="practicas" items="${practicaList}" >
								<option value="${practicas.asi_codigo}">${practicas.bean1} - ${practicas.bean2}</option>
						</c:forEach>
					</select>						
				</div>
			</div>
			<div class="form-group ">
				<label class="col-xs-3 control-label" for="semestre">Seleccione el semestre que desea consultar:</label>
				<div class="col-xs-9">
					<select class="form-control" id="semestre" name="semestre">				
							<option value="1">Primer Semestre</option>
							<option value="2">Segundo Semestre</option>
					</select>						
				</div>
			</div>
			<div class="form-group ">
				<label class="col-xs-3 control-label" for="agnio">Seleccione el a�o que desea consultar:</label>
				<div class="col-xs-9">
					<select class="form-control" id="agnio" name="agnio">				
							<option value="2014">2014</option>
							<option value="2015">2015</option>
							<option value="2016">2016</option>
							<option value="2017">2017</option>
							<option value="2018">2018</option>
							<option value="2019">2019</option>
							<option value="2020">2020</option>
							<option value="2021">2021</option>
							<option value="2022">2022</option>
							<option value="2023">2023</option>
							<option value="2024">2024</option>
							<option value="2025">2025</option>
							<option value="2026">2026</option>
							<option value="2027">2027</option>
							<option value="2028">2028</option>
							<option value="2029">2029</option>
							<option value="2030">2030</option>
							<option value="2031">2031</option>
							<option value="2032">2032</option>
							<option value="2033">2033</option>
							<option value="2034">2034</option>
							<option value="2035">2035</option>
							<option value="2036">2036</option>
							<option value="2037">2037</option>
							<option value="2038">2038</option>
							<option value="2039">2039</option>
							<option value="2040">2040</option>
					</select>						
				</div>
			</div>
			<button type="submit" name="accion" value="listado" class="btn btn-primary">Generar Informe de Notas de Alumnos por Gesti�n</button>
	</form>
</c:if>
