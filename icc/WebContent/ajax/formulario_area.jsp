<%@ page language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<link rel="stylesheet" href="css/alertify.core.css" />
<link rel="stylesheet" href="css/alertify.default.css" id="toggleCSS" />
<script src="js/alertify.min.js"></script>
<script type="text/javascript" src="js/validaciones_admin.js"></script>
<div id="myModal_registro" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	 <div class="modal-dialog modal-lg">
	    <div class="modal-content">
	    	<div class="modal-header">
	    		<script type="text/javascript">
					$('#salir').click(function() {
				    	      location.reload();
					});
				</script>
		        <button type="button"  id="salir" name="salir" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		        <h4 class="modal-title" id="myModalLabel">Formulario de registro de Nueva Area</h4>
		    </div>
		    <div class="modal-body">
		    	<form class="form-horizontal" id="formulario_area" role="form" method="POST" action="areasServlet.do">
					<script type="text/javascript">
					$(document).ready(function () {
							$('#nombre').bind('copy paste drop', function (e) {
					           e.preventDefault();
					        });       
					        $("#nombre").focusin(function() {
					        	$("#nombre").attr('autocomplete', 'off');
					        });
					        $("#nombre").focusout(function() {
					        	$("#nombre").removeAttr('autocomplete');
					        });
					});
				    </script>
					<div class="form-group">
					<label class="col-xs-3 control-label" for="nombre">Nombre:</label>
						<div class="col-xs-9">
							<input type="text" class="form-control" id="nombre" name="nombre" placeholder="Ej: Area Administrativa" onkeypress="return funcionMaestra(event,'nombres','1')">
						</div>
					</div>			
					<button type="submit" onclick="return validacion_area(event,1)" class="btn btn-primary">Guardar Area</button>
				</form>
		    </div>
		</div>
	</div>
</div>