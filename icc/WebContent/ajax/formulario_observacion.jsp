<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<script type="text/javascript" src="js/validaciones_supervisor.js"></script>
<div id="myModal_registro" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	 <div class="modal-dialog modal-lg">
	    <div class="modal-content">
	    	<div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		        <h4 class="modal-title" id="myModalLabel">Bitácora de ${alumno.alu_nombres} ${alumno.alu_apellido_p} ${alumno.alu_apellido_m}</h4>
		    </div>
		    <div class="modal-body">
		    	<form class="form-horizontal" role="form" method="POST" action="FormularioObservacion">
					<div class="form-group">
						<script type="text/javascript">
						$(document).ready(function () {
								$('#observacion').bind('drop', function (e) {
						           e.preventDefault();
						        });       
						        $("#observacion").focusin(function() {
						        	$("#observacion").attr('autocomplete', 'off');
						        });
						        $("#observacion").focusout(function() {
						        	$("#observacion").removeAttr('autocomplete');
						        });
						});
					    </script>
						<label class="col-xs-3 control-label" for="observacion">Observación:</label>
						<div class="col-xs-9">
							<textarea class="form-control no_resize" rows="6" id="observacion" name="observacion" placeholder="- Lo que el alumno ha escrito en esta bitácora es acorde a lo realizado en la obra." onkeypress="return funcionMaestra(event,'descri','2')"></textarea>				
						</div>
					</div>
					<input type="hidden" id="bit_id" name="bit_id" value="${bitacora.bit_id}">
					<input type="hidden" id="pra_id" name="pra_id" value="${bitacora.pra_id}">
					<div class="col-xs-offset-2 col-sm-offset-2 col-md-offset-2 col-lg-offset-2">
						<button type="submit"  class="btn btn-primary">Guardar Observación</button>
					</div>					
				</form>
		    </div>
		</div>
	</div>
</div>