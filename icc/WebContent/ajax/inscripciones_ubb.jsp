<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<script type="text/javascript">
			$.fn.dataTable.TableTools.defaults.aButtons = ["pdf", "xls" ];
			$(document).ready(function() {
			    var table = $('#datatable').DataTable();
			    var tt = new $.fn.dataTable.TableTools( table ); 
			    $( tt.fnContainer() ).insertBefore('div.dataTables_wrapper');    
			} );
			
</script>
<c:if test="${exito == 1}">
	<div class="col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1">
		<div class="alert alert-success alert-dismissible" role="alert">
		  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		  <span class="glyphicon glyphicon-ok"></span>
		  <strong>Exito</strong> ${mensaje} [Total de Registros: ${InscripcionesList.size()}]
		</div>
	</div>
</c:if>
<c:if test="${exito == 0}">
	<div class="col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1">
		<div class="alert alert-danger alert-dismissible" role="alert">
		  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		  <span class="glyphicon glyphicon-remove"></span>
		  <strong>Error</strong> ${mensaje}
		</div>
	</div>
</c:if>
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 columna">
		<div class="table-responsive">
			<table cellpadding="0" cellspacing="0" border="0" class="display table table-striped table-bordered table-hover" id="datatable">
				<thead>
				  <tr >
				  	 <th>#</th>
				     <th>ALU_RUT</th>
				     <th>ASI_CODIGO</th>
				     <th>PLAN_ID</th>
				     <th>INS_FECHA</th>
				     <th>INS_SEMESTRE</th>
				     <th>INS_AGNIO</th>
				     <th>INS_NOTA</th>
				     <th>INS_ESTADO</th>
				  </tr>
		        </thead>
		        <tbody>
		        	<c:set var="contador" value="1"/>
		        	<c:forEach items="${InscripcionesList}" var="inscripcion" >
		        	<tr>
		        		<td align="center">${contador}</td>
		        		<td align="center">${inscripcion.alu_rut}</td>
		        		<td align="center">${inscripcion.asi_codigo}</td>
		        		<td align="center">${inscripcion.plan_id}</td>
		        		<td align="center">${inscripcion.ins_fecha}</td>
		        		<td align="center">${inscripcion.ins_semestre}</td>
		        		<td align="center">${inscripcion.ins_agnio}</td>
		        		<td align="center">${inscripcion.ins_nota}</td>
		        		<td align="center">${inscripcion.ins_estado}</td>
		        	</tr>
		        	<c:set var="contador" value="${contador+1}"/>
		        	</c:forEach>
		        </tbody>
		    </table>
		</div>
	</div>
</div>