<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
	        <h4 class="modal-title" id="myModalLabel">Académicos Evaluadores ${gestion.asi_nombre}</h4>
	    </div>
	      <div class="modal-body">	      							        			
			<form class="form-horizontal" id="#" role="form" method="POST" action="ComisionControlador">
			<div class="form-group">
				<label class="control-label col-xs-3" for="descripcion">Profesor Guía:</label>
				<div class="col-xs-9">
					<input type="text" class="form-control" id="guia" name="guia" readonly="readonly" value="${profesor.aca_rut} - ${profesor.aca_nombres} ${profesor.aca_apellido_p} ${profesor.aca_apellido_m}">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-xs-3" for="descripcion">Profesor Lector Bitácora:</label>
				<div class="col-xs-9">							
					<select class="form-control" id="lector" name="lector" required>
					<option value="">Seleccione Académico..</option>
					<c:forEach var="academico" items="${academicosList}" >
							<option value="${academico.aca_rut}">${academico.aca_nombres} ${academico.aca_apellido_p} ${academico.aca_apellido_m}</option>
					</c:forEach>
					</select>							
				</div>

			</div>
			<c:if test="${formulario == 'operativa'}">
				<input type="hidden" id="rol_comision" name="rol_comision" value="0">
			</c:if>
			
			<c:if test="${formulario == 'profesional'}">
			
			<div class="form-group">
				<label class="col-xs-6 col-sm-6 col-md-6 col-lg-6 control-label">Académicos Comisión Evaluadora Defensa Oral Gestión Profesional:</label>
				<label class="col-xs-6 col-sm-6 col-md-6 col-lg-6 control-label">Académicos Seleccionados para la Comisión :</label>
			</div>
			
			<div class="form-group">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				    <select class="form-control" multiple="multiple" id="comision" name="comision[]">
						<c:forEach var="academico" items="${academicosList}" >
							<c:if test="${academico.aca_rut == profesor.aca_rut}">
								<option selected value="${academico.aca_rut}">${academico.aca_nombres} ${academico.aca_apellido_p} ${academico.aca_apellido_m} (Ev # ${academico.bean_uno})</option>						
							</c:if>
							<c:if test="${academico.aca_rut != profesor.aca_rut}">
								<option value="${academico.aca_rut}">${academico.aca_nombres} ${academico.aca_apellido_p} ${academico.aca_apellido_m} (Ev # ${academico.bean_uno})</option>						
							</c:if>
						</c:forEach>
				    </select>
			    </div>
			 </div>
			 <script type="text/javascript">
			 	var ultima_seleccion_valida = null;
				$(document).ready(function(){	
					$('#comision').multiSelect({ keepOrder: true });					
					
					$('#comision').change(function(event) {						
						if ($(this).val().length > 5) {
						  alert('Solo puede seleccionar un maximo de 5 academicos');
						  var x = $('#comision').val();
						  $(this).multiSelect('deselect', x[x.length - 1]);
						}
						else {
						 ultima_seleccion_valida = $(this).val();
						}
					});				
				});
			 </script>
				<input type="hidden" id="rol_comision" name="rol_comision" value="${rol_comision}">
			</c:if>
				<div id="hola">
				</div>	
				<button type="submit" class="btn btn-primary" onclick="return validar_comision()"><span class="glyphicon glyphicon-floppy-disk"></span> Guardar</button>

				<input type="hidden" id="pra_id" name="pra_id" value="${practica.pra_id}">
				<input type="hidden" id="plan_id" name="plan_id" value="${gestion.plan_id}">
				<input type="hidden" id="asi_codigo" name="asi_codigo" value="${gestion.asi_codigo}">
				<input type="hidden" id="rol_bitacora" name="rol_bitacora" value="${rol_bitacora}">
				<input type="hidden" id="num_academicos" name="num_academicos" value="5">
				<input type="hidden" id="actual_academicos" name="actual_academicos" value="0">
				<input type="hidden" id="formulario" name="formulario" value="${formulario}">
			</form>
	      <div class="modal-footer">
	      <!-- 
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	        <button type="button" class="btn btn-primary">Guardar</button>
	        -->
	     </div>
    </div>
  </div>
</div>
</div>