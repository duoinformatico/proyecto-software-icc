<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	 <div class="modal-dialog modal-lg">
	    <div class="modal-content">
	    	<div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		        <h4 class="modal-title" id="myModalLabel">Detalle de Historial</h4>
		    </div>
		    <div class="modal-body">
			    <form class="form-horizontal">
			    	<div class="form-group">
							<label class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1  control-label" for="empresa_detalle">Empresa:</label>
							<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
								<input type="text" class="form-control" id="empresa_detalle" name="empresa_detalle" value="${empresa.emp_nombre}" readonly="readonly">
							</div>
					</div>
					<div class="form-group">
							<label class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1  control-label" for="supervisor_detalle">Supervisor:</label>
							<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
								<input type="text" class="form-control" id="supervisor_detalle" name="supervisor_detalle" value="${supervisor.sup_nombres} ${supervisor.sup_apellido_p} ${supervisor.sup_apellido_m}" readonly="readonly">
							</div>
					</div>
					<div class="form-group">
							<label class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1  control-label" for="profesor_detalle">Profesor Guía:</label>
							<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
								<input type="text" class="form-control" id="profesor_detalle" name="profesor_detalle" value="${profesor.aca_nombres} ${profesor.aca_apellido_p} ${profesor.aca_apellido_m}" readonly="readonly">
							</div>
					</div>
					<div class="form-group">
							<label class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1  control-label" for="alumno_detalle">Alumno:</label>
							<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
								<input type="text" class="form-control" id="alumno_detalle" name="alumno_detalle" value="${alumno.alu_nombres} ${alumno.alu_apellido_p} ${alumno.alu_apellido_m}" readonly="readonly">
							</div>
					</div>
					<div class="form-group">
							<label class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1  control-label" for="asignatura_detalle">Asignatura:</label>
							<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
								<input type="text" class="form-control" id="asignatura_detalle" name="asignatura_detalle" value="${asignatura.asi_nombre}" readonly="readonly">
							</div>
					</div>
					<div class="form-group">
							<label class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1  control-label" for="inscripcion_detalle">Fecha Inscripción Asignatura:</label>
							<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
								<input type="text" class="form-control" id="inscripcion_detalle" name="inscripcion_detalle" value="${HISTORIAL.ins_fecha}" readonly="readonly">
							</div>
					</div>
					<div class="form-group ">
							<label class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1  control-label" for="nombre_detalle">Nombre de Obra:</label>
							<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
								<input type="text" class="form-control" id="nombre_detalle" name="nombre_detalle" value="${HISTORIAL.pra_obra_nombre}" readonly="readonly">
							</div>
					</div>
					<div class="form-group">
							<label class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1  control-label" for="comuna_detalle">Comuna:</label>
							<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
								<input type="text" class="form-control" id="comuna_detalle" name="comuna_detalle" value="${comuna.com_nombre}" readonly="readonly">
							</div>
					</div>
					<div class="form-group ">
							<label class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1  control-label" for="direccion_detalle">Dirección:</label>
							<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
								<input type="text" class="form-control" id="direccion_detalle" name="direccion_detalle" value="${HISTORIAL.pra_direccion}  " readonly="readonly">
							</div>
					</div>
					<div class="form-group ">
							<label class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1  control-label" for="inicio_detalle">Fecha Inicio Práctica:</label>
							<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
								<input type="email" class="form-control" id="inicio_detalle" name="inicio_detalle" value="${HISTORIAL.pra_fecha_ini}" readonly="readonly">						
							</div>
					</div>
					<div class="form-group">
							<label class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1  control-label" for="termino_detalle">Fecha Término Práctica:</label>
							<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
								<input type="text" class="form-control" id="termino_detalle" name="termino_detalle" value="${HISTORIAL.pra_fecha_fin}" readonly="readonly">
							</div>
					</div>
					<div class="form-group">
							<label class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1  control-label" for="estado_detalle">Horas:</label>
							<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
								<input type="url" class="form-control" id="estado_detalle" name="estado_detalle" value="${HISTORIAL.pra_horas}" readonly="readonly">
							</div>
					</div>
					<div class="form-group">
							<label class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1  control-label" for="estado_detalle">Estado:</label>
							<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
								<input type="url" class="form-control" id="estado_detalle" name="estado_detalle" value="${HISTORIAL.pra_estado}" readonly="readonly">
							</div>
					</div>
		    	</form>
		   </div>
	    </div>
	</div>
</div>

		