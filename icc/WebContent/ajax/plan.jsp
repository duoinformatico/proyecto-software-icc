<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<script type="text/javascript" src="js/validaciones_admin.js"></script>

<div id="myModalDetalle" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	 <div class="modal-dialog modal-lg">
	    <div class="modal-content">
	    	<div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		        <h4 class="modal-title" id="myModalLabel">Detalle Plan Estudio</h4>
		    </div>
		    <div class="modal-body">
			    <form class="form-horizontal" id="especialidades" role="form" method="POST" action="#">	  
			    	<div class="form-group">
						<label class="col-xs-3 control-label" for="nombre">Nombre:</label>
						<div class="col-xs-9">
							<input type="text" class="form-control" id="nombre" name="nombre" value="${plan.plan_nombre}" readonly="readonly">
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-xs-3 control-label" for="carrera">Carrera:</label>
						<div class="col-xs-9">
							<input type="text" class="form-control" id="carrera" name="carrera" value="${carrera.car_nombre}" readonly="readonly">
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-xs-3 control-label" for="fecha">Fecha creación:</label>
						<div class="col-xs-9">
							<input type="text" class="form-control" id="fecha" name="fecha" value="${plan.plan_fecha}" readonly="readonly">
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-xs-3 control-label" for="estado">Estado:</label>
						<div class="col-xs-9">
							<input type="text" class="form-control" id="estado" name="estado" value="${plan.plan_estado}" readonly="readonly">
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-xs-3 control-label" for="asignatura" >Asignaturas Asociadas:</label>
						
						<c:forEach items="${asignaturaList}" var="asignatura" >
							<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9 col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3">
			    				<input type="text" class="form-control"  readonly="readonly" value="${asignatura.asi_nombre}" >
			    				<br>
			    			</div>
			    		
			    		</c:forEach>
					</div>  	
			    	
			    	
			    </form>
			</div>
		</div>
	</div>
</div>