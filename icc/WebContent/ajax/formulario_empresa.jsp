<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<link rel="stylesheet" href="css/alertify.core.css" />
<link rel="stylesheet" href="css/alertify.default.css" id="toggleCSS" />
<script src="js/alertify.min.js"></script>
<script type="text/javascript" src="js/validar_rut.js"></script>
<script type="text/javascript" src="js/validaciones_admin.js"></script>
<div id="myModal_registro" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	 <div class="modal-dialog modal-lg">
	    <div class="modal-content">
	    	<div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		        <h4 class="modal-title" id="myModalLabel">Formulario de registro Nueva Empresa</h4>
		    </div>
		    <div class="modal-body">
		    	<form class="form-horizontal" id="formulario_empresa" role="form" method="POST" action="empresaServlet.do">
					<div class="form-group">
					<label class="col-xs-3 control-label" for="rut">Rut:</label>
						<div class="col-xs-9">
							<input type="text" class="form-control" id="rut" name="rut" placeholder="24.026.937-6" 
							ondrop="return false;" onChange="Rut(this.value)" onkeypress="return funcionMaestra(event, 'rut', 1)">
						</div>
					</div>			
					<div class="form-group">
						<label class="col-xs-3 control-label" for="region">Región:</label>
						<div class="col-xs-9">							
							<select class="form-control" id="region" name="region">
							<option value="">Seleccione Región...</option>
							<c:forEach var="regiones" items="${regionList}" >
									<option value="${regiones.reg_id}">${regiones.reg_nombre}</option>
							</c:forEach>
							</select>							
						</div>
						</div>
						<div id="div_provincia">
							<div class="form-group">
							<label class="col-xs-3 control-label" for="provincia">Provincia:</label>
							<div class="col-xs-9">							
								<select class="form-control" id="provincia" name="provincia">
								<option value="">Seleccione Provincia...</option>						
								</select>					
							</div>
						</div>
						</div>
						<div id="div_comuna">
							<div class="form-group">
								<label class="col-xs-3 control-label" for="comuna">Comuna:</label>
								<div class="col-xs-9">
									<select class="form-control" id="comuna" name="comuna">
									<option value="">Seleccione Comuna...</option>
									</select>							
								</div>
							</div>
						</div>
					<script type="text/javascript">
					$(document).ready(function () {
							$('#nombre').bind('copy paste drop', function (e) {
					           e.preventDefault();
					        });       
					        $("#nombre").focusin(function() {
					        	$("#nombre").attr('autocomplete', 'off');
					        });
					        $("#nombre").focusout(function() {
					        	$("#nombre").removeAttr('autocomplete');
					        });
					        $('#direccion').bind('copy paste drop', function (e) {
						           e.preventDefault();
						        });
					        $("#direccion").focusin(function() {
					        	$("#direccion").attr('autocomplete', 'off');
					        });
					        $("#direccion").focusout(function() {
					        	$("#direccion").removeAttr('autocomplete');
					        });
					        $('#descripcion').bind('copy paste drop', function (e) {
						           e.preventDefault();
						        });
					        $("#descripcion").focusin(function() {
					        	$("#descripcion").attr('autocomplete', 'off');
					        });
					        $("#descripcion").focusout(function() {
					        	$("#descripcion").removeAttr('autocomplete');
					        });
					        $('#email').bind('copy paste drop', function (e) {
						           e.preventDefault();
						        });
					        $("#email").focusin(function() {
					        	$("#email").attr('autocomplete', 'off');
					        });
					        $("#email").focusout(function() {
					        	$("#email").removeAttr('autocomplete');
					        });
					        $('#telefono').bind('copy paste drop', function (e) {
						           e.preventDefault();
						        });
					        $("#telefono").focusin(function() {
					        	$("#telefono").attr('autocomplete', 'off');
					        	if($("#codigo").val() == ""){
					        		$("#telefono").attr('readonly', 'readonly');
					        		$("#codigo").focus();
					        	}
					        	else{
					        		$("#telefono").removeAttr('readonly');
					        	}
					        	if($("#codigo").val() != ""){
					        		if($("#codigo").val() == 2){
					        			$("#telefono").attr('maxlength', '8');
					        		}
					        		else{
					        			$("#telefono").attr('maxlength', '7');
					        		}
					        	}					        	
					        });
							$("#codigo").change(function(){					        	
								if($("#codigo").val() != ""){
					        		$("#telefono").focus();
					        		$("#telefono").val("");
					        	}					        	
					        });
					        $("#telefono").change(function(){					        	
					        	
					        	if($("#telefono").val().length == 8 && $("#codigo").val() == 2){
					        		$("#telefono").val("+56-"+$("#codigo").val()+"-"+$("#telefono").val());
					        	}
					        	if($("#telefono").val().length == 7 && $("#codigo").val() != 2){
					        		$("#telefono").val("+56-"+$("#codigo").val()+"-"+$("#telefono").val());
					        	}
					        });					        	
					        
					        $("#telefono").focusout(function() {
					        	$("#telefono").removeAttr('autocomplete');
					        	$("#telefono").removeAttr('maxlength');
					        	if($("#codigo").val() == ""){
					        		$("#telefono").attr('readonly', 'readonly');
					        		$("#codigo").focus();
					        	}
					        	else{
					        		$("#telefono").removeAttr('readonly');
					        	}
					        	if($("#telefono").val().length == 8 && $("#codigo").val() == 2){
					        		$("#telefono").val("+56-"+$("#codigo").val()+"-"+$("#telefono").val());
					        	}
					        	if($("#telefono").val().length < 8 && $("#codigo").val() == 2){
					        		$("#telefono").val("");
					        	}
					        	if($("#telefono").val().length == 7 && $("#codigo").val() != 2){
					        		$("#telefono").val("+56-"+$("#codigo").val()+"-"+$("#telefono").val());
					        	}
					        	if($("#telefono").val().length < 7 && $("#codigo").val() != 2){
					        		$("#telefono").val("");
					        	}
					        });
					        $('#celular').bind('copy paste drop', function (e) {
						           e.preventDefault();
						        });
					        $("#celular").focusin(function() {
					        	$("#celular").attr('autocomplete', 'off');
					        	$("#celular").attr('maxlength', '8');					        	
					        });
					       $("#celular").focusout(function() {
					        	$("#celular").removeAttr('autocomplete');
					        	$("#celular").removeAttr('maxlength');
					        	if($("#celular").val().length < 8){
					        		$("#celular").val("");
					        	}
					        	if($("#celular").val().length == 8){
					        		$("#celular").val("+569"+$("#celular").val());
					        	}
					        });
					        $('#web').bind('copy paste drop', function (e) {
						           e.preventDefault();
						        });
					        $("#web").focusin(function() {
					        	$("#web").attr('autocomplete', 'off');
					        });
					        $("#web").focusout(function() {
					        	$("#web").removeAttr('autocomplete');
					        });
				      });
				    </script>
					<div class="form-group ">
					<label class="col-xs-3 control-label" for="nombre">Nombre:</label>
						<div class="col-xs-9">
							<input type="text" class="form-control" id="nombre" name="nombre" placeholder="Masisa Cabrero" onkeypress="return funcionMaestra(event,'nombres','1')">
						</div>
					</div>
					<!--  
					<div class="form-group">
						<label class="col-xs-3 col-sm-3 col-md-3 col-lg-3 control-label" for="descripcion">Manejo de Rubros:</label>
						<div class="col-xs-9">
							<button type="button" id="giro" name="giro" value="giro" class="btn btn-primary  btn-xs"  onclick='agregarGiro()'><span class="glyphicon glyphicon-plus"></span> Agregar Giro</button>
							<button type="button" id="giro" name="giro" value="giro" class="btn btn-danger  btn-xs"  onclick='quitarGiro()'><span class="glyphicon glyphicon-minus"></span> Quitar Giro</button>
						</div>
					</div>
					-->
					<div class="form-group">
						<div class="col-xs-9 col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3">
							<div id="div_giro_1">
							</div>
							<div id="div_giro_2">
							</div>
							<div id="div_giro_3">
							</div>
						</div>
					</div>
					<div class="form-group">
					<label class="col-xs-3 control-label" for="direccion">Dirección:</label>
						<div class="col-xs-9">
							<input type="text" class="form-control" id="direccion" name="direccion" placeholder="Los Peumos #47 - Collao" onkeypress="return funcionMaestra(event,'dire','1')">
						</div>
					</div>
					<div class="form-group">
						<label class="col-xs-3 control-label" for="telefono">Teléfono:</label>
						<div class="col-xs-4">	
							<select class="form-control" id="codigo" name="codigo">
								<option value="">Código Area...</option>
								<optgroup label="Zona Norte">
									<option value="58">(58) Arica-Parinacota</option>
									<option value="57">(57) Iquique-Tamarugal</option>
									<option value="55">(55) Tocopilla-Loa-Antofagasta</option>
									<option value="51">(51) Huasco</option>
									<option value="52">(52) Chañaral-Copiapó</option>
									<option value="51">(51) Elqui</option>
									<option value="53">(53) Choapa-Limarí</option>
								</optgroup>
								<optgroup label="Zona Centro">
									<option value="32">(32) Isla de Pascua-Valparaíso-Marga Marga(Quilpué-Villa Alemana)</option>
									<option value="33">(33) Petorca-Quillota-Marga Marga(Limache y Olmué)</option>
									<option value="34">(34) Los Andes-San Felipe de Aconcagua</option>
									<option value="35">(35) San Antonio</option>
									<option value="2"> ( 2) Chacabuco-Cordillera-Maipo-Melipilla-Santiago-Talagante</option>
									<option value="72">(72) Cachapoal-ardenal Caro-Colchagua</option>
									<option value="71">(71) Talca</option>
									<option value="73">(73) Cauquenes-Linares</option>
									<option value="75">(75) Curicó</option>
								</optgroup>
								<optgroup label="Zona Sur">
									<option value="41">(41) Arauco-Concepción </option>
									<option value="42">(42) Ñuble</option>
									<option value="43">(43) Bío-Bío</option>
									<option value="45">(45) Cautín-Malleco</option>
									<option value="63">(63) Ranco-Valdivia</option>
									<option value="64">(64) Osorno</option>
									<option value="65">(65) Chiloé-Llanquihue-Palena </option>
									<option value="67">(67) Aisén-Capitán Prat-Coihaique-General Carrera</option>
									<option value="68">(68) Aisén(Puyuhuapi)</option>
									<option value="61">(61) Antártica Chilena-Magallanes-Tierra del Fuego-Última Esperanza</option>
								</optgroup>
							</select>							
						</div>
						<div class="col-xs-5">
							<div class="input-group">
								<input type="tel" class="form-control" id="telefono" name="telefono" placeholder="Ej:2323421" onkeypress="return funcionMaestra(event,'fijo',1)" >
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-phone-alt"></span>
								</span>
							</div>	
						</div>	
					</div>
					<div class="form-group">
						<label class="col-xs-3 control-label" for="celular">Celular:</label>
						<div class="col-xs-9">
							<div class="input-group">
								<input type="tel" class="form-control" id="celular" name="celular" placeholder="Ej: +56998345670" onkeypress="return funcionMaestra(event,'celular',1)">
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-phone"></span>
								</span>
							</div>	
						</div>	
					</div>
					<div class="form-group">
					<label class="col-xs-3 control-label" for="descripcion">Descripción:</label>
						<div class="col-xs-9">
							<textarea class="form-control" id="descripcion" name="descripcion" placeholder="- Empresa dedicada a la Reconstrucción de Edificios, Restauración, etc." onkeypress="return funcionMaestra(event,'descri','2')"></textarea>
						</div>
					</div>
					<div class="form-group ">
					<label class="col-xs-3 control-label" for="email">Email:</label>
						<div class="col-xs-9">
							<input type="email" class="form-control" id="email" name="email" placeholder="usuario@ejemplo.com" onkeypress="return funcionMaestra(event,'email','2')">						
						</div>
					</div>
					<div class="form-group">
					<label class="col-xs-3 control-label" for="web">Pagina Web:</label>
						<div class="col-xs-9">
							<input type="url" class="form-control" id="web" name="web" placeholder="http://www.ejemplo.com" onkeypress="return funcionMaestra(event,'web','2')">
						</div>
					</div>
					<div class="col-xs-offset-4 col-sm-offset-4 col-md-offset-4 col-lg-offset-4">
						<button type="submit" onclick="return validar_empresa(event,1)" class="btn btn-primary">Guardar Empresa</button>
						<input type="hidden" id="num_giros" name="num_giros" value="3">
						<input type="hidden" id="actual_giros" name="actual_giros" value="0">
					</div>					
				</form>
		    </div>
		</div>
	</div>
</div>