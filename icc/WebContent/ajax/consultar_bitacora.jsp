<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<div id="myModal_consulta" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
	        <h4 class="modal-title" id="myModalLabel">${mensaje}</h4>
	    </div>
	      <div class="modal-body">
	      							        			
	        <legend>Ultima Edición: ${bitacora.bit_fecha_edicion} a las ${bitacora.bit_hora_edicion}</legend>
	        
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 columna">
			<form class="form-horizontal" id="#" role="form" method="POST" enctype="multipart/form-data" action="formularioBitacoraServlet.do">
				<div class="row">
					<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 columna">
						<label class="control-label" for="descripcion">Descripción Bitácora:</label>
					</div>
					<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9 columna">
						<textarea class="form-control no_resize" rows="6" name="descripcion" readonly="readonly">${bitacora.bit_descripcion}</textarea>
					</div>
				</div>
				<br>
				
				<c:set var="count2" value="1" scope="page" />
				<div class="row">			
					
						<c:if test="${count2 == 1 }">
							<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 columna">
								<div class="form-group">
									<label class="control-label" for="descripcion">Asignaturas Tributadas:</label>
								</div>
							</div>
						</c:if>
							<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9 columna">
								<div class="table-responsive">
									<table class="table table-striped table-bordered table-hover" >
										<thead>
										  <tr >
										  	 <th>#</th>
										  	 <th>Código</th>
										     <th>Asignatura</th>
										  </tr>
								        </thead>
								        <tbody>
								        	<c:forEach var="asignatura" items="${asignaturaBitacoraList}" >
								        	<tr>
								        		<td>${count2}</td>
								        		<td>${asignatura.asi_codigo}</td>
								        		<td>${asignatura.asi_nombre}</td>
								        	</tr>
								        	<c:set var="count2" value="${count2 + 1}"/>
								        	</c:forEach>
								        </tbody>
								    </table>
								</div>
							</div>	
									
				</div>		
					
				<div class="form-group">
					<c:set var="count" value="0" scope="page" />
					<div class="row">
					<c:forEach var="imagen" items="${imagenList}" >
						<c:if test="${count % 2 == 0 }">
							</div>
							<div class="row">
							<div class="col-xs-12 col-sm-5 col-md-5 col-lg-5  col-sm-offset-1 col-md-offset-1 col-lg-offset-1">	
								<br>
										<legend >Imagen subida el ${imagen.img_fecha} a las ${imagen.img_hora} Horas:</legend>
										<img src="image/${imagen.img_url}" class="img-responsive img-rounded img-thumbnail center-block" style="max-width:100%;height:auto; "/>
							</div>
						</c:if>
						<c:if test="${count % 2 != 0}">
							<div class="col-xs-12 col-sm-5 col-md-5 col-lg-5  ">	
								<br>
										<legend >Imagen subida el ${imagen.img_fecha} a las ${imagen.img_hora} Horas:</legend>
										<img src="image/${imagen.img_url}" class="img-responsive img-rounded img-thumbnail center-block" style="max-width:100%;height:auto; "/>
							</div>
						</c:if>
						<c:set var="count" value="${count + 1}"/>		
					</c:forEach>
					<c:if test="${fn:length(imagenList) % 2 != 0}">		
						</div>
					</c:if>
				</div>
				<div class="form-group">
					<div class="col-xs-9 col-sm-10 col-md-10 col-lg-10 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1">
							<c:forEach var="observacion" items="${observacionList}" >
								<br>
								<label class="col-xs-9 col-sm-9 col-md-9 col-lg-9 control-label" for="descripcion">Observación Realizada el ${observacion.obs_fecha} a las ${observacion.obs_hora} Horas:</label>	
								<textarea class="form-control no_resize " rows="6" name="descripcion" readonly="readonly">${observacion.obs_texto}</textarea>
							</c:forEach>	
					</div>
				</div>
			</form>
			</div>
	      </div>
	      <div class="modal-footer">
	      <!-- 
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	        <button type="button" class="btn btn-primary">Guardar</button>
	        -->
	     </div>
    </div>
  </div>
</div>

