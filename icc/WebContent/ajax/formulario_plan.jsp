<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<script type="text/javascript" src="js/validaciones_admin.js"></script>
<div id="myModal_registro" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	 <div class="modal-dialog modal-lg">
	    <div class="modal-content">
	    	<div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		        <h4 class="modal-title" id="myModalLabel">Formulario de registro Nuevo Plan de Estudio</h4>
		    </div>
		    <div class="modal-body">
		    	<form class="form-horizontal" role="form" method="POST" action="planesServlet.do">
					<div class="form-group">
					<label class="col-xs-3 control-label" for="nombre">Nombre:</label>
						<div class="col-xs-9">
							<input type="text" class="form-control" id="nombre" name="nombre" placeholder="Ej: Malla 2015 Ing Cosntrucción" required onkeypress="return funcionMaestra(event,'nombres','1')">
						</div>
					</div>			
					<div class="form-group">
						<label class="col-xs-3 control-label" for="carrera">Carrera:</label>
						<div class="col-xs-9">							
							<select class="form-control" id="carrera" name="carrera" required>
							<option value="">Seleccione Carrera...</option>
							<c:forEach var="carrera" items="${carrerasList}" >
								<option value="${carrera.car_codigo}">${carrera.car_nombre} ( Cod: ${carrera.car_codigo} - Sede ${carrera.car_sede})</option>
							</c:forEach>
							</select>							
						</div>
					</div>
					<div class="form-group">
						<label class="col-xs-3 control-label" for="semestre">Nº Semestres:</label>
						<div class="col-xs-9">							
							<select class="form-control" id="semestre" name="semestre" required>
							<option value="">Semestres...</option>
								<option value="4">4</option>
								<option value="5">5</option>
								<option value="6">6</option>
								<option value="7">7</option>
								<option value="8">8</option>
								<option value="9">9</option>
								<option value="10">10</option>
								<option value="11">11</option>
								<option value="12">12</option>
								<option value="13">13</option>
							</select>							
						</div>
					</div>
					<div class="form-group">
						<div id="div_semestres">
						</div>
					</div>
					<script type="text/javascript">
					$(document).ready(function () {
						$('#nombre').bind('copy paste drop', function (e) {
				           e.preventDefault();
				        });       
				        $("#nombre").focusin(function() {
				        	$("#nombre").attr('autocomplete', 'off');
				        });
				        $("#nombre").focusout(function() {
				        	$("#nombre").removeAttr('autocomplete');
				        });
						
						$("#semestre").change(function (){
							if($(this).val() != ""){
								semestresPlan($("#semestre").val());
							}
							else{
								$("#div_semestres").empty();
							}
						});
					});
					</script>
					<button type="submit"  class="btn btn-primary">Guardar Plan Estudio</button>
					<input type="hidden" class="form-control" id="etapa" name="etapa" value="1">										
				</form>
		    </div>
		</div>
	</div>
</div>