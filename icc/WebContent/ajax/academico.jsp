<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	 <div class="modal-dialog modal-lg">
	    <div class="modal-content">
	    	<div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		        <h4 class="modal-title" id="myModalLabel">Detalle Académico</h4>
		    </div>
		    <div class="modal-body">
			    <form class="form-horizontal">
			    	<div class="form-group">
							<label class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1  control-label" for="rut_detalle">Rut:</label>
							<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
								<input type="text" class="form-control" id="rut_detalle" name="rut_detalle" value="${ACADEMICO.aca_rut}" readonly="readonly">
							</div>
					</div>
					<div class="form-group">
							<label class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1  control-label" for="departamento_detalle">Depto:</label>
							<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
								<input type="text" class="form-control" id="departamento_detalle" name="departamento_detalle" value="${DEPARTAMENTO.dep_nombre}" readonly="readonly">
							</div>
					</div>
					<div class="form-group ">
							<label class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1  control-label" for="nombre_detalle">Nombre:</label>
							<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
								<input type="text" class="form-control" id="nombre_detalle" name="nombre_detalle" value="${ACADEMICO.aca_nombres} " readonly="readonly">
							</div>
					</div>
					<div class="form-group ">
							<label class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1  control-label" for="apellidos_detalle">Apellidos:</label>
							<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
								<input type="text" class="form-control" id="apellidos_detalle" name="apellidos_detalle" value="${ACADEMICO.aca_apellido_p} ${ACADEMICO.aca_apellido_m}  " readonly="readonly">
							</div>
					</div>
					<div class="form-group ">
							<label class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1  control-label" for="email_detalle">Email:</label>
							<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
								<input type="email" class="form-control" id="email_detalle" name="email_detalle" value="${ACADEMICO.aca_email}" readonly="readonly">						
							</div>
					</div>
					<div class="form-group">
							<label class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1  control-label" for="telefono_detalle">Teléfono Fijo:</label>
							<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
								<input type="text" class="form-control" id="telefono_detalle" name="telefono_detalle" value="${ACADEMICO.aca_telefono}" readonly="readonly">
							</div>
					</div>
					<div class="form-group">
							<label class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1  control-label" for="celular_detalle">Celular:</label>
							<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
								<input type="tel" maxlength="20" class="form-control" id="celular_detalle" name="celular_detalle" value="${ACADEMICO.aca_celular}" readonly="readonly">
							</div>
					</div>
					<div class="form-group">
							<label class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1  control-label" for="estado_detalle">Estado:</label>
							<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
								<input type="url" class="form-control" id="estado_detalle" name="estado_detalle" value="${ACADEMICO.aca_estado}" readonly="readonly">
							</div>
					</div>
					<div class="form-group">
							<label class="col-xs-2 col-sm-2 col-md-2 col-lg-2   control-label" for="esp_detalle">Especialidades:</label>
							<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1">
								<c:forEach items="${especialidadList}" var="especialidad" >
							    		<input type="text" class="form-control" id="esp_detalle" name="esp_detalle" readonly="readonly" value="${especialidad.esp_nombre}" >
							    		<br>
				    			</c:forEach>
			    			</div>
					</div>
		    	</form>
		   </div>
	    </div>
	</div>
</div>

		