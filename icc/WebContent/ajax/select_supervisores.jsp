<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<div class="form-group ">
<label class="col-xs-3 control-label" for="supervisor">Supervisores Activos :</label>
	<div class="col-xs-9">
		<select class="form-control" id="supervisor" name="supervisor" required>	
			<option value="">Seleccione Supervisor...</option>
			<c:forEach var="supervisores" items="${supervisorList}" >
				<option value="${supervisores.sup_rut}">${supervisores.sup_nombres} ${supervisores.sup_apellido_p} ${supervisores.sup_apellido_m} - ${supervisores.sup_profesion}</option>
			</c:forEach>
		</select>
	</div>
</div>