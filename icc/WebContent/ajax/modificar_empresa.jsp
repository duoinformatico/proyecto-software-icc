<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<link rel="stylesheet" href="css/alertify.core.css" />
<link rel="stylesheet" href="css/alertify.default.css" id="toggleCSS" />
<script src="js/alertify.min.js"></script>
<script type="text/javascript" src="js/validaciones_admin.js"></script>

<div id="myModal_modif" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
 <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
    		<script type="text/javascript">
				$('#salir').click(function() {	 
			    	      location.reload();
				});
			</script>
	        <button type="button"  id="salir" name="salir" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
	        <h4 class="modal-title" id="myModalLabel">Modificar Empresa</h4>
	    </div>
		    <div class="modal-body">
			    <form class="form-horizontal" id="formulario_empresa_modif" role="form" method="POST" action="modificarEmpresaServlet.do">
			    	<div class="form-group">
							<label class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1  control-label" for="rut_modif">Rut:</label>
							<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
								<input type="text" class="form-control" id="rut_modif" name="rut_modif" value="${empresa.emp_rut}" readonly="readonly">
							</div>
					</div>
					<div class="form-group">
						<label class="col-xs-3 control-label" for="region_modif">Región:</label>
						<div class="col-xs-9">							
							<select class="form-control" id="region_modif" name="region_modif" >
							<option value="">Seleccione Región...</option>
							<c:forEach var="regiones" items="${regionList}" >
								<c:if test="${regiones.reg_id == region.reg_id}" >
										<option value="${region.reg_id}" selected="selected">${region.reg_nombre}</option>
								</c:if>
								<c:if test="${regiones.reg_id != region.reg_id}" >
									<option value="${regiones.reg_id}">${regiones.reg_nombre}</option>
								</c:if>
							</c:forEach>
							</select>							
						</div>
						</div>
						<div id="div_provincia_modif">
							<div class="form-group">
							<label class="col-xs-3 control-label" for="provincia_modif">Provincia:</label>
							<div class="col-xs-9">							
								<select class="form-control" id="provincia_modif" name="provincia_modif">				
									<option value="${provincia.pro_id}">${provincia.pro_nombre}</option>						
								</select>					
							</div>
						</div>
						</div>
						<div id="div_comuna_modif">
							<div class="form-group">
								<label class="col-xs-3 control-label" for="comuna_modif">Comuna:</label>
								<div class="col-xs-9">
									<select class="form-control" id="comuna_modif" name="comuna_modif">
										<option value="${comuna.com_id}">${comuna.com_nombre}</option>	
									</select>							
								</div>
							</div>
						</div>
					<script type="text/javascript">
					$(document).ready(function () {
							$('#nombre_modif').bind('copy paste drop', function (e) {
					           e.preventDefault();
					        });       
					        $("#nombre_modif").focusin(function() {
					        	$("#nombre_modif").attr('autocomplete', 'off');
					        });
					        $("#nombre_modif").focusout(function() {
					        	$("#nombre_modif").removeAttr('autocomplete');
					        });
					        $('#direccion_modif').bind('copy paste drop', function (e) {
						           e.preventDefault();
						        });
					        $("#direccion_modif").focusin(function() {
					        	$("#direccion_modif").attr('autocomplete', 'off');
					        });
					        $("#direccion_modif").focusout(function() {
					        	$("#direccion_modif").removeAttr('autocomplete');
					        });
					        $('#telefono_modif').bind('copy paste drop', function (e) {
						           e.preventDefault();
						        });
					        $("#telefono_modif").focusin(function() {
					        	$("#telefono_modif").attr('autocomplete', 'off');
					        	if($("#codigo_modif").val() == ""){
					        		$("#telefono_modif").attr('readonly', 'readonly');
					        		$("#codigo_modif").focus();
					        	}
					        	else{
					        		$("#telefono_modif").removeAttr('readonly');
					        	}
					        	if($("#codigo_modif").val() != ""){
					        		if($("#codigo_modif").val() == 2){
					        			$("#telefono_modif").attr('maxlength', '8');
					        		}
					        		else{
					        			$("#telefono_modif").attr('maxlength', '7');
					        		}
					        	}					        	
					        });
							$("#codigo_modif").change(function(){					        	
								if($("#codigo_modif").val() != ""){
					        		$("#telefono_modif").focus();
					        		$("#telefono_modif").val("");
					        	}					        	
					        });
					        $("#telefono_modif").change(function(){					        	
					        	
					        	if($("#telefono_modif").val().length == 8 && $("#codigo_modif").val() == 2){
					        		$("#telefono_modif").val("+56-"+$("#codigo_modif").val()+"-"+$("#telefono_modif").val());
					        	}
					        	if($("#telefono_modif").val().length == 7 && $("#codigo_modif").val() != 2){
					        		$("#telefono_modif").val("+56-"+$("#codigo_modif").val()+"-"+$("#telefono_modif").val());
					        	}
					        });					        	
					        
					        $("#telefono_modif").focusout(function() {
					        	$("#telefono_modif").removeAttr('autocomplete');
					        	$("#telefono_modif").removeAttr('maxlength');
					        	if($("#codigo_modif").val() == ""){
					        		$("#telefono_modif").attr('readonly', 'readonly');
					        		$("#codigo_modif").focus();
					        	}
					        	else{
					        		$("#telefono_modif").removeAttr('readonly');
					        	}
					        	if($("#telefono_modif").val().length == 8 && $("#codigo_modif").val() == 2){
					        		$("#telefono_modif").val("+56-"+$("#codigo_modif").val()+"-"+$("#telefono_modif").val());
					        	}
					        	if($("#telefono_modif").val().length < 8 && $("#codigo_modif").val() == 2){
					        		$("#telefono_modif").val("");
					        	}
					        	if($("#telefono_modif").val().length == 7 && $("#codigo_modif").val() != 2){
					        		$("#telefono_modif").val("+56-"+$("#codigo_modif").val()+"-"+$("#telefono_modif").val());
					        	}
					        	if($("#telefono_modif").val().length < 7 && $("#codigo_modif").val() != 2){
					        		$("#telefono_modif").val("");
					        	}
					        });
					        $('#celular_modif').bind('copy paste drop', function (e) {
						           e.preventDefault();
						        });
					        $("#celular_modif").focusin(function() {
					        	$("#celular_modif").attr('autocomplete', 'off');
					        	$("#celular_modif").attr('maxlength', '8');					        	
					        });
					       $("#celular_modif").focusout(function() {
					        	$("#celular_modif").removeAttr('autocomplete');
					        	$("#celular_modif").removeAttr('maxlength');
					        	if($("#celular_modif").val().length < 8){
					        		$("#celular_modif").val("");
					        	}
					        	if($("#celular_modif").val().length == 8){
					        		$("#celular_modif").val("+569"+$("#celular_modif").val());
					        	}
					        });
					        $('#descripcion_modif').bind('copy paste drop', function (e) {
						           e.preventDefault();
						        });
					        $("#descripcion_modif").focusin(function() {
					        	$("#descripcion_modif").attr('autocomplete', 'off');
					        });
					        $("#descripcion_modif").focusout(function() {
					        	$("#descripcion_modif").removeAttr('autocomplete');
					        });
					        $('#email_modif').bind('copy paste drop', function (e) {
						           e.preventDefault();
						        });
					        $("#email_modif").focusin(function() {
					        	$("#email_modif").attr('autocomplete', 'off');
					        });
					        $("#email_modif").focusout(function() {
					        	$("#email_modif").removeAttr('autocomplete');
					        });
					        $('#web_modif').bind('copy paste drop', function (e) {
						           e.preventDefault();
						        });
					        $("#web_modif").focusin(function() {
					        	$("#web_modif").attr('autocomplete', 'off');
					        });
					        $("#web_modif").focusout(function() {
					        	$("#web_modif").removeAttr('autocomplete');
					        });
				      });
				    </script>
					<div class="form-group ">
							<label class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1  control-label" for="nombre_modif">Nombre:</label>
							<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
								<input type="text" class="form-control" id="nombre_modif" name="nombre_modif" value="${empresa.emp_nombre}" onkeypress="return funcionMaestra(event,'nombres','1')">
							</div>
					</div>
					<div class="form-group">
							<label class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1  control-label" for="direccion_modif">Dirección:</label>
							<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
								<input type="text" class="form-control" id="direccion_modif" name="direccion_modif" value="${empresa.emp_direccion}" onkeypress="return funcionMaestra(event,'dire','1')">
							</div>
					</div>
					<div class="form-group">
						<label class="col-xs-3 control-label" for="telefono_modif">Teléfono:</label>
						<div class="col-xs-4">
							<select class="form-control" id="codigo_modif" name="codigo_modif">
								<option value="">Código Area...</option>
								<optgroup label="Zona Norte">
									<c:if test="${codigo == 58}" >			
										<option value="58" selected="selected">(58) Arica-Parinacota</option>
									</c:if>
									<c:if test="${codigo != 58}" >			
										<option value="58">(58) Arica-Parinacota</option>
									</c:if>
									<c:if test="${codigo == 57}" >		
										<option value="57" selected="selected">(57) Iquique-Tamarugal</option>	
									</c:if>
									<c:if test="${codigo != 57}" >			
										<option value="57">(57) Iquique-Tamarugal</option>
									</c:if>
									<c:if test="${codigo == 55}" >			
										<option value="55" selected="selected">(55) Tocopilla-Loa-Antofagasta</option>
									</c:if>
									<c:if test="${codigo != 55}" >			
										<option value="55">(55) Tocopilla-Loa-Antofagasta</option>
									</c:if>
									<c:if test="${codigo == 51}" >			
										<option value="51" selected="selected">(51) Huasco</option>
									</c:if>
									<c:if test="${codigo != 51}" >			
										<option value="51">(51) Huasco</option>
									</c:if>
									<c:if test="${codigo == 52}" >			
										<option value="52" selected="selected">(52) Chañaral-Copiapó</option>
									</c:if>
									<c:if test="${codigo != 52}" >			
										<option value="52">(52) Chañaral-Copiapó</option>
									</c:if>
									<c:if test="${codigo == 51}" >			
										<option value="51" selected="selected">(51) Elqui</option>
									</c:if>
									<c:if test="${codigo != 51}" >			
										<option value="51">(51) Elqui</option>
									</c:if>
									<c:if test="${codigo == 53}" >			
										<option value="53" selected="selected">(53) Choapa-Limarí</option>
									</c:if>
									<c:if test="${codigo != 53}" >			
										<option value="53">(53) Choapa-Limarí</option>
									</c:if>
								</optgroup>
								<optgroup label="Zona Centro">
									<c:if test="${codigo == 32}" >			
										<option value="32" selected="selected">(73)>(32) Isla de Pascua-Valparaíso-Marga Marga(Quilpué-Villa Alemana)</option>
									</c:if>
									<c:if test="${codigo != 32}" >			
										<option value="32">(32) Isla de Pascua-Valparaíso-Marga Marga(Quilpué-Villa Alemana)</option>
									</c:if>
									<c:if test="${codigo == 33}" >		
										<option value="33" selected="selected">(73)>(33) Petorca-Quillota-Marga Marga(Limache y Olmué)</option>
									</c:if>
									<c:if test="${codigo != 33}" >			
										<option value="33">(33) Petorca-Quillota-Marga Marga(Limache y Olmué)</option>
									</c:if>
									<c:if test="${codigo == 34}" >			
										<option value="34" selected="selected">(73)>(34) Los Andes-San Felipe de Aconcagua</option>
									</c:if>
									<c:if test="${codigo != 34}" >			
										<option value="34">(34) Los Andes-San Felipe de Aconcagua</option>
									</c:if>
									<c:if test="${codigo == 35}" >		
										<option value="35" selected="selected">(73)>(35) San Antonio</option>	
									</c:if>
									<c:if test="${codigo != 35}" >			
										<option value="35">(35) San Antonio</option>
									</c:if>
									<c:if test="${codigo == 2}" >			
										<option value="2" selected="selected">(73)> ( 2) Chacabuco-Cordillera-Maipo-Melipilla-Santiago-Talagante</option>
									</c:if>
									<c:if test="${codigo != 2}" >			
										<option value="2"> ( 2) Chacabuco-Cordillera-Maipo-Melipilla-Santiago-Talagante</option>
									</c:if>
									<c:if test="${codigo == 72}" >			
										<option value="72" selected="selected">(73)>(72) Cachapoal-ardenal Caro-Colchagua</option>
									</c:if>
									<c:if test="${codigo != 72}" >			
										<option value="72">(72) Cachapoal-ardenal Caro-Colchagua</option>
									</c:if>
									<c:if test="${codigo == 71}" >			
										<option value="71" selected="selected">(73)>(71) Talca</option>
									</c:if>
									<c:if test="${codigo != 71}" >			
										<option value="71">(71) Talca</option>
									</c:if>
									<c:if test="${codigo == 73}" >			
										<option value="73" selected="selected">(73) Cauquenes-Linares</option>
									</c:if>
									<c:if test="${codigo != 73}" >				
										<option value="73">(73) Cauquenes-Linares</option>
									</c:if>
									<c:if test="${codigo == 75}" >			
										<option value="75" selected="selected">(75) Curicó</option> 
									</c:if>
									<c:if test="${codigo != 75}" >			
										<option value="75">(75) Curicó</option>
									</c:if>
									
								</optgroup>
								<optgroup label="Zona Sur">
									<c:if test="${codigo == 41}" >			
										<option value="41" selected="selected">(41) Arauco-Concepción </option>
									</c:if>
									<c:if test="${codigo != 41}" >			
										<option value="41">(41) Arauco-Concepción </option>
									</c:if>
									<c:if test="${codigo == 42}" >			
										<option value="42" selected="selected">(42) Ñuble</option>
									</c:if>
									<c:if test="${codigo != 42}" >			
										<option value="42">(42) Ñuble</option>
									</c:if>
									<c:if test="${codigo == 43}" >			
										<option value="43" selected="selected">(43) Bío-Bío</option>
									</c:if>
									<c:if test="${codigo != 43}" >			
										<option value="43">(43) Bío-Bío</option>
									</c:if>
									<c:if test="${codigo == 45}" >			
										<option value="45" selected="selected">(45) Cautín-Malleco</option>
									</c:if>
									<c:if test="${codigo != 45}" >			
										<option value="45">(45) Cautín-Malleco</option>
									</c:if>
									<c:if test="${codigo == 63}" >		
										<option value="63" selected="selected">(63) Ranco-Valdivia</option>
									</c:if>
									<c:if test="${codigo != 63}" >			
										<option value="63">(63) Ranco-Valdivia</option>
									</c:if>
									<c:if test="${codigo == 64}" >			
										<option value="64" selected="selected">(64) Osorno</option>
									</c:if>
									<c:if test="${codigo != 64}" >			
										<option value="64">(64) Osorno</option>
									</c:if>
									<c:if test="${codigo == 65}" >			
										<option value="65" selected="selected">(65) Chiloé-Llanquihue-Palena </option>
									</c:if>
									<c:if test="${codigo != 65}" >			
										<option value="65">(65) Chiloé-Llanquihue-Palena </option>
									</c:if>
									<c:if test="${codigo == 67}" >			
										<option value="67" selected="selected">(67) Aisén-Capitán Prat-Coihaique-General Carrera</option>
									</c:if>
									<c:if test="${codigo != 67}" >			
										<option value="67">(67) Aisén-Capitán Prat-Coihaique-General Carrera</option>
									</c:if>
									<c:if test="${codigo == 68}" >			
										<option value="68" selected="selected">(68) Aisén(Puyuhuapi)</option>
									</c:if>
									<c:if test="${codigo != 68}" >			
										<option value="68">(68) Aisén(Puyuhuapi)</option>
									</c:if>
									<c:if test="${codigo == 61}" >			
										<option value="61" selected="selected">(61) Antártica Chilena-Magallanes-Tierra del Fuego-Última Esperanza</option>
									</c:if>
									<c:if test="${codigo != 61}" >			
										<option value="61">(61) Antártica Chilena-Magallanes-Tierra del Fuego-Última Esperanza</option>
									</c:if>
								</optgroup>	
							</select>							
						</div>
						<div class="col-xs-5">
							<div class="input-group">
								<c:if test="${bandera == 0}" >
									<input type="tel" class="form-control" id="telefono_modif" name="telefono_modif" value="+56-${codigo}-${telefono}" placeholder="Ej:2323421" onkeypress="return funcionMaestra(event,'fijo',1)" >
								</c:if>
								<c:if test="${bandera == 1}" >
									<input type="tel" class="form-control" id="telefono_modif" name="telefono_modif" value="" placeholder="Ej:2323421" onkeypress="return funcionMaestra(event,'fijo',1)" >
								</c:if>
								
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-phone-alt"></span>
								</span>
							</div>	
						</div>	
					</div>
					<div class="form-group">
						<label class="col-xs-3 control-label" for="celular">Celular:</label>
						<div class="col-xs-9">
							<div class="input-group">
								<input type="tel" class="form-control" id="celular_modif" name="celular_modif" value="${empresa.emp_celular}" placeholder="Ej: +56998345670" onkeypress="return funcionMaestra(event,'celular',1)">
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-phone"></span>
								</span>
							</div>	
						</div>	
					</div>
					<div class="form-group">
							<label class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 control-label" for="descripcion_modif">Descripción:</label>
							<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
								<textarea class="form-control no_resize" id="descripcion_modif" name="descripcion_modif" onkeypress="return funcionMaestra(event,'descri','2')">${empresa.emp_descripcion}</textarea>
							</div>
					</div>
					<div class="form-group ">
							<label class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1  control-label" for="email">Email:</label>
							<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
								<input type="text" class="form-control" id="email_modif" name="email_modif" value="${empresa.emp_email}" onkeypress="return funcionMaestra(event,'email','2')">						
							</div>
					</div>
					<div class="form-group">
							<label class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1  control-label" for="web_modif">Pagina Web:</label>
							<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
								<input type="text" class="form-control" id="web_modif" name="web_modif" value="${empresa.emp_web}" onkeypress="return funcionMaestra(event,'web','2')">
							</div>
					</div>
					<div class="col-xs-offset-4 col-sm-offset-4 col-md-offset-4 col-lg-offset-4">
		    			<button type="submit"  onclick="return validar_empresa(event,2)" class="btn btn-primary">Modificar Empresa</button>
		    		</div>
		    </form>
		   </div>
	    </div>
	</div>
</div>
</div>
		