<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<c:forEach var="i" begin="1" end="9" step="1">
	<c:if test="${ n_asignatura == i}">
		<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
			<select required class="form-control" id="asignatura_${i}" name="asignatura_${i}">	
				<option value="">Asignatura...</option>
				<c:forEach var="asignatura" items="${asignaturasList}" >
					<option value="${asignatura.asi_codigo}">${asignatura.asi_nombre}</option>
				</c:forEach>
			</select>
		</div>
	</c:if>
</c:forEach>
