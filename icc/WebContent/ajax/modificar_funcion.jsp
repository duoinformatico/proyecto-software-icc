<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<link rel="stylesheet" href="css/alertify.core.css" />
<link rel="stylesheet" href="css/alertify.default.css" id="toggleCSS" />
<script src="js/alertify.min.js"></script>
<script type="text/javascript" src="js/validaciones_admin.js"></script>
<div id="myModal_modif" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	 <div class="modal-dialog modal-lg">
	    <div class="modal-content">
	    	<div class="modal-header">
	    		<script type="text/javascript">
					$('#salir').click(function() {	 
				    	      location.reload();
					});
				</script>
		        <button type="button" id="salir" name="salir" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		        <h4 class="modal-title" id="myModalLabel">Modificar Función</h4>
		    </div>
		    <div class="modal-body">
		    	<form class="form-horizontal" id="formulario_funcion_modif" role="form" method="POST" action="modificarFuncionServlet.do">
		    		<input type="hidden" name="funcion_id_m" id="funcion_id_m" value="${id}">
		    		<script type="text/javascript">
					$(document).ready(function () {
							$('#nombre_m').bind('copy paste drop', function (e) {
					           e.preventDefault();
					        });       
					        $("#nombre_m").focusin(function() {
					        	$("#nombre").attr('autocomplete', 'off');
					        });
					        $("#nombre_m").focusout(function() {
					        	$("#nombre_m").removeAttr('autocomplete');
					        });
					});
				    </script>
					<div class="form-group">
					<label class="col-xs-3 control-label" for="nombre_m">Nombre:</label>
						<div class="col-xs-9">
							<input type="text" class="form-control" id="nombre_m" name="nombre_m" value="${funcion.fun_nombre}" placeholder="Ej: Suelos" onkeypress="return funcionMaestra(event,'nombres','1')">
						</div>
					</div>			
					<button type="submit" onclick="return validacion_funcion(event,2)" class="btn btn-primary">Guardar Cambios</button>
				</form>
		    </div>
		</div>
	</div>
</div>