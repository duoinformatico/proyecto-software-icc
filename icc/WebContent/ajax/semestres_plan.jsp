<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<div class="col-xs-12">
<div class="table-responsive">
<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered table-hover" id="datatable">
	<thead>
		<tr>
			<c:forEach var="i" begin="1" end="${semestres}">
				<th>S # ${i}</th>
			</c:forEach>
		</tr>
	</thead>
	<tbody>
		<tr>
			<c:forEach var="i" begin="1" end="${semestres}">
				<td>
					<select class="form-control" id="asignaturas_${i}" name="asignaturas_${i}" required>
						<option value="">N� Asignaturas...</option>
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="4">4</option>
						<option value="5">5</option>
						<option value="6">6</option>
						<option value="7">7</option>
						<option value="8">8</option>
						<option value="9">9</option>
						<option value="10">10</option>
					</select>
				</td>
			</c:forEach>
		</tr>		        		
	</tbody>
</table>
</div>
</div>