<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<link rel="stylesheet" href="css/alertify.core.css" />
<link rel="stylesheet" href="css/alertify.default.css" id="toggleCSS" />
<script src="js/alertify.min.js"></script>
<script type="text/javascript" src="js/validaciones_admin.js"></script>
<div id="myModal_empresas" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	 <div class="modal-dialog modal-lg">
	    <div class="modal-content">
	    	<div class="modal-header">
		        <button type="button"  id="salir" name="salir" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		        <h4 class="modal-title" id="myModalLabel">Listado de Empresas</h4>
		        
		    </div>
		    <div class="modal-body">
		    	<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 columna">
								<div class="table-responsive">
									<table class="table table-striped table-bordered table-hover" >
										<thead>
										  <tr >
										  	 <th>#</th>
										     <th>Rut</th>
										     <th>Nombre</th>
										     <th>Direcci&oacute;n</th>
										  </tr>
								        </thead>
								        <tbody>
								        	<c:set var="count" value="1" scope="page" />
								        	<c:forEach items="${empresaList}" var="empresa" >
								        	<tr>
								        		<td>${count}</td>
								        		<td>${empresa.emp_rut}</td>
								        		<td>${empresa.emp_nombre}</td>
								        		<td>${empresa.emp_direccion}</td>
								        	</tr>
								        	<c:set var="count" value="${count + 1}"/>
								        	</c:forEach>
								        </tbody>
								    </table>
								</div>
						</div>
				</div>
		    </div>
		</div>
	</div>
</div>