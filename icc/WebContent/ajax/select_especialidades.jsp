<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<c:forEach var="i" begin="1" end="4" step="1">
	<c:if test="${ n_especialidad == i}">
		
			<select required class="form-control" id="especialidad_${i}" name="especialidad_${i}">	
				<option value="">Especialidad...</option>
				<c:forEach var="especialidad" items="${especialidadList}" >
					<option value="${especialidad.esp_id}">${especialidad.esp_nombre}</option>
				</c:forEach>
			</select>
			<br>
		
	</c:if>
</c:forEach>
