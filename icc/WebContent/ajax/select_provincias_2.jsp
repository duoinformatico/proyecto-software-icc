<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<div class="form-group">
	<label class="col-xs-3 control-label" for="provincia_2">Provincia Direcci�n Residencial:</label>
	<div class="col-xs-9">	
		<select class="form-control" id="provincia_2" name="provincia_2" required>
			<option value="">Seleccione Provincia...</option>
			<c:forEach var="provincias" items="${provinciaList}" >
				<option value="${provincias.pro_id}">${provincias.pro_nombre}</option>
			</c:forEach>
		</select>							
	</div>
</div>