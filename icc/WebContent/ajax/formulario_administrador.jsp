<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<link rel="stylesheet" href="css/alertify.core.css" />
<link rel="stylesheet" href="css/alertify.default.css" id="toggleCSS" />
<script src="js/alertify.min.js"></script>
<script type="text/javascript" src="js/validar_rut.js"></script>
<script type="text/javascript" src="js/validaciones_admin.js"></script>
<div id="myModal_registro" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	 <div class="modal-dialog modal-lg">
	    <div class="modal-content">
	    	<div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		        <h4 class="modal-title" id="myModalLabel">Formulario de registro Nuevo Administrador</h4>
		    </div>
		    <div class="modal-body">
		    	 <form class="form-horizontal" id="formulario_admin" role="form" method="POST" action="adminServlet.do">
					<div class="form-group">
					<label class="col-xs-3 control-label" for="rut">Rut:</label>
						<div class="col-xs-9">
							<input type="tel" class="form-control" id="rut" name="rut" placeholder="16.765.136-4"
							 ondrop="return false;" onChange="Rut(this.value)" onkeypress="return funcionMaestra(event, 'rut', 1)">
						</div>
					</div>
					<script type="text/javascript">
					    $(document).ready(function () {
							$('#rut').bind('copy paste drop', function (e) {
					           e.preventDefault();
					        });       
					        $("#rut").focusin(function() {
					        	$("#rut").attr('autocomplete', 'off');
					        	$("#rut").attr('maxlength', '9');
					        });
					        $("#rut").focusout(function() {
					        	$("#rut").removeAttr('autocomplete');
					        	$("#rut").removeAttr('maxlength');
					        });
					        $('#nombre').bind('copy paste drop', function (e) {
					           e.preventDefault();
					        });       
					        $("#nombre").focusin(function() {
					        	$("#nombre").attr('autocomplete', 'off');
					        });
					        $("#nombre").focusout(function() {
					        	$("#nombre").removeAttr('autocomplete');
					        });
					        $('#paterno').bind('copy paste drop', function (e) {
						           e.preventDefault();
						        });
					        $("#paterno").focusin(function() {
					        	$("#paterno").attr('autocomplete', 'off');
					        });
					        $("#paterno").focusout(function() {
					        	$("#paterno").removeAttr('autocomplete');
					        });
					        $('#materno').bind('copy paste drop', function (e) {
						           e.preventDefault();
						        });
					        $("#materno").focusin(function() {
					        	$("#materno").attr('autocomplete', 'off');
					        });
					        $("#materno").focusout(function() {
					        	$("#materno").removeAttr('autocomplete');
					        });
					        $('#email').bind('copy paste drop', function (e) {
						           e.preventDefault();
						        });
					        $("#email").focusin(function() {
					        	$("#email").attr('autocomplete', 'off');
					        });
					        $("#email").focusout(function() {
					        	$("#email").removeAttr('autocomplete');
					        });
					        $('#telefono').bind('copy paste drop', function (e) {
						           e.preventDefault();
						        });
					        $("#telefono").focusin(function() {
					        	$("#telefono").attr('autocomplete', 'off');
					        	if($("#codigo").val() == ""){
					        		$("#telefono").attr('readonly', 'readonly');
					        		$("#codigo").focus();
					        	}
					        	else{
					        		$("#telefono").removeAttr('readonly');
					        	}
					        	if($("#codigo").val() != ""){
					        		if($("#codigo").val() == 2){
					        			$("#telefono").attr('maxlength', '8');
					        		}
					        		else{
					        			$("#telefono").attr('maxlength', '7');
					        		}
					        	}					        	
					        });
							$("#codigo").change(function(){					        	
								if($("#codigo").val() != ""){
					        		$("#telefono").focus();
					        		$("#telefono").val("");
					        	}					        	
					        });
					        $("#telefono").change(function(){					        	
					        	
					        	if($("#telefono").val().length == 8 && $("#codigo").val() == 2){
					        		$("#telefono").val("+56-"+$("#codigo").val()+"-"+$("#telefono").val());
					        	}
					        	if($("#telefono").val().length == 7 && $("#codigo").val() != 2){
					        		$("#telefono").val("+56-"+$("#codigo").val()+"-"+$("#telefono").val());
					        	}
					        });					        	
					        
					        $("#telefono").focusout(function() {
					        	$("#telefono").removeAttr('autocomplete');
					        	$("#telefono").removeAttr('maxlength');
					        	if($("#codigo").val() == ""){
					        		$("#telefono").attr('readonly', 'readonly');
					        		$("#codigo").focus();
					        	}
					        	else{
					        		$("#telefono").removeAttr('readonly');
					        	}
					        	if($("#telefono").val().length == 8 && $("#codigo").val() == 2){
					        		$("#telefono").val("+56-"+$("#codigo").val()+"-"+$("#telefono").val());
					        	}
					        	if($("#telefono").val().length < 8 && $("#codigo").val() == 2){
					        		$("#telefono").val("");
					        	}
					        	if($("#telefono").val().length == 7 && $("#codigo").val() != 2){
					        		$("#telefono").val("+56-"+$("#codigo").val()+"-"+$("#telefono").val());
					        	}
					        	if($("#telefono").val().length < 7 && $("#codigo").val() != 2){
					        		$("#telefono").val("");
					        	}
					        });
					        $('#celular').bind('copy paste drop', function (e) {
						           e.preventDefault();
						        });
					        $("#celular").focusin(function() {
					        	$("#celular").attr('autocomplete', 'off');
					        	$("#celular").attr('maxlength', '8');					        	
					        });
					       $("#celular").focusout(function() {
					        	$("#celular").removeAttr('autocomplete');
					        	$("#celular").removeAttr('maxlength');
					        	if($("#celular").val().length < 8){
					        		$("#celular").val("");
					        	}
					        	if($("#celular").val().length == 8){
					        		$("#celular").val("+569"+$("#celular").val());
					        	}
					        });
					        $('#cargo').bind('copy paste drop', function (e) {
						           e.preventDefault();
						        });
					        $("#cargo").focusin(function() {
					        	$("#cargo").attr('autocomplete', 'off');
					        });
					        $("#cargo").focusout(function() {
					        	$("#cargo").removeAttr('autocomplete');
					        });
					      });
				    </script>

					<div class="form-group">
						<label class="col-xs-3 control-label" for="nombre">Nombres</label>
						<div class="col-xs-9">
							<input type="text" class="form-control" id="nombre" name="nombre" placeholder="Jonathan Fernando" onkeypress="return funcionMaestra(event,'nombres','1')">
						</div>	
					</div>
					<div class="form-group">
						<label class="col-xs-3 control-label" for="paterno">Apellido Paterno</label>
						<div class="col-xs-9">
							<input type="text" class="form-control" id="paterno" name="paterno" placeholder="Fierro" onkeypress="return funcionMaestra(event,'nombres','1')">
						</div>	
					</div>
					<div class="form-group">
						<label class="col-xs-3 control-label" for="materno">Apellido Materno</label>
						<div class="col-xs-9">
							<input type="text" class="form-control" id="materno" name="materno" placeholder="Gutierrez" onkeypress="return funcionMaestra(event,'nombres','1')">
						</div>	
					</div>
					<div class="alert alert-warning alert-dismissible" role="alert">
					  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					  <span class="glyphicon glyphicon-remove"></span>
					  <strong>Advertencia</strong> Es muy importante que el email ingresado sea correcto, por favor verificar cuidadosamente al momento de ingresar.
					</div>
					<div class="form-group">
						<label class="col-xs-3 control-label" for="email">E-Mail:</label>
						<div class="col-xs-9">
							<div class="input-group">
								<input type="email" class="form-control" id="email" name="email" placeholder=" ejemplo@ejempo.com" onkeypress="return funcionMaestra(event,'email','2')"> 
								<span class="input-group-addon">
									@
								</span>
							</div>						
						</div>
					</div>
					<div class="form-group">
						<label class="col-xs-3 control-label" for="telefono">Teléfono:</label>
						<div class="col-xs-4">	
							<select class="form-control" id="codigo" name="codigo">
								<option value="">Código Area...</option>
								<optgroup label="Zona Norte">
									<option value="58">(58) Arica-Parinacota</option>
									<option value="57">(57) Iquique-Tamarugal</option>
									<option value="55">(55) Tocopilla-Loa-Antofagasta</option>
									<option value="51">(51) Huasco</option>
									<option value="52">(52) Chañaral-Copiapó</option>
									<option value="51">(51) Elqui</option>
									<option value="53">(53) Choapa-Limarí</option>
								</optgroup>
								<optgroup label="Zona Centro">
									<option value="32">(32) Isla de Pascua-Valparaíso-Marga Marga(Quilpué-Villa Alemana)</option>
									<option value="33">(33) Petorca-Quillota-Marga Marga(Limache y Olmué)</option>
									<option value="34">(34) Los Andes-San Felipe de Aconcagua</option>
									<option value="35">(35) San Antonio</option>
									<option value="2"> ( 2) Chacabuco-Cordillera-Maipo-Melipilla-Santiago-Talagante</option>
									<option value="72">(72) Cachapoal-ardenal Caro-Colchagua</option>
									<option value="71">(71) Talca</option>
									<option value="73">(73) Cauquenes-Linares</option>
									<option value="75">(75) Curicó</option>
								</optgroup>
								<optgroup label="Zona Sur">
									<option value="41">(41) Arauco-Concepción </option>
									<option value="42">(42) Ñuble</option>
									<option value="43">(43) Bío-Bío</option>
									<option value="45">(45) Cautín-Malleco</option>
									<option value="63">(63) Ranco-Valdivia</option>
									<option value="64">(64) Osorno</option>
									<option value="65">(65) Chiloé-Llanquihue-Palena </option>
									<option value="67">(67) Aisén-Capitán Prat-Coihaique-General Carrera</option>
									<option value="68">(68) Aisén(Puyuhuapi)</option>
									<option value="61">(61) Antártica Chilena-Magallanes-Tierra del Fuego-Última Esperanza</option>
								</optgroup>
							</select>							
						</div>
						<div class="col-xs-5">
							<div class="input-group">
								<input type="tel" class="form-control" id="telefono" name="telefono" placeholder="Ej:2323421" onkeypress="return funcionMaestra(event,'fijo',1)" >
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-phone-alt"></span>
								</span>
							</div>	
						</div>	
					</div>
					<div class="form-group">
						<label class="col-xs-3 control-label" for="celular">Celular:</label>
						<div class="col-xs-9">
							<div class="input-group">
								<input type="tel" class="form-control" id="celular" name="celular" placeholder="Ej: +56998345670" onkeypress="return funcionMaestra(event,'celular',1)">
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-phone"></span>
								</span>
							</div>	
						</div>	
					</div>
					<div class="form-group">
						<label class="col-xs-3 control-label" for="cargo">Cargo:</label>
						<div class="col-xs-9">
							<input type="text" class="form-control" id="cargo" name="cargo" placeholder="Ej: Director de Escuela - Coordinador de Prácticas" onkeypress="return funcionMaestra(event,'nombres','1')"
							 required >
						</div>	
					</div>
					<div class="form-group">
						<label class="col-xs-3 control-label" for="estado">Estado</label>
						<div class="col-xs-9">
							<select  class="form-control" id="estado" name="estado" size="1">
								<option value="activo">Activo</option>
								<option value="inactivo">Inactivo</option>
								<option value="eliminado">Eliminado</option>
							</select>
						</div>	
					</div>
					<div class="col-xs-offset-4 col-sm-offset-4 col-md-offset-4 col-lg-offset-4">
						<button type="submit" onclick="return validar_administrador(event,1)" class="btn btn-primary">Registrar Administrador</button>
					</div>
				</form>
		    </div>
		</div>
	 </div>
</div>