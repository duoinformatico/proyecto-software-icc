<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<div class="form-group">
	<label class="col-xs-3 control-label" for="provincia">Provincia:</label>
	<div class="col-xs-9">	
		<select class="form-control" id="provincia" name="provincia" required>
			<option value="">Seleccione Provincia...</option>
			<c:forEach var="provincias" items="${provinciaList}" >
				<option value="${provincias.pro_id}">${provincias.pro_nombre}</option>
			</c:forEach>
		</select>							
	</div>
</div>