<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<script type="text/javascript" src="js/validaciones_admin.js"></script>
<div id="myModalConfirmacion" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	 <div class="modal-dialog modal-md">
	    <div class="modal-content">
	    	<div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		        <h4 class="modal-title">Mensaje de Confirmación</h4>
		    </div>
		    <div class="modal-body">
		    	
					<h4 class="modal-title" id="myModalLabel">${mensaje}</h4>			
				</form>
		    </div>
		    <div class="modal-footer">
		    <form class="form-horizontal" role="form" method="POST" action="GenerarClaveControlador">
		        <div class="col-xs-offset-2 col-sm-offset-2 col-md-offset-2 col-lg-offset-2">
		        	<input type="hidden" id="rut" name="rut" value="${rut}">
					<input type="hidden" id="correo" name="correo" value="${correo}">
					<button type="submit"  class="btn btn-primary">Generar Nueva Contraseña</button>
					<button type="submit"  class="btn btn-danger  btn-xg" data-dismiss="modal">Cancelar</button>
				</div>
			</form>	
		    </div>
		</div>
	</div>
</div>