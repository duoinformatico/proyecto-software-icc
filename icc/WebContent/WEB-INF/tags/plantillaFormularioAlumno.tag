<%@tag description="plantilla alumno" pageEncoding="UTF-8" %>
<%@attribute name="header" fragment="true" %>
<%@attribute name="footer" fragment="true" %>
<!DOCTYPE html>
<html lang="es">
	  <head id="pageheader">
	 	  	<meta charset="utf-8">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta name="viewport" content="width=device-width, initial-scale=1">			
			<link rel="stylesheet" href="css/formularios.css">
			<link rel="stylesheet" href="css/bootstrap.min.css">
			<link rel="stylesheet" href="css/datepicker.css">
			<link rel="stylesheet" href="css/bootstrap.css">
			<link rel="stylesheet" href="css/bootstrap-theme.min.css">
			<link rel="stylesheet" href="css/dataTables.bootstrap.css">
			<link rel="stylesheet" href="css/general.css">
			<script type="text/javascript" src="js/jquery.js"></script>
			<script type="text/javascript" src="js/bootstrap.min.js"></script>
			<script type="text/javascript" src="js/bootstrap-datepicker.js"></script>
			<script type="text/javascript" src="js/validaciones_alumno.js"></script>	
			<script type="text/javascript" src="js/jquery.dataTables.js"></script>
			<script type="text/javascript" src="js/dataTables.bootstrap.js"></script>
			<script type="text/javascript" src="js/dataTables.bootstrapPagination.js"></script>
		</head>
		<jsp:invoke fragment="header"/> 
	  <body>	  	
	  <div class="container-fluid">
	   <div class="container banner">
		   <div class="imagen-banner">
		   	<img src="img/1.png" class="img-responsive"/>
		   	<!-- <img src="img/logoicubb.png"/> -->
		   </div>
		   <header>	  	  
		  	<div class="barra">
				  <nav id="myNavbar" class="navbar navbar-default" role="navigation">			
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
			                    <span class="sr-only">Toggle navigation</span>
			                    <span class="icon-bar"></span>
			                    <span class="icon-bar"></span>
			                    <span class="icon-bar"></span>
			                </button>
							<a class="navbar-brand" href="IndexPerfiles">Inicio</a>
						</div>
						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			                <ul class="nav navbar-nav">
			                    <li><a href="preInscripcionServlet.do"><span class="glyphicon glyphicon-file"></span> Pre-Inscribir Gestión</a></li>
			                    <li><a href="HistorialAlumno"><span class="glyphicon glyphicon-book"></span> Mis Prácticas</a></li>
			               		<li><a href="EvaluacionesControlador"><span class="glyphicon glyphicon-list-alt"></span> Mis Evaluaciones</a></li>
			                </ul>
			                <ul class="nav navbar-nav navbar-right">
			                    <li class="dropdown">
			                        <a href="#" data-toggle="dropdown" class="dropdown-toggle"><span class="glyphicon glyphicon-cog"></span>Perfil Personal <b class="caret"></b></a>
			                        <ul class="dropdown-menu">
			                            <li><a href="perfilesServlet.do"><span class="glyphicon glyphicon-user"></span> Información Personal</a></li>
			                            <li><a href="MenuCambioClave"><span class="glyphicon glyphicon-credit-card"></span> Cambiar Password</a></li>
			                            <li><a href="CerrarSesionControlador"><span class="glyphicon glyphicon-off"></span> Cerrar Sesión</a></li>
			                        </ul>
			                    </li>
			                </ul>
			           </div>
	
				  </nav>
			    </div>
		  	</header>   	
		 </div>
		 <div class="container">
	  		<jsp:doBody />
	  		<footer>
	  		
	  		</footer>
	  	  </div>
	  		</div>  
  	  </body>
 </html>