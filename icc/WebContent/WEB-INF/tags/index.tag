<%@tag description="plantilla index" pageEncoding="UTF-8" %>
<%@attribute name="header" fragment="true" %>
<%@attribute name="footer" fragment="true" %>
<!DOCTYPE html>
<html lang="es">
<head>
	<div id="pageheader">
	  	<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">		
		<link rel="stylesheet" href="css/formularios.css">
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
		<link rel="stylesheet" href="css/bootstrap-theme.min.css">
		<link rel="stylesheet" href="css/general.css">
		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/bootstrap.min.js"></script>
		<script type="text/javascript" src="js/validar_rut.js"></script>
		<script type="text/javascript" src="js/validaciones_admin.js"></script>
		<jsp:invoke fragment="header"/>
	</div>
</head>  
<body>
	<div class="container-fluid">
		<div class="container banner">
			<div class="imagen-banner">
				<img src="img/1.png" class="img-responsive" alt="Responsive image"/>
				<!-- <img src="img/logoicubb.png"/> -->
			</div>
			<header>
				<div class="barra">
					<nav id="myNavbar" class="navbar navbar-default" role="navigation">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle"
								data-toggle="collapse"
								data-target="#bs-example-navbar-collapse-1">
								<span class="sr-only">Toggle navigation</span> 
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
							<a class="navbar-brand" href="#">Inicio</a>
						</div>
						<div class="collapse navbar-collapse"
							id="bs-example-navbar-collapse-1">
							<ul class="nav navbar-nav navbar-right">
								<li class="dropdown" id="menuLogin"><a
									class="dropdown-toggle" href="#" data-toggle="dropdown"
									id="navLogin">Login</a>
									<div class="dropdown-menu" style="padding: 20px; min-width: 220px;">
										<script type="text/javascript">
										    $(document).ready(function () {
										    	$('#rut').bind('copy paste drop', function (e) {
											           e.preventDefault();
											        });
										        $("#rut").focusin(function() {
										        	$("#rut").attr('autocomplete', 'off');
										        });
										        $("#rut").focusout(function() {
										        	$("#rut").removeAttr('autocomplete');
										        });
										    	$('#pass').bind('copy drop', function (e) {
											           e.preventDefault();
											        });
										        $("#pass").focusin(function() {
										        	$("#pass").attr('autocomplete', 'off');
										        });
										        $("#pass").focusout(function() {
										        	$("#pass").removeAttr('autocomplete');
										        });
										    });
										</script>
										<form class="form-horizontal" id="formLogin" role="form" method="POST" action="LoginControlador">

											<input name="rut" id="rut" class="form-control" placeholder="Ej: 17.541.517-3" type="tel" maxlength="9" required
											 onChange="Rut(this.value)" onkeypress="return funcionMaestra(event, 'rut', 1)">
											<br>
											<input name="pass" id="pass" class="form-control" placeholder="Password" type="password" required><br>
											
											<button type="submit" class="btn btn-primary">Login</button>

										</form>
									</div>
								</li>
							</ul>
						</div>
					</nav>
				</div>
			</header>
		</div>
		<div class="container">
		<br>
		<div class="row">
			<div class="col-xs-12 col-sm-10 col-md-10 col-lg-10 col-sm-offset-1 col-md-offset-1 col-lg-offset-1">
				<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
			  <!-- Indicators -->
			  <ol class="carousel-indicators">
			    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
			    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
			    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
			    <li data-target="#carousel-example-generic" data-slide-to="3"></li>
			  </ol>
			
			  <!-- Wrapper for slides -->
			  <div class="carousel-inner">
			    <div class="item active">
			      <img src="img/contru/c0.jpg" 	class="CarouselImg">
			      <div class="carousel-caption">
			        ...
			      </div>
			    </div>
			    <div class="item">
			      <img src="img/contru/c2.jpg" class="CarouselImg">
			      <div class="carousel-caption">
			        ...
			      </div>
			    </div>
			    <div class="item">
			      <img src="img/contru/c6.jpg" class="CarouselImg">
			      <div class="carousel-caption">
			        ...
			      </div>
			    </div>
			    <div class="item">
			      <img src="img/contru/c1.jpg" class="CarouselImg">
			      <div class="carousel-caption">
			        ...
			      </div>
			    </div>

			  </div>
			
			  <!-- Controls -->
			  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
			    <span class="glyphicon glyphicon-chevron-left"></span>
			  </a>
			  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
			    <span class="glyphicon glyphicon-chevron-right"></span>
			  </a>
			</div>
			</div>
		</div>
		<br>
			
			<script>
			    $(document).ready(function(){
			        $('#carousel-example-generic').carousel({
			            interval: 2500
			        });
			    });
			</script>
			
			<footer> </footer>
			<jsp:doBody />
		</div>
	</div>
</body>
</html>