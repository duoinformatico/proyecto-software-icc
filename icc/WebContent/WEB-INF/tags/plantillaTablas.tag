<%@tag description="Simple Wrapper Tag" pageEncoding="UTF-8" %>
<%@attribute name="header" fragment="true" %>
<%@attribute name="footer" fragment="true" %>
<%@attribute name="menu" fragment="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="es">
  <head id="pageheader">
		<meta http-equiv="Content-Type" content="text/html;">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta charset="utf-8">
		<link rel="stylesheet" href="css/formularios.css">		
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<link rel="stylesheet" href="css/dataTables.bootstrap.css">
		<link rel="stylesheet" href="css/bootstrap.css">
		<link rel="stylesheet" href="css/bootstrap-theme.min.css">
		<link rel="stylesheet" href="css/datepicker.css">		
	    <link rel="stylesheet" href="extensions/TableTools/css/dataTables.tableTools.css">
		<link rel="stylesheet" href="css/multi-select.css">
		<link rel="stylesheet" href="css/general.css">
		<script type='text/javascript' src="js/validaciones.js"></script>
		<script type='text/javascript' src="js/validaciones_admin.js"></script>
		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/jquery.dataTables.js"></script>
		<script type="text/javascript" src="js/bootstrap.min.js"></script>
		<script type="text/javascript" src="js/dataTables.bootstrap.js"></script>
		<script type="text/javascript" src="js/dataTables.bootstrapPagination.js"></script>
		<script type="text/javascript" src="js/bootstrap-datepicker.js"></script>
		<script type="text/javascript" src="extensions/TableTools/js/dataTables.tableTools.js"></script>
		<script type="text/javascript" src="js/jquery.multi-select.js"></script>
		
		<script type="text/javascript">
			$.fn.dataTable.TableTools.defaults.aButtons = ["pdf", "xls" ];
			$(document).ready(function() {
			    var table = $('#datatable').DataTable();
			    var tt = new $.fn.dataTable.TableTools( table ); 
			    $( tt.fnContainer() ).insertBefore('div.dataTables_wrapper');    
			} );
			
		</script>
	<jsp:invoke fragment="header"/>
  </head>
  <body>  
  		<div class="container-fluid">
			   <div class="container banner">
				   <div class="imagen-banner">
					   <img src="img/1.png" class="img-responsive"/>
					   	<!-- <img src="img/logoicubb.png"/> -->
				   </div>
			   <header>
			   <div class="barra">
						<nav id="Navbar" class="navbar navbar-default" role="navigation">
								<div class="navbar-header">
									<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					                    <span class="sr-only">Navegación</span>
					                    <span class="icon-bar"></span>
					                    <span class="icon-bar"></span>
					                    <span class="icon-bar"></span>
					                </button>
									<a class="navbar-brand " href="IndexPerfiles">Inicio</a>
								</div>
								<c:if test="${tipo == 'admin'}" >	
									<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						                <ul class="nav navbar-nav">
						                    <li class="dropdown">
						                        <a href="#" data-toggle="dropdown" class="dropdown-toggle">Opciones de Administración <b class="caret"></b></a>
						                        <ul class="dropdown-menu">
						                            <li><a href="academicoServlet.do">Controlar Académicos</a></li>
						                            <li><a href="areasServlet.do">Controlar Areas</a></li>
						                            <li><a href="adminServlet.do">Controlar Administradores</a></li>
						                            <li><a href="asignaturaServlet.do">Controlar Asignatura</a>
						                            <li><a href="empresaServlet.do">Controlar Empresas</a></li>
						                            <li><a href="especialidadServlet.do">Controlar Especialidades</a></li>
						                            <li><a href="funcionesServlet.do">Controlar Funciones</a></li>
						                            <li><a href="supervisorServlet.do">Controlar Supervisor</a></li>
						                            <li><a href="planesServlet.do">Controlar Planes de Estudio</a></li>
						                            <li><a href="MenuPracticas">Historial de Prácticas</a></li>
						                            <li><a href="EvaluacionesControlador">Historial Evaluaciones</a></li>
						                            <li><a href="MenuRecuperarClaveControlador">Recuperar Password</a></li>
						                            <li><a href="CargaAlumnoControlador">Sincronizar BD UBB</a></li>
						                            <li><a href="GenerarInformesControlador">Generar Informes</a></li>
						                            <li><a href="MenuRubricaControlador">Mantener Rúbricas</a></li>
						                            <!--  <li><a href="#">Controlar Plan de Estudio</a></li>
						                            <li><a href="#">Controlar Asignatura</a></li>
						                            
						                            <li><a href="MenuObraControlador">Registrar Tipo de Obra</a></li>-->
						                        </ul>
						                    </li>
	
						                    <li><a href="MenuPendientesControlador"><span class="glyphicon glyphicon-list-alt"></span> Pre-Inscripciones Pendientes</a></li>
						                    <li><a href="ComisionControlador"><span class="glyphicon glyphicon-th-list"></span> Asignar Académicos</a></li>
						                    <!--  
						                    <li class="dropdown">
						                        <a href="#" data-toggle="dropdown" class="dropdown-toggle"><span class="glyphicon glyphicon-briefcase"></span> Rúbricas<b class="caret"></b></a>
						                        <ul class="dropdown-menu">
						                            <li><a href="FormularioAutoevaluacionControlador">Autoevaluación I-II-III</a></li>
						                            <li><a href="FormularioEvaluacionSupervisorControlador">Ev Supervisor I-II-III</a></li>
						                            <li><a href="FormularioEvaluacionInformeControlador">Ev Informe I-II-III</a></li>
						                            <li><a href="#">Autoevaluación G.Profesional</a></li>
						                            <li><a href="#">Ev Supervisor G.Profesional</a></li>
						                            <li><a href="#">Ev Informe G.Profesional</a></li>
						                            <li><a href="#">Ev Comisión de Titulo</a></li>
						                            <li><a href="MenuInstrumentosControlador">Ver Rubricas</a></li>
						                        </ul>
						                    </li>
						                    -->
						                    
						                </ul>
						                <ul class="nav navbar-nav navbar-right">
						                    <li class="dropdown">
						                        <a href="#" data-toggle="dropdown" class="dropdown-toggle"><span class="glyphicon glyphicon-cog"></span> Perfil Personal <b class="caret"></b></a>
						                        <ul class="dropdown-menu">
						                            <li><a href="perfilesServlet.do"><span class="glyphicon glyphicon-user"></span>  Información Personal</a></li>
						                            <li><a href="MenuCambioClave"><span class="glyphicon glyphicon-credit-card"></span> Cambiar Password</a></li>
						                            <li><a href="CerrarSesionControlador"><span class="glyphicon glyphicon-off"></span>  Cerrar Sesión</a></li>
						                        </ul>
						                    </li>
						                </ul>
						           </div>
						         </c:if>
						         <c:if test="${tipo == 'administrador-academico'}" >
						             <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						                <ul class="nav navbar-nav">
						                    <li class="dropdown">
						                        <a href="#" data-toggle="dropdown" class="dropdown-toggle">Opciones de Administración<b class="caret"></b></a>
						                        <ul class="dropdown-menu">
						                            <li><a href="academicoServlet.do">Controlar Académicos</a></li>
						                            <li><a href="areasServlet.do">Controlar Areas</a></li>
						                            <li><a href="adminServlet.do">Controlar Administradores</a></li>
						                            <li><a href="asignaturaServlet.do">Controlar Asignatura</a>
						                            <li><a href="empresaServlet.do">Controlar Empresas</a></li>
						                            <li><a href="especialidadServlet.do">Controlar Especialidades</a></li>
						                            <li><a href="funcionesServlet.do">Controlar Funciones</a></li>
						                            <li><a href="supervisorServlet.do">Controlar Supervisor</a></li>
						                            <li><a href="planesServlet.do">Controlar Planes de Estudio</a></li>
						                            <li><a href="MenuPracticas">Historial de Prácticas</a></li>
						                            <li><a href="EvaluacionesControlador">Historial de Evaluaciones</a></li>
						                            <li><a href="MenuRecuperarClaveControlador">Recuperar Password</a></li>
						                            <li><a href="CargaAlumnoControlador">Sincronizar BD UBB</a></li>
						                            <li><a href="GenerarInformesControlador">Generar Informes</a></li>
						                            <li><a href="MenuRubricaControlador">Mantener Rúbricas</a></li>
						                            <!--  <li><a href="#">Controlar Plan de Estudio</a></li>
						                            <li><a href="#">Controlar Asignatura</a></li>
						                            
						                            <li><a href="MenuObraControlador">Registrar Tipo de Obra</a></li>-->
						                        </ul>
						                    </li>
											<li class="dropdown">
						                        <a href="#" data-toggle="dropdown" class="dropdown-toggle">Opciones Académicas<b class="caret"></b></a>
						                        <ul class="dropdown-menu">
						                        	<li><a href="MenuEvaluacionControlador">Evaluar Alumnos</a></li>
							                    	<li><a href="MenuSeguirAlumnos">Seguir Alumnos</a></li>
						                        </ul>
						                    </li>
						                    <li><a href="MenuPendientesControlador"><span class="glyphicon glyphicon-list-alt"></span> Pre-Inscripciones Pendientes</a></li>
						                    <li><a href="ComisionControlador"><span class="glyphicon glyphicon-th-list"></span> Asignar Académicos</a></li>
						                    
						                    <!--  
						                    <li class="dropdown">
						                        <a href="#" data-toggle="dropdown" class="dropdown-toggle"><span class="glyphicon glyphicon-briefcase"></span> Rúbricas<b class="caret"></b></a>
						                        <ul class="dropdown-menu">
						                            <li><a href="FormularioAutoevaluacionControlador">Autoevaluación I-II-III</a></li>
						                            <li><a href="FormularioEvaluacionSupervisorControlador">Ev Supervisor I-II-III</a></li>
						                            <li><a href="FormularioEvaluacionInformeControlador">Ev Informe I-II-III</a></li>
						                            <li><a href="#">Autoevaluación G.Profesional</a></li>
						                            <li><a href="#">Ev Supervisor G.Profesional</a></li>
						                            <li><a href="#">Ev Informe G.Profesional</a></li>
						                            <li><a href="#">Ev Comisión de Titulo</a></li>
						                            <li><a href="MenuInstrumentosControlador">Ver Rubricas</a></li>
						                        </ul>
						                    </li>
						                    -->
						                    
						                </ul>	
						                <ul class="nav navbar-nav navbar-right">
						                    <li class="dropdown">
						                        <a href="#" data-toggle="dropdown" class="dropdown-toggle"><span class="glyphicon glyphicon-cog"></span> Perfil Personal <b class="caret"></b></a>
						                        <ul class="dropdown-menu">
						                            <li><a href="perfilesServlet.do"><span class="glyphicon glyphicon-user"></span>  Información Personal</a></li>
						                            <li><a href="MenuCambioClave"><span class="glyphicon glyphicon-credit-card"></span> Cambiar Password</a></li>
						                            <li><a href="CerrarSesionControlador"><span class="glyphicon glyphicon-off"></span>  Cerrar Sesión</a></li>
						                        </ul>
						                    </li>
			                 			</ul>		                
						           </div>
						         </c:if>
						</nav>
					</div>
				</header>
			</div>
			<div class="container">			
	  		    <footer>		
	  		    </footer>
	  		    <jsp:doBody />
  			</div>
		</div>
  </body>
</html>