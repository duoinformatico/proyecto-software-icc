<%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:plantillaAcademico>
	<jsp:attribute name="header">
		<script>
		$(document)
				.ready(
						function() {
							$('#datatable')
									.dataTable(
											{
												"sDom" : "<'row'<'col-xs-6'T><'col-xs-6'f>r>t<'row'<'col-xs-6'i><'col-xs-6'p>>",
												"sPaginationType" : "bootstrap"
											});
						});
		</script>
    </jsp:attribute>
    <jsp:body>
			<div class="row">
				<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1">
					<legend>Estudiantes que Usted tiene a Cargo.</legend>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 columna">
						<div class="table-responsive">
							<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered table-hover" id="datatable">
								<thead>
								  <tr>
								  	 <th>Rut Alumno</th>
								  	 <th>Nombre de Obra</th>
								     <th>Rut Empresa</th>				     
								     <th>Estado</th>
								     <th>Revisi�n</th>
								  </tr>
						        </thead>
						        <tbody>
						        	<c:forEach items="${practicaList}" var="practica" >
						        	<tr>
						        		<td>${practica.alu_rut}</td>
						      			<td>${practica.pra_obra_nombre}</td>
						        		<td>${practica.emp_rut}</td>
						        		<td>${practica.pra_estado}</td>
										<td>
											<form action="BitacoraServlet.do" method="POST" >
												<input type="hidden" id="id" name="id" value="${practica.pra_id}"/>
												<c:if test="${practica.pra_estado == 'Finalizada'}" >
								        			<c:forEach items="${estadosList}" var="estado">
							        					<c:if test="${(estado.pra_id == practica.pra_id) && (estado.bean_uno == 0) }">
							        						<button type="button" class="btn btn-success  btn-xs" id="'${estado.pra_id}'" disabled="disabled"><span class="glyphicon glyphicon-ok"></span>Informe No Subido</button>
							        					</c:if>
							        					<c:if test="${(estado.pra_id == practica.pra_id) && (estado.bean_uno == 1) }">
							        						<a href="FormularioInformeControlador?fileName=${practica.pra_informe}" class="btn btn-success  btn-xs"> <span class="glyphicon glyphicon-ok"></span> Descargar Informe</a>
							        					</c:if>									        					
							        				</c:forEach>
							        			</c:if>
							        			<button type="submit" id="bitacora" name="bitacora" value="bitacora"  class="btn btn-primary  btn-xs" ><span class="glyphicon glyphicon-folder-open"></span> Revisar</button>
											</form>				
										</td>
						        	</tr>
						        	</c:forEach>
						        </tbody>
						    </table>
						</div>
					</div>
			</div>
	</jsp:body>
</t:plantillaAcademico>
