<%@ page language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:plantillaAcademico>
    <jsp:attribute name="header">
    </jsp:attribute>
    <jsp:body>
    	<div class="row">
    		<div class="col-xs-12 col-sm-12 col-sm-offset-1 col-md-offset-1 columna">
    			<h3>Bienvenid@ al Sistema: ${usuario} (Perfil Académico)</h3>
    		</div>
    	</div>	
    	<div class="row">
    		<div class="col-xs-8 col-sm-8 col-xs-offset-2 col-sm-offset-2 col-md-offset-2 columna">
    			<h3>- Usted tiene a cargo a <span class="badge">${total}</span> Estudiantes con Práctica Activa este Semestre.</h3>
    		</div>
    	</div>	
    </jsp:body>
</t:plantillaAcademico>