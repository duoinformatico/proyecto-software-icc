function confirmar(rut)
{
		var respuesta = confirm('¿Está seguro que desea eliminar  '+rut+'?');
		if(respuesta)
		{
			location.href='AccionesEmpresaControlador?rut='+rut;
		}
}
function redireccionar(id)
{
		location.href='FormularioPendientesControlador?id='+id;
}

function utf8_encode(argString) {
	  //  discuss at: http://phpjs.org/functions/utf8_encode/
	  // original by: Webtoolkit.info (http://www.webtoolkit.info/)
	  // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	  // improved by: sowberry
	  // improved by: Jack
	  // improved by: Yves Sucaet
	  // improved by: kirilloid
	  // bugfixed by: Onno Marsman
	  // bugfixed by: Onno Marsman
	  // bugfixed by: Ulrich
	  // bugfixed by: Rafal Kukawski
	  // bugfixed by: kirilloid
	  //   example 1: utf8_encode('Kevin van Zonneveld');
	  //   returns 1: 'Kevin van Zonneveld'

	  if (argString === null || typeof argString === 'undefined') {
	    return '';
	  }

	  var string = (argString + ''); // .replace(/\r\n/g, "\n").replace(/\r/g, "\n");
	  var utftext = '',
	    start, end, stringl = 0;

	  start = end = 0;
	  stringl = string.length;
	  for (var n = 0; n < stringl; n++) {
	    var c1 = string.charCodeAt(n);
	    var enc = null;

	    if (c1 < 128) {
	      end++;
	    } else if (c1 > 127 && c1 < 2048) {
	      enc = String.fromCharCode(
	        (c1 >> 6) | 192, (c1 & 63) | 128
	      );
	    } else if ((c1 & 0xF800) != 0xD800) {
	      enc = String.fromCharCode(
	        (c1 >> 12) | 224, ((c1 >> 6) & 63) | 128, (c1 & 63) | 128
	      );
	    } else { // surrogate pairs
	      if ((c1 & 0xFC00) != 0xD800) {
	        throw new RangeError('Unmatched trail surrogate at ' + n);
	      }
	      var c2 = string.charCodeAt(++n);
	      if ((c2 & 0xFC00) != 0xDC00) {
	        throw new RangeError('Unmatched lead surrogate at ' + (n - 1));
	      }
	      c1 = ((c1 & 0x3FF) << 10) + (c2 & 0x3FF) + 0x10000;
	      enc = String.fromCharCode(
	        (c1 >> 18) | 240, ((c1 >> 12) & 63) | 128, ((c1 >> 6) & 63) | 128, (c1 & 63) | 128
	      );
	    }
	    if (enc !== null) {
	      if (end > start) {
	        utftext += string.slice(start, end);
	      }
	      utftext += enc;
	      start = end = n + 1;
	    }
	  }

	  if (end > start) {
	    utftext += string.slice(start, stringl);
	  }

	  return utftext;
	}
function utf8_decode(str_data) {
	  //  discuss at: http://phpjs.org/functions/utf8_decode/
	  // original by: Webtoolkit.info (http://www.webtoolkit.info/)
	  //    input by: Aman Gupta
	  //    input by: Brett Zamir (http://brett-zamir.me)
	  // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	  // improved by: Norman "zEh" Fuchs
	  // bugfixed by: hitwork
	  // bugfixed by: Onno Marsman
	  // bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	  // bugfixed by: kirilloid
	  //   example 1: utf8_decode('Kevin van Zonneveld');
	  //   returns 1: 'Kevin van Zonneveld'

	  var tmp_arr = [],
	    i = 0,
	    ac = 0,
	    c1 = 0,
	    c2 = 0,
	    c3 = 0,
	    c4 = 0;

	  str_data += '';

	  while (i < str_data.length) {
	    c1 = str_data.charCodeAt(i);
	    if (c1 <= 191) {
	      tmp_arr[ac++] = String.fromCharCode(c1);
	      i++;
	    } else if (c1 <= 223) {
	      c2 = str_data.charCodeAt(i + 1);
	      tmp_arr[ac++] = String.fromCharCode(((c1 & 31) << 6) | (c2 & 63));
	      i += 2;
	    } else if (c1 <= 239) {
	      // http://en.wikipedia.org/wiki/UTF-8#Codepage_layout
	      c2 = str_data.charCodeAt(i + 1);
	      c3 = str_data.charCodeAt(i + 2);
	      tmp_arr[ac++] = String.fromCharCode(((c1 & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
	      i += 3;
	    } else {
	      c2 = str_data.charCodeAt(i + 1);
	      c3 = str_data.charCodeAt(i + 2);
	      c4 = str_data.charCodeAt(i + 3);
	      c1 = ((c1 & 7) << 18) | ((c2 & 63) << 12) | ((c3 & 63) << 6) | (c4 & 63);
	      c1 -= 0x10000;
	      tmp_arr[ac++] = String.fromCharCode(0xD800 | ((c1 >> 10) & 0x3FF));
	      tmp_arr[ac++] = String.fromCharCode(0xDC00 | (c1 & 0x3FF));
	      i += 4;
	    }
	  }

	  return tmp_arr.join('');
	}


function cambiarPass(nombres,apellido_p,apellido_m,correo,tipo)//cambia la contraseña de un usuario
{
	var rut = $("#rut").val();
	var nombres_utf8 = utf8_decode(nombres);
	var paterno_utf8 = utf8_decode(apellido_p);
	var materno_utf8 = utf8_decode(apellido_m);
	$.ajax({
		async: true,
		type: "get",
		dataType: "html",
		contentType: "aplication/x-www-form-urlencoded",
		url:"AjaxControlador",
		data: {rut_confirmacion_pass:rut,nombres:nombres_utf8,apellido_p:paterno_utf8,apellido_m:materno_utf8,correo:correo,tipo:tipo},
		success: function(datos){
			$("#div_confirmacion").empty();
			$("#div_confirmacion").html(''+datos);
			$("#myModalConfirmacion").modal('show');
		}
	});
	return false;
}

function cargarInscripcionAsignatura(){//envía los parametros para la carga de inscripcion de asignaturas.
	if(!$("#agnio").val()){
		return false;
	}
	if(!$("#periodo").val()){
		return false;
	}
	$.ajax({
		async: true,
		type: "get",
		dataType: "html",
		contentType: "aplication/x-www-form-urlencoded",
		url:"AjaxControlador",
		data: {agnio:$("#agnio").val(), periodo:$("#periodo").val()},
		success: function(datos){
			$("#div_inscripciones").empty();
			$("#div_inscripciones").html(''+datos);
		}
	});
	return false;
}

function cargarAlumnosPlanEstudio(){//envía los parametros para la carga de alumnos + cursa plan de estudios.
	if(!$("#agnio_alumno").val()){
		return false;
	}
	if(!$("#periodo_alumno").val()){
		return false;
	}
	$.ajax({
		async: true,
		type: "get",
		dataType: "html",
		contentType: "aplication/x-www-form-urlencoded",
		url:"AjaxControlador",
		data: {agnio_alumno:$("#agnio_alumno").val(), periodo_alumno:$("#periodo_alumno").val()
	
		}
	});
	return false;
}

function agregarEspecialidad()//agrega un select especialidades
{
	if($("#actual_especialidades").val() == $("#num_especialidades").val()){
		alert("No puede agregar mas especialidades");
	}
	else{
		for(var i = 0; i < $("#num_especialidades").val(); i++){
			if($("#actual_especialidades").val() == i){				
				$("#actual_especialidades").val(i+1);
				
				var n_especialidad = $("#actual_especialidades").val();
				$.ajax({
					async: true,
					type: "get",
					dataType: "html",
					contentType: "aplication/x-www-form-urlencoded",
					url:"AjaxControlador",
					data: {n_especialidad : n_especialidad},
					success: function(datos){
						$("#div_especialidad_"+(i+1)).html(''+datos);						
					}
				});
				break;
			}
			
		}
	}
}

function quitarEspecialidad()//quita un select de especialidad
{
	if($("#actual_especialidades").val() == 0){
			
	}
	else{
		for(var i = 1; i <= $("#num_especialidades").val(); i++){
			if($("#actual_especialidades").val() == i){				
				$("#actual_especialidades").val(i-1);
				if(i != $("#num_especialidades").val()){
					$("#actual_especialidades").val(i-1);
				}
				$("#div_especialidad_"+(i)).empty();
				break;
			}
		}
	}
}

function registrarPlan(id)//redirecciona para registrar un plan
{
	$.ajax({
		async: true,
		type: "get",
		dataType: "html",
		contentType: "aplication/x-www-form-urlencoded",
		url:"menuPlanesServlet.do",
		data: {id : id},
		success: function(datos){
			$("#div_registro").empty();
			$("#div_registro").html(''+datos);
			$("#myModal_registro").modal('show');
		}
	});
	return false;
}

function registrarRubrica(id)//redirecciona para registrar una rúbrica
{
	$.ajax({
		async: true,
		type: "get",
		dataType: "html",
		contentType: "aplication/x-www-form-urlencoded",
		url:"AjaxControlador",
		data: {rubrica_id : id},
		success: function(datos){
			$("#div_registro").empty();
			$("#div_registro").html(''+datos);
			$("#myModal_registro").modal('show');
		}
	});
	return false;
}

function semestresPlan(semestres)//
{
	$.ajax({
		async: true,
		type: "get",
		dataType: "html",
		contentType: "aplication/x-www-form-urlencoded",
		url:"AjaxControlador",
		data: {cantidad_semestres : semestres},
		success: function(datos){
			$("#div_semestres").empty();
			$("#div_semestres").html(''+datos);
		}
	});
	return false;
}

function registrarAcademico(id)//redirecciona para registrar un academico
{
	$.ajax({
		async: true,
		type: "get",
		dataType: "html",
		contentType: "aplication/x-www-form-urlencoded",
		url:"menuAcademicoServlet.do",
		data: {id : id},
		success: function(datos){
			$("#div_registro").empty();
			$("#div_registro").html(''+datos);
			$("#myModal_registro").modal('show');
		}
	});
	return false;
}

function registrarAdministrador(id)//redirecciona para registrar un administrador
{
	$.ajax({
		async: true,
		type: "get",
		dataType: "html",
		contentType: "aplication/x-www-form-urlencoded",
		url:"menuAdministradorServlet.do",
		data: {id : id},
		success: function(datos){
			$("#div_registro").empty();
			$("#div_registro").html(''+datos);
			$("#myModal_registro").modal('show');
		}
	});
	return false;
}

function registrarEmpresa(id)//redirecciona para registrar una empresa
{
	$.ajax({
		async: true,
		type: "get",
		dataType: "html",
		contentType: "aplication/x-www-form-urlencoded",
		url:"menuEmpresaServlet.do",
		data: {id : id},
		success: function(datos){
			$("#div_registro").empty();
			$("#div_registro").html(''+datos);
			$("#myModal_registro").modal('show');
		}
	});
	return false;
}
function registrarArea(id)
{
	$.ajax({
		async: true,
		type: "get",
		dataType: "html",
		contentType: "aplication/x-www-form-urlencoded",
		url:"menuAreaServlet.do",
		data: {id : id},
		success: function(datos){
			$("#div_registro").empty();
			$("#div_registro").html(''+datos);
			$("#myModal_registro").modal('show');
		}
	});
	return false;
}

function registrarFuncion(id)
{
	$.ajax({
		async: true,
		type: "get",
		dataType: "html",
		contentType: "aplication/x-www-form-urlencoded",
		url:"menuFuncionServlet.do",
		data: {id : id},
		success: function(datos){
			$("#div_registro").empty();
			$("#div_registro").html(''+datos);
			$("#myModal_registro").modal('show');
		}
	});
	return false;
}
function registrarSupervisor(id)//redirecciona para registrar un supervisor
{
	$.ajax({
		async: true,
		type: "get",
		dataType: "html",
		contentType: "aplication/x-www-form-urlencoded",
		url:"menuSupervisorServlet.do",
		data: {id : id},
		success: function(datos){
			$("#div_registro").empty();
			$("#div_registro").html(''+datos);
			$("#myModal_registro").modal('show');
		}
	});
	return false;
}

function registrarEspecialidad(id)//redirecciona para registrar una especialidad
{
	$.ajax({
		async: true,
		type: "get",
		dataType: "html",
		contentType: "aplication/x-www-form-urlencoded",
		url:"menuEspecialidadServlet.do",
		data: {id : id},
		success: function(datos){
			$("#div_registro").empty();
			$("#div_registro").html(''+datos);
			$("#myModal_registro").modal('show');
		}
	});
	return false;
}

function verRubrica(ins_id, rut, pra_id, rol_id)//redirecciona para ver un instrumento de evaluacion
{
	$.ajax({
		async: true,
		type: "get",
		dataType: "html",
		contentType: "aplication/x-www-form-urlencoded",
		url:"AjaxControlador",
		data: {ins_id : ins_id, rut_evaluador : rut, practica_id : pra_id, rol_id : rol_id },
		success: function(datos){
			$("#div_instrumento").empty();
			$("#div_instrumento").html(''+datos);
			$("#myModal").modal('show');
			
		}
	});
	return false;
}

function verDetalleEvaluacion(tipo, rut, pra_id, rol_id, eva_fecha)//redirecciona para ver el detalle de una evaluacion
{	//tipo: corresponde si es evaluacion de 'alumno', 'supervisor' o 'academico'
	//rut: rut del evaluador
	//pra_id: codigo de la práctica que fue evaluada
	//rol_id: rol con que el evaluador evaluó la practica, sirve para determinar con que rubrica evaluó
	//eva_fecha: sirve para determinar con que rubrica evaluó
	$.ajax({
		async: true,
		type: "get",
		dataType: "html",
		contentType: "aplication/x-www-form-urlencoded",
		url:"AjaxControlador",
		data: {tipo_eval : tipo, evaluador_rut : rut, id_practica : pra_id, id_rol : rol_id, eva_fecha : eva_fecha },
		success: function(datos){
			$("#div_instrumento").empty();
			$("#div_instrumento").html(''+datos);
			$("#myModal").modal('show');
			
		}
	});
	return false;
}

function verPruebaInstrumento(tipo, ins_id)//redirecciona para ver la rubrica (muestra)
{	//tipo: corresponde si es evaluacion de 'alumno', 'supervisor' o 'academico'
	//ins_id: corresponde al id del instrumento
	$.ajax({
		async: true,
		type: "get",
		dataType: "html",
		contentType: "aplication/x-www-form-urlencoded",
		url:"AjaxControlador",
		data: {tipo_evaluador : tipo, id_instrumento : ins_id },
		success: function(datos){
			$("#div_instrumento").empty();
			$("#div_instrumento").html(''+datos);
			$("#myModal").modal('show');
			
		}
	});
	return false;
}

function verEmpresa(emp_rut)//redirecciona para ver una Empresa
{
	$.ajax({
		async: true,
		type: "get",
		dataType: "html",
		contentType: "aplication/x-www-form-urlencoded",
		url:"AjaxControlador",
		data: {emp_rut : emp_rut},
		success: function(datos){
			$("#div_empresa").empty();
			$("#div_empresa").html(''+datos);
			$("#myModal").modal('show');
		}
	});
	return false;
}

function verAcademico(aca_rut)//redirecciona para ver una Empresa
{
	$.ajax({
		async: true,
		type: "get",
		dataType: "html",
		contentType: "aplication/x-www-form-urlencoded",
		url:"AjaxControlador",
		data: {aca_rut : aca_rut},
		success: function(datos){
			$("#div_academico").empty();
			$("#div_academico").html(''+datos);
			$("#myModal").modal('show');
		}
	});
	return false;
}

function verAdministrador(adm_rut)//redirecciona para ver un administrador
{
	$.ajax({
		async: true,
		type: "get",
		dataType: "html",
		contentType: "aplication/x-www-form-urlencoded",
		url:"AjaxControlador",
		data: {adm_rut : adm_rut},
		success: function(datos){
			$("#div_administrador").empty();
			$("#div_administrador").html(''+datos);
			$("#myModal").modal('show');
		}
	});
	return false;
}

function verSupervisor(sup_rut)//redirecciona para ver un supervisor
{
	$.ajax({
		async: true,
		type: "get",
		dataType: "html",
		contentType: "aplication/x-www-form-urlencoded",
		url:"AjaxControlador",
		data: {sup_rut : sup_rut},
		success: function(datos){
			$("#div_supervisor").empty();
			$("#div_supervisor").html(''+datos);
			$("#myModal").modal('show');
		}
	});
	return false;
}

function verHistorial(pra_id)//redirecciona para ver historial
{
	$.ajax({
		async: true,
		type: "get",
		dataType: "html",
		contentType: "aplication/x-www-form-urlencoded",
		url:"AjaxControlador",
		data: {pra_id : pra_id},
		success: function(datos){
			$("#div_historial").empty();
			$("#div_historial").html(''+datos);
			$("#myModal").modal('show');
		}
	});
	return false;
}

function verPractica(pra_id)//redirecciona para ver una practica
{
	$.ajax({
		async: true,
		type: "get",
		dataType: "html",
		contentType: "aplication/x-www-form-urlencoded",
		url:"practicaServlet.do",
		data: {pra_id : pra_id},
		success: function(datos){
			$("#div_practica").empty();
			$("#div_practica").html(''+datos);
			$("#myModal1").modal('show');
		}
	});
	return false;
}

function verPendiente(pra_id)//redirecciona para ver una practica pendiente
{
	$.ajax({
		async: true,
		type: "get",
		dataType: "html",
		contentType: "aplication/x-www-form-urlencoded",
		url:"pendienteServlet.do",
		data: {pra_id : pra_id},
		success: function(datos){
			$("#div_pendiente").empty();
			$("#div_pendiente").html(''+datos);
			$("#myModal2").modal('show');
		}
	});
	return false;
}

function verPlan(plan_id)//redirecciona para ver una practica
{
	$.ajax({
		async: true,
		type: "get",
		dataType: "html",
		contentType: "aplication/x-www-form-urlencoded",
		url:"AjaxControlador",
		data: {plan_id : plan_id},
		success: function(datos){
			$("#div_detalle").empty();
			$("#div_detalle").html(''+datos);
			$("#myModalDetalle").modal('show');
		}
	});
	return false;
}

function modificarAcademico(aca_rut)//redirecciona para modificar un acad�mico
{
	$.ajax({
		async: true,
		type: "get",
		dataType: "html",
		contentType: "aplication/x-www-form-urlencoded",
		url:"modificarAcademicoServlet.do",
		data: {aca_rut : aca_rut},
		success: function(datos){
			$("#div_mod_aca").empty();
			$("#div_mod_aca").html(''+datos);
			$("#myModal_modif").modal('show');
		}
	});
	return false;
}

function modificarAdministrador(adm_rut)//redirecciona para modificar un admin
{
	$.ajax({
		async: true,
		type: "get",
		dataType: "html",
		contentType: "aplication/x-www-form-urlencoded",
		url:"modificarAdministradorServlet.do",
		data: {adm_rut : adm_rut},
		success: function(datos){
			$("#div_mod_adm").empty();
			$("#div_mod_adm").html(''+datos);
			$("#myModal_modif").modal('show');
		}
	});
	return false;
}

function modificarEmpresa(emp_rut)//redirecciona para modificar una empresa
{
	$.ajax({
		async: true,
		type: "get",
		dataType: "html",
		contentType: "aplication/x-www-form-urlencoded",
		url:"modificarEmpresaServlet.do",
		data: {emp_rut : emp_rut},
		success: function(datos){
			$("#div_mod_emp").empty();
			$("#div_mod_emp").html(''+datos);
			$("#myModal_modif").modal('show');
		}
	});
	return false;
}

function modificarSupervisor(sup_rut)//redirecciona para modificar un supervisor
{
	$.ajax({
		async: true,
		type: "get",
		dataType: "html",
		contentType: "aplication/x-www-form-urlencoded",
		url:"modificarSupervisorServlet.do",
		data: {sup_rut : sup_rut},
		success: function(datos){
			$("#div_mod_sup").empty();
			$("#div_mod_sup").html(''+datos);
			$("#myModal_modif").modal('show');
		}
	});
	return false;
}

function modificarPlan(plan_id)//redirecciona para modificar un plan
{
	$.ajax({
		async: true,
		type: "get",
		dataType: "html",
		contentType: "aplication/x-www-form-urlencoded",
		url:"modificarPlanServlet.do",
		data: {plan_id : plan_id},
		success: function(datos){
			$("#div_mod_plan").empty();
			$("#div_mod_plan").html(''+datos);
			$("#myModal_modif").modal('show');
		}
	});
	return false;
}
function modificarEspecialidad(esp_id)//redirecciona para modificar una especialidad
{
	$.ajax({
		async: true,
		type: "get",
		dataType: "html",
		contentType: "aplication/x-www-form-urlencoded",
		url:"modificarEspecialidadServlet.do",
		data: {esp_id : esp_id},
		success: function(datos){
			$("#div_mod_esp").empty();
			$("#div_mod_esp").html(''+datos);
			$("#myModal_modif").modal('show');
		}
	});
	return false;
}
function modificarArea(area_id)//redirecciona para modificar una area
{
	$.ajax({
		async: true,
		type: "get",
		dataType: "html",
		contentType: "aplication/x-www-form-urlencoded",
		url:"modificarAreaServlet.do",
		data: {area_id : area_id},
		success: function(datos){
			$("#div_mod_area").empty();
			$("#div_mod_area").html(''+datos);
			$("#myModal_modif").modal('show');
		}
	});
	return false;
}
function modificarFuncion(funcion_id)//redirecciona para modificar una area
{
	$.ajax({
		async: true,
		type: "get",
		dataType: "html",
		contentType: "aplication/x-www-form-urlencoded",
		url:"modificarFuncionServlet.do",
		data: {funcion_id : funcion_id},
		success: function(datos){
			$("#div_mod_funcion").empty();
			$("#div_mod_funcion").html(''+datos);
			$("#myModal_modif").modal('show');
		}
	});
	return false;
}

$(document).ready(function(){	
	$("#region").change(seleccionRegion);
	$("#provincia").change(seleccionProvincia);
	$("#region_modif").change(seleccionRegionModif);
	$("#provincia_modif").change(seleccionProvinciaModif);
	$("#empresa").change(seleccionEmpresa);
	$("#giro_1").change(seleccionRubro1);
	$("#giro_2").change(seleccionRubro2);
	$("#giro_3").change(seleccionRubro3);
	for(var i=1;i <= 12; i++){
		$("input[name=item"+i+"]").change(calcularPuntaje);
		//$("input[name=item"+i+"]").change(mensajePuntaje);
	}
	
	function seleccionRegion(){
		var x;
		x = $("#region");
		$.ajax({
			async: true,
			type: "get",
			dataType: "html",
			contentType: "aplication/x-www-form-urlencoded",
			url:"AjaxControlador",
			data: {region : x.val()},
			success: function(datos){
				$("#div_provincia").empty();
				$("#div_provincia").html(''+datos);
				$("#provincia").change(seleccionProvincia);
			}
		});
		return false;
	}
	
	function seleccionProvincia(){//actualiza el select de comunas segun la provincia que se selecciona
		var x;
		x = $("#provincia");
		$.ajax({
			async: true,
			type: "get",
			dataType: "html",
			contentType: "aplication/x-www-form-urlencoded",
			url:"AjaxControlador",
			data: {provincia : x.val()},
			success: function(datos){				
				$("#div_comuna").empty();
				$("#div_comuna").html(''+datos);	
			}
		});
		return false;		
	}
	function seleccionRegionModif(){
		var x;
		x = $("#region_modif");
		$.ajax({
			async: true,
			type: "get",
			dataType: "html",
			contentType: "aplication/x-www-form-urlencoded",
			url:"AjaxControlador",
			data: {region_modif : x.val()},
			success: function(datos){
				$("#div_provincia_modif").empty();
				$("#div_provincia_modif").html(''+datos);
				$("#provincia_modif").change(seleccionProvinciaModif);
			}
		});
		return false;
	}
	
	function seleccionProvinciaModif(){//actualiza el select de comunas segun la provincia que se selecciona
		var x;
		x = $("#provincia_modif");
		$.ajax({
			async: true,
			type: "get",
			dataType: "html",
			contentType: "aplication/x-www-form-urlencoded",
			url:"AjaxControlador",
			data: {provincia_modif : x.val()},
			success: function(datos){				
				$("#div_comuna_modif").empty();
				$("#div_comuna_modif").html(''+datos);	
			}
		});
		return false;		
	}
	function seleccionEmpresa(){//actualiza el select de supervisores segun la empresa que se selecciona
		var x;
		x = $("#empresa");
		$.ajax({
			async: true,
			type: "get",
			dataType: "html",
			contentType: "aplication/x-www-form-urlencoded",
			url:"AjaxControlador",
			data: {empresa : x.val()},
			success: function(datos){				
				$("#div_supervisor").empty();
				$("#div_supervisor").html(''+datos);	
			}
		});
		return false;		
	}

	function seleccionRubro1(){//actualiza el select de rubro del giro seleccionado
		var x;
		x = $("#giro_1");
		$.ajax({
			async: true,
			type: "get",
			dataType: "html",
			contentType: "aplication/x-www-form-urlencoded",
			url:"AjaxControlador",
			data: {giro : x.val(), num_giro : 1},
			success: function(datos){				
				$("#div_giro_1").empty();
				$("#div_giro_1").html(''+datos);
				$("#giro_1").change(seleccionProvincia);
			}
		});
		return false;		
	}
	function seleccionRubro2(){//actualiza el select de rubro del giro seleccionado
		var x;
		x = $("#giro_2");
		$.ajax({
			async: true,
			type: "get",
			dataType: "html",
			contentType: "aplication/x-www-form-urlencoded",
			url:"AjaxControlador",
			data: {giro : x.val(), num_giro : 2},
			success: function(datos){				
				$("#div_giro_2").empty();
				$("#div_giro_2").html(''+datos);
				("#giro_2").change(seleccionProvincia);
			}
		});
		return false;		
	}
	function seleccionRubro3(){//actualiza el select de rubro del giro seleccionado
		var x;
		x = $("#giro_3");
		$.ajax({
			async: true,
			type: "get",
			dataType: "html",
			contentType: "aplication/x-www-form-urlencoded",
			url:"AjaxControlador",
			data: {giro : x.val(), num_giro : 3},
			success: function(datos){				
				$("#div_giro_3").empty();
				$("#div_giro_3").html(''+datos);
				("#giro_3").change(seleccionProvincia);
			}
		});
		return false;		
	}

	function mensajePuntaje(){
		alert("El valor de "+$(this)+":"+$(this).val());
	}
	
	function calcularPuntaje(){		
		var suma = 0;		
		for(var i=1;i <= 12; i++){
			 if( $("input:radio[name=item"+i+"]").is(':checked')) { 
				 suma = suma + parseInt($('input:radio[name=item'+i+']:checked').val());
				 //alert($("input[name='item"+i+"']").val());
				 //alert($('input:radio[name=item'+i+']:checked').val());
			 }			
			
		}
		//alert(suma);
		var nota = 6/36;
		
		$("input:text[name=puntaje]").val(suma);
		if(suma == 0){
			$("input:text[name=nota]").val(1);
		}
		else{
			$("input:text[name=nota]").val(((nota * suma) + 1).toFixed(2));
		}
	}
});

function asignarComision(pra_id, asi_codigo, plan_id)//redirecciona para mostrar un formulario segun el tipo de gestion
{
	$.ajax({
		async: true,
		type: "get",
		dataType: "html",
		contentType: "aplication/x-www-form-urlencoded",
		url:"AjaxControlador",
		data: {practica : pra_id, asignatura : asi_codigo, plan:plan_id},
		success: function(datos){
			$("#div_asignar").empty();
			$("#div_asignar").html(''+datos);
			$("#myModal").modal('show');
			
		}
	});
	return false;
}

function agregarAcademico()//agrega un select de academicos para la comision evaluadora de la gestión profesional
{
	if($("#actual_academicos").val() == $("#num_academicos").val()){
		alert("No puede agregar mas Academicos");
	}
	else{
		for(var i = 0; i < $("#num_academicos").val(); i++){
			if($("#actual_academicos").val() == i){
				//$("#asignatura_"+(i+1)).css("display", "block");				
				$("#actual_academicos").val(i+1);
				//alert("Agregando Asignaturas\nActual Asignaturas "+$("#actual_academicos").val());
				
				var n_academico = $("#actual_academicos").val();
				$.ajax({
					async: true,
					type: "get",
					dataType: "html",
					contentType: "aplication/x-www-form-urlencoded",
					url:"AjaxControlador",
					data: {n_academico : n_academico},
					success: function(datos){
						$("#div_academico_"+(i+1)).html(''+datos);
						//$("#hola").html(''+datos);
					}
				});
				break;
			}
			
		}
	}
}



function quitarAcademico()//agrega un select de academicos para la comision evaluadora de la gestión profesional
{
	if($("#actual_academicos").val() == 0){
		//alert("No puede quitar mas");		
	}
	else{
		for(var i = 1; i <= $("#num_academicos").val(); i++){
			if($("#actual_academicos").val() == i){				
				//$("#asignatura_"+i).css("display", "none");
				//$("#asignatura_"+i).val("000000");
				$("#actual_academicos").val(i-1);
				if(i != $("#num_academicos").val()){
					$("#actual_academicos").val(i-1);
				}
				//alert("Quitando Asignaturas\nActual Asignaturas "+$("#actual_academicos").val());
				$("#div_academico_"+(i)).empty();
				break;
			}
			
		}
	}
}


function funcionMaestra(e,eleccion,numero) 
{
	//el numero 1 es para cuando se necesita que no se ingresen puntos y otros caracteres
	if(numero == '1')
	{
		bool1 = especial_character(e);
	}
	//el numero 2 es para cuando se necesita que no se ingresen  otros caracteres, pero si puntos
	if(numero == '2')
	{
		bool1 = especial_character2(e);
	}
	if(bool1 == false)
	{
		return bool1;
	}
	bool2 = solo_letras(e,eleccion);
	return bool2;
}
	
	// evita insercion de algunos caracteres
function especial_character(e)
{
    var key = e.charCode ? e.charCode : e.keyCode;
    if (key == 46 || key == 145 || key == 146 || key == 39 || key == 37 || key == 33 || key == 161)
    {
        return false;
    }    
}
//evita insercion de caracteres, pero si deja insertar puntos
function especial_character2(e)
{
    var key = e.charCode ? e.charCode : e.keyCode;
    if (key == 145 || key == 146 || key == 39 || key == 37 || key == 33 || key == 161)
    {
        return false;
    }    
}

function solo_letras(elEvento,eleccion) 
{
	  // Variables que definen los caracteres permitidos
	  var permitidos = eleccion;
	  var numeros = "0123456789";
	  //var caracteres = "";
	  //
	  var caracteres = " abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZáéíóúÁÉÍÓÚ#/.@:";
	  var email = " abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ@_-";
	  var nombres = " abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZáéíóúÁÉÍÓÚ";
	  var direccion = " abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZáéíóúÁÉÍÓÚ#-.";
	  var descripcion = " abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZáéíóúÁÉÍÓÚ-,";
	  var web = " abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZáéíóúÁÉÍÓÚ:/_-.";
	  var tele_fijo = "-";
	  var celular = "*+#";
	  var rut = "kK";
	  var numeros_caracteres = numeros + caracteres;
	  var email_num = numeros + email;
	  var teclas_especiales = [8, 9, 37, 39, 46];
	  var clave = "abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ";
	  // 9 = Tab, 8 = BackSpace, 46 = Supr, 37 = flecha izquierda, 39 = flecha derecha			 
	 
	  // Seleccionar los caracteres a partir del parámetro de la función
	  switch(permitidos) {
		 case 'num':
			permitidos = numeros;
			break;
		 case 'car':
			permitidos = caracteres;
			break;
		 case 'num_car':
			permitidos = numeros_caracteres;
			break;
		 case 'nombres':
			permitidos = nombres;
			break;
		 case 'email':
			permitidos = email_num;
			break;
		 case 'dire':
			permitidos = direccion+ numeros;
			break;
		 case 'descri':
			permitidos = descripcion+ numeros;
			break;
		 case 'fijo':
			permitidos = tele_fijo+ numeros;
			break;
		 case 'celular':
			permitidos = celular+ numeros;
			break;
		 case 'rut':
				permitidos = rut + numeros;
				break;
		 case 'web':
			permitidos = web + numeros;
			break;
		 case 'clave':
				permitidos = clave + numeros;
				break;
	  }
		 
	  // Obtener la tecla pulsada 
	  var evento = elEvento || window.event;
	  var codigoCaracter = evento.charCode || evento.keyCode;
	  var caracter = String.fromCharCode(codigoCaracter);
	 
	  // Comprobar si la tecla pulsada es alguna de las teclas especiales
	  // (teclas de borrado y flechas horizontales)
	  var tecla_especial = false;
	  for(var i in teclas_especiales) {
		 if(codigoCaracter == teclas_especiales[i]) {
			tecla_especial = true;
			break;
		 }
	  }
	 
	  // Comprobar si la tecla pulsada se encuentra en los caracteres permitidos
	  // o si es una tecla especial
	  return permitidos.indexOf(caracter) != -1 || tecla_especial;
		/* Sólo números agregar / alfinal del input
		<input type="text" id="texto" onkeypress="return permite(event, 'num')" >

		// Sólo letras
		<input type="text" id="texto" onkeypress="return permite(event, 'car')" >

		// Sólo letras o números
		<input type="text" id="texto" onkeypress="return permite(event, 'num_car')" >*/
}

function permite(elEvento) {
	  // Variables que definen los caracteres permitidos
	  var permitidos = "num_car";
	  var numeros = "0123456789";
	  //var caracteres = "";
	  var caracteres = " abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ#/.@:";
	  var numeros_caracteres = numeros + caracteres;
	  var teclas_especiales = [8, 9, 37, 39, 46];
	  // 9 = Tab, 8 = BackSpace, 46 = Supr, 37 = flecha izquierda, 39 = flecha derecha			 
	 
	  // Seleccionar los caracteres a partir del parámetro de la función
	  switch(permitidos) {
		 case 'num':
			permitidos = numeros;
			break;
		 case 'car':
			permitidos = caracteres;
			break;
		 case 'num_car':
			permitidos = numeros_caracteres;
			break;
	  }
	 
	  // Obtener la tecla pulsada 
	  var evento = elEvento || window.event;
	  var codigoCaracter = evento.charCode || evento.keyCode;
	  var caracter = String.fromCharCode(codigoCaracter);
	 
	  // Comprobar si la tecla pulsada es alguna de las teclas especiales
	  // (teclas de borrado y flechas horizontales)
	  var tecla_especial = false;
	  for(var i in teclas_especiales) {
		 if(codigoCaracter == teclas_especiales[i]) {
			tecla_especial = true;
			break;
		 }
	  }
	 
	  // Comprobar si la tecla pulsada se encuentra en los caracteres permitidos
	  // o si es una tecla especial
	  return permitidos.indexOf(caracter) != -1 || tecla_especial;
		/* Sólo números agregar / alfinal del input
		<input type="text" id="texto" onkeypress="return permite(event, 'num')" >

		// Sólo letras
		<input type="text" id="texto" onkeypress="return permite(event, 'car')" >

		// Sólo letras o números
		<input type="text" id="texto" onkeypress="return permite(event, 'num_car')" >*/
}

function validar_comision(){//valida el formulario de asignacion de comision
	var valido = true;
	$(".help-block").remove();
	$(".alert.alert-danger.alert-dismissible").remove();
	
	
	if(!$('#comision').val() && $("#formulario").val() == "profesional"){//si la comisón está vacia
		alert("La comision no puede estar vacia");
		$("#ms-comision").parent().append(
				'<div class="alert alert-danger alert-dismissible" role="alert">'
				  +'<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'
				  +'<strong>Error!</strong> La comision no puede estar vacia.'
				+'</div>'
		  );
		valido = false;
	}
	else{
		if($("#comision").val().length >= 2){//
			/*$("#comision").removeAttr("data-toggle");
			$("#comision").removeAttr("data-original-title");
			$("#comision").tooltip('destroy');*/
		}
		else{
			if(valido == true){
				$("#comision").focus();
				
			}
			
			/*$("#comision").attr("data-toggle","tooltip");
			$("#comision").attr("data-original-title","Debe seleccionar academicos para la comision");
			$("#comision").tooltip({
		        placement : 'bottom',
		        //placement : 'right',
		        trigger: 'focus'
		    });
			$('#comision').tooltip('show');*/
			//$("#ms-comision").append('<span class="help-block">Debe Seleccionar academicos para la comision.</span>');
			/*$("#ms-comision").append(
					'<div class="alert alert-warning alert-dismissible" role="alert">'
					  +'<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'
					  +'<strong>Warning!</strong> Better check yourself, you are not looking too good.'
					+'</div>'
			  );*/
			$("#ms-comision").parent().append(
					'<div class="alert alert-danger alert-dismissible" role="alert">'
					  +'<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'
					  +'<strong>Error!</strong> Debe seleccionar los academicos para la comision.'
					+'</div>'
			  );
			
			valido = false;
		}
	}
	
		
	
	if(valido == false){//si existe algun campo invalido
		return valido;
	}
	else{
		return true;
	}
}

function buscarUsuario()//busca un usuario
{
	
	$.ajax({
		async: true,
		type: "get",
		dataType: "html",
		contentType: "aplication/x-www-form-urlencoded",
		url:"AjaxControlador",
		data: {rut_usuario : $("#rut").val(), tipo_usuario : $("#usuario").val()},
		success: function(datos){				
			$("#div_usuario").empty();
			$("#div_usuario").html(''+datos);
		}
	});
	return false;
}

function buscarInforme()//busca un Informe
{
	
	$.ajax({
		async: true,
		type: "get",
		dataType: "html",
		contentType: "aplication/x-www-form-urlencoded",
		url:"AjaxControlador",
		data: {tipo_informe : $("#informe").val()},
		success: function(datos){				
			$("#div_usuario").empty();
			$("#div_usuario").html(''+datos);
		}
	});
	return false;
}
function validacion_area(e,opcion)
{
	var valido = true;
	$(".help-block").remove();
	$(".alert.alert-danger.alert-dismissible").remove();
	$(".glyphicon.glyphicon-remove.form-control-feedback").remove();
	if(opcion == 1)
	{
		if(!$('#nombre').val())
		{
			$("#nombre").focus();
			$("#nombre").attr("data-toggle","tooltip");
			$("#nombre").attr("data-original-title","El campo no puede est\xe1r vac\xedo");
			$("#nombre").tooltip({
		        placement : 'bottom',
		        trigger: 'focus'
		    });
			$("#nombre").parent().addClass("has-error has-feedback");
			$('#nombre').tooltip('show');
			//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
			valido = false;
			bandera = false;
		}
		
		if(valido == false){//si existe algun campo invalido		
			return false;
		}
		else{

			$("#nombre").parent().removeClass('has-error');
			$("#nombre").parent().addClass("has-success");
			e.preventDefault();
			rotacion_area(e,opcion);
		}
	}
	if(opcion == 2)
	{
		if(!$('#nombre_m').val())
		{
			$("#nombre_m").focus();
			$("#nombre_m").attr("data-toggle","tooltip");
			$("#nombre_m").attr("data-original-title","El campo no puede est\xe1r vac\xedo");
			$("#nombre_m").tooltip({
		        placement : 'bottom',
		        trigger: 'focus'
		    });
			$("#nombre_m").parent().addClass("has-error has-feedback");
			$('#nombre_m').tooltip('show');
			//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
			valido = false;
			bandera = false;
		}
		
		if(valido == false){//si existe algun campo invalido		
			return false;
		}
		else{

			$("#nombre_m").parent().removeClass('has-error');
			$("#nombre_m").parent().addClass("has-success");
			e.preventDefault();
			rotacion_area(e,opcion);
		}
	}
}

function validacion_funcion(e,opcion)
{
	var valido = true;
	$(".help-block").remove();
	$(".alert.alert-danger.alert-dismissible").remove();
	$(".glyphicon.glyphicon-remove.form-control-feedback").remove();
	if(opcion == 1)
	{
		if(!$('#nombre').val())
		{
			$("#nombre").focus();
			$("#nombre").attr("data-toggle","tooltip");
			$("#nombre").attr("data-original-title","El campo no puede est\xe1r vac\xedo");
			$("#nombre").tooltip({
		        placement : 'bottom',
		        trigger: 'focus'
		    });
			$("#nombre").parent().addClass("has-error has-feedback");
			$('#nombre').tooltip('show');
			//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
			valido = false;
			bandera = false;
		}
		
		if(valido == false){//si existe algun campo invalido		
			return false;
		}
		else{

			$("#nombre").parent().removeClass('has-error');
			$("#nombre").parent().addClass("has-success");
			e.preventDefault();
			rotacion_funcion(e,opcion);
		}
	}
	if(opcion == 2)
	{
		if(!$('#nombre_m').val())
		{
			$("#nombre_m").focus();
			$("#nombre_m").attr("data-toggle","tooltip");
			$("#nombre_m").attr("data-original-title","El campo no puede est\xe1r vac\xedo");
			$("#nombre_m").tooltip({
		        placement : 'bottom',
		        trigger: 'focus'
		    });
			$("#nombre_m").parent().addClass("has-error has-feedback");
			$('#nombre_m').tooltip('show');
			//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
			valido = false;
			bandera = false;
		}
		
		if(valido == false){//si existe algun campo invalido		
			return false;
		}
		else{

			$("#nombre_m").parent().removeClass('has-error');
			$("#nombre_m").parent().addClass("has-success");
			e.preventDefault();
			rotacion_funcion(e,opcion);
		}
	}
}
function validacion_cambio_clave(e,opcion)
{
	var valido = true;
	var bandera = true;
	$(".help-block").remove();
	$(".alert.alert-danger.alert-dismissible").remove();
	$(".glyphicon.glyphicon-remove.form-control-feedback").remove();
	if(!$('#actual').val())
	{
		$("#actual").focus();
		$("#actual").attr("data-toggle","tooltip");
		$("#actual").attr("data-original-title","El campo no puede est\xe1r vac\xedo");
		$("#actual").tooltip({
	        placement : 'bottom',
	        trigger: 'focus'
	    });
		$("#actual").parent().addClass("has-error has-feedback");
		$('#actual').tooltip('show');
		//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
		valido = false;
		bandera = false;
	}
	if(bandera == true)
	{
		$("#actual").parent().removeClass('has-error');
		$("#actual").parent().addClass("has-success");
		if(!$('#nueva').val())
		{
			$("#nueva").focus();
			$("#nueva").attr("data-toggle","tooltip");
			$("#nueva").attr("data-original-title","El campo no puede est\xe1r vac\xedo");
			$("#nueva").tooltip({
		        placement : 'bottom',
		        trigger: 'focus'
		    });
			$("#nueva").parent().addClass("has-error has-feedback");
			$('#nueva').tooltip('show');
			//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
			valido = false;
			bandera = false;
		}else{
			if($('#nueva').val().length < 9)
			{
				$("#nueva").focus();
				$("#nueva").attr("data-toggle","tooltip");
				$("#nueva").attr("data-original-title","La clave de acceso debe contener a lo menos 9 caracteres");
				$("#nueva").tooltip({
			        placement : 'bottom',
			        trigger: 'focus'
			    });
				$("#nueva").parent().addClass("has-error has-feedback");
				$('#nueva').tooltip('show');
				//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
				valido = false;
				bandera = false;
			}
		}
	}
	if(bandera == true)
	{
		$("#nueva").parent().removeClass('has-error');
		$("#nueva").parent().addClass("has-success");
		if(!$('#confirmacion').val())
		{
			$("#confirmacion").focus();
			$("#confirmacion").attr("data-toggle","tooltip");
			$("#confirmacion").attr("data-original-title","El campo no puede est\xe1r vac\xedo");
			$("#confirmacion").tooltip({
		        placement : 'bottom',
		        trigger: 'focus'
		    });
			$("#confirmacion").parent().addClass("has-error has-feedback");
			$('#confirmacion').tooltip('show');
			//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
			valido = false;
			bandera = false;
		}else{
			if($('#confirmacion').val().length < 9)
			{
				$("#confirmacion").focus();
				$("#confirmacion").attr("data-toggle","tooltip");
				$("#confirmacion").attr("data-original-title","La clave de acceso debe contener a lo menos 9 caracteres");
				$("#confirmacion").tooltip({
			        placement : 'bottom',
			        trigger: 'focus'
			    });
				$("#confirmacion").parent().addClass("has-error has-feedback");
				$('#confirmacion').tooltip('show');
				valido = false;
				bandera = false;
			}else
			{
				if($('#confirmacion').val() != $('#nueva').val())
				{
					$("#confirmacion").focus();
					$("#confirmacion").attr("data-toggle","tooltip");
					$("#confirmacion").attr("data-original-title","La nueva clave de acceso no es igual en los dos campos");
					$("#confirmacion").tooltip({
				        placement : 'bottom',
				        trigger: 'focus'
				    });
					$("#confirmacion").parent().addClass("has-error has-feedback");
					$("#nueva").parent().removeClass('has-success');
					$("#nueva").parent().addClass("has-error has-feedback");
					$('#confirmacion').tooltip('show');
					valido = false;
					bandera = false;
				}

			}
		}
	}
	if(valido == false){//si existe algun campo invalido		
		return false;
	}
	else{

		$("#confirmacion").parent().removeClass('has-error');
		$("#confirmacion").parent().addClass("has-success");
		e.preventDefault();
		rotacion_clave(e,opcion);
	}
}

function validar_academico(e,opcion){//valida el formulario de asignacion de comision
	var valido = true;
	var regularCelular = "[\+][5][6][9][4-9][0-9]{7}";//----  /^\+\d{2,3}\s\d{9}$/   ---  /^/\+/\d{3}[5-9][0-9]{7}$/ --- /^\+\d{11}$/
	var regularTelefono = "[\+][5][6][-][0-9]{1,2}[-][2-3][1-9][0-9]{5,6}";
	var bandera = true;
	$(".help-block").remove();
	$(".alert.alert-danger.alert-dismissible").remove();
	$(".glyphicon.glyphicon-remove.form-control-feedback").remove();

	if(opcion==1)
	{
		if(!$('#rut').val())
		{
			$("#rut").focus();
			$("#rut").attr("data-toggle","tooltip");
			$("#rut").attr("data-original-title","El rut no puede ser nulo");
			$("#rut").tooltip({
		        placement : 'bottom',
		        trigger: 'focus'
		    });
			$("#rut").parent().addClass("has-error has-feedback");
			$('#rut').tooltip('show');
			//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
			valido = false;
			bandera = false;
		}
		if(bandera == true)
		{
			$("#rut").parent().removeClass('has-error');
			$("#rut").parent().addClass("has-success");
			if(!$('#departamento').val())
			{
				$("#departamento").focus();
				$("#departamento").attr("data-toggle","tooltip");
				$("#departamento").attr("data-original-title","Debe seleccionar uno de los valores");
				$("#departamento").tooltip({
			        placement : 'bottom',
			        trigger: 'focus'
			    });
				$("#departamento").parent().addClass("has-error has-feedback");
				$('#departamento').tooltip('show');
				//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
				valido = false;
				bandera = false;
			}
		}
		if(bandera == true)
		{
			$("#departamento").parent().removeClass('has-error');
			$("#departamento").parent().addClass("has-success");
			if(!$('#nombre').val())
			{
				$("#nombre").focus();
				$("#nombre").attr("data-toggle","tooltip");
				$("#nombre").attr("data-original-title","El campo no puede est\xe1r Vac\xedo");
				$("#nombre").tooltip({
			        placement : 'bottom',
			        trigger: 'focus'
			    });
				$("#nombre").parent().addClass("has-error has-feedback");
				$('#nombre').tooltip('show');
				//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
				valido = false;
				bandera = false;
			}
		}
		if(bandera == true)
		{
			$("#nombre").parent().removeClass('has-error');
			$("#nombre").parent().addClass("has-success");
			if(!$('#paterno').val())
			{
				$("#paterno").focus();
				$("#paterno").attr("data-toggle","tooltip");
				$("#paterno").attr("data-original-title","El campo no puede est\xe1r Vac\xedo");
				$("#paterno").tooltip({
			        placement : 'bottom',
			        trigger: 'focus'
			    });
				$("#paterno").parent().addClass("has-error has-feedback");
				$('#paterno').tooltip('show');
				//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
				valido = false;
				bandera = false;
			}
		}
		if(bandera == true)
		{
			$("#paterno").parent().removeClass('has-error');
			$("#paterno").parent().addClass("has-success");
			if(!$('#materno').val())
			{
				$("#materno").focus();
				$("#materno").attr("data-toggle","tooltip");
				$("#materno").attr("data-original-title","El campo no puede est\xe1r Vac\xedo");
				$("#materno").tooltip({
			        placement : 'bottom',
			        trigger: 'focus'
			    });
				$("#materno").parent().addClass("has-error has-feedback");
				$('#materno').tooltip('show');
				//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
				valido = false;
				bandera = false;
			}
		}
		if(bandera == true)
		{
			$("#materno").parent().removeClass('has-error');
			$("#materno").parent().addClass("has-success");
			if(!$('#email').val())
			{
				$("#email").focus();
				$("#email").attr("data-toggle","tooltip");
				$("#email").attr("data-original-title","El campo no puede est\xe1r Vac\xedo");
				$("#email").tooltip({
			        placement : 'bottom',
			        trigger: 'focus'
			    });
				$("#email").parent().addClass("has-error has-feedback");
				$('#email').tooltip('show');
				//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
				valido = false;
				bandera = false;
			}
		}
		if(bandera == true)
		{
			$("#email").parent().removeClass('has-error');
			$("#email").parent().addClass("has-success");
			if($("#telefono").val().length == "" && $("#codigo").val() == "")
			{//el telefono fijo es valido si está Vac\xedo
				$("#telefono").removeAttr("data-toggle");
				$("#telefono").removeAttr("data-original-title");
				$("#telefono").tooltip('destroy');
				$("#telefono").parent().parent().parent().removeClass("has-error has-feedback");
			}
			else
			{
				if($("#telefono").val().match(regularTelefono)){//
					$("#telefono").removeAttr("data-toggle");
					$("#telefono").removeAttr("data-original-title");
					$("#telefono").tooltip('destroy');
					$("#telefono").parent().parent().parent().removeClass("has-error has-feedback");
				}
				else
				{
					if(valido == true){
						$("#codigo").focus();
						$("#codigo").val("");
					}		
					$("#telefono").attr("data-toggle","tooltip");
					$("#telefono").attr("data-original-title","El numero "+$("#telefono").val()+" no corresponde a un telefono valido. Seleccione el codigo de area, luego digite su numero telefono de 7 digitos(8 digitos R.M.)");
					$("#telefono").tooltip({
				        placement : 'bottom',
				        trigger: 'focus'
				    });
					//$('#telefono').tooltip('show');
					$("#telefono").parent().parent().parent().addClass("has-error has-feedback");
					//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
					bandera = false;
					valido = false;
				}		
			}
		}
		if(bandera == true)
		{
			$("#telefono").parent().removeClass('has-error');
			$("#telefono").parent().addClass("has-success");
			$("#codigo").parent().removeClass('has-error');
			$("#codigo").parent().addClass("has-success");
			if($("#celular").val().length == "" ){//el campo celular es valido si está Vac\xedo
				$("#celular").removeAttr("data-toggle");
				$("#celular").removeAttr("data-original-title");
				$("#celular").parent().parent().parent().removeClass("has-error has-feedback");
			}
			else
			{
				if($("#celular").val().match(regularCelular)){//validación del campo de celular(expresion regular)
					$("#celular").removeAttr("data-toggle");
					$("#celular").removeAttr("data-original-title");
					$("#celular").tooltip('destroy');
					$("#celular").parent().parent().parent().removeClass("has-error has-feedback");
				}
				else
				{
					if(valido == true)
					{
						$("#celular").focus();
					}		
					$("#celular").attr("data-toggle","tooltip");
					$("#celular").attr("data-original-title","El numero "+$("#celular").val()+" no corresponde a un movil valido. Ingrese los 8 digitos de su numero movil");
					$("#celular").tooltip({
				        placement : 'bottom',
				        trigger: 'focus'
				    });
					$("#celular").val("");
					$('#celular').tooltip('show');
					$("#celular").parent().parent().parent().addClass("has-error has-feedback");
					//$("#celular").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
					bandera = false;
					valido = false;
				}
			}		
		}
		if(bandera == true)
		{
			if(!$("#especialidad").val()){//si la especialidad es vacia (ninguna seleccionada)
				alert('Debe elegir a los menos una especialidad');
				valido = false;
				bandera = false;
			}
		}
		
	}
	if(opcion==2)
	{
		if(!$('#rut_modif').val())
		{
			$("#rut_modif").focus();
			$("#rut_modif").attr("data-toggle","tooltip");
			$("#rut_modif").attr("data-original-title","El rut no puede ser nulo");
			$("#rut_modif").tooltip({
		        placement : 'bottom',
		        trigger: 'focus'
		    });
			$("#rut_modif").parent().addClass("has-error has-feedback");
			$('#rut_modif').tooltip('show');
			//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
			valido = false;
			bandera = false;
		}
		if(bandera == true)
		{
			$("#rut_modif").parent().removeClass('has-error');
			$("#rut_modif").parent().addClass("has-success");
			if(!$('#departamento_modif').val())
			{
				$("#departamento_modif").focus();
				$("#departamento_modif").attr("data-toggle","tooltip");
				$("#departamento_modif").attr("data-original-title","Debe seleccionar uno de los valores");
				$("#departamento_modif").tooltip({
			        placement : 'bottom',
			        trigger: 'focus'
			    });
				$("#departamento_modif").parent().addClass("has-error has-feedback");
				$('#departamento_modif').tooltip('show');
				//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
				valido = false;
				bandera = false;
			}
		}
		if(bandera == true)
		{
			$("#departamento_modif").parent().removeClass('has-error');
			$("#departamento_modif").parent().addClass("has-success");
			if(!$('#nombre_modif').val())
			{
				$("#nombre_modif").focus();
				$("#nombre_modif").attr("data-toggle","tooltip");
				$("#nombre_modif").attr("data-original-title","El campo no puede est\xe1r Vac\xedo");
				$("#nombre_modif").tooltip({
			        placement : 'bottom',
			        trigger: 'focus'
			    });
				$("#nombre_modif").parent().addClass("has-error has-feedback");
				$('#nombre_modif').tooltip('show');
				//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
				valido = false;
				bandera = false;
			}
		}
		if(bandera == true)
		{
			$("#nombre_modif").parent().removeClass('has-error');
			$("#nombre_modif").parent().addClass("has-success");
			if(!$('#paterno_modif').val())
			{
				$("#paterno_modif").focus();
				$("#paterno_modif").attr("data-toggle","tooltip");
				$("#paterno_modif").attr("data-original-title","El campo no puede est\xe1r Vac\xedo");
				$("#paterno_modif").tooltip({
			        placement : 'bottom',
			        trigger: 'focus'
			    });
				$("#paterno_modif").parent().addClass("has-error has-feedback");
				$('#paterno_modif').tooltip('show');
				//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
				valido = false;
				bandera = false;
			}
		}
		if(bandera == true)
		{
			$("#paterno_modif").parent().removeClass('has-error');
			$("#paterno_modif").parent().addClass("has-success");
			if(!$('#materno_modif').val())
			{
				$("#materno_modif").focus();
				$("#materno_modif").attr("data-toggle","tooltip");
				$("#materno_modif").attr("data-original-title","El campo no puede est\xe1r Vac\xedo");
				$("#materno_modif").tooltip({
			        placement : 'bottom',
			        trigger: 'focus'
			    });
				$("#materno_modif").parent().addClass("has-error has-feedback");
				$('#materno_modif').tooltip('show');
				//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
				valido = false;
				bandera = false;
			}
		}
		if(bandera == true)
		{
			$("#materno_modif").parent().removeClass('has-error');
			$("#materno_modif").parent().addClass("has-success");
			if(!$('#email_modif').val())
			{
				$("#email_modif").focus();
				$("#email_modif").attr("data-toggle","tooltip");
				$("#email_modif").attr("data-original-title","El campo no puede est\xe1r Vac\xedo");
				$("#email_modif").tooltip({
			        placement : 'bottom',
			        trigger: 'focus'
			    });
				$("#email_modif").parent().addClass("has-error has-feedback");
				$('#email_modif').tooltip('show');
				//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
				valido = false;
				bandera = false;
			}
		}
		if(bandera == true)
		{
			$("#email_modif").parent().removeClass('has-error');
			$("#email_modif").parent().addClass("has-success");
			if($("#telefono_modif").val().length == "" && $("#codigo_modif").val() == "")
			{//el telefono fijo es valido si está Vac\xedo
				$("#telefono_modif").removeAttr("data-toggle");
				$("#telefono_modif").removeAttr("data-original-title");
				$("#telefono_modif").tooltip('destroy');
				$("#telefono_modif").parent().parent().parent().removeClass("has-error has-feedback");
			}
			else
			{
				if($("#telefono_modif").val().match(regularTelefono)){//
					$("#telefono_modif").removeAttr("data-toggle");
					$("#telefono_modif").removeAttr("data-original-title");
					$("#telefono_modif").tooltip('destroy');
					$("#telefono_modif").parent().parent().parent().removeClass("has-error has-feedback");
				}
				else
				{
					if(valido == true){
						$("#codigo_modif").focus();
						$("#codigo_modif").val("");
					}		
					$("#telefono_modif").attr("data-toggle","tooltip");
					$("#telefono_modif").attr("data-original-title","El numero "+$("#telefono_modif").val()+" no corresponde a un telefono valido. Seleccione el codigo de area, luego digite su numero telefono de 7 digitos(8 digitos R.M.)");
					$("#telefono_modif").tooltip({
				        placement : 'bottom',
				        trigger: 'focus'
				    });
					//$('#telefono').tooltip('show');
					$("#telefono_modif").parent().parent().parent().addClass("has-error has-feedback");
					//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
					bandera = false;
					valido = false;
				}		
			}
		}
		if(bandera == true)
		{
			$("#telefono_modif").parent().removeClass('has-error');
			$("#telefono_modif").parent().addClass("has-success");
			$("#codigo_modif").parent().removeClass('has-error');
			$("#codigo_modif").parent().addClass("has-success");
			if($("#celular_modif").val().length == "" ){//el campo celular es valido si está Vac\xedo
				$("#celular_modif").removeAttr("data-toggle");
				$("#celular_modif").removeAttr("data-original-title");
				$("#celular_modif").parent().parent().parent().removeClass("has-error has-feedback");
			}
			else
			{
				if($("#celular_modif").val().match(regularCelular)){//validación del campo de celular(expresion regular)
					$("#celular_modif").removeAttr("data-toggle");
					$("#celular_modif").removeAttr("data-original-title");
					$("#celular_modif").tooltip('destroy');
					$("#celular_modif").parent().parent().parent().removeClass("has-error has-feedback");
				}
				else
				{
					if(valido == true)
					{
						$("#celular_modif").focus();
					}		
					$("#celular_modif").attr("data-toggle","tooltip");
					$("#celular_modif").attr("data-original-title","El numero "+$("#celular_modif").val()+" no corresponde a un movil valido. Ingrese los 8 digitos de su numero movil");
					$("#celular_modif").tooltip({
				        placement : 'bottom',
				        trigger: 'focus'
				    });
					$("#celular_modif").val("");
					$('#celular_modif').tooltip('show');
					$("#celular_modif").parent().parent().parent().addClass("has-error has-feedback");
					//$("#celular").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
					bandera = false;
					valido = false;
				}
			}		
		}
	}
	if(valido == false){//si existe algun campo invalido		
		return false;
	}
	else{
		if(opcion==1)
		{
			$("#celular").parent().removeClass('has-error');
			$("#celular").parent().addClass("has-success");
			$("#estado").parent().removeClass('has-error');
			$("#estado").parent().addClass("has-success");
		}
		if(opcion==2)
		{
			$("#celular_modif").parent().removeClass('has-error');
			$("#celular_modif").parent().addClass("has-success");
			$("#estado_modif").parent().removeClass('has-error');
			$("#estado_modif").parent().addClass("has-success");
		}	
		
		e.preventDefault();
		rotacion_aca(e,opcion);
	}
}

function validar_administrador(e,opcion){//valida el formulario crecion/modificacion admin
	var valido = true;
	var bandera = true;
	var regularCelular = "[\+][5][6][9][4-9][0-9]{7}";//----  /^\+\d{2,3}\s\d{9}$/   ---  /^/\+/\d{3}[5-9][0-9]{7}$/ --- /^\+\d{11}$/
	var regularTelefono = "[\+][5][6][-][0-9]{1,2}[-][2-3][1-9][0-9]{5,6}";
	$(".help-block").remove();
	$(".alert.alert-danger.alert-dismissible").remove();
	$(".glyphicon.glyphicon-remove.form-control-feedback").remove();
	if(opcion==1)
	{
		if(!$('#rut').val())
		{
			$("#rut").focus();
			$("#rut").attr("data-toggle","tooltip");
			$("#rut").attr("data-original-title","El rut no puede ser nulo");
			$("#rut").tooltip({
		        placement : 'bottom',
		        trigger: 'focus'
		    });
			$("#rut").parent().addClass("has-error has-feedback");
			$('#rut').tooltip('show');
			//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
			valido = false;
			bandera = false;
		}
		
		if(bandera == true)
		{
			$("#rut").parent().removeClass('has-error');
			$("#rut").parent().addClass("has-success");
			if(!$('#nombre').val())
			{
				$("#nombre").focus();
				$("#nombre").attr("data-toggle","tooltip");
				$("#nombre").attr("data-original-title","El campo no puede est\xe1r Vac\xedo");
				$("#nombre").tooltip({
			        placement : 'bottom',
			        trigger: 'focus'
			    });
				$("#nombre").parent().addClass("has-error has-feedback");
				$('#nombre').tooltip('show');
				//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
				valido = false;
				bandera = false;
			}
		}
		if(bandera == true)
		{
			$("#nombre").parent().removeClass('has-error');
			$("#nombre").parent().addClass("has-success");
			if(!$('#paterno').val())
			{
				$("#paterno").focus();
				$("#paterno").attr("data-toggle","tooltip");
				$("#paterno").attr("data-original-title","El campo no puede est\xe1r Vac\xedo");
				$("#paterno").tooltip({
			        placement : 'bottom',
			        trigger: 'focus'
			    });
				$("#paterno").parent().addClass("has-error has-feedback");
				$('#paterno').tooltip('show');
				//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
				valido = false;
				bandera = false;
			}
		}
		if(bandera == true)
		{
			$("#paterno").parent().removeClass('has-error');
			$("#paterno").parent().addClass("has-success");
			if(!$('#materno').val())
			{
				$("#materno").focus();
				$("#materno").attr("data-toggle","tooltip");
				$("#materno").attr("data-original-title","El campo no puede est\xe1r Vac\xedo");
				$("#materno").tooltip({
			        placement : 'bottom',
			        trigger: 'focus'
			    });
				$("#materno").parent().addClass("has-error has-feedback");
				$('#materno').tooltip('show');
				//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
				valido = false;
				bandera = false;
			}
		}
		if(bandera == true)
		{
			$("#materno").parent().removeClass('has-error');
			$("#materno").parent().addClass("has-success");
			if(!$('#email').val())
			{
				$("#email").focus();
				$("#email").attr("data-toggle","tooltip");
				$("#email").attr("data-original-title","El campo no puede est\xe1r Vac\xedo");
				$("#email").tooltip({
			        placement : 'bottom',
			        trigger: 'focus'
			    });
				$("#email").parent().addClass("has-error has-feedback");
				$('#email').tooltip('show');
				//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
				valido = false;
				bandera = false;
			}
		}
		if(bandera == true)
		{
			$("#email").parent().removeClass('has-error');
			$("#email").parent().addClass("has-success");
			if($("#telefono").val().length == "" && $("#codigo").val() == "")
			{//el telefono fijo es valido si está Vac\xedo
				$("#telefono").removeAttr("data-toggle");
				$("#telefono").removeAttr("data-original-title");
				$("#telefono").tooltip('destroy');
				$("#telefono").parent().parent().parent().removeClass("has-error has-feedback");
			}
			else
			{
				if($("#telefono").val().match(regularTelefono)){//
					$("#telefono").removeAttr("data-toggle");
					$("#telefono").removeAttr("data-original-title");
					$("#telefono").tooltip('destroy');
					$("#telefono").parent().parent().parent().removeClass("has-error has-feedback");
				}
				else
				{
					if(valido == true){
						$("#codigo").focus();
						$("#codigo").val("");
					}		
					$("#telefono").attr("data-toggle","tooltip");
					$("#telefono").attr("data-original-title","El numero "+$("#telefono").val()+" no corresponde a un telefono valido. Seleccione el codigo de area, luego digite su numero telefono de 7 digitos(8 digitos R.M.)");
					$("#telefono").tooltip({
				        placement : 'bottom',
				        trigger: 'focus'
				    });
					//$('#telefono').tooltip('show');
					$("#telefono").parent().parent().parent().addClass("has-error has-feedback");
					//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
					bandera = false;
					valido = false;
				}		
			}
		}
		if(bandera == true)
		{
			$("#telefono").parent().removeClass('has-error');
			$("#telefono").parent().addClass("has-success");
			$("#codigo").parent().removeClass('has-error');
			$("#codigo").parent().addClass("has-success");
			if($("#celular").val().length == "" ){//el campo celular es valido si está Vac\xedo
				$("#celular").removeAttr("data-toggle");
				$("#celular").removeAttr("data-original-title");
				$("#celular").parent().parent().parent().removeClass("has-error has-feedback");
			}
			else{
				if($("#celular").val().match(regularCelular)){//validación del campo de celular(expresion regular)
					$("#celular").removeAttr("data-toggle");
					$("#celular").removeAttr("data-original-title");
					$("#celular").tooltip('destroy');
					$("#celular").parent().parent().parent().removeClass("has-error has-feedback");
				}
				else{
					if(valido == true){
						$("#celular").focus();
					}		
					$("#celular").attr("data-toggle","tooltip");
					$("#celular").attr("data-original-title","El numero "+$("#celular").val()+" no corresponde a un movil valido. Ingrese los 8 digitos de su numero movil");
					$("#celular").tooltip({
				        placement : 'bottom',
				        trigger: 'focus'
				    });
					$("#celular").val("");
					$('#celular').tooltip('show');
					$("#celular").parent().parent().parent().addClass("has-error has-feedback");
					//$("#celular").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
					bandera = false;
					valido = false;
				}
		}		
		}
	}
	if(opcion == 2)
	{
		if(!$('#rut_modif').val())
		{
			$("#rut_modif").focus();
			$("#rut_modif").attr("data-toggle","tooltip");
			$("#rut_modif").attr("data-original-title","El rut no puede ser nulo");
			$("#rut_modif").tooltip({
		        placement : 'bottom',
		        trigger: 'focus'
		    });
			$("#rut_modif").parent().addClass("has-error has-feedback");
			$('#rut_modif').tooltip('show');
			//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
			valido = false;
			bandera = false;
		}
		if(bandera == true)
		{
			$("#rut_modif").parent().removeClass('has-error');
			$("#rut_modif").parent().addClass("has-success");
			if(!$('#nombre_modif').val())
			{
				$("#nombre_modif").focus();
				$("#nombre_modif").attr("data-toggle","tooltip");
				$("#nombre_modif").attr("data-original-title","El campo no puede est\xe1r Vac\xedo");
				$("#nombre_modif").tooltip({
			        placement : 'bottom',
			        trigger: 'focus'
			    });
				$("#nombre_modif").parent().addClass("has-error has-feedback");
				$('#nombre_modif').tooltip('show');
				//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
				valido = false;
				bandera = false;
			}
		}
		if(bandera == true)
		{
			$("#nombre_modif").parent().removeClass('has-error');
			$("#nombre_modif").parent().addClass("has-success");
			if(!$('#paterno_modif').val())
			{
				$("#paterno_modif").focus();
				$("#paterno_modif").attr("data-toggle","tooltip");
				$("#paterno_modif").attr("data-original-title","El campo no puede est\xe1r Vac\xedo");
				$("#paterno_modif").tooltip({
			        placement : 'bottom',
			        trigger: 'focus'
			    });
				$("#paterno_modif").parent().addClass("has-error has-feedback");
				$('#paterno_modif').tooltip('show');
				//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
				valido = false;
				bandera = false;
			}
		}
		if(bandera == true)
		{
			$("#paterno_modif").parent().removeClass('has-error');
			$("#paterno_modif").parent().addClass("has-success");
			if(!$('#materno_modif').val())
			{
				$("#materno_modif").focus();
				$("#materno_modif").attr("data-toggle","tooltip");
				$("#materno_modif").attr("data-original-title","El campo no puede est\xe1r Vac\xedo");
				$("#materno_modif").tooltip({
			        placement : 'bottom',
			        trigger: 'focus'
			    });
				$("#materno_modif").parent().addClass("has-error has-feedback");
				$('#materno_modif').tooltip('show');
				//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
				valido = false;
				bandera = false;
			}
		}
		if(bandera == true)
		{
			$("#materno_modif").parent().removeClass('has-error');
			$("#materno_modif").parent().addClass("has-success");
			if(!$('#email_modif').val())
			{
				$("#email_modif").focus();
				$("#email_modif").attr("data-toggle","tooltip");
				$("#email_modif").attr("data-original-title","El campo no puede est\xe1r Vac\xedo");
				$("#email_modif").tooltip({
			        placement : 'bottom',
			        trigger: 'focus'
			    });
				$("#email_modif").parent().addClass("has-error has-feedback");
				$('#email_modif').tooltip('show');
				//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
				valido = false;
				bandera = false;
			}
		}
		if(bandera == true)
		{
			$("#email_modif").parent().removeClass('has-error');
			$("#email_modif").parent().addClass("has-success");
			if($("#telefono_modif").val().length == "" && $("#codigo_modif").val() == "")
			{//el telefono fijo es valido si está Vac\xedo
				$("#telefono_modif").removeAttr("data-toggle");
				$("#telefono_modif").removeAttr("data-original-title");
				$("#telefono_modif").tooltip('destroy');
				$("#telefono_modif").parent().parent().parent().removeClass("has-error has-feedback");
			}
			else
			{
				if($("#telefono_modif").val().match(regularTelefono)){//
					$("#telefono_modif").removeAttr("data-toggle");
					$("#telefono_modif").removeAttr("data-original-title");
					$("#telefono_modif").tooltip('destroy');
					$("#telefono_modif").parent().parent().parent().removeClass("has-error has-feedback");
				}
				else
				{
					if(valido == true){
						$("#codigo_modif").focus();
						$("#codigo_modif").val("");
					}		
					$("#telefono_modif").attr("data-toggle","tooltip");
					$("#telefono_modif").attr("data-original-title","El numero "+$("#telefono_modif").val()+" no corresponde a un telefono valido. Seleccione el codigo de area, luego digite su numero telefono de 7 digitos(8 digitos R.M.)");
					$("#telefono_modif").tooltip({
				        placement : 'bottom',
				        trigger: 'focus'
				    });
					//$('#telefono').tooltip('show');
					$("#telefono_modif").parent().parent().parent().addClass("has-error has-feedback");
					//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
					bandera = false;
					valido = false;
				}		
			}
		}
		if(bandera == true)
		{
			$("#telefono_modif").parent().removeClass('has-error');
			$("#telefono_modif").parent().addClass("has-success");
			$("#codigo_modif").parent().removeClass('has-error');
			$("#codigo_modif").parent().addClass("has-success");
			if($("#celular_modif").val().length == "" ){//el campo celular es valido si está Vac\xedo
				$("#celular_modif").removeAttr("data-toggle");
				$("#celular_modif").removeAttr("data-original-title");
				$("#celular_modif").parent().parent().parent().removeClass("has-error has-feedback");
			}
			else
			{
				if($("#celular_modif").val().match(regularCelular)){//validación del campo de celular(expresion regular)
					$("#celular_modif").removeAttr("data-toggle");
					$("#celular_modif").removeAttr("data-original-title");
					$("#celular_modif").tooltip('destroy');
					$("#celular_modif").parent().parent().parent().removeClass("has-error has-feedback");
				}
				else
				{
					if(valido == true)
					{
						$("#celular_modif").focus();
					}		
					$("#celular_modif").attr("data-toggle","tooltip");
					$("#celular_modif").attr("data-original-title","El numero "+$("#celular_modif").val()+" no corresponde a un movil valido. Ingrese los 8 digitos de su numero movil");
					$("#celular_modif").tooltip({
				        placement : 'bottom',
				        trigger: 'focus'
				    });
					$("#celular_modif").val("");
					$('#celular_modif').tooltip('show');
					$("#celular_modif").parent().parent().parent().addClass("has-error has-feedback");
					//$("#celular").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
					bandera = false;
					valido = false;
				}
			}		
		}
	}
	if(valido == false){//si existe algun campo invalido		
		return false;
	}
	else{
		if(opcion == 1)
		{
			$("#celular").parent().removeClass('has-error');
			$("#celular").parent().addClass("has-success");
			$("#cargo").parent().addClass("has-success");
			$("#estado").parent().addClass("has-success");

		}
		if(opcion == 2)
		{
			$("#celular_modif").parent().removeClass('has-error');
			$("#celular_modif").parent().addClass("has-success");
			$("#cargo_modif").parent().addClass("has-success");
			$("#estado_modif").parent().addClass("has-success");

		}
		e.preventDefault();
		rotacion_adm(e,opcion);
	}
}


function validar_supervisor(e, opcion){//valida el formulario crecion/modificacion supervisor
	var valido = true;
	bandera = true;
	var regularCelular = "[\+][5][6][9][4-9][0-9]{7}";//----  /^\+\d{2,3}\s\d{9}$/   ---  /^/\+/\d{3}[5-9][0-9]{7}$/ --- /^\+\d{11}$/
	var regularTelefono = "[\+][5][6][-][0-9]{1,2}[-][2-3][1-9][0-9]{5,6}";
	$(".help-block").remove();
	$(".alert.alert-danger.alert-dismissible").remove();
	$(".glyphicon.glyphicon-remove.form-control-feedback").remove();
	if(opcion == 1)
	{
		if(!$('#rut').val())
		{
			$("#rut").focus();
			$("#rut").attr("data-toggle","tooltip");
			$("#rut").attr("data-original-title","El rut no puede ser nulo");
			$("#rut").tooltip({
		        placement : 'bottom',
		        trigger: 'focus'
		    });
			$("#rut").parent().addClass("has-error has-feedback");
			$('#rut').tooltip('show');
			//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
			valido = false;
			bandera = false;
		}
		if(bandera == true)
		{
			$("#rut").parent().removeClass('has-error');
			$("#rut").parent().addClass("has-success");
			if(!$('#nombre').val())
			{
				$("#nombre").focus();
				$("#nombre").attr("data-toggle","tooltip");
				$("#nombre").attr("data-original-title","El campo no puede est\xe1r Vac\xedo");
				$("#nombre").tooltip({
			        placement : 'bottom',
			        trigger: 'focus'
			    });
				$("#nombre").parent().addClass("has-error has-feedback");
				$('#nombre').tooltip('show');
				//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
				valido = false;
				bandera = false;
			}
		}
		if(bandera == true)
		{
			$("#nombre").parent().removeClass('has-error');
			$("#nombre").parent().addClass("has-success");
			if(!$('#paterno').val())
			{
				$("#paterno").focus();
				$("#paterno").attr("data-toggle","tooltip");
				$("#paterno").attr("data-original-title","El campo no puede est\xe1r Vac\xedo");
				$("#paterno").tooltip({
			        placement : 'bottom',
			        trigger: 'focus'
			    });
				$("#paterno").parent().addClass("has-error has-feedback");
				$('#paterno').tooltip('show');
				//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
				valido = false;
				bandera = false;
			}
		}
		if(bandera == true)
		{
			$("#paterno").parent().removeClass('has-error');
			$("#paterno").parent().addClass("has-success");
			if(!$('#materno').val())
			{
				$("#materno").focus();
				$("#materno").attr("data-toggle","tooltip");
				$("#materno").attr("data-original-title","El campo no puede est\xe1r Vac\xedo");
				$("#materno").tooltip({
			        placement : 'bottom',
			        trigger: 'focus'
			    });
				$("#materno").parent().addClass("has-error has-feedback");
				$('#materno').tooltip('show');
				//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
				valido = false;
				bandera = false;
			}
		}
		if(bandera == true)
		{
			$("#materno").parent().removeClass('has-error');
			$("#materno").parent().addClass("has-success");
			if(!$('#email').val())
			{
				$("#email").focus();
				$("#email").attr("data-toggle","tooltip");
				$("#email").attr("data-original-title","El campo no puede est\xe1r Vac\xedo");
				$("#email").tooltip({
			        placement : 'bottom',
			        trigger: 'focus'
			    });
				$("#email").parent().addClass("has-error has-feedback");
				$('#email').tooltip('show');
				//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
				valido = false;
				bandera = false;
			}
		}
		if(bandera == true)
		{
			$("#email").parent().removeClass('has-error');
			$("#email").parent().addClass("has-success");
			if(!$('#empresa').val())
			{
				$("#empresa").focus();
				$("#empresa").attr("data-toggle","tooltip");
				$("#empresa").attr("data-original-title","Debe seleccionar uno de los valores");
				$("#empresa").tooltip({
			        placement : 'bottom',
			        trigger: 'focus'
			    });
				$("#empresa").parent().addClass("has-error has-feedback");
				$('#empresa').tooltip('show');
				//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
				valido = false;
				bandera = false;
			}
		}
		if(bandera == true)
		{
			$("#empresa").parent().removeClass('has-error');
			$("#empresa").parent().addClass("has-success");
			if($("#telefono").val().length == "" && $("#codigo").val() == "")
			{//el telefono fijo es valido si está Vac\xedo
				$("#telefono").removeAttr("data-toggle");
				$("#telefono").removeAttr("data-original-title");
				$("#telefono").tooltip('destroy');
				$("#telefono").parent().parent().parent().removeClass("has-error has-feedback");
			}
			else
			{
				if($("#telefono").val().match(regularTelefono)){//
					$("#telefono").removeAttr("data-toggle");
					$("#telefono").removeAttr("data-original-title");
					$("#telefono").tooltip('destroy');
					$("#telefono").parent().parent().parent().removeClass("has-error has-feedback");
				}
				else
				{
					if(valido == true){
						$("#codigo").focus();
						$("#codigo").val("");
					}		
					$("#telefono").attr("data-toggle","tooltip");
					$("#telefono").attr("data-original-title","El numero "+$("#telefono").val()+" no corresponde a un telefono valido. Seleccione el codigo de area, luego digite su numero telefono de 7 digitos(8 digitos R.M.)");
					$("#telefono").tooltip({
				        placement : 'bottom',
				        trigger: 'focus'
				    });
					//$('#telefono').tooltip('show');
					$("#telefono").parent().parent().parent().addClass("has-error has-feedback");
					//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
					bandera = false;
					valido = false;
				}		
			}
		}
		if(bandera == true)
		{
			$("#telefono").parent().removeClass('has-error');
			$("#telefono").parent().addClass("has-success");
			$("#codigo").parent().removeClass('has-error');
			$("#codigo").parent().addClass("has-success ");
			if($("#celular").val().length == "" ){//el campo celular es valido si está Vac\xedo
				$("#celular").removeAttr("data-toggle");
				$("#celular").removeAttr("data-original-title");
				$("#celular").parent().parent().parent().removeClass("has-error has-feedback");
			}
			else
			{
				if($("#celular").val().match(regularCelular)){//validación del campo de celular(expresion regular)
					$("#celular").removeAttr("data-toggle");
					$("#celular").removeAttr("data-original-title");
					$("#celular").tooltip('destroy');
					$("#celular").parent().parent().parent().removeClass("has-error has-feedback");
				}
				else
				{
					if(valido == true){
						$("#celular").focus();
					}		
					$("#celular").attr("data-toggle","tooltip");
					$("#celular").attr("data-original-title","El numero "+$("#celular").val()+" no corresponde a un movil valido. Ingrese los 8 digitos de su numero movil");
					$("#celular").tooltip({
				        placement : 'bottom',
				        trigger: 'focus'
				    });
					$("#celular").val("");
					$('#celular').tooltip('show');
					$("#celular").parent().parent().parent().addClass("has-error has-feedback");
					//$("#celular").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
					bandera = false;
					valido = false;
				}
			}		
		}
	}
	if(opcion == 2)
	{
		if(!$('#rut_modif').val())
		{
			$("#rut_modif").focus();
			$("#rut_modif").attr("data-toggle","tooltip");
			$("#rut_modif").attr("data-original-title","El rut no puede ser nulo");
			$("#rut_modif").tooltip({
		        placement : 'bottom',
		        trigger: 'focus'
		    });
			$("#rut_modif").parent().addClass("has-error has-feedback");
			$('#rut_modif').tooltip('show');
			//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
			valido = false;
			bandera = false;
		}
		if(bandera == true)
		{
			$("#rut_modif").parent().removeClass('has-error');
			$("#rut_modif").parent().addClass("has-success");
			if(!$('#nombre_modif').val())
			{
				$("#nombre_modif").focus();
				$("#nombre_modif").attr("data-toggle","tooltip");
				$("#nombre_modif").attr("data-original-title","El campo no puede est\xe1r Vac\xedo");
				$("#nombre_modif").tooltip({
			        placement : 'bottom',
			        trigger: 'focus'
			    });
				$("#nombre_modif").parent().addClass("has-error has-feedback");
				$('#nombre_modif').tooltip('show');
				//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
				valido = false;
				bandera = false;
			}
		}
		if(bandera == true)
		{
			$("#nombre_modif").parent().removeClass('has-error');
			$("#nombre_modif").parent().addClass("has-success");
			if(!$('#paterno_modif').val())
			{
				$("#paterno_modif").focus();
				$("#paterno_modif").attr("data-toggle","tooltip");
				$("#paterno_modif").attr("data-original-title","El campo no puede est\xe1r Vac\xedo");
				$("#paterno_modif").tooltip({
			        placement : 'bottom',
			        trigger: 'focus'
			    });
				$("#paterno_modif").parent().addClass("has-error has-feedback");
				$('#paterno_modif').tooltip('show');
				//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
				valido = false;
				bandera = false;
			}
		}
		if(bandera == true)
		{
			$("#paterno_modif").parent().removeClass('has-error');
			$("#paterno_modif").parent().addClass("has-success");
			if(!$('#materno_modif').val())
			{
				$("#materno_modif").focus();
				$("#materno_modif").attr("data-toggle","tooltip");
				$("#materno_modif").attr("data-original-title","El campo no puede est\xe1r Vac\xedo");
				$("#materno_modif").tooltip({
			        placement : 'bottom',
			        trigger: 'focus'
			    });
				$("#materno_modif").parent().addClass("has-error has-feedback");
				$('#materno_modif').tooltip('show');
				//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
				valido = false;
				bandera = false;
			}
		}
		if(bandera == true)
		{
			$("#materno_modif").parent().removeClass('has-error');
			$("#materno_modif").parent().addClass("has-success");
			if(!$('#email_modif').val())
			{
				$("#email_modif").focus();
				$("#email_modif").attr("data-toggle","tooltip");
				$("#email_modif").attr("data-original-title","El campo no puede est\xe1r Vac\xedo");
				$("#email_modif").tooltip({
			        placement : 'bottom',
			        trigger: 'focus'
			    });
				$("#email_modif").parent().addClass("has-error has-feedback");
				$('#email_modif').tooltip('show');
				//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
				valido = false;
				bandera = false;
			}
		}
		if(bandera == true)
		{
			$("#email_modif").parent().removeClass('has-error');
			$("#email_modif").parent().addClass("has-success");
			if(!$('#empresa_modif').val())
			{
				$("#empresa_modif").focus();
				$("#empresa_modif").attr("data-toggle","tooltip");
				$("#empresa_modif").attr("data-original-title","Debe seleccionar uno de los valores");
				$("#empresa_modif").tooltip({
			        placement : 'bottom',
			        trigger: 'focus'
			    });
				$("#empresa_modif").parent().addClass("has-error has-feedback");
				$('#empresa_modif').tooltip('show');
				//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
				valido = false;
				bandera = false;
			}
		}
		if(bandera == true)
		{
			$("#empresa_modif").parent().removeClass('has-error');
			$("#empresa_modif").parent().addClass("has-success");
			if($("#telefono_modif").val().length == "" && $("#codigo_modif").val() == "")
			{//el telefono fijo es valido si está Vac\xedo
				$("#telefono_modif").removeAttr("data-toggle");
				$("#telefono_modif").removeAttr("data-original-title");
				$("#telefono_modif").tooltip('destroy');
				$("#telefono_modif").parent().parent().parent().removeClass("has-error has-feedback");
			}
			else
			{
				if($("#telefono_modif").val().match(regularTelefono)){//
					$("#telefono_modif").removeAttr("data-toggle");
					$("#telefono_modif").removeAttr("data-original-title");
					$("#telefono_modif").tooltip('destroy');
					$("#telefono_modif").parent().parent().parent().removeClass("has-error has-feedback");
				}
				else
				{
					if(valido == true){
						$("#codigo_modif").focus();
						$("#codigo_modif").val("");
					}		
					$("#telefono_modif").attr("data-toggle","tooltip");
					$("#telefono_modif").attr("data-original-title","El numero "+$("#telefono_modif").val()+" no corresponde a un telefono valido. Seleccione el codigo de area, luego digite su numero telefono de 7 digitos(8 digitos R.M.)");
					$("#telefono_modif").tooltip({
				        placement : 'bottom',
				        trigger: 'focus'
				    });
					//$('#telefono').tooltip('show');
					$("#telefono_modif").parent().parent().parent().addClass("has-error has-feedback");
					//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
					bandera = false;
					valido = false;
				}		
			}
		}
		if(bandera == true)
		{
			$("#telefono_modif").parent().removeClass('has-error');
			$("#telefono_modif").parent().addClass("has-success");
			$("#codigo_modif").parent().removeClass('has-error');
			$("#codigo_modif").parent().addClass("has-success");
			if($("#celular_modif").val().length == "" ){//el campo celular es valido si está Vac\xedo
				$("#celular_modif").removeAttr("data-toggle");
				$("#celular_modif").removeAttr("data-original-title");
				$("#celular_modif").parent().parent().parent().removeClass("has-error has-feedback");
			}
			else
			{
				if($("#celular_modif").val().match(regularCelular)){//validación del campo de celular(expresion regular)
					$("#celular_modif").removeAttr("data-toggle");
					$("#celular_modif").removeAttr("data-original-title");
					$("#celular_modif").tooltip('destroy');
					$("#celular_modif").parent().parent().parent().removeClass("has-error has-feedback");
				}
				else
				{
					if(valido == true)
					{
						$("#celular_modif").focus();
					}		
					$("#celular_modif").attr("data-toggle","tooltip");
					$("#celular_modif").attr("data-original-title","El numero "+$("#celular_modif").val()+" no corresponde a un movil valido. Ingrese los 8 digitos de su numero movil");
					$("#celular_modif").tooltip({
				        placement : 'bottom',
				        trigger: 'focus'
				    });
					$("#celular_modif").val("");
					$('#celular_modif').tooltip('show');
					$("#celular_modif").parent().parent().parent().addClass("has-error has-feedback");
					//$("#celular").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
					bandera = false;
					valido = false;
				}
			}		
		}
	}
	
	
	if(valido == false){//si existe algun campo invalido		
		return false;
	}
	else{
		if(opcion == 1)
		{
			$("#celular").parent().removeClass('has-error');
			$("#celular").parent().addClass("has-success");
			$("#cargo").parent().addClass("has-success");
			$("#profesion").parent().addClass("has-success");
		}
		if(opcion == 2)
		{
			$("#celular_modif").parent().removeClass('has-error');
			$("#celular_modif").parent().addClass("has-success");
			$("#cargo_modif").parent().addClass("has-success");
			$("#profesion_modif").parent().addClass("has-success");
			$("#estado_modif").parent().addClass("has-success");
		}
		
		e.preventDefault();
		rotacion_sup(e,opcion);
	}
}

function validar_empresa(e, opcion){//valida el formulario crecion/modificacion empresa
	var valido = true;
	bandera = true;
	var regularCelular = "[\+][5][6][9][4-9][0-9]{7}";//----  /^\+\d{2,3}\s\d{9}$/   ---  /^/\+/\d{3}[5-9][0-9]{7}$/ --- /^\+\d{11}$/
	var regularTelefono = "[\+][5][6][-][0-9]{1,2}[-][2-3][1-9][0-9]{5,6}";
	var regularDireccion = "[A-Za-záéíóúàèìòùÀÈÌÒÙÁÉÍÓÚñÑüÜ ][#][0-9]";//expresion regular de direccion Caracteres + # + numeros
	$(".help-block").remove();
	$(".alert.alert-danger.alert-dismissible").remove();
	$(".glyphicon.glyphicon-remove.form-control-feedback").remove();
	if(opcion == 1)
	{
		if(!$('#rut').val())
		{
			$("#rut").focus();
			$("#rut").attr("data-toggle","tooltip");
			$("#rut").attr("data-original-title","El rut no puede ser nulo");
			$("#rut").tooltip({
		        placement : 'bottom',
		        trigger: 'focus'
		    });
			$("#rut").parent().addClass("has-error has-feedback");
			$('#rut').tooltip('show');
			//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
			valido = false;
			bandera = false;
		}
		if(bandera == true)
		{
			$("#rut").parent().removeClass('has-error');
			$("#rut").parent().addClass("has-success");
			if(!$('#region').val())
			{
				$("#region").focus();
				$("#region").attr("data-toggle","tooltip");
				$("#region").attr("data-original-title","Debe seleccionar uno de los valores");
				$("#region").tooltip({
			        placement : 'bottom',
			        trigger: 'focus'
			    });
				$("#region").parent().addClass("has-error has-feedback");
				$('#region').tooltip('show');
				//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
				valido = false;
				bandera = false;
			}
		}
		if(bandera == true)
		{
			$("#region").parent().removeClass('has-error');
			$("#region").parent().addClass("has-success");
			if(!$('#provincia').val())
			{
				$("#provincia").focus();
				$("#provincia").attr("data-toggle","tooltip");
				$("#provincia").attr("data-original-title","Debe seleccionar uno de los valores");
				$("#provincia").tooltip({
			        placement : 'bottom',
			        trigger: 'focus'
			    });
				$("#provincia").parent().addClass("has-error has-feedback");
				$('#provincia').tooltip('show');
				//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
				valido = false;
				bandera = false;
			}
		}
		if(bandera == true)
		{
			$("#provincia").parent().removeClass('has-error');
			$("#provincia").parent().addClass("has-success");
			if(!$('#comuna').val())
			{
				$("#comuna").focus();
				$("#comuna").attr("data-toggle","tooltip");
				$("#comuna").attr("data-original-title","Debe seleccionar uno de los valores");
				$("#comuna").tooltip({
			        placement : 'bottom',
			        trigger: 'focus'
			    });
				$("#comuna").parent().addClass("has-error has-feedback");
				$('#comuna').tooltip('show');
				//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
				valido = false;
				bandera = false;
			}
		}
		if(bandera == true)
		{
			$("#comuna").parent().removeClass('has-error');
			$("#comuna").parent().addClass("has-success");
			if(!$('#nombre').val())
			{
				$("#nombre").focus();
				$("#nombre").attr("data-toggle","tooltip");
				$("#nombre").attr("data-original-title","El campo no puede est\xe1r Vac\xedo");
				$("#nombre").tooltip({
			        placement : 'bottom',
			        trigger: 'focus'
			    });
				$("#nombre").parent().addClass("has-error has-feedback");
				$('#nombre').tooltip('show');
				//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
				valido = false;
				bandera = false;
			}
		}
		if(bandera == true)
		{
			$("#nombre").parent().removeClass('has-error');
			$("#nombre").parent().addClass("has-success");
			if(!$('#direccion').val())
			{
				$("#direccion").focus();
				$("#direccion").attr("data-toggle","tooltip");
				$("#direccion").attr("data-original-title","El campo no puede est\xe1r Vac\xedo");
				$("#direccion").tooltip({
			        placement : 'bottom',
			        trigger: 'focus'
			    });
				$("#direccion").parent().addClass("has-error has-feedback");
				$('#direccion').tooltip('show');
				//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
				valido = false;
				bandera = false;
			}else{
				if($("#direccion").val().match(regularDireccion)){//si está bien validado
					$("#direccion").removeAttr("data-toggle");
					$("#direccion").removeAttr("data-original-title");
					$("#direccion").tooltip('destroy');
					$("#direccion").parent().parent().removeClass("has-error has-feedback");
					$("#direccion").parent().parent().addClass("has-success has-feedback");
					$("#direccion").parent().append('<span class="glyphicon glyphicon-ok form-control-feedback"></span>');
				}
				else{
					if(valido == true){
						$("#direccion").focus();
					}
					
					$("#direccion").attr("data-toggle","tooltip");
					$("#direccion").attr("data-original-title","No corresponde a una direccion valida Ej: La Calle #889 El Sector");
					$("#direccion").tooltip({
				        placement : 'bottom',
				        //placement : 'right',
				        trigger: 'focus'
				    });
					$('#direccion').tooltip('show');
					$("#direccion").parent().parent().addClass("has-error has-feedback");
					$("#direccion").parent().append('<span class="glyphicon glyphicon-remove form-control-feedback"></span>');
					valido = false;
				}
			}
		}
		if(bandera == true)
		{
			$("#direccion").parent().removeClass('has-error');
			$("#direccion").parent().addClass("has-success");
			if($("#telefono").val().length == "" && $("#codigo").val() == "")
			{//el telefono fijo es valido si está Vac\xedo
				$("#telefono").removeAttr("data-toggle");
				$("#telefono").removeAttr("data-original-title");
				$("#telefono").tooltip('destroy');
				$("#telefono").parent().parent().parent().removeClass("has-error has-feedback");
			}
			else
			{
				if($("#telefono").val().match(regularTelefono)){//
					$("#telefono").removeAttr("data-toggle");
					$("#telefono").removeAttr("data-original-title");
					$("#telefono").tooltip('destroy');
					$("#telefono").parent().parent().parent().removeClass("has-error has-feedback");
				}
				else
				{
					if(valido == true){
						$("#codigo").focus();
						$("#codigo").val("");
					}		
					$("#telefono").attr("data-toggle","tooltip");
					$("#telefono").attr("data-original-title","El numero "+$("#telefono").val()+" no corresponde a un telefono valido. Seleccione el codigo de area, luego digite su numero telefono de 7 digitos(8 digitos R.M.)");
					$("#telefono").tooltip({
				        placement : 'bottom',
				        trigger: 'focus'
				    });
					//$('#telefono').tooltip('show');
					$("#telefono").parent().parent().parent().addClass("has-error has-feedback");
					//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
					bandera = false;
					valido = false;
				}		
			}
		}
		if(bandera == true)
		{
			$("#telefono").parent().removeClass('has-error');
			$("#telefono").parent().addClass("has-success");
			$("#codigo").parent().removeClass('has-error');
			$("#codigo").parent().addClass("has-success");
			if($("#celular").val().length == "" ){//el campo celular es valido si está Vac\xedo
				$("#celular").removeAttr("data-toggle");
				$("#celular").removeAttr("data-original-title");
				$("#celular").parent().parent().parent().removeClass("has-error has-feedback");
			}
			else
			{
				if($("#celular").val().match(regularCelular)){//validación del campo de celular(expresion regular)
					$("#celular").removeAttr("data-toggle");
					$("#celular").removeAttr("data-original-title");
					$("#celular").tooltip('destroy');
					$("#celular").parent().parent().parent().removeClass("has-error has-feedback");
				}
				else
				{
					if(valido == true){
						$("#celular").focus();
					}		
					$("#celular").attr("data-toggle","tooltip");
					$("#celular").attr("data-original-title","El numero "+$("#celular").val()+" no corresponde a un movil valido. Ingrese los 8 digitos de su numero movil");
					$("#celular").tooltip({
				        placement : 'bottom',
				        trigger: 'focus'
				    });
					$("#celular").val("");
					$('#celular').tooltip('show');
					$("#celular").parent().parent().parent().addClass("has-error has-feedback");
					//$("#celular").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
					bandera = false;
					valido = false;
				}
			}		
		}
		if(bandera == true)
		{
			$("#celular").parent().removeClass('has-error');
			$("#celular").parent().addClass("has-success");
			if(!$('#descripcion').val())
			{
				$("#descripcion").focus();
				$("#descripcion").attr("data-toggle","tooltip");
				$("#descripcion").attr("data-original-title","El campo no puede est\xe1r Vac\xedo");
				$("#descripcion").tooltip({
			        placement : 'bottom',
			        trigger: 'focus'
			    });
				$("#descripcion").parent().addClass("has-error has-feedback");
				$('#descripcion').tooltip('show');
				//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
				valido = false;
				bandera = false;
			}
		}
		$("#descripcion").parent().removeClass('has-error');
		$("#descripcion").parent().addClass("has-success");
		$("#email").parent().removeClass('has-error');
		$("#email").parent().addClass("has-success");	
	}
	if(opcion == 2)
	{
		if(!$('#rut_modif').val())
		{
			$("#rut_modif").focus();
			$("#rut_modif").attr("data-toggle","tooltip");
			$("#rut_modif").attr("data-original-title","El rut no puede ser nulo");
			$("#rut_modif").tooltip({
		        placement : 'bottom',
		        trigger: 'focus'
		    });
			$("#rut_modif").parent().addClass("has-error has-feedback");
			$('#ru_modift').tooltip('show');
			//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
			valido = false;
			bandera = false;
		}
		if(bandera == true)
		{
			$("#rut_modif").parent().removeClass('has-error');
			$("#rut_modif").parent().addClass("has-success");
			if(!$('#region_modif').val())
			{
				$("#region_modif").focus();
				$("#region_modif").attr("data-toggle","tooltip");
				$("#region_modif").attr("data-original-title","Debe seleccionar uno de los valores");
				$("#region_modif").tooltip({
			        placement : 'bottom',
			        trigger: 'focus'
			    });
				$("#region_modif").parent().addClass("has-error has-feedback");
				$('#region_modif').tooltip('show');
				//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
				valido = false;
				bandera = false;
			}
		}
		if(bandera == true)
		{
			$("#region_modif").parent().removeClass('has-error');
			$("#region_modif").parent().addClass("has-success");
			if(!$('#provincia_modif').val())
			{
				$("#provincia_modif").focus();
				$("#provincia_modif").attr("data-toggle","tooltip");
				$("#provincia_modif").attr("data-original-title","Debe seleccionar uno de los valores");
				$("#provincia_modif").tooltip({
			        placement : 'bottom',
			        trigger: 'focus'
			    });
				$("#provincia_modif").parent().addClass("has-error has-feedback");
				$('#provincia_modif').tooltip('show');
				//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
				valido = false;
				bandera = false;
			}
		}
		if(bandera == true)
		{
			$("#provincia_modif").parent().removeClass('has-error');
			$("#provincia_modif").parent().addClass("has-success");
			if(!$('#comuna_modif').val())
			{
				$("#comuna_modif").focus();
				$("#comuna_modif").attr("data-toggle","tooltip");
				$("#comuna_modif").attr("data-original-title","Debe seleccionar uno de los valores");
				$("#comun_modif").tooltip({
			        placement : 'bottom',
			        trigger: 'focus'
			    });
				$("#comuna_modif").parent().addClass("has-error has-feedback");
				$('#comuna_modif').tooltip('show');
				//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
				valido = false;
				bandera = false;
			}
		}
		if(bandera == true)
		{
			$("#comuna_modif").parent().removeClass('has-error');
			$("#comuna_modif").parent().addClass("has-success");
			if(!$('#nombre_modif').val())
			{
				$("#nombre_modif").focus();
				$("#nombre_modif").attr("data-toggle","tooltip");
				$("#nombre_modif").attr("data-original-title","El campo no puede est\xe1r Vac\xedo");
				$("#nombre_modif").tooltip({
			        placement : 'bottom',
			        trigger: 'focus'
			    });
				$("#nombre_modif").parent().addClass("has-error has-feedback");
				$('#nombre_modif').tooltip('show');
				//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
				valido = false;
				bandera = false;
			}
		}
		if(bandera == true)
		{
			$("#nombre_modif").parent().removeClass('has-error');
			$("#nombre_modif").parent().addClass("has-success");
			if(!$('#direccion_modif').val())
			{
				$("#direccion_modif").focus();
				$("#direccion_modif").attr("data-toggle","tooltip");
				$("#direccion_modif").attr("data-original-title","El campo no puede est\xe1r Vac\xedo");
				$("#direccion_modif").tooltip({
			        placement : 'bottom',
			        trigger: 'focus'
			    });
				$("#direccion_modif").parent().addClass("has-error has-feedback");
				$('#direccion_modif').tooltip('show');
				//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
				valido = false;
				bandera = false;
			}else{
				if($("#direccion_modif").val().match(regularDireccion)){//si está bien validado
					$("#direccion_modif").removeAttr("data-toggle");
					$("#direccion_modif").removeAttr("data-original-title");
					$("#direccion_modif").tooltip('destroy');
					$("#direccion_modif").parent().parent().removeClass("has-error has-feedback");
					$("#direccion_modif").parent().parent().addClass("has-success has-feedback");
					$("#direccion_modif").parent().append('<span class="glyphicon glyphicon-ok form-control-feedback"></span>');
				}
				else{
					if(valido == true){
						$("#direccion_modif").focus();
					}
					
					$("#direccion_modif").attr("data-toggle","tooltip");
					$("#direccion_modif").attr("data-original-title","No corresponde a una direccion valida Ej: La Calle #889 El Sector");
					$("#direccion_modif").tooltip({
				        placement : 'bottom',
				        //placement : 'right',
				        trigger: 'focus'
				    });
					$('#direccion_modif').tooltip('show');
					$("#direccion_modif").parent().parent().addClass("has-error has-feedback");
					$("#direccion_modif").parent().append('<span class="glyphicon glyphicon-remove form-control-feedback"></span>');
					valido = false;
				}
			}
		}
		if(bandera == true)
		{
			$("#direccion_modif").parent().removeClass('has-error');
			$("#direccion_modif").parent().addClass("has-success");
			if($("#telefono_modif").val().length == "" && $("#codigo_modif").val() == "")
			{//el telefono fijo es valido si está Vac\xedo
				$("#telefono_modif").removeAttr("data-toggle");
				$("#telefono_modif").removeAttr("data-original-title");
				$("#telefono_modif").tooltip('destroy');
				$("#telefono_modif").parent().parent().parent().removeClass("has-error has-feedback");
			}
			else
			{
				if($("#telefono_modif").val().match(regularTelefono)){//
					$("#telefono_modif").removeAttr("data-toggle");
					$("#telefono_modif").removeAttr("data-original-title");
					$("#telefono_modif").tooltip('destroy');
					$("#telefono_modif").parent().parent().parent().removeClass("has-error has-feedback");
				}
				else
				{
					if(valido == true){
						$("#codigo_modif").focus();
						$("#codigo_modif").val("");
					}		
					$("#telefono_modif").attr("data-toggle","tooltip");
					$("#telefono_modif").attr("data-original-title","El numero "+$("#telefono_modif").val()+" no corresponde a un telefono valido. Seleccione el codigo de area, luego digite su numero telefono de 7 digitos(8 digitos R.M.)");
					$("#telefono_modif").tooltip({
				        placement : 'bottom',
				        trigger: 'focus'
				    });
					//$('#telefono').tooltip('show');
					$("#telefono_modif").parent().parent().parent().addClass("has-error has-feedback");
					//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
					bandera = false;
					valido = false;
				}		
			}
		}
		if(bandera == true)
		{
			$("#telefono_modif").parent().removeClass('has-error');
			$("#telefono_modif").parent().addClass("has-success");
			$("#codigo_modif").parent().removeClass('has-error');
			$("#codigo_modif").parent().addClass("has-success");
			if($("#celular_modif").val().length == "" ){//el campo celular es valido si está Vac\xedo
				$("#celular_modif").removeAttr("data-toggle");
				$("#celular_modif").removeAttr("data-original-title");
				$("#celular_modif").parent().parent().parent().removeClass("has-error has-feedback");
			}
			else
			{
				if($("#celular_modif").val().match(regularCelular)){//validación del campo de celular(expresion regular)
					$("#celular_modif").removeAttr("data-toggle");
					$("#celular_modif").removeAttr("data-original-title");
					$("#celular_modif").tooltip('destroy');
					$("#celular_modif").parent().parent().parent().removeClass("has-error has-feedback");
				}
				else
				{
					if(valido == true)
					{
						$("#celular_modif").focus();
					}		
					$("#celular_modif").attr("data-toggle","tooltip");
					$("#celular_modif").attr("data-original-title","El numero "+$("#celular_modif").val()+" no corresponde a un movil valido. Ingrese los 8 digitos de su numero movil");
					$("#celular_modif").tooltip({
				        placement : 'bottom',
				        trigger: 'focus'
				    });
					$("#celular_modif").val("");
					$('#celular_modif').tooltip('show');
					$("#celular_modif").parent().parent().parent().addClass("has-error has-feedback");
					//$("#celular").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
					bandera = false;
					valido = false;
				}
			}		
		}
		if(bandera == true)
		{
			$("#celular_modif").parent().removeClass('has-error');
			$("#celular_modif").parent().addClass("has-success");
			if(!$('#descripcion_modif').val())
			{
				$("#descripcion_modif").focus();
				$("#descripcion_modif").attr("data-toggle","tooltip");
				$("#descripcion_modif").attr("data-original-title","El campo no puede est\xe1r Vac\xedo");
				$("#descripcion_modif").tooltip({
			        placement : 'bottom',
			        trigger: 'focus'
			    });
				$("#descripcion_modif").parent().addClass("has-error has-feedback");
				$('#descripcion_modif').tooltip('show');
				//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
				valido = false;
				bandera = false;
			}
		}
		if(bandera == true)
		{
			$("#descripcion_modif").parent().removeClass('has-error');
			$("#descripcion_modif").parent().addClass("has-success");
			if(!$('#email_modif').val())
			{
				$("#email_modif").focus();
				$("#email_modif").attr("data-toggle","tooltip");
				$("#email_modif").attr("data-original-title","El campo no puede est\xe1r Vac\xedo");
				$("#email_modif").tooltip({
			        placement : 'bottom',
			        trigger: 'focus'
			    });
				$("#email_modif").parent().addClass("has-error has-feedback");
				$('#email_modif').tooltip('show');
				//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
				valido = false;
				bandera = false;
			}
		}
		
		if(bandera == true)
		{
			$("#email_modif").parent().removeClass('has-error');
			$("#email_modif").parent().addClass("has-success");
			if(!$('#web_modif').val())
			{
				$("#web_modif").focus();
				$("#web_modif").attr("data-toggle","tooltip");
				$("#web_modif").attr("data-original-title","El campo no puede est\xe1r Vac\xedo");
				$("#web_modif").tooltip({
			        placement : 'bottom',
			        trigger: 'focus'
			    });
				$("#web_modif").parent().addClass("has-error has-feedback");
				$('#web_modif').tooltip('show');
				//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
				valido = false;
				bandera = false;
			}
		}
	}
	
	if(valido == false){//si existe algun campo invalido		
		return false;
	}
	else{
		if(opcion == 1)
		{
			$("#web").parent().removeClass('has-error');
			$("#web").parent().addClass("has-success");
		}
		if(opcion == 2)
		{
			$("#web_modif").parent().removeClass('has-error');
			$("#web_modif").parent().addClass("has-success");
		}
		
		e.preventDefault();
		rotacion_emp(e,opcion);
	}
}


function validar_plan(e,opcion){//valida el formulario de plan de estudio
	var valido = true;
	var bandera = true;
	$(".help-block").remove();
	$(".alert.alert-danger.alert-dismissible").remove();
	$(".glyphicon.glyphicon-remove.form-control-feedback").remove();
	
	for (var i = 1; i <= $("#total").val(); i++) {
		
		if (!$("#codigo_"+i).val()) {//si el codigo es nulo
			$("#codigo_"+i).focus();
			$("#codigo_"+i).attr("data-toggle","tooltip");
			$("#codigo_"+i).attr("data-original-title","El codigo no puede ser nulo");
			$("#codigo_"+i).tooltip({
		        placement : 'bottom',
		        trigger: 'focus'
		    });
			$("#codigo_"+i).tooltip("show");
			valido = false;
			bandera = false;
			i = $("#total").val() + 1;//quebra el ciclo y ocasiona el focus donde está el error
		}
		else{//si el codigo es distinto de nulo pero //validar que no sea 000000 //validar que el codigo no se encuentre repetido
			var repetido = false;
			for (var j = 1; j < $("#total").val(); j++) {
				if(!$("#codigo_"+j).val()){
					
				}
				else{
					if($("#codigo_"+i).val() == $("#codigo_"+j).val() && i != j){
						repetido = true;
					}
				}				
			}
			if(repetido){
				$("#codigo_"+i).focus();
				$("#codigo_"+i).attr("data-toggle","tooltip");
				$("#codigo_"+i).attr("data-original-title","El codigo ingresado se encuentra repetido. El codigo de asignatura debe ser unico");
				$("#codigo_"+i).tooltip({
			        placement : 'bottom',
			        trigger: 'focus'
			    });
				$("#codigo_"+i).tooltip("show");
				valido = false;
				bandera = false;
				i = $("#total").val() + 1;//quebra el ciclo y ocasiona el focus donde está el error
			}
		}
		if(bandera == true){
			$("#codigo_"+i).removeAttr("data-toggle");
			$("#codigo_"+i).removeAttr("data-original-title");
			$("#codigo_"+i).tooltip('destroy');
			if (!$("#nombre_"+i).val()) {//si el nombre es nulo
				$("#nombre_"+i).focus();
				$("#nombre_"+i).attr("data-toggle","tooltip");
				$("#nombre_"+i).attr("data-original-title","El nombre de la asignatura no puede ser nulo");
				$("#nombre_"+i).tooltip({
			        placement : 'bottom',
			        trigger: 'focus'
			    });
				$("#nombre_"+i).tooltip("show");
				valido = false;
				bandera = false;
				i = $("#total").val() + 1;
			}
		}
		if(bandera == true){
			$("#nombre_"+i).removeAttr("data-toggle");
			$("#nombre_"+i).removeAttr("data-original-title");
			$("#nombre_"+i).tooltip('destroy');
			if (!$("#horas_"+i).val()) {//si el horas es nulo
				$("#horas_"+i).focus();
				$("#horas_"+i).attr("data-toggle","tooltip");
				$("#horas_"+i).attr("data-original-title","La cantidad de horas no puede ser vacia");
				$("#horas_"+i).tooltip({
			        placement : 'bottom',
			        trigger: 'focus'
			    });
				$("#horas_"+i).tooltip("show");
				valido = false;
				bandera = false;
				i = $("#total").val() + 1;
			}
			else{//validar hora maxima y hora minima
				if($("#horas_"+i).val() < 1){
					$("#horas_"+i).focus();
					$("#horas_"+i).attr("data-toggle","tooltip");
					$("#horas_"+i).attr("data-original-title","La cantidad de horas no puede ser menor que 1");
					$("#horas_"+i).tooltip({
				        placement : 'bottom',
				        trigger: 'focus'
				    });
					$("#horas_"+i).tooltip("show");
					valido = false;
					bandera = false;
					i = $("#total").val() + 1;
				}
				if($("#horas_"+i).val() > 1000){
					$("#horas_"+i).focus();
					$("#horas_"+i).attr("data-toggle","tooltip");
					$("#horas_"+i).attr("data-original-title","La cantidad de horas no puede ser mayor que 1000");
					$("#horas_"+i).tooltip({
				        placement : 'bottom',
				        trigger: 'focus'
				    });
					$("#horas_"+i).tooltip("show");
					valido = false;
					bandera = false;
					i = $("#total").val() + 1;
				}
			}
		}
		if(bandera == true){
			$("#horas_"+i).removeAttr("data-toggle");
			$("#horas_"+i).removeAttr("data-original-title");
			$("#horas_"+i).tooltip('destroy');
			if (!$("#creditos_"+i).val()) {//si el creditos es nulo
				$("#creditos_"+i).focus();
				$("#creditos_"+i).attr("data-toggle","tooltip");
				$("#creditos_"+i).attr("data-original-title","La cantidad de creditos no puede ser vacia");
				$("#creditos_"+i).tooltip({
			        placement : 'bottom',
			        trigger: 'focus'
			    });
				$("#creditos_"+i).tooltip("show");
				valido = false;
				bandera = false;
				i = $("#total").val() + 1;
			}
			else{//validar creditos max y min
				if($("#creditos_"+i).val() < 0){
					$("#creditos_"+i).focus();
					$("#creditos_"+i).attr("data-toggle","tooltip");
					$("#creditos_"+i).attr("data-original-title","La cantidad de creditos no puede ser menor que 0");
					$("#creditos_"+i).tooltip({
				        placement : 'bottom',
				        trigger: 'focus'
				    });
					$("#creditos_"+i).tooltip("show");
					valido = false;
					bandera = false;
					i = $("#total").val() + 1;
				}
				if($("#creditos_"+i).val() > 50){
					$("#creditos_"+i).focus();
					$("#creditos_"+i).attr("data-toggle","tooltip");
					$("#creditos_"+i).attr("data-original-title","La cantidad de creditos no puede ser mayor que 50");
					$("#creditos_"+i).tooltip({
				        placement : 'bottom',
				        trigger: 'focus'
				    });
					$("#creditos_"+i).tooltip("show");
					valido = false;
					bandera = false;
					i = $("#total").val() + 1;
				}
			}
		}
		if(bandera == true){
			$("#creditos_"+i).removeAttr("data-toggle");
			$("#creditos_"+i).removeAttr("data-original-title");
			$("#creditos_"+i).tooltip('destroy');
			if ($("#tipo_"+i).val() == "") {//si el tipo asignatura no está seleccionado
				$("#tipo_"+i).focus();
				$("#tipo_"+i).attr("data-toggle","tooltip");
				$("#tipo_"+i).attr("data-original-title","Debe seleccionar el tipo de asignatura");
				$("#tipo_"+i).tooltip({
			        placement : 'bottom',
			        trigger: 'focus'
			    });
				$("#tipo_"+i).tooltip("show");
				valido = false;
				bandera = false;
				i = $("#total").val() + 1;
			}
		}
		if(bandera == true){
			$("#tipo_"+i).removeAttr("data-toggle");
			$("#tipo_"+i).removeAttr("data-original-title");
			$("#tipo_"+i).tooltip('destroy');
		}
		
		
		/*$("#codigo_"+i).val();
		$("#nombre_"+i).val();
		$("#horas_"+i).val();
		$("#creditos_"+i).val();*/
	}
	return valido;
}


function validar_practica(e, opcion){//valida el formulario practica
	var valido = true;
	bandera = true;
	var regularDireccion = "[A-Za-záéíóúàèìòùÀÈÌÒÙÁÉÍÓÚñÑüÜ ][#][0-9]";//expresion regular de direccion Caracteres + # + numeros
	$(".help-block").remove();
	$(".alert.alert-danger.alert-dismissible").remove();
	$(".glyphicon.glyphicon-remove.form-control-feedback").remove();
	
	if(bandera == true)
	{
		if(!$('#empresa').val())
		{
			$("#empresa").focus();
			$("#empresa").attr("data-toggle","tooltip");
			$("#empresa").attr("data-original-title","Debe seleccionar uno de los valores");
			$("#empresa").tooltip({
		        placement : 'bottom',
		        trigger: 'focus'
		    });
			$("#empresa").parent().addClass("has-error has-feedback");
			$('#empresa').tooltip('show');
			//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
			valido = false;
			bandera = false;
		}
	}
	if(bandera == true)
	{
		$("#empresa").parent().removeClass('has-error');
		$("#empresa").parent().addClass("has-success");
		if(!$('#supervisor').val())
		{
			$("#supervisor").focus();
			$("#supervisor").attr("data-toggle","tooltip");
			$("#supervisor").attr("data-original-title","Debe seleccionar uno de los valores");
			$("#supervisor").tooltip({
		        placement : 'bottom',
		        trigger: 'focus'
		    });
			$("#supervisor").parent().addClass("has-error has-feedback");
			$('#supervisor').tooltip('show');
			//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
			valido = false;
			bandera = false;
		}
	}
	if(bandera == true)
	{
		$("#supervisor").parent().removeClass('has-error');
		$("#supervisor").parent().addClass("has-success");
		if(!$('#profesor').val())
		{
			$("#profesor").focus();
			$("#profesor").attr("data-toggle","tooltip");
			$("#profesor").attr("data-original-title","Debe seleccionar uno de los valores");
			$("#profesor").tooltip({
		        placement : 'bottom',
		        trigger: 'focus'
		    });
			$("#profesor").parent().addClass("has-error has-feedback");
			$('#profesor').tooltip('show');
			//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
			valido = false;
			bandera = false;
		}
	}
	if(bandera == true)
	{
		$("#profesor").parent().removeClass('has-error');
		$("#profesor").parent().addClass("has-success");
		if(!$('#region').val())
		{
			$("#region").focus();
			$("#region").attr("data-toggle","tooltip");
			$("#region").attr("data-original-title","Debe seleccionar uno de los valores");
			$("#region").tooltip({
		        placement : 'bottom',
		        trigger: 'focus'
		    });
			$("#region").parent().addClass("has-error has-feedback");
			$('#region').tooltip('show');
			//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
			valido = false;
			bandera = false;
		}
	}
	if(bandera == true)
	{
		$("#region").parent().removeClass('has-error');
		$("#region").parent().addClass("has-success");
		if(!$('#provincia').val())
		{
			$("#provincia").focus();
			$("#provincia").attr("data-toggle","tooltip");
			$("#provincia").attr("data-original-title","Debe seleccionar uno de los valores");
			$("#provincia").tooltip({
		        placement : 'bottom',
		        trigger: 'focus'
		    });
			$("#provincia").parent().addClass("has-error has-feedback");
			$('#provincia').tooltip('show');
			//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
			valido = false;
			bandera = false;
		}
	}
	if(bandera == true)
	{
		$("#provincia").parent().removeClass('has-error');
		$("#provincia").parent().addClass("has-success");
		if(!$('#comuna').val())
		{
			$("#comuna").focus();
			$("#comuna").attr("data-toggle","tooltip");
			$("#comuna").attr("data-original-title","Debe seleccionar uno de los valores");
			$("#comuna").tooltip({
		        placement : 'bottom',
		        trigger: 'focus'
		    });
			$("#comuna").parent().addClass("has-error has-feedback");
			$('#comuna').tooltip('show');
			//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
			valido = false;
			bandera = false;
		}
	}
	if(bandera == true)
	{
		$("#comuna").parent().removeClass('has-error');
		$("#comuna").parent().addClass("has-success");
		if(!$('#obra').val())
		{
			$("#obra").focus();
			$("#obra").attr("data-toggle","tooltip");
			$("#obra").attr("data-original-title","Debe seleccionar uno de los valores");
			$("#obra").tooltip({
		        placement : 'bottom',
		        trigger: 'focus'
		    });
			$("#obra").parent().addClass("has-error has-feedback");
			$('#obra').tooltip('show');
			//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
			valido = false;
			bandera = false;
		}
	}
	if(bandera == true)
	{
		$("#obra").parent().removeClass('has-error');
		$("#obra").parent().addClass("has-success");
		if(!$('#asignatura').val())
		{
			$("#asignatura").focus();
			$("#asignatura").attr("data-toggle","tooltip");
			$("#asignatura").attr("data-original-title","Debe seleccionar uno de los valores");
			$("#asignatura").tooltip({
		        placement : 'bottom',
		        trigger: 'focus'
		    });
			$("#asignatura").parent().addClass("has-error has-feedback");
			$('#asignatura').tooltip('show');
			//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
			valido = false;
			bandera = false;
		}
	}
	if(bandera == true)
	{
		$("#asignatura").parent().removeClass('has-error');
		$("#asignatura").parent().addClass("has-success");
		if(!$('#plan').val())
		{
			$("#plan").focus();
			$("#plan").attr("data-toggle","tooltip");
			$("#plan").attr("data-original-title","Debe seleccionar uno de los valores");
			$("#plan").tooltip({
		        placement : 'bottom',
		        trigger: 'focus'
		    });
			$("#plan").parent().addClass("has-error has-feedback");
			$('#plan').tooltip('show');
			//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
			valido = false;
			bandera = false;
		}
	}
	if(bandera == true)
	{
		$("#plan").parent().removeClass('has-error');
		$("#plan").parent().addClass("has-success");
		if(!$('#obra_nombre').val())
		{
			$("#obra_nombre").focus();
			$("#obra_nombre").attr("data-toggle","tooltip");
			$("#obra_nombre").attr("data-original-title","El campo no puede est\xe1r Vac\xedo");
			$("#obra_nombre").tooltip({
		        placement : 'bottom',
		        trigger: 'focus'
		    });
			$("#obra_nombre").parent().addClass("has-error has-feedback");
			$('#obra_nombre').tooltip('show');
			//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
			valido = false;
			bandera = false;
		}
	}
	if(bandera == true)
	{
		$("#obra_nombre").parent().removeClass('has-error');
		$("#obra_nombre").parent().addClass("has-success");
		if(!$('#direccion').val())
		{
			$("#direccion").focus();
			$("#direccion").attr("data-toggle","tooltip");
			$("#direccion").attr("data-original-title","El campo no puede est\xe1r Vac\xedo");
			$("#direccion").tooltip({
		        placement : 'bottom',
		        trigger: 'focus'
		    });
			$("#direccion").parent().addClass("has-error has-feedback");
			$('#direccion').tooltip('show');
			//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
			valido = false;
			bandera = false;
		}else{
			if($("#direccion").val().match(regularDireccion)){//si está bien validado
				$("#direccion").removeAttr("data-toggle");
				$("#direccion").removeAttr("data-original-title");
				$("#direccion").tooltip('destroy');
				$("#direccion").parent().parent().removeClass("has-error has-feedback");
				$("#direccion").parent().parent().addClass("has-success has-feedback");
				$("#direccion").parent().append('<span class="glyphicon glyphicon-ok form-control-feedback"></span>');
			}
			else{
				if(valido == true){
					$("#direccion").focus();
				}
				
				$("#direccion").attr("data-toggle","tooltip");
				$("#direccion").attr("data-original-title","No corresponde a una direccion valida Ej: La Calle #889 El Sector");
				$("#direccion").tooltip({
			        placement : 'bottom',
			        //placement : 'right',
			        trigger: 'focus'
			    });
				$('#direccion').tooltip('show');
				$("#direccion").parent().parent().addClass("has-error has-feedback");
				$("#direccion").parent().append('<span class="glyphicon glyphicon-remove form-control-feedback"></span>');
				valido = false;
			}
		}
	}
	if(bandera == true)
	{
		$("#direccion").parent().removeClass('has-error');
		$("#direccion").parent().addClass("has-success");
		if(!$('#obra_nombre').val())
		{
			$("#obra_nombre").focus();
			$("#obra_nombre").attr("data-toggle","tooltip");
			$("#obra_nombre").attr("data-original-title","El campo no puede est\xe1r Vac\xedo");
			$("#obra_nombre").tooltip({
		        placement : 'bottom',
		        trigger: 'focus'
		    });
			$("#obra_nombre").parent().addClass("has-error has-feedback");
			$('#obra_nombre').tooltip('show');
			//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
			valido = false;
			bandera = false;
		}
	}
	if(bandera == true)
	{
		$("#obra_nombre").parent().removeClass('has-error');
		$("#obra_nombre").parent().addClass("has-success");
		if(!$('#tareas').val())
		{
			$("#tareas").focus();
			$("#tareas").attr("data-toggle","tooltip");
			$("#tareas").attr("data-original-title","El campo no puede est\xe1r Vac\xedo");
			$("#tareas").tooltip({
		        placement : 'bottom',
		        trigger: 'focus'
		    });
			$("#tareas").parent().addClass("has-error has-feedback");
			$('#tareas').tooltip('show');
			//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
			valido = false;
			bandera = false;
		}
	}
	if(bandera == true)
	{
		$("#tareas").parent().removeClass('has-error');
		$("#tareas").parent().addClass("has-success");
		$("#inicio").parent().removeClass('has-error');
		$("#inicio").parent().addClass("has-success");
		$("#termino").parent().removeClass('has-error');
		$("#termino").parent().addClass("has-success");
		if(!$('#horas').val())
		{
			$("#horas").focus();
			$("#horas").attr("data-toggle","tooltip");
			$("#horas").attr("data-original-title","El campo no puede est\xe1r Vac\xedo");
			$("#horas").tooltip({
		        placement : 'bottom',
		        trigger: 'focus'
		    });
			$("#horas").parent().addClass("has-error has-feedback");
			$('#horas').tooltip('show');
			//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
			valido = false;
			bandera = false;
		}
	}
	if(opcion == 2)
	{
		if(bandera == true)
		{
			$("#horas").parent().removeClass('has-error');
			$("#horas").parent().addClass("has-success");
			if(!$('#estado').val())
			{
				$("#estado").focus();
				$("#estado").attr("data-toggle","tooltip");
				$("#estado").attr("data-original-title","Debe seleccionar uno de los valores");
				$("#estado").tooltip({
			        placement : 'bottom',
			        trigger: 'focus'
			    });
				$("#estado").parent().addClass("has-error has-feedback");
				$('#estado').tooltip('show');
				//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
				valido = false;
				bandera = false;
			}
		}
	}
	
	if(valido == false){//si existe algun campo invalido		
		return false;
	}
	else{
		if(opcion == 1)
		{
			$("#horas").parent().removeClass('has-error');
			$("#horas").parent().addClass("has-success");
		}
		if(opcion == 2)
		{
			$("#estado").parent().removeClass('has-error');
			$("#estado").parent().addClass("has-success");
		}	
		if(opcion == 3)
		{
			$("#horas").parent().removeClass('has-error');
			$("#horas").parent().addClass("has-success");
		}
		e.preventDefault();
		rotacion_pra(e,opcion);
	}
}
function validar_especialidad(e){//valida el formulario crecion/modificacion especialidad
	var valido = true;
	if(!$('#nombre').val())
	{
		$("#nombre").focus();
		$("#nombre").attr("data-toggle","tooltip");
		$("#nombre").attr("data-original-title","El campo no puede est\xe1r Vac\xedo");
		$("#nombre").tooltip({
	        placement : 'bottom',
	        trigger: 'focus'
	    });
		$("#nombre").parent().addClass("has-error has-feedback");
		$('#nombre').tooltip('show');
		//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
		valido = false;
	}
	if(valido == false){//si existe algun campo invalido		
		return false;
	}
	else{
		$("#nombre").parent().removeClass("has-error");
		$("#nombre").parent().addClass("has-success");
		e.preventDefault();
		$("#toggleCSS").attr("href", "css/alertify.bootstrap.css");
		alertify.confirm("\xbfEst\xe1 seguro que los datos ingresados son correctos?", function (e2) {
			if (e2) {
				alertify.success("Procesando Datos de Nueva Especialidad...");
				$("#formulario_especialidad").submit();//envia el formulario
			} else {
				alertify.error("Se cancel\xf3 el registro.");
				e.preventDefault();
			}
		}, "Default Value");
		
	}
}
function validar_especialidad_modificar(e){//valida el formulario crecion/modificacion especialidad
	var valido = true;
	if(!$('#nombre_m').val())
	{
		$("#nombre_m").focus();
		$("#nombre_m").attr("data-toggle","tooltip");
		$("#nombre_m").attr("data-original-title","El campo no puede est\xe1r Vac\xedo");
		$("#nombre_m").tooltip({
	        placement : 'bottom',
	        trigger: 'focus'
	    });
		$("#nombre_m").parent().addClass("has-error has-feedback");
		$('#nombre_m').tooltip('show');
		//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
		valido = false;
	}
	if(valido == false){//si existe algun campo invalido		
		return false;
	}
	else{
		$("#nombre_m").parent().removeClass("has-error");
		$("#nombre_m").parent().addClass("has-success");
		e.preventDefault();
		$("#toggleCSS").attr("href", "css/alertify.bootstrap.css");
		alertify.confirm("\xbfEst\xe1 seguro que desea modificar los datos de la Especialidad?", function (e2) {
			if (e2) {
				alertify.success("Procesando Nuevos Datos de Especialidad...");
				$("#formulario_especialidad_modif").submit();//envia el formulario
			} else {
				alertify.error("Se cancel\xf3 la Modificaci\xf3n.");
			}
		}, "Default Value");
	}
}

function rotacion_pra(e,opcion)
{
	$("#toggleCSS").attr("href", "css/alertify.bootstrap.css");
	if(opcion == 1)
	{
		alertify.confirm("\xbfEst\xe1 seguro que desea aprobar esta pr\xe1ctica?", function (e2) {
			if (e2) {
				alertify.success("Procesando Datos de Pr\xe1ctiva Aprobada...");
				$("#accion").val('aprobar');
				$("#formulario_practica").submit();//envia el formulario
			} else {
				alertify.error("Se cancel\xf3 el registro.");
				e.preventDefault();
			}
		}, "Default Value");
	}
	if(opcion == 2)
	{
		alertify.confirm("\xbfEst\xe1 seguro que desea modificar los datos de la práctica?", function (e2) {
			if (e2) {
				alertify.success("Procesando Nuevos Datos de Práctica...");
				$("#formulario_practica").submit();//envia el formulario
			} else {
				alertify.error("Se cancel\xf3 la Modificaci\xf3n.");
				e.preventDefault();
			}
		}, "Default Value");
	}
	if(opcion == 3)
	{
		alertify.confirm("\xbfEst\xe1 seguro que desea rechazar esta pr\xe1ctica?", function (e2) {
			if (e2) {
				alertify.success("Procesando Rechazo de Práctica...");
				$("#accion").val('rechazar');
				$("#formulario_practica").submit();//envia el formulario
			} else {
				alertify.error("Se cancel\xf3 el rechazo.");
				e.preventDefault();
			}
		}, "Default Value");
	}
	
}
function rotacion_aca(e,opcion)
{
	$("#toggleCSS").attr("href", "css/alertify.bootstrap.css");
	if(opcion == 1)
	{
		alertify.confirm("\xbfEst\xe1 seguro que los datos ingresados son correctos?", function (e2) {
			if (e2) {
				alertify.success("Procesando Datos de Nuevo Académico...");
				$("#formulario_academico").submit();//envia el formulario
			} else {
				alertify.error("Se cancel\xf3 el registro.");
				e.preventDefault();
			}
		}, "Default Value");
	}
	if(opcion == 2)
	{
		alertify.confirm("\xbfEst\xe1 seguro que desea modificar los datos del Académico?", function (e2) {
			if (e2) {
				alertify.success("Procesando Nuevos Datos de Académico...");
				$("#formulario_academico_modif").submit();//envia el formulario
			} else {
				alertify.error("Se cancel\xf3 la Modificaci\xf3n.");
				e.preventDefault();
			}
		}, "Default Value");
	}
	
}
function rotacion_adm(e,opcion)
{
	$("#toggleCSS").attr("href", "css/alertify.bootstrap.css");
	if(opcion == 1)
	{
		alertify.confirm("\xbfEst\xe1 seguro que los datos ingresados son correctos?", function (e2) {
			if (e2) {
				alertify.success("Procesando Datos de Nuevo Administrador...");
				$("#formulario_admin").submit();//envia el formulario
			} else {
				alertify.error("Se cancel\xf3 el registro.");
				e.preventDefault();
			}
		}, "Default Value");
	}
	if(opcion == 2)
	{
		alertify.confirm("\xbfEst\xe1 seguro que desea modificar los datos del Administrador?", function (e2) {
			if (e2) {
				alertify.success("Procesando Nuevos Datos de Administrador...");
				$("#formulario_admin_modif").submit();//envia el formulario
			} else {
				alertify.error("Se cancel\xf3 la Modificaci\xf3n.");
				e.preventDefault();
			}
		}, "Default Value");
	}
	if(opcion == 3)
	{
		alertify.confirm("\xbfEst\xe1 seguro que desea modificar sus datos personales?", function (e2) {
			if (e2) {
				alertify.success("Procesando Nuevos Datos Personales...");
				$("#formulario_admin").submit();//envia el formulario
			} else {
				alertify.error("Se cancel\xf3 la Modificaci\xf3n.");
				e.preventDefault();
			}
		}, "Default Value");
	}
}
function rotacion_sup(e,opcion)
{
	$("#toggleCSS").attr("href", "css/alertify.bootstrap.css");
	if(opcion == 1)
	{
		alertify.confirm("\xbfEst\xe1 seguro que los datos ingresados son correctos?", function (e2) {
			if (e2) {
				alertify.success("Procesando Datos de Nuevo Supervisor...");
				$("#formulario_supervisor").submit();//envia el formulario
			} else {
				alertify.error("Se cancel\xf3 el registro.");
				e.preventDefault();
			}
		}, "Default Value");
	}
	if(opcion == 2)
	{
		alertify.confirm("\xbfEst\xe1 seguro que desea modificar los datos del Supervisor?", function (e2) {
			if (e2) {
				alertify.success("Procesando Nuevos Datos de Supervisor...");
				$("#formulario_supervisor_modif").submit();//envia el formulario
			} else {
				alertify.error("Se cancel\xf3 la Modificaci\xf3n.");
				e.preventDefault();
			}
		}, "Default Value");
	}
	
}
function rotacion_emp(e,opcion)
{
	$("#toggleCSS").attr("href", "css/alertify.bootstrap.css");
	if(opcion == 1)
	{
		alertify.confirm("\xbfEst\xe1 seguro que los datos ingresados son correctos?", function (e2) {
			if (e2) {
				alertify.success("Procesando Datos de Nueva Empresa...");
				$("#formulario_empresa").submit();//envia el formulario
			} else {
				alertify.error("Se cancel\xf3 el registro.");
				e.preventDefault();
			}
		}, "Default Value");
	}
	if(opcion == 2)
	{
		alertify.confirm("\xbfEst\xe1 seguro que desea modificar los datos de la Empresa?", function (e2) {
			if (e2) {
				alertify.success("Procesando Nuevos Datos de Empresa...");
				$("#formulario_empresa_modif").submit();//envia el formulario
			} else {
				alertify.error("Se cancel\xf3 la Modificaci\xf3n.");
				e.preventDefault();
			}
		}, "Default Value");
	}
	
}

function rotacion_clave(e,opcion)
{
	$("#toggleCSS").attr("href", "css/alertify.bootstrap.css");
	if(opcion == 1)
	{
		alertify.confirm("\xbfEst\xe1 seguro que desea cambiar su clave de acceso?", function (e2) {
			if (e2) {
				alertify.success("Procesando Nueva Clave...");
				$("#formulario_clave").submit();//envia el formulario
			} else {
				alertify.error("Se cancel\xf3 el Proceso.");
				e.preventDefault();
			}
		}, "Default Value");
	}
}
function rotacion_area(e,opcion)
{
	$("#toggleCSS").attr("href", "css/alertify.bootstrap.css");
	if(opcion == 1)
	{
		alertify.confirm("\xbfEst\xe1 seguro que Registrar la Nueva Area?", function (e2) {
			if (e2) {
				alertify.success("Procesando Nueva Area...");
				$("#formulario_area").submit();//envia el formulario
			} else {
				alertify.error("Se cancel\xf3 el Proceso.");
				e.preventDefault();
			}
		}, "Default Value");
	}
	if(opcion == 2)
	{
		alertify.confirm("\xbfEst\xe1 seguro que Desea Modificar el Area?", function (e2) {
			if (e2) {
				alertify.success("Procesando Datos de Area...");
				$("#formulario_area_modif").submit();//envia el formulario
			} else {
				alertify.error("Se cancel\xf3 el Proceso.");
				e.preventDefault();
			}
		}, "Default Value");
	}
}


function actualizarListaPracticas(){//cuando cambia la selección de carreras en el mantenedor de rubricas, acualiza las asignaturas de tipo practica
	var x;
	x = $("#carrera");
	$.ajax({
		async: true,
		type: "get",
		dataType: "html",
		contentType: "aplication/x-www-form-urlencoded",
		url:"AjaxControlador",
		data: {carrera_rubrica : x.val()},
		success: function(datos){
			$("#div_asignaturas").empty();
			$("#div_asignaturas").html(''+datos);
			$("#carrera").change(actualizarListaRoles);
		}
	});
	return false;
}

function actualizarListaRoles(){//cuando cambia la selección de asignaturas en el mantenedor de rubricas, actualiza el select (lista) de roles
	var x, y;
	if(!$('#asignatura').val()){
		
	}
	else{
		x = $("#asignatura").val();
		y = x.split("-");
		$.ajax({
			async: true,
			type: "get",
			dataType: "html",
			contentType: "aplication/x-www-form-urlencoded",
			url:"AjaxControlador",
			data: {asignatura_gestion : y[0], plan_gestion : y[1]},
			success: function(datos){
				$("#div_roles").empty();
				$("#div_roles").html(''+datos);
			}
		});
	}
	
	return false;
}

function buscarAsignaturasAlumno(){//busca las asignaturas inscritas registradas en el sistema
	var x;
	if(!$('#rut').val()){
		
	}
	else{
		x = $("#rut").val();
		$.ajax({
			async: true,
			type: "get",
			dataType: "html",
			contentType: "aplication/x-www-form-urlencoded",
			url:"AjaxControlador",
			data: {rut_asignaturas_registradas : x},
			success: function(datos){
				$("#div_asignaturas").empty();
				$("#div_asignaturas").html(''+datos);
			}
		});
	}
	
	return false;
}

function rotacion_funcion(e,opcion)
{
	$("#toggleCSS").attr("href", "css/alertify.bootstrap.css");
	if(opcion == 1)
	{
		alertify.confirm("\xbfEst\xe1 seguro que Registrar la Nueva Función?", function (e2) {
			if (e2) {
				alertify.success("Procesando Nueva Función...");
				$("#formulario_funcion").submit();//envia el formulario
			} else {
				alertify.error("Se cancel\xf3 el Proceso.");
				e.preventDefault();
			}
		}, "Default Value");
	}
	if(opcion == 2)
	{
		alertify.confirm("\xbfEst\xe1 seguro que Desea Modificar la Función?", function (e2) {
			if (e2) {
				alertify.success("Procesando Datos de Función...");
				$("#formulario_funcion_modif").submit();//envia el formulario
			} else {
				alertify.error("Se cancel\xf3 el Proceso.");
				e.preventDefault();
			}
		}, "Default Value");
	}
}