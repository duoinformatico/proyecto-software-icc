function confirmarEmpresa(rut)
{
		var respuesta = confirm('�Est� seguro que desea eliminar  '+rut+'?');
		if(respuesta)
		{
			location.href='AccionesEmpresa?rut='+rut;
		}
}
function confirmarAdmin(rut)
{
		var respuesta = confirm('�Est� seguro que desea eliminar  '+rut+'?');
		if(respuesta)
		{
			location.href='AccionesAdministrador?rut='+rut;
		}
}
function confirmarAcademico(rut)
{
		var respuesta = confirm('�Est� seguro que desea eliminar  '+rut+'?');
		if(respuesta)
		{
			location.href='AccionesAcademico?rut='+rut;
		}
}
function redireccionar(id)
{
		location.href='pendienteServlet.do?id='+id;
}
function redireccionar2(id)
{
		location.href='practicaServlet.do?id='+id;
}
function redireccionar_bitacora(id)
{
		location.href='BitacoraServlet.do?id='+id;
}
$(document).ready(function(){	
	$("#region").change(seleccionRegion);
	$("#provincia").change(seleccionProvincia);
	$("#empresa").change(seleccionEmpresa);
	
	function seleccionRegion(){
		var x;
		x = $("#region");
		$.ajax({
			async: true,
			type: "get",
			dataType: "html",
			contentType: "aplication/x-www-form-urlencoded",
			url:"AjaxControlador",
			data: {region : x.val()},
			success: function(datos){
				$("#div_provincia").empty();
				$("#div_provincia").html(''+datos);
				$("#provincia").change(seleccionProvincia);
			}
		});
		return false;		
	}
	
	function seleccionProvincia(){//actualiza el select de comunas segun la provincia que se selecciona
		var x;
		x = $("#provincia");
		$.ajax({
			async: true,
			type: "get",
			dataType: "html",
			contentType: "aplication/x-www-form-urlencoded",
			url:"AjaxControlador",
			data: {provincia : x.val()},
			success: function(datos){				
				$("#div_comuna").empty();
				$("#div_comuna").html(''+datos);	
			}
		});
		return false;		
	}
	
	function seleccionEmpresa(){//actualiza el select de supervisores segun la empresa que se selecciona
		var x;
		x = $("#empresa");
		$.ajax({
			async: true,
			type: "get",
			dataType: "html",
			contentType: "aplication/x-www-form-urlencoded",
			url:"AjaxControlador",
			data: {empresa : x.val()},
			success: function(datos){				
				$("#div_supervisor").empty();
				$("#div_supervisor").html(''+datos);	
			}
		});
		return false;		
	}


});
