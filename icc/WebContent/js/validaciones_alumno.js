function confirmar(rut)
{
		var respuesta = confirm('¿Está seguro que desea eliminar  '+rut+'?');
		if(respuesta)
		{
			location.href='AccionesEmpresaControlador?rut='+rut;
		}
}
function redireccionar(id)
{
		location.href='FormularioPendientesControlador?id='+id;
}
function redireccionar_bitacora(id)
{

		location.href='BitacoraServlet.do?id='+id;
}

$(document).ready(function(){	
	$("#region").change(seleccionRegion);
	$("#provincia").change(seleccionProvincia);
	$("#empresa").change(seleccionEmpresa);
	$("#region_2").change(seleccionRegion_2);
	$("#provincia_2").change(seleccionProvincia_2);

	function seleccionRegion(){
		var x;
		x = $("#region");
		$.ajax({
			async: true,
			type: "get",
			dataType: "html",
			contentType: "aplication/x-www-form-urlencoded",
			url:"AjaxControlador",
			data: {region : x.val()},
			success: function(datos){
				$("#div_provincia").empty();
				$("#div_provincia").html(''+datos);
				$("#provincia").change(seleccionProvincia);
			}
		});
		return false;		
	}
	
	function seleccionProvincia(){//actualiza el select de comunas segun la provincia que se selecciona
		var x;
		x = $("#provincia");
		$.ajax({
			async: true,
			type: "get",
			dataType: "html",
			contentType: "aplication/x-www-form-urlencoded",
			url:"AjaxControlador",
			data: {provincia : x.val()},
			success: function(datos){				
				$("#div_comuna").empty();
				$("#div_comuna").html(''+datos);	
			}
		});
		return false;		
	}
	
	function seleccionEmpresa(){//actualiza el select de supervisores segun la empresa que se selecciona
		var x;
		x = $("#empresa");
		$.ajax({
			async: true,
			type: "get",
			dataType: "html",
			contentType: "aplication/x-www-form-urlencoded",
			url:"AjaxControlador",
			data: {empresa : x.val()},
			success: function(datos){				
				$("#div_supervisor").empty();
				$("#div_supervisor").html(''+datos);	
			}
		});
		return false;		
	}


});

function seleccionRegion_2(){
	var x;
	x = $("#region_2");
	$.ajax({
		async: true,
		type: "get",
		dataType: "html",
		contentType: "aplication/x-www-form-urlencoded",
		url:"AjaxControlador",
		data: {region_2 : x.val()},
		success: function(datos){
			$("#div_provincia_2").empty();
			$("#div_provincia_2").html(''+datos);
			$("#provincia_2").change(seleccionProvincia);
		}
	});
	return false;		
}

function seleccionProvincia_2(){//actualiza el select de comunas segun la provincia que se selecciona
	var x;
	x = $("#provincia_2");
	$.ajax({
		async: true,
		type: "get",
		dataType: "html",
		contentType: "aplication/x-www-form-urlencoded",
		url:"AjaxControlador",
		data: {provincia_2 : x.val()},
		success: function(datos){				
			$("#div_comuna_2").empty();
			$("#div_comuna_2").html(''+datos);	
		}
	});
	return false;		
}

function actualizarBotonEspecialidades()//actualiza el boton de especialidades de los academicos
{
	//alert("RUT Academico= "+$("#profesor").val());
	$.ajax({
		async: true,
		type: "get",
		dataType: "html",
		contentType: "aplication/x-www-form-urlencoded",
		url:"EspecialidadesControlador",
		data: {aca_rut_boton : $("#profesor").val()},
		success: function(datos){
			$("#div_boton_especialidades").empty();
			$("#div_boton_especialidades").html(''+datos);
		}
	});
	return false;
}

function mostrarEmpresas()//redirecciona a la vista de todas las empresas registradas para los alumnos
{
	$.ajax({
		async: true,
		type: "get",
		dataType: "html",
		contentType: "aplication/x-www-form-urlencoded",
		url:"AjaxControlador",
		data: {empresas_alumnos : 1},
		success: function(datos){
			$("#div_empresas").empty();
			$("#div_empresas").html(''+datos);
			$("#myModal_empresas").modal('show');
		}
	});
	return false;
}
function verEspecialidades(aca_rut)//redirecciona para las especialidades de un academico
{
	$.ajax({
		async: true,
		type: "get",
		dataType: "html",
		contentType: "aplication/x-www-form-urlencoded",
		url:"EspecialidadesControlador",
		data: {aca_rut : aca_rut},
		success: function(datos){
			$("#div_especialidades").empty();
			$("#div_especialidades").html(''+datos);
			$("#myModal").modal('show');
		}
	});
	return false;
}

function verBitacora(bit_id)//redirecciona para ver una bitacora en formulario de edición
{
	$.ajax({
		async: true,
		type: "get",
		dataType: "html",
		contentType: "aplication/x-www-form-urlencoded",
		url:"AjaxControlador",
		data: {bit_id : bit_id},
		success: function(datos){
			$("#div_bitacora").empty();
			$("#div_bitacora").html(''+datos);
			$("#myModal").modal('show');
		}
	});
	return false;
}

function subirInforme(pra_id)//redirecciona para subir un informe
{
	$.ajax({
		async: true,
		type: "get",
		dataType: "html",
		contentType: "aplication/x-www-form-urlencoded",
		url:"AjaxControlador",
		data: {pra_id_informe : pra_id},
		success: function(datos){
			$("#div_informe").empty();
			$("#div_informe").html(''+datos);
			$("#myModalInforme").modal('show');
			
		}
	});
	return false;
}


function consultarBitacora(bit_id,plan_id)//redirecciona para ver una bitacora en formulario , pero sin poder editarla
{
	$.ajax({
		async: true,
		type: "get",
		dataType: "html",
		contentType: "aplication/x-www-form-urlencoded",
		url:"formularioBitacoraServlet.do",
		data: {bit_id : bit_id,plan_id : plan_id},
		success: function(datos){
			$("#div_consultar").empty();
			$("#div_consultar").html(''+datos);
			$("#myModal_consulta").modal('show');
			
		}
	});
	return false;
}

function agregarImagen()//agrega un input para ingreso de archivo de imagenes
{
	
	if($("#actual_imagenes").val() == $("#num_imagenes").val()){
		alert("No puede agregar mas Im\xe1genes");
	}
	else{
		for(var i = 0; i < $("#num_imagenes").val(); i++){
			if($("#actual_imagenes").val() == i){
				//$("#asignatura_"+(i+1)).css("display", "block");				
				$("#actual_imagenes").val(i+1);
				
				
				var n_imagenes = $("#actual_imagenes").val();
				$.ajax({
					async: true,
					type: "get",
					dataType: "html",
					contentType: "aplication/x-www-form-urlencoded",
					url:"AjaxControlador",
					data: {n_imagenes : n_imagenes},
					success: function(datos){
						$("#div_imagen_"+(i+1)).html(''+datos);						
					}
				});
				break;
			}
			
		}
	}
}
function quitarImagen()//quita un input de imagen
{
	//alert("Quitando Asignaturas\nActual Asignaturas "+$("#actual_asignaturas").val());
	if($("#actual_imagenes").val() == 0){
		//alert("No puede quitar mas");		
	}
	else{
		for(var i = 1; i <= $("#num_imagenes").val(); i++){
			if($("#actual_imagenes").val() == i){				
				//$("#asignatura_"+i).css("display", "none");
				//$("#asignatura_"+i).val("000000");
				$("#actual_imagenes").val(i-1);
				if(i != $("#num_imagenes").val()){
					$("#actual_imagenes").val(i-1);
				}
				//alert("Quitando Asignaturas\nActual Asignaturas "+$("#actual_asignaturas").val());
				$("#div_imagen_"+(i)).empty();
				break;
			}
			
		}
	}
}

function verRubrica(ins_id, rut, pra_id, rol_id)//redirecciona para ver un instrumento de evaluacion
{
	$.ajax({
		async: true,
		type: "get",
		dataType: "html",
		contentType: "aplication/x-www-form-urlencoded",
		url:"AjaxControlador",
		data: {ins_id : ins_id, rut_evaluador : rut, practica_id : pra_id, rol_id : rol_id },
		success: function(datos){
			$("#div_instrumento").empty();
			$("#div_instrumento").html(''+datos);
			$("#myModal").modal('show');
			
		}
	});
	return false;
}

function verDetalleEvaluacion(tipo, rut, pra_id, rol_id, eva_fecha)//redirecciona para ver el detalle de una evaluacion
{	//tipo: corresponde si es evaluacion de 'alumno', 'supervisor' o 'academico'
	//rut: rut del evaluador
	//pra_id: codigo de la práctica que fue evaluada
	//rol_id: rol con que el evaluador evaluó la practica, sirve para determinar con que rubrica evaluó
	//eva_fecha: sirve para determinar con que rubrica evaluó
	$.ajax({
		async: true,
		type: "get",
		dataType: "html",
		contentType: "aplication/x-www-form-urlencoded",
		url:"AjaxControlador",
		data: {tipo_eval : tipo, evaluador_rut : rut, id_practica : pra_id, id_rol : rol_id, eva_fecha : eva_fecha },
		success: function(datos){
			$("#div_instrumento").empty();
			$("#div_instrumento").html(''+datos);
			$("#myModal").modal('show');
			
		}
	});
	return false;
}

function calcularPuntaje(nombre_items){		
	var suma = 0;
	var nombre_items = $("#nombre_items").val();
	var items = nombre_items.split("-");
	//alert("Cantidad Items Rubrica = "+items.length+"\nElementos"+items);
	for(var i=0;i <= items.length; i++){
		//alert(items.length);
		 if( $("input[name='"+items[i]+"']").is(':checked')) { 
			 suma = suma + parseInt($("input[name='"+items[i]+"']:checked").val());
			 //alert($("input[name='"+items[i]+"']:checked").val());
		 }			
		
	}
	//alert(suma);
	var x1 = 4.0;//nota minima de aprobación
	var x2 = 7.0;//nota maxima
	var Y = (suma/parseInt($("#max_puntaje").val()))*100;
	var X = (x2 - x1 )*((Y - 60)/(100 - 60)) + x1;
	//alert("Nota = "+X.toFixed(2));
	//var nota = 6/36;
	
	$("input:text[name=puntaje]").val(suma);
	if(X < 1){
		//$("input:hidden[name=nota]").val(1.0);
		$("#nota").val(1.0);
	}
	else{
		//$("input:hidden[name=nota]").val(X.toFixed(2));
		$("#nota").val(X.toFixed(2));
	}
}

function funcionMaestra(e,eleccion,numero) 
{
	//el numero 1 es para cuando se necesita que no se ingresen puntos y otros caracteres
	if(numero == '1')
	{
		bool1 = especial_character(e);
	}
	//el numero 2 es para cuando se necesita que no se ingresen  otros caracteres, pero si puntos
	if(numero == '2')
	{
		bool1 = especial_character2(e);
	}
	if(bool1 == false)
	{
		return bool1;
	}
	bool2 = solo_letras(e,eleccion);
	return bool2;
}
	
	// evita insercion de algunos caracteres
function especial_character(e)
{
    var key = e.charCode ? e.charCode : e.keyCode;
    if (key == 46 || key == 145 || key == 146 || key == 39 || key == 37 || key == 33 || key == 161)
    {
        return false;
    }    
}
//evita insercion de caracteres, pero si deja insertar puntos
function especial_character2(e)
{
    var key = e.charCode ? e.charCode : e.keyCode;
    if (key == 145 || key == 146 || key == 39 || key == 37 || key == 33 || key == 161)
    {
        return false;
    }    
}

function solo_letras(elEvento,eleccion) 
{
	  // Variables que definen los caracteres permitidos
	  var permitidos = eleccion;
	  var numeros = "0123456789";
	  //var caracteres = "";
	  //
	  var caracteres = " abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZáéíóúÁÉÍÓÚ#/.@:";
	  var email = " abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ@_-";
	  var nombres = " abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZáéíóúÁÉÍÓÚ";
	  var direccion = " abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZáéíóúÁÉÍÓÚ#-.";
	  var descripcion = " abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZáéíóúÁÉÍÓÚ-,";
	  var web = " abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZáéíóúÁÉÍÓÚ:/_-.";
	  var tele_fijo = "-";
	  var celular = "*+#";
	  var rut = "kK";
	  var numeros_caracteres = numeros + caracteres;
	  var email_num = numeros + email;
	  var teclas_especiales = [8, 9, 37, 39, 46];
	  var clave = "abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ";
	  // 9 = Tab, 8 = BackSpace, 46 = Supr, 37 = flecha izquierda, 39 = flecha derecha			 
	 
	  // Seleccionar los caracteres a partir del parámetro de la función
	  switch(permitidos) {
		 case 'num':
			permitidos = numeros;
			break;
		 case 'car':
			permitidos = caracteres;
			break;
		 case 'num_car':
			permitidos = numeros_caracteres;
			break;
		 case 'nombres':
			permitidos = nombres;
			break;
		 case 'email':
			permitidos = email_num;
			break;
		 case 'dire':
			permitidos = direccion+ numeros;
			break;
		 case 'descri':
			permitidos = descripcion+ numeros;
			break;
		 case 'fijo':
			permitidos = tele_fijo+ numeros;
			break;
		 case 'celular':
			permitidos = celular+ numeros;
			break;
		 case 'rut':
				permitidos = rut + numeros;
				break;
		 case 'web':
			permitidos = web + numeros;
			break;
		 case 'clave':
				permitidos = clave + numeros;
				break;
	  }
		 
	  // Obtener la tecla pulsada 
	  var evento = elEvento || window.event;
	  var codigoCaracter = evento.charCode || evento.keyCode;
	  var caracter = String.fromCharCode(codigoCaracter);
	 
	  // Comprobar si la tecla pulsada es alguna de las teclas especiales
	  // (teclas de borrado y flechas horizontales)
	  var tecla_especial = false;
	  for(var i in teclas_especiales) {
		 if(codigoCaracter == teclas_especiales[i]) {
			tecla_especial = true;
			break;
		 }
	  }
	 
	  // Comprobar si la tecla pulsada se encuentra en los caracteres permitidos
	  // o si es una tecla especial
	  return permitidos.indexOf(caracter) != -1 || tecla_especial;
		/* Sólo números agregar / alfinal del input
		<input type="text" id="texto" onkeypress="return permite(event, 'num')" >

		// Sólo letras
		<input type="text" id="texto" onkeypress="return permite(event, 'car')" >

		// Sólo letras o números
		<input type="text" id="texto" onkeypress="return permite(event, 'num_car')" >*/
}

function permite(elEvento) {
	  // Variables que definen los caracteres permitidos
	  var permitidos = "num_car";
	  var numeros = "0123456789";
	  //var caracteres = "";
	  var caracteres = " abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ#/.@:";
	  var numeros_caracteres = numeros + caracteres;
	  var teclas_especiales = [8, 9, 37, 39, 46];
	  // 9 = Tab, 8 = BackSpace, 46 = Supr, 37 = flecha izquierda, 39 = flecha derecha			 
	 
	  // Seleccionar los caracteres a partir del parámetro de la función
	  switch(permitidos) {
		 case 'num':
			permitidos = numeros;
			break;
		 case 'car':
			permitidos = caracteres;
			break;
		 case 'num_car':
			permitidos = numeros_caracteres;
			break;
	  }
	 
	  // Obtener la tecla pulsada 
	  var evento = elEvento || window.event;
	  var codigoCaracter = evento.charCode || evento.keyCode;
	  var caracter = String.fromCharCode(codigoCaracter);
	 
	  // Comprobar si la tecla pulsada es alguna de las teclas especiales
	  // (teclas de borrado y flechas horizontales)
	  var tecla_especial = false;
	  for(var i in teclas_especiales) {
		 if(codigoCaracter == teclas_especiales[i]) {
			tecla_especial = true;
			break;
		 }
	  }
	 
	  // Comprobar si la tecla pulsada se encuentra en los caracteres permitidos
	  // o si es una tecla especial
	  return permitidos.indexOf(caracter) != -1 || tecla_especial;
		/* Sólo números agregar / alfinal del input
		<input type="text" id="texto" onkeypress="return permite(event, 'num')" >

		// Sólo letras
		<input type="text" id="texto" onkeypress="return permite(event, 'car')" >

		// Sólo letras o números
		<input type="text" id="texto" onkeypress="return permite(event, 'num_car')" >*/
}

function validar_preinscripcion(){
	//alert("Validando Formulario 1");
	var valido = true;
	//var regularDireccion = "(/^[a-zA-ZáéíóúàèìòùÀÈÌÒÙÁÉÍÓÚñÑüÜ]/)/";//expresion regular de direccion Caracteres + # + numeros
	var regularDireccion = "[A-Za-záéíóúàèìòùÀÈÌÒÙÁÉÍÓÚñÑüÜ ][#][0-9]";//expresion regular de direccion Caracteres + # + numeros
	//valida la expresion regular de una dirección
	
	$(".glyphicon.glyphicon-remove.form-control-feedback").remove();
	
	if($("#direccion").val().match(regularDireccion)){//si está bien validado
		$("#direccion").removeAttr("data-toggle");
		$("#direccion").removeAttr("data-original-title");
		$("#direccion").tooltip('destroy');
		$("#direccion").parent().parent().removeClass("has-error has-feedback");
		$("#direccion").parent().parent().addClass("has-success has-feedback");
		$("#direccion").parent().append('<span class="glyphicon glyphicon-ok form-control-feedback"></span>');
	}
	else{
		if(valido == true){
			$("#direccion").focus();
		}
		
		$("#direccion").attr("data-toggle","tooltip");
		$("#direccion").attr("data-original-title","No corresponde a una direccion v\xe1lida Ej: La Calle #889");
		$("#direccion").tooltip({
	        placement : 'bottom',
	        //placement : 'right',
	        trigger: 'focus'
	    });
		$('#direccion').tooltip('show');
		$("#direccion").parent().parent().addClass("has-error has-feedback");
		$("#direccion").parent().append('<span class="glyphicon glyphicon-remove form-control-feedback"></span>');
		valido = false;
	}
	
	//###############Validacion del campo fecha inicio
	if($("#inicio").val() != ""){//si está bien validado la cantidad de horas hora
		$("#inicio").removeAttr("data-toggle");
		$("#inicio").removeAttr("data-original-title");
		$("#inicio").tooltip('destroy');
		$("#inicio").parent().parent().removeClass("has-error has-feedback");
	}
	else{
		if(valido == true){
			$("#inicio").focus();
		}		
		$("#inicio").attr("data-toggle","tooltip");
		$("#inicio").attr("data-original-title","La fecha de inicio de pr\xe1ctica no puede estar vac\xcda");
		$("#inicio").tooltip({
	        placement : 'bottom',
	        trigger: 'focus'
	    });
		//$('#inicio').tooltip('show');
		$("#inicio").parent().parent().addClass("has-error has-feedback");
		valido = false;
	}
	
	//###############Validacion del campo fecha termino
	if($("#termino").val() != ""){//si está bien validado la cantidad de horas hora
		$("#termino").removeAttr("data-toggle");
		$("#termino").removeAttr("data-original-title");
		$("#termino").tooltip('destroy');
		$("#termino").parent().parent().removeClass("has-error has-feedback");
	}
	else{
		if(valido == true){
			$("#termino").focus();
		}		
		$("#termino").attr("data-toggle","tooltip");
		$("#termino").attr("data-original-title","La fecha de t\xe9rmino de pr\xe1ctica no puede estar vacia");
		$("#termino").tooltip({
	        placement : 'bottom',
	        trigger: 'focus'
	    });
		//$('#termino').tooltip('show');
		$("#termino").parent().parent().addClass("has-error has-feedback");
		valido = false;
	}
	
	//###################Validacion del campo de horas
	if($("#horas").val() <= 1000 && $("#horas").val() >= 100){//si está bien validado la cantidad de horas
		$("#horas").removeAttr("data-toggle");
		$("#horas").removeAttr("data-original-title");
		$("#horas").tooltip('destroy');	
		$("#horas").parent().parent().removeClass("has-error has-feedback");
	}
	else{
		if(valido == true){
			$("#horas").focus();
		}
		$("#horas").attr("data-placement","bottom");
		$("#horas").attr("data-toggle","tooltip");
		$("#horas").attr("data-original-title","M\xednimo: 100 horas | M\xe1ximo: 1000 horas");
		$("#horas").tooltip({
	        placement : 'bottom',
	        //placement : 'right',
	        trigger: 'focus'
	    });
		//$('#horas').tooltip('show');
		//alert($("#horas").parent());
		$("#horas").parent().parent().addClass("has-error has-feedback");
		$("#horas").parent().append('<span class="glyphicon glyphicon-remove form-control-feedback"></span>');
		valido = false;
	}	
	
	/*
	if($("#nombre_obra").val().length == 0 ){
		//alert("El campo nombre de la obra esta vacio nuevo1");
		$("#nombre_obra").focus();
		$("#nombre_obra").attr("data-toggle","tooltip");
		$("#nombre_obra").attr("data-original-title","El nombre de la obra no puede ser vacio");
		$("#nombre_obra").tooltip({
	        placement : 'bottom',
	        trigger: 'focus'
	    });
		$('#nombre_obra').tooltip('show');
		valido = false;
	}
	else{
		$("#nombre_obra").removeAttr("data-toggle","tooltip");
		$("#nombre_obra").removeAttr("data-original-title","El nombre de la obra no puede ser vacio");
		$('#nombre_obra').tooltip('destroy');
	}*/
	
	if(valido == false){//si existe algun campo invalido
		return valido;
	}
	else{
		return true;
		$("#preinscripcion").submit();//envia el formulario
	}
}

function validar_evaluacion(){//valida el formulario de evaluacion(autoevaluacion para el alumno)
	var valido = true;
	$(".help-block").remove();
	$(".alert.alert-danger.alert-dismissible").remove();
	
	
	if(!$('#area').val()){//si la asignacion de areas que trabajó el alumno está vacia
		//alert("Debe seleccionar las areas en que trabajo");
		$("#ms-area").parent().append(
				'<div class="alert alert-danger alert-dismissible" role="alert">'
				  +'<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'
				  +'<span class="glyphicon glyphicon-remove form-control-feedback"></span><strong>Error!</strong> Debe seleccionar las areas en que trabajo durante la Practica.'
				+'</div>'
		  );
		valido = false;
	}
	else{
		if($("#area").val().length >= 2){
			
		}
		else{
			if(valido == true){
				$("#area").focus();
				
			}
			$("#ms-area").parent().append(
					'<div class="alert alert-danger alert-dismissible" role="alert">'
					  +'<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'
					  +'<span class="glyphicon glyphicon-remove form-control-feedback"></span><strong>Error!</strong> Debe seleccionar el minimo de areas en que trabajo.'
					+'</div>'
			  );
			
			valido = false;
		}
	}
	
	if(!$('#funcion').val()){//si la asignacion de funcions que trabajó el alumno está vacia
		//alert("Debe seleccionar las funcions en que trabajo");
		$("#ms-funcion").parent().append(
				'<div class="alert alert-danger alert-dismissible" role="alert">'
				  +'<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'
				  +'<span class="glyphicon glyphicon-remove form-control-feedback"></span><strong>Error!</strong> Debe seleccionar las funciones en que trabajo durante la Practica.'
				+'</div>'
		  );
		valido = false;
	}
	else{
		if($("#funcion").val().length >= 2){
			
		}
		else{
			if(valido == true){
				$("#funcion").focus();
				
			}
			$("#ms-funcion").parent().append(
					'<div class="alert alert-danger alert-dismissible" role="alert">'
					  +'<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'
					  +'<span class="glyphicon glyphicon-remove form-control-feedback"></span><strong>Error!</strong> Debe seleccionar el minimo de funciones en que trabajo.'
					+'</div>'
			  );
			
			valido = false;
		}
	}
	
		
	
	if(valido == false){//si existe algun campo invalido
		return valido;
	}
	else{
		return true;
	}
}

function validacion_cambio_clave(e,opcion)
{
	var valido = true;
	var bandera = true;
	$(".help-block").remove();
	$(".alert.alert-danger.alert-dismissible").remove();
	$(".glyphicon.glyphicon-remove.form-control-feedback").remove();
	if(!$('#actual').val())
	{
		$("#actual").focus();
		$("#actual").attr("data-toggle","tooltip");
		$("#actual").attr("data-original-title","El campo no puede est\xe1r vac\xedo");
		$("#actual").tooltip({
	        placement : 'bottom',
	        trigger: 'focus'
	    });
		$("#actual").parent().addClass("has-error has-feedback");
		$('#actual').tooltip('show');
		//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
		valido = false;
		bandera = false;
	}
	if(bandera == true)
	{
		$("#actual").parent().removeClass('has-error');
		$("#actual").parent().addClass("has-success");
		if(!$('#nueva').val())
		{
			$("#nueva").focus();
			$("#nueva").attr("data-toggle","tooltip");
			$("#nueva").attr("data-original-title","El campo no puede est\xe1r vac\xedo");
			$("#nueva").tooltip({
		        placement : 'bottom',
		        trigger: 'focus'
		    });
			$("#nueva").parent().addClass("has-error has-feedback");
			$('#nueva').tooltip('show');
			//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
			valido = false;
			bandera = false;
		}else{
			if($('#nueva').val().length < 9)
			{
				$("#nueva").focus();
				$("#nueva").attr("data-toggle","tooltip");
				$("#nueva").attr("data-original-title","La clave de acceso debe contener a lo menos 9 caracteres");
				$("#nueva").tooltip({
			        placement : 'bottom',
			        trigger: 'focus'
			    });
				$("#nueva").parent().addClass("has-error has-feedback");
				$('#nueva').tooltip('show');
				//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
				valido = false;
				bandera = false;
			}
		}
	}
	if(bandera == true)
	{
		$("#nueva").parent().removeClass('has-error');
		$("#nueva").parent().addClass("has-success");
		if(!$('#confirmacion').val())
		{
			$("#confirmacion").focus();
			$("#confirmacion").attr("data-toggle","tooltip");
			$("#confirmacion").attr("data-original-title","El campo no puede est\xe1r vac\xedo");
			$("#confirmacion").tooltip({
		        placement : 'bottom',
		        trigger: 'focus'
		    });
			$("#confirmacion").parent().addClass("has-error has-feedback");
			$('#confirmacion').tooltip('show');
			//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
			valido = false;
			bandera = false;
		}else{
			if($('#confirmacion').val().length < 9)
			{
				$("#confirmacion").focus();
				$("#confirmacion").attr("data-toggle","tooltip");
				$("#confirmacion").attr("data-original-title","La clave de acceso debe contener a lo menos 9 caracteres");
				$("#confirmacion").tooltip({
			        placement : 'bottom',
			        trigger: 'focus'
			    });
				$("#confirmacion").parent().addClass("has-error has-feedback");
				$('#confirmacion').tooltip('show');
				valido = false;
				bandera = false;
			}else
			{
				if($('#confirmacion').val() != $('#nueva').val())
				{
					$("#confirmacion").focus();
					$("#confirmacion").attr("data-toggle","tooltip");
					$("#confirmacion").attr("data-original-title","La nueva clave de acceso no es igual en los dos campos");
					$("#confirmacion").tooltip({
				        placement : 'bottom',
				        trigger: 'focus'
				    });
					$("#confirmacion").parent().addClass("has-error has-feedback");
					$("#nueva").parent().removeClass('has-success');
					$("#nueva").parent().addClass("has-error has-feedback");
					$('#confirmacion').tooltip('show');
					valido = false;
					bandera = false;
				}

			}
		}
	}
	if(valido == false){//si existe algun campo invalido		
		return false;
	}
	else{

		$("#confirmacion").parent().removeClass('has-error');
		$("#confirmacion").parent().addClass("has-success");
		e.preventDefault();
		rotacion_clave(e,opcion);
	}
}

function validar_alumno(e){//valida el formulario modificar perfil
	var valido = true;
	var bandera = true;
	var regularCelular = "[\+][5][6][9][5-9][0-9]{7}";//----  /^\+\d{2,3}\s\d{9}$/   ---  /^/\+/\d{3}[5-9][0-9]{7}$/ --- /^\+\d{11}$/
	var regularTelefono = "[\+][5][6][-][0-9]{1,2}[-][2][1-9][0-9]{5,6}";
	var regularDireccion = "[A-Za-záéíóúàèìòùÀÈÌÒÙÁÉÍÓÚñÑüÜ ][#][0-9]";//expresion regular de direccion Caracteres + # + numeros
	$(".help-block").remove();
	$(".alert.alert-danger.alert-dismissible").remove();
	$(".glyphicon.glyphicon-remove.form-control-feedback").remove();
	if(!$('#rut').val())
	{
		$("#rut").focus();
		$("#rut").attr("data-toggle","tooltip");
		$("#rut").attr("data-original-title","El rut no puede ser nulo");
		$("#rut").tooltip({
	        placement : 'bottom',
	        trigger: 'focus'
	    });
		$("#rut").parent().addClass("has-error has-feedback");
		$('#rut').tooltip('show');
		//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
		valido = false;
		bandera = false;
	}
	
	if(bandera == true)
	{
		$("#rut").parent().removeClass('has-error');
		$("#rut").parent().addClass("has-success");
		if(!$('#nombre').val())
		{
			$("#nombre").focus();
			$("#nombre").attr("data-toggle","tooltip");
			$("#nombre").attr("data-original-title","El campo no puede est\xe1r Vaci\xedo");
			$("#nombre").tooltip({
		        placement : 'bottom',
		        trigger: 'focus'
		    });
			$("#nombre").parent().addClass("has-error has-feedback");
			$('#nombre').tooltip('show');
			//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
			valido = false;
			bandera = false;
		}
	}
	if(bandera == true)
	{
		$("#nombre").parent().removeClass('has-error');
		$("#nombre").parent().addClass("has-success");
		if(!$('#paterno').val())
		{
			$("#paterno").focus();
			$("#paterno").attr("data-toggle","tooltip");
			$("#paterno").attr("data-original-title","El campo no puede est\xe1r Vaci\xedo");
			$("#paterno").tooltip({
		        placement : 'bottom',
		        trigger: 'focus'
		    });
			$("#paterno").parent().addClass("has-error has-feedback");
			$('#paterno').tooltip('show');
			//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
			valido = false;
			bandera = false;
		}
	}
	if(bandera == true)
	{
		$("#paterno").parent().removeClass('has-error');
		$("#paterno").parent().addClass("has-success");
		if(!$('#materno').val())
		{
			$("#materno").focus();
			$("#materno").attr("data-toggle","tooltip");
			$("#materno").attr("data-original-title","El campo no puede est\xe1r Vaci\xedo");
			$("#materno").tooltip({
		        placement : 'bottom',
		        trigger: 'focus'
		    });
			$("#materno").parent().addClass("has-error has-feedback");
			$('#materno').tooltip('show');
			//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
			valido = false;
			bandera = false;
		}
	}
	if(bandera == true)
	{
		$("#materno").parent().removeClass('has-error');
		$("#materno").parent().addClass("has-success");
		if(!$('#email').val())
		{
			$("#email").focus();
			$("#email").attr("data-toggle","tooltip");
			$("#email").attr("data-original-title","El campo no puede est\xe1r Vaci\xedo");
			$("#email").tooltip({
		        placement : 'bottom',
		        trigger: 'focus'
		    });
			$("#email").parent().addClass("has-error has-feedback");
			$('#email').tooltip('show');
			//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
			valido = false;
			bandera = false;
		}
	}
	if(bandera == true)
	{
		$("#email").parent().removeClass('has-error');
		$("#email").parent().addClass("has-success");
		if($("#telefono").val().length == "" && $("#codigo").val() == "")
		{//el telefono fijo es valido si está Vaci\xedo
			$("#telefono").removeAttr("data-toggle");
			$("#telefono").removeAttr("data-original-title");
			$("#telefono").tooltip('destroy');
			$("#telefono").parent().parent().parent().removeClass("has-error has-feedback");
		}
		else
		{
			if($("#telefono").val().match(regularTelefono)){//
				$("#telefono").removeAttr("data-toggle");
				$("#telefono").removeAttr("data-original-title");
				$("#telefono").tooltip('destroy');
				$("#telefono").parent().parent().parent().removeClass("has-error has-feedback");
			}
			else
			{
				if(valido == true){
					$("#codigo").focus();
					$("#codigo").val("");
				}		
				$("#telefono").attr("data-toggle","tooltip");
				$("#telefono").attr("data-original-title","El numero "+$("#telefono").val()+" no corresponde a un telefono valido. Seleccione el codigo de area, luego digite su numero telefono de 7 digitos(8 digitos R.M.)");
				$("#telefono").tooltip({
			        placement : 'bottom',
			        trigger: 'focus'
			    });
				//$('#telefono').tooltip('show');
				$("#telefono").parent().parent().parent().addClass("has-error has-feedback");
				//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
				bandera = false;
				valido = false;
			}		
		}
	}
	if(bandera == true)
	{
		$("#telefono").parent().removeClass('has-error');
		$("#telefono").parent().addClass("has-success");
		$("#codigo").parent().removeClass('has-error');
		$("#codigo").parent().addClass("has-success");
		if($("#celular").val().length == "" ){//el campo celular es valido si está Vaci\xedo
			$("#celular").removeAttr("data-toggle");
			$("#celular").removeAttr("data-original-title");
			$("#celular").parent().parent().parent().removeClass("has-error has-feedback");
		}
		else
		{
			if($("#celular").val().match(regularCelular)){//validación del campo de celular(expresion regular)
				$("#celular").removeAttr("data-toggle");
				$("#celular").removeAttr("data-original-title");
				$("#celular").tooltip('destroy');
				$("#celular").parent().parent().parent().removeClass("has-error has-feedback");
			}
			else{
				if(valido == true){
					$("#celular").focus();
				}		
				$("#celular").attr("data-toggle","tooltip");
				$("#celular").attr("data-original-title","El numero "+$("#celular").val()+" no corresponde a un movil valido. Ingrese los 8 digitos de su numero movil");
				$("#celular").tooltip({
			        placement : 'bottom',
			        trigger: 'focus'
			    });
				$("#celular").val("");
				$('#celular').tooltip('show');
				$("#celular").parent().parent().parent().addClass("has-error has-feedback");
				//$("#celular").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
				bandera = false;
				valido = false;
			}
		}		
	}
	if(bandera == true)
	{
		$("#celular").parent().removeClass('has-error');
		$("#celular").parent().addClass("has-success");
		if(!$('#residencial').val())
		{
			$("#residencial").focus();
			$("#residencial").attr("data-toggle","tooltip");
			$("#residencial").attr("data-original-title","El campo no puede est\xe1r Vac\xedo");
			$("#residencial").tooltip({
		        placement : 'bottom',
		        trigger: 'focus'
		    });
			$("#residencial").parent().addClass("has-error has-feedback");
			$('#residencial').tooltip('show');
			//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
			valido = false;
			bandera = false;
		}else{
			if($("#residencial").val().match(regularDireccion)){//si está bien validado
				$("#residencial").removeAttr("data-toggle");
				$("#residencial").removeAttr("data-original-title");
				$("#residencial").tooltip('destroy');
				$("#residencial").parent().parent().removeClass("has-error has-feedback");
				$("#residencial").parent().parent().addClass("has-success has-feedback");
				$("#residencial").parent().append('<span class="glyphicon glyphicon-ok form-control-feedback"></span>');
			}
			else{
				if(valido == true){
					$("#residencial").focus();
				}
				
				$("#residencial").attr("data-toggle","tooltip");
				$("#residencial").attr("data-original-title","No corresponde a una direccion valida Ej: La Calle #889 El Sector");
				$("#residencial").tooltip({
			        placement : 'bottom',
			        //placement : 'right',
			        trigger: 'focus'
			    });
				$('#residencial').tooltip('show');
				$("#residencial").parent().parent().addClass("has-error has-feedback");
				$("#residencial").parent().append('<span class="glyphicon glyphicon-remove form-control-feedback"></span>');
				valido = false;
			}
		}
	}
	if(bandera == true)
	{
		$("#residencial").parent().removeClass('has-error');
		$("#residencial").parent().addClass("has-success");
		if(!$('#academica').val())
		{
			$("#academica").focus();
			$("#academica").attr("data-toggle","tooltip");
			$("#academica").attr("data-original-title","El campo no puede est\xe1r Vac\xedo");
			$("#academica").tooltip({
		        placement : 'bottom',
		        trigger: 'focus'
		    });
			$("#academica").parent().addClass("has-error has-feedback");
			$('#academica').tooltip('show');
			//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
			valido = false;
			bandera = false;
		}else{
			if($("#academica").val().match(regularDireccion)){//si está bien validado
				$("#academica").removeAttr("data-toggle");
				$("#academica").removeAttr("data-original-title");
				$("#academica").tooltip('destroy');
				$("#academica").parent().parent().removeClass("has-error has-feedback");
				$("#academica").parent().parent().addClass("has-success has-feedback");
				$("#academica").parent().append('<span class="glyphicon glyphicon-ok form-control-feedback"></span>');
			}
			else{
				if(valido == true){
					$("#academica").focus();
				}
				
				$("#academica").attr("data-toggle","tooltip");
				$("#academica").attr("data-original-title","No corresponde a una direccion valida Ej: La Calle #889 El Sector");
				$("#academica").tooltip({
			        placement : 'bottom',
			        //placement : 'right',
			        trigger: 'focus'
			    });
				$('#academica').tooltip('show');
				$("#academica").parent().parent().addClass("has-error has-feedback");
				$("#academica").parent().append('<span class="glyphicon glyphicon-remove form-control-feedback"></span>');
				valido = false;
			}
		}
	}
	if(bandera == true)
	{
		$("#academica").parent().removeClass('has-error');
		$("#academica").parent().addClass("has-success");
		if(!$('#region').val())
		{
			$("#region").focus();
			$("#region").attr("data-toggle","tooltip");
			$("#region").attr("data-original-title","Debe seleccionar uno de los valores");
			$("#region").tooltip({
		        placement : 'bottom',
		        trigger: 'focus'
		    });
			$("#region").parent().addClass("has-error has-feedback");
			$('#region').tooltip('show');
			//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
			valido = false;
			bandera = false;
		}
	}
	if(bandera == true)
	{
		$("#region").parent().removeClass('has-error');
		$("#region").parent().addClass("has-success");
		if(!$('#provincia').val())
		{
			$("#provincia").focus();
			$("#provincia").attr("data-toggle","tooltip");
			$("#provincia").attr("data-original-title","Debe seleccionar uno de los valores");
			$("#provincia").tooltip({
		        placement : 'bottom',
		        trigger: 'focus'
		    });
			$("#provincia").parent().addClass("has-error has-feedback");
			$('#provincia').tooltip('show');
			//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
			valido = false;
			bandera = false;
		}
	}
	if(bandera == true)
	{
		$("#provincia").parent().removeClass('has-error');
		$("#provincia").parent().addClass("has-success");
		if(!$('#comuna').val())
		{
			$("#comuna").focus();
			$("#comuna").attr("data-toggle","tooltip");
			$("#comuna").attr("data-original-title","Debe seleccionar uno de los valores");
			$("#comuna").tooltip({
		        placement : 'bottom',
		        trigger: 'focus'
		    });
			$("#comuna").parent().addClass("has-error has-feedback");
			$('#comuna').tooltip('show');
			//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
			valido = false;
			bandera = false;
		}
	}
	if(bandera == true)
	{
		$("#comuna").parent().removeClass('has-error');
		$("#comuna").parent().addClass("has-success");
		if(!$('#region_2').val())
		{
			$("#region_2").focus();
			$("#region_2").attr("data-toggle","tooltip");
			$("#region_2").attr("data-original-title","Debe seleccionar uno de los valores");
			$("#region_2").tooltip({
		        placement : 'bottom',
		        trigger: 'focus'
		    });
			$("#region_2").parent().addClass("has-error has-feedback");
			$('#region_2').tooltip('show');
			//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
			valido = false;
			bandera = false;
		}
	}
	if(bandera == true)
	{
		$("#region_2").parent().removeClass('has-error');
		$("#region_2").parent().addClass("has-success");
		if(!$('#provincia_2').val())
		{
			$("#provincia_2").focus();
			$("#provincia_2").attr("data-toggle","tooltip");
			$("#provincia_2").attr("data-original-title","Debe seleccionar uno de los valores");
			$("#provincia_2").tooltip({
		        placement : 'bottom',
		        trigger: 'focus'
		    });
			$("#provincia_2").parent().addClass("has-error has-feedback");
			$('#provincia_2').tooltip('show');
			//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
			valido = false;
			bandera = false;
		}
	}
	if(bandera == true)
	{
		$("#provincia_2").parent().removeClass('has-error');
		$("#provincia_2").parent().addClass("has-success");
		if(!$('#comuna_2').val())
		{
			$("#comuna_2").focus();
			$("#comuna_2").attr("data-toggle","tooltip");
			$("#comuna_2").attr("data-original-title","Debe seleccionar uno de los valores");
			$("#comuna_2").tooltip({
		        placement : 'bottom',
		        trigger: 'focus'
		    });
			$("#comuna_2").parent().addClass("has-error has-feedback");
			$('#comuna_2').tooltip('show');
			//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
			valido = false;
			bandera = false;
		}
	}
	if(valido == false){//si existe algun campo invalido		
		return false;
	}
	else{
		$("#comuna_2").parent().removeClass('has-error');
		$("#comuna_2").parent().addClass("has-success");
		e.preventDefault();
		rotacion_perfil(e);
	}
}
function rotacion_clave(e,opcion)
{
	$("#toggleCSS").attr("href", "css/alertify.bootstrap.css");
	if(opcion == 1)
	{
		alertify.confirm("\xbfEst\xe1 seguro que desea cambiar su clave de acceso?", function (e2) {
			if (e2) {
				alertify.success("Procesando Nueva Clave...");
				$("#formulario_clave").submit();//envia el formulario
			} else {
				alertify.error("Se cancel\xf3 el Proceso.");
				e.preventDefault();
			}
		}, "Default Value");
	}
}
function rotacion_perfil(e)
{
	$("#toggleCSS").attr("href", "css/alertify.bootstrap.css");
	alertify.confirm("\xbfEst\xe1 seguro que desea cambiar su informaci\xf3n personal?", function (e2) {
		if (e2) {
			alertify.success("Procesando Nueva Clave...");
			$("#formulario_alumno").submit();//envia el formulario
		} else {
			alertify.error("Se cancel\xf3 el Proceso.");
			e.preventDefault();
		}
	}, "Default Value");
}