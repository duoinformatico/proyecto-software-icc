function verRubrica(ins_id, rut, pra_id, rol_id)//redirecciona para ver un instrumento de evaluacion
{
	$.ajax({
		async: true,
		type: "get",
		dataType: "html",
		contentType: "aplication/x-www-form-urlencoded",
		url:"AjaxControlador",
		data: {ins_id : ins_id, rut_evaluador : rut, practica_id : pra_id, rol_id : rol_id },
		success: function(datos){
			$("#div_instrumento").empty();
			$("#div_instrumento").html(''+datos);
			$("#myModal").modal('show');
			
		}
	});
	return false;
}

function verDetalleEvaluacion(tipo, rut, pra_id, rol_id, eva_fecha)//redirecciona para ver el detalle de una evaluacion
{	//tipo: corresponde si es evaluacion de 'alumno', 'supervisor' o 'academico'
	//rut: rut del evaluador
	//pra_id: codigo de la práctica que fue evaluada
	//rol_id: rol con que el evaluador evaluó la practica, sirve para determinar con que rubrica evaluó
	//eva_fecha: sirve para determinar con que rubrica evaluó
	$.ajax({
		async: true,
		type: "get",
		dataType: "html",
		contentType: "aplication/x-www-form-urlencoded",
		url:"AjaxControlador",
		data: {tipo_eval : tipo, evaluador_rut : rut, id_practica : pra_id, id_rol : rol_id, eva_fecha : eva_fecha },
		success: function(datos){
			$("#div_instrumento").empty();
			$("#div_instrumento").html(''+datos);
			$("#myModal").modal('show');
			
		}
	});
	return false;
}

function consultarBitacora(bit_id,plan_id)//redirecciona para ver una bitacora en formulario , pero sin poder editarla
{
	$.ajax({
		async: true,
		type: "get",
		dataType: "html",
		contentType: "aplication/x-www-form-urlencoded",
		url:"formularioBitacoraServlet.do",
		data: {bit_id : bit_id,plan_id : plan_id},
		success: function(datos){
			$("#div_consultar").empty();
			$("#div_consultar").html(''+datos);
			$("#myModal_consulta").modal('show');	
		}
	});
	return false;
}

function calcularPuntaje(nombre_items){		
	var suma = 0;
	var nombre_items = $("#nombre_items").val();
	var items = nombre_items.split("-");
	//alert("Cantidad Items Rubrica = "+items.length+"\nElementos"+items);
	for(var i=0;i <= items.length; i++){
		//alert(items.length);
		 if( $("input[name='"+items[i]+"']").is(':checked')) { 
			 suma = suma + parseInt($("input[name='"+items[i]+"']:checked").val());
			 //alert($("input[name='"+items[i]+"']:checked").val());
		 }			
		
	}
	//alert(suma);
	var x1 = 4.0;//nota minima de aprobación
	var x2 = 7.0;//nota maxima
	var Y = (suma/parseInt($("#max_puntaje").val()))*100;
	var X = (x2 - x1 )*((Y - 60)/(100 - 60)) + x1;
	//alert("Nota = "+X.toFixed(2));
	//var nota = 6/36;
	
	$("input:text[name=puntaje]").val(suma);
	if(X < 1){
		$("input:text[name=nota]").val(1.0);
	}
	else{
		$("input:text[name=nota]").val(X.toFixed(2));
	}
}


function realizarObservacion(bit_id)//ridirige a la vista para realizar una obs
{
	$.ajax({
		async: true,
		type: "get",
		dataType: "html",
		contentType: "aplication/x-www-form-urlencoded",
		url:"AjaxControlador",
		data: {bit_id_obs:bit_id},
		success: function(datos){
			$("#div_observacion").empty();
			$("#div_observacion").html(''+datos);
			$("#myModal_registro").modal('show');
		}
	});
	return false;
}

function verBitacoras(pra_id)//redirecciona para ver las bitacoras de un alumno a cargo del academico
{
		location.href='BitacoraServlet.do?id='+pra_id;
}

function funcionMaestra(e,eleccion,numero) 
{
	//el numero 1 es para cuando se necesita que no se ingresen puntos y otros caracteres
	if(numero == '1')
	{
		bool1 = especial_character(e);
	}
	//el numero 2 es para cuando se necesita que no se ingresen  otros caracteres, pero si puntos
	if(numero == '2')
	{
		bool1 = especial_character2(e);
	}
	if(bool1 == false)
	{
		return bool1;
	}
	bool2 = solo_letras(e,eleccion);
	return bool2;
}
	
	// evita insercion de algunos caracteres
function especial_character(e)
{
    var key = e.charCode ? e.charCode : e.keyCode;
    if (key == 46 || key == 145 || key == 146 || key == 39 || key == 37 || key == 33 || key == 161 || key == 42)
    {
        return false;
    }    
}
//evita insercion de caracteres, pero si deja insertar puntos
function especial_character2(e)
{
    var key = e.charCode ? e.charCode : e.keyCode;
    if (key == 145 || key == 146 || key == 39 || key == 37 || key == 33 || key == 161 || key == 42)
    {
        return false;
    }    
}

function solo_letras(elEvento,eleccion) 
{
	  // Variables que definen los caracteres permitidos
	  var permitidos = eleccion;
	  var numeros = "0123456789";
	  //var caracteres = "";
	  //
	  var caracteres = " abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZáéíóúÁÉÍÓÚ#/.@:";
	  var email = " abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ@_-";
	  var nombres = " abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZáéíóúÁÉÍÓÚ";
	  var direccion = " abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZáéíóúÁÉÍÓÚ#-.";
	  var descripcion = " abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZáéíóúÁÉÍÓÚ-,";
	  var web = " abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZáéíóúÁÉÍÓÚ:/_-.";
	  var tele_fijo = "-";
	  var celular = "*+#";
	  var numeros_caracteres = numeros + caracteres;
	  var email_num = numeros + email;
	  var teclas_especiales = [8, 9, 37, 39, 46];
	  var clave = "abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ";
	  // 9 = Tab, 8 = BackSpace, 46 = Supr, 37 = flecha izquierda, 39 = flecha derecha			 
	 
	  // Seleccionar los caracteres a partir del parámetro de la función
	  switch(permitidos) {
		 case 'num':
			permitidos = numeros;
			break;
		 case 'car':
			permitidos = caracteres;
			break;
		 case 'num_car':
			permitidos = numeros_caracteres;
			break;
		 case 'nombres':
			permitidos = nombres;
			break;
		 case 'email':
			permitidos = email_num;
			break;
		 case 'dire':
			permitidos = direccion+ numeros;
			break;
		 case 'descri':
			permitidos = descripcion+ numeros;
			break;
		 case 'fijo':
			permitidos = tele_fijo+ numeros;
			break;
		 case 'celular':
			permitidos = celular+ numeros;
			break;
		 case 'web':
			permitidos = web + numeros;
			break;
		 case 'clave':
				permitidos = clave + numeros;
				break;
	  }
		 
	  // Obtener la tecla pulsada 
	  var evento = elEvento || window.event;
	  var codigoCaracter = evento.charCode || evento.keyCode;
	  var caracter = String.fromCharCode(codigoCaracter);
	 
	  // Comprobar si la tecla pulsada es alguna de las teclas especiales
	  // (teclas de borrado y flechas horizontales)
	  var tecla_especial = false;
	  for(var i in teclas_especiales) {
		 if(codigoCaracter == teclas_especiales[i]) {
			tecla_especial = true;
			break;
		 }
	  }
	 
	  // Comprobar si la tecla pulsada se encuentra en los caracteres permitidos
	  // o si es una tecla especial
	  return permitidos.indexOf(caracter) != -1 || tecla_especial;
		/* Sólo números agregar / alfinal del input
		<input type="text" id="texto" onkeypress="return permite(event, 'num')" >

		// Sólo letras
		<input type="text" id="texto" onkeypress="return permite(event, 'car')" >

		// Sólo letras o números
		<input type="text" id="texto" onkeypress="return permite(event, 'num_car')" >*/
}

function validacion_cambio_clave(e,opcion)
{
	var valido = true;
	var bandera = true;
	$(".help-block").remove();
	$(".alert.alert-danger.alert-dismissible").remove();
	$(".glyphicon.glyphicon-remove.form-control-feedback").remove();
	if(!$('#actual').val())
	{
		$("#actual").focus();
		$("#actual").attr("data-toggle","tooltip");
		$("#actual").attr("data-original-title","El campo no puede est\xe1r vac\xedo");
		$("#actual").tooltip({
	        placement : 'bottom',
	        trigger: 'focus'
	    });
		$("#actual").parent().addClass("has-error has-feedback");
		$('#actual').tooltip('show');
		//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
		valido = false;
		bandera = false;
	}
	if(bandera == true)
	{
		$("#actual").parent().removeClass('has-error');
		$("#actual").parent().addClass("has-success");
		if(!$('#nueva').val())
		{
			$("#nueva").focus();
			$("#nueva").attr("data-toggle","tooltip");
			$("#nueva").attr("data-original-title","El campo no puede est\xe1r vac\xedo");
			$("#nueva").tooltip({
		        placement : 'bottom',
		        trigger: 'focus'
		    });
			$("#nueva").parent().addClass("has-error has-feedback");
			$('#nueva').tooltip('show');
			//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
			valido = false;
			bandera = false;
		}else{
			if($('#nueva').val().length < 9)
			{
				$("#nueva").focus();
				$("#nueva").attr("data-toggle","tooltip");
				$("#nueva").attr("data-original-title","La clave de acceso debe contener a lo menos 9 caracteres");
				$("#nueva").tooltip({
			        placement : 'bottom',
			        trigger: 'focus'
			    });
				$("#nueva").parent().addClass("has-error has-feedback");
				$('#nueva').tooltip('show');
				//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
				valido = false;
				bandera = false;
			}
		}
	}
	if(bandera == true)
	{
		$("#nueva").parent().removeClass('has-error');
		$("#nueva").parent().addClass("has-success");
		if(!$('#confirmacion').val())
		{
			$("#confirmacion").focus();
			$("#confirmacion").attr("data-toggle","tooltip");
			$("#confirmacion").attr("data-original-title","El campo no puede est\xe1r vac\xedo");
			$("#confirmacion").tooltip({
		        placement : 'bottom',
		        trigger: 'focus'
		    });
			$("#confirmacion").parent().addClass("has-error has-feedback");
			$('#confirmacion').tooltip('show');
			//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
			valido = false;
			bandera = false;
		}else{
			if($('#confirmacion').val().length < 9)
			{
				$("#confirmacion").focus();
				$("#confirmacion").attr("data-toggle","tooltip");
				$("#confirmacion").attr("data-original-title","La clave de acceso debe contener a lo menos 9 caracteres");
				$("#confirmacion").tooltip({
			        placement : 'bottom',
			        trigger: 'focus'
			    });
				$("#confirmacion").parent().addClass("has-error has-feedback");
				$('#confirmacion').tooltip('show');
				valido = false;
				bandera = false;
			}else
			{
				if($('#confirmacion').val() != $('#nueva').val())
				{
					$("#confirmacion").focus();
					$("#confirmacion").attr("data-toggle","tooltip");
					$("#confirmacion").attr("data-original-title","La nueva clave de acceso no es igual en los dos campos");
					$("#confirmacion").tooltip({
				        placement : 'bottom',
				        trigger: 'focus'
				    });
					$("#confirmacion").parent().addClass("has-error has-feedback");
					$("#nueva").parent().removeClass('has-success');
					$("#nueva").parent().addClass("has-error has-feedback");
					$('#confirmacion').tooltip('show');
					valido = false;
					bandera = false;
				}

			}
		}
	}
	if(valido == false){//si existe algun campo invalido		
		return false;
	}
	else{

		$("#confirmacion").parent().removeClass('has-error');
		$("#confirmacion").parent().addClass("has-success");
		e.preventDefault();
		rotacion_clave(e,opcion);
	}
}

function validar_academico(e,opcion){//valida el formulario perfil academico
	var valido = true;
	var bandera = true;
	var regularCelular = "[\+][5][6][9][5-9][0-9]{7}";//----  /^\+\d{2,3}\s\d{9}$/   ---  /^/\+/\d{3}[5-9][0-9]{7}$/ --- /^\+\d{11}$/
	var regularTelefono = "[\+][5][6][-][0-9]{1,2}[-][2][1-9][0-9]{5,6}";
	$(".help-block").remove();
	$(".alert.alert-danger.alert-dismissible").remove();
	$(".glyphicon.glyphicon-remove.form-control-feedback").remove();
	if(opcion==1)
	{
		if(!$('#rut').val())
		{
			$("#rut").focus();
			$("#rut").attr("data-toggle","tooltip");
			$("#rut").attr("data-original-title","El rut no puede ser nulo");
			$("#rut").tooltip({
		        placement : 'bottom',
		        trigger: 'focus'
		    });
			$("#rut").parent().addClass("has-error has-feedback");
			$('#rut').tooltip('show');
			//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
			valido = false;
			bandera = false;
		}
		
		if(bandera == true)
		{
			$("#rut").parent().removeClass('has-error');
			$("#rut").parent().addClass("has-success");
			if(!$('#nombre').val())
			{
				$("#nombre").focus();
				$("#nombre").attr("data-toggle","tooltip");
				$("#nombre").attr("data-original-title","El campo no puede est\xe1r Vaci\xedo");
				$("#nombre").tooltip({
			        placement : 'bottom',
			        trigger: 'focus'
			    });
				$("#nombre").parent().addClass("has-error has-feedback");
				$('#nombre').tooltip('show');
				//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
				valido = false;
				bandera = false;
			}
		}
		if(bandera == true)
		{
			$("#nombre").parent().removeClass('has-error');
			$("#nombre").parent().addClass("has-success");
			if(!$('#paterno').val())
			{
				$("#paterno").focus();
				$("#paterno").attr("data-toggle","tooltip");
				$("#paterno").attr("data-original-title","El campo no puede est\xe1r Vaci\xedo");
				$("#paterno").tooltip({
			        placement : 'bottom',
			        trigger: 'focus'
			    });
				$("#paterno").parent().addClass("has-error has-feedback");
				$('#paterno').tooltip('show');
				//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
				valido = false;
				bandera = false;
			}
		}
		if(bandera == true)
		{
			$("#paterno").parent().removeClass('has-error');
			$("#paterno").parent().addClass("has-success");
			if(!$('#materno').val())
			{
				$("#materno").focus();
				$("#materno").attr("data-toggle","tooltip");
				$("#materno").attr("data-original-title","El campo no puede est\xe1r Vaci\xedo");
				$("#materno").tooltip({
			        placement : 'bottom',
			        trigger: 'focus'
			    });
				$("#materno").parent().addClass("has-error has-feedback");
				$('#materno').tooltip('show');
				//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
				valido = false;
				bandera = false;
			}
		}
		if(bandera == true)
		{
			$("#materno").parent().removeClass('has-error');
			$("#materno").parent().addClass("has-success");
			if(!$('#email').val())
			{
				$("#email").focus();
				$("#email").attr("data-toggle","tooltip");
				$("#email").attr("data-original-title","El campo no puede est\xe1r Vaci\xedo");
				$("#email").tooltip({
			        placement : 'bottom',
			        trigger: 'focus'
			    });
				$("#email").parent().addClass("has-error has-feedback");
				$('#email').tooltip('show');
				//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
				valido = false;
				bandera = false;
			}
		}
		if(bandera == true)
		{
			$("#email").parent().removeClass('has-error');
			$("#email").parent().addClass("has-success");
			if($("#telefono").val().length == "" && $("#codigo").val() == "")
			{//el telefono fijo es valido si está Vaci\xedo
				$("#telefono").removeAttr("data-toggle");
				$("#telefono").removeAttr("data-original-title");
				$("#telefono").tooltip('destroy');
				$("#telefono").parent().parent().parent().removeClass("has-error has-feedback");
			}
			else
			{
				if($("#telefono").val().match(regularTelefono)){//
					$("#telefono").removeAttr("data-toggle");
					$("#telefono").removeAttr("data-original-title");
					$("#telefono").tooltip('destroy');
					$("#telefono").parent().parent().parent().removeClass("has-error has-feedback");
				}
				else
				{
					if(valido == true){
						$("#codigo").focus();
						$("#codigo").val("");
					}		
					$("#telefono").attr("data-toggle","tooltip");
					$("#telefono").attr("data-original-title","El numero "+$("#telefono").val()+" no corresponde a un telefono valido. Seleccione el codigo de area, luego digite su numero telefono de 7 digitos(8 digitos R.M.)");
					$("#telefono").tooltip({
				        placement : 'bottom',
				        trigger: 'focus'
				    });
					//$('#telefono').tooltip('show');
					$("#telefono").parent().parent().parent().addClass("has-error has-feedback");
					//$("#telefono").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
					bandera = false;
					valido = false;
				}		
			}
		}
		if(bandera == true)
		{
			$("#telefono").parent().removeClass('has-error');
			$("#telefono").parent().addClass("has-success");
			$("#codigo").parent().removeClass('has-error');
			$("#codigo").parent().addClass("has-success");
			if($("#celular").val().length == "" ){//el campo celular es valido si está Vaci\xedo
				$("#celular").removeAttr("data-toggle");
				$("#celular").removeAttr("data-original-title");
				$("#celular").parent().parent().parent().removeClass("has-error has-feedback");
			}
			else{
				if($("#celular").val().match(regularCelular)){//validación del campo de celular(expresion regular)
					$("#celular").removeAttr("data-toggle");
					$("#celular").removeAttr("data-original-title");
					$("#celular").tooltip('destroy');
					$("#celular").parent().parent().parent().removeClass("has-error has-feedback");
				}
				else{
					if(valido == true){
						$("#celular").focus();
					}		
					$("#celular").attr("data-toggle","tooltip");
					$("#celular").attr("data-original-title","El numero "+$("#celular").val()+" no corresponde a un movil valido. Ingrese los 8 digitos de su numero movil");
					$("#celular").tooltip({
				        placement : 'bottom',
				        trigger: 'focus'
				    });
					$("#celular").val("");
					$('#celular').tooltip('show');
					$("#celular").parent().parent().parent().addClass("has-error has-feedback");
					//$("#celular").parent().append(' <span class="glyphicon glyphicon-remove form-control-feedback"></span> ');
					bandera = false;
					valido = false;
				}
			}		
		}
	}

	if(valido == false){//si existe algun campo invalido		
		return false;
	}
	else{
			$("#celular").parent().removeClass('has-error');
			$("#celular").parent().addClass("has-success");
			e.preventDefault();
			rotacion_perfil(e);
	}
}

function rotacion_perfil(e)
{
	$("#toggleCSS").attr("href", "css/alertify.bootstrap.css");
	alertify.confirm("\xbfEst\xe1 seguro que desea cambiar su informaci\xf3n personal?", function (e2) {
		if (e2) {
			alertify.success("Procesando Nueva Clave...");
			$("#formulario_academico").submit();//envia el formulario
		} else {
			alertify.error("Se cancel\xf3 el Proceso.");
			e.preventDefault();
		}
	}, "Default Value");
}

function rotacion_clave(e,opcion)
{
	$("#toggleCSS").attr("href", "css/alertify.bootstrap.css");
	if(opcion == 1)
	{
		alertify.confirm("\xbfEst\xe1 seguro que desea cambiar su clave de acceso?", function (e2) {
			if (e2) {
				alertify.success("Procesando Nueva Clave...");
				$("#formulario_clave").submit();//envia el formulario
			} else {
				alertify.error("Se cancel\xf3 el Proceso.");
				e.preventDefault();
			}
		}, "Default Value");
	}
}