<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<link rel="stylesheet" href="css/alertify.core.css" />
<link rel="stylesheet" href="css/alertify.default.css" id="toggleCSS" />
<script src="js/alertify.min.js"></script>
<t:plantillaSupervisor>
    <jsp:attribute name="header">

    </jsp:attribute>			
    <jsp:body>
    		<div class="row">
				<div class="col-xs-9 col-sm-offset-1 col-xs-offset-1">
					<legend>Modificar Información Personal</legend>
				</div>
	 		</div>
	 		<c:if test="${exito == 1}">
				<div class="col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1">
					<div class="alert alert-success alert-dismissible" role="alert">
					  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					  <span class="glyphicon glyphicon-ok"></span>
					  <strong>Exito</strong> Ha modificado Exitosamente su Información Personal .
					</div>
				</div>
			</c:if>
	     	<div class="row">
				<div class="col-xs-9 col-sm-9 col-sm-offset-1 col-xs-offset-1 columna">	
					<form class="form-horizontal" id="formulario_supervisor"  role="form" method="POST" action="perfilesServlet.do">
						<div class="form-group">
						<label class="col-xs-3 control-label" for="rut">Rut:</label>
							<div class="col-xs-9">
								<input type="text" maxlength="20" class="form-control" id="rut" name="rut" value="${supervisor.sup_rut}" readonly="readonly">
							</div>
						</div>
						<script type="text/javascript">
						$(document).ready(function () {
								$('#nombre').bind('copy paste drop', function (e) {
						           e.preventDefault();
						        });       
						        $("#nombre").focusin(function() {
						        	$("#nombre").attr('autocomplete', 'off');
						        });
						        $("#nombre").focusout(function() {
						        	$("#nombre").removeAttr('autocomplete');
						        });
						        $('#paterno').bind('copy paste drop', function (e) {
							           e.preventDefault();
							        });
						        $("#paterno").focusin(function() {
						        	$("#paterno").attr('autocomplete', 'off');
						        });
						        $("#paterno").focusout(function() {
						        	$("#paterno").removeAttr('autocomplete');
						        });
						        $('#materno').bind('copy paste drop', function (e) {
							           e.preventDefault();
							        });
						        $("#materno").focusin(function() {
						        	$("#materno").attr('autocomplete', 'off');
						        });
						        $("#materno").focusout(function() {
						        	$("#materno").removeAttr('autocomplete');
						        });
						        $('#email').bind('copy paste drop', function (e) {
							           e.preventDefault();
							        });
						        $("#email").focusin(function() {
						        	$("#email").attr('autocomplete', 'off');
						        });
						        $("#email").focusout(function() {
						        	$("#email").removeAttr('autocomplete');
						        });
						        $('#telefono').bind('copy paste drop', function (e) {
							           e.preventDefault();
							        });
						        $("#telefono").focusin(function() {
						        	$("#telefono").attr('autocomplete', 'off');
						        	if($("#codigo").val() == ""){
						        		$("#telefono").attr('readonly', 'readonly');
						        		$("#codigo").focus();
						        	}
						        	else{
						        		$("#telefono").removeAttr('readonly');
						        	}
						        	if($("#codigo").val() != ""){
						        		if($("#codigo").val() == 2){
						        			$("#telefono").attr('maxlength', '8');
						        		}
						        		else{
						        			$("#telefono").attr('maxlength', '7');
						        		}
						        	}					        	
						        });
								$("#codigo").change(function(){					        	
									if($("#codigo").val() != ""){
						        		$("#telefono").focus();
						        		$("#telefono").val("");
						        	}					        	
						        });
						        $("#telefono").change(function(){					        	
						        	
						        	if($("#telefono").val().length == 8 && $("#codigo").val() == 2){
						        		$("#telefono").val("+56-"+$("#codigo").val()+"-"+$("#telefono").val());
						        	}
						        	if($("#telefono").val().length == 7 && $("#codigo").val() != 2){
						        		$("#telefono").val("+56-"+$("#codigo").val()+"-"+$("#telefono").val());
						        	}
						        });					        	
						        
						        $("#telefono").focusout(function() {
						        	$("#telefono").removeAttr('autocomplete');
						        	$("#telefono").removeAttr('maxlength');
						        	if($("#codigo").val() == ""){
						        		$("#telefono").attr('readonly', 'readonly');
						        		$("#codigo").focus();
						        	}
						        	else{
						        		$("#telefono").removeAttr('readonly');
						        	}
						        	if($("#telefono").val().length == 8 && $("#codigo").val() == 2){
						        		$("#telefono").val("+56-"+$("#codigo").val()+"-"+$("#telefono").val());
						        	}
						        	if($("#telefono").val().length < 8 && $("#codigo").val() == 2){
						        		$("#telefono").val("");
						        	}
						        	if($("#telefono").val().length == 7 && $("#codigo").val() != 2){
						        		$("#telefono").val("+56-"+$("#codigo").val()+"-"+$("#telefono").val());
						        	}
						        	if($("#telefono").val().length < 7 && $("#codigo").val() != 2){
						        		$("#telefono").val("");
						        	}
						        });
						        $('#celular').bind('copy paste drop', function (e) {
							           e.preventDefault();
							        });
						        $("#celular").focusin(function() {
						        	$("#celular").attr('autocomplete', 'off');
						        	$("#celular").attr('maxlength', '8');					        	
						        });
						       $("#celular").focusout(function() {
						        	$("#celular").removeAttr('autocomplete');
						        	$("#celular").removeAttr('maxlength');
						        	if($("#celular").val().length < 8){
						        		$("#celular").val("");
						        	}
						        	if($("#celular").val().length == 8){
						        		$("#celular").val("+569"+$("#celular").val());
						        	}
						        });
						        $('#profesion').bind('copy paste drop', function (e) {
						           e.preventDefault();
						        });
						        $("#profesion").focusin(function() {
						        	$("#profesion").attr('autocomplete', 'off');
						        });
						        $("#profesion").focusout(function() {
						        	$("#profesion").removeAttr('autocomplete');
						        });
						        $('#cargo').bind('copy paste drop', function (e) {
							           e.preventDefault();
							        });
						        $("#cargo").focusin(function() {
						        	$("#cargo").attr('autocomplete', 'off');
						        });
						        $("#cargo").focusout(function() {
						        	$("#cargo").removeAttr('autocomplete');
						        });
					      });
					    </script>
						<div class="form-group">
						<label class="col-xs-3 control-label" for="nombres">Nombres:</label>
							<div class="col-xs-9">
								<input type="text" maxlength="20" class="form-control" id="nombre" name="nombre" value="${supervisor.sup_nombres}" readonly="readonly">
							</div>
						</div>
						<div class="form-group">
						<label class="col-xs-3 control-label" for="paterno">Apellido Paterno:</label>
							<div class="col-xs-9">
								<input type="text" maxlength="20" class="form-control" id="paterno" name="paterno" value="${supervisor.sup_apellido_p}" readonly="readonly">
							</div>
						</div>
						<div class="form-group">
						<label class="col-xs-3 control-label" for="materno">Apellido Materno:</label>
							<div class="col-xs-9">
								<input type="text" maxlength="20" class="form-control" id="materno" name="materno" value="${supervisor.sup_apellido_m}" readonly="readonly">
							</div>
						</div>
						<div class="form-group">
						<label class="col-xs-3 control-label" for="email">E-mail:</label>
							<div class="col-xs-9">
								<input type="text" maxlength="20" class="form-control" id="email" name="email" value="${supervisor.sup_email}" readonly="readonly">
							</div>
						</div>
						<div class="form-group">
							<label class="col-xs-3 control-label" for="telefono">Teléfono:</label>
							<div class="col-xs-4">	
								<select class="form-control" id="codigo" name="codigo">
									<option value="">Código Area...</option>
									<optgroup label="Zona Norte">
										<c:if test="${codigo == 58}" >			
											<option value="58" selected="selected">(58) Arica-Parinacota</option>
										</c:if>
										<c:if test="${codigo != 58}" >			
											<option value="58">(58) Arica-Parinacota</option>
										</c:if>
										<c:if test="${codigo == 57}" >		
											<option value="57" selected="selected">(57) Iquique-Tamarugal</option>	
										</c:if>
										<c:if test="${codigo != 57}" >			
											<option value="57">(57) Iquique-Tamarugal</option>
										</c:if>
										<c:if test="${codigo == 55}" >			
											<option value="55" selected="selected">(55) Tocopilla-Loa-Antofagasta</option>
										</c:if>
										<c:if test="${codigo != 55}" >			
											<option value="55">(55) Tocopilla-Loa-Antofagasta</option>
										</c:if>
										<c:if test="${codigo == 51}" >			
											<option value="51" selected="selected">(51) Huasco</option>
										</c:if>
										<c:if test="${codigo != 51}" >			
											<option value="51">(51) Huasco</option>
										</c:if>
										<c:if test="${codigo == 52}" >			
											<option value="52" selected="selected">(52) Chañaral-Copiapó</option>
										</c:if>
										<c:if test="${codigo != 52}" >			
											<option value="52">(52) Chañaral-Copiapó</option>
										</c:if>
										<c:if test="${codigo == 51}" >			
											<option value="51" selected="selected">(51) Elqui</option>
										</c:if>
										<c:if test="${codigo != 51}" >			
											<option value="51">(51) Elqui</option>
										</c:if>
										<c:if test="${codigo == 53}" >			
											<option value="53" selected="selected">(53) Choapa-Limarí</option>
										</c:if>
										<c:if test="${codigo != 53}" >			
											<option value="53">(53) Choapa-Limarí</option>
										</c:if>
									</optgroup>
									<optgroup label="Zona Centro">
										<c:if test="${codigo == 32}" >			
											<option value="32" selected="selected">(73)>(32) Isla de Pascua-Valparaíso-Marga Marga(Quilpué-Villa Alemana)</option>
										</c:if>
										<c:if test="${codigo != 32}" >			
											<option value="32">(32) Isla de Pascua-Valparaíso-Marga Marga(Quilpué-Villa Alemana)</option>
										</c:if>
										<c:if test="${codigo == 33}" >		
											<option value="33" selected="selected">(73)>(33) Petorca-Quillota-Marga Marga(Limache y Olmué)</option>
										</c:if>
										<c:if test="${codigo != 33}" >			
											<option value="33">(33) Petorca-Quillota-Marga Marga(Limache y Olmué)</option>
										</c:if>
										<c:if test="${codigo == 34}" >			
											<option value="34" selected="selected">(73)>(34) Los Andes-San Felipe de Aconcagua</option>
										</c:if>
										<c:if test="${codigo != 34}" >			
											<option value="34">(34) Los Andes-San Felipe de Aconcagua</option>
										</c:if>
										<c:if test="${codigo == 35}" >		
											<option value="35" selected="selected">(73)>(35) San Antonio</option>	
										</c:if>
										<c:if test="${codigo != 35}" >			
											<option value="35">(35) San Antonio</option>
										</c:if>
										<c:if test="${codigo == 2}" >			
											<option value="2" selected="selected">(73)> ( 2) Chacabuco-Cordillera-Maipo-Melipilla-Santiago-Talagante</option>
										</c:if>
										<c:if test="${codigo != 2}" >			
											<option value="2"> ( 2) Chacabuco-Cordillera-Maipo-Melipilla-Santiago-Talagante</option>
										</c:if>
										<c:if test="${codigo == 72}" >			
											<option value="72" selected="selected">(73)>(72) Cachapoal-ardenal Caro-Colchagua</option>
										</c:if>
										<c:if test="${codigo != 72}" >			
											<option value="72">(72) Cachapoal-ardenal Caro-Colchagua</option>
										</c:if>
										<c:if test="${codigo == 71}" >			
											<option value="71" selected="selected">(73)>(71) Talca</option>
										</c:if>
										<c:if test="${codigo != 71}" >			
											<option value="71">(71) Talca</option>
										</c:if>
										<c:if test="${codigo == 73}" >			
											<option value="73" selected="selected">(73) Cauquenes-Linares</option>
										</c:if>
										<c:if test="${codigo != 73}" >				
											<option value="73">(73) Cauquenes-Linares</option>
										</c:if>
										<c:if test="${codigo == 75}" >			
											<option value="75" selected="selected">(75) Curicó</option> 
										</c:if>
										<c:if test="${codigo != 75}" >			
											<option value="75">(75) Curicó</option>
										</c:if>
										
									</optgroup>
									<optgroup label="Zona Sur">
										<c:if test="${codigo == 41}" >			
											<option value="41" selected="selected">(41) Arauco-Concepción </option>
										</c:if>
										<c:if test="${codigo != 41}" >			
											<option value="41">(41) Arauco-Concepción </option>
										</c:if>
										<c:if test="${codigo == 42}" >			
											<option value="42" selected="selected">(42) Ñuble</option>
										</c:if>
										<c:if test="${codigo != 42}" >			
											<option value="42">(42) Ñuble</option>
										</c:if>
										<c:if test="${codigo == 43}" >			
											<option value="43" selected="selected">(43) Bío-Bío</option>
										</c:if>
										<c:if test="${codigo != 43}" >			
											<option value="43">(43) Bío-Bío</option>
										</c:if>
										<c:if test="${codigo == 45}" >			
											<option value="45" selected="selected">(45) Cautín-Malleco</option>
										</c:if>
										<c:if test="${codigo != 45}" >			
											<option value="45">(45) Cautín-Malleco</option>
										</c:if>
										<c:if test="${codigo == 63}" >		
											<option value="63" selected="selected">(63) Ranco-Valdivia</option>
										</c:if>
										<c:if test="${codigo != 63}" >			
											<option value="63">(63) Ranco-Valdivia</option>
										</c:if>
										<c:if test="${codigo == 64}" >			
											<option value="64" selected="selected">(64) Osorno</option>
										</c:if>
										<c:if test="${codigo != 64}" >			
											<option value="64">(64) Osorno</option>
										</c:if>
										<c:if test="${codigo == 65}" >			
											<option value="65" selected="selected">(65) Chiloé-Llanquihue-Palena </option>
										</c:if>
										<c:if test="${codigo != 65}" >			
											<option value="65">(65) Chiloé-Llanquihue-Palena </option>
										</c:if>
										<c:if test="${codigo == 67}" >			
											<option value="67" selected="selected">(67) Aisén-Capitán Prat-Coihaique-General Carrera</option>
										</c:if>
										<c:if test="${codigo != 67}" >			
											<option value="67">(67) Aisén-Capitán Prat-Coihaique-General Carrera</option>
										</c:if>
										<c:if test="${codigo == 68}" >			
											<option value="68" selected="selected">(68) Aisén(Puyuhuapi)</option>
										</c:if>
										<c:if test="${codigo != 68}" >			
											<option value="68">(68) Aisén(Puyuhuapi)</option>
										</c:if>
										<c:if test="${codigo == 61}" >			
											<option value="61" selected="selected">(61) Antártica Chilena-Magallanes-Tierra del Fuego-Última Esperanza</option>
										</c:if>
										<c:if test="${codigo != 61}" >			
											<option value="61">(61) Antártica Chilena-Magallanes-Tierra del Fuego-Última Esperanza</option>
										</c:if>
									</optgroup>	
								</select>							
							</div>
							<div class="col-xs-5">
								<div class="input-group">
									<c:if test="${bandera == 0}" >
										<input type="tel" class="form-control" id="telefono" name="telefono" placeholder="Ej:2323421" value="+56-${codigo}-${telefono}" onkeypress="return funcionMaestra(event,'fijo',1)" >			
									</c:if>
									<c:if test="${bandera == 1}" >	
										<input type="tel" class="form-control" id="telefono" name="telefono" placeholder="Ej:2323421" value="" onkeypress="return funcionMaestra(event,'fijo',1)" >		
									</c:if>
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-phone-alt"></span>
									</span>
								</div>	
							</div>	
						</div>
						<div class="form-group">
							<label class="col-xs-3 control-label" for="celular">Celular:</label>
							<div class="col-xs-9">
								<div class="input-group">
									<input type="tel" class="form-control" id="celular" name="celular" placeholder="Ej: +56998345670" value="${supervisor.sup_celular}" onkeypress="return funcionMaestra(event,'celular',1)">
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-phone"></span>
									</span>
								</div>	
							</div>	
						</div>
						<div class="form-group">
						<label class="col-xs-3 control-label" for="profesion">Profesión:</label>
							<div class="col-xs-9">
								<input type="text" maxlength="20" class="form-control" id="profesion" name="profesion" value="${supervisor.sup_profesion}"
								 onkeypress="return funcionMaestra(event,'nombres','1')">
							</div>
						</div>
						<div class="form-group">
						<label class="col-xs-3 control-label" for="cargo">Cargo:</label>
							<div class="col-xs-9">
								<input type="text" maxlength="20" class="form-control" id="cargo" name="cargo" value="${supervisor.sup_cargo}"
								 onkeypress="return funcionMaestra(event,'nombres','1')">
							</div>
						</div>
						<div class="form-group">
							<label class="col-xs-2 col-sm-2 col-md-2 col-lg-2 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1  control-label" for="estado">Estado:</label>
							<div class="col-xs-9">
									<input type="text" maxlength="20" class="form-control" id="estado" name="estado" readonly="readonly" value="${supervisor.sup_estado}"
								 onkeypress="return funcionMaestra(event,'nombres','1')">
							</div>
					</div>
						<button type="submit" onclick="return validar_supervisor(event,1)" class="btn btn-primary">Modificar Datos</button>

					</form>	
				</div>
			</div>
	</jsp:body>
</t:plantillaSupervisor>