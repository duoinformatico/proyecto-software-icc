<%@ page language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<t:plantillaSupervisor>
    <jsp:attribute name="header">
    	<script>
		$(document)
				.ready(
						function() {
							$('#datatable')
									.dataTable(
											{
												"sDom" : "<'row'<'col-xs-6'T><'col-xs-6'f>r>t<'row'<'col-xs-6'i><'col-xs-6'p>>",
												"sPaginationType" : "bootstrap"
											});
						});
		</script>
    </jsp:attribute>			
    <jsp:body>
    	 <div class="row">
			<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1">
				<legend> Bit�coras de ${alumno.alu_nombres} ${alumno.alu_apellido_p} ${alumno.alu_apellido_m}</legend>
			</div>
		 </div>
		 <c:if test="${exito == 1}">
				<div class="col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1">
					<div class="alert alert-success alert-dismissible" role="alert">
					  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					  <span class="glyphicon glyphicon-ok"></span>
					  <strong>Exito</strong> ${mensaje}
					</div>
				</div>
			</c:if>
			<c:if test="${exito == 0}">
				<div class="col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1">
					<div class="alert alert-danger alert-dismissible" role="alert">
					  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					  <span class="glyphicon glyphicon-remove"></span>
					  <strong>Error</strong> ${mensaje}
					</div>
				</div>
			</c:if>
	     	<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  columna">	
					<div class="table-responsive">
							<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered table-hover" id="datatable">
								<thead>
								  <tr >
								     <th>Fecha Inicio</th>
								     <th>Fecha T�rmino</th>
								     <th>Pr�ctica</th>
								     <th>Fecha �ltima Edici�n</th>
								     <th>Acciones</th>
								  </tr>
						        </thead>
						        <tbody>
						        	<c:forEach items="${bitacoraList}" var="bitacora" >
						        	<tr>
						        		<td>
						        			<fmt:formatDate value="${bitacora.bit_fecha_ini}" var="fecha_ini" type="date" pattern="dd-MM-yyyy" />
						        			${fecha_ini}
						        		</td>
						        		<td>
						        			<fmt:formatDate value="${bitacora.bit_fecha_fin}" var="fecha_fin" type="date" pattern="dd-MM-yyyy" />
						        			${fecha_fin}
						        		</td>
						        		<td>${practica.pra_obra_nombre}</td>
						        		<td>${bitacora.bit_fecha_edicion}</td>
						        		<td>
						                    <button type="button" id="bitacora" name="bitacora" value="bitacora"  class="btn btn-primary  btn-xs" onclick='consultarBitacora("${bitacora.bit_id}","${practica.plan_id}")'><span class="glyphicon glyphicon-folder-open"></span> Ver Bit�cora</button>
						        			<c:if test="${bitacora.bean_uno == 1}">
													<button type="button" id="bitacora" name="bitacora" value="bitacora" class="btn btn-primary  btn-xs"  onclick='realizarObservacion("${bitacora.bit_id}")'><span class="glyphicon glyphicon-pencil"></span> Realizar Observaci�n</button>
											</c:if>    			
						        		</td>
		     		
						        	</tr>
						        	</c:forEach>
						        </tbody>
						    </table>
					</div>
				</div>
			</div>
			<div id="div_observacion">
			
			</div>
			<div id="div_consultar">
			
			</div>
	</jsp:body>
</t:plantillaSupervisor>