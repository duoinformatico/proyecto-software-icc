<%@ page language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:plantillaSupervisor>
    <jsp:attribute name="header">
    	
    </jsp:attribute>
    <jsp:body>
    	<div class="row">
    		<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 columna">
    			<div class="col-xs-12 col-sm-12 col-sm-offset-1 col-md-offset-1 columna">
	    			<h3>Bienvenid@ al Sistema: ${usuario} (Perfil Supervisor)</h3>
	    		</div>
    		</div>
    	</div>
    	<div class="row">
    		<div class="col-xs-8 col-sm-8 col-xs-offset-2 col-sm-offset-2 col-md-offset-2 columna">
    			<h3>- Usted tiene a cargo a <span class="badge">${total}</span> Estudiantes con Pr�ctica Activa este Semestre.</h3>

    	</div>
    </jsp:body>
</t:plantillaSupervisor>