<%@ page language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:plantillaSupervisor>
<jsp:attribute name="header">
    </jsp:attribute>
    <jsp:body>
    <script>
		$(document)
				.ready(
						function() {
							$('#datatable')
									.dataTable(
											{
												"sDom" : "<'row'<'col-xs-6'T><'col-xs-6'f>r>t<'row'<'col-xs-6'i><'col-xs-6'p>>",
												"sPaginationType" : "bootstrap"
											});
						});
	</script>
    <script>
		$(document)
				.ready(
						function() {
							$('#tabla_evaluados')
									.dataTable(
											{
												"sDom" : "<'row'<'col-xs-6'T><'col-xs-6'f>r>t<'row'<'col-xs-6'i><'col-xs-6'p>>",
												"sPaginationType" : "bootstrap"
											});
						});
	</script>
    	<div class="row">
			<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1">
				<legend>Alumn@s Asignados</legend>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  columna">
				
				<ul class="nav nav-tabs" role="tablist">
				  <li class="active"><a href="#home" role="tab" data-toggle="tab">No Evaluados</a></li>
				  <li><a href="#profile" role="tab" data-toggle="tab">Evaluados</a></li>
				  <!--
				  <li><a href="#messages" role="tab" data-toggle="tab">Messages</a></li>
				  <li><a href="#settings" role="tab" data-toggle="tab">Settings</a></li>
				-->
				</ul>
				
				<!-- Tab panes -->
				<div class="tab-content">
				  <div class="tab-pane fade in active" id="home">
				  	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xs-offset-0 col-sm-offset-0 col-md-offset-0 col-lg-offset-0">
				  		<div class="table-responsive">
							<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered table-hover" id="datatable">
								<thead>
								  <tr >
								  	 <th>#</th>
								     <th>Pr�ctica</th>
								     <th>Alumno</th>
								     <th>Tipo Evaluacion</th>
								     <th>Estado</th>
								     <th>Acciones</th>
								  </tr>
						        </thead>
						        <tbody>				        		
						        	<c:forEach items="${evaluacionesList}" var="evaluacion">
						        	<c:if test="${evaluacion.eva_estado == 'Sin Evaluar'}">
						        	<c:set var="flag_finalizada" value="1"/>
						        	<c:forEach items="${practicasList}" var="practica">
						        	<c:if test="${evaluacion.pra_id == practica.pra_id && flag_finalizada == 1 && 
						        				 (practica.pra_estado == 'Finalizada' || practica.pra_estado == 'Comisionada' || practica.pra_estado == 'Evaluada')}">
						        		<c:set var="flag_finalizada" value="2"/>
						        	<tr>
						        		<td class="line_color_table">${evaluacion.pra_id}</td>
						        		<c:set var="flag_practica" value="1"/>
						        		<c:set var="flag_alumno" value="1"/>
						        		<c:set var="flag_instrumento" value="1"/>
						        		<c:forEach items="${practicasList}" var="practica">
						        			<c:if test="${practica.pra_id == evaluacion.pra_id && flag_practica == 1}" >
						        				<td>PRA_ID: ${practica.pra_id} - ${practica.emp_rut} <c:forEach items="${empresasList}" var="empresa"><c:if test="${empresa.emp_rut == practica.emp_rut}" >${empresa.emp_nombre}</c:if></c:forEach><br>Nombre Obra: ${practica.pra_obra_nombre}</td>
						        				<c:set var="flag_practica" value="2"/>				        				
						        				<c:forEach items="${alumnosList}" var="alumno">
						        					<c:if test="${practica.alu_rut == alumno.alu_rut && flag_alumno == 1}" >
						        						<td>${alumno.alu_rut} ${alumno.alu_nombres} ${alumno.alu_apellido_p} ${alumno.alu_apellido_m}</td>
						        						<c:set var="flag_alumno" value="2"/>
						        					</c:if>
						        				</c:forEach>
						        			</c:if>				        			
						        		</c:forEach>
						        		<td>${evaluacion.eva_nombre}</td>
						        		<td>${evaluacion.eva_estado}</td>
				        				<c:forEach items="${instrumentosList}" var="instrumento">
						        			<c:if test="${instrumento.rol_id == evaluacion.rol_id && flag_instrumento == 1}">
						        				<td><button type="button" class="btn btn-primary  btn-xs" id="'${instrumento.ins_id}'" onclick='verRubrica("${instrumento.ins_id}", "${evaluacion.sup_rut}", "${evaluacion.pra_id}", "${evaluacion.rol_id}")'>Evaluar</button></td>
						        				<c:set var="flag_instrumento" value="2"/>
						        			</c:if>
						        		</c:forEach>        				
						        	</tr>
						        	</c:if>
						        	</c:forEach>
						        	</c:if>
						        	</c:forEach>
						        </tbody>
						    </table>
						</div>
				  	</div>
				  </div>
				  <div class="tab-pane fade" id="profile">
				  	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xs-offset-0 col-sm-offset-0 col-md-offset-0 col-lg-offset-0">
				  		<div class="table-responsive">
							<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered table-hover" id="tabla_evaluados">
								<thead>
								  <tr >
								  	 <th>#</th>
								     <th>Pr�ctica</th>
								     <th>Alumno</th>
								     <th>Tipo Evaluacion</th>
								     <th>Nota</th>
								     <th>Observacion</th>
								     <th>Acciones</th>
								  </tr>
						        </thead>
						        <tbody>				        		
						        	<c:forEach items="${evaluacionesList}" var="evaluacion">
						        	<c:if test="${evaluacion.eva_estado == 'Evaluado'}">						        	
						        	<tr>
						        		<td>${evaluacion.pra_id}</td>
						        		<c:set var="flag_practica" value="1"/>
						        		<c:set var="flag_alumno" value="1"/>
						        		<c:set var="flag_instrumento" value="1"/>
						        		<c:forEach items="${practicasList}" var="practica">
						        			<c:if test="${practica.pra_id == evaluacion.pra_id && flag_practica == 1}" >
						        				<td>PRA_ID: ${practica.pra_id} - ${practica.emp_rut} <c:forEach items="${empresasList}" var="empresa"><c:if test="${empresa.emp_rut == practica.emp_rut}" >${empresa.emp_nombre}</c:if></c:forEach><br>Nombre Obra: ${practica.pra_obra_nombre}</td>
						        				<c:set var="flag_practica" value="2"/>				        				
						        				<c:forEach items="${alumnosList}" var="alumno">
						        					<c:if test="${practica.alu_rut == alumno.alu_rut && flag_alumno == 1}" >
						        						<td>${alumno.alu_rut} ${alumno.alu_nombres} ${alumno.alu_apellido_p} ${alumno.alu_apellido_m}</td>
						        						<c:set var="flag_alumno" value="2"/>
						        					</c:if>
						        				</c:forEach>
						        			</c:if>				        			
						        		</c:forEach>
						        		<td>${evaluacion.eva_nombre}</td>
						        		<td>${evaluacion.eva_nota}</td>
						        		<td>${evaluacion.eva_observacion}</td>
				        				<c:forEach items="${instrumentosList}" var="instrumento">
						        			<c:if test="${instrumento.rol_id == evaluacion.rol_id && flag_instrumento == 1}">
						        				<td><button type="button" class="btn btn-primary btn-xs" id="'${instrumento.ins_id}'" onclick='verDetalleEvaluacion("supervisor", "${evaluacion.sup_rut}", "${evaluacion.pra_id}", "${evaluacion.rol_id}", "${evaluacion.getFecha()}")'>Detalle</button></td>
						        				<c:set var="flag_instrumento" value="2"/>
						        			</c:if>
						        		</c:forEach>        				
						        	</tr>
						        	</c:if>
						        	</c:forEach>
						        </tbody>
						    </table>
						</div>
				  	</div>
				  </div>
				  <!-- 
				  <div class="tab-pane" id="messages">Hola TAB 3</div>
				  <div class="tab-pane" id="settings">Hola TAB 4</div>
				  -->
				</div>
				
			</div>
		</div>
		<div id="div_instrumento">
		</div>
    </jsp:body>
</t:plantillaSupervisor>