<%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:plantillaFormularioAdmin>
	<jsp:attribute name="header">		
    </jsp:attribute>
    <jsp:body>
    	<script type="text/javascript">
			$(document).ready(function () {
				for (var i = 1; i <= $("#total").val(); i++) {
					$("#codigo_"+i).bind('copy paste drop', function (e) {
			           e.preventDefault();
			        });       
			        $("#codigo_"+i).focusin(function() {
			        	$("#codigo_"+i).attr('autocomplete', 'off');
			        	$("#codigo_"+i).attr('maxlength', '6');
			        });
			        $("#codigo_"+i).focusout(function() {
			        	$("#codigo_"+i).removeAttr('autocomplete');
			        	$("#codigo_"+i).removeAttr('maxlength');
			        });
			        
			        $("#nombre_"+i).bind('copy paste drop', function (e) {
			           e.preventDefault();
			        });       
			        $("#nombre_"+i).focusin(function() {
			        	$("#nombre_"+i).attr('autocomplete', 'off');
			        	$("#nombre_"+i).attr('maxlength', '100');
			        });
			        $("#nombre_"+i).focusout(function() {
			        	$("#nombre_"+i).removeAttr('autocomplete');
			        	$("#nombre_"+i).removeAttr('maxlength');
			        });
			        
			        $("#horas_"+i).bind('copy paste drop', function (e) {
			           e.preventDefault();
			        });       
			        $("#horas_"+i).focusin(function() {
			        	$("#horas_"+i).attr('autocomplete', 'off');
			        	$("#horas_"+i).attr('maxlength', '4');
			        });
			        $("#horas_"+i).focusout(function() {
			        	$("#horas_"+i).removeAttr('autocomplete');
			        	$("#horas_"+i).removeAttr('maxlength');
			        });
			        
			        $("#creditos_"+i).bind('copy paste drop', function (e) {
			           e.preventDefault();
			        });       
			        $("#creditos_"+i).focusin(function() {
			        	$("#creditos_"+i).attr('autocomplete', 'off');
			        	$("#creditos_"+i).attr('maxlength', '2');
			        });
			        $("#creditos_"+i).focusout(function() {
			        	$("#creditos_"+i).removeAttr('autocomplete');
			        	$("#creditos_"+i).removeAttr('maxlength');
			        });
				}
			});
		</script>
			<div class="row">
				<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7 col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3">
					<legend>Nuevo Plan de Estudio: ${nombre}</legend>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
				<form class="form-horizontal" role="form" method="POST" action="planesServlet.do">
						<div class="table-responsive">
							<table cellpadding="0" cellspacing="0" border="0" class="display table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<c:forEach var="i" begin="0" end="${semestres}">
											<c:if test="${i == 0}">
							        			<th>#</th>
							        		</c:if>
							        		<c:if test="${i != 0}">
							        			<th>Sem # ${i}</th>
							        		</c:if>											
										</c:forEach>
									</tr>
								</thead>
						        <tbody>
						        	<c:set var="n" value="1"/>
						        	<c:forEach var="j" begin="1" end="${max}">
							        	<tr>
							        		<c:forEach var="i" begin="0" end="${semestres}">
							        		<c:if test="${i == 0}">
							        			<td class="vert-align" align="center">${j}</td>
							        		</c:if>
							        		<c:if test="${i != 0}">
								        		<c:if test="${asignaturas_semestre[i-1] >= j}">
								        			<td>
								        				<input type="tel" class="form-control" id="codigo_${n}" name="codigo_${n}" placeholder="Codigo" maxlength="6" onkeypress="return funcionMaestra(event, 'num', 1)" autocomplete="off">
										        		<input type="text" class="form-control" id="nombre_${n}" name="nombre_${n}" placeholder="Nombre" maxlength="100" autocomplete="off">
										        		<input type="tel" class="form-control" id="horas_${n}" name="horas_${n}" placeholder="Horas" maxlength="4" onkeypress="return funcionMaestra(event, 'num', 1)" autocomplete="off">
										        		<input type="tel" class="form-control" id="creditos_${n}" name="creditos_${n}" placeholder="Creditos" maxlength="2" onkeypress="return funcionMaestra(event, 'num', 1)" autocomplete="off">
										        		<input type="hidden" class="form-control" id="semestre_${n}" name="semestre_${n}" value="${i}">
										        		<select class="form-control" id="tipo_${n}" name="tipo_${n}">
															<option value="">Tipo Asignatura...</option>
															<c:forEach var="tipo" items="${asignatura_tipo}">
																<option value="${tipo.asi_tp_id}">${tipo.asi_tp_nombre}</option>
															</c:forEach>
														</select>
										        	</td>
										        	<c:set var="n" value="${n+1}"/>
								        		</c:if>
								        		<c:if test="${asignaturas_semestre[i-1] < j}">
								        			<td>
										        	</td>
								        		</c:if>								        			
							        		</c:if>								        										        	
								        	</c:forEach>
							        	</tr>
						        	</c:forEach>
						        </tbody>
						    </table>
						</div>
						<button type="submit" class="btn btn-primary" onclick="return validar_plan(event,1)">Registrar Plan de Estudio</button>
						<input type="hidden" class="form-control" id="carrera" name="carrera" value="${carrera}">
						<input type="hidden" class="form-control" id="nombre" name="nombre" value="${nombre}">
						<input type="hidden" class="form-control" id="semestres" name="semestres" value="${semestres}">
						<input type="hidden" class="form-control" id="max" name="max" value="${max}">
						<input type="hidden" class="form-control" id="total" name="total" value="${total}">
						<input type="hidden" class="form-control" id="etapa" name="etapa" value="2">
						<div class="form-group">
						<label class="col-xs-3 control-label" for=""></label>
							<div class="col-xs-9">
								
							</div>
						</div>
					</form>
					</div>
			</div>
	</jsp:body>
</t:plantillaFormularioAdmin>
