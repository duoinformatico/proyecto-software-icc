<%@ page language="java"  pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:plantillaTablas>
	<jsp:attribute name="header">
    </jsp:attribute>
    <jsp:body>
    <script type="text/javascript">
			$(document).ready(function() {
				$('#categorias-datatable')
						.dataTable(
								{
									"sDom" : "<'row'<'col-xs-6'T><'col-xs-6'f>r>t<'row'<'col-xs-6'i><'col-xs-6'p>>",
									"sPaginationType" : "bootstrap"
								});
				$('#roles-datatable')
				.dataTable(
						{
							"sDom" : "<'row'<'col-xs-6'T><'col-xs-6'f>r>t<'row'<'col-xs-6'i><'col-xs-6'p>>",
							"sPaginationType" : "bootstrap"
						});
				$('#instrumentos-datatable')
				.dataTable(
						{
							"sDom" : "<'row'<'col-xs-6'T><'col-xs-6'f>r>t<'row'<'col-xs-6'i><'col-xs-6'p>>",
							"sPaginationType" : "bootstrap"
						});
				$("#carrera").change(actualizarListaPracticas);
				$("#asignatura").change(actualizarListaRoles);
			});
			</script>
			<c:if test="${exito == 1}">
			<br>
				<div class="col-xs-offset-1 col-xs-10">
					<div class="alert alert-success alert-dismissible" role="alert">
					  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					  <span class="glyphicon glyphicon-ok"></span>
					  <strong>Exito</strong> ${mensaje}.<br>
					  Escalas (${fn:length(escalasList)}) - Categorías (${fn:length(categoriasList)}) - Roles (${fn:length(rolesList)}) - Prácticas (${fn:length(gestionesList)})
					</div>
				</div>
			</c:if>
			<c:if test="${exito == 0}">
			<br>
				<div class="col-xs-offset-1 col-xs-10">
					<div class="alert alert-danger alert-dismissible" role="alert">
					  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					  <span class="glyphicon glyphicon-remove"></span>
					  <strong>Error</strong> ${mensaje}<br>
					  <c:if test="${fn:length(escalasList) > 0}">
					  	Escalas (${fn:length(escalasList)})<span class="glyphicon glyphicon-ok"></span> -
					  </c:if>
					  <c:if test="${fn:length(escalasList) <= 0}">
					  	Escalas (${fn:length(escalasList)})<span class="glyphicon glyphicon-remove"></span> -
					  </c:if>
					  <c:if test="${fn:length(categoriasList)-15 > 2}">
					  	Escalas (${fn:length(categoriasList)})<span class="glyphicon glyphicon-ok"></span> -
					  </c:if>
					  <c:if test="${fn:length(categoriasList)-15 <= 2}">
					  	Escalas (${fn:length(categoriasList)})<span class="glyphicon glyphicon-remove"></span> -
					  </c:if>
					  <c:if test="${fn:length(rolesList) > 0}">
					  	Escalas (${fn:length(rolesList)})<span class="glyphicon glyphicon-ok"></span> -
					  </c:if>
					  <c:if test="${fn:length(rolesList) <= 0}">
					  	Escalas (${fn:length(rolesList)})<span class="glyphicon glyphicon-remove"></span> -
					  </c:if>
					  <c:if test="${fn:length(gestionesList) > 0}">
					  	Escalas (${fn:length(gestionesList)})<span class="glyphicon glyphicon-ok"></span>
					  </c:if>
					  <c:if test="${fn:length(gestionesList) <= 0}">
					  	Escalas (${fn:length(gestionesList)})<span class="glyphicon glyphicon-remove"></span>
					  </c:if>
					</div>
				</div>
			</c:if>
			<div class="row">
				<div class="col-xs-offset-1 col-xs-10">

						<br>
						<div class="alert alert-warning alert-dismissible" role="alert">
						  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
						  <span class="glyphicon glyphicon-warning-sign form-control-feedback"></span><strong> Advertencia!</strong> Tenga en cuenta que crear una nueva rúbrica sobreescribirá el actual instrumento de evaluación si existe para una asignatura.
						</div>
						<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target=".bs-example-modal-lg">Registrar Nueva Rúbrica</button>
				</div>
			</div>
			
			<div id="formulario_rubrica">
				<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
				  <div class="modal-dialog modal-lg">
				    <div class="modal-content">
				      <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				        <h4 class="modal-title" id="myModalLabel">Configuración Creación Rúbrica</h4>
				    </div>
				    
				    <div class="modal-body">
				    	<form class="form-horizontal" id="formulario_config_rubrica" role="form" method="POST" action="FormularioInstrumentoControlador">
							
							
							
							<div class="form-group">
								<label class="col-xs-3 control-label" for="escala">Escala:</label>
								<div class="col-xs-9">							
									<select class="form-control" id="escala" name="escala" required>
									<option value="">Seleccione Escala...</option>
									<c:forEach items="${escalasList}" var="escala" >
										<option value="${escala.esc_id}">${escala.esc_nombre}</option>
									</c:forEach>
									</select>							
								</div>
							</div>
							
							
							<div class="form-group">
								<label class="col-xs-3 control-label" for="carrera">Carrera:</label>
								<div class="col-xs-9">							
									<select class="form-control" id="carrera" name="carrera" required>
									<option value="">Seleccione Carrera...</option>
									<c:forEach var="carrera" items="${carrerasList}" >
										<option value="${carrera.car_codigo}">${carrera.car_nombre} ( Cod: ${carrera.car_codigo} - Sede ${carrera.car_sede})</option>
									</c:forEach>
									</select>							
								</div>
							</div>
							
							<div id="div_asignaturas">
							<div class="form-group">
								<label class="col-xs-3 control-label" for="asignatura">Asignatura:</label>
								<div class="col-xs-9">							
									<select class="form-control" id="asignatura" name="asignatura" required>
										<option value="">Seleccione Asignatura...</option>
									</select>							
								</div>
							</div>
							</div>
							
							<div id="div_roles">
							<div class="form-group">
								<label class="col-xs-3 control-label" for="rol">Rol Evaluador:</label>
								<div class="col-xs-9">							
									<select class="form-control" id="rol" name="rol" required>
										<option value="">Seleccione Rol Evaluador...</option>
									</select>							
								</div>
							</div>
							</div>
							
							<div class="form-group">
								<label class="col-xs-3 control-label" for="nombre_instrumento">Nombre Instrumento:</label>
								<div class="col-xs-4">
									<input type="text" class="form-control" id="nombre_instrumento" name="nombre_instrumento" placeholder="Ej: Evaluación Supervisor Gestión Op III" required onkeypress="return funcionMaestra(event,'dire','1')">
								</div>
							
								<label class="col-xs-2 control-label" for="items">Cantidad Items:</label>
								<div class="col-xs-3">
									<input type="tel" class="form-control" id="items" name="items" placeholder="Ej: 12" required maxlength="2" onkeypress="return funcionMaestra(event,'num','1')">
								</div>
							</div>
							
								<input type="hidden" class="form-control" id="etapa" name="etapa" value="1">			
							
							
							<div class="modal-footer">				      
						        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
						        <button type="submit" class="btn btn-primary">Guardar</button>				        
						     </div>
							
				    	</form>
				    	
				    </div>				    
				    
				    </div>
				  </div>
				</div>
			</div>
			<br><br>
			
			<div class="row">			
				<div class="col-xs-12 columna">
					<legend>Rúbricas Registradas</legend>
						<div class="table-responsive">
							<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered table-hover" id="instrumentos-datatable">
								<thead>
								  <tr>
								     <th>Codigo</th>
								     <th>Nombre</th>
								     <th>Rúbricas</th>
								  </tr>
						        </thead>
						        <tbody>
						        	<c:forEach items="${asignaturasList}" var="asignatura" >
						        	<tr>
						        		<td class="vert-align" align="center">${asignatura.asi_codigo}-${asignatura.plan_id}</td>
						        		<td class="vert-align">${asignatura.asi_nombre}</td>
						        		<td>
						        			<c:forEach items="${asignatura.instrumentos}" var="instrumento" >
						        				<c:if test="${instrumento.ins_estado == 'activo'}">
												  	<button type="button" class="btn btn-primary btn-xs" onclick='verPruebaInstrumento("supervisor", "${instrumento.ins_id}")'><span class="glyphicon glyphicon-ok"></span> ${instrumento.ins_nombre}</button><br>
												</c:if>
												<c:if test="${instrumento.ins_estado == 'inactivo'}">
												  	<button type="button" class="btn btn-danger btn-xs" onclick='verPruebaInstrumento("supervisor", "${instrumento.ins_id}")'><span class="glyphicon glyphicon-remove"></span> ${instrumento.ins_nombre}</button><br>
												</c:if>
						        			</c:forEach>
						        		</td>
						        	</tr>
						        	</c:forEach>
						        </tbody>
						    </table>
						</div>
						<div id="div_instrumento">
						</div>
					</div>
			</div>
			
			<div class="row">			
				<div class="col-xs-12 columna">
					<legend>Escalas Registradas</legend>
						<div class="table-responsive">
							<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered table-hover" id="datatable">
								<thead>
								  <tr>
								     <th>ID</th>
								     <th>Nombre</th>
								     <th># Opciones</th>
								     <th>Min Valor</th>
								     <th>Max Valor</th>
								     <th>Intervalo</th>
								     <th>% Exigencia</th>
								     <th>Valores</th>
								  </tr>
						        </thead>
						        <tbody>
						        	<c:forEach items="${escalasList}" var="escala" >
						        	<tr>
						        		<td align="center">${escala.esc_id}</td>
						        		<td>${escala.esc_nombre}</td>
						        		<td align="center">${escala.esc_num_opciones}</td>
						        		<td align="center">${escala.esc_min_valor}</td>
						        		<td align="center">${escala.esc_max_valor}</td>
						        		<td align="center">${escala.esc_intervalo}</td>
						        		<td align="center">${escala.esc_exigencia}</td>
						        		<td>
						        		<c:forEach items="${escala.valores}" var="valores" >
							        		${valores.val_nombre}(${valores.val_valor}) -
						        		</c:forEach>
						        		</td>
						        	</tr>
						        	</c:forEach>
						        </tbody>
						    </table>
						</div>
					</div>
			</div>
			<div class="row">			
				<div class="col-xs-12 columna">
					<legend>Categorías Registradas</legend>
						<div class="table-responsive">
							<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered table-hover" id="categorias-datatable">
								<thead>
								  <tr>
								     <th>ID</th>
								     <th>Nombre</th>

								  </tr>
						        </thead>
						        <tbody>
						        	<c:forEach items="${categoriasList}" var="categoria">
						        	<tr>
						        		<td align="center">${categoria.cat_id}</td>
						        		<td>${categoria.cat_nombre}</td>
						        	</tr>
						        	</c:forEach>
						        </tbody>
						    </table>
						</div>
					</div>
			</div>
			<div class="row">			
				<div class="col-xs-12 columna">
					<legend>Roles Registrados</legend>
						<div class="table-responsive">
							<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered table-hover" id="roles-datatable">
								<thead>
								  <tr>
								     <th>ID</th>
								     <th>Plan Estudio</th>
								     <th>Asignatura</th>
								     <th>Nombre Rol</th>
								     <th>% Ponderación</th>

								  </tr>
						        </thead>
						        <tbody>
						        	<c:forEach items="${rolesList}" var="rol">
						        	<tr>
						        		<td align="center">${rol.rol_id}</td>
						        		<td align="center">${rol.plan_id}</td>
						        		<td align="center">${rol.asi_codigo}</td>
						        		<td>${rol.rol_nombre}</td>
						        		<td align="center">${rol.rol_ponderacion}</td>
						        	</tr>
						        	</c:forEach>
						        </tbody>
						    </table>
						</div>
					</div>
			</div>
			<div id="div_registro">
			</div>
	</jsp:body>
</t:plantillaTablas>