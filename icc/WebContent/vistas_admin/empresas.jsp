<%@ page language="java"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:plantillaTablas>
	<jsp:attribute name="header">
    </jsp:attribute>
    <jsp:body>
    	<div class="row">
			<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7 col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3">
				<legend>Lista de Empresas Registradas</legend>
			</div>
		</div>
		<c:if test="${exito == 1}">
				<div class="col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1">
					<div class="alert alert-success alert-dismissible" role="alert">
					  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					  <span class="glyphicon glyphicon-ok"></span>
					  <strong>Exito</strong> ${mensaje}
					</div>
				</div>
			</c:if>
			<c:if test="${exito == 0}">
				<div class="col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1">
					<div class="alert alert-danger alert-dismissible" role="alert">
					  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					  <span class="glyphicon glyphicon-remove"></span>
					  <strong>Error</strong> ${mensaje}
					</div>
				</div>
			</c:if>
		<div class="row">
				<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1">
					<button type="button" class="btn btn-primary  btn-xs"  id="reg" onclick='registrarEmpresa("1")'>Registrar Nueva Empresa</button>
				</div>
		</div>
		<br>
		<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 columna">
						<div class="table-responsive">
							<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered table-hover" id="datatable">
								<thead>
								  <tr >
								     <th>Rut</th>
								     <th>Nombre</th>
								     <th>Direcci&oacute;n</th>
								     <th>Modificar</th>
								     <th>Detalle</th>
								  </tr>
						        </thead>
						        <tbody>
						        	<c:forEach items="${empresaList}" var="empresa" >
						        	<tr>
						        		<td>${empresa.emp_rut}</td>
						        		<td>${empresa.emp_nombre}</td>
						        		<td>${empresa.emp_direccion}</td>
										<td><button type="button" class="btn btn-primary  btn-xs" id="'${empresa.emp_rut}'" onclick='modificarEmpresa("${empresa.emp_rut}")'>Modificar</button></td>
										<td><button type="button" class="btn btn-primary  btn-xs" id="'${empresa.emp_rut}'" onclick='verEmpresa("${empresa.emp_rut}")'>Detalle</button></td>
						        	</tr>
						        	</c:forEach>
						        </tbody>
						    </table>
						</div>
				</div>
		</div>
	</jsp:body>
</t:plantillaTablas>
<div id="div_empresa">
</div>
<div id="div_mod_emp">
</div>
<div id="div_registro">
</div>