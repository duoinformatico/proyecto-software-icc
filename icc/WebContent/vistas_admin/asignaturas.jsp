<%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:plantillaTablas>
	<jsp:attribute name="header">
		
    </jsp:attribute>
    <jsp:body>
			<div class="row">
				<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7 col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3">
					<legend>Lista de Asignaturas Registradas</legend>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 columna">
						<div class="table-responsive">
							<table cellpadding="0" cellspacing="0" border="0" class="display table table-striped table-bordered table-hover" id="datatable">
								<thead>
								  <tr >
								     <th>Nombre</th>
								     <th>Plan</th>
								     <th>Tipo Asignatura</th>
								  </tr>
						        </thead>
						        <tbody>
						        	<c:forEach items="${asignaturaList}" var="asignatura" >
						        	<tr>
						        		<td>${asignatura.asi_nombre}</td>
						        		<td>${asignatura.bean1}</td>
						        		<td>${asignatura.bean2}</td>										
						        	</tr>
						        	</c:forEach>
						        </tbody>
						    </table>
						</div>
					</div>
			</div>
	</jsp:body>
</t:plantillaTablas>
<div id="div_mod_asig">
</div>
<div id="div_registro">
</div>