<%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<script type="text/javascript" src="js/validaciones_admin.js"></script>
<t:plantillaFormularioAdmin>
	<jsp:attribute name="header">
		
    </jsp:attribute>
    <jsp:body>
			<div class="row">
				<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7 col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3">
					<legend>M�dulo de Generaci�n de Informes</legend>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<form class="form-horizontal" role="form" method="POST" action="#">
						<div class="form-group ">
							<label class="col-xs-3 control-label" for="informe">Seleccione el tipo de Informe:</label>
							<div class="col-xs-9">
								<select class="form-control" id="informe" name="informe">			
										<option value="gestiones">Informe de Gestiones</option>	
										<option value="regiones">Informe de Alumnos por Regiones</option>	
										<option value="comunas">Informe de Alumnos por Comunas</option>
										<option value="empresas">Informe de Estudiantes por Empresa Seg�n Pr�ctica</option>
										<option value="obras">Informe de Estudiantes por Tipo de Obra seg�n Pr�ctica</option>
										<option value="supervisores">Informe de Estudiantes por Supervisor seg�n Pr�ctica</option>
										<option value="areas">Informe de Estudiantes por Area seg�n Pr�ctica</option>
										<option value="academicos">Informe de Estudiantes por Acad�mico seg�n Pr�ctica</option>
										<option value="notas">Informe de Notas de Estudiantes por semestre seg�n Pr�ctica</option>
								</select>						
							</div>
						</div>
						
					</form>
					<div class="col-xs-offset-4 col-sm-offset-4 col-md-offset-4 col-lg-offset-4">
						<button class="btn btn-primary" onclick="buscarInforme()"><span class="glyphicon glyphicon-search"></span> Inspecionar</button>
					</div>
					<br>
					<div id="div_usuario">
					</div>
				</div>
			</div>
			<br>
			<div class="row">
				
			</div>
	</jsp:body>
</t:plantillaFormularioAdmin>

