<%@ page language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:plantillaTablas>
    <jsp:attribute name="header">
  	
    </jsp:attribute>
    <jsp:body>
    <script type="text/javascript">
		/*$(document)
				.ready(
						function() {
							$('#datatable')
									.dataTable(
											{
												"sDom" : "<'row'<'col-xs-6'T><'col-xs-6'f>r>t<'row'<'col-xs-6'i><'col-xs-6'p>>",
												"sPaginationType" : "bootstrap"
											});
						});*/
	</script>
    <script type="text/javascript">
		$(document)
				.ready(
						function() {
							$('#tabla_evaluados')
									.dataTable(
											{
												"sDom" : "<'row'<'col-xs-6'T><'col-xs-6'f>r>t<'row'<'col-xs-6'i><'col-xs-6'p>>",
												"sPaginationType" : "bootstrap"
											});
						});
	</script>
    	<div class="row">
			<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1">
				<legend>Historial de Evaluaciones</legend>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  columna">
				
				<ul class="nav nav-tabs" role="tablist">
				<!-- 
				  <li class="active"><a href="#home" role="tab" data-toggle="tab">Gesti�n Operativa I</a></li>
				   <li><a href="#home" role="tab" data-toggle="tab">Gesti�n Operativa II</a></li>
				  <li><a href="#home" role="tab" data-toggle="tab">Gesti�n Operativa III</a></li>
				  <li><a href="#home" role="tab" data-toggle="tab">Gesti�n Profesional</a></li>
				
				  <li><a href="#messages" role="tab" data-toggle="tab">Messages</a></li>
				  <li><a href="#settings" role="tab" data-toggle="tab">Settings</a></li>
				-->
				</ul>
				
				<!-- Tab panes -->
				<div class="tab-content">
				  <div class="tab-pane fade in active" id="home">
				  	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xs-offset-0 col-sm-offset-0 col-md-offset-0 col-lg-offset-0">
				  		<div class="table-responsive">
							<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered table-hover" id="datatable">
								<thead>
								  <tr >
								  	 <th>#</th>
								     <th>Pr�ctica</th>
								     <th>Alumno</th>
								     <th>Tipo Gesti�n</th>
								     <th>Evaluaciones</th>
								     <th>Nota</th>
								     <th>Informe</th>
								  </tr>
						        </thead>
						        <tbody>
						        <c:forEach items="${practicaList}" var="practica" >
						        <c:if test="${practica.pra_estado == 'Finalizada' || practica.pra_estado == 'Comisionada' || practica.pra_estado == 'Evaluada'}">
						        	<tr>
						        		<td class="vert-align" align="center">${practica.pra_id}</td>
						        		<td class="vert-align" align="left">${practica.emp_rut} <c:forEach items="${empresasList}" var="empresa"><c:if test="${empresa.emp_rut == practica.emp_rut}" >${empresa.emp_nombre}</c:if></c:forEach><br>Nombre Obra: ${practica.pra_obra_nombre}</td>
						        		<c:set var="flag_alumno" value="1"/>
						        		<c:forEach items="${alumnosList}" var="alumno">
						        					<c:if test="${practica.alu_rut == alumno.alu_rut && flag_alumno == 1}" >
						        						<td class="vert-align">${alumno.alu_rut} ${alumno.alu_nombres} ${alumno.alu_apellido_p} ${alumno.alu_apellido_m}</td>
						        						<c:set var="flag_alumno" value="2"/>
						        					</c:if>
						        		</c:forEach>
						        		<td class="vert-align" align="center">${practica.asi_codigo} - <c:forEach items="${asignaturasList}" var="asignatura"><c:if test="${asignatura.asi_codigo == practica.asi_codigo}" >${asignatura.asi_nombre}</c:if></c:forEach></td>
						        		<td class="vert-align">
						        		<c:set var="flag_nota" value="0"/>
						        		<c:forEach items="${evaluacionesAlumnoList}" var="evaluacion" >
						        			<c:set var="flag_instrumento" value="1"/>
						        			<c:if test="${evaluacion.pra_id == practica.pra_id && evaluacion.eva_nombre != 'Profesor Gu�a' && evaluacion.eva_estado == 'Evaluado'}">
						        				<!-- botones nota + nombre evaluacion -->
						        											        			
						        				<c:forEach items="${instrumentosList}" var="instrumento">
								        			<c:if test="${instrumento.rol_id == evaluacion.rol_id && flag_instrumento == 1}">
								        				<button type="button" class="btn btn-primary  btn-xs" id="'${instrumento.ins_id}'" onclick='verDetalleEvaluacion("alumno", "${evaluacion.alu_rut}", "${evaluacion.pra_id}", "${evaluacion.rol_id}", "${evaluacion.getFecha()}")'><span class="glyphicon glyphicon-ok"></span> ${evaluacion.eva_nombre} (${evaluacion.eva_nota})</button>
								        				<c:set var="flag_instrumento" value="2"/>
								        			</c:if>
								        		</c:forEach>								        				
						        			</c:if>
						        			<c:if test="${evaluacion.pra_id == practica.pra_id && evaluacion.eva_nombre != 'Profesor Gu�a' && evaluacion.eva_estado == 'Sin Evaluar'}">
						        				<!-- botones nota + nombre evaluacion -->
						        				<button type="button" class="btn btn-danger btn-xs" id="'${evaluacion.alu_rut}${evaluacion.pra_id}${evaluacion.rol_id}'"><span class="glyphicon glyphicon-remove"></span> ${evaluacion.eva_nombre}</button><br>
						        			</c:if>		
						        		</c:forEach>
						        		
						        		<c:set var="flag_nota" value="0"/>
						        		<c:forEach items="${evaluacionesSupervisorList}" var="evaluacion" >
						        			<c:set var="flag_instrumento" value="1"/>
						        			<c:if test="${evaluacion.pra_id == practica.pra_id && evaluacion.eva_nombre != 'Profesor Gu�a' && evaluacion.eva_estado == 'Evaluado'}">
						        				<!-- botones nota + nombre evaluacion -->								        			
						        				<c:forEach items="${instrumentosList}" var="instrumento">
								        			<c:if test="${instrumento.rol_id == evaluacion.rol_id && flag_instrumento == 1}">
								        				<button type="button" class="btn btn-primary  btn-xs" id="'${instrumento.ins_id}'" onclick='verDetalleEvaluacion("supervisor", "${evaluacion.sup_rut}", "${evaluacion.pra_id}", "${evaluacion.rol_id}", "${evaluacion.getFecha()}")'><span class="glyphicon glyphicon-ok"></span> ${evaluacion.eva_nombre} (${evaluacion.eva_nota})</button>
								        				<c:set var="flag_instrumento" value="2"/>
								        			</c:if>
								        		</c:forEach>								        				
						        			</c:if>
						        			<c:if test="${evaluacion.pra_id == practica.pra_id && evaluacion.eva_nombre != 'Profesor Gu�a' && 
						        						  evaluacion.eva_estado == 'Sin Evaluar'}">
						        				<c:set var="flag_supervisor" value="1"/>
						        				<c:forEach items="${supervisoresList}" var="supervisor">
						        					<c:if test="${evaluacion.sup_rut == supervisor.sup_rut && flag_supervisor == 1}" >
						        						<!-- botones nota + nombre evaluacion -->
						        						<button type="button" class="btn btn-danger btn-xs" id="'${evaluacion.sup_rut}${evaluacion.pra_id}${evaluacion.rol_id}'"><span class="glyphicon glyphicon-remove"></span> ${evaluacion.eva_nombre} (${supervisor.sup_nombres} ${supervisor.sup_apellido_p} ${supervisor.sup_apellido_m})</button><br>
						        						<c:set var="flag_supervisor" value="2"/>
						        					</c:if>
						        				</c:forEach>
						        			</c:if>		
						        		</c:forEach>
						        		
						        		<c:set var="flag_nota" value="0"/>
						        		<c:forEach items="${evaluacionesAcademicoList}" var="evaluacion" >
						        			<c:set var="flag_instrumento" value="1"/>
						        			<c:if test="${evaluacion.pra_id == practica.pra_id && evaluacion.eva_nombre != 'Profesor Gu�a' && evaluacion.eva_estado == 'Evaluado'}">
						        				<!-- botones nota + nombre evaluacion -->								        			
						        				<c:forEach items="${instrumentosList}" var="instrumento">
								        			<c:if test="${instrumento.rol_id == evaluacion.rol_id && flag_instrumento == 1}">
								        				<button type="button" class="btn btn-primary  btn-xs" id="'${instrumento.ins_id}'" onclick='verDetalleEvaluacion("academico", "${evaluacion.aca_rut}", "${evaluacion.pra_id}", "${evaluacion.rol_id}", "${evaluacion.getFecha()}")'><span class="glyphicon glyphicon-ok"></span> ${evaluacion.eva_nombre} (${evaluacion.eva_nota})</button>
								        				<c:set var="flag_instrumento" value="2"/>
								        			</c:if>
								        		</c:forEach>								        				
						        			</c:if>
						        			<c:if test="${evaluacion.pra_id == practica.pra_id && evaluacion.eva_nombre != 'Profesor Gu�a' && 
						        						  evaluacion.eva_estado == 'Sin Evaluar'}">
						        				<c:set var="flag_academico" value="1"/>
						        				<c:forEach items="${academicosList}" var="academico">
						        					<c:if test="${evaluacion.aca_rut == academico.aca_rut && flag_academico == 1}" >
						        						<!-- botones nota + nombre evaluacion -->
						        						<button type="button" class="btn btn-danger btn-xs" id="'${evaluacion.aca_rut}${evaluacion.pra_id}${evaluacion.rol_id}'"><span class="glyphicon glyphicon-remove"></span> ${evaluacion.eva_nombre} (${academico.aca_nombres} ${academico.aca_apellido_p} ${academico.aca_apellido_m})</button><br>
						        						<c:set var="flag_academico" value="2"/>
						        					</c:if>
						        				</c:forEach>
						        				</c:if>		
						        		</c:forEach>
						        		
						        		</td>
						        		</td>
						        		<c:if test="${practica.pra_nota > 0.0}">
						        			<td class="vert-align" align="center"><button type="button" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-ok"></span> ${practica.pra_nota}</button></td>
						        		</c:if>
						        		<c:if test="${practica.pra_nota == 0.0}">
						        			<td class="vert-align" align="center">No calculada</td>
						        		</c:if>
						        		
						        		
						        		
						        		<c:set var="flag_informe" value="1"/>
						        		<c:forEach items="${estadosList}" var="estado">
				        					<c:if test="${(estado.pra_id == practica.pra_id) && (estado.bean_uno == 0) && flag_informe == 1}">
				        						<td class="vert-align" align="center"><button type="button" class="btn btn-success  btn-sm" id="'${estado.pra_id}'" onclick='subirInforme("${estado.pra_id}")'><span class="glyphicon glyphicon-open"></span> Subir Informe</button></td>
				        						<c:set var="flag_informe" value="2"/>
				        					</c:if>
				        					<c:if test="${(estado.pra_id == practica.pra_id) && (estado.bean_uno == 1) && flag_informe == 1}">
				        						<td class="vert-align" align="center"><a href="FormularioInformeControlador?fileName=${practica.pra_informe}" class="btn btn-success  btn-sm"> <span class="glyphicon glyphicon-save"></span> Descargar Informe</a></td>
				        						<c:set var="flag_informe" value="2"/>
				        					</c:if>									        					
				        				</c:forEach>
						        		
						        		</tr>
						        </c:if>
						        </c:forEach>		        		
						        </tbody>
						    </table>
						</div>
				  	</div>
				  </div>
				  <!-- 
				  <div class="tab-pane" id="messages">Hola TAB 3</div>
				  <div class="tab-pane" id="settings">Hola TAB 4</div>
				  -->
				</div>
				
			</div>
		</div>
		<div id="div_instrumento">
		</div>
		<div id="div_informe">
		</div>
    </jsp:body>
</t:plantillaTablas>