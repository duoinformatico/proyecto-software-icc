<%@ page language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:plantillaTablas>
    <jsp:attribute name="header">
  	
    </jsp:attribute>
    <jsp:body>
    	<div class="row">
			<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1">
				<legend>Pr�cticas Finalizadas</legend>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  columna">
				
				<ul class="nav nav-tabs" role="tablist">
				<!-- 
				  <li class="active"><a href="#home" role="tab" data-toggle="tab">Gesti�n Operativa I</a></li>
				   <li><a href="#home" role="tab" data-toggle="tab">Gesti�n Operativa II</a></li>
				  <li><a href="#home" role="tab" data-toggle="tab">Gesti�n Operativa III</a></li>
				  <li><a href="#home" role="tab" data-toggle="tab">Gesti�n Profesional</a></li>
				
				  <li><a href="#messages" role="tab" data-toggle="tab">Messages</a></li>
				  <li><a href="#settings" role="tab" data-toggle="tab">Settings</a></li>
				-->
				</ul>
				
				<!-- Tab panes -->
				<div class="tab-content">
				  <div class="tab-pane fade in active" id="home">
				  	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xs-offset-0 col-sm-offset-0 col-md-offset-0 col-lg-offset-0">
				  		<div class="table-responsive">
							<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered table-hover" id="datatable">
								<thead>
								  <tr >
								  	 <th>#</th>
								     <th>Pr�ctica</th>
								     <th>Tipo Gesti�n</th>
								     <th>Alumno</th>
								     <th>Supervisor</th>
								     <th>Profesor Gu�a</th>
								     <th>Acciones</th>
								  </tr>
						        </thead>
						        <tbody>
						        <c:forEach items="${practicasList}" var="practica" >
						        	<tr>
						        		<td>${practica.pra_id}</td>
						        		<td>${practica.emp_rut} <c:forEach items="${empresasList}" var="empresa"><c:if test="${empresa.emp_rut == practica.emp_rut}" >${empresa.emp_nombre}</c:if></c:forEach><br>Nombre Obra: ${practica.pra_obra_nombre}</td>
						        		<td>${practica.asi_codigo} - <c:forEach items="${asignaturasList}" var="asignatura"><c:if test="${asignatura.asi_codigo == practica.asi_codigo}" >${asignatura.asi_nombre}</c:if></c:forEach></td>
						        		<c:set var="flag_alumno" value="1"/>
						        		<c:forEach items="${alumnosList}" var="alumno" >
											<c:if test="${practica.alu_rut == alumno.alu_rut && flag_alumno == 1}">
												<td>${alumno.alu_rut} ${alumno.alu_nombres} ${alumno.alu_apellido_p} ${alumno.alu_apellido_m}</td>
												<c:set var="flag_alumno" value="2"/>
											</c:if>						        			
						        		</c:forEach>
						        		<c:set var="flag_supervisor" value="1"/>
						        		<c:forEach items="${supervisoresList}" var="supervisor" >
											<c:if test="${practica.sup_rut == supervisor.sup_rut && flag_supervisor == 1}">
												<td>${supervisor.sup_rut} ${supervisor.sup_nombres} ${supervisor.sup_apellido_p} ${supervisor.sup_apellido_m}</td>
												<c:set var="flag_supervisor" value="2"/>
											</c:if>						        			
						        		</c:forEach>
						        		<c:set var="flag_profesor" value="1"/>
						        		<c:forEach items="${profesoresList}" var="profesor" >
											<c:if test="${practica.aca_rut == profesor.aca_rut && flag_profesor == 1}">
												<td>${profesor.aca_rut} ${profesor.aca_nombres} ${profesor.aca_apellido_p} ${profesor.aca_apellido_m}</td>
												<c:set var="flag_profesor" value="2"/>
											</c:if>						        			
						        		</c:forEach>

						        		<td><button type="button" class="btn btn-primary  btn-xs" id="'${practica.pra_id}'" onclick='asignarComision(${practica.pra_id}, ${practica.asi_codigo}, ${practica.plan_id})'><span class="glyphicon glyphicon-ok"></span> Asignar Acad�mico</button></td>
						        	</tr>
						        </c:forEach>		        		
						        </tbody>
						    </table>
						</div>
				  	</div>
				  </div>
				  <!-- 
				  <div class="tab-pane" id="messages">Hola TAB 3</div>
				  <div class="tab-pane" id="settings">Hola TAB 4</div>
				  -->
				</div>
				
				
			</div>
		</div>
		<div id="div_asignar">
		</div>
    </jsp:body>
</t:plantillaTablas>