<%@ page language="java"  pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:plantillaFormularioAdmin>
	<jsp:attribute name="header">		
    </jsp:attribute>
    <jsp:body>
    	<script type="text/javascript">
			$(document).ready(function () {

			});
		</script>
			<div class="row">
				<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7 col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3">
					<legend>Nuevo Instrumento de Evaluación: ${nombre_instrumento}</legend>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
				<form class="form-horizontal" role="form" method="POST" action="FormularioInstrumentoControlador">
						<div class="table-responsive">
							<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-hover" id="datatable">
								<thead>
								  <tr >
								     <th colspan="2">Evaluación</th>
								     <th colspan="${escala.esc_num_opciones}">Criterios de Evaluación</th>
								  </tr>
						        </thead>
						        <tbody>
						        	<tr>
						        		<td colspan="2"><b>Indivudual</b></td>
						        		<c:forEach items="${escala.valores}" var="valores" >
						        			<td align="center">${valores.val_nombre}</td>
						        		</c:forEach>        		
									</tr>
									<tr>
										<td colspan="1"><b>Categoría</b></td>
										<td colspan="1"><b>Concepto</b></td>
						        		<c:forEach items="${escala.valores}" var="valores" >
						        		<fmt:parseNumber var="valor" pattern="#" integerOnly="true" value="${valores.val_valor}" />
						        			<td align="center">${valor}</td>
						        		</c:forEach>  
									</tr>
									<c:forEach var="i" begin="1" end="${n_items}">
									<tr>
						        			
				        					<td class="vert-align">
				        					<div class="col-xs-10">
				        						<select class="form-control" id="categoria_${i}" name="categoria_${i}" required>
												<option value="">Seleccione Categoría...</option>
				        						${categoria.cat_nombre}				        						
												<c:forEach items="${categoriasList}" var="categoria" >
													<option value="${categoria.cat_id}">${categoria.cat_nombre}</option>
												</c:forEach>
												</select>
											</div>
				        					</td>
			        				
						        			
						        			<td class="vert-align"><input type="text" class="form-control" id="item_${i}" name="item_${i}" placeholder="Ej: Desempeño del Estudiante en su area de trabajo" required onkeypress="return funcionMaestra(event,'nombres','1')"></td>
						        			<c:forEach begin="1" end="${escala.esc_num_opciones}" var="j" >
						        				
							        			<td class="vert-align" align="center">
							        				<textarea class="form-control no_resize" id="opc_descripcion_${i}_${j}" name="opc_descripcion_${i}_${j}" placeholder="Descripción del parámetro de evaluacion" onkeypress="return funcionMaestra(event,'descri','1')"></textarea>			
							        			</td>			        				
						        			</c:forEach>
						        	</tr>
						        	</c:forEach>
				        	</tbody>
					    </table>
					    <label class="control-label" for="observacion">Descripción del Instrumento de Evaluación:</label>
						<textarea class="form-control no_resize" id="descripcion" name="descripcion" placeholder="Descripción..." onkeypress="return funcionMaestra(event,'descri','1')"></textarea>			
       					<br>
						<button type="submit" class="btn btn-primary" onclick="return validar_plan(event,1)">Registrar Rúbrica de Evaluacion</button>
						<input type="hidden" class="form-control" id="carrera" name="carrera" value="${carrera}">
						<input type="hidden" class="form-control" id="nombre_instrumento" name="nombre_instrumento" value="${nombre_instrumento}">
						<input type="hidden" class="form-control" id="max_puntaje" name="max_puntaje" value="${escala.esc_max_valor * n_items}">
						<input type="hidden" class="form-control" id="esc_id" name="esc_id" value="${escala.esc_id}">
						<input type="hidden" class="form-control" id="rol_id" name="rol_id" value="${rol}">
						<input type="hidden" class="form-control" id="n_items" name="n_items" value="${n_items}">
						<input type="hidden" class="form-control" id="n_opciones" name="n_opciones" value="${escala.esc_num_opciones}">
						<input type="hidden" class="form-control" id="etapa" name="etapa" value="2">
						<div class="form-group">
						<label class="col-xs-3 control-label" for=""></label>
							<div class="col-xs-9">
								
							</div>
						</div>
					</form>
					</div>
			</div>
	</jsp:body>
</t:plantillaFormularioAdmin>
