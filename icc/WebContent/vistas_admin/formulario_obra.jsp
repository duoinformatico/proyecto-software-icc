<%@ page language="java"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:plantillaFormularioAdmin>
    <jsp:attribute name="header">

    </jsp:attribute>			
    <jsp:body>
			<div class="row">
				<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1">
					<legend>Registro de Nuevo Tipo de Obra</legend>
				</div>
			</div>
	        <div class="row">
				<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1">
					<form class="form-horizontal" role="form" action="FormularioObraControlador" method="post">
						<div class="form-group">
						<label class="col-xs-3 control-label" for="nombre">Nombre de Tipo de Obra:</label>
							<div class="col-xs-9">
								<input type="text" class="form-control" id=nombre name="nombre" placeholder="ej: Obra Industrial"></input>
							</div>
						</div>	
					<button type="submit"  class="btn btn-primary  btn-xg">Registrar Tipo de Obra</button>
					</form>
				</div>
			</div>
	</jsp:body>
</t:plantillaFormularioAdmin>