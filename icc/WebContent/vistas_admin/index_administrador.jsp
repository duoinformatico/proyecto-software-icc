<%@ page language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:plantillaFormularioAdmin>
    <jsp:attribute name="header">
    </jsp:attribute>
    <jsp:body>
    	<div class="row">
    		<div class="col-xs-7 col-sm-7 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 columna">
    			<h3>Bienvenido al Sistema Sr/a ${datos}</h3>
    		</div>
    	</div>
    	<div class="row">
    		<div class="col-xs-8 col-sm-8 col-xs-offset-2 col-sm-offset-2 col-md-offset-2 columna">
    			<h3>- Existen <span class="badge">${total}</span> Pre-Inscripciones Pendientes de alumnos</span></h3>
    		</div>
    	</div>	
    </jsp:body>
</t:plantillaFormularioAdmin>