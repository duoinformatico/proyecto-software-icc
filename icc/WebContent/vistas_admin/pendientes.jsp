<%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:plantillaTablas>
	<jsp:attribute name="header">

    </jsp:attribute>
    <jsp:body>
			<div class="row">
				<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1">
					<legend>Lista de Pre-Inscripciones Pendientes</legend>
				</div>
	 		</div>
	 		<c:if test="${exito == 1}">
				<div class="col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1">
					<div class="alert alert-success alert-dismissible" role="alert">
					  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					  <span class="glyphicon glyphicon-ok"></span>
					  <strong>Exito</strong> ${mensaje}
					</div>
				</div>
			</c:if>
			<c:if test="${exito == 0}">
				<div class="col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1">
					<div class="alert alert-danger alert-dismissible" role="alert">
					  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					  <span class="glyphicon glyphicon-remove"></span>
					  <strong>Error</strong> ${mensaje}
					</div>
				</div>
			</c:if>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 columna">
						<div class="table-responsive">
							<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered table-hover" id="datatable">
								<thead>
								  <tr>
								     <th>Nombre de Obra</th>
								     <th>Rut Empresa</th>
								     <th>Rut Alumno</th>
								     <th>Fecha de Inscripci�n</th>
								     <th>Estado</th>
								     <th>Revisi�n</th>
								  </tr>
						        </thead>
						        <tbody>
						        	<c:forEach items="${pendientesList}" var="pendiente" >
						        	<tr>
						        	    <td>${pendiente.pra_obra_nombre}</td>
						        		<td>${pendiente.emp_rut}</td>
						        		<td>${pendiente.alu_rut}</td>
						        		<td>${pendiente.ins_fecha}</td>
						        		<td>${pendiente.pra_estado}</td>
										<td><button type="button" class="btn btn-primary  btn-xs" id="'${pendiente.pra_id}'" onclick='verPendiente("${pendiente.pra_id}")'>Revisar</button></td>
						        	</tr>
						        	</c:forEach>
						        </tbody>
						    </table>
						</div>
					</div>
			</div>
			<div id="div_pendiente">
			</div>
	</jsp:body>
</t:plantillaTablas>