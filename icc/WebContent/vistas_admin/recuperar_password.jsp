<%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<script type="text/javascript" src="js/validar_rut.js"></script>
<t:plantillaFormularioAdmin>
	<jsp:attribute name="header">
		
    </jsp:attribute>
    <jsp:body>
			<div class="row">
				<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7 col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3">
					<legend>Recuperaci�n de Contrase�a</legend>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<form class="form-horizontal" role="form" method="POST" action="#">
						<script type="text/javascript">
						$(document).ready(function () {
								$('#rut').bind('copy paste drop', function (e) {
							           e.preventDefault();
							        });
						        $("#rut").focusin(function() {
						        	$("#rut").attr('autocomplete', 'off');
						        });
						        $("#rut").focusout(function() {
						        	$("#rut").removeAttr('autocomplete');
						        });
					      });
					    </script>
						<div class="form-group">
							<label class="col-xs-3 control-label" for="rut">Rut:</label>
							<div class="col-xs-9">
								<input type="text" class="form-control" id="rut" name="rut" placeholder="16.765.136-4"
								 ondrop="return false;" onChange="Rut(this.value)" onkeypress="return funcionMaestra(event, 'rut', 1)">
							</div>	
						</div>
						<div class="form-group ">
							<label class="col-xs-3 control-label" for="usuario">Tipo de Usuario:</label>
							<div class="col-xs-9">
								<select class="form-control" id="usuario" name="usuario">			
										<option value="Alumno">Alumno</option>	
										<option value="Administrador">Administrador</option>	
										<option value="Academico">Acad�mico</option>	
										<option value="Supervisor">Supervisor</option>								
								</select>						
							</div>
						</div>
						
					</form>
					<div class="col-xs-offset-4 col-sm-offset-4 col-md-offset-4 col-lg-offset-4">
						<button class="btn btn-primary" onclick="buscarUsuario()"><span class="glyphicon glyphicon-search"></span> Buscar</button>
					</div>
					<br>
					<div id="div_usuario">
						<c:if test="${bandera == 1}">
							<div class="col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1">
								<div class="alert alert-success alert-dismissible" role="alert">
								  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
								  <span class="glyphicon glyphicon-ok"></span>
								  <strong>Exito</strong> ${mensaje}.
								</div>
							</div>
						</c:if>
						<c:if test="${bandera == 2}">
							<div class="col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1">
								<div class="alert alert-danger alert-dismissible" role="alert">
								  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
								  <span class="glyphicon glyphicon-remove"></span>
								  <strong>Error</strong> ${mensaje}
								</div>
							</div>
						</c:if>
					</div>
					<div id="div_confirmacion">
					</div>
				</div>
			</div>
			<br>
			<div class="row">
				
			</div>
	</jsp:body>
</t:plantillaFormularioAdmin>

