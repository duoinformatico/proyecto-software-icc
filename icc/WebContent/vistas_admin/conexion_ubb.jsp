<%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:plantillaTablas>
	<jsp:attribute name="header">
		
    </jsp:attribute>
    <jsp:body>
			<div class="row">
				<br>
				<div class="col-xs-12 col-sm-11 col-md-11 col-lg-11 col-sm-offset-1 col-md-offset-1 col-lg-offset-1">
					<legend>Sincronizaci�n con Base de Datos Central UBB</legend>
				</div>
			</div>
			<c:if test="${exito == 1}">
				<div class="col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1">
					<div class="alert alert-success alert-dismissible" role="alert">
					  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					  <span class="glyphicon glyphicon-ok"></span>
					  <strong>Exito</strong> ${mensaje}
					</div>
				</div>
			</c:if>
			<c:if test="${exito == 0}">
				<div class="col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1">
					<div class="alert alert-danger alert-dismissible" role="alert">
					  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					  <span class="glyphicon glyphicon-remove"></span>
					  <strong>Error</strong> ${mensaje}
					</div>
				</div>
			</c:if>
			<br>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<form class="form-horizontal" role="form" method="POST" action="#">
						<div class="form-group ">
							
						</div>
						<br>
						<div class="alert alert-warning alert-dismissible" role="alert">
						  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
						  <span class="glyphicon glyphicon-warning-sign form-control-feedback"></span><strong> Advertencia!</strong> Tenga en cuenta que este procedimiento puede generar muchas transacciones y demora en la carga del sistema.
						</div>
						<button type="button" class="btn btn-primary btn-lg" onclick="cargarAlumnosPlanEstudio()">Cargar Alumnos Nuevos</button>
						<input type="hidden" id="agnio_alumno" name="agnio_alumno" value="${agnio_alumno}">
						<input type="hidden" id="periodo_alumno" name="periodo_alumno" value="${periodo_alumno}">
					</form>
				</div>
			</div>
			<br>
			
			<div class="row">
				<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7 col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3">
					<legend>Consultar Asignaturas Alumno</legend>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<form class="form-horizontal" role="form">
						<script type="text/javascript">
						$(document).ready(function () {
								$('#rut').bind('copy paste drop', function (e) {
							           e.preventDefault();
							        });
						        $("#rut").focusin(function() {
						        	$("#rut").attr('autocomplete', 'off');
						        });
						        $("#rut").focusout(function() {
						        	$("#rut").removeAttr('autocomplete');
						        });
					      });
					    </script>
						<div class="form-group">
							<label class="col-xs-3 control-label" for="rut">Rut:</label>
							<div class="col-xs-9">
								<input type="text" class="form-control" id="rut" name="rut" placeholder="16.765.136-4"
								 ondrop="return false;" onChange="Rut(this.value)" onkeypress="return funcionMaestra(event, 'rut', 1)" maxlegth="9">
							</div>	
						</div>					
						
					</form>
					<div class="col-xs-12 col-xs-offset-5">
						<button class="btn btn-primary btn-lg" onclick="buscarAsignaturasAlumno()"><span class="glyphicon glyphicon-search"></span> Buscar</button>
						<br><br>
					</div>
					
					<div id="div_asignaturas">
	
					</div>

				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7 col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3">
					<legend>Actualizar Inscripciones de Asignaturas Alumno</legend>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<form class="form-horizontal" role="form" method="POST" action="#">
						<div class="form-group ">
							<label class="col-xs-1 control-label" for="agnio">A�o:</label>
							<div class="col-xs-3">
								<select class="form-control" id="agnio" name="agnio">
										<option value="">Seleccione A�o</option>
										<c:forEach var="i" begin="0" end="9">
											<option value="${agnio_alumno - i}">${agnio_alumno - i}</option>
							          	</c:forEach>			
																	
								</select>						
							</div>
							<label class="col-xs-3 control-label" for="periodo">Semestre:</label>
							<div class="col-xs-3">
								<select class="form-control" id="periodo" name="periodo">
										<option value="">Seleccione Per�odo</option>			
										<option value="1">1</option>
										<!--<option value="2">2</option>-->								
								</select>						
							</div>
						</div>
						<br>
						<div class="alert alert-warning alert-dismissible" role="alert">
						  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
						  <span class="glyphicon glyphicon-warning-sign form-control-feedback"></span><strong> Advertencia!</strong> Tenga en cuenta que este procedimiento puede generar muchas transacciones y demora en la carga del sistema.
						</div>
						<button type="button" class="btn btn-primary  btn-lg" onclick="cargarInscripcionAsignatura()">Cargar Inscripciones Asignaturas</button>
					</form>
				</div>
			</div>
			<br>
			<div id="div_inscripciones">
				<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 columna">
				<div class="table-responsive">
					<table cellpadding="0" cellspacing="0" border="0" class="display table table-striped table-bordered table-hover" id="datatable">
						<thead>
						  <tr >
						     <th>ALU_RUT</th>
						     <th>ASI_CODIGO</th>
						     <th>PLAN_ID</th>
						     <th>INS_FECHA</th>
						     <th>INS_SEMESTRE</th>
						     <th>INS_AGNIO</th>
						     <th>INS_NOTA</th>
						     <th>INS_ESTADO</th>
						  </tr>
				        </thead>
				        <tbody>
				        	<tr>
				        		<td align="center">${inscripcion.alu_rut}</td>
				        		<td align="center">${inscripcion.asi_codigo}</td>
				        		<td align="center">${inscripcion.plan_id}</td>
				        		<td align="center">${inscripcion.ins_fecha}</td>
				        		<td align="center">${inscripcion.ins_semestre}</td>
				        		<td align="center">${inscripcion.ins_agnio}</td>
				        		<td align="center">${inscripcion.ins_nota}</td>
				        		<td align="center">${inscripcion.ins_estado}</td>
				        	</tr>
				        </tbody>
				    </table>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="js/validar_rut.js"></script>
			
			
	</jsp:body>
</t:plantillaTablas>