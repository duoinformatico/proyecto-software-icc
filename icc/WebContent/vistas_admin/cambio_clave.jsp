<%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<link rel="stylesheet" href="css/alertify.core.css" />
<link rel="stylesheet" href="css/alertify.default.css" id="toggleCSS" />
<script src="js/alertify.min.js"></script>
<script type="text/javascript" src="js/validaciones_admin.js"></script>
<script type="text/javascript" src="js/validar_rut.js"></script>
<t:plantillaTablas>
	<jsp:attribute name="header">
		
    </jsp:attribute>
    <jsp:body>
			<div class="row">
				<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7 col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3">
					<legend>M�dulo de Cambio de Contrase�a</legend>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div id="div_usuario">
						<c:if test="${bandera == 1}">
							<div class="col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1">
								<div class="alert alert-success alert-dismissible" role="alert">
								  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
								  <span class="glyphicon glyphicon-ok"></span>
								  <strong>Exito</strong> ${mensaje}
								</div>
							</div>
						</c:if>
						<c:if test="${bandera == 2}">
							<div class="col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1">
								<div class="alert alert-danger alert-dismissible" role="alert">
								  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
								  <span class="glyphicon glyphicon-remove"></span>
								  <strong>Error</strong> ${mensaje}
								</div>
							</div>
						</c:if>
					</div>
					<form class="form-horizontal" id="formulario_clave" role="form" method="POST" action="MenuCambioClave">
						<script type="text/javascript">
						$(document).ready(function () {
								$('#actual').bind('copy paste drop', function (e) {
							           e.preventDefault();
							        });
						        $("#actual").focusin(function() {
						        	$("#actual").attr('autocomplete', 'off');
						        });
						        $("#actual").focusout(function() {
						        	$("#actual").removeAttr('autocomplete');
						        });
						        $('#confirmacion').bind('copy paste drop', function (e) {
							           e.preventDefault();
							        });
						        $("#confirmacion").focusin(function() {
						        	$("#confirmacion").attr('autocomplete', 'off');
						        });
						        $("#confirmacion").focusout(function() {
						        	$("#confirmacion").removeAttr('autocomplete');
						        });
					      });
					    </script>
						<div class="form-group">
							<label class="col-xs-3 control-label" for="actual">Contrase�a Actual:</label>
							<div class="col-xs-9">
								<input type="password" class="form-control" id="actual" name="actual" ondrop="return false;">
							</div>	
						</div>
						<div class="alert alert-warning alert-dismissible" role="alert">
						  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
						  <span class="glyphicon glyphicon-remove"></span>
						  <strong>Advertencia</strong> La nueva clave de acceso solo puede contener caracteres alfanum�ricos y debe tener a lo menos 9 d�gitos.
						</div>
						<div class="form-group">
							<label class="col-xs-3 control-label" for="nueva">Contrase�a Nueva:</label>
							<div class="col-xs-9">
								<input type="password" class="form-control" id="nueva" name="nueva" ondrop="return false;" onkeypress="return funcionMaestra(event,'clave','1')">
							</div>	
						</div>
						<div class="form-group">
							<label class="col-xs-3 control-label" for="confirmacion">Vuelva a Escribir Contrase�a Nueva:</label>
							<div class="col-xs-9">
								<input type="password" class="form-control" id="confirmacion" name="confirmacion" ondrop="return false;" onkeypress="return funcionMaestra(event,'clave','1')">
							</div>	
						</div>	
						<button class="btn btn-primary" onclick="return validacion_cambio_clave(event,1)">Cambiar Clave de Acceso</button>			
					</form>
					
					<br>
				</div>
			</div>
	</jsp:body>
</t:plantillaTablas>

