<%@ page language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:plantillaAlumno>
    <jsp:attribute name="header">

    </jsp:attribute>			
    <jsp:body>
	     	<div class="row">
				<div class="col-xs-9 col-sm-offset-1 col-xs-offset-1">
					<legend>Mis Gestiones Operativas y Profesional</legend>
				</div>
			</div>
			<c:if test="${exito == 1}">
				<div class="col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1">
					<div class="alert alert-success alert-dismissible" role="alert">
					  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					  <span class="glyphicon glyphicon-ok"></span>
					  <strong>Exito</strong> ${mensaje}
					</div>
				</div>
			</c:if>
			<c:if test="${exito == 0}">
				<div class="col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1">
					<div class="alert alert-danger alert-dismissible" role="alert">
					  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					  <span class="glyphicon glyphicon-remove"></span>
					  <strong>Error</strong> ${mensaje}
					</div>
				</div>
			</c:if>
	     	<div class="row">
				<div class="col-xs-12 col-sm-12 columna ">	
					<div class="table-responsive">
							<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered table-hover" id="datatable">
								<thead>
								  <tr >
								     <th>Empresa</th>
								     <th>Supervisor</th>
								     <th>Asignatura</th>
								     <th>Nombre de Obra</th>
								     <th>Fecha de Inicio</th>
								     <th>Fecha de Término</th>
								     <th>Estado</th>
								     <th>Acciones</th>
								  </tr>
						        </thead>
						        <tbody>
						        	<c:forEach items="${practicaList}" var="practica" >
						        	<tr>
						        		<td>${practica.emp_rut}</td>
						        		<td>${practica.sup_rut}</td>
						        		<td>${practica.asi_codigo} - <c:forEach items="${asignaturasList}" var="asignatura"><c:if test="${asignatura.asi_codigo == practica.asi_codigo}" >${asignatura.asi_nombre}</c:if></c:forEach></td>
						        		<td>${practica.pra_obra_nombre}</td>
						        		<td>${practica.pra_fecha_ini}</td>
						        		<td>${practica.pra_fecha_fin}</td>
						        		<td>${practica.pra_estado}</td>
						        		<c:if test="${practica.pra_estado == 'Pendiente'}" >
						        			<td><button class="btn btn-primary  btn-xs disabled"><span class="glyphicon glyphicon-folder-open"></span> Sin Bitácoras</button></td>
						        		</c:if>
						        		<c:if test="${practica.pra_estado == 'Rechazada'}" >
						        			<td><button class="btn btn-primary  btn-xs disabled"><span class="glyphicon glyphicon-folder-open"></span> Sin Bitácoras</button></td>
						        		</c:if>
						        		<c:if test="${practica.pra_estado == 'Aceptada' || practica.pra_estado == 'Finalizada' || practica.pra_estado == 'Comisionada' || practica.pra_estado == 'Evaluada'}" >
						        				<td>
						        					<form action="BitacoraServlet.do" method="POST" >
														<input type="hidden" id="id" name="id" value="${practica.pra_id}"/>
														
														<button type="submit" id="bitacora" name="bitacora" value="bitacora"  class="btn btn-primary  btn-xs" ><span class="glyphicon glyphicon-folder-open"></span> Mis Bitácoras</button>
													
							        				
							        				
							        				<c:if test="${practica.pra_estado == 'Finalizada' || practica.pra_estado == 'Comisionada' || practica.pra_estado == 'Evaluada'}" >
									        		
									        		<c:forEach items="${estadosList}" var="estado">
								        					<c:if test="${(estado.pra_id == practica.pra_id) && (estado.bean_uno == 0) }">
								        						<button type="button" class="btn btn-success  btn-xs" id="'${estado.pra_id}'" onclick='subirInforme("${estado.pra_id}")'><span class="glyphicon glyphicon-open"></span> Subir Informe</button>
								        					</c:if>
								        					<c:if test="${(estado.pra_id == practica.pra_id) && (estado.bean_uno == 1) }">
								        						<a href="FormularioInformeControlador?fileName=${practica.pra_informe}" class="btn btn-success  btn-xs"> <span class="glyphicon glyphicon-save"></span> Descargar Informe</a>
								        					</c:if>									        					
								        				</c:forEach>
								        			</c:if>
								        			<c:set var="flag_instrumento" value="1"/>
							        				<c:forEach items="${evaluacionesList}" var="evaluacion">				        				
											        	<c:if test="${practica.pra_estado == 'Comisionada' && evaluacion.eva_estado == 'Sin Evaluar' && flag_instrumento == 1}">
									        				<c:forEach items="${instrumentosList}" var="instrumento">
											        			<c:if test="${instrumento.rol_id == evaluacion.rol_id && flag_instrumento == 1}">
											        				<button type="button" class="btn btn-success  btn-xs" id="'${instrumento.ins_id}'" onclick='verRubrica("${instrumento.ins_id}", "${evaluacion.alu_rut}", "${evaluacion.pra_id}", "${evaluacion.rol_id}")'><span class="glyphicon glyphicon-ok"></span> Autoevaluar</button>
											        				<c:set var="flag_instrumento" value="2"/>
											        			</c:if>
											        		</c:forEach> 		
											        	</c:if>
											        </c:forEach>  
											        </form>      									        				
						        				</td>
						        		</c:if>						        				        		
						        	</tr>
						        	</c:forEach>
						        </tbody>
						    </table>
					</div>
				</div>
			</div>
			<div id="div_instrumento">
			</div>
			<div id="div_informe">
			</div>
		
	</jsp:body>
</t:plantillaAlumno>