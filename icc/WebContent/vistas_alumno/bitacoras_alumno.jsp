<%@ page language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<t:plantillaAlumno>
    <jsp:attribute name="header">
    </jsp:attribute>			
    <jsp:body>
    	 <div class="row">
			<div class="col-xs-12">
				<legend>Mis Bit�coras</legend>
			</div>
		 </div>
		   <c:if test="${exito == 1}">
				<div class="col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1">
					<div class="alert alert-success alert-dismissible" role="alert">
					  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					  <span class="glyphicon glyphicon-ok"></span>
					  <strong>Exito</strong> ${mensaje}
					</div>
				</div>
			</c:if>
			<c:if test="${exito == 0}">
				<div class="col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1">
					<div class="alert alert-danger alert-dismissible" role="alert">
					  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					  <span class="glyphicon glyphicon-remove"></span>
					  <strong>Error</strong> ${mensaje}
					</div>
				</div>
			</c:if>
		 	<div id="div_bitacoras">
		 	</div>
	     	<div class="row">
				<div class="col-xs-12 col-sm-12 columna">	
					<div class="table-responsive">
							<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered table-hover" id="datatable">
								<thead>
								  <tr>
								  	 <th>#</th>
								     <th>Fecha Inicio</th>
								     <th>Fecha T�rmino</th>
								     <th>Pr�ctica</th>
								     <th>Fecha �ltima Edici�n</th>
								     <th>Acciones</th>
								  </tr>
						        </thead>
						        <tbody>
						        	<c:set var="contador" value="1"/>	
						        	<c:forEach items="${bitacoraList}" var="bitacora" >
						        	<tr>
						        	    <td>${contador}</td>
						        		<td>
						        			<fmt:formatDate value="${bitacora.bit_fecha_ini}" var="fecha_ini" type="date" pattern="dd-MM-yyyy" />
						        			${fecha_ini}
						        		</td>
						        		<td>
						        			<fmt:formatDate value="${bitacora.bit_fecha_fin}" var="fecha_fin" type="date" pattern="dd-MM-yyyy" />
						        			${fecha_fin}
						        		</td>
						        		<td>${practica.pra_obra_nombre}</td>
						        		<td>${bitacora.bit_fecha_edicion}</td>
						        		<td>
						        			<c:if test="${bitacora.bean_uno == 1}">
												<button type="button" id="bitacora" name="bitacora" value="bitacora" class="btn btn-primary  btn-xs"  onclick='verBitacora("${bitacora.bit_id}")'><span class="glyphicon glyphicon-pencil"></span> Editar</button> 
											</c:if>    			
						        			<button type="button" id="bitacora" name="bitacora" value="bitacora" class="btn btn-primary  btn-xs" onclick='consultarBitacora("${bitacora.bit_id}","${practica.plan_id}")'><span class="glyphicon glyphicon-search"></span>  Revisar</button>
						        		</td>
						        	</tr>
						        	<c:set var="contador" value="${contador+1}"/>
						        	</c:forEach>
						        </tbody>
						    </table>
					</div>
				</div>
			</div>
			
			<div id="div_bitacora">
			
			</div>
			<div id="div_consultar">
			
			</div>
	</jsp:body>
</t:plantillaAlumno>