<%@ page language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:plantillaFormularioAlumno>
    <jsp:attribute name="header">
    </jsp:attribute>
    <jsp:body>
    	<div class="row">
    		<div class="col-xs-12 col-sm-12 col-sm-offset-1 col-md-offset-1 columna">
    			<h3>Bienvenid@ al Sistema: ${usuario}</h3>
    		</div>
    	</div>
    	<div class="row">
    		<div class="col-xs-8 col-sm-8 col-xs-offset-2 col-sm-offset-2 col-md-offset-2 columna">
    			<c:if test="${cantidad == 0}" >
    				<h3>-Usted tiene inscrita <span class="badge">${cantidad}</span> Pr�cticas Profesionales este Semestre</h3>
    			</c:if>
    			<c:if test="${cantidad == 1}" >
    				<h3>-Usted tiene inscrita <span class="badge">${cantidad}</span> Pr�ctica Profesional este Semestre en la Obra:
    				<br>
    				<div class="col-xs-offset-1 col-sm-offset-1 col-md-offset-1 columna">
	    				<c:forEach var="practica" items="${practicaList}" >
							-${practica.pra_obra_nombre} a cargo de la empresa ${practica.emp_rut}
							<br>
						</c:forEach>
    				</h3>
    			</c:if>
    			<c:if test="${cantidad > 1}" >
    				<h3>-Usted tiene inscrita <span class="badge">${cantidad}</span> Pr�cticas Profesionales este Semestre en las Obras:
    				<br>
    				<div class="col-xs-offset-1 col-sm-offset-1 col-md-offset-1 columna">
	    				<c:forEach var="practica" items="${practicaList}" >
							-${practica.pra_obra_nombre} a cargo de la empresa ${practica.emp_rut}
							<br>
						</c:forEach>
    				</h3>
    			</c:if>
    		</div>
    	</div>
    	<div class="row">
    		<div class="col-xs-9 col-sm-9 col-xs-offset-2 col-sm-offset-2 col-md-offset-2 columna">
    			  <h3>- Aqu� una lista de las empresas que tienen convenios con la escuela de Ingenier�a en Construcci�n <button type="button" class="btn btn-primary  btn-xs" id="lista" onclick='mostrarEmpresas()'> <span class="glyphicon glyphicon-list-alt"></span></button></h3> 
    		</div>
    	</div>
    	<div id="div_empresas">
		</div>
    </jsp:body>
</t:plantillaFormularioAlumno>