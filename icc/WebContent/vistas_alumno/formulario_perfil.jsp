<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<link rel="stylesheet" href="css/alertify.core.css" />
<link rel="stylesheet" href="css/alertify.default.css" id="toggleCSS" />
<script src="js/alertify.min.js"></script>
<script type="text/javascript" src="js/validaciones_alumno.js"></script>
<t:plantillaFormularioAlumno>
    <jsp:attribute name="header">

    </jsp:attribute>			
    <jsp:body>
    		<script type="text/javascript">
						
						$(document).ready(function () {
								$('#nombre').bind('copy paste drop', function (e) {
						           e.preventDefault();
						        });       
						        $("#nombre").focusin(function() {
						        	$("#nombre").attr('autocomplete', 'off');
						        });
						        $("#nombre").focusout(function() {
						        	$("#nombre").removeAttr('autocomplete');
						        });
						        $('#paterno').bind('copy paste drop', function (e) {
							           e.preventDefault();
							        });
						        $("#paterno").focusin(function() {
						        	$("#paterno").attr('autocomplete', 'off');
						        });
						        $("#paterno").focusout(function() {
						        	$("#paterno").removeAttr('autocomplete');
						        });
						        $('#materno').bind('copy paste drop', function (e) {
							           e.preventDefault();
							        });
						        $("#materno").focusin(function() {
						        	$("#materno").attr('autocomplete', 'off');
						        });
						        $("#materno").focusout(function() {
						        	$("#materno").removeAttr('autocomplete');
						        });
						        $('#email').bind('copy paste drop', function (e) {
							           e.preventDefault();
							        });
						        $("#email").focusin(function() {
						        	$("#email").attr('autocomplete', 'off');
						        });
						        $("#email").focusout(function() {
						        	$("#email").removeAttr('autocomplete');
						        });
						        $('#telefono').bind('copy paste drop', function (e) {
							           e.preventDefault();
							        });
						        $("#telefono").focusin(function() {
						        	$("#telefono").attr('autocomplete', 'off');
						        	if($("#codigo").val() == ""){
						        		$("#telefono").attr('readonly', 'readonly');
						        		$("#codigo").focus();
						        	}
						        	else{
						        		$("#telefono").removeAttr('readonly');
						        	}
						        	if($("#codigo").val() != ""){
						        		if($("#codigo").val() == 2){
						        			$("#telefono").attr('maxlength', '8');
						        		}
						        		else{
						        			$("#telefono").attr('maxlength', '7');
						        		}
						        	}					        	
						        });
								$("#codigo").change(function(){					        	
									if($("#codigo").val() != ""){
						        		$("#telefono").focus();
						        		$("#telefono").val("");
						        	}					        	
						        });
						        $("#telefono").change(function(){					        	
						        	
						        	if($("#telefono").val().length == 8 && $("#codigo").val() == 2){
						        		$("#telefono").val("+56-"+$("#codigo").val()+"-"+$("#telefono").val());
						        	}
						        	if($("#telefono").val().length == 7 && $("#codigo").val() != 2){
						        		$("#telefono").val("+56-"+$("#codigo").val()+"-"+$("#telefono").val());
						        	}
						        });					        	
						        
						        $("#telefono").focusout(function() {
						        	$("#telefono").removeAttr('autocomplete');
						        	$("#telefono").removeAttr('maxlength');
						        	if($("#codigo").val() == ""){
						        		$("#telefono").attr('readonly', 'readonly');
						        		$("#codigo").focus();
						        	}
						        	else{
						        		$("#telefono").removeAttr('readonly');
						        	}
						        	if($("#telefono").val().length == 8 && $("#codigo").val() == 2){
						        		$("#telefono").val("+56-"+$("#codigo").val()+"-"+$("#telefono").val());
						        	}
						        	if($("#telefono").val().length < 8 && $("#codigo").val() == 2){
						        		$("#telefono").val("");
						        	}
						        	if($("#telefono").val().length == 7 && $("#codigo").val() != 2){
						        		$("#telefono").val("+56-"+$("#codigo").val()+"-"+$("#telefono").val());
						        	}
						        	if($("#telefono").val().length < 7 && $("#codigo").val() != 2){
						        		$("#telefono").val("");
						        	}
						        });
						        $('#celular').bind('copy paste drop', function (e) {
							           e.preventDefault();
							        });
						        $("#celular").focusin(function() {
						        	$("#celular").attr('autocomplete', 'off');
						        	$("#celular").attr('maxlength', '8');					        	
						        });
						       $("#celular").focusout(function() {
						        	$("#celular").removeAttr('autocomplete');
						        	$("#celular").removeAttr('maxlength');
						        	if($("#celular").val().length < 8){
						        		$("#celular").val("");
						        	}
						        	if($("#celular").val().length == 8){
						        		$("#celular").val("+569"+$("#celular").val());
						        	}
						        });
						        $('#residencial').bind('copy paste drop', function (e) {
						           e.preventDefault();
						        });
						        $("#residencial").focusin(function() {
						        	$("#residencial").attr('autocomplete', 'off');
						        });
						        $("#residencial").focusout(function() {
						        	$("#residencial").removeAttr('autocomplete');
						        });
						        $('#academica').bind('copy paste drop', function (e) {
							           e.preventDefault();
							        });
						        $("#academica").focusin(function() {
						        	$("#academica").attr('autocomplete', 'off');
						        });
						        $("#academica").focusout(function() {
						        	$("#academica").removeAttr('autocomplete');
						        });
					      });
					    </script>
    		<div class="row">
				<div class="col-xs-9 col-sm-offset-1 col-xs-offset-1">
					<legend>Modificar Información Personal</legend>
				</div>
	 		</div>
	 		<c:if test="${exito == 1}">
				<div class="col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1">
					<div class="alert alert-success alert-dismissible" role="alert">
					  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					  <span class="glyphicon glyphicon-ok"></span>
					  <strong>Exito</strong> Ha modificado Exitosamente su Información Personal .
					</div>
				</div>
			</c:if>
	     	<div class="row">
				<div class="col-xs-9 col-sm-9 col-sm-offset-1 col-xs-offset-1 columna">	
					<form class="form-horizontal" id="formulario_alumno"  role="form" method="POST" action="perfilesServlet.do">
						<div class="form-group">
						<label class="col-xs-3 control-label" for="rut">Rut:</label>
							<div class="col-xs-9">
								<input type="text" maxlength="20" class="form-control" id="rut" name="rut" value="${alumno.alu_rut}" readonly="readonly">
							</div>
						</div>
						
						<div class="form-group">
						<label class="col-xs-3 control-label" for="nombres">Nombres:</label>
							<div class="col-xs-9">
								<input type="text" maxlength="20" class="form-control" id="nombre" name="nombre" value="${alumno.alu_nombres}" readonly="readonly">
							</div>
						</div>
						<div class="form-group">
						<label class="col-xs-3 control-label" for="paterno">Apellido Paterno:</label>
							<div class="col-xs-9">
								<input type="text" maxlength="20" class="form-control" id="paterno" name="paterno" value="${alumno.alu_apellido_p}" readonly="readonly">
							</div>
						</div>
						<div class="form-group">
						<label class="col-xs-3 control-label" for="materno">Apellido Materno:</label>
							<div class="col-xs-9">
								<input type="text" maxlength="20" class="form-control" id="materno" name="materno" value="${alumno.alu_apellido_m}" readonly="readonly">
							</div>
						</div>
						<div class="form-group">
						<label class="col-xs-3 control-label" for="email">E-mail:</label>
							<div class="col-xs-9">
								<input type="text" maxlength="20" class="form-control" id="email" name="email" value="${alumno.alu_email}" readonly="readonly">
							</div>
						</div>
						<div class="form-group">
							<label class="col-xs-3 control-label" for="telefono">Teléfono:</label>
							<div class="col-xs-4">	
								<select class="form-control" id="codigo" name="codigo">
									<option value="">Código Area...</option>
									<optgroup label="Zona Norte">
										<c:if test="${codigo == 58}" >			
											<option value="58" selected="selected">(58) Arica-Parinacota</option>
										</c:if>
										<c:if test="${codigo != 58}" >			
											<option value="58">(58) Arica-Parinacota</option>
										</c:if>
										<c:if test="${codigo == 57}" >		
											<option value="57" selected="selected">(57) Iquique-Tamarugal</option>	
										</c:if>
										<c:if test="${codigo != 57}" >			
											<option value="57">(57) Iquique-Tamarugal</option>
										</c:if>
										<c:if test="${codigo == 55}" >			
											<option value="55" selected="selected">(55) Tocopilla-Loa-Antofagasta</option>
										</c:if>
										<c:if test="${codigo != 55}" >			
											<option value="55">(55) Tocopilla-Loa-Antofagasta</option>
										</c:if>
										<c:if test="${codigo == 51}" >			
											<option value="51" selected="selected">(51) Huasco</option>
										</c:if>
										<c:if test="${codigo != 51}" >			
											<option value="51">(51) Huasco</option>
										</c:if>
										<c:if test="${codigo == 52}" >			
											<option value="52" selected="selected">(52) Chañaral-Copiapó</option>
										</c:if>
										<c:if test="${codigo != 52}" >			
											<option value="52">(52) Chañaral-Copiapó</option>
										</c:if>
										<c:if test="${codigo == 51}" >			
											<option value="51" selected="selected">(51) Elqui</option>
										</c:if>
										<c:if test="${codigo != 51}" >			
											<option value="51">(51) Elqui</option>
										</c:if>
										<c:if test="${codigo == 53}" >			
											<option value="53" selected="selected">(53) Choapa-Limarí</option>
										</c:if>
										<c:if test="${codigo != 53}" >			
											<option value="53">(53) Choapa-Limarí</option>
										</c:if>
									</optgroup>
									<optgroup label="Zona Centro">
										<c:if test="${codigo == 32}" >			
											<option value="32" selected="selected">(73)>(32) Isla de Pascua-Valparaíso-Marga Marga(Quilpué-Villa Alemana)</option>
										</c:if>
										<c:if test="${codigo != 32}" >			
											<option value="32">(32) Isla de Pascua-Valparaíso-Marga Marga(Quilpué-Villa Alemana)</option>
										</c:if>
										<c:if test="${codigo == 33}" >		
											<option value="33" selected="selected">(73)>(33) Petorca-Quillota-Marga Marga(Limache y Olmué)</option>
										</c:if>
										<c:if test="${codigo != 33}" >			
											<option value="33">(33) Petorca-Quillota-Marga Marga(Limache y Olmué)</option>
										</c:if>
										<c:if test="${codigo == 34}" >			
											<option value="34" selected="selected">(73)>(34) Los Andes-San Felipe de Aconcagua</option>
										</c:if>
										<c:if test="${codigo != 34}" >			
											<option value="34">(34) Los Andes-San Felipe de Aconcagua</option>
										</c:if>
										<c:if test="${codigo == 35}" >		
											<option value="35" selected="selected">(73)>(35) San Antonio</option>	
										</c:if>
										<c:if test="${codigo != 35}" >			
											<option value="35">(35) San Antonio</option>
										</c:if>
										<c:if test="${codigo == 2}" >			
											<option value="2" selected="selected">(73)> ( 2) Chacabuco-Cordillera-Maipo-Melipilla-Santiago-Talagante</option>
										</c:if>
										<c:if test="${codigo != 2}" >			
											<option value="2"> ( 2) Chacabuco-Cordillera-Maipo-Melipilla-Santiago-Talagante</option>
										</c:if>
										<c:if test="${codigo == 72}" >			
											<option value="72" selected="selected">(73)>(72) Cachapoal-ardenal Caro-Colchagua</option>
										</c:if>
										<c:if test="${codigo != 72}" >			
											<option value="72">(72) Cachapoal-ardenal Caro-Colchagua</option>
										</c:if>
										<c:if test="${codigo == 71}" >			
											<option value="71" selected="selected">(73)>(71) Talca</option>
										</c:if>
										<c:if test="${codigo != 71}" >			
											<option value="71">(71) Talca</option>
										</c:if>
										<c:if test="${codigo == 73}" >			
											<option value="73" selected="selected">(73) Cauquenes-Linares</option>
										</c:if>
										<c:if test="${codigo != 73}" >				
											<option value="73">(73) Cauquenes-Linares</option>
										</c:if>
										<c:if test="${codigo == 75}" >			
											<option value="75" selected="selected">(75) Curicó</option> 
										</c:if>
										<c:if test="${codigo != 75}" >			
											<option value="75">(75) Curicó</option>
										</c:if>
										
									</optgroup>
									<optgroup label="Zona Sur">
										<c:if test="${codigo == 41}" >			
											<option value="41" selected="selected">(41) Arauco-Concepción </option>
										</c:if>
										<c:if test="${codigo != 41}" >			
											<option value="41">(41) Arauco-Concepción </option>
										</c:if>
										<c:if test="${codigo == 42}" >			
											<option value="42" selected="selected">(42) Ñuble</option>
										</c:if>
										<c:if test="${codigo != 42}" >			
											<option value="42">(42) Ñuble</option>
										</c:if>
										<c:if test="${codigo == 43}" >			
											<option value="43" selected="selected">(43) Bío-Bío</option>
										</c:if>
										<c:if test="${codigo != 43}" >			
											<option value="43">(43) Bío-Bío</option>
										</c:if>
										<c:if test="${codigo == 45}" >			
											<option value="45" selected="selected">(45) Cautín-Malleco</option>
										</c:if>
										<c:if test="${codigo != 45}" >			
											<option value="45">(45) Cautín-Malleco</option>
										</c:if>
										<c:if test="${codigo == 63}" >		
											<option value="63" selected="selected">(63) Ranco-Valdivia</option>
										</c:if>
										<c:if test="${codigo != 63}" >			
											<option value="63">(63) Ranco-Valdivia</option>
										</c:if>
										<c:if test="${codigo == 64}" >			
											<option value="64" selected="selected">(64) Osorno</option>
										</c:if>
										<c:if test="${codigo != 64}" >			
											<option value="64">(64) Osorno</option>
										</c:if>
										<c:if test="${codigo == 65}" >			
											<option value="65" selected="selected">(65) Chiloé-Llanquihue-Palena </option>
										</c:if>
										<c:if test="${codigo != 65}" >			
											<option value="65">(65) Chiloé-Llanquihue-Palena </option>
										</c:if>
										<c:if test="${codigo == 67}" >			
											<option value="67" selected="selected">(67) Aisén-Capitán Prat-Coihaique-General Carrera</option>
										</c:if>
										<c:if test="${codigo != 67}" >			
											<option value="67">(67) Aisén-Capitán Prat-Coihaique-General Carrera</option>
										</c:if>
										<c:if test="${codigo == 68}" >			
											<option value="68" selected="selected">(68) Aisén(Puyuhuapi)</option>
										</c:if>
										<c:if test="${codigo != 68}" >			
											<option value="68">(68) Aisén(Puyuhuapi)</option>
										</c:if>
										<c:if test="${codigo == 61}" >			
											<option value="61" selected="selected">(61) Antártica Chilena-Magallanes-Tierra del Fuego-Última Esperanza</option>
										</c:if>
										<c:if test="${codigo != 61}" >			
											<option value="61">(61) Antártica Chilena-Magallanes-Tierra del Fuego-Última Esperanza</option>
										</c:if>
									</optgroup>	
								</select>							
							</div>
							<div class="col-xs-5">
								<div class="input-group">
									<c:if test="${bandera == 0}" >
										<input type="tel" class="form-control" id="telefono" name="telefono" placeholder="Ej:2323421" value="+56-${codigo}-${telefono}" onkeypress="return funcionMaestra(event,'fijo',1)" >			
									</c:if>
									<c:if test="${bandera == 1}" >	
										<input type="tel" class="form-control" id="telefono" name="telefono" placeholder="Ej:2323421" value="" onkeypress="return funcionMaestra(event,'fijo',1)" >		
									</c:if>
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-phone-alt"></span>
									</span>
								</div>	
							</div>	
						</div>
						<div class="form-group">
							<label class="col-xs-3 control-label" for="celular">Celular:</label>
							<div class="col-xs-9">
								<div class="input-group">
									<input type="tel" class="form-control" id="celular" name="celular" placeholder="Ej: +56998345670" value="${alumno.alu_celular}" onkeypress="return funcionMaestra(event,'celular',1)">
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-phone"></span>
									</span>
								</div>	
							</div>	
						</div>
						<c:if test="${contador == 0}" >
							<div class="form-group">
							<label class="col-xs-3 control-label" for="residencial">Dirección Residencial:</label>
								<div class="col-xs-9">
									<input type="text" maxlength="30" class="form-control" id="residencial" name="residencial" value=""
									 onkeypress="return funcionMaestra(event,'dire','1')" placeholder="Los Peumos #47 - Collao">
								</div>
							</div>
							<div class="form-group">
							<label class="col-xs-3 control-label" for="academica">Dirección Académica:</label>
								<div class="col-xs-9">
									<input type="text" maxlength="30" class="form-control" id="academica" name="academica" value=""
									 onkeypress="return funcionMaestra(event,'dire','1')" placeholder="Los Peumos #47 - Collao">
								</div>
							</div>
						</c:if>
						<c:if test="${contador == 2}" >
							<div class="form-group">
								<label class="col-xs-3 control-label" for="residencial">Dirección Residencial:</label>
								<div class="col-xs-9">
									<c:forEach var="direccion_r" items="${direccionList}" >
										<c:if test="${direccion_r.dir_tp_id == 2}" >
											<input type="text" maxlength="20" class="form-control" id="residencial" name="residencial" value="${direccion_r.dir_calle} #${direccion_r.dir_numero} -${direccion_r.dir_sector}"
									 		onkeypress="return funcionMaestra(event,'dire','1')" placeholder="Los Peumos #47 - Collao">
										</c:if>
									</c:forEach>
								</div>
							</div>
							<div class="form-group">
								<label class="col-xs-3 control-label" for="academica">Dirección Académica:</label>
								<div class="col-xs-9">
									<c:forEach var="direccion_r" items="${direccionList}" >
										<c:if test="${direccion_r.dir_tp_id == 1}" >
											<input type="text" maxlength="20" class="form-control" id="academica" name="academica" value="${direccion_r.dir_calle} #${direccion_r.dir_numero} -${direccion_r.dir_sector}"
									 		onkeypress="return funcionMaestra(event,'dire','1')" placeholder="Los Peumos #47 - Collao">
										</c:if>
									</c:forEach>
									
								</div>
							</div>
						</c:if>
						
						
				 		<c:if test="${contador == 0}" >
								<div class="row">
									<div class="col-xs-5 col-sm-offset-1 col-xs-offset-1">
										<legend>Ubicación Dirección Academica</legend>
									</div>
						 		</div>
								<div class="form-group">
									<label class="col-xs-3 control-label" for="region">Región Dirección Académica:</label>
									<div class="col-xs-9">							
										<select class="form-control" id="region" name="region" required>
										<option value="">Seleccione Región...</option>
										<c:forEach var="regiones" items="${regionList}" >
												<option value="${regiones.reg_id}">${regiones.reg_nombre}</option>
										</c:forEach>
										</select>							
									</div>
								</div>
									<div id="div_provincia">
										<div class="form-group">
											<label class="col-xs-3 control-label" for="provincia">Provincia Dirección Académica:</label>
											<div class="col-xs-9">							
												<select class="form-control" id="provincia" name="provincia" required>
												<option value="">Seleccione Provincia...</option>
												<c:forEach var="provincias" items="${provinciaList}" >
														<option value="${provincias.pro_id}">${provincias.pro_nombre}</option>
												</c:forEach>							
												</select>					
											</div>
										</div>
									</div>
									<div id="div_comuna">
										<div class="form-group">
											<label class="col-xs-3 control-label" for="comuna">Comuna Dirección Académica:</label>
											<div class="col-xs-9">
												<select class="form-control" id="comuna" name="comuna" required>
												<option value="">Seleccione Comuna...</option>
												<c:forEach var="comunas" items="${comunaList}" >
														<option value="${comunas.com_id}">${comunas.com_nombre}</option>
												</c:forEach>	
												</select>							
											</div>
										</div>
									</div>
							
								<div class="row">
									<div class="col-xs-5 col-sm-offset-1 col-xs-offset-1">
										<legend>Ubicación Dirección Residencial</legend>
									</div>
						 		</div>
								<div class="form-group">
								<label class="col-xs-3 control-label" for="region">Región Dirección Residencial:</label>
								<div class="col-xs-9">							
									<select class="form-control" id="region_2" name="region_2" required>
									<option value="">Seleccione Región...</option>
									<c:forEach var="regiones" items="${regionList}" >
											<option value="${regiones.reg_id}">${regiones.reg_nombre}</option>
									</c:forEach>
									</select>							
								</div>
								</div>
								<div id="div_provincia_2">
									<div class="form-group">
									<label class="col-xs-3 control-label" for="provincia">Provincia Dirección Residencial:</label>
									<div class="col-xs-9">							
										<select class="form-control" id="provincia_2" name="provincia_2" required>
										<option value="">Seleccione Provincia...</option>
										<c:forEach var="provincias" items="${provinciaList}" >
												<option value="${provincias.pro_id}">${provincias.pro_nombre}</option>
										</c:forEach>							
										</select>					
									</div>
								</div>
								</div>
								<div id="div_comuna_2">
									<div class="form-group">
										<label class="col-xs-3 control-label" for="comuna">Comuna Dirección Residencial:</label>
										<div class="col-xs-9">
											<select class="form-control" id="comuna_2" name="comuna_2" required>
											<option value="">Seleccione Comuna...</option>
											<c:forEach var="comunas" items="${comunaList}" >
													<option value="${comunas.com_id}">${comunas.com_nombre}</option>
											</c:forEach>	
											</select>							
										</div>
									</div>
								</div>
				 		</c:if>
				 		
				 		<c:if test="${contador == 2}" >
				 			<c:forEach var="direccion_r" items="${direccionList}" >
									<c:if test="${direccion_r.dir_tp_id == 1}" >
										<div class="row">
											<div class="col-xs-5 col-sm-offset-1 col-xs-offset-1">
												<legend>Ubicación Dirección Academica</legend>
											</div>
								 		</div>
										<div class="form-group">
											<label class="col-xs-3 control-label" for="region">Región Dirección Académica:</label>
											<div class="col-xs-9">							
												<select class="form-control" id="region" name="region" required>
												<option value="">Seleccione Región...</option>
												<c:forEach var="regiones" items="${regionList}" >
													<c:if test="${regiones.reg_id == region_aca.reg_id}" >
															<option value="${region_aca.reg_id}" selected="selected">${region_aca.reg_nombre}</option>
														</c:if>
														<c:if test="${regiones.reg_id != region_aca.reg_id}" >
															<option value="${regiones.reg_id}">${regiones.reg_nombre}</option>
														</c:if>
												</c:forEach>
												</select>							
											</div>
										</div>
											<div id="div_provincia">
												<div class="form-group">
													<label class="col-xs-3 control-label" for="provincia">Provincia Dirección Académica:</label>
													<div class="col-xs-9">							
														<select class="form-control" id="provincia" name="provincia" required>
														<option value="">Seleccione Provincia...</option>
														<c:forEach var="provincias" items="${provinciaList}" >
															<c:if test="${provincias.pro_id == provincia_aca.pro_id}" >
																<option value="${provincia_aca.pro_id}" selected="selected">${provincia_aca.pro_nombre}</option>
															</c:if>
														</c:forEach>							
														</select>					
													</div>
												</div>
											</div>
											<div id="div_comuna">
												<div class="form-group">
													<label class="col-xs-3 control-label" for="comuna">Comuna Dirección Académica:</label>
													<div class="col-xs-9">
														<select class="form-control" id="comuna" name="comuna" required>
														<option value="">Seleccione Comuna...</option>
														<c:forEach var="comunas" items="${comunaList}" >
															<c:if test="${comunas.com_id == comuna_aca.com_id}" >
															<option value="${comuna_aca.com_id}" selected="selected">${comuna_aca.com_nombre}</option>
															</c:if>
														</c:forEach>	
														</select>							
													</div>
												</div>
											</div>
									</c:if>
									<c:if test="${direccion_r.dir_tp_id == 2}" >
										<div class="row">
											<div class="col-xs-5 col-sm-offset-1 col-xs-offset-1">
												<legend>Ubicación Dirección Residencial</legend>
											</div>
								 		</div>
										<div class="form-group">
										<label class="col-xs-3 control-label" for="region">Región Dirección Residencial:</label>
										<div class="col-xs-9">							
											<select class="form-control" id="region_2" name="region_2" required>
											<option value="">Seleccione Región...</option>
											<c:forEach var="regiones" items="${regionList}" >
												<c:if test="${regiones.reg_id == region_res.reg_id}" >
														<option value="${region_res.reg_id}" selected="selected">${region_res.reg_nombre}</option>
													</c:if>
													<c:if test="${regiones.reg_id != region_res.reg_id}" >
														<option value="${regiones.reg_id}">${regiones.reg_nombre}</option>
													</c:if>
											</c:forEach>
											</select>							
										</div>
										</div>
										<div id="div_provincia_2">
											<div class="form-group">
											<label class="col-xs-3 control-label" for="provincia">Provincia Dirección Residencial:</label>
											<div class="col-xs-9">							
												<select class="form-control" id="provincia_2" name="provincia_2" required>
												<option value="">Seleccione Provincia...</option>
												<c:forEach var="provincias" items="${provinciaList}" >
													<c:if test="${provincias.pro_id == provincia_res.pro_id}" >
														<option value="${provincia_res.pro_id}" selected="selected">${provincia_res.pro_nombre}</option>
													</c:if>
												</c:forEach>							
												</select>					
											</div>
										</div>
										</div>
										<div id="div_comuna_2">
											<div class="form-group">
												<label class="col-xs-3 control-label" for="comuna">Comuna Dirección Residencial:</label>
												<div class="col-xs-9">
													<select class="form-control" id="comuna_2" name="comuna_2" required>
													<option value="">Seleccione Comuna...</option>
													<c:forEach var="comunas" items="${comunaList}" >
														<c:if test="${comunas.com_id == comuna_res.com_id}" >
														<option value="${comuna_res.com_id}" selected="selected">${comuna_res.com_nombre}</option>
														</c:if>
													</c:forEach>	
													</select>							
												</div>
											</div>
										</div>
									</c:if>
							</c:forEach>
				 		</c:if>
						
						
						<button type="submit" onclick="return validar_alumno(event)" class="btn btn-primary">Modificar Datos</button>
					</div>
					</form>	
				</div>
			</div>
	</jsp:body>
</t:plantillaFormularioAlumno>


						
						
