<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:plantillaFormularioAlumno>
    <jsp:attribute name="header"> 
		
    </jsp:attribute>
    		
    <jsp:body>
    <script>
	$(document).ready(function(){
		$("#profesor").change(actualizarBotonEspecialidades);
		
		$("#horas").tooltip({
	        placement : 'top'
	    });
		
	});
	$(document).ready(function () {
		$('#alumno').bind('copy paste drop', function (e) {
	           e.preventDefault();
	    });       
	    $("#alumno").focusin(function() {
        	$("#alumno").attr('autocomplete', 'off');
        });
        $("#alumno").focusout(function() {
        	$("#alumno").removeAttr('autocomplete');
        });
		$('#nombre_obra').bind('copy paste drop', function (e) {
	           e.preventDefault();
	    });       
	    $("#nombre_obra").focusin(function() {
        	$("#nombre_obra").attr('autocomplete', 'off');
        });
        $("#nombre_obra").focusout(function() {
        	$("#nombre_obra").removeAttr('autocomplete');
        });
        $('#direccion').bind('copy paste drop', function (e) {
	           e.preventDefault();
	        });
        $("#direccion").focusin(function() {
        	$("#direccion").attr('autocomplete', 'off');
        });
        $("#direccion").focusout(function() {
        	$("#direccion").removeAttr('autocomplete');
        });
        $('#tareas').bind('copy paste drop', function (e) {
	           e.preventDefault();
	        });
        $("#tareas").focusin(function() {
        	$("#tareas").attr('autocomplete', 'off');
        });
        $("#tareas").focusout(function() {
        	$("#tareas").removeAttr('autocomplete');
        });
        $('#inicio').bind('copy paste drop', function (e) {
	           e.preventDefault();
	        });
     $("#inicio").focusin(function() {
     	$("#inicio").attr('readonly', 'readonly');
     	$("#inicio").attr('autocomplete', 'off');
     });
     $("#inicio").focusout(function() {
     	$("#inicio").removeAttr('readonly');
     	$("#inicio").removeAttr('autocomplete');
     });
     $('#termino').bind('copy paste drop', function (e) {
	           e.preventDefault();
	        });
     $("#termino").focusin(function() {
     	$("#termino").attr('readonly', 'readonly');
     	$("#termino").attr('autocomplete', 'off');
     });
     $("#termino").focusout(function() {
     	$("#termino").removeAttr('readonly');
     	$("#termino").removeAttr('autocomplete');
     });
        $('#horas').bind('copy paste drop', function (e) {
	           e.preventDefault();
	        });
        $("#horas").focusin(function() {
        	$("#horas").attr('autocomplete', 'off');
        });
        $("#horas").focusout(function() {
        	$("#horas").removeAttr('autocomplete');
        });
	});
	</script>
    		<div class="row">
				<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8  col-sm-offset-4 col-md-offset-4  col-lg-offset-4 columna">
					<legend>Pre-Inscribir Gestión</legend>
				</div>
			</div>
	     	<div class="row">
				<div class="col-xs-12 col-sm-10 col-md-10 col-lg-10 col-sm-offset-1 col-md-offset-1  col-lg-offset-1 columna">		
					<form class="form-horizontal" id="preinscripcion" role="form" method="POST" action="preInscripcionServlet.do">
						<div class="form-group">
						<label class="col-xs-3 control-label" for="empresa">Empresa:</label>
							<div class="col-xs-9">
								<select class="form-control" id="empresa" name="empresa" required>				
									<c:forEach var="empresas" items="${empresasList}" >
											<option value="${empresas.emp_rut}">${empresas.emp_nombre}</option>
									</c:forEach>
									<option selected disabled selected="selected">Seleccione Empresa...</option>	
								</select>
							</div>
						</div>
						<div id="div_supervisor">
							<div class="form-group ">
							<label class="col-xs-3 control-label" for="supervisor">Supervisores Activos :</label>
								<div class="col-xs-9">
									<select class="form-control" id="supervisor" name="supervisor">										
												<option value="">Seleccione Supervisor...</option>							
									</select>
								</div>
							</div>
						</div>
						<div class="form-group">
						<label class="col-xs-3 control-label" for="profesor">Profesor Guía:</label>
							<div class="col-xs-8">
								<select class="form-control" id="profesor" name="profesor" required>	
									<c:forEach var="academico" items="${academicosList}" >
											<option value="${academico.aca_rut}">${academico.aca_nombres} ${academico.aca_apellido_p} ${academico.aca_apellido_m}</option>
									</c:forEach>
									<option selected disabled selected="selected">Seleccione Profesor Guía...</option>
								</select>
							</div>
							<div id="div_boton_especialidades">
								
							</div>							
						</div>					
						<div class="form-group">
						<label class="col-xs-3 control-label" for="obra">Tipo Práctica/Obra:</label>
							<div class="col-xs-9">
								<select class="form-control" id="obra" name="obra" required>	
									<c:forEach var="obras" items="${obrasList}" >
											<option value="${obras.obr_id}">${obras.obr_nombre}</option>
									</c:forEach>
									<option selected disabled selected="selected">Seleccione Obra...</option>
								</select>
							</div>
						</div>
						<div class="form-group">
						<label class="col-xs-3 control-label" for="gestion">Gestión:</label>
							<div class="col-xs-9">
								<select class="form-control" id="gestion" name="gestion" required>
										<option value="${inscripcion.asi_codigo}"> ${asignatura.asi_nombre}</option>
								</select>
							</div>
						</div>
						<div class="form-group ">
						<label class="col-xs-3 control-label" for="plan">Plan de Estudio:</label>
							<div class="col-xs-9">
								<select class="form-control" id="plan" name="plan" required>
										<option value="${inscripcion.plan_id}"> ${plan.plan_nombre}</option>
								</select>
							</div>
						</div>
						<h3>Información de la Obra</h3>
						<div class="form-group ">
						<label class="col-xs-3 control-label" for="nombre_obra">Nombre de la Obra:</label>
							<div class="col-xs-9">
								<input type="text" class="form-control" id="nombre_obra" name="nombre_obra" value="" placeholder="Ej: Reconstrucción Edificio Algarrobo" required onkeypress="return funcionMaestra(event,'nombres','1')">				

								<!-- <div class="alert alert-danger alert-dismissible" role="alert">
									<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
  									<strong>Error!</strong> Nombre de obra debe empezar con una letra.								
								</div>-->
							</div>
							
						</div>
						
						<div class="form-group">
						<label class="col-xs-3 control-label" for="region">Región:</label>
						<div class="col-xs-9">							
							<select class="form-control" id="region" name="region" required>
							<option value="">Seleccione Región...</option>
							<c:forEach var="regiones" items="${regionList}" >
								<option value="${regiones.reg_id}">${regiones.reg_nombre}</option>
							</c:forEach>
							</select>							
						</div>
						</div>
					
						<div id="div_provincia">
							<div class="form-group">
								<label class="col-xs-3 control-label" for="provincia">Provincia:</label>
								<div class="col-xs-9">							
									<select class="form-control" id="provincia" name="provincia" required>
									<option value="">Seleccione Provincia...</option>										
									</select>					
								</div>
							</div>
						</div>			
						<div id="div_comuna">
							<div class="form-group">
								<label class="col-xs-3 control-label" for="comuna">Comuna:</label>
								<div class="col-xs-9">
									
									<select class="form-control" id="comuna" name="comuna" required>
									<option value="">Seleccione Comuna...</option>
									</select>							
								</div>
							</div>
						</div>
						<div class="form-group ">
						<label class="col-xs-3 control-label" for="direccion">Dirección de la Obra:</label>
							<div class="col-xs-9">
								<input type="text" class="form-control" id="direccion" name="direccion" value="" placeholder="Ej: Los Alerces #889" required onkeypress="return funcionMaestra(event,'dire','1')">						
							</div>
						</div>
						<div class="form-group ">
						<label class="col-xs-3 control-label" for="tareas">Labor a Desarrollar:</label>
							<div class="col-xs-9">
								<textarea class="form-control" id="tareas" name="tareas" placeholder="Ej: - Supervisión de Obra" onkeypress="return funcionMaestra(event,'descri','2')"></textarea>					
							</div>
						</div>
						<script type="text/javascript">
				            // When the document is ready
				            $(document).ready(function () {
				                
				                $('#inicio').datepicker({
				                    format: "dd-mm-yyyy",
				                    weekStart: 1,
				                    autoclose: true,
				                    orientation: "auto bottom",
				                    startDate: "+0d",
				                    endDate: "+6w"
				                });  
				                $('#termino').datepicker({
				                    format: "dd-mm-yyyy",
				                    weekStart: 1,
				                    autoclose: true,
				                    orientation: "auto bottom",
				                    startDate: "+0d",
					                endDate: "+10m"
				                });  
				            });
				        </script>
						<div class="form-group ">
						<label class="col-xs-3 control-label" for="inicio">Fecha Inicio Práctica:</label>
							<div class="col-xs-9">
								<div class='input-group date'>
				                    <input type='text' class="form-control"  id="inicio" name="inicio" placeholder="Haga click para desplegar un Calendario" required readonly="readonly"/>
				                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
				                    </span>
			                	</div>					    
				            </div>
						</div>
						<div class="form-group ">
						<label class="col-xs-3 control-label" for="termino">Fecha Término Práctica:</label>
							<div class="col-xs-9">
								<div class='input-group date'>
				                    <input type='text' class="form-control"  id="termino" name="termino" placeholder="Haga click para desplegar un Calendario" required readonly="readonly"/>
				                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
				                    </span>
			                	</div>						
							</div>
						</div>
						<div class="form-group">
						<label class="col-xs-3 control-label" for="horas">Cantidad de Horas:</label>
							<div class="col-xs-9">
								<input type="tel" class="form-control" id="horas" name="horas" value="" required maxlength="4" onkeypress="return funcionMaestra(event,'num','1')">
								
							</div>
						</div>
						<input type="hidden" id="fecha_ins" name="fecha_ins" value="${inscripcion.ins_fecha}" >
						<input type="hidden" id="alumno" name="alumno" value="${rut}">
						<button type="submit" class="btn btn-primary" onclick="return validar_preinscripcion()">Registrar Práctica</button>					
					</form>
				</div>
			</div>
			<div id="div_especialidades">
			</div>  	
    </jsp:body>
    
</t:plantillaFormularioAlumno>